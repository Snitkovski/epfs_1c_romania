<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@Имя">
		<xsl:attribute name="Name"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@Вид">
		<xsl:attribute name="Kind"><xsl:choose><xsl:when test="current()='Реквизит'">Attribute</xsl:when><xsl:when test="current()='Свойство'">Property</xsl:when><xsl:when test="current()='Измерение'">Dimension</xsl:when><xsl:when test="current()='ТабличнаяЧасть'">TabularSection</xsl:when><xsl:when test="current()='ПодчиненныйСправочник'">SubordinateCatalog</xsl:when><xsl:when test="current()='НаборЗаписейПоследовательности'">SequenceRecordSet</xsl:when><xsl:when test="current()='НаборДвиженийРегистраБухгалтерии'">AccountingRegistersRecordSet</xsl:when><xsl:when test="current()='НаборДвиженийРегистраНакопления'">AccumulationRegistersRecordSet</xsl:when><xsl:when test="current()='НаборДвиженийРегистраРасчета'">CalculationRegistersRecordSet</xsl:when><xsl:when test="current()='НаборДвиженийРегистраСведений'">InformationRegistersRecordSet</xsl:when><xsl:when test="current()='НаборЗаписейПодчиненногоРегистраСведений'">SubordinateInformationRegistersRecordSet</xsl:when><xsl:when test="current()='ВидыСубконтоСчета'">AccountExtDimensionTypes</xsl:when><xsl:otherwise>current()</xsl:otherwise></xsl:choose></xsl:attribute>
	</xsl:template>
	<xsl:template match="@Тип">
		<xsl:attribute name="Type"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@Отключить">
		<xsl:attribute name="Disable"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@Имя">
		<xsl:attribute name="Name"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@ИспользуетсяПриЗагрузке">
		<xsl:attribute name="UsedOnImport"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="Источник">
		<Source>
			<xsl:apply-templates select="@*|node()"/>
		</Source>
	</xsl:template>
	<xsl:template match="Приемник">
		<Destination>
			<xsl:apply-templates select="@*|node()"/>
		</Destination>
	</xsl:template>
	<xsl:template match="Имя">
		<Name>
			<xsl:apply-templates select="@*|node()"/>
		</Name>
	</xsl:template>
	<xsl:template match="Ид">
		<ID>
			<xsl:apply-templates select="@*|node()"/>
		</ID>
	</xsl:template>
	<xsl:template match="ДатаВремяСоздания">
		<CreationDateTime>
			<xsl:apply-templates select="@*|node()"/>
		</CreationDateTime>
	</xsl:template>
	<xsl:template match="ВерсияФормата">
		<FormatVersion>
			<xsl:apply-templates select="@*|node()"/>
		</FormatVersion>
	</xsl:template>
	<xsl:template match="ПравилаОбмена">
		<ExchangeRules>
			<xsl:apply-templates select="@*|node()"/>
		</ExchangeRules>
	</xsl:template>
	<xsl:template match="ПравилаВыгрузкиДанных">
		<DataExportRules>
			<xsl:apply-templates select="@*|node()"/>
		</DataExportRules>
	</xsl:template>
	<xsl:template match="ДатаВыгрузки">
		<ExportDate>
			<xsl:apply-templates select="@*|node()"/>
		</ExportDate>
	</xsl:template>
	<xsl:template match="ИмяКонфигурацииИсточника">
		<SourceConfigurationName>
			<xsl:apply-templates select="@*|node()"/>
		</SourceConfigurationName>
	</xsl:template>
	<xsl:template match="ВерсияКонфигурацииИсточника">
		<SourceConfigurationVersion>
			<xsl:apply-templates select="@*|node()"/>
		</SourceConfigurationVersion>
	</xsl:template>
	<xsl:template match="ИмяКонфигурацииПриемника">
		<DestinationConfigurationName>
			<xsl:apply-templates select="@*|node()"/>
		</DestinationConfigurationName>
	</xsl:template>
	<xsl:template match="ИдПравилКонвертации">
		<ConversionRuleIDs>
			<xsl:apply-templates select="@*|node()"/>
		</ConversionRuleIDs>
	</xsl:template>
	<xsl:template match="Код">
		<Code>
			<xsl:apply-templates select="@*|node()"/>
		</Code>
	</xsl:template>
	<xsl:template match="Свойство">
		<Property>
			<xsl:apply-templates select="@*|node()"/>
		</Property>
	</xsl:template>
	<xsl:template match="ПередОбработкойВыгрузки">
		<BeforeProcessExport>
			<xsl:apply-templates select="@*|node()"/>
		</BeforeProcessExport>
	</xsl:template>
	<xsl:template match="ПослеОбработкиВыгрузки">
		<AfterProcessExport>
			<xsl:apply-templates select="@*|node()"/>
		</AfterProcessExport>
	</xsl:template>
	<xsl:template match="Наименование">
		<Description>
			<xsl:apply-templates select="@*|node()"/>
		</Description>
	</xsl:template>
	<xsl:template match="Порядок">
		<Order>
			<xsl:apply-templates select="@*|node()"/>
		</Order>
	</xsl:template>
	<xsl:template match="НеЗамещать">
		<DoNotReplace>
			<xsl:apply-templates select="@*|node()"/>
		</DoNotReplace>
	</xsl:template>
	<xsl:template match="КодПравилаКонвертации">
		<ConversionRuleCode>
			<xsl:apply-templates select="@*|node()"/>
		</ConversionRuleCode>
	</xsl:template>
	<xsl:template match="ПередВыгрузкой">
		<BeforeExport>
			<xsl:apply-templates select="@*|node()"/>
		</BeforeExport>
	</xsl:template>
	<xsl:template match="ПриВыгрузке">
		<OnExport>
			<xsl:apply-templates select="@*|node()"/>
		</OnExport>
	</xsl:template>
	<xsl:template match="ПослеВыгрузки">
		<AfterExport>
			<xsl:apply-templates select="@*|node()"/>
		</AfterExport>
	</xsl:template>
	<xsl:template match="ВыгружатьГруппуЧерезФайл">
		<ExportGroupToFile>
			<xsl:apply-templates select="@*|node()"/>
		</ExportGroupToFile>
	</xsl:template>
	<xsl:template match="ПолучитьИзВходящихДанных">
		<GetFromIncomingData>
			<xsl:apply-templates select="@*|node()"/>
		</GetFromIncomingData>
	</xsl:template>
	<xsl:template match="Группа">
		<Group>
			<xsl:apply-templates select="@*|node()"/>
		</Group>
	</xsl:template>
	<xsl:template match="ПриводитьКДлине">
		<CastToLength>
			<xsl:apply-templates select="@*|node()"/>
		</CastToLength>
	</xsl:template>
	<xsl:template match="ИмяПараметраДляПередачи">
		<ParameterForTransferName>
			<xsl:apply-templates select="@*|node()"/>
		</ParameterForTransferName>
	</xsl:template>
	<xsl:template match="ПоискПоДатеНаРавенство">
		<SearchByEqualDate>
			<xsl:apply-templates select="@*|node()"/>
		</SearchByEqualDate>
	</xsl:template>
	<xsl:template match="Свойства">
		<Properties>
			<xsl:apply-templates select="@*|node()"/>
		</Properties>
	</xsl:template>
	<xsl:template match="Значение">
		<Value>
			<xsl:apply-templates select="@*|node()"/>
		</Value>
	</xsl:template>
	<xsl:template match="Значения">
		<Values>
			<xsl:apply-templates select="@*|node()"/>
		</Values>
	</xsl:template>
	<xsl:template match="СинхронизироватьПоИдентификатору">
		<SynchronizeByID>
			<xsl:apply-templates select="@*|node()"/>
		</SynchronizeByID>
	</xsl:template>
	<xsl:template match="НеСоздаватьЕслиНеНайден">
		<DoNotCreateIfNotFound>
			<xsl:apply-templates select="@*|node()"/>
		</DoNotCreateIfNotFound>
	</xsl:template>
	<xsl:template match="РегистрироватьОбъектНаУзлеОтправителе">
		<RecordObjectChangeAtSenderNode>
			<xsl:apply-templates select="@*|node()"/>
		</RecordObjectChangeAtSenderNode>
	</xsl:template>
	<xsl:template match="НеВыгружатьОбъектыСвойствПоСсылкам">
		<DoNotExportPropertyObjectsByRefs>
			<xsl:apply-templates select="@*|node()"/>
		</DoNotExportPropertyObjectsByRefs>
	</xsl:template>
	<xsl:template match="ПродолжитьПоискПоПолямПоискаЕслиПоИдентификаторуНеНашли">
		<SearchBySearchFieldsIfNotFoundByID>
			<xsl:apply-templates select="@*|node()"/>
		</SearchBySearchFieldsIfNotFoundByID>
	</xsl:template>
	<xsl:template match="ПриПереносеОбъектаПоСсылкеУстанавливатьТолькоGIUD">
		<OnMoveObjectByRefSetGIUDOnly>
			<xsl:apply-templates select="@*|node()"/>
		</OnMoveObjectByRefSetGIUDOnly>
	</xsl:template>
	<xsl:template match="НеЗамещатьОбъектСозданныйВИнформационнойБазеПриемнике">
		<DoNotReplaceObjectCreatedInDestinationInfobase>
			<xsl:apply-templates select="@*|node()"/>
		</DoNotReplaceObjectCreatedInDestinationInfobase>
	</xsl:template>
	<xsl:template match="ИспользоватьБыстрыйПоискПриЗагрузке">
		<UseQuickSearchOnImport>
			<xsl:apply-templates select="@*|node()"/>
		</UseQuickSearchOnImport>
	</xsl:template>
	<xsl:template match="ГенерироватьНовыйНомерИлиКодЕслиНеУказан">
		<GenerateNewNumberOrCodeIfNotSet>
			<xsl:apply-templates select="@*|node()"/>
		</GenerateNewNumberOrCodeIfNotSet>
	</xsl:template>
	<xsl:template match="НеЗапоминатьВыгруженные">
		<DoNotRememberExported>
			<xsl:apply-templates select="@*|node()"/>
		</DoNotRememberExported>
	</xsl:template>
	<xsl:template match="ПослеВыгрузкиВФайл">
		<AfterExportToFile>
			<xsl:apply-templates select="@*|node()"/>
		</AfterExportToFile>
	</xsl:template>
	<xsl:template match="ПередЗагрузкой">
		<BeforeImport>
			<xsl:apply-templates select="@*|node()"/>
		</BeforeImport>
	</xsl:template>
	<xsl:template match="ПриЗагрузке">
		<OnImport>
			<xsl:apply-templates select="@*|node()"/>
		</OnImport>
	</xsl:template>
	<xsl:template match="ПослеЗагрузки">
		<AfterImport>
			<xsl:apply-templates select="@*|node()"/>
		</AfterImport>
	</xsl:template>
	<xsl:template match="ПоследовательностьПолейПоиска">
		<SearchFieldSequence>
			<xsl:apply-templates select="@*|node()"/>
		</SearchFieldSequence>
	</xsl:template>
	<xsl:template match="ПриоритетОбъектовОбмена">
		<ExchangeObjectsPriority>
			<xsl:apply-templates select="@*|node()"/>
		</ExchangeObjectsPriority>
	</xsl:template>
	<xsl:template match="НастройкаВариантовПоискаОбъектов">
		<ObjectSearchOptionsSettings>
			<xsl:apply-templates select="@*|node()"/>
		</ObjectSearchOptionsSettings>
	</xsl:template>
	<xsl:template match="ПоискПоТабличнымЧастям">
		<SearchInTabularSections>
			<xsl:apply-templates select="@*|node()"/>
		</SearchInTabularSections>
	</xsl:template>
	<xsl:template match="ПоляПоиска">
		<SearchFields>
			<xsl:apply-templates select="@*|node()"/>
		</SearchFields>
	</xsl:template>
	<xsl:template match="ПоляТаблицы">
		<TableFields>
			<xsl:apply-templates select="@*|node()"/>
		</TableFields>
	</xsl:template>
	<xsl:template match="Правило">
		<Rule>
			<xsl:apply-templates select="@*|node()"/>
		</Rule>
	</xsl:template>
	<xsl:template match="ИмяНастройкиДляАлгоритма">
		<AlgorithmSettingName>
			<xsl:apply-templates select="@*|node()"/>
		</AlgorithmSettingName>
	</xsl:template>
	<xsl:template match="ИмяНастройкиДляПользователя">
		<UserSettingsName>
			<xsl:apply-templates select="@*|node()"/>
		</UserSettingsName>
	</xsl:template>
	<xsl:template match="ОписаниеНастройкиДляПользователя">
		<SettingDetailsForUser>
			<xsl:apply-templates select="@*|node()"/>
		</SettingDetailsForUser>
	</xsl:template>
	<xsl:template match="ВариантПоиска">
		<SearchMode>
			<xsl:apply-templates select="@*|node()"/>
		</SearchMode>
	</xsl:template>
	<xsl:template match="ПравилаКонвертацииОбъектов">
		<ObjectConversionRules>
			<xsl:apply-templates select="@*|node()"/>
		</ObjectConversionRules>
	</xsl:template>
	<xsl:template match="СпособОтбораДанных">
		<DataFilterMethod>
			<xsl:choose>
				<xsl:when test="text()='СтандартнаяВыборка'">StandardSelection</xsl:when>
				<xsl:when test="text()='ПроизвольныйАлгоритм'">ArbitraryAlgorithm</xsl:when>
				<xsl:otherwise>text()</xsl:otherwise>
			</xsl:choose>
		</DataFilterMethod>
	</xsl:template>
	<xsl:template match="ОбъектВыборки">
		<SelectionObject>
			<xsl:apply-templates select="@*|node()"/>
		</SelectionObject>
	</xsl:template>
	<xsl:template match="УдалятьЗаПериод">
		<DeleteForPeriod>
			<xsl:apply-templates select="@*|node()"/>
		</DeleteForPeriod>
	</xsl:template>
	<xsl:template match="Непосредственно">
		<Directly>
			<xsl:apply-templates select="@*|node()"/>
		</Directly>
	</xsl:template>
	<xsl:template match="ПередОбработкойПравила">
		<BeforeProcessRule>
			<xsl:apply-templates select="@*|node()"/>
		</BeforeProcessRule>
	</xsl:template>
	<xsl:template match="ПослеОбработкиПравила">
		<AfterProcessRule>
			<xsl:apply-templates select="@*|node()"/>
		</AfterProcessRule>
	</xsl:template>
	<xsl:template match="ПередУдалениемОбъекта">
		<BeforeDeleteObject>
			<xsl:apply-templates select="@*|node()"/>
		</BeforeDeleteObject>
	</xsl:template>
	<xsl:template match="ПравилаОчисткиДанных">
		<DataClearingRules>
			<xsl:apply-templates select="@*|node()"/>
		</DataClearingRules>
	</xsl:template>
	<xsl:template match="Текст">
		<Text>
			<xsl:apply-templates select="@*|node()"/>
		</Text>
	</xsl:template>
	<xsl:template match="Алгоритм">
		<Algorithm>
			<xsl:apply-templates select="@*|node()"/>
		</Algorithm>
	</xsl:template>
	<xsl:template match="Алгоритмы">
		<Algorithms>
			<xsl:apply-templates select="@*|node()"/>
		</Algorithms>
	</xsl:template>
	<xsl:template match="Запрос">
		<Query>
			<xsl:apply-templates select="@*|node()"/>
		</Query>
	</xsl:template>
	<xsl:template match="Запросы">
		<Queries>
			<xsl:apply-templates select="@*|node()"/>
		</Queries>
	</xsl:template>
	<xsl:template match="Параметр">
		<Parameter>
			<xsl:apply-templates select="@*|node()"/>
		</Parameter>
	</xsl:template>
	<xsl:template match="Параметры">
		<Parameters>
			<xsl:apply-templates select="@*|node()"/>
		</Parameters>
	</xsl:template>
	<xsl:template match="ВыбиратьДанныеДляВыгрузкиОднимЗапросом">
		<SelectExportDataInSingleQuery>
			<xsl:apply-templates select="@*|node()"/>
		</SelectExportDataInSingleQuery>
	</xsl:template>
	<xsl:template match="НеВыгружатьОбъектыСозданныеВБазеПриемнике">
		<DoNotExportObjectsCreatedInDestinationInfobase>
			<xsl:apply-templates select="@*|node()"/>
		</DoNotExportObjectsCreatedInDestinationInfobase>
	</xsl:template>
	<xsl:template match="ИмяТипаПриемника">
		<DestinationTypeName>
			<xsl:apply-templates select="@*|node()"/>
		</DestinationTypeName>
	</xsl:template>
	<xsl:template match="Обработка">
		<DataProcessor>
			<xsl:apply-templates select="@*|node()"/>
		</DataProcessor>
	</xsl:template>
	<xsl:template match="Обработки">
		<DataProcessors>
			<xsl:apply-templates select="@*|node()"/>
		</DataProcessors>
	</xsl:template>
	<xsl:template match="ПередВыгрузкойОбъекта">
		<BeforeExportObject>
			<xsl:apply-templates select="@*|node()"/>
		</BeforeExportObject>
	</xsl:template>
	<xsl:template match="ПослеВыгрузкиОбъекта">
		<AfterExportObject>
			<xsl:apply-templates select="@*|node()"/>
		</AfterExportObject>
	</xsl:template>
</xsl:stylesheet>
