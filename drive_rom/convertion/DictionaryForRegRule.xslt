<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@Отключить">
		<xsl:attribute name="Disable"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@Валидное">
		<xsl:attribute name="Valid"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@Имя">
		<xsl:attribute name="Name"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@ВерсияПлатформы">
		<xsl:attribute name="PlatformVersion"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@ВерсияКонфигурации">
		<xsl:attribute name="ConfigurationVersion"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="@СинонимКонфигурации">
		<xsl:attribute name="ConfigurationSynonym"><xsl:value-of select="current()"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="ПравилаРегистрации">
		<RecordRules>
			<xsl:apply-templates select="@*|node()"/>
		</RecordRules>
	</xsl:template>
	<xsl:template match="ВерсияФормата">
		<FormatVersion>
			<xsl:apply-templates select="@*|node()"/>
		</FormatVersion>
	</xsl:template>
	<xsl:template match="Ид">
		<ID>
			<xsl:apply-templates select="@*|node()"/>
		</ID>
	</xsl:template>
	<xsl:template match="Наименование">
		<Description>
			<xsl:apply-templates select="@*|node()"/>
		</Description>
	</xsl:template>
	<xsl:template match="ДатаВремяСоздания">
		<CreationDateTime>
			<xsl:apply-templates select="@*|node()"/>
		</CreationDateTime>
	</xsl:template>
	<xsl:template match="ПланОбмена">
		<ExchangePlan>
			<xsl:apply-templates select="@*|node()"/>
		</ExchangePlan>
	</xsl:template>
	<xsl:template match="Комментарий">
		<Comment>
			<xsl:apply-templates select="@*|node()"/>
		</Comment>
	</xsl:template>
	<xsl:template match="Конфигурация">
		<Configuration>
			<xsl:apply-templates select="@*|node()"/>
		</Configuration>
	</xsl:template>
	<xsl:template match="ПравилаРегистрацииОбъектов">
		<ObjectsRegistrationRules>
			<xsl:apply-templates select="@*|node()"/>
		</ObjectsRegistrationRules>
	</xsl:template>
	<xsl:template match="ПравилаРегистрации">
		<RecordRules>
			<xsl:apply-templates select="@*|node()"/>
		</RecordRules>
	</xsl:template>
	<xsl:template match="Правило">
		<Rule>
			<xsl:apply-templates select="@*|node()"/>
		</Rule>
	</xsl:template>
	<xsl:template match="Группа">
		<Group>
			<xsl:apply-templates select="@*|node()"/>
		</Group>
	</xsl:template>
	<xsl:template match="ОбъектНастройки">
		<SettingObject>
			<xsl:apply-templates select="@*|node()"/>
		</SettingObject>
	</xsl:template>
	<xsl:template match="ОбъектМетаданныхИмя">
		<MetadataObjectName>
			<xsl:apply-templates select="@*|node()"/>
		</MetadataObjectName>
	</xsl:template>
	<xsl:template match="РеквизитРежимаВыгрузки">
		<ExportModeAttribute>
			<xsl:apply-templates select="@*|node()"/>
		</ExportModeAttribute>
	</xsl:template>
	<xsl:template match="ОтборПоСвойствамПланаОбмена">
		<FilterByExchangePlanProperties>
			<xsl:apply-templates select="@*|node()"/>
		</FilterByExchangePlanProperties>
	</xsl:template>
	<xsl:template match="ОтборПоСвойствамОбъекта">
		<FilterByObjectProperties>
			<xsl:apply-templates select="@*|node()"/>
		</FilterByObjectProperties>
	</xsl:template>
	<xsl:template match="ПередОбработкой">
		<BeforeProcess>
			<xsl:apply-templates select="@*|node()"/>
		</BeforeProcess>
	</xsl:template>
	<xsl:template match="ПриОбработкеДополнительный">
		<OnProcessAdditional>
			<xsl:apply-templates select="@*|node()"/>
		</OnProcessAdditional>
	</xsl:template>
	<xsl:template match="ПослеОбработки">
		<AfterProcess>
			<xsl:apply-templates select="@*|node()"/>
		</AfterProcess>
	</xsl:template>
	<xsl:template match="ЭлементОтбора">
		<FilterItem>
			<xsl:apply-templates select="@*|node()"/>
		</FilterItem>
	</xsl:template>
	<xsl:template match="СвойствоОбъекта">
		<ObjectProperty>
			<xsl:apply-templates select="@*|node()"/>
		</ObjectProperty>
	</xsl:template>
	<xsl:template match="СвойствоПланаОбмена">
		<ExchangePlanProperty>
			<xsl:apply-templates select="@*|node()"/>
		</ExchangePlanProperty>
	</xsl:template>
	<xsl:template match="ВидСравнения">
		<ComparisonType>
			<xsl:choose>
				<xsl:when test="text()='Равно'">Equal</xsl:when>
				<xsl:when test="text()='НеРавно'">NotEqual</xsl:when>
				<xsl:when test="text()='Больше'">Greater</xsl:when>
				<xsl:when test="text()='GreaterOrEqual'">AND</xsl:when>
				<xsl:when test="text()='Меньше'">Less</xsl:when>
				<xsl:when test="text()='LessOrEqual'">AND</xsl:when>
				<xsl:otherwise>text()</xsl:otherwise>
			</xsl:choose>
		</ComparisonType>
	</xsl:template>
	<xsl:template match="ЭтоСтрокаКонстанты">
		<IsConstantString>
			<xsl:apply-templates select="@*|node()"/>
		</IsConstantString>
	</xsl:template>
	<xsl:template match="ТипСвойстваОбъекта">
		<ObjectPropertyType>
			<xsl:apply-templates select="@*|node()"/>
		</ObjectPropertyType>
	</xsl:template>
	<xsl:template match="ЗначениеКонстанты">
		<ConstantValue>
			<xsl:apply-templates select="@*|node()"/>
		</ConstantValue>
	</xsl:template>
	<xsl:template match="Вид">
		<Kind>
			<xsl:apply-templates select="@*|node()"/>
		</Kind>
	</xsl:template>
	<xsl:template match="БулевоЗначениеГруппы">
		<BooleanGroupValue>
			<xsl:choose>
				<xsl:when test="text()='ИЛИ'">OR</xsl:when>
				<xsl:when test="text()='И'">AND</xsl:when>
				<xsl:otherwise>text()</xsl:otherwise>
			</xsl:choose>
		</BooleanGroupValue>
	</xsl:template>
	<xsl:template match="СоставПланаОбмена">
		<ExchangePlanContent>
			<xsl:apply-templates select="@*|node()"/>
		</ExchangePlanContent>
	</xsl:template>
	<xsl:template match="Элемент">
		<Item>
			<xsl:apply-templates select="@*|node()"/>
		</Item>
	</xsl:template>
	<xsl:template match="Тип">
		<Type>
			<xsl:apply-templates select="@*|node()"/>
		</Type>
	</xsl:template>
	<xsl:template match="Авторегистрация">
		<AutoRegistration>
			<xsl:apply-templates select="@*|node()"/>
		</AutoRegistration>
	</xsl:template>
	<xsl:template match="Код">
		<Code>
			<xsl:apply-templates select="@*|node()"/>
		</Code>
	</xsl:template>
	<xsl:template match="ОбъектМетаданныхТип">
		<MetadataObjectType>
			<xsl:apply-templates select="@*|node()"/>
		</MetadataObjectType>
	</xsl:template>
	<xsl:template match="ТаблицаСвойствПланаОбмена">
		<ExchangePlanPropertiesTable>
			<xsl:apply-templates select="@*|node()"/>
		</ExchangePlanPropertiesTable>
	</xsl:template>
	<xsl:template match="ТаблицаСвойствОбъекта">
		<ObjectPropertiesTable>
			<xsl:apply-templates select="@*|node()"/>
		</ObjectPropertiesTable>
	</xsl:template>
	<xsl:template match="Свойство">
		<Property>
			<xsl:apply-templates select="@*|node()"/>
		</Property>
	</xsl:template>
</xsl:stylesheet>
