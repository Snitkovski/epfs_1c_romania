﻿//
//
//
//
//
//
Function LengthRatio(Val Characteristic) Export
	
	//Query = New Query;
	//Query.SetParameter("Ref", Characteristic);
	//Query.SetParameter("Property", Constants.CharacteristicLength.Get());
	AddAttr = PropertyManager.GetValuesProperties(Characteristic);
	Value = 1;
	For Each Attr in AddAttr Do
		If StrFind(TrimAll(Attr.Property), "Lungime",,,) > 0 Then
			//Value = Number(Attr.Value);
			Value = Attr.Value;
			Break;
		EndIf;
	EndDo;
	Return Value;
	//Query.Text = 
	//"SELECT
	//|	NomenclatureCharacteristicsAdditionalAttributes.Value
	//|FROM
	//|	Catalog.ProductsAndServicesCharacteristics.AdditionalAttributes AS NomenclatureCharacteristicsAdditionalAttributes
	//|WHERE
	//|	NomenclatureCharacteristicsAdditionalAttributes.Ref = &Ref
	//|	AND NomenclatureCharacteristicsAdditionalAttributes.Property = &Property";
	//Select = Query.Execute().Select();
	//If Select.Next() Then
	//	Return Select.Value;
	//Else
	//	Return 1;
	//EndIf;
	
EndFunction

//©# (End) ArNi
