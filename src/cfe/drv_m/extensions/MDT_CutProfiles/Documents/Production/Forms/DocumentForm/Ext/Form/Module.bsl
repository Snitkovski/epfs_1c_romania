﻿#Region FormCommandHandlers

&AtClient
Procedure Cut_CutProfilesBefore(Command)

	ClearMessages();
	
	If Object.Products.Count() = 0 Or Object.Inventory.Count() = 0 Then
		// Nothing to cut
		Return;
	EndIf;
	
	Cut_CutProfilesBeforeAtServer();
	
EndProcedure

#EndRegion

#Region InternalProceduresAndFunctions

&AtServer
Procedure Cut_CutProfilesBeforeAtServer()
	
	If NOT CheckForCommand() Then
		Return;
	EndIf;
	
	FinishedGoodsSourceRow = Object.Products[0];
	MaterialsSourceRow = Object.Inventory[0];
	
	//If FinishedGoodsSourceRow.ProductsAndServices <> MaterialsSourceRow.ProductsAndServices Then
	If FinishedGoodsSourceRow.Products <> MaterialsSourceRow.Products Then
		TextMessage = StrTemplate(
			NStr("ru='Номенклатура %1 не найдена в таблице ""Материалы"".';
				 |en='Product %1 was not found in table ""Materials""';
				 |ro='Produsul %1 nu a fost gasit in ""Materiale""'"),
			//FinishedGoodsSourceRow.ProductsAndServices);
			FinishedGoodsSourceRow.Products);
		CommonClientServer.MessageToUser(TextMessage);
		Return;
	EndIf;
	
	//SourceLength   = SmallBusinessServer.LengthRatio(FinishedGoodsSourceRow.Characteristic);
	SourceLength   = DriveServer.LengthRatio(FinishedGoodsSourceRow.Characteristic);
	//MaterialLength = SmallBusinessServer.LengthRatio(MaterialsSourceRow.Characteristic);
	MaterialLength = DriveServer.LengthRatio(MaterialsSourceRow.Characteristic);
	NewLength      = SourceLength - MaterialLength;
	If NewLength <= 0 Then
		TextMessage = StrTemplate(
			NStr("ru='Не хватает длины профиля. Исходный профиль длиной %1 м. Новый профиль длиной %2 м.';
				 |en='The source profile has %1 m and New profile has %2 m - Not enough length!';
				 |ro='Profilul de baza are %1 m iar profilul nou are %2 m - Lungime incorecta!'"),
			SourceLength,
			MaterialLength);
		CommonClientServer.MessageToUser(TextMessage);
	Else
		// Source material
		MaterialsSourceRow.CostPercentage = (MaterialLength / SourceLength) * 100;
		
		// New material
		NewMaterialRow = Object.Inventory.Add();
		FillPropertyValues(NewMaterialRow, MaterialsSourceRow,,"Characteristic, CostPercentage");
		NewMaterialRow.Characteristic = CharacteristicOfLength(NewLength);
		NewMaterialRow.CostPercentage = (NewLength / SourceLength) * 100;
		NewMaterialRow.Quantity = NewLength;
	EndIf;
	
EndProcedure

&AtServer
Function CheckForCommand()
	
	Check = True;
	// 1. Type of document
	If Object.OperationKind <> Enums.OperationTypesProduction.Disassembly Then
		TextMessage = NStr("ru='Команда доступна только для вида операции ""Разборка"".';
							|en='This command is only available for the type transaction ""Disassembly"".';
							|ro='Aceasta comanda este disponibila doar pentru tipul de tranzactie ""Dezmembrare"".'");
		CommonClientServer.MessageToUser(TextMessage);
		Check = False;
		Return Check;
	EndIf;
	// 2. Count of row FinishedGoods
	If Object.Products.Count() <> 1 Then
		TextMessage = NStr("ru='Команда доступна только если в табличной части ""Продукция"" одна строка.';
							|en='This command is available only if in table ""Finished goods"" has one line';
							|ro='Aceasta comanda este disponibila doar daca tabelul ""Produse finite"" are o singura linie'");
		CommonClientServer.MessageToUser(TextMessage);
		Check = False;
	EndIf;
	// 3. Count of row Inventory
	If Object.Inventory.Count() <> 1 Then
		TextMessage = NStr("ru='Команда доступна только если в табличной части ""Материалы"" одна строка.';
							|en='This command is available only if in table ""Materials"" has one line';
							|ro='Aceasta comanda este disponibila doar daca tabelul ""Materiale"" are o singura linie'");
		CommonClientServer.MessageToUser(TextMessage);
		Check = False;
	EndIf;
	// 4. Quantity in tables
	FinishedGoodsQuantity = Object.Products.Total("Quantity");
	InventoryQuantity = Object.Inventory.Total("Quantity");
	//If FinishedGoodsQuantity <> InventoryQuantity Then
	//	TextMessage = NStr("ru='Не совпадает количество в табличных частях.';
	//						|en='Tables has different quantity of products';
	//						|ro='Tabelele au cantitati diferite!'");
	//	CommonClientServer.MessageToUser(TextMessage);
	//	Check = False;
	//EndIf;
	
	Return Check;
	
EndFunction

&AtServerNoContext
Function CharacteristicOfLength(Length)
	
	Query = New Query;
	Query.SetParameter("Length", Length);
	//Query.SetParameter("Property", Constants.CharacteristicLength.Get());
	SelectionProperty =  ChartsOfCharacteristicTypes.AdditionalAttributesAndInfo.Select();
	While SelectionProperty.Next() Do
		If (StrFind(TrimAll(SelectionProperty.Description), "Lungime",,,) > 0) and (StrFind(TrimAll(SelectionProperty.Description), "Profile",,,) > 0) Then
			Query.SetParameter("Property", SelectionProperty.Ref);
			Break;
		EndIf;
	EndDo;
	//Query.Text = 
	//"SELECT
	//|	ProductsAndServicesCharacteristicsAdditionalAttributes.Ref AS Ref,
	//|	CAST(ProductsAndServicesCharacteristicsAdditionalAttributes.Value AS NUMBER(10,2))
	//|FROM
	//|	Catalog.ProductsAndServicesCharacteristics.AdditionalAttributes AS ProductsAndServicesCharacteristicsAdditionalAttributes
	//|WHERE
	//|	ProductsAndServicesCharacteristicsAdditionalAttributes.Value = &Length
	//|	AND ProductsAndServicesCharacteristicsAdditionalAttributes.Property = &Property";
	Query.Text =
	"SELECT
	|	ProductsCharacteristicsAdditionalAttributes.Ref AS Ref,
	|	CAST(ProductsCharacteristicsAdditionalAttributes.Value AS NUMBER(10,2))
	|FROM
	|	Catalog.ProductsCharacteristics.AdditionalAttributes AS ProductsCharacteristicsAdditionalAttributes
	|WHERE
	|	ProductsCharacteristicsAdditionalAttributes.Value = &Length
	|	AND ProductsCharacteristicsAdditionalAttributes.Property = &Property";
	Select = Query.Execute().Select();
	If Select.Next() Then
		Return Select.Ref;
	Else
		TextMessage = StrTemplate(NStr("ru='Не найдена характеристика с длиной %1.';
										|en='Characteristic with length %1 is not found.';
										|ro='Caracteristica cu lungimea %1 nu a fost gasita.'"), Length);
		CommonClientServer.MessageToUser(TextMessage);
		Return Undefined;
	EndIf;
	
EndFunction

#EndRegion

//&AtClient
//Procedure Cut_CutProfilesBefore(Command)
//	
//	
//EndProcedure

