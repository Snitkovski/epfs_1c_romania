﻿// Transforms an XML string to XDTO contact information object.
//
//  Parameters:
//      Text          - String - XML string. 
//      ExpectedKind  - CatalogRef.ContactInformationKinds, EnumRef.ContactInformationTypes, Structure. 
//      ReadResults   - Structure - target for additional fields:
//                          * ErrorText - String - description of read procedure errors. Value returned by the
//                             function is of valid type but unfilled.
//
&Вместо("ContactInformationDeserialization")
Function CIbugfix_ContactInformationDeserialization(Val Text, Val ExpectedKind = Undefined, ReadResults = Undefined) Export
	
//	ExpectedType = ContactInformationManagement.ContactInformationKindType(ExpectedKind);
	ExpectedType = ContactInformationManagementInternalCached.ContactInformationKindType(ExpectedKind);
	
	EnumAddress      = Enums.ContactInformationTypes.Address;
	EnumEmailAddress = Enums.ContactInformationTypes.EmailAddress;
	EnumWebpage      = Enums.ContactInformationTypes.WebPage;
	EnumPhone        = Enums.ContactInformationTypes.Phone;
	EnumFax          = Enums.ContactInformationTypes.Fax;
	EnumOther        = Enums.ContactInformationTypes.Other;
	EnumSkype        = Enums.ContactInformationTypes.Skype;
	
//	Namespace = ContactInformationClientServerCached.Namespace();
	Namespace = ContactsManagerClientServer.Namespace();
//	If ContactInformationClientServer.IsXMLContactInformation(Text) Then
	If ContactsManagerClientServer.IsXMLContactInformation(Text) Then
		XMLReader = New XMLReader;
		XMLReader.SetString(Text);
		
		ErrorText = Undefined;
		Try
			Result = XDTOFactory.ReadXML(XMLReader, XDTOFactory.Type(Namespace, "ContactInformation"));
		Except //Check old version of format contact information
			Try
				Text = StrReplace(Text,"ssl/contactinfo", "ssl/contactinfoSB");
				XMLReader.SetString(Text);
				Result = XDTOFactory.ReadXML(XMLReader, XDTOFactory.Type(Namespace + "SB", "ContactInformation"));
			Except
				// Invalid XML format
//				WriteLogEvent(ContactInformationManagementService.EventLogMessageText(),
				WriteLogEvent(CIbugfix_EventLogMessageText(),
					EventLogLevel.Error, , Text, DetailErrorDescription(ErrorInfo())
				);
				
				If TypeOf(ExpectedKind) = Type("CatalogRef.ContactInformationKinds") Then
					ErrorText = StrReplace(NStr("en='Incorrect XML format of contact information for ""%1"". Field values were cleared.';ru='Некорректный формат XML контактной информации для ""%1"", значения полей были очищены.';it = 'Formato XML non corretto delle informazioni di contatto per ""%1"", campo di valore sono state cancellate.';lv = 'Nekorekts XML kontaktinformācijas formāts ""%1"", lauku vērtības tika notīrītas.';ro = 'Formatul XML incorect al informațiilor de contact pentru ""%1"". Valorile câmpurilor au fost șterse.';vi = 'Thông tin liên hệ có định dạng XML không chính xác đối với ""%1"", giá trị của trường đã bị xóa.'"),
						"%1", String(ExpectedKind));
				Else
					ErrorText = NStr("en='Incorrect XML format of contact information. Field values were cleared.';ru='Некорректный формат XML контактной информации, значения полей были очищены.';az = 'Əlaqə informasiyasının XML səhv formatı, sahələrin qiymətləri təmizlənmişdi.';it = 'Non corretto contatto formato XML le informazioni, i valori di campo sono state cancellate.';lv = 'Nekorekts XML kontaktinformācijas formāts, lauku vērtības tika notīrītas.';ro = 'Formatul XML incorect al informațiilor de contact. Valorile câmpurilor au fost șterse.';vi = 'Thông tin liên hệ có định dạng XML không chính xác, giá trị các trường đã bị xóa.'");
				EndIf;
			EndTry;
		EndTry;
		
		If ErrorText = Undefined Then
			// Checking for type match
			TypeFound = ?(Result.Content = Undefined, Undefined, Result.Content.Type());
			If ExpectedType = EnumAddress And TypeFound <> XDTOFactory.Type(Namespace, "Address") Then
				ErrorText = NStr("en='An error occurred when deserializing the contact information, address is expected';ru='Ошибка десериализации контактной информации, ожидается адрес';az = 'Əlaqə informasiyasının deserializasiyasi səhvi, ünvan gözlənilir';lv = 'Kontaktinformācijas deserializācijas kļūda, tiek gaidīta adrese';ro = 'A apărut o eroare la dezertilizarea informațiilor de contact, este de așteptat adresa';vi = 'Lỗi cơ cấu hóa thông tin liên hệ, dự tính địa chỉ'");
			ElsIf ExpectedType = EnumEmailAddress And TypeFound <> XDTOFactory.Type(Namespace, "Email") Then
				ErrorText = NStr("en='An error occurred when deserializing the contact information, email address is expected';ru='Ошибка десериализации контактной информации, ожидается адрес электронной почты';az = 'Əlaqə informasiyasının deserializasiyasi səhvi, elektron poçt ünvanı gözlənilir';lv = 'Kontaktinformācijas deserializācijas kļūda, tiek gaidīta e-pasta adrese';ro = 'A apărut o eroare la dezertilizarea informațiilor de contact, este de așteptat adresa de e-mail';vi = 'Lỗi cơ cấu hóa thông tin liên hệ, dự tính địa chỉ email'");
			ElsIf ExpectedType = EnumWebpage And TypeFound <> XDTOFactory.Type(Namespace, "Website") Then
				ErrorText = NStr("en='An error occurred when deserializing the contact information, web page is expected';ru='Ошибка десериализации контактной информации, ожидается веб-страница';az = 'Əlaqə informasiyasının deserializasiyasi səhvi, veb səhifə gözlənilir';it = 'Contattaci errore informazioni deserializzare, in attesa che la pagina web';lv = 'Kontaktinformācijas deserializācijas kļūda, tiek gaidīta web lapa';ro = 'A apărut o eroare la dezertilizarea informațiilor de contact, este de așteptat o pagină web';vi = 'Lỗi cơ cấu hóa thông tin liên hệ, dự tính trang web'");
			ElsIf ExpectedType = EnumPhone And TypeFound <> XDTOFactory.Type(Namespace, "PhoneNumber") Then
				ErrorText = NStr("en='An error occurred when deserializing the contact information, phone number is expected';ru='Ошибка десериализации контактной информации, ожидается телефон';az = 'Əlaqə informasiyasının deserializasiyasi səhvi, telefon gözlənilir';lv = 'Kontaktinformācijas deserializācijas kļūda, tiek gaidīts telefons';ro = 'A apărut o eroare la dezertilizarea informațiilor de contact, numărul de telefon este așteptat';vi = 'Lỗi cơ cấu hóa thông tin liên hệ, dự tính điện thoại'");
			ElsIf ExpectedType = EnumFax And TypeFound <> XDTOFactory.Type(Namespace, "FaxNumber") Then
				ErrorText = NStr("en='An error occurred when deserializing the contact information, phone number is expected';ru='Ошибка десериализации контактной информации, ожидается телефон';az = 'Əlaqə informasiyasının deserializasiyasi səhvi, telefon gözlənilir';lv = 'Kontaktinformācijas deserializācijas kļūda, tiek gaidīts telefons';ro = 'A apărut o eroare la dezertilizarea informațiilor de contact, numărul de telefon este așteptat';vi = 'Lỗi cơ cấu hóa thông tin liên hệ, dự tính điện thoại'");
			ElsIf ExpectedType = EnumOther And TypeFound <> XDTOFactory.Type(Namespace, "Others") Then
				ErrorText = NStr("en='Contact information deserialization error. Other data is expected.';ru='Ошибка десериализации контактной информации, ожидается ""другое""';it = 'Contattaci errore informazioni deserializzare, aspettando ""Un altro""';lv = 'Kontaktinformācijas deserializācijas kļūda, tiek gaidīts ""cits""';ro = 'Informația de contact eroare de deserializare. Se așteaptă alte date.';vi = 'Lỗi cơ cấu hóa thông tin liên hệ, dự tính ""khác""'");
			ElsIf ExpectedType = EnumSkype And TypeFound <> XDTOFactory.Type(Namespace, "Skype") Then
				ErrorText = NStr("ru = 'Ошибка десериализации контактной информации, ожидается ""skype""'; en = 'Contact information deserialization error. Skype is expected.';ro = 'Informația de contact eroare de deserializare. Skype este așteptat.';vi = 'Lỗi sắp xếp lại thông tin liên hệ, đang chờ ""skype"".'");
			EndIf;
		EndIf;
		
		If ErrorText = Undefined Then
			// Reading was successful
			Return Result;
		EndIf;
		
		// Checking for errors, returning detailed information
		If ReadResults = Undefined Then
			Raise ErrorText;
		ElsIf TypeOf(ReadResults) <> Type("Structure") Then
			ReadResults = New Structure;
		EndIf;
		ReadResults.Insert("ErrorText", ErrorText);
		
		// Returning an empty object
		Text = "";
	EndIf;
	
	If TypeOf(Text) = Type("ValueList") Then
		Presentation = "";
		IsNew = Text.Count() = 0;
	Else
		Presentation = String(Text);
		IsNew = IsBlankString(Text);
	EndIf;
	
	Result = XDTOFactory.Create(XDTOFactory.Type(Namespace, "ContactInformation"));
	
	// Parsing
	If ExpectedType = EnumAddress Then
		If IsNew Then
			Result.Content = XDTOFactory.Create(XDTOFactory.Type(Namespace, "Address"));
		Else
			Result = CIbugfix_AddressDeserialization(Text, Presentation, ExpectedType);
		EndIf;
		
	ElsIf ExpectedType = EnumPhone Then
		If IsNew Then
			Result.Content = XDTOFactory.Create(XDTOFactory.Type(Namespace, "PhoneNumber"));
		Else
			Result = PhoneDeserialization(Text, Presentation, ExpectedType)
		EndIf;
		
	ElsIf ExpectedType = EnumFax Then
		If IsNew Then
			Result.Content = XDTOFactory.Create(XDTOFactory.Type(Namespace, "FaxNumber"));
		Else
			Result = FaxDeserialization(Text, Presentation, ExpectedType)
		EndIf;
		
	ElsIf ExpectedType = EnumEmailAddress Then
		If IsNew Then
			Result.Content = XDTOFactory.Create(XDTOFactory.Type(Namespace, "Email"));
		Else
			Result = OtherContactInformationDeserialization(Text, Presentation, ExpectedType)
		EndIf;
		
	ElsIf ExpectedType = EnumWebpage Then
		If IsNew Then
			Result.Content = XDTOFactory.Create(XDTOFactory.Type(Namespace, "Website"));
		Else
			Result = OtherContactInformationDeserialization(Text, Presentation, ExpectedType)
		EndIf;
		
	ElsIf ExpectedType = EnumOther Then
		If IsNew Then
			Result.Content = XDTOFactory.Create(XDTOFactory.Type(Namespace, "Others"));
		Else
			Result = OtherContactInformationDeserialization(Text, Presentation, ExpectedType)
		EndIf;
		
	ElsIf ExpectedType = EnumSkype Then
		If IsNew Then
			Result.Content = XDTOFactory.Create(XDTOFactory.Type(Namespace, "Skype"));
		Else
			Result = OtherContactInformationDeserialization(Text, Presentation, ExpectedType)
		EndIf;
		
	Else
		Raise NStr("en='An error occurred while deserializing contact information, the expected type is not specified';ru='Ошибка десериализации контактной информации, не указан ожидаемый тип';az = 'Əlaqə informasiyasının deserializasiyasi səhvi, gözlənən tip göstərilməmişdir';lv = 'Kontaktinformācijas deserializācijas kļūda, nav norādīts gaidāms tips';ro = 'A apărut o eroare în timpul dezerlizării informațiilor de contact, tipul de așteptat nu este specificat';vi = 'Lỗi trình tự của thông tin liên hệ, chưa chỉ ra kiểu mong muốn'");
	EndIf;
	
	Return Result;
EndFunction

// Returns an event name for recording to the contact information event log.
//
Function CIbugfix_EventLogMessageText() Export
	Return NStr("en = 'Contact information'; ru = 'Контактная информация';tr = 'İletişim bilgileri';ro = 'Informații de contact';pl = 'Dane kontaktowe';de = 'Kontaktinformationen';es_ES = 'Información de contacto'",
//		CommonUseClientServer.MainLanguageCode());
		CommonClientServer.DefaultLanguageCode());
EndFunction

// Internal, for serialization purposes.
Function CIbugfix_AddressDeserialization(Val FieldsValues, Val Presentation, Val ExpectedType = Undefined)
	
	If ContactsManagerClientServer.IsXMLContactInformation(FieldsValues) Then
		// Common format of contact information.
		Return ContactsManagerInternal.ContactsFromXML(FieldsValues, ExpectedType);
	EndIf;
	
	If ExpectedType <> Undefined Then
		If ExpectedType <> Enums.ContactInformationTypes.Address Then
			Raise NStr("en='Contact information deserialization error. Address is expected.';ro='Eroare deserializată a informației de contact. Adresa este așteptată.';ru='Ошибка десериализации контактной информации, ожидается адрес'");
		EndIf;
	EndIf;
	
	// Old format with line separator and equality.
	Namespace = ContactsManagerClientServer.Namespace();
	
	Result = XDTOFactory.Create(XDTOFactory.Type(Namespace, "ContactInformation"));
	
	Result.Comment = "";
	Result.Content      = XDTOFactory.Create(XDTOFactory.Type(Namespace, "Address"));
	
	MainCountryName  = Upper(AddressManagerClientServer.MainCountry().Description);
	ApartmentItem = Undefined;
	BuildingUnitItem   = Undefined;
	HouseItem      = Undefined;
	
	// National
	NationalAddress = XDTOFactory.Create(XDTOFactory.Type(AddressManager.Namespace(), "AddressUS"));
	
	// Common composition
	Address = Result.Content;
	Address.Country = MainCountryName; // Default country
	AddressDomestic = True;
	
	FieldValueType = TypeOf(FieldsValues);
	If FieldValueType = Type("ValueList") Then
		FieldsList = FieldsValues;
	ElsIf FieldValueType = Type("Structure") Then
		FieldsList = ContactsManagerClientServer.ConvertStringToFieldList(
			AddressManagerClientServer.FieldsString(FieldsValues, False));
	Else
		// Already transformed to a string
		FieldsList = ContactsManagerClientServer.ConvertStringToFieldList(FieldsValues);
	EndIf;
	
	ApartmentTypeUndefined = True;
	UnitTypeUndefined  = True;
	HouseTypeUndefined     = True;
	PresentationField      = "";
	
	For Each ListItem In FieldsList Do
		FieldName = Upper(ListItem.Presentation);
		
		If FieldName="INDEXOF" Then
			ZipCodeItem = GenerateAdditionalAddressItem(NationalAddress);
			ZipCodeItem.AddressItemType = AddressManagerClientServer.ZipCodeSerializationCode();
			ZipCodeItem.Value = ListItem.Value;
			
		ElsIf FieldName = "COUNTRY" Then
			Address.Country = ListItem.Value;
			If Upper(ListItem.Value) <> MainCountryName Then
				AddressDomestic = False;
			EndIf;
			
		ElsIf FieldName = "COUNTRYCODE" Then
			// No action required
			
		ElsIf FieldName = "STATECODE" Then
			NationalAddress.USTerritorialEntitiy = CodeState(ListItem.Value);
			
		ElsIf FieldName = "STATE" Then
			NationalAddress.USTerritorialEntitiy = ListItem.Value;
			
		ElsIf FieldName = "DISTRICT" Then
			If NationalAddress.MunicipalEntityDistrictProperty = Undefined Then
				NationalAddress.MunicipalEntityDistrictProperty = XDTOFactory.Create( NationalAddress.Type().Properties.Get("MunicipalEntityDistrictProperty").Type )
			EndIf;
			NationalAddress.MunicipalEntityDistrictProperty.District = ListItem.Value;
			
		ElsIf FieldName = "CITY" Then
			NationalAddress.City = ListItem.Value;
			
		ElsIf FieldName = "LOCALITY" Then
			NationalAddress.Settlmnt = ListItem.Value;
			
		ElsIf FieldName = "STREET" Then
			NationalAddress.Street = ListItem.Value;
			
		ElsIf FieldName = "HOUSETYPE" Then
			If HouseItem = Undefined Then
				HouseItem = GenerateNumberOfAdditionalAddressItem(NationalAddress);
			EndIf;
			HouseItem.Type = AddressManagerClientServer.AddressingObjectSerializationCode(ListItem.Value);
			HouseTypeUndefined = False;
			
		ElsIf FieldName = "HOUSE" Then
			If HouseItem = Undefined Then
				HouseItem = GenerateNumberOfAdditionalAddressItem(NationalAddress);
			EndIf;
			HouseItem.Value = ListItem.Value;
			
		ElsIf FieldName = "BUILDINGUNITTYPE" Then
			If BuildingUnitItem = Undefined Then
				BuildingUnitItem = GenerateNumberOfAdditionalAddressItem(NationalAddress);
			EndIf;
			BuildingUnitItem.Type = AddressManagerClientServer.AddressingObjectSerializationCode(ListItem.Value);
			UnitTypeUndefined = False;
			
		ElsIf FieldName = "BUILDINGUNIT" Then
			If BuildingUnitItem = Undefined Then
				BuildingUnitItem = GenerateNumberOfAdditionalAddressItem(NationalAddress);
			EndIf;
			BuildingUnitItem.Value = ListItem.Value;
			
		ElsIf FieldName = "APARTMENTTYPE" Then
			If ApartmentItem = Undefined Then
				ApartmentItem = GenerateNumberOfAdditionalAddressItem(NationalAddress);
			EndIf;
			ApartmentItem.Type = AddressManagerClientServer.AddressingObjectSerializationCode(ListItem.Value);
			ApartmentTypeUndefined = False;
			
		ElsIf FieldName = "APARTMENT" Then
			If ApartmentItem = Undefined Then
				ApartmentItem = GenerateNumberOfAdditionalAddressItem(NationalAddress);
			EndIf;
			ApartmentItem.Value = ListItem.Value;
			
		ElsIf FieldName = "PRESENTATION" Then
			PresentationField = TrimAll(ListItem.Value);
			
		EndIf;
		
	EndDo;
	
	// Default preferences
	If HouseTypeUndefined AND HouseItem <> Undefined Then
		HouseItem.Type = AddressManagerClientServer.AddressingObjectSerializationCode("House");
	EndIf;
	
	If UnitTypeUndefined AND BuildingUnitItem <> Undefined Then
		BuildingUnitItem.Type = AddressManagerClientServer.AddressingObjectSerializationCode("BuildingUnit");
	EndIf;
	
	If ApartmentTypeUndefined AND ApartmentItem <> Undefined Then
		ApartmentItem.Type = AddressManagerClientServer.AddressingObjectSerializationCode("Apartment");
	EndIf;
	
	// Presentation with priorities.
	If Not IsBlankString(Presentation) Then
		Result.Presentation = Presentation;
	Else
		Result.Presentation = PresentationField;
	EndIf;
	
	Address.Content = ?(AddressDomestic, NationalAddress, Result.Presentation);
	
	Return Result;
EndFunction
