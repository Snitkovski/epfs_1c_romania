﻿&AtServer
Procedure UserSettingsToDataParameters()
	
	//If Find(info, "Denumire platitor:") > 0 Then
	//		infoNodeTABLE = nodeTABLE;
	//		Break;
	//	EndIf;
	
	If Find(Report.SettingsComposer.Settings.OutputParameters.Items[11].Value, "ABC/XYZ") > 0 Then
		//If Report.SettingsComposer.Settings.OutputParameters.Items[11].Value = 
		//Analiza ABC/XYZ a  pe nomenclator
		DCUserSettings = Report.SettingsComposer.UserSettings;
		For Each DCUserSetting In DCUserSettings.Items Do
			Type = ReportsClientServer.SettingTypeAsString(TypeOf(DCUserSetting));
			
			If Type = "SettingsParameterValue"
				AND TypeOf(DCUserSetting.Value) = Type("StandardPeriod")
				AND DCUserSetting.Use Then
				
				Value = DCUserSetting.Value;
				PeriodBeginning = Value.StartDate;
				PeriodEnd = Value.EndDate;
				
			ElsIf Type = "SettingsParameterValue"
				//AND String(DCUserSetting.Value) = "Products"
				AND String(DCUserSetting.Parameter) = "AnalysisObject"
				AND DCUserSetting.Use Then
				
				AnalysisObjectValue = DCUserSettings.Items[16].RightValue;
				
			ElsIf Type = "SettingsParameterValue"
				//AND String(DCUserSetting.Value) = "AnalysisParameter"  
				AND String(DCUserSetting.Parameter) = "AnalysisParameter"
				AND DCUserSetting.Use Then
				
				AnalysisParameterValue = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterXClassStart"
				AND DCUserSetting.Use Then
				
				ParameterXClassStart = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterXClassEnd"
				AND DCUserSetting.Use Then
				
				ParameterXClassEnd = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterYClassStart"
				AND DCUserSetting.Use Then
				
				ParameterYClassStart = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterYClassEnd"
				AND DCUserSetting.Use Then
				
				ParameterYClassEnd = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterZClassStart"
				AND DCUserSetting.Use Then
				
				ParameterZClassStart = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterZClassEnd"
				AND DCUserSetting.Use Then
				
				ParameterZClassEnd = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "PercentageAClass"
				AND DCUserSetting.Use Then
				
				PercentageAClass = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "PercentageBClass"
				AND DCUserSetting.Use Then
				
				PercentageBClass = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "PercentageCClass"
				AND DCUserSetting.Use Then
				
				PercentageCClass = DCUserSetting.Value;
				
			ElsIf Type = "FilterItem"
				
				AND TypeOf(DCUserSettings.Items[17].RightValue) = Type("CatalogRef.Employees")
				AND DCUserSetting.Use Then
				
				ResponsibleFilterValue = DCUserSettings.Items[17].RightValue;
				
				//ElsIf Type = "FilterItem"
				//	
				//	AND TypeOf(DCUserSettings.Items[15].RightValue) = Type("CatalogRef.Companies")
				//	AND DCUserSetting.Use Then  						
				//	
				//	CompanyFilterValue = DCUserSettings.Items[15].RightValue;	
				//	
				
			EndIf;
			
		EndDo;
		
		Report.SettingsComposer.Settings.AdditionalProperties.Insert("BeginOfPeriod", PeriodBeginning);
		Report.SettingsComposer.Settings.AdditionalProperties.Insert("EndOfPeriod", PeriodEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("BeginOfPeriod", PeriodBeginning);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("EndOfPeriod", PeriodEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ItmPeriod", Value);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("AnalysisParameter", AnalysisParameterValue);
		
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterXClassStart", ParameterXClassStart);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterXClassEnd", ParameterXClassEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterYClassStart", ParameterYClassStart);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterYClassEnd", ParameterYClassEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterZClassStart", ParameterZClassStart);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterZClassEnd", ParameterZClassEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("PercentageAClass", PercentageAClass);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("PercentageBClass", PercentageBClass);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("PercentageCClass", PercentageCClass);

		//Report.SettingsComposer.Settings.Filter.Items[0].RightValue = CompanyFilterValue; 
		//Report.SettingsComposer.Settings.Filter.Items[0].ComparisonType = DataCompositionComparisonType.InListByHierarchy;   
		//Report.SettingsComposer.Settings.Filter.Items[0].Use = True;
		
		Report.SettingsComposer.Settings.Filter.Items[1].RightValue = AnalysisObjectValue;
		Report.SettingsComposer.Settings.Filter.Items[1].ComparisonType = DataCompositionComparisonType.InListByHierarchy;
		Report.SettingsComposer.Settings.Filter.Items[1].Use = True;  //Элементы.Список.Обновить();
		
		Report.SettingsComposer.Settings.Filter.Items[2].RightValue = ResponsibleFilterValue;
		Report.SettingsComposer.Settings.Filter.Items[2].ComparisonType = DataCompositionComparisonType.InListByHierarchy;
		Report.SettingsComposer.Settings.Filter.Items[2].Use = True;
		
	EndIf;
		
EndProcedure

&AtClient
Procedure Report_ABCXYZ_ReportComposeResultBefore(Command)
	
	UserSettingsToDataParameters();
	
EndProcedure

//&AtClient
//Procedure ac_ReportComposeResultBefore(Command)
//	
//	 UserSettingsToDataParameters();
//	
//EndProcedure
