﻿
//&AtServer
//Procedure executeAtServer()
//	
//	if Not Object.Company=Catalogs.Companies.EmptyRef() Then
//		
//		Query = New Query;
//		Query.Text = 
		
	//	    "SELECT
	//	    |	EmployeeTaskWorks.Ref AS Ref,
	//	    |	EmployeeTaskWorks.Day AS Day,
	//	    |	EmployeeTaskWorks.Ref.Employee AS Employee,
	//	    |	EmployeeTaskWorks.Ref.State AS State,
	//	    |	EmployeeTaskWorks.DurationInHours AS DurationInHours,
	//	    |	EmployeeTaskWorks.Price AS Price,
	//	    |	EmployeeTaskWorks.Amount AS Amount,
	//	    |	EmployeeTaskWorks.Comment AS Comment,
	//	    |	EmployeeTaskWorks.Customer AS Customer,
	//	    |	EmployeeTaskWorks.Customer.ContractByDefault AS CustomerContractByDefault,
	//	    |	EmployeeTaskWorks.Ref.Company AS Company
	//	    |INTO TT
	//	    |FROM
	//	    |	Document.EmployeeTask.Works AS EmployeeTaskWorks
	//	    |WHERE
	//	    |	EmployeeTaskWorks.Day BETWEEN &DateBegin AND &DateEnd
	//	    |;
	//	    |
	//	    |////////////////////////////////////////////////////////////////////////////////
	//	    |SELECT
	//	    |	TT.Ref AS Ref,
	//	    |	TT.Day AS Day,
	//	    |	TT.State AS State,
	//	    |	TT.Price AS Price,
	//	    |	TT.Amount AS Amount,
	//	    |	TT.Comment AS Comment,
	//	    |	TT.DurationInHours AS DurationInHours,
	//	    |	TT.CustomerContractByDefault AS CustomerContractByDefault,
	//	    |	TT.Customer AS Customer,
	//	    |	TT.Employee AS Employee,
	//	    |	TT.Company AS Company,
	//	    |	HoursWorkedBalance.HoursBalance AS HoursBalance,
	//	    |	HoursWorkedBalance.Company AS Company1,
	//	    |	HoursWorkedBalance.Counterparty AS Counterparty,
	//	    |	HoursWorkedBalance.Contract AS Contract,
	//	    |	HoursWorkedBalance.EmployeeTask AS EmployeeTask
	//	    |FROM
	//	    |	AccumulationRegister.HoursWorked.Balance(&DateEnd, ) AS HoursWorkedBalance
	//	    |		INNER JOIN TT AS TT
	//	    |		ON HoursWorkedBalance.EmployeeTask.Ref = TT.Ref
	//	    |			AND HoursWorkedBalance.Counterparty = TT.Customer
	//	    |			AND HoursWorkedBalance.Contract = TT.CustomerContractByDefault
	//	    |			AND HoursWorkedBalance.Company = TT.Company
	//	    |WHERE
	//	    |	HoursWorkedBalance.Company = &Company
	//	    |TOTALS BY
	//	    |	Customer";			
	//	Query.SetParameter("DateBegin", Object.DateBegin);
	//	//Query.SetParameter("DateEnd", EndOfDay(Object.DateEnd)-1);// сделать концом дня?
	//	Query.SetParameter("DateEnd", EndOfDay(Object.DateEnd));
	//	Query.SetParameter("Company", Object.Company); 		
	//	QueryResult = Query.Execute();
	//	
	//	SelectionDetailRecords = QueryResult.Select(QueryResultIteration.ByGroups);
	//			
	//	While SelectionDetailRecords.Next() Do
	//		
	//		Selection=SelectionDetailRecords.Select();	
	//		
	//		NewDoc=Documents.WorkComplete.CreateDocument();
	//		                                                 			
	//		NewDoc.Company=Object.Company;
	//		NEwdoc.Counterparty=SelectionDetailRecords.Customer;
	//		IF SelectionDetailRecords.Customer.ContractByDefault<> Null Then
	//			NewDoc.Contract=SelectionDetailRecords.Customer.ContractByDefault;
	//			NewDoc.Startperiod=Object.DateBegin;
	//			NewDoc.EndPeriod=Object.DateEnd;
	//			
	//		EndIf;
	//		While Selection.Next() Do
	//			
	//			Day=Selection.Day;
	//			//BeginOfPeriod = BegOfMonth(Date(Object.Date) -1);
	//			NewDoc.Date=EndOfMonth(Day)-1;
	//			//NewDoc.SummToBePaid=NewDoc.Summ;		
	//			NewDoc.Rate = Selection.price;
	//			
	//			NewLine=NewDoc.Tasks.Add();
	//			NewLine.Employee=Selection.Employee;  
	//			NewLine.Task=Selection.Ref;
	//			NewLine.TaskType=Selection.Ref.State;
	//			NewLine.Rate=Selection.price; 				
	//			NewLine.Date=Selection.Day;				
	//			NewLine.Quantity=Selection.DurationInHours;
	//			NewLine.Summ=Selection.Amount;
	//			NewLine.Comments=Selection.Comment;
	//		EndDo;
	//		NewDoc.Write();
	//		
	//	EndDo; 
	//	Mes=NStr("ru = 'Было создано '; en = '';ro = 'Au fost create '")+SelectionDetailRecords.Count()+NStr("ru = ' документа '; en = ' documents was created';ro = ' documente'"); 
	//	Message(Mes);
	//Else 
	//	Message(NStr("ru= 'Заполните поле Организация'; en='Complete field Company'; ro='Completați câmpul Compania'"));
	//EndIf;
	
	&AtServer
Procedure executeAtServer()
	
	if Not Object.Company=Catalogs.Companies.EmptyRef() Then
		
		Query = New Query;
		Query.Text =
		
		    "SELECT
		    |	EmployeeTaskWorks.Ref AS Ref,
		    |	EmployeeTaskWorks.Day AS Day,
		    |	EmployeeTaskWorks.Ref.Employee AS Employee,
		    |	EmployeeTaskWorks.Ref.State AS State,
		    |	ISNULL(EmployeeTaskWorks.DurationInHours, 0) AS DurationInHours,
		    |	ISNULL(EmployeeTaskWorks.Price, 0) AS Price,
		    |	ISNULL(EmployeeTaskWorks.Amount, 0) AS Amount,
		    |	EmployeeTaskWorks.Comment AS Comment,
		    |	EmployeeTaskWorks.Customer AS Customer,
		    |	EmployeeTaskWorks.Ref.Company AS Company,
		    |	EmployeeTaskWorks.Contract AS Contract
		    |INTO TT
		    |FROM
		    |	Document.EmployeeTask.Works AS EmployeeTaskWorks
		    |WHERE
		    |	EmployeeTaskWorks.Day BETWEEN &DateBegin AND &DateEnd
		    |;
		    |
		    |////////////////////////////////////////////////////////////////////////////////
		    |SELECT
		    |	TT.Ref AS Ref,
		    |	TT.Day AS Day,
		    |	TT.State AS State,
		    |	ISNULL(TT.Price, 0) AS Price,
		    |	ISNULL(TT.Amount, 0) AS Amount,
		    |	TT.Comment AS Comment,
		    |	ISNULL(TT.DurationInHours, 0) AS DurationInHours,
		    |	TT.Customer AS Customer,
		    |	TT.Employee AS Employee,
		    |	TT.Company AS Company,
		    |	ISNULL(HoursWorkedBalance.HoursBalance, 0) AS HoursBalance,
		    |	HoursWorkedBalance.Company AS Company1,
		    |	HoursWorkedBalance.Counterparty AS Counterparty,
		    |	HoursWorkedBalance.Contract AS Contract,
		    |	HoursWorkedBalance.EmployeeTask AS EmployeeTask,
		    |	TT.Contract AS Contract1
		    |FROM
		    |	AccumulationRegister.HoursWorked.Balance(&DateEnd, ) AS HoursWorkedBalance
		    |		INNER JOIN TT AS TT
		    |		ON HoursWorkedBalance.EmployeeTask.Ref = TT.Ref
		    |			AND HoursWorkedBalance.Counterparty = TT.Customer
		    |			AND HoursWorkedBalance.Company = TT.Company
		    |WHERE
		    |	HoursWorkedBalance.Company = &Company
		    |
		    |ORDER BY
		    |	Day
		    |TOTALS BY
		    |	Customer,
		    |	Contract1";
		
		Query.SetParameter("DateBegin", Object.DateBegin);
		//Query.SetParameter("DateEnd", EndOfDay(Object.DateEnd)-1);// сделать концом дня?
		Query.SetParameter("DateEnd", EndOfDay(Object.DateEnd));
		Query.SetParameter("Company", Object.Company);
		QueryResult = Query.Execute();
		
		//SelectionDetailRecords = QueryResult.Select(QueryResultIteration.ByGroups);
		SelectionDetailRecordsByCustomer = QueryResult.Select(QueryResultIteration.ByGroups,"Customer");
		
		NumberOfDocuments = 0;
		
		While SelectionDetailRecordsByCustomer.Next() Do
			
			SelectionDetailRecords = SelectionDetailRecordsByCustomer.Select(QueryResultIteration.ByGroups,"Contract1");

			While SelectionDetailRecords.Next() Do
				
				Selection=SelectionDetailRecords.Select();
				
				NewDoc=Documents.WorkComplete.CreateDocument();
				
				NewDoc.Company=Object.Company;
				NEwdoc.Counterparty=SelectionDetailRecords.Customer;
				IF SelectionDetailRecords.Contract1 <> Null Then
					NewDoc.Contract=SelectionDetailRecords.Contract1;
					NewDoc.Startperiod=Object.DateBegin;
					NewDoc.EndPeriod=Object.DateEnd;
					
				EndIf;
				While Selection.Next() Do
					
					Day=Selection.Day;
					//BeginOfPeriod = BegOfMonth(Date(Object.Date) -1);
					NewDoc.Date=EndOfMonth(Day)-1;
					//NewDoc.SummToBePaid=NewDoc.Summ;		
					NewDoc.Rate = Selection.price;
					
					NewLine=NewDoc.Tasks.Add();
					NewLine.Employee=Selection.Employee;
					NewLine.Task=Selection.Ref;
					NewLine.TaskType=Selection.Ref.State;
					NewLine.Rate=Selection.price;
					NewLine.Date=Selection.Day;
					NewLine.Quantity=Selection.DurationInHours;
					NewLine.Summ=Selection.Amount;
					NewLine.Comments=Selection.Comment;
				EndDo;
				
				NewDoc.Tasks.Sort("Date Asc");
				
				NewDoc.Write();
				
			EndDo;
			
			NumberOfDocuments = NumberOfDocuments + SelectionDetailRecords.Count();
		EndDo;
		
			NumberOfCounterparties = SelectionDetailRecordsByCustomer.Count();
		
		Mes=NStr("ru = 'Было создано '; en = '';ro = 'Au fost create '")+NumberOfDocuments +NStr("ru = ' документов '; en = ' documents was created';ro = ' documente'");
		Message(Mes);
	Else
		Message(NStr("ru= 'Заполните поле Организация'; en='Complete field Company'; ro='Completați câmpul Compania'"));
	EndIf;

EndProcedure

&AtClient
Procedure executa(Command)
	
	executeAtServer();
EndProcedure
