﻿
&AtServer
&Around("RepresentMyAgenda")
Procedure ac_RepresentMyAgenda()
	//// Вставить содержимое метода.
	//ProceedWithCall(); 	
	
	MyCurrentDayTasks.Clear();
	
	Query = New Query();
	
	Query.Text =
	"SELECT ALLOWED
	|	UserEmployees.Employee
	|INTO UserEmployees
	|FROM
	|	InformationRegister.UserEmployees AS UserEmployees
	|WHERE
	|	UserEmployees.User = &User
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	NestedSelect.Order AS Order,
	|	NestedSelect.Number AS Number,
	|	NestedSelect.LineNumber AS LineNumber,
	|	NestedSelect.Ref AS Ref,
	|	NestedSelect.CounterpartyPresentation AS CounterpartyPresentation,
	|	NestedSelect.Subject AS Subject,
	|	NestedSelect.Content AS Content,
	|	NestedSelect.Responsible AS Responsible,
	|	NestedSelect.ProductsPresentation AS ProductsPresentation,
	|	NestedSelect.Comment AS Comment,
	|	NestedSelect.CommentOfDocument AS CommentOfDocument,
	|	NestedSelect.CustomerPresentation AS CustomerPresentation,
	|	NestedSelect.Day AS Day,
	|	NestedSelect.BeginTime AS BeginTime,
	|	NestedSelect.EndTime AS EndTime,
	|	NestedSelect.Closed AS Closed
	|FROM
	|	(SELECT
	|		1 AS Order,
	|		Event.Number AS Number,
	|		1 AS LineNumber,
	|		Event.Ref AS Ref,
	|		CASE
	|			WHEN VALUETYPE(EventParticipants.Contact) <> TYPE(Catalog.Counterparties)
	|					OR EventParticipants.Contact = VALUE(Catalog.Counterparties.EmptyRef)
	|				THEN ""<Not specified>""
	|			ELSE PRESENTATION(EventParticipants.Contact)
	|		END AS CounterpartyPresentation,
	|		CASE
	|			WHEN Event.Subject = """"
	|				THEN ""<The subject is not specified>""
	|			ELSE Event.Subject
	|		END AS Subject,
	|		CASE
	|			WHEN (CAST(Event.Content AS STRING(255))) = """"
	|				THEN ""<Content is not specified>""
	|			ELSE CAST(Event.Content AS STRING(255))
	|		END AS Content,
	|		Event.Responsible AS Responsible,
	|		UNDEFINED AS ProductsPresentation,
	|		UNDEFINED AS Comment,
	|		UNDEFINED AS CommentOfDocument,
	|		UNDEFINED AS CustomerPresentation,
	|		UNDEFINED AS Day,
	|		Event.EventBegin AS BeginTime,
	|		Event.EventEnding AS EndTime,
	|		UNDEFINED AS Closed
	|	FROM
	|		Document.Event AS Event
	|			LEFT JOIN Document.Event.Participants AS EventParticipants
	|			ON Event.Ref = EventParticipants.Ref
	|				AND (EventParticipants.LineNumber = 1)
	|	WHERE
	|		Event.Responsible IN
	|				(SELECT
	|					UserEmployees.Employee
	|				FROM
	|					UserEmployees)
	|		AND Event.DeletionMark = FALSE
	|		AND Event.EventBegin >= &BeginTime
	|		AND Event.EventBegin <= &EndTime
	|		AND (&State = VALUE(Catalog.JobAndEventStatuses.EmptyRef)
	|				OR Event.State = &State)
	|		AND (&EventType = VALUE(Enum.EventTypes.EmptyRef)
	|				OR Event.EventType = &EventType)
	|		AND (&Counterparty = VALUE(Catalog.Counterparties.EmptyRef)
	|				OR EventParticipants.Contact = &Counterparty)
	|
	|	UNION ALL
	|
	|	SELECT
	|		2,
	|		WorkOrderWorks.Ref.Number,
	|		WorkOrderWorks.LineNumber,
	|		WorkOrderWorks.Ref,
	|		UNDEFINED,
	|		UNDEFINED,
	|		UNDEFINED,
	|		WorkOrderWorks.Ref.Employee,
	|		PRESENTATION(WorkOrderWorks.Products),
	|		CAST(WorkOrderWorks.Comment AS STRING(255)),
	|		CAST(WorkOrderWorks.Ref.Comment AS STRING(255)),
	|		PRESENTATION(WorkOrderWorks.Customer),
	|		WorkOrderWorks.Day,
	|		DATEADD(WorkOrderWorks.Day, SECOND, HOUR(WorkOrderWorks.BeginTime) * 3600 + MINUTE(WorkOrderWorks.BeginTime) * 60 + SECOND(WorkOrderWorks.BeginTime)),
	|		DATEADD(WorkOrderWorks.Day, SECOND, HOUR(WorkOrderWorks.EndTime) * 3600 + MINUTE(WorkOrderWorks.EndTime) * 60 + SECOND(WorkOrderWorks.EndTime)),
	|		CASE
	|			WHEN WorkOrderWorks.Ref.State = VALUE(Catalog.JobAndEventStatuses.Completed) OR
	|				 WorkOrderWorks.Ref.State.Description = ""Completa-Ne taxata"" OR
	|				 WorkOrderWorks.Ref.State.Description = ""Completa-Gratuita""
	|	THEN TRUE
	|			ELSE FALSE
	|		END
	|	FROM
	|		Document.EmployeeTask.Works AS WorkOrderWorks
	|	WHERE
	|		WorkOrderWorks.Ref.Posted = TRUE
	|		AND WorkOrderWorks.Ref.Employee IN
	|				(SELECT
	|					UserEmployees.Employee
	|				FROM
	|					UserEmployees)
	|		AND DATEADD(WorkOrderWorks.Day, SECOND, HOUR(WorkOrderWorks.BeginTime) * 3600 + MINUTE(WorkOrderWorks.BeginTime) * 60 + SECOND(WorkOrderWorks.BeginTime)) >= &BeginTime
	|		AND DATEADD(WorkOrderWorks.Day, SECOND, HOUR(WorkOrderWorks.EndTime) * 3600 + MINUTE(WorkOrderWorks.EndTime) * 60 + SECOND(WorkOrderWorks.EndTime)) <= &EndTime
	|		AND (&Counterparty = VALUE(Catalog.Counterparties.EmptyRef)
	|				OR WorkOrderWorks.Customer = &Counterparty)
	|		AND (&EventType = VALUE(Enum.EventTypes.EmptyRef)
	|				OR &EventType = ""WorkOrder"")
	|		AND (&State = VALUE(Catalog.JobAndEventStatuses.EmptyRef)
	|				OR WorkOrderWorks.Ref.State = &State)) AS NestedSelect
	|
	|ORDER BY
	|	Order,
	|	Day,
	|	BeginTime,
	|	EndTime,
	|	Number,
	|	LineNumber
	|TOTALS BY
	|	BeginTime,
	|	Order";

	Query.SetParameter("User", Users.CurrentUser());
	Query.SetParameter("DateOfSchedule", DateOfSchedule);
	Query.SetParameter("Counterparty", FilterCounterparty);
	Query.SetParameter("EventType", StringIntoEnumeration(FilterEventType));
	Query.SetParameter("State", FilterState);
	
	If RadioButton = "Day" Then
		Query.SetParameter("BeginTime", BegOfDay(DateOfSchedule));
		Query.SetParameter("EndTime", EndOfDay(DateOfSchedule));
	ElsIf RadioButton = "Week" Then
		Query.SetParameter("BeginTime", BegOfWeek(DateOfSchedule));
		Query.SetParameter("EndTime", EndOfWeek(DateOfSchedule));
	Else
		Query.SetParameter("BeginTime", BegOfMonth(DateOfSchedule));
		Query.SetParameter("EndTime", EndOfMonth(DateOfSchedule));
	EndIf;
	
	QueryResultt = Query.Execute();
	
	TemplateMyCurrentDayTasks = DataProcessors.EmployeeCalendar.GetTemplate("MyCurrentDayTasks");
	
	If QueryResultt.IsEmpty() Then
		TemplateArea = TemplateMyCurrentDayTasks.GetArea("RowEmptyPeriod");
		MyCurrentDayTasks.Put(TemplateArea);
		Return;
	EndIf;
	
	SelectionByDate = QueryResultt.Select(QueryResultIteration.ByGroups);
	DateCount = SelectionByDate.Count();
	
	While SelectionByDate.Next() Do
		
		If DateCount > 1 Then
			TemplateArea = TemplateMyCurrentDayTasks.GetArea("GroupDate");
			TemplateArea.Parameters.Date = String(Format(SelectionByDate.BeginTime, "DF=dd:MM:yyyy"));
			MyCurrentDayTasks.Put(TemplateArea);
			MyCurrentDayTasks.StartRowGroup();
		EndIf;
		
		SelectionByOrder = SelectionByDate.Select(QueryResultIteration.ByGroups);
		
		While SelectionByOrder.Next() Do
			
			If SelectionByOrder.Order = 1 Then
				
				SelectionOfQueryResult = SelectionByOrder.Select();
				
				TemplateArea = TemplateMyCurrentDayTasks.GetArea("GroupEvents");
				TemplateArea.Parameters.EventsQuantity = String(SelectionOfQueryResult.Count());
				MyCurrentDayTasks.Put(TemplateArea);
				
				MyCurrentDayTasks.StartRowGroup();
				
				While SelectionOfQueryResult.Next() Do
				
					TemplateArea = TemplateMyCurrentDayTasks.GetArea("RowEvent");
					TemplateArea.Parameters.Subject = SelectionOfQueryResult.Subject;
					
					If BegOfDay(SelectionByDate.BeginTime) = BegOfDay(SelectionOfQueryResult.BeginTime)
					   AND BegOfDay(SelectionByDate.BeginTime) = BegOfDay(SelectionOfQueryResult.EndTime) Then
						TemplateArea.Parameters.Period = "" + Format(SelectionOfQueryResult.BeginTime, "DF=HH:mm") + " - " + Format(SelectionOfQueryResult.EndTime, "DF=HH:mm");
					Else
						TemplateArea.Parameters.Period = "" + Format(SelectionOfQueryResult.BeginTime, "DF=HH:mm") + " " + Format(SelectionOfQueryResult.BeginTime, "DF=dd:MM:yyyy") + " - " + Format(SelectionOfQueryResult.EndTime, "DF=HH:mm") + " " + Format(SelectionOfQueryResult.EndTime, "DF=dd:MM:yyyy");
					EndIf;
					
					TemplateArea.Parameters.Content = SelectionOfQueryResult.Content;
					TemplateArea.Parameters.Counterparty = SelectionOfQueryResult.CounterpartyPresentation;
					
					MyCurrentDayTasks.Put(TemplateArea);
					
					DetailsStructure = New Structure;
					DetailsStructure.Insert("DocumentRef", SelectionOfQueryResult.Ref);
					CellCoordinates = "R" + String(MyCurrentDayTasks.TableHeight - 3) + "C10";
					MyCurrentDayTasks.Area(CellCoordinates).Details = DetailsStructure;
					
					If SelectionOfQueryResult.EndTime < DateOfSchedule Then
						MarkCompletedJob(MyCurrentDayTasks.TableHeight - 3, False, StyleColorPastEvent, "MyCurrentDayTasks");
					EndIf;
					
				EndDo;
				
				MyCurrentDayTasks.EndRowGroup();
				
			ElsIf SelectionByOrder.Order = 2 Then
				
				SelectionOfQueryResult = SelectionByOrder.Select();
				TemplateArea = TemplateMyCurrentDayTasks.GetArea("GroupTasks");
				TemplateArea.Parameters.TasksQuantity = String(SelectionOfQueryResult.Count());
				MyCurrentDayTasks.Put(TemplateArea);
				MyCurrentDayTasks.StartRowGroup();
				
				While SelectionOfQueryResult.Next() Do
				
					TemplateArea = TemplateMyCurrentDayTasks.GetArea("RowTask");
					
					If ValueIsFilled(SelectionOfQueryResult.ProductsPresentation) Then
						TemplateArea.Parameters.Description = SelectionOfQueryResult.ProductsPresentation;
					ElsIf ValueIsFilled(SelectionOfQueryResult.Comment) Then
						TemplateArea.Parameters.Description = SelectionOfQueryResult.Comment;
					ElsIf ValueIsFilled(SelectionOfQueryResult.CommentOfDocument) Then
						TemplateArea.Parameters.Description = SelectionOfQueryResult.CommentOfDocument;
					Else
						TemplateArea.Parameters.Description = "<Name is not specified>";
					EndIf;
					
					If BegOfDay(SelectionByDate.BeginTime) = BegOfDay(SelectionOfQueryResult.EndTime) Then
						TemplateArea.Parameters.Period = "" + Format(SelectionOfQueryResult.EndTime, "DF=HH:mm");
					Else
						TemplateArea.Parameters.Period = "" + Format(SelectionOfQueryResult.EndTime, "DF=HH:mm") + " " + Format(SelectionOfQueryResult.Day, "DLF=D");
					EndIf;
					
					If ValueIsFilled(SelectionOfQueryResult.CommentOfDocument) Then
						TemplateArea.Parameters.Definition = SelectionOfQueryResult.CommentOfDocument;
					ElsIf ValueIsFilled(SelectionOfQueryResult.Comment) Then
						TemplateArea.Parameters.Definition = SelectionOfQueryResult.Comment;
					ElsIf ValueIsFilled(SelectionOfQueryResult.ProductsPresentation) Then
						TemplateArea.Parameters.Definition = SelectionOfQueryResult.ProductsPresentation;
					Else
						TemplateArea.Parameters.Definition = "<Description is not specified>";
					EndIf;
					
					TemplateArea.Parameters.Counterparty = ?(ValueIsFilled(SelectionOfQueryResult.CustomerPresentation), SelectionOfQueryResult.CustomerPresentation, "<Not specified>");
					
					If SelectionOfQueryResult.Closed Then
						TemplateArea.Parameters.Flag = "";
					Else
						TemplateArea.Parameters.Flag = "";
					EndIf;
					
					MyCurrentDayTasks.Put(TemplateArea);
					
					DetailsStructure = New Structure;
					DetailsStructure.Insert("DocumentRef", SelectionOfQueryResult.Ref);
					DetailsStructure.Insert("Date", SelectionOfQueryResult.EndTime);
					CellCoordinates = "R" + String(MyCurrentDayTasks.TableHeight - 3) + "C10";
					MyCurrentDayTasks.Area(CellCoordinates).Details = DetailsStructure;
					
					If SelectionOfQueryResult.Closed Then
						MarkCompletedJob(MyCurrentDayTasks.TableHeight - 3, True, StyleColorCompletedJob, "MyCurrentDayTasks");
					ElsIf SelectionOfQueryResult.EndTime < DateOfSchedule Then
						MarkCompletedJob(MyCurrentDayTasks.TableHeight - 3, False, StyleColorOverdueJob, "MyCurrentDayTasks");
					EndIf;
				
				EndDo;
			
				MyCurrentDayTasks.EndRowGroup();
				
			EndIf;
			
		EndDo;
		
		If DateCount > 1 Then
			MyCurrentDayTasks.EndRowGroup();
		EndIf;
		
	EndDo;

EndProcedure
