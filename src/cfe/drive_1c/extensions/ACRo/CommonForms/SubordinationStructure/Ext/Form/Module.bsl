﻿
&AtServer
&Around("ObjectsByFilterCriteria")
Function ac_ObjectsByFilterCriteria(FilterCriteriaValue)
	
	If Not (Metadata.FilterCriteria.RelatedDocuments.Type.ContainsType(TypeOf(FilterCriteriaValue)) or Type("DocumentRef.WorkComplete") = TypeOf(FilterCriteriaValue))  Then
		Return Undefined;
	EndIf;
		
	SetPrivilegedMode(True);
	
	//Query = New Query;
	//Query.Text = "SELECT
	//|	RelatedDocuments.Ref
	//|FROM
	//|	FilterCriterion.RelatedDocuments(&FilterCriteriaValue) AS RelatedDocuments";
	//
	//Query.SetParameter("FilterCriteriaValue", FilterCriteriaValue);
	//Return Query.Execute().Unload();
	
	Query = New Query;
	Query.Text = "SELECT
	|	RelatedDocuments.Ref AS Ref
	|FROM
	|	FilterCriterion.RelatedDocuments(&FilterCriteriaValue) AS RelatedDocuments
	|
	|UNION ALL
	|
	|SELECT
	|	SalesOrder.Ref
	|FROM
	|	Document.SalesOrder AS SalesOrder
	|WHERE
	|	SalesOrder.ac_BasisDocument = &FilterCriteriaValue
	|	AND NOT SalesOrder.Ref = &FilterCriteriaValue
	| ";
	//|UNION ALL
	//|
	//|SELECT
	//|	SalesOrder.ac_BasisDocument
	//|FROM
	//|	Document.SalesOrder AS SalesOrder
	//|WHERE
	//|	SalesOrder.Ref = &FilterCriteriaValue
	//|	AND NOT SalesOrder.ac_BasisDocument = &FilterCriteriaValue";
	
	Query.SetParameter("FilterCriteriaValue", FilterCriteriaValue);
	If Type("DocumentRef.WorkComplete") = TypeOf(FilterCriteriaValue) Then
		Query.Text = StrReplace(Query.Text, "SELECT
		|	RelatedDocuments.Ref AS Ref
		|FROM
		|	FilterCriterion.RelatedDocuments(&FilterCriteriaValue) AS RelatedDocuments
		|
		|UNION ALL", "");
	Endif;
	Return Query.Execute().Unload();

	//
	//Запрос.Текст = "ВЫБРАТЬ
	//| СвязанныеДокументы.Ссылка КАК Ссылка
	//|ИЗ
	//| КритерийОтбора.СвязанныеДокументы(&ЗначениеКритерияОтбора) КАК СвязанныеДокументы
	//|
	//|ОБЪЕДИНИТЬ ВСЕ
	//|
	//|ВЫБРАТЬ
	//| СчетНаОплатуПоставщика.Ссылка
	//|ИЗ
	//| Документ.СчетНаОплатуПоставщика КАК СчетНаОплатуПоставщика
	//|ГДЕ
	//| СчетНаОплатуПоставщика.ЕВ_ДокументОснование = &ЗначениеКритерияОтбора
	//| И НЕ СчетНаОплатуПоставщика.Ссылка = &ЗначениеКритерияОтбора
	//|
	//|ОБЪЕДИНИТЬ ВСЕ
	//|
	//|ВЫБРАТЬ
	//| СчетНаОплатуПоставщика.ЕВ_ДокументОснование
	//|ИЗ
	//| Документ.СчетНаОплатуПоставщика КАК СчетНаОплатуПоставщика
	//|ГДЕ
	//| СчетНаОплатуПоставщика.Ссылка = &ЗначениеКритерияОтбора
	//| И НЕ СчетНаОплатуПоставщика.ЕВ_ДокументОснование = &ЗначениеКритерияОтбора";
	//
	//Запрос.УстановитьПараметр("ЗначениеКритерияОтбора", ЗначениеКритерияОтбора);
	//Возврат Запрос.Выполнить().Выгрузить();

	//// Insert method content.
	//Result = ProceedWithCall(FilterCriteriaValue);
	//Return Result;
EndFunction

&AtServer
Procedure ac_OnCreateAtServerAround(Cancel, StandardProcessing)
	
	If Parameters.Property("AutoTest") Then // Return when a form is received for analysis.
		Return;
	EndIf;
	
	DefineSettings();
	
	Try
		//Parameters.Property("FilterObject", ObjectRef);
		ObjectRef = Parameters.FilterObject;
	Except
		Return
	EndTry;
	InitialObject = ObjectRef;
	If ValueIsFilled(ObjectRef) Then
		UpdateSubordinationStructureTree();
	EndIf;
	
EndProcedure
