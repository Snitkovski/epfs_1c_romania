﻿
//&AtServer
//&Around("UpdateInfoOnReportOption")
//Procedure ac_UpdateInfoOnReportOption()
//	//// Вставить содержимое метода.
//	//ProceedWithCall();
//	
//	
//	
//	DCUserSettings = Report.SettingsComposer.UserSettings;
//	For Each DCUserSetting In DCUserSettings.Items Do
//		Type = ReportsClientServer.SettingTypeAsString(TypeOf(DCUserSetting));
//		
//		If Type = "SettingsParameterValue"
//			AND TypeOf(DCUserSetting.Value) = Type("StandardPeriod")
//			AND DCUserSetting.Use Then
//			
//			//ItemID = ReportsClientServer.CastIDToName(DCUserSetting.UserSettingID);
//			
//			//PeriodBeginning    = Items.Find(Type + "_Start_"    + ItemID);
//			//PeriodEnd = Items.Find(Type + "_End_" + ItemID);
//			//If PeriodBeginning = Undefined Or PeriodEnd = Undefined Then
//			//	Continue;
//			//EndIf; 
//			
//			Value = DCUserSetting.Value;
//			PeriodBeginning = Value.StartDate;
//			PeriodEnd = Value.EndDate;				
//			
//		EndIf;
//		
//	EndDo;
//	
//	Report.SettingsComposer.Settings.AdditionalProperties.Insert("VariantKey", CurrentVariantKey);
//	Report.SettingsComposer.Settings.AdditionalProperties.Insert("DescriptionOption", ReportCurrentOptionDescription);
//	Report.SettingsComposer.Settings.AdditionalProperties.Insert("BeginOfPeriod", PeriodBeginning);
//	Report.SettingsComposer.Settings.AdditionalProperties.Insert("EndOfPeriod", PeriodEnd);
//	Report.SettingsComposer.Settings.DataParameters.SetParameterValue("BeginOfPeriod", PeriodBeginning);
//	Report.SettingsComposer.Settings.DataParameters.SetParameterValue("EndOfPeriod", PeriodEnd);
//	
//	Query = New Query;
//	Query.Text =
//	"SELECT ALLOWED TOP 1
//	|	ReportsOptions.Ref AS OptionRef,
//	|	ReportsOptions.PredefinedVariant.MeasurementsKey AS MeasurementsKey,
//	|	ReportsOptions.PredefinedVariant AS PredefinedRef,
//	|	CASE
//	|		WHEN ReportsOptions.Custom
//	|				OR ReportsOptions.Parent.VariantKey IS NULL 
//	|			THEN ReportsOptions.VariantKey
//	|		ELSE ReportsOptions.Parent.VariantKey
//	|	END AS OriginalOptionName,
//	|	ReportsOptions.Custom AS Custom
//	|FROM
//	|	Catalog.ReportsOptions AS ReportsOptions
//	|WHERE
//	|	ReportsOptions.Report = &Report
//	|	AND ReportsOptions.VariantKey = &VariantKey";
//	Query.SetParameter("Report", ReportSettings.ReportRef);
//	Query.SetParameter("VariantKey", CurrentVariantKey);
//	
//	Selection = Query.Execute().Select();
//	
//	If Selection.Next() Then
//		ReportSettings.Insert("OptionRef",          Selection.OptionRef);
//		ReportSettings.Insert("MeasurementsKey",            Selection.MeasurementsKey);
//		ReportSettings.Insert("PredefinedRef", Selection.PredefinedRef);
//		ReportSettings.Insert("OriginalOptionName",   ?(Selection.Custom, Selection.OriginalOptionName, CurrentVariantKey));
//		ReportSettings.Insert("Custom",       Selection.Custom);
//	Else
//		ReportSettings.Insert("OptionRef",          Undefined);
//		ReportSettings.Insert("MeasurementsKey",            Undefined);
//		ReportSettings.Insert("PredefinedRef", Undefined);
//		ReportSettings.Insert("OriginalOptionName",   Undefined);
//		ReportSettings.Insert("Custom",       Undefined);
//	EndIf;

//	
//EndProcedure

//&AtServer
//&After("QuickSettingsImportSettingsToComposer")
//Procedure ac_QuickSettingsImportSettingsToComposer(FillingParameters)
//	
//	//If 	
//	
//	DCUserSettings = Report.SettingsComposer.UserSettings;
//	For Each DCUserSetting In DCUserSettings.Items Do
//		Type = ReportsClientServer.SettingTypeAsString(TypeOf(DCUserSetting));
//		
//		If Type = "SettingsParameterValue"
//			AND TypeOf(DCUserSetting.Value) = Type("StandardPeriod")
//			AND DCUserSetting.Use Then
//			
//			//ItemID = ReportsClientServer.CastIDToName(DCUserSetting.UserSettingID);
//			
//			//PeriodBeginning    = Items.Find(Type + "_Start_"    + ItemID);
//			//PeriodEnd = Items.Find(Type + "_End_" + ItemID);
//			//If PeriodBeginning = Undefined Or PeriodEnd = Undefined Then
//			//	Continue;
//			//EndIf; 
//			
//			Value = DCUserSetting.Value;
//			PeriodBeginning = Value.StartDate;
//			PeriodEnd = Value.EndDate;				
//			
//		EndIf;
//		
//	EndDo;

//	
//	Report.SettingsComposer.Settings.AdditionalProperties.Insert("BeginOfPeriod", PeriodBeginning);
//	Report.SettingsComposer.Settings.AdditionalProperties.Insert("EndOfPeriod", PeriodEnd);
//	Report.SettingsComposer.Settings.DataParameters.SetParameterValue("BeginOfPeriod", PeriodBeginning);
//	Report.SettingsComposer.Settings.DataParameters.SetParameterValue("EndOfPeriod", PeriodEnd);

//	
//EndProcedure

&AtServer
Procedure UserSettingsToDataParameters()
	
	//If Find(info, "Denumire platitor:") > 0 Then
	//		infoNodeTABLE = nodeTABLE;
	//		Break;
	//	EndIf;
	
	If Find(Report.SettingsComposer.Settings.OutputParameters.Items[11].Value, "ABC/XYZ") > 0 Then
		//If Report.SettingsComposer.Settings.OutputParameters.Items[11].Value = 
		//Analiza ABC/XYZ a  pe nomenclator
		DCUserSettings = Report.SettingsComposer.UserSettings;
		For Each DCUserSetting In DCUserSettings.Items Do
			Type = ReportsClientServer.SettingTypeAsString(TypeOf(DCUserSetting));
			
			If Type = "SettingsParameterValue"
				AND TypeOf(DCUserSetting.Value) = Type("StandardPeriod")
				AND DCUserSetting.Use Then
				
				Value = DCUserSetting.Value;
				PeriodBeginning = Value.StartDate;
				PeriodEnd = Value.EndDate;
				
			ElsIf Type = "SettingsParameterValue"
				//AND String(DCUserSetting.Value) = "Products"
				AND String(DCUserSetting.Parameter) = "AnalysisObject"
				AND DCUserSetting.Use Then
				
				AnalysisObjectValue = DCUserSettings.Items[16].RightValue;
				
			ElsIf Type = "SettingsParameterValue"
				//AND String(DCUserSetting.Value) = "AnalysisParameter"  
				AND String(DCUserSetting.Parameter) = "AnalysisParameter"
				AND DCUserSetting.Use Then
				
				AnalysisParameterValue = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterXClassStart"
				AND DCUserSetting.Use Then
				
				ParameterXClassStart = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterXClassEnd"
				AND DCUserSetting.Use Then
				
				ParameterXClassEnd = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterYClassStart"
				AND DCUserSetting.Use Then
				
				ParameterYClassStart = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterYClassEnd"
				AND DCUserSetting.Use Then
				
				ParameterYClassEnd = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterZClassStart"
				AND DCUserSetting.Use Then
				
				ParameterZClassStart = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "ParameterZClassEnd"
				AND DCUserSetting.Use Then
				
				ParameterZClassEnd = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "PercentageAClass"
				AND DCUserSetting.Use Then
				
				PercentageAClass = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "PercentageBClass"
				AND DCUserSetting.Use Then
				
				PercentageBClass = DCUserSetting.Value;
				
			ElsIf Type = "SettingsParameterValue"
				
				AND String(DCUserSetting.Parameter) = "PercentageCClass"
				AND DCUserSetting.Use Then
				
				PercentageCClass = DCUserSetting.Value;
				
			ElsIf Type = "FilterItem"
				
				AND TypeOf(DCUserSettings.Items[17].RightValue) = Type("CatalogRef.Employees")
				AND DCUserSetting.Use Then
				
				ResponsibleFilterValue = DCUserSettings.Items[17].RightValue;
				
				//ElsIf Type = "FilterItem"
				//	
				//	AND TypeOf(DCUserSettings.Items[15].RightValue) = Type("CatalogRef.Companies")
				//	AND DCUserSetting.Use Then  						
				//	
				//	CompanyFilterValue = DCUserSettings.Items[15].RightValue;	
				//	
				
			EndIf;
			
		EndDo;
		
		Report.SettingsComposer.Settings.AdditionalProperties.Insert("BeginOfPeriod", PeriodBeginning);
		Report.SettingsComposer.Settings.AdditionalProperties.Insert("EndOfPeriod", PeriodEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("BeginOfPeriod", PeriodBeginning);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("EndOfPeriod", PeriodEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ItmPeriod", Value);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("AnalysisParameter", AnalysisParameterValue);
		
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterXClassStart", ParameterXClassStart);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterXClassEnd", ParameterXClassEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterYClassStart", ParameterYClassStart);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterYClassEnd", ParameterYClassEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterZClassStart", ParameterZClassStart);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("ParameterZClassEnd", ParameterZClassEnd);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("PercentageAClass", PercentageAClass);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("PercentageBClass", PercentageBClass);
		Report.SettingsComposer.Settings.DataParameters.SetParameterValue("PercentageCClass", PercentageCClass);

		//Report.SettingsComposer.Settings.Filter.Items[0].RightValue = CompanyFilterValue; 
		//Report.SettingsComposer.Settings.Filter.Items[0].ComparisonType = DataCompositionComparisonType.InListByHierarchy;   
		//Report.SettingsComposer.Settings.Filter.Items[0].Use = True;
		
		Report.SettingsComposer.Settings.Filter.Items[1].RightValue = AnalysisObjectValue;
		Report.SettingsComposer.Settings.Filter.Items[1].ComparisonType = DataCompositionComparisonType.InListByHierarchy;
		Report.SettingsComposer.Settings.Filter.Items[1].Use = True;  //Элементы.Список.Обновить();
		
		Report.SettingsComposer.Settings.Filter.Items[2].RightValue = ResponsibleFilterValue;
		Report.SettingsComposer.Settings.Filter.Items[2].ComparisonType = DataCompositionComparisonType.InListByHierarchy;
		Report.SettingsComposer.Settings.Filter.Items[2].Use = True;
		
	EndIf;
		
EndProcedure

&AtClient
Procedure ac_ReportComposeResultBefore(Command)
	
	 UserSettingsToDataParameters();
	
EndProcedure

