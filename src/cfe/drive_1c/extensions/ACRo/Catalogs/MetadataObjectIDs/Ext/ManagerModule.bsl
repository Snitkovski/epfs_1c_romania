﻿
//Function MetadataObjectCollectionProperties(ExtensionsObjects = False) Export

// For internal use only.
&Around("MetadataObjectCollectionProperties")
Function ac_MetadataObjectCollectionProperties(ExtensionsObjects = False) Export
	
	//// Вставить содержимое метода.
	//Result = ProceedWithCall(ExtensionsObjects);
	//Return Result;	
	
	MetadataObjectCollectionProperties = New ValueTable;
	MetadataObjectCollectionProperties.Columns.Add("Name",                       New TypeDescription("String",, New StringQualifiers(50)));
	MetadataObjectCollectionProperties.Columns.Add("SingularName",               New TypeDescription("String",, New StringQualifiers(50)));
	MetadataObjectCollectionProperties.Columns.Add("Synonym",                   New TypeDescription("String",, New StringQualifiers(255)));
	MetadataObjectCollectionProperties.Columns.Add("SingularSynonym",           New TypeDescription("String",, New StringQualifiers(255)));
	MetadataObjectCollectionProperties.Columns.Add("CollectionOrder",          New TypeDescription("Number"));
	MetadataObjectCollectionProperties.Columns.Add("NoData",                 New TypeDescription("Boolean"));
	MetadataObjectCollectionProperties.Columns.Add("NoMetadataObjectKey", New TypeDescription("Boolean"));
	MetadataObjectCollectionProperties.Columns.Add("ID",             New TypeDescription("UUID"));
	MetadataObjectCollectionProperties.Columns.Add("ExtensionsObjects",         New TypeDescription("Boolean"));
	
	// Constants
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("627a6fb8-872a-11e3-bb87-005056c00008");
	Row.Name             = "Constants";
	Row.Synonym         = NStr("en='Constants';pl='Stałe';ro='Constante';ru='Константы'");
	Row.SingularName     = "Constant";
	Row.SingularSynonym = NStr("en='Constant';pl='Konstanta';ro='Constant';ru='Константа'");
	
	// Subsystems
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("cdf5ac50-08e8-46af-9a80-4e63fd4a88ff");
	Row.Name             = "Subsystems";
	Row.Synonym         = NStr("en='Subsystems';pl='Podsystemy';ro='Subsisteme';ru='Подсистемы'");
	Row.SingularName     = "Subsystem";
	Row.SingularSynonym = NStr("en='Subsystem';pl='Podsystem';ro='Subsistem';ru='Подсистема'");
	Row.NoData       = True;
	Row.NoMetadataObjectKey = True;
	Row.ExtensionsObjects = True;
	
	// Roles
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("115c4f55-9c20-4e86-a6d0-d0167ec053a1");
	Row.Name             = "Roles";
	Row.Synonym         = NStr("en='Roles';pl='Role';ro='Roluri';ru='Роли'");
	Row.SingularName     = "Role";
	Row.SingularSynonym = NStr("en='Role';pl='Wykonuje rolę';ro='Rol';ru='Роль'");
	Row.NoData       = True;
	Row.NoMetadataObjectKey = True;
	Row.ExtensionsObjects = True;
	
	// ExchangePlans
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("269651e0-4b06-4f9d-aaab-a8d2b6bc6077");
	Row.Name             = "ExchangePlans";
	Row.Synonym         = NStr("en='Exchange plans';pl='Plany wymiany';ro='Planurile de schimb';ru='Планы обмена'");
	Row.SingularName     = "ExchangePlan";
	Row.SingularSynonym = NStr("en='Exchange plan';pl='Planu wymiany';ro='Plan de schimb';ru='План обмена'");
	Row.ExtensionsObjects = True;
	
	// Catalogs
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("ede89702-30f5-4a2a-8e81-c3a823b7e161");
	Row.Name             = "Catalogs";
	Row.Synonym         = NStr("en='Catalogs';pl='Kartoteki';ro='Cataloage';ru='Справочники'");
	Row.SingularName     = "Catalog";
	Row.SingularSynonym = NStr("en='Catalog';pl='Katalog';ro='Catalog';ru='Справочник'");
	Row.ExtensionsObjects = True;
	
	// Documents
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("96c6ab56-0375-40d5-99a2-b83efa3dac8b");
	Row.Name             = "Documents";
	Row.Synonym         = NStr("en='Documents';pl='Dokumenty';ro='Documente';ru='Документы'");
	Row.SingularName     = "Document";
	Row.SingularSynonym = NStr("en='Document';pl='Document';ro='Document';ru='Документ'");
	Row.ExtensionsObjects = True;
	
	// DocumentJournals
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("07938234-e29b-4cff-961a-9af07a4c6185");
	Row.Name             = "DocumentJournals";
	Row.Synonym         = NStr("en='Document journals';pl='Dzienniki dokumentów';ro='Registrele documentelor';ru='Журналы документов'");
	Row.SingularName     = "DocumentJournal";
	Row.SingularSynonym = NStr("en='Document journal';pl='Dziennik dokumentów';ro='Document jurnal';ru='Журнал документов'");
	Row.NoData       = True;
	
	// Reports
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("706cf832-0ae5-45b5-8a4a-1f251d054f3b");
	Row.Name             = "Reports";
	Row.Synonym         = NStr("en='Reports';pl='Sprawozdania';ro='Rapoarte';ru='Отчеты'");
	Row.SingularName     = "Report";
	Row.SingularSynonym = NStr("en='Report';pl='Raport';ro='Raport';ru='Отчет'");
	Row.NoData       = True;
	Row.ExtensionsObjects = True;
	
	// Data processors
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("ae480426-487e-40b2-98ba-d207777449f3");
	Row.Name             = "DataProcessors";
	Row.Synonym         = NStr("en='Data processors';pl='Przetwarzanie';ro='Procesoare de date';ru='Обработки'");
	Row.SingularName     = "DataProcessor";
	Row.SingularSynonym = NStr("en='Data processor';pl='Przetwarzanie';ro='Procesare de date';ru='Обработка'");
	Row.NoData       = True;
	Row.ExtensionsObjects = True;
	
	// ChartsOfCharacteristicTypes
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("8b5649b9-cdd1-4698-9aac-12ba146835c4");
	Row.Name             = "ChartsOfCharacteristicTypes";
	Row.Synonym         = NStr("en='Charts of characteristic types';pl='Plany rodzajów charakterystyk';ro='Diagrame de tipuri caracteristice';ru='Планы видов характеристик'");
	Row.SingularName     = "ChartOfCharacteristicTypes";
	Row.SingularSynonym = NStr("en='Chart of characteristic types';pl='Plan rodzajów charakterystyk';ro='Diagrama tipurilor caracteristice';ru='План видов характеристик'");
	
	// ChartsOfAccounts
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("4295af27-543f-4373-bcfc-c0ace9b7620c");
	Row.Name             = "ChartsOfAccounts";
	Row.Synonym         = NStr("en='Charts of accounts';pl='Plany rachunków';ro='Planurile conturilor';ru='Планы счетов'");
	Row.SingularName     = "ChartOfAccounts";
	Row.SingularSynonym = NStr("en='Chart of accounts';pl='Plan rachunków';ro='Plan de conturi';ru='План счетов'");
	
	// ChartsOfCalculationTypes
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("fca3e7e1-1bf1-49c8-9921-aafb4e787c75");
	Row.Name             = "ChartsOfCalculationTypes";
	Row.Synonym         = NStr("en='Charts of calculation types';pl='Plany rodzajów obliczeń';ro='Diagrame de tipuri de calcul';ru='Планы видов расчета'");
	Row.SingularName     = "ChartOfCalculationTypes";
	Row.SingularSynonym = NStr("en='Chart of calculation types';pl='Plan rodzajów rozliczenia';ro='Graficul tipurilor de calcul';ru='План видов расчета'");
	
	// InformationRegisters
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("d7ecc1e9-c068-44dd-83c2-1323ec52dbbb");
	Row.Name             = "InformationRegisters";
	Row.Synonym         = NStr("en='Information registers';pl='Rejestry informacji';ro='Registre de date';ru='Регистры сведений'");
	Row.SingularName     = "InformationRegister";
	Row.SingularSynonym = NStr("en='Information register';pl='Rejestr informacji';ro='Registrul de informații';ru='Регистр сведений'");
	Row.ExtensionsObjects = True;
	
	// AccumulationRegisters
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("74083488-b01e-4441-84a6-c386ce88cdb5");
	Row.Name             = "AccumulationRegisters";
	Row.Synonym         = NStr("en='Accumulation registers';pl='Rejestry kumulacji';ro='Registre de acumulare';ru='Регистры накопления'");
	Row.SingularName     = "AccumulationRegister";
	Row.SingularSynonym = NStr("en='Accumulation register';pl='Rejestr akumulacji';ro='Registrul de acumulare';ru='Регистр накопления'");
	Row.ExtensionsObjects = True;
	
	// AccountingRegisters
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("9a0d75ff-0eda-454e-b2b7-d2412ffdff18");
	Row.Name             = "AccountingRegisters";
	Row.Synonym         = NStr("en='Accounting registers';pl='Rejestry księgowości';ro='Registre contabile';ru='Регистры бухгалтерии'");
	Row.SingularName     = "AccountingRegister";
	Row.SingularSynonym = NStr("en='Accounting register';pl='Rejestr księgowości';ro='Registrul contabil';ru='Регистр бухгалтерии'");
	
	// CalculationRegisters
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("f330686a-0acf-4e26-9cda-108f1404687d");
	Row.Name             = "CalculationRegisters";
	Row.Synonym         = NStr("en='Calculation registers';pl='Rejestry rozliczenia';ro='Registre de calcul';ru='Регистры расчета'");
	Row.SingularName     = "CalculationRegister";
	Row.SingularSynonym = NStr("en='Calculation register';pl='Rejestr rozliczenia';ro='Registrul de calcul';ru='Регистр расчета'");
	
	// BusinessProcesses
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("a8cdd0e0-c27f-4bf0-9718-10ec054dc468");
	Row.Name             = "BusinessProcesses";
	Row.Synonym         = NStr("en='Business processes';pl='Procesy biznesowe';ro='Procesele de afaceri';ru='Бизнес-процессы'");
	Row.SingularName     = "BusinessProcess";
	Row.SingularSynonym = NStr("en='Business process';pl='Proces biznesowy';ro='Procesul de afaceri';ru='Бизнес-процесс'");
	
	// Tasks
	Row = MetadataObjectCollectionProperties.Add();
	Row.ID   = New UUID("8d9153ad-7cea-4e25-9542-a557ee59fd16");
	Row.Name             = "Tasks";
	Row.Synonym         = NStr("en='Tasks';pl='Zadań';ro='Sarcini';ru='Задания'");
	Row.SingularName     = "Task";
	Row.SingularSynonym = NStr("en='Task';pl='Zadanie';ro='Sarcină';ru='Задача'");
	
	For each Row In MetadataObjectCollectionProperties Do
		Row.CollectionOrder = MetadataObjectCollectionProperties.IndexOf(Row);
	EndDo;
	
	If ExtensionsObjects Then
		MetadataObjectCollectionProperties = MetadataObjectCollectionProperties.Copy(
			New Structure("ExtensionsObjects", True));
	EndIf;
	
	MetadataObjectCollectionProperties.Indexes.Add("ID");
	
	Return MetadataObjectCollectionProperties;
	
EndFunction
