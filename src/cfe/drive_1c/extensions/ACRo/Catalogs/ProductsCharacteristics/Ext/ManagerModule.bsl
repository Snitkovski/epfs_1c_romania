﻿
//&Around("ChoiceDataGetProcessing")
//Procedure ac_ChoiceDataGetProcessing(ChoiceData, Parameters, StandardProcessing)
//	
//	If ValueIsFilled (Documents.SalesOrder.ac_BasisDocument) Then
//		
//		Products 		 = Parameters.Filter.Owner;
//		ProductsCategory = Parameters.Filter.Owner.ProductsCategory;
//		

//		FilterArray = New Array;
//		FilterArray.Add(Products);
//		FilterArray.Add(ProductsCategory);
//		
//		Parameters.Filter.Insert("Owner", FilterArray);
//	Else 
//		
//		ProceedWithCall(ChoiceData, Parameters, StandardProcessing);
//		
//	EndIf;
	
//If Parameters.Filter.Property("Owner") AND TypeOf(Parameters.Filter.Owner) = Type("CatalogRef.Products") Then
//		// If selection parameter link by products and
//		// services value is set, then add selection parameters by the owner filter - product group.
//		
//		Products 		 = Parameters.Filter.Owner;
//		ProductsCategory = Parameters.Filter.Owner.ProductsCategory;
//		
//		MessageText = "";
//		If Not ValueIsFilled(Products) Then
//			MessageText = NStr("en = 'Product is not filled in.'; tr = 'Ürün doldurulmadı.';ro = 'Produsul nu este completat.';pl = 'Nie wypełniono pola towaru.';ru = 'Не заполнена номенклатура!';de = 'Das Produkt ist nicht ausgefüllt';es_ES = 'Producto no se ha rellenado.'");
//		ElsIf Parameters.Property("ThisIsReceiptDocument") AND Products.ProductsType = Enums.ProductsTypes.Service Then
//			MessageText = NStr("en = 'Accounting by variants is not kept for services of external counterparties.'; tr = 'Harici karşı tarafların hizmetleri için değişkenler bazında muhasebe yapılmaz.';ro = 'Accounting by variants is not kept for services of external counterparties.';pl = 'Dla kontrahentów zewnętrznych nie jest prowadzona ewidencja według wariantów.';ru = 'Для услуг сторонних контрагентов не ведется учет по вариантам!';de = 'Die Abrechnung nach Varianten erfolgt nicht für Dienstleistungen externer Geschäftspartner.';es_ES = 'Contabilidad por variantes no se ha guardado para los servicios de las contrapartes externas.'");
//		ElsIf Not Products.UseCharacteristics Then
//			MessageText = NStr("en = 'Accounting by variants is not kept for the product.'; tr = 'Ürün için değişkenler bazında muhasebe işlemi yapılmaz.';ro = 'Accounting by variants is not kept for the product.';pl = 'Ewidencja według wariantów nie jest prowadzona dla produktu.';ru = 'Для номенклатуры не ведется учет по вариантам.';de = 'Die Abrechnung nach Varianten wird für das Produkt nicht durchgeführt.';es_ES = 'Contabilidad por variantes no se ha guardado para el producto.'");
//		EndIf;
//		
//		If Not IsBlankString(MessageText) Then
//			CommonClientServer.MessageToUser(MessageText);
//			StandardProcessing = False;
//			Return;
//		EndIf;
//		
//		FilterArray = New Array;
//		FilterArray.Add(Products);
//		FilterArray.Add(ProductsCategory);
//		
//		Parameters.Filter.Insert("Owner", FilterArray);
//		
//	EndIf;

//EndProcedure

//&After("ChoiceDataGetProcessing")
//Procedure ac_ChoiceDataGetProcessing(ChoiceData, Parameters, StandardProcessing)
//	
//	If ValueIsFilled (Documents.SalesOrder.ac_BasisDocument) Then
//		
//	EndIf;	
//	
//EndProcedure
