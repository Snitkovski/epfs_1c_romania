﻿
&AtServer
Procedure ac_OnCreateAtServerAfter(Cancel, StandardProcessing)
	
	// StandardSubsystems.AttachableCommands
	//AttachableCommands.OnCreateAtServer(ThisObject);
	// End StandardSubsystems.AttachableCommands
EndProcedure

//&AtClient
//Procedure PrintHTMLDocument(Command)	
//	
//	Items.ContractHTMLDocument.Document.execCommand("Print");
//	
//EndProcedure

&AtClient
Procedure ac_NotificationProcessingAround(EventName, Parameter, Source)
	If EventName = "PredefinedTemplateRestoration" Then
		If Parameter = Object.ContractForm Then
			FilterParameters = New Structure;
			FilterParameters.Insert("FormRefs", Object.ContractForm);
			ParameterArray = Object.EditableParameters.FindRows(FilterParameters);
			For Each String In ParameterArray Do
				String.Value = "";
			EndDo;
		EndIf;
	EndIf;
	
	If EventName = "ContractTemplateChangeAndRecordAtServer" Then
		If Parameter = Object.ContractForm Then
			DocumentCreated = False;
			GetBlankParameters();
			GenerateAndShowContract();
			Modified = True;
			ShowDocumentBeginning = True;
			CurrentParameterClicked = "";
		EndIf;
	EndIf;
	
	// StandardSubsystems.Properties
	If PropertyManagerClient.ProcessNofifications(ThisObject, EventName, Parameter) Then
		UpdateAdditionalAttributeItems();
		PropertyManagerClient.AfterImportAdditionalAttributes(ThisObject);
	EndIf;
	// End StandardSubsystems.Properties
EndProcedure

//&AtClient
//Procedure ac_OnOpenAfter(Cancel)
//	
//	
//	// StandardSubsystems.Properties
//	PropertyManagerClient.AfterImportAdditionalAttributes(ThisObject);
//	// End StandardSubsystems.Properties
//	
//	FormManagement();

//	
//EndProcedure

&AtServer
&Around("GenerateAndShowContract")
Procedure ac_GenerateAndShowContract()
	
	//ProceedWithCall();

	If Not DocumentCreated Then
		
		EditableParameters.Clear();
		FilterParameters = New Structure("FormRefs", Object.ContractForm);
		ArrayInfobaseParameters = Object.InfobaseParameters.FindRows(FilterParameters);
		For Each Parameter In ArrayInfobaseParameters Do
			NewRow = EditableParameters.Add();
			NewRow.Presentation = Parameter.Presentation;
			NewRow.Value = Parameter.Value;
			NewRow.ID = Parameter.ID;
			if Parameter.Parameter = Undefined Then
				NewRow.Parameter = Parameter.ac_Parameter;
			else
				NewRow.Parameter = Parameter.Parameter;
			EndIf;
			NewRow.LineNumber = Parameter.LineNumber;
		EndDo;
		
		ArrayEditedParameters = Object.EditableParameters.FindRows(FilterParameters);
		For Each Parameter In ArrayEditedParameters Do
			NewRow = EditableParameters.Add();
			NewRow.Presentation = Parameter.Presentation;
			NewRow.Value = Parameter.Value;
			NewRow.ID = Parameter.ID;
			NewRow.LineNumber = Parameter.LineNumber;
		EndDo;
		
		GeneratedDocument = DriveCreationOfPrintedFormsOfContract.ac_GetGeneratedContractHTML(Object, OpeningDocument, EditableParameters);
		If ContractHTMLDocument = GeneratedDocument Then
			                                                    //ac_GetGeneratedContractHTML(Object, Document = Undefined, ListOfParameters)
																 //ac_GetGeneratedContractHTML(Object, OpeningDocument, EditableParameters);
			DocumentCreated = True;
		EndIf;
		ContractHTMLDocument = GeneratedDocument;
		
		FilterParameters = New Structure("Parameter", PredefinedValue("Enum.ContractsWithCounterpartiesTemplatesParameters.Facsimile"));
		Rows = EditableParameters.FindRows(FilterParameters);
		For Each String In Rows Do
			ID = String.GetID();
			EditableParameters.Delete(EditableParameters.FindByID(ID));
		EndDo;
		
		FilterParameters.Parameter = PredefinedValue("Enum.ContractsWithCounterpartiesTemplatesParameters.Logo");
		Rows = EditableParameters.FindRows(FilterParameters);
		For Each String In Rows Do
			ID = String.GetID();
			EditableParameters.Delete(EditableParameters.FindByID(ID))
		EndDo;
		
		For Each String In EditableParameters Do
			If ValueIsFilled(String.Value) Then
				String.ValueIsFilled = True;
			Else
				String.ValueIsFilled = False;
			EndIf;
		EndDo;
	EndIf;

EndProcedure

&AtServer
&Around("GetBlankParameters")
Procedure ac_GetBlankParameters()
	
	FilterParameters = New Structure("FormRefs", Object.ContractForm);
	ObjectEditedParameters		= Object.EditableParameters.FindRows(FilterParameters);
	ObjectInfobaseParameters	= Object.InfobaseParameters.FindRows(FilterParameters);
	
	For Each Parameter In ObjectEditedParameters Do
		FilterParameters = New Structure("ID", Parameter.ID);
		If Object.ContractForm.EditableParameters.FindRows(FilterParameters).Count() <> 0 Then
			Continue;
		EndIf;
		FilterParameters.Insert("FormRefs", Object.ContractForm);
		Rows = Object.EditableParameters.FindRows(FilterParameters);
		If Rows.Count() > 0 Then
			Object.EditableParameters.Delete(Rows[0]);
		EndIf;
	EndDo;
	
	For Each Parameter In Object.ContractForm.EditableParameters Do
		FilterParameters = New Structure("FormRefs, ID", Object.ContractForm, Parameter.ID);
		If Object.EditableParameters.FindRows(FilterParameters).Count() > 0 Then
			Continue;
		EndIf;
		NewRow = Object.EditableParameters.Add();
		NewRow.FormRefs		= Object.ContractForm;
		NewRow.Presentation	= Parameter.Presentation;
		NewRow.ID			= Parameter.ID;
	EndDo;
	
	For Each Parameter In ObjectInfobaseParameters Do
		FilterParameters = New Structure("ID", Parameter.ID);
		Rows = Object.ContractForm.InfobaseParameters.FindRows(FilterParameters);
		If Rows.Count() <> 0 Then
			Parameter.Presentation = Rows[0].Presentation;
			Continue;
		EndIf;
		FilterParameters.Insert("FormRefs", Object.ContractForm);
		Rows = Object.InfobaseParameters.FindRows(FilterParameters);
		If Rows.Count() > 0 Then
			Object.InfobaseParameters.Delete(Rows[0]);
		EndIf;
	EndDo;
	
	For Each Parameter In Object.ContractForm.InfobaseParameters Do
		FilterParameters = New Structure("FormRefs, ID", Object.ContractForm, Parameter.ID);
		If Object.InfobaseParameters.FindRows(FilterParameters).Count() > 0 Then
			Continue;
		EndIf;
		NewRow = Object.InfobaseParameters.Add();
		NewRow.FormRefs		= Object.ContractForm;
		NewRow.Presentation	= Parameter.Presentation;
		NewRow.ID			= Parameter.ID;
		NewRow.Parameter	= Parameter.Parameter;
		NewRow.ac_Parameter	= Parameter.ac_Parameter;
	EndDo;
	
EndProcedure

&AtClient
Procedure ac_OnOpenBefore(Cancel)
	ac_OnOpenBeforeAtServer();
EndProcedure

&AtServer
Procedure ac_OnOpenBeforeAtServer()
	
	NewFilterElement = AbonData.Filter.Items.Add(Type("DataCompositionFilterItem"));
	
	//FilterItem = UserFilter.Items.Add(Type("DataCompositionFilterItem"));             
	//FilterItem.LeftValue = New DataCompositionField("DeletionMark");
	//FilterItem.Use = true;
	//FilterItem.ComparisonType = DataCompositionComparisonType.NotEqual;
	//FilterItem.RightValue = False;
	//
	//Display ListSettingsComposerUserSettings
	//Items.ListSettingsComposerUserSettings.Visible = True;

	NewFilterElement.LeftValue = New DataCompositionField("Contract");
	NewFilterElement.ComparisonType = DataCompositionComparisonType.Equal; // больше или меньше и т.п.
	NewFilterElement.Use = true;;
	NewFilterElement.RightValue = Object.Ref; // с чем сравниваем

EndProcedure

&AtClient
Procedure ac_ContractHTMLDocumentDocumentCompleteAround(Item)
	
	//document = Items.ContractHTMLDocument.Document;
	//EditedParametersOnPage = document.getElementsByName("parameter");  	
	//	
	//Iterator = 0;
	//For Each Parameter In EditedParametersOnPage Do		
	//		
	//	FilterParameters = New Structure("ID", Parameter.id);
	//	String = EditableParameters.FindRows(FilterParameters);
	//	If String.Count() > 0 Then 
	//		RowIndex = EditableParameters.IndexOf(String[0]);
	//		Shift = Iterator - RowIndex;
	//		If Shift <> 0 Then 
	//			EditableParameters.Move(RowIndex, Shift);
	//		EndIf;
	//	EndIf;
	//	Iterator = Iterator + 1;
	//	
	//EndDo;

	DocumentCreated = True;

EndProcedure

//&AtServer
//Procedure ac_InformationRegister_AbonDataBeforeAddRowAfterAtServer()
//	
//	//InformationRegister_AbonDataContract = Object.Ref;
//	
//	//NewFilterElement = Список.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
//	
//	
//	//////NewFilterElement = List.Filter.Items.Add(Type("DataCompositionFilterItem")); 
//	
//	
//	//DynamicList
//	//NewFilterElement.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("_наименование_поля_компоновки_"); 
//	//NewFilterElement.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно; // больше или меньше и т.п.
//	//NewFilterElement.Использование = Истина;
//	//NewFilterElement.ПравоеЗначение = Значение; // с чем сравниваем
//	
//	
//	
//   // OwnerField = New DataCompositionField("Owner");
//   //For Each FilterItem In DictionaryDiscussions.SettingsComposer.Settings.Filter.Items Do
//   //   If FilterItem.ViewMode = DataCompositionSettingsItemViewMode.Inaccessible 
//   //      And FilterItem.LeftValue = OwnerField
//   //      And FilterItem.Use Then
//   //      FilterItem.RightValue = Object.Ref;
//   //   EndIf;
//   //EndDo;
//	//
//	
//   //  OwnerField = New DataCompositionField("Contract");
//   //For Each FilterItem In DictionaryDiscussions.SettingsComposer.Settings.Filter.Items Do
//   //   If FilterItem.ViewMode = DataCompositionSettingsItemViewMode.Inaccessible 
//   //      And FilterItem.LeftValue = OwnerField
//   //      And FilterItem.Use Then
//   //      FilterItem.RightValue = Object.Ref;
//   //   EndIf;
//   //EndDo;
//	

//EndProcedure

//&AtClient
//Procedure ac_InformationRegister_AbonDataBeforeAddRowAfter(Item, Cancel, Clone, Parent, Folder, Parameter)
//	
//	//ac_InformationRegister_AbonDataBeforeAddRowAfterAtServer();
//	
//	//NewFilterElement = List.Filter.Items.Add(Type("DataCompositionFilterItem"));
//	
//	
//	 OwnerField = New DataCompositionField("Contract");
//   For Each FilterItem In DictionaryDiscussions.SettingsComposer.Settings.Filter.Items Do
//      If FilterItem.ViewMode = DataCompositionSettingsItemViewMode.Inaccessible 
//         And FilterItem.LeftValue = OwnerField
//         And FilterItem.Use Then
//         FilterItem.RightValue = Object.Ref;
//      EndIf;
//   EndDo;

//	
//	
//EndProcedure

