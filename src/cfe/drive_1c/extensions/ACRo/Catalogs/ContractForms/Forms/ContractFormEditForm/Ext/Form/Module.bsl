﻿
&AtClient
Procedure InsertEditableTermen(Command)
	
	InsertParameter("%Parameter%");
	
	If FormattedDocument.FindText("%Parameter%") <> Undefined Then
		
		EditableTermen = InsertEditableTermenAtServer();
		
		BeginningBookmark = FormattedDocument.FindText("%Parameter%").BeginBookmark;
		EndBookmark = FormattedDocument.FindText("%Parameter%").EndBookmark;
		
		BeginningPosition = FormattedDocument.GetBookmarkPosition(BeginningBookmark);
		
		EndPosition = BeginningPosition + StrLen("{Period}") + 1;
		
		FormattedDocument.Delete(BeginningBookmark, EndBookmark);
		FormattedDocument.Insert(BeginningBookmark, EditableTermen);
		
		EndBookmark = FormattedDocument.GetPositionBookmark(EndPosition);
		Items.FormattedDocument.SetTextSelectionBounds(BeginningBookmark, EndBookmark);
	EndIf;
	
EndProcedure

&AtServer
Function InsertEditableTermenAtServer()
	
	For ParameterNumber = 0 To Object.EditableParameters.Count() Do
		//Presentation = "{Period" + (ParameterNumber + 1) + "}";    
		Presentation = "{Termen" + (ParameterNumber + 1) + "}";
				
		If FormattedDocument.FindText(Presentation) <> Undefined Then
			Continue;
		EndIf;
		
		ID = "parameterPeriod" + (ParameterNumber + 1);
		
		If Object.EditableParameters.FindRows(New Structure("ID", ID)).Count() <> 0 Then
			Break;
		Else
			NewRow = Object.EditableParameters.Add();
			NewRow.Presentation = Presentation;
			NewRow.ID = ID;
			Break;
		EndIf;
	EndDo;
	
	Return Presentation;
	
EndFunction

&AtServer
Function InsertProductNameAtServer()
	
	For ParameterNumber = 0 To Object.EditableParameters.Count() Do
		Presentation = "{ProductName" + (ParameterNumber + 1) + "}";
		
		If FormattedDocument.FindText(Presentation) <> Undefined Then
			Continue;
		EndIf;
		
		ID = "parameterProductName" + (ParameterNumber + 1);
		
		If Object.EditableParameters.FindRows(New Structure("ID", ID)).Count() <> 0 Then
			Break;
		Else
			NewRow = Object.EditableParameters.Add();
			NewRow.Presentation = Presentation;
			NewRow.ID = ID;
			Break;
		EndIf;
	EndDo;
	
	Return Presentation;
EndFunction

&AtClient
Procedure InsertProductName(Command)

	InsertParameter("%Parameter%");
	
	If FormattedDocument.FindText("%Parameter%") <> Undefined Then
		
		ProductName = InsertProductNameAtServer();
		
		BeginningBookmark = FormattedDocument.FindText("%Parameter%").BeginBookmark;
		EndBookmark = FormattedDocument.FindText("%Parameter%").EndBookmark;
		
		BeginningPosition = FormattedDocument.GetBookmarkPosition(BeginningBookmark);
		
		EndPosition = BeginningPosition + StrLen("{ProductName}") + 1;
		
		FormattedDocument.Delete(BeginningBookmark, EndBookmark);
		FormattedDocument.Insert(BeginningBookmark, ProductName);
		
		EndBookmark = FormattedDocument.GetPositionBookmark(EndPosition);
		Items.FormattedDocument.SetTextSelectionBounds(BeginningBookmark, EndBookmark);
	EndIf;
	
EndProcedure

&AtClient
Procedure ac_InsertAbonProductNameAround(Command)
	
	ac_InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.AbonProductName"));
	
EndProcedure

&AtClient
Procedure ac_InsertProductNameFromCatalogAround(Command)
	
	ac_InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.ProductNameFromCatalog"));

	//OpenForm("Catalog.Products.ChoiceForm",,,,,,New NotifyDescription("CatalogProductsChoiceFormOpeningBeforeEnd", ThisForm), FormWindowOpeningMode.LockWholeInterface);
	             	
EndProcedure

//&AtServer
//Procedure CatalogProductsChoiceFormOpeningBeforeEnd(Result, AdditionalParameters) Export
//	
//	If Result = Undefined Then
//		Return;
//	EndIf;
//	
//	Document = Undefined;
//	
//	Parameter = Enums.ac_ContractsWithCounterpartiesTemplatesParameters.ProductNameFromCatalog;	
//	
//	DriveCreationOfPrintedFormsOfContract.GetParameterValue(Object, Document, Parameter , Result);
//	
//	
//EndProcedure   

&AtClient
Procedure ac_InsertCompanyHeadPositionAround(Command)
	
	ac_InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.CompanyContactPersonPosition"));
	
EndProcedure

&AtClient
Procedure ac_InsertContractPriceHourAround(Command)
	
	ac_InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.ContractPriceHour"));
	
EndProcedure

&AtClient
Procedure ac_InsertAbonContractPriceAround(Command)
	
	ac_InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.AbonContractPrice"));
	
EndProcedure

&AtClient
Procedure ac_InsertCompanyPostalAddressAround(Command)
	InsertInfobaseParameter(PredefinedValue("Enum.ContractsWithCounterpartiesTemplatesParameters.CompanyPostalAddress"));
EndProcedure

&AtClient
Procedure ac_InsertCompanyRegistrationNumberAround(Command)
	//InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.CompanyRegistrationNumber"));
	ac_InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.CompanyRegistrationNumber"));
EndProcedure

&AtClient
Procedure ac_InsertCounterpartyNameAround(Command)
	InsertInfobaseParameter(PredefinedValue("Enum.ContractsWithCounterpartiesTemplatesParameters.CompanyCounterpartyName"));
	//InsertInfobaseParameter(PredefinedValue("Enum.ContractsWithCounterpartiesTemplatesParameters.CompanyName"));
EndProcedure

&AtClient
Procedure ac_InsertCounterpartyRegistrationNumberAround(Command)
	ac_InsertInfobaseParameter(PredefinedValue("Enum.ac_ContractsWithCounterpartiesTemplatesParameters.CounterpartyRegistrationNumber"));
EndProcedure

&AtClient
Procedure ac_InsertCompanyFaxAround(Command)
	InsertInfobaseParameter(PredefinedValue("Enum.ContractsWithCounterpartiesTemplatesParameters.CompanyFax"));
EndProcedure

//&AtServer
//&Around("InfobaseParameter")
//Function ac_InfobaseParameter(Parameter, Case)
//	// Insert method content.
//	//If TypeOf(Parameter) <> Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
//	//	Return ProceedWithCall(Parameter, Case);  	
//	//	
//	//EndIf;
//	
//	If TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
//		
//	SimilarParameters = Object.InfobaseParameters.FindRows(New Structure("Parameter", Parameter));
//	For ParameterNumber = 0 To SimilarParameters.Count() Do
//		
//		If TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
//			If ParameterNumber = 0 Then
//				Presentation = "{" + Parameter;
//			Else
//				Presentation = "{" + Parameter + (ParameterNumber + 1);
//			EndIf; 			
//		EndIf;
//		
//		If FormattedDocument.FindText(Presentation) <> Undefined Then
//			Continue;
//		EndIf;
//		
//		If Case <> Undefined Then
//			CasePresentation = " (" + Case + ")";
//		Else
//			CasePresentation = "";
//		EndIf;
//		
//		If TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
//			Presentation = Presentation + CasePresentation + "}";
//			ID = "infoParameter" + Parameter + (ParameterNumber + 1);
//			
//			If Object.InfobaseParameters.FindRows(New Structure("ID", ID)).Count() <> 0 Then
//				Break;
//			Else
//				NewRow = Object.InfobaseParameters.Add();
//				NewRow.Presentation = Presentation;
//				NewRow.ID = ID;
//				NewRow.Parameter = Parameter;
//				Break;
//			EndIf;
//		EndIf;
//	EndDo;
//	
//	Return Presentation;
//	
//	Else
//	
//	Return ProceedWithCall(Parameter, Case);
//	
//	EndIf;
//		
//	//Return Result;
//EndFunction

&AtClient
&Around("InsertInfobaseParameter")
Procedure ac_InsertInfobaseParameter(InfobaseParameter, Case = Undefined)

	InsertParameter("%Parameter%");
	
	If FormattedDocument.FindText("%Parameter%") <> Undefined Then
		
		Parameter = ac_InfobaseParameter(InfobaseParameter, Case);
		
		BeginningBookmark = FormattedDocument.FindText("%Parameter%").BeginBookmark;
		EndBookmark = FormattedDocument.FindText("%Parameter%").EndBookmark;
		
		BeginningPosition = FormattedDocument.GetBookmarkPosition(BeginningBookmark);
		EndPosition = BeginningPosition + StrLen(Parameter);
		
		FormattedDocument.Delete(BeginningBookmark, EndBookmark);
		FormattedDocument.Insert(BeginningBookmark, Parameter);
		
		EndBookmark = FormattedDocument.GetPositionBookmark(EndPosition);
		Items.FormattedDocument.SetTextSelectionBounds(BeginningBookmark, EndBookmark);
	EndIf;
	
EndProcedure

&AtServer
&Around("InfobaseParameter")
Function ac_InfobaseParameter(Parameter, Case)

	If TypeOf(Parameter) = Type("EnumRef.ContractsWithCounterpartiesTemplatesParameters")Then
		
	         SimilarParameters = Object.InfobaseParameters.FindRows(New Structure("Parameter", Parameter));
			 
			 ElsIf TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
				 
	         SimilarParameters = Object.InfobaseParameters.FindRows(New Structure("ac_Parameter", Parameter));
			 
	EndIf;
			 
	//SimilarParameters = Object.InfobaseParameters.FindRows(New Structure("Parameter", Parameter));
	//SimilarParameters = Object.InfobaseParameters.FindRows(New Structure("ac_Parameter", Parameter));
	For ParameterNumber = 0 To SimilarParameters.Count() Do
		
		If TypeOf(Parameter) = Type("EnumRef.ContractsWithCounterpartiesTemplatesParameters")
			OR TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
			If ParameterNumber = 0 Then
				Presentation = "{" + Parameter;
			Else
				Presentation = "{" + Parameter + (ParameterNumber + 1);
			EndIf;
		ElsIf TypeOf(Parameter) = Type("ChartOfCharacteristicTypesRef.AdditionalAttributesAndInfo") Then
			If ParameterNumber = 0 Then
				Presentation = "{" + Parameter.Description;
			Else
				Presentation = "{" + Parameter.Description + (ParameterNumber + 1);
			EndIf;
		EndIf;
		
		If FormattedDocument.FindText(Presentation) <> Undefined
			
			//AND
			OR
			FormattedDocument.FindText(Presentation) <> Undefined AND
			TypeOf(FormattedDocument.FindText(Presentation)) <> Type("FormattedDocumentRange") Then
			//Parameter = Object.InfobaseParameters.ac_Parameter;
			Continue;
		EndIf;
		                                                       
		If Case <> Undefined Then
			CasePresentation = " (" + Case + ")";
		Else
			CasePresentation = "";
		EndIf;
		
		If TypeOf(Parameter) = Type("EnumRef.ContractsWithCounterpartiesTemplatesParameters")
			OR TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters")	Then
			Presentation = Presentation + CasePresentation + "}";
			ID = "infoParameter" + Parameter + (ParameterNumber + 1);
		ElsIf TypeOf(Parameter) = Type("ChartOfCharacteristicTypesRef.AdditionalAttributesAndInfo") Then
			ParameterNamePresentation = StrReplace(Parameter.Description, " ", "");
			ParameterNamePresentation = StrReplace(ParameterNamePresentation, "(", "");
			ParameterNamePresentation = StrReplace(ParameterNamePresentation, ")", "");
			
			Presentation = Presentation + "}";
			ID = "additionalParameter" + ParameterNamePresentation + (ParameterNumber + 1);
		EndIf;
		
		If Object.InfobaseParameters.FindRows(New Structure("ID", ID)).Count() <> 0 Then
			Break;
		Else
			NewRow = Object.InfobaseParameters.Add();
			NewRow.Presentation = Presentation;
			NewRow.ID = ID;
			NewRow.Parameter = Parameter;
			NewRow.ac_Parameter = Parameter;
			Break;
		EndIf;
	EndDo;
	
	If not StrEndsWith(Presentation, "}") Then
		Presentation = Presentation + "}"
	EndIf;
	
	Return Presentation;

	//If TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
	//	
	//SimilarParameters = Object.InfobaseParameters.FindRows(New Structure("Parameter", Parameter));
	//For ParameterNumber = 0 To SimilarParameters.Count() Do
	//	
	//	If TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
	//		If ParameterNumber = 0 Then
	//			Presentation = "{" + Parameter;
	//		Else
	//			Presentation = "{" + Parameter + (ParameterNumber + 1);
	//		EndIf; 			
	//	EndIf;
	//	
	//	If FormattedDocument.FindText(Presentation) <> Undefined Then
	//		Continue;
	//	EndIf;
	//	
	//	If Case <> Undefined Then
	//		CasePresentation = " (" + Case + ")";
	//	Else
	//		CasePresentation = "";
	//	EndIf;
	//	
	//	If TypeOf(Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
	//		Presentation = Presentation + CasePresentation + "}";
	//		ID = "infoParameter" + Parameter + (ParameterNumber + 1);
	//		
	//		If Object.InfobaseParameters.FindRows(New Structure("ID", ID)).Count() <> 0 Then
	//			Break;
	//		Else
	//			NewRow = Object.InfobaseParameters.Add();
	//			NewRow.Presentation = Presentation;
	//			NewRow.ID = ID;
	//			NewRow.Parameter = Parameter;
	//			Break;
	//		EndIf;
	//	EndIf;
	//EndDo;
	//
	//Return Presentation;
	//
	//Else
	//
	////Return ProceedWithCall(Parameter, Case);
	//Result = ProceedWithCall(Parameter, Case);
	//Return Result;

	//EndIf;
	
EndFunction

&AtClient
Procedure ac_OnCloseAround(Exit)
	FormText = "";
	FormattedDocument.GetHTML(FormText, New Structure());
	If FormText <> FormTextAtOpening Then
		Notify("ContractTemplateChangeAndRecordAtServer", Object.Ref);
	EndIf;
EndProcedure

