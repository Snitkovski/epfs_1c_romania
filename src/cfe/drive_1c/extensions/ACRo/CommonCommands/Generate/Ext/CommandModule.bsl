﻿
&AtClient
Procedure CommandProcessing(CommandParameter, CommandExecuteParameters)
	StrData = new Structure("Base", CommandParameter);
	FormParameters = New Structure("FillingValues", StrData);
	OpenForm("Document.SalesOrder.ObjectForm", FormParameters, CommandExecuteParameters.Source, CommandExecuteParameters.Uniqueness, CommandExecuteParameters.Window, CommandExecuteParameters.URL);

EndProcedure
