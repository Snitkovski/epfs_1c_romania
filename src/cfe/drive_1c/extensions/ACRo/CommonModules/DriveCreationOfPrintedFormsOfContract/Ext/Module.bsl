﻿
&Around("GetParameterValue")
Function ac_GetParameterValue(Object, Document, Parameter, PresentationParameter)Export

	//Function GetParameterValue(Object, Document = Undefined, Parameter, PresentationParameter = Undefined) Export
	
	Parameters = Enums.ContractsWithCounterpartiesTemplatesParameters;
	ac_Parameters = Enums.ac_ContractsWithCounterpartiesTemplatesParameters;
	
	If Document <> Undefined Then
		Company = Document.Company;
	Else
		If DriveReUse.CounterpartyContractsControlNeeded()
			AND ValueIsFilled(Object.Company) Then
			Company = Object.Company;
		ElsIf ValueIsFilled(DriveReUse.GetValueOfSetting("MainCompany")) Then
			Company = DriveReUse.GetValueOfSetting("MainCompany");
		Else
			Company = Catalogs.Companies.MainCompany;
		EndIf;
	EndIf;
	
	//If Parameter = Parameters.ProductNameAnnex Then
	//	Query = New Query;
	//	Query.Text = 
	//	
	//	  "SELECT TOP 1
	//	  |	SalesOrder.Ref AS Ref
	//	  |INTO TemporaryTable1
	//	  |FROM
	//	  |	Document.SalesOrder AS SalesOrder
	//	  |		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	//	  |		ON SalesOrder.Contract = CounterpartyContracts.Ref
	//	  |WHERE
	//	  |	SalesOrder.Company = &Company
	//	  |	AND SalesOrder.Contract.Owner = &Owner
	//	  |
	//	  |ORDER BY
	//	  |	SalesOrder.Date
	//	  |;
	//	  |
	//	  |////////////////////////////////////////////////////////////////////////////////
	//	  |SELECT
	//	  |	SalesOrderInventory.Characteristic AS Characteristic,
	//	  |	SalesOrderInventory.Quantity AS Quantity,
	//	  |	SalesOrderInventory.Price AS Price,
	//	  |	SalesOrderInventory.Amount AS Amount
	//	  |FROM
	//	  |	Document.SalesOrder.Inventory AS SalesOrderInventory";  		
	//	
	//			
	//	Query.SetParameter("Company",Object.Company);
	//	Query.SetParameter("Counterparty",Object.Owner);
	//	
	//	QuerySelect = Query.Execute().Select();
	//	
	//	While QuerySelect.Next() Do
	//		
	//		ParameterValue = ?(QuerySelect.Characteristic.Description <> "", QuerySelect.Characteristic.Description, "");
	//	EndDo;	
	//EndIf;
	
	If Parameter = Parameters.BankCompany Then
		
		ParameterValue = ?(Company.BankAccountByDefault.Bank.Description <> "",
		Company.BankAccountByDefault.Bank.Description, "");
		
	ElsIf Parameter = Parameters.CounterpartyBank Then
		
		ParameterValue = ?(Object.Owner.BankAccountByDefault.Bank.Description <> "",
		Object.Owner.BankAccountByDefault.Bank.Description, "");
		
	ElsIf Parameter = Parameters.Date Then
		
		ParameterValue = ?(Format(Object.ContractDate, "DLF=D") <> "",
		Format(Object.ContractDate, "DLF=D"), "");
		
	ElsIf Parameter = Parameters.DocumentNumber Then
		
		ParameterValue = ?(Object.ContractNo <> "", Object.ContractNo, "");
		
	ElsIf Parameter = Parameters.DocumentAmount Then
		
		ParameterValue = ?(Object.Amount <> "", Object.Amount, "");
		
	ElsIf Parameter = Parameters.PositionOfContactPersonOfCounterparty Then
		
		If ValueIsFilled(Object.Owner.ContactPerson.Position) Then
			ParameterValue = Object.Owner.ContactPerson.Position;
		Else
			If Object.Owner.ContactPerson.ContactPersonRoles.Count() <> 0 Then
				ParameterValue = ?(Object.Owner.ContactPerson.ContactPersonRoles[0].ContactPersonRole.Description <> "",
				Object.Owner.ContactPerson.ContactPersonRoles[0].ContactPersonRole.Description, "");
			Else
				ParameterValue = "";
			EndIf;
		EndIf;
		
	ElsIf Parameter = Parameters.ContactPersonOfCounterparty Then
		
		ParameterValue = ?(ValueIsFilled(Object.Owner.ContactPerson),
		Object.Owner.ContactPerson.Description, "");
		
	ElsIf Parameter = Parameters.ContactPersonOfCounterpartyInitials Then
		
		ContactPerson = Object.Owner.ContactPerson;
		If Not ValueIsFilled(ContactPerson) Then
			Return "";
		EndIf;
		
		NameParts	= IndividualsClientServer.NameParts(ContactPerson.Description);
		Surname		= NameParts.Surname;
		
		Case	= CaseParameter(PresentationParameter);
		DeclineValue(Parameter, Surname, Case);
		Initials	= DriveServer.GetSurnameNamePatronymic(Surname, NameParts.Name, NameParts.Patronymic, True);
		
		Return Initials;
		
	ElsIf Parameter = Parameters.CompanyName Then
		
		ParameterValue = ?(Company.DescriptionFull <> "",
		Company.DescriptionFull, "");
		
	ElsIf Parameter = Parameters.CompanyCounterpartyName Then
		
		ParameterValue = ?(Object.Owner.DescriptionFull <> "",
		Object.Owner.DescriptionFull, "");
		
	ElsIf Parameter = Parameters.ContractNo Then
		
		ParameterValue = ?(Object.ContractNo <> "",
		Object.ContractNo, "");
		
	ElsIf Parameter = Parameters.CompanyBankAcc Then
		
		ParameterValue = ?(Company.BankAccountByDefault.AccountNo <> "",
		Company.BankAccountByDefault.AccountNo, "");
		
	ElsIf Parameter = Parameters.CounterpartyBankAcc Then
		
		ParameterValue = ?(Object.Owner.BankAccountByDefault.AccountNo <> "",
		Object.Owner.BankAccountByDefault.AccountNo, "");
		
	ElsIf Parameter = Parameters.CompanyLegalAddress Then
		
		ParameterValue = "";
		For Each String In Company.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CompanyLegalAddress Then
				ParameterValue = String.Presentation;
				
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CompanyActualAddress Then
		
		ParameterValue = "";
		For Each String In Company.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CompanyActualAddress Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CompanyPhone Then
		
		ParameterValue = "";
		For Each String In Company.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CompanyPhone Then
				
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CompanyFax Then
		
		ParameterValue = "";
		For Each String In Company.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CompanyFax Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CompanyEmailAddress Then
		
		ParameterValue = "";
		For Each String In Company.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CompanyEmail Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CompanyPostalAddress Then
		
		ParameterValue = "";
		For Each String In Company.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CompanyPostalAddress Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CounterpartyLegalAddress Then
		
		ParameterValue = "";
		For Each String In Object.Owner.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CounterpartyLegalAddress Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CounterpartyFactAddress Then
		
		ParameterValue = "";
		For Each String In Object.Owner.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CounterpartyActualAddress Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CounterpartyPhone Then
		
		ParameterValue = "";
		For Each String In Object.Owner.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CounterpartyPhone Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CounterpartyFax Then
		
		ParameterValue = "";
		For Each String In Object.Owner.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CounterpartyFax Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CounterpartyEMailAddress Then
		
		ParameterValue = "";
		For Each String In Object.Owner.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CounterpartyEmail Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CounterpartyPostalAddress Then
		
		ParameterValue = "";
		For Each String In Object.Owner.ContactInformation Do
			
			If String.Kind = Catalogs.ContactInformationKinds.CounterpartyPostalAddress Then
				ParameterValue = String.Presentation;
			EndIf;
		EndDo;
		ParameterValue = ?(ParameterValue <> "", ParameterValue, "");
		
	ElsIf Parameter = Parameters.CounterpartyTIN Then
		
		ParameterValue = ?(Object.Owner.TIN <> "",
		Object.Owner.TIN, "");
		
	ElsIf Parameter = Parameters.CompanyTIN Then
		
		ParameterValue = ?(Company.TIN <> "",
		Company.TIN, "");
		
	ElsIf Parameter = ac_Parameters.CounterpartyRegistrationNumber Then

		ParameterValue = ?(Object.Owner.RegistrationNumber <> "",
		TrimAll(Object.Owner.RegistrationNumber), "");
		
	ElsIf Parameter = ac_Parameters.CompanyRegistrationNumber Then
		
		ParameterValue = ?(Company.RegistrationNumber <> "",
		TrimAll(Company.RegistrationNumber), "");

	ElsIf Parameter = Parameters.CompanyHead Then
		
		ResponsiblePersons = DriveServer.OrganizationalUnitsResponsiblePersons(Company, CurrentDate());
		If Not ValueIsFilled(ResponsiblePersons.Head) Then
			Return "";
		EndIf;
		ParameterValue = ?(ValueIsFilled(ResponsiblePersons.Head.Ind),
		ResponsiblePersons.Head.Ind.Description, "");
		
	ElsIf Parameter = Parameters.CompanyHeadInitials Then
		
		ResponsiblePersons = DriveServer.OrganizationalUnitsResponsiblePersons(Company, CurrentDate());
		If Not ValueIsFilled(ResponsiblePersons.Head) Then
			Return "";
		EndIf;
		CompanyHead = ResponsiblePersons.Head.Ind;
		
		Selection = InformationRegisters.ChangeHistoryOfIndividualNames.Select(,, New Structure("Ind", CompanyHead));
		If Selection.Next() Then
			Name = Selection.Name;
			Patronymic = Selection.Patronymic;
			Surname = Selection.Surname;
		Else
			Return "";
		EndIf;
		
		Case = CaseParameter(PresentationParameter);
		DeclineValue(Parameter, Surname, Case);
		Initials = Surname + " " + Mid(Name, 1, 1) + ". " + Mid(Patronymic, 1, 1) + ".";
		
		Return Initials;
		
	ElsIf Parameter = ac_Parameters.CompanyContactPersonPosition Then
	
		ResponsiblePersons = DriveServer.OrganizationalUnitsResponsiblePersons(Company, CurrentDate());
		
		ParameterValue = ?(ValueIsFilled(ResponsiblePersons.HeadPositionRefs),
		ResponsiblePersons.HeadPosition, "");
		
	ElsIf Parameter = ac_Parameters.ContractPriceHour Then
		
		Query = New Query;
		Query.Text =
		"SELECT
		|	CounterpartyContracts.Rate AS Rate
		|FROM
		|	Catalog.CounterpartyContracts AS CounterpartyContracts
		|WHERE
		|	CounterpartyContracts.Owner = &Owner";
		
		Query.SetParameter("Owner", Object.Owner);
		
		QueryResult = Query.Execute();
		
		SelectionDetailRecords = QueryResult.Select();
		
		While SelectionDetailRecords.Next() Do
			
			Rate = SelectionDetailRecords.Rate;
			
		EndDo;
		
		Return Rate;

	ElsIf Parameter = ac_Parameters.AbonContractPrice Then
		
		Query = New Query;
		Query.Text =
		
		"SELECT
		|	ISNULL(ac_AbonDataSliceLast.Price, 0) AS Price
		|FROM
		|	InformationRegister.ac_AbonData.SliceLast(
		|			&Date,
		|			Contract = &Contract
		|				) AS ac_AbonDataSliceLast";
		
		//Query.SetParameter("Product", Product);
		Query.SetParameter("Date", CurrentDate());
		Query.SetParameter("Contract",Object.Ref);
		
		QueryResult = Query.Execute();
		Selection = QueryResult.Select();
		
		While Selection.Next() Do
			
			Price = Selection.Price;
			
		EndDo;
		
		Return  Price;
		
	ElsIf Parameter = ac_Parameters.AbonProductName Then
		
		Query = New Query;
		Query.Text =
		
		"SELECT
		|	ac_AbonDataSliceLast.Products AS Product,
		|	ac_AbonDataSliceLast.Products.Presentation AS ProductsPresentation
		|FROM
		|	InformationRegister.ac_AbonData.SliceLast(&Date, Contract = &Contract) AS ac_AbonDataSliceLast";

		Query.SetParameter("Date", CurrentDate());
		Query.SetParameter("Contract",Object.Ref);
		
		QueryResult = Query.Execute();
		Selection = QueryResult.Select();
		
		While Selection.Next() Do
			
			//AbonProductName = Selection.Product;
			AbonProductName = Selection.ProductsPresentation;
			
		EndDo;
		
		Return  AbonProductName;
		
	ElsIf Parameter = ac_Parameters.ProductNameFromCatalog Then
		
		Query = New Query;
		Query.Text =
		"SELECT
		|	CounterpartyContracts.ContractMainProduct AS ContractMainProduct
		|FROM
		|	Catalog.CounterpartyContracts AS CounterpartyContracts
		|WHERE
		|	CounterpartyContracts.Owner = &Owner
		|	AND CounterpartyContracts.Ref = &Contract";
		
		Query.SetParameter("Owner", Object.Owner);
		Query.SetParameter("Contract",Object.Ref);
		
		QueryResult = Query.Execute();
		
		SelectionDetailRecords = QueryResult.Select();
		
		While SelectionDetailRecords.Next() Do
			
			ContractMainProduct = SelectionDetailRecords.ContractMainProduct;
			
		EndDo;
		
		Return ContractMainProduct;

	ElsIf Parameter = Parameters.CounterpartyBIK Then
		
		ParameterValue = ?(ValueIsFilled(Object.Owner.BankAccountByDefault.Bank.Code),
		Object.Owner.BankAccountByDefault.Bank.Code, "");
		
	ElsIf Parameter = Parameters.CompanyBIK Then
		
		ParameterValue = ?(ValueIsFilled(Company.BankAccountByDefault.Bank.Code),
		Company.BankAccountByDefault.Bank.Code, "");

	ElsIf Parameter = Parameters.DocumentDate Then
		
		If Document <> Undefined Then
			ParameterValue = Format(Document.Date, "DLF=D");
		Else
			ParameterValue = "";
		EndIf;
		
	ElsIf Parameter = "PassportData" Then
		
		Query = New Query;
		Query.Text =
		
		"SELECT
		|	LegalDocuments.Number,
		|	LegalDocuments.IssueDate,
		|	LegalDocuments.ExpiryDate,
		|	LegalDocuments.Authority
		|FROM
		|	Catalog.LegalDocuments AS LegalDocuments
		|WHERE
		|	LegalDocuments.DocumentKind.Ref = &DocumentKind
		|	AND LegalDocuments.Owner = &Owner";
		
		Query.SetParameter("DocumentKind", Catalogs.DocumentTypes.IdentityDocument);
		Query.SetParameter("Owner", Object.Owner);
		
		QueryResult = Query.Execute();
		SelectionDetailRecords = QueryResult.Select();
		PassportData = New Structure;
		PassportData.Insert("Number",			"");
		PassportData.Insert("IssueDate",		"");
		PassportData.Insert("ValidityPeriod",	"");
		//PassportData.Insert("WhoIssued",		"");
		PassportData.Insert("IssuedBy",		"");
		
		While SelectionDetailRecords.Next() Do
			If ValueIsFilled(SelectionDetailRecords.Number) Then
				PassportData.Number = SelectionDetailRecords.Number;
			EndIf;
			If ValueIsFilled(SelectionDetailRecords.IssueDate) Then
				PassportData.IssueDate = Format(SelectionDetailRecords.IssueDate, "DLF=D");
			EndIf;
			If ValueIsFilled(SelectionDetailRecords.ExpiryDate) Then
				PassportData.ValidityPeriod = Format(SelectionDetailRecords.ExpiryDate, "DLF=D");
			EndIf;
			//If ValueIsFilled(SelectionDetailRecords.Authority) Then 
			//	PassportData.WhoIssued = SelectionDetailRecords.Authority;
			//EndIf;
			
			If ValueIsFilled(SelectionDetailRecords.Authority) Then
				PassportData.IssuedBy = SelectionDetailRecords.Authority;
			EndIf;
		EndDo;
		
		Return PassportData;
		
	ElsIf Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_IssueDate
		OR Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_Authority
		OR Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_Number
		OR Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_ExpiryDate Then
		
		PassportDataAttribute = Mid(Parameter, StrLen("PassportData_") + 1, StrLen(Parameter) - StrLen("PassportData_"));
		Query = New Query;
		Query.Text =
		"SELECT
		|	LegalDocuments." + PassportDataAttribute + "
		|FROM
		|	Catalog.LegalDocuments AS LegalDocuments
		|WHERE LegalDocuments.Individual.Ref = &Owner
		|	AND LegalDocuments.DocumentKind = &DocumentKind";
		
		Query.SetParameter("DocumentKind",	Catalogs.DocumentTypes.IdentityDocument);
		Query.SetParameter("Owner",			Object.Owner);
		
		QueryResult = Query.Execute();
		SelectionDetailRecords = QueryResult.Select();
		
		If SelectionDetailRecords.Next() Then
			If TypeOf(SelectionDetailRecords[PassportDataAttribute]) = Type("Date") Then
				Return Format(SelectionDetailRecords[PassportDataAttribute], "DLF=D");
			Else
				Return SelectionDetailRecords[PassportDataAttribute];
			EndIf;
		EndIf;
		
	ElsIf Parameter = Parameters.Facsimile Then
		If ValueIsFilled(Company.FileFacsimilePrinting) Then
			PictureData = AttachedFiles.GetBinaryFileData(Company.FileFacsimilePrinting);
			If ValueIsFilled(PictureData) Then
				Return "<img src=""data:image/png;base64," + Base64String(PictureData) + """>";
			EndIf;
		EndIf;
		
		Return StringFunctionsClientServer.SubstituteParametersToString("<span style='background-color: #DCDCDC'>%1</span>",
		NStr("en = 'Facsimile is not set for company'; ru = 'Не указано факсимиле для компании';tr = 'Şirket için faks  ayarlanmamış';ro = 'Duplicarea nu este setată pentru companie';pl = 'Nie ustawiono faksymile dla firmy';de = 'Faksimile ist nicht für Firma festgelegt';es_ES = 'Facsímil no está establecido para la empresa'"));
		
	ElsIf Parameter = Parameters.Logo Then
		If ValueIsFilled(Company.LogoFile) Then
			PictureData = AttachedFiles.GetBinaryFileData(Company.LogoFile);
			If ValueIsFilled(PictureData) Then
				Return "<img src=""data:image/png;base64," + Base64String(PictureData) + """>";
			EndIf;
		EndIf;
		
		Return StringFunctionsClientServer.SubstituteParametersToString("<span style='background-color: #DCDCDC'>%1</span>",
		NStr("en = 'Logo is not set for company'; ru = 'Не указан логотип компании';tr = 'Şirket için logo ayarlanmadı';ro = 'Logo-ul nu este setat pentru companie';pl = 'Nie ustawiono logo dla firmy';de = 'Logo ist nicht für Firma festgelegt';es_ES = 'Logotipo no está establecido para la empresa'"));
		
	EndIf;
	
	Case = CaseParameter(PresentationParameter);
	If Case = Undefined OR Not ValueIsFilled(ParameterValue) Then
		Return ParameterValue;
	EndIf;
	
	DeclineValue(Parameter, ParameterValue, Case);
	Return ParameterValue;

	//EndFunction 	

	////////////////////////////////////////////////////////////////////////////////
	//ParameterValue = ProceedWithCall(Object, Document, Parameter, PresentationParameter);

	//////Parameters = Enums.ac_ContractsWithCounterpartiesTemplatesParameters;
	
	//If Document <> Undefined Then
	//	Company = Document.Company;
	//Else
	//	If DriveReUse.CounterpartyContractsControlNeeded()
	//		AND ValueIsFilled(Object.Company) Then
	//		Company = Object.Company;
	//	ElsIf ValueIsFilled(DriveReUse.GetValueOfSetting("MainCompany")) Then
	//		Company = DriveReUse.GetValueOfSetting("MainCompany");
	//	Else
	//		Company = Catalogs.Companies.MainCompany;
	//	EndIf;
	//EndIf; 	   		
	
	////If Parameter = Parameters.CounterpartyRegistrationNumber Then
	////	
	////	ParameterValue = ?(Object.Owner.RegistrationNumber <> "",
	////	Object.Owner.RegistrationNumber, "");
	////	
	////ElsIf Parameter = Parameters.CompanyRegistrationNumber Then
	////	
	////	ParameterValue = ?(Company.RegistrationNumber <> "",
	////	Company.RegistrationNumber, "");
	////EndIf;
	////Return ParameterValue;

	//Return Result;
EndFunction

&Around("GetGeneratedContractHTML")
Function ac_GetGeneratedContractHTML(Object, Document = Undefined, ListOfParameters)Export
	//// Insert method content.
	//Result = ProceedWithCall(Object, Document, ListOfParameters);
	//Return Result;
	
	If Not ValueIsFilled(Object.ContractForm) Then
		ContractHTMLDocument = EmptyDocumentField();
		Return ContractHTMLDocument;
	EndIf;
	
	If Object.ContractForm.Form.Get() = Undefined Then
		Return "";
	EndIf;
	
	ContractHTMLDocument = Object.ContractForm.Form.Get().HTMLText;
	
	If Find(ContractHTMLDocument, "<body>") Then
		ContractHTMLDocument = StrReplace(ContractHTMLDocument, "<body>", "<body style='margin:0;padding:0px;overflow:auto;width:100%;height:100%;'>");
	ElsIf Find(ContractHTMLDocument, "<BODY>") Then
		ContractHTMLDocument = StrReplace(ContractHTMLDocument, "<BODY>", "<BODY style='margin:0;padding:0px;overflow:auto;width:100%;height:100%;'>");
	EndIf;
	
	If Find(ContractHTMLDocument, "http-equiv=""X-UA-Compatible""") Then
		ContractHTMLDocument = StrReplace(ContractHTMLDocument, "http-equiv=""X-UA-Compatible""", "");
	ElsIf Find(ContractHTMLDocument, "http-equiv=""X-UA-Compatible""") Then
		ContractHTMLDocument = StrReplace(ContractHTMLDocument, "http-equiv='X-UA-Compatible'", "");
	EndIf;
	
	CounterpartyPassportData = Undefined;
	
	ParameterType = "";
	For Each Parameter In ListOfParameters Do
		If TypeOf(Parameter.Parameter) = Type("EnumRef.ContractsWithCounterpartiesTemplatesParameters") Then
			If Parameter.Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_IssueDate
				OR Parameter.Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_Authority
				OR Parameter.Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_Number
				OR Parameter.Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.PassportData_ExpiryDate Then
				
				If CounterpartyPassportData = Undefined Then
					CounterpartyPassportData = GetParameterValue(Object, , "PassportData");
					//CounterpartyPassportData = ac_GetParameterValue(Object, , "PassportData");
					
				EndIf;
				
				PassportDataAttribute = Mid(Parameter.Parameter, StrLen("PassportData_") + 1, StrLen(Parameter.Parameter) - StrLen("PassportData_"));
				ParameterValue = CounterpartyPassportData[PassportDataAttribute];
			Else
				ParameterValue = GetParameterValue(Object, Document, Parameter.Parameter, Parameter.Presentation);
				//ParameterValue = ac_GetParameterValue(Object, Document, Parameter.Parameter, Parameter.Presentation);
			EndIf;
			
			If Parameter.Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.Facsimile
				OR Parameter.Parameter = Enums.ContractsWithCounterpartiesTemplatesParameters.Logo Then
				ParameterType = "imageParameter";
			EndIf;
			
			If ValueIsFilled(Parameter.Value) Then
				ParameterValue = Parameter.Value;
			Else
				Parameter.Value = ParameterValue;
			EndIf;
			
			If ValueIsFilled(ParameterValue) Then
				ParameterColor = "#FFFFFF";
				ParameterClass = "Filled";
			Else
				ParameterValue = UnfilledFieldPresentation();
				ParameterColor = "#DCDCDC";
				ParameterClass = "Empty";
			EndIf;
		ElsIf TypeOf(Parameter.Parameter) = Type("EnumRef.ac_ContractsWithCounterpartiesTemplatesParameters") Then
			
			ParameterValue = ac_GetParameterValue(Object, Document, Parameter.Parameter, Parameter.Presentation);
						
			If ValueIsFilled(Parameter.Value) Then
				ParameterValue = Parameter.Value;
			Else
				Parameter.Value = ParameterValue;
			EndIf;
			
			If ValueIsFilled(ParameterValue) Then
				ParameterColor = "#FFFFFF";
				ParameterClass = "Filled";
			Else
				ParameterValue = UnfilledFieldPresentation();
				ParameterColor = "#DCDCDC";
				ParameterClass = "Empty";
			EndIf;
		ElsIf TypeOf(Parameter.Parameter) = Type("ChartOfCharacteristicTypesRef.AdditionalAttributesAndInfo") Then
			
			ParameterValue = GetAdditionalAttributeValue(Object, Document, Parameter.Parameter);
			
			If ValueIsFilled(Parameter.Value) Then
				ParameterValue = Parameter.Value;
			Else
				Parameter.Value = ParameterValue;
			EndIf;
			
			If ValueIsFilled(ParameterValue) Then
				ParameterColor = "#FFFFFF";
				ParameterClass = "Filled";
			Else
				ParameterValue = UnfilledFieldPresentation();
				ParameterColor = "#DCDCDC";
				ParameterClass = "Empty";
			EndIf;
			
		Else
			If ValueIsFilled(Parameter.Value) Then
				ParameterValue = Parameter.Value;
				ParameterColor = "#FFFFFF";
				ParameterClass = "Filled";
			Else
				ParameterValue = UnfilledFieldPresentation();
				ParameterColor = "#DCDCDC";
				ParameterClass = "Empty";
			EndIf;
		EndIf;
		
		HTMLParameter = "<a name='parameterType' id='parameterID' style = 'background-color: parameterColor' class='parameterClass'>parameterValue</a>";
		HTMLParameter = StrReplace(HTMLParameter, "parameterID", Parameter.ID);
		HTMLParameter = StrReplace(HTMLParameter, "parameterValue", ParameterValue);
		HTMLParameter = StrReplace(HTMLParameter, "parameterColor", ParameterColor);
		HTMLParameter = StrReplace(HTMLParameter, "parameterClass", ParameterClass);
		If ValueIsFilled(ParameterType) Then
			HTMLParameter = StrReplace(HTMLParameter, "parameterType", ParameterType);
		Else
			HTMLParameter = StrReplace(HTMLParameter, "parameterType", "parameter");
		EndIf;
			
		ContractHTMLDocument = StrReplace(ContractHTMLDocument, Parameter.Presentation, HTMLParameter);
	EndDo;
	
	ContractHTMLDocument = StrReplace(ContractHTMLDocument, "/*PageBreak*/", "<div style='page-break-after:always'></div>");
	
	Return ContractHTMLDocument;

EndFunction

