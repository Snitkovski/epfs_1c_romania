﻿&AtServer
Function CustomerRate(customer)
	return Customer.ContractByDefault.Rate;
EndFunction

&AtServer
Function CustomerContractByDefault(customer)
	return Customer.ContractByDefault;
EndFunction

&AtServer
Function ContractRate(Contract)
	
	//If ValueIsFilled(Contract.Rate) then
	//	return Contract.Rate;
	//Else	
	//	
	//Query = New Query;
	//Query.Text = 
	//	"SELECT
	//	|	ISNULL(ac_AbonDataSliceLast.Price, 0) AS Price
	//	|FROM
	//	|	InformationRegister.ac_AbonData.SliceLast(&Date, ) AS ac_AbonDataSliceLast
	//	|WHERE
	//	|	ac_AbonDataSliceLast.Contract = &Contract";    		

	//
	//Query.SetParameter("Date", Object.Date);
	//Query.SetParameter("Contract", Contract); 	
	//QueryResult = Query.Execute();	
	//SelectionDetailRecords = QueryResult.Select();
	//
	//While SelectionDetailRecords.Next() Do
	//	return SelectionDetailRecords.Price;
	//EndDo;       	
	//  EndIf;
	
	return Contract.Rate;
	
EndFunction

&AtClient
Procedure ac_WorkKindOnChangeAround(Item)
	
EndProcedure

&AtClient
Procedure ac_WorksWorkKindOnChangeAround(Item)
	
EndProcedure

//&AtClient
//Procedure WorksConsumerOnChange(Item)
//	
//	CurrentRow = Items.Works.CurrentData;
//	
//	If TypeOf(CurrentRow.Customer) = Type("CatalogRef.Counterparties")Then
//		
//		CurrentRow.Price = CustomerRate(CurrentRow.Customer);
//		CalculateDuration(CurrentRow);
//		CalculateAmount(CurrentRow);
//		
//	EndIf;  
//	
//EndProcedure

&AtClient
Procedure WorksOnActivateRow(Item)
	  	
	CurrentRow = Items.Works.CurrentData;
	CurrentRow.Customer = Object.Works[0].Customer;
	Items.WorksConsumer.ReadOnly = True;
	
	CurrentRow.Contract = Object.Works[0].Contract;
	
	//CurrentRow.Price = CustomerRate(CurrentRow.Customer); 	
	
	CurrentRow.Price = Object.Works[0].Price;
	
EndProcedure

&AtClient
Procedure WorksRowConsumerOnChange(Item)
	
	CurrentRow = Items.Works.CurrentData;
	
	If TypeOf(CurrentRow.Customer) = Type("CatalogRef.Counterparties")Then
		
		CurrentRow.Price = CustomerRate(CurrentRow.Customer);
		CurrentRow.Contract = CustomerContractByDefault(CurrentRow.Customer);
		
		CalculateDuration(CurrentRow);
		CalculateAmount(CurrentRow);
		
	EndIf;

EndProcedure

&AtClient
Procedure ac_WorksOnStartEditAround(Item, NewRow, Copy)
	
	If NewRow AND Not Copy Then
		
		CurrentRow = Items.Works.CurrentData;
		
		CurrentRow.Price = ContractRate(CurrentRow.Contract);
		CalculateAmount(CurrentRow);
		
	EndIf;
	
EndProcedure

&AtClient
Procedure ac_WorksContractOnChangeAfter(Item)
	
	CurrentRow = Items.Works.CurrentData;
	
	CurrentRow.Price = ContractRate(CurrentRow.Contract);
	CalculateDuration(CurrentRow);
	CalculateAmount(CurrentRow);
	
EndProcedure

&AtServer
Procedure ac_OnCreateAtServerAfter(Cancel, StandardProcessing)
	
	If Object.Works.Count() = 1 Then
		If Not ValueIsFilled(Object.Works[0].Customer) Then
			Object.Works[0].Customer = Catalogs.Counterparties.EmptyRef();
		EndIf;
	EndIf;
	
EndProcedure

