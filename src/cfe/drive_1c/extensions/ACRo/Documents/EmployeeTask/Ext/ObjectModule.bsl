﻿
&After("Posting")
Procedure ac_Posting(Cancel, PostingMode)

	//register HoursWorked Expense 
	RegisterRecords.HoursWorked.Write = True;
	For each CurrentRowWorks In Works Do
		If ValueIsFilled(CurrentRowWorks.Contract) then
			
			If State = Catalogs.JobAndEventStatuses.Completed
				
				AND OperationKind = Enums.OperationTypesEmployeeTask.External
				AND CurrentRowWorks.Customer.Supplier = FALSE
				AND CurrentRowWorks.Customer.OtherRelationship = FALSE
				AND CurrentRowWorks.Contract.PriceKind <> ValueIsFilled(Catalogs.PriceTypes.EmptyRef()) Then
				
				Record = RegisterRecords.HoursWorked.Add();
				Record.RecordType = AccumulationRecordType.Receipt;
				Record.Period = CurrentRowWorks.Day;
				Record.Company = Company;
				Record.Counterparty = CurrentRowWorks.Customer;
				Record.Contract = CurrentRowWorks.Contract;
				Record.EmployeeTask = Ref;
				Record.Hours = CurrentRowWorks.DurationInHours;
			EndIf;
				
			Else
				Message("Câmpul ""Contract"" nu este completat!");
				Cancel = True;
			EndIf;
			
		EndDo;

EndProcedure
