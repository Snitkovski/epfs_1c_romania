﻿
//&AtClient
//Procedure ac_OnOpenAfter(Cancel)
//	
//	PaymentReceipt = PaymentReceiptExistance();
//	
//	Items.HoursPaidInAdvance.Enabled=False;
//	
//	If  PaymentReceipt=True Then
//		
//		Items.HoursPaidInAdvance.Enabled=True;
//	//else
//	//	 Items.HoursPaidInAdvance.Enabled=False;
//		
//	EndIf;

//	
//EndProcedure

//&AtServer
//Function PaymentReceiptExistance()
//	
//	Query = New Query;
//	Query.Text =
//	
//	  "SELECT
//	  |	PaymentReceipt.Ref AS Ref,
//	  |	PaymentReceipt.BasisDocument.Ref AS BasisDocumentRef
//	  |FROM
//	  |	Document.PaymentReceipt AS PaymentReceipt
//	  |WHERE
//	  |	PaymentReceipt.BasisDocument.Ref = &Ref
//	  |	AND PaymentReceipt.Posted"; 	
//		
//	Query.SetParameter("Ref",Object.Ref); 
//	QuerySelect = Query.Execute().Select();
//	
//	While QuerySelect.Next() Do
//		
//	   PaymentReceipt=QuerySelect.Ref;
//	   SalesOrder=QuerySelect.BasisDocumentRef; 	   
//	       
//   EndDo;
//   If  PaymentReceipt <> Undefined Then
//	
//		 Return  True 
// 	else 
//		 Return False;
//	 
//	EndIf;	 
//EndFunction	

&AtClient
Procedure ac_OnOpenAfter(Cancel)
	
	//Object.HoursPaidInAdvance = GetHoursPaidInAdvance();	
	HoursPaidInAdvanceFormAttribute = GetHoursPaidInAdvance();
	
	If  ValueIsFilled (Object.ac_BasisDocument) Then
		
		Items.BasisDocument.Visible = False;
		Items.ac_BasisDocument.Visible = True;
		
	Else
		Items.ac_BasisDocument.Visible = False;
	EndIf;
	
EndProcedure

&AtServer
Function GetHoursPaidInAdvance()

	//Query = New Query;
	//Query.Text = 
	//	"SELECT
	//	|	HoursPaidInAdvanceTurnovers.HoursTurnover AS HoursTurnover
	//	|FROM
	//	|	AccumulationRegister.HoursPaidInAdvance.Turnovers(, , Auto, ) AS HoursPaidInAdvanceTurnovers
	//	|WHERE
	//	|	HoursPaidInAdvanceTurnovers.Recorder.BasisDocument = &BasisDocument";
	//
	//Query.SetParameter("BasisDocument", Object.Ref);
	//
	//QueryResult = Query.Execute();
	//
	//SelectionDetailRecords = QueryResult.Select();
	//
	//While SelectionDetailRecords.Next() Do
	//	
	//	HoursPaidInAdvance = SelectionDetailRecords.HoursTurnover;
	//	
	//EndDo;
	
	Query = New Query;
	Query.Text =
		"SELECT
		|	HoursPaidInAdvance.Hours AS Hours
		|FROM
		|	AccumulationRegister.HoursPaidInAdvance AS HoursPaidInAdvance
		|WHERE
		|	HoursPaidInAdvance.Recorder.BasisDocument = &BasisDocument";
	
	Query.SetParameter("BasisDocument", Object.Ref);
	
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	HoursPaidInAdvance = 0;
	
	While SelectionDetailRecords.Next() Do
		
		HoursPaidInAdvance = HoursPaidInAdvance + SelectionDetailRecords.Hours;

	EndDo;

	Return  HoursPaidInAdvance

EndFunction

&AtClient
Procedure ac_InventoryCharacteristicOnChangeAround(Item)
	
	TabularSectionRow = Items.Inventory.CurrentData;
	
	StructureData = New Structure;
	StructureData.Insert("Company", 				Object.Company);
	StructureData.Insert("Products", 	TabularSectionRow.Products);
	StructureData.Insert("Characteristic",			TabularSectionRow.Characteristic);
	
	If ValueIsFilled(Object.PriceKind) Then
	
		StructureData.Insert("ProcessingDate", 		Object.Date);
		StructureData.Insert("DocumentCurrency", 	Object.DocumentCurrency);
		StructureData.Insert("AmountIncludesVAT",	Object.AmountIncludesVAT);
		StructureData.Insert("VATRate", 			TabularSectionRow.VATRate);
		StructureData.Insert("Price", 				TabularSectionRow.Price);
		StructureData.Insert("PriceKind",			Object.PriceKind);
		StructureData.Insert("MeasurementUnit", 	TabularSectionRow.MeasurementUnit);
		
	EndIf;
	
	AddGLAccountsToStructure(ThisObject, "Inventory", StructureData);
	StructureData = GetDataCharacteristicOnChange(StructureData);
	
	// Bundles
	If StructureData.IsBundle Then
		
		ReplaceInventoryLineWithBundleData(ThisObject, TabularSectionRow, StructureData);
		ClearCheckboxDiscountsAreCalculatedClient("CalculateAmountInTabularSectionLine", "Amount");
		RecalculateSubtotal();
		// End Bundles
		
		Else If  ValueIsFilled (Object.ac_BasisDocument) Then
			
		Else
			TabularSectionRow.Price			= StructureData.Price;
			TabularSectionRow.Content		= "";
			TabularSectionRow.Specification = StructureData.Specification;
			
			CalculateAmountInTabularSectionLine();
			
			ClearEstimate();
			
			// Bundles
		EndIf;
		
	EndIf;
	// End Bundles 
	
	If String(TabularSectionRow.MeasurementUnit) = "ora" OR String(TabularSectionRow.MeasurementUnit) = "user" then
		
		  Product = TabularSectionRow.Products;
		  Price = GetCounterpartyContractPrice(Contract,Product);
		  
		  //TabularSectionRow.Price = GetCounterpartyContractPrice(Contract);
		  
		  CurrensyRate = GetCurrensyRate();
		  PriceInLei = Price * CurrensyRate;
		  
		  TabularSectionRow.Price = PriceInLei;
		  TabularSectionRow.Amount = PriceInLei * TabularSectionRow.Quantity;
		  
		  VATRate = TabularSectionRow.VATRate;
		  
		  TabularSectionRow.VATAmount = TabularSectionRow.Amount* GetVATRate(VATRate)/100;
		  TabularSectionRow.Total = TabularSectionRow.Amount + TabularSectionRow.VATAmount;
	EndIf;

EndProcedure

&AtClient
Procedure ac_InventoryProductsOnChangeAround(Item)
	
	TabularSectionRow = Items.Inventory.CurrentData;
	
	StructureData = New Structure;
	StructureData.Insert("Company", Object.Company);
	StructureData.Insert("Products", TabularSectionRow.Products);
	StructureData.Insert("Characteristic", TabularSectionRow.Characteristic);
	StructureData.Insert("VATTaxation", Object.VATTaxation);
	
	If ValueIsFilled(Object.PriceKind) Then
		
		StructureData.Insert("ProcessingDate", Object.Date);
		StructureData.Insert("DocumentCurrency", Object.DocumentCurrency);
		StructureData.Insert("AmountIncludesVAT", Object.AmountIncludesVAT);
		StructureData.Insert("PriceKind", Object.PriceKind);
		StructureData.Insert("Factor", 1);
		StructureData.Insert("DiscountMarkupKind", Object.DiscountMarkupKind);
		
	EndIf;
	
	// DiscountCards
	StructureData.Insert("DiscountCard", Object.DiscountCard);
	StructureData.Insert("DiscountPercentByDiscountCard", Object.DiscountPercentByDiscountCard);
	// End DiscountCards
	
	AddGLAccountsToStructure(ThisObject, "Inventory", StructureData);
	StructureData = GetDataProductsOnChange(StructureData);
	
	// Bundles
	If StructureData.IsBundle And Not StructureData.UseCharacteristics Then
		
		ReplaceInventoryLineWithBundleData(ThisObject, TabularSectionRow, StructureData);
		ClearCheckboxDiscountsAreCalculatedClient("CalculateAmountInTabularSectionLine", "Amount");
		RecalculateSubtotal();
		
		Else If  ValueIsFilled (Object.ac_BasisDocument) Then
			 //StructureData.Insert("MeasurementUnit", StructureData.Products.MeasurementUnit);
			 Products = TabularSectionRow.Products;
			 TabularSectionRow.MeasurementUnit = GetMeasurementUnit(Products);
			 //Items.BasisDocument.Visible = False;
			 //Items.ac_BasisDocument.Visible = True;  
			
		Else
			// End Bundles
			//Items.ac_BasisDocument.Visible = False;
			FillPropertyValues(TabularSectionRow, StructureData);
			TabularSectionRow.Quantity				= 1;
			TabularSectionRow.Content				= "";
			TabularSectionRow.ProductsTypeInventory	= StructureData.IsInventoryItem;
			
			CalculateAmountInTabularSectionLine();
			
			ClearEstimate();
		EndIf;
		// Bundles
	EndIf;
	// End Bundles 
	
	If String(TabularSectionRow.MeasurementUnit) = "ora" OR String(TabularSectionRow.MeasurementUnit) = "user" then
		
		  Product = TabularSectionRow.Products;
		  Price = GetCounterpartyContractPrice(Contract,Product);
		  
		  //TabularSectionRow.Price = GetCounterpartyContractPrice(Contract);
		  
		  CurrensyRate = GetCurrensyRate();
		  PriceInLei = Price * CurrensyRate;
		  
		  TabularSectionRow.Price = PriceInLei;
		  TabularSectionRow.Amount = PriceInLei * TabularSectionRow.Quantity;
		  
		  VATRate = TabularSectionRow.VATRate;
		  
		  TabularSectionRow.VATAmount = TabularSectionRow.Amount* GetVATRate(VATRate)/100;
		  TabularSectionRow.Total = TabularSectionRow.Amount + TabularSectionRow.VATAmount;
	EndIf;

EndProcedure

&AtServer
Function GetCurrensyRate()
	  		
	Query = New Query;
	Query.Text =
		"SELECT
		|	ExchangeRateSliceLast.Rate AS Rate
		|FROM
		|	InformationRegister.ExchangeRate.SliceLast(&DocDate, Currency.Description = ""EUR"") AS ExchangeRateSliceLast";
	
	Query.SetParameter("DocDate", Object.Date);
	
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		CurrensyRate = SelectionDetailRecords.Rate;
		
	EndDo;
	
	Return  CurrensyRate
	
EndFunction

&AtServer
Function GetVATRate(VATRate)

	Rate = VATRate.Rate;
	
	Return  Rate
	
EndFunction

&AtServer
Function GetMeasurementUnit(Products)

	MeasurementUnit = Products.MeasurementUnit;
	
	Return  MeasurementUnit
	
EndFunction

&AtServer
Function GetCounterpartyContractPrice(Contract, Product)

	Query = New Query;
	Query.Text =
		
	"SELECT
	|	ISNULL(ac_AbonDataSliceLast.Price, 0) AS Price
	|FROM
	|	InformationRegister.ac_AbonData.SliceLast(
	|			&Date,
	|			Contract = &Contract
	|				AND Products = &Product) AS ac_AbonDataSliceLast";
	
	Query.SetParameter("Product", Product);
	Query.SetParameter("Date", Object.Date);
	Query.SetParameter("Contract",Object.Contract);
	QueryResult = Query.Execute();
	
	If NOT QueryResult.IsEmpty() Then
		
		Selection = QueryResult.Select();
		                                  		
		While Selection.Next() Do
			
			Price = Selection.Price;
			
		EndDo;
		
	Else
		
		Price = Contract.Rate;
		
	EndIf;
		
		Return  Price
	
EndFunction

