﻿
&After("Filling")
Procedure ac_Filling(FillingData, StandardProcessing) Export
		
	If TypeOf(FillingData) = Type("Structure") Then
		
		If FillingData.Property("Base") Then
			If TypeOf(FillingData.Base) = Type("DocumentRef.WorkComplete") Then
				
				FillPropertyValues(ThisObject, FillingData.Base,,"Number, Date, Author");
				ac_BasisDocument = FillingData.Base;
				
				Date = CurrentDate();
				CurrensyRate = GetCurrensyRate();
				
				NewRow = Inventory.Add();
				NewRow.Quantity = FillingData.Base.Tasks.Total("Quantity");
				NewRow.Price = FillingData.Base.Rate * CurrensyRate;
				NewRow.Amount = NewRow.Quantity*NewRow.Price;
				NewRow.MeasurementUnit = GetService().MeasurementUnit;
				NewRow.Products = GetService().Product;
				
			EndIf;
			
		EndIf;
	EndIf;

EndProcedure

Function GetCurrensyRate()
	  		
	Query = New Query;
	Query.Text =
		"SELECT
		|	ExchangeRateSliceLast.Rate AS Rate
		|FROM
		|	InformationRegister.ExchangeRate.SliceLast(&DocDate, Currency.Description = ""EUR"") AS ExchangeRateSliceLast";
	
	Query.SetParameter("DocDate", Date);
	
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		CurrensyRate = SelectionDetailRecords.Rate;
		
	EndDo;
	
	Return  CurrensyRate
	
EndFunction

Function GetService()
	q = New Query("SELECT TOP 1
	              |	ac_ServiceProduct.Product AS Product,
	              |	ac_ServiceProduct.MeasurementUnit AS MeasurementUnit
	              |FROM
	              |	InformationRegister.ac_ServiceProduct AS ac_ServiceProduct");
	Rez = q.Execute().Select();

	While Rez.Next() Do
		
		Record = New Structure;
		Record.Insert("Product", Rez.Product);
		Record.Insert("MeasurementUnit", Rez.MeasurementUnit);
		
	EndDo;
	
	Return Record
	
EndFunction

//&After("Posting")
//Procedure ac_Posting(Cancel, PostingMode)
//	
//	If HoursPaidInAdvance <> 0 Then
//	
//	// регистр HoursPaidInAdvance 
//	RegisterRecords.HoursPaidInAdvance.Write = True;
//	Record = RegisterRecords.HoursPaidInAdvance.Add();
//	Record.Period = Date;
//	Record.Company = Company;
//	Record.Counterparty = Counterparty;
//	Record.Contract = Contract;
//	Record.Hours = HoursPaidInAdvance;
//	
//	EndIf; 	
//	
//EndProcedure

