﻿
Procedure OnDefineSettings(Settings) Export
    
	// К какому объекту привязывается. Лучше задавать в виде строки, чтобы не тянуть объект в Расширение!
	// Metadata.Object for link to. It's better way - as String, in this case we don't to pull Object into Extension
    Settings.Placement.Add("Document.SalesInvoice");
    Settings.AddPrintCommands = True;

EndProcedure

Function CreatePrintForm(ObjectArray, PrintObjects, TemplateName)

	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_SalesInvoice";
	Spreadsheet.FitToPage = True;
	//Spreadsheet.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	//Spreadsheet.ПолеСлева = 20;
	//Spreadsheet.ПолеСправа = 20;
	//Spreadsheet.ПолеСверху = 5;
	//Spreadsheet.ПолеСнизу = 5;
	//Spreadsheet.АвтоМасштаб = Истина;
	//Spreadsheet.ОтображатьЗаголовки = Ложь;
	//Spreadsheet.ОтображатьСетку = Ложь;
	//Spreadsheet.ДвусторонняяПечать = ТипДвустороннейПечати.ПереворотВлево;

	Template	= Documents.SalesInvoice.GetTemplate("FacturaFiscalaDeVinzare");
	Query		= New Query;
	
	Query.Parameters.Insert("ObjectsArray", ObjectArray);
	
	Query.Text =
	"SELECT ALLOWED
	|	SalesInvoice.Ref AS Ref,
	|	SalesInvoice.Number AS Number,
	|	SalesInvoice.Date AS Date,
	|	SalesInvoice.Company AS Company,
	|	SalesInvoice.Counterparty AS Counterparty,
	|	SalesInvoice.Contract AS Contract,
	|	SalesInvoice.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesInvoice.DocumentCurrency AS DocumentCurrency,
	|	CAST(SalesInvoice.Comment AS STRING(1024)) AS Comment,
	|	SalesInvoice.Order AS Order,
	|	SalesInvoice.DocumentSubtotal AS DocumentSubtotal
	|INTO SalesInvoices
	|FROM
	|	Document.SalesInvoice AS SalesInvoice
	|WHERE
	|	SalesInvoice.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesInvoice.Ref AS Ref,
	|	SalesInvoice.Number AS DocumentNumber,
	|	SalesInvoice.Date AS DocumentDate,
	|	SalesInvoice.Company AS Company,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	SalesInvoice.Counterparty AS Counterparty,
	|	SalesInvoice.Contract AS Contract,
	|	SalesInvoice.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesInvoice.DocumentCurrency AS DocumentCurrency,
	|	ISNULL(SalesOrder.Number, """") AS SalesOrderNumber,
	|	ISNULL(SalesOrder.Date, DATETIME(1, 1, 1)) AS SalesOrderDate,
	|	SalesInvoice.DocumentSubtotal AS DocumentSubtotal
	|INTO Header
	|FROM
	|	SalesInvoices AS SalesInvoice
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON SalesInvoice.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON SalesInvoice.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON SalesInvoice.Contract = CounterpartyContracts.Ref
	|		LEFT JOIN Document.SalesOrder AS SalesOrder
	|		ON SalesInvoice.Order = SalesOrder.Ref
	|
	|GROUP BY
	|	SalesInvoice.Number,
	|	SalesInvoice.Date,
	|	SalesInvoice.Counterparty,
	|	SalesInvoice.Company,
	|	Companies.LogoFile,
	|	SalesInvoice.Ref,
	|	SalesInvoice.Comment,
	|	ISNULL(SalesOrder.Date, DATETIME(1, 1, 1)),
	|	ISNULL(SalesOrder.Number, """"),
	|	SalesInvoice.DocumentCurrency,
	|	SalesInvoice.AmountIncludesVAT,
	|	SalesInvoice.Contract,
	|	SalesInvoice.DocumentSubtotal
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesInvoiceInventory.Ref AS Ref,
	|	SalesInvoiceInventory.LineNumber AS LineNumber,
	|	SalesInvoiceInventory.Products AS Products,
	|	SalesInvoiceInventory.Characteristic AS Characteristic,
	|	SalesInvoiceInventory.Batch AS Batch,
	|	SalesInvoiceInventory.Quantity AS Quantity,
	|	SalesInvoiceInventory.Reserve AS Reserve,
	|	SalesInvoiceInventory.MeasurementUnit AS MeasurementUnit,
	|	SalesInvoiceInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SalesInvoiceInventory.Total - SalesInvoiceInventory.VATAmount AS Amount,
	|	SalesInvoiceInventory.VATRate AS VATRate,
	|	SalesInvoiceInventory.VATAmount AS VATAmount,
	|	SalesInvoiceInventory.Total AS Total,
	|	SalesInvoiceInventory.Order AS Order,
	|	SalesInvoiceInventory.Content AS Content,
	|	SalesInvoiceInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	SalesInvoiceInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	SalesInvoiceInventory.ConnectionKey AS ConnectionKey,
	|	SalesInvoiceInventory.BundleProduct AS BundleProduct,
	|	SalesInvoiceInventory.BundleCharacteristic AS BundleCharacteristic,
	|	SalesInvoiceInventory.SerialNumbers AS SerialNumbers
	|INTO FilteredInventory
	|FROM
	|	Document.SalesInvoice.Inventory AS SalesInvoiceInventory
	|		INNER JOIN SalesInvoices AS SalesInvoices
	|		ON SalesInvoiceInventory.Ref = SalesInvoices.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	FilteredInventory.ConnectionKey AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	ISNULL(SalesOrder.Number, Header.SalesOrderNumber) AS SalesOrderNumber,
	|	ISNULL(SalesOrder.Date, Header.SalesOrderDate) AS SalesOrderDate,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	FilteredInventory.Batch AS Batch,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic,
	|	FilteredInventory.SerialNumbers AS SerialNumbers,
	|	Header.DocumentSubtotal AS DocumentSubtotal
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|		LEFT JOIN Document.SalesOrder AS SalesOrder
	|		ON (FilteredInventory.Order = SalesOrder.Ref)
	|			AND (Header.SalesOrderNumber = """")";

	ResultArray = Query.ExecuteBatch();
	
	Selection = ResultArray[3].Select(QueryResultIteration.ByGroupsWithHierarchy);
		 
	PreHeader			= Template.GetArea("PreHeader");
	Header				= Template.GetArea("Header");
	CompanyInfo         = Template.GetArea("CompanyInfo");
	CounterpartyInfo    = Template.GetArea("CounterpartyInfo");
	PreLine             = Template.GetArea("PreLine");
	Line				= Template.GetArea("Line");
	PostLine            = Template.GetArea("PostLine");
	SignaturesArea		= Template.GetArea("Signatures");
	//EmptyLineArea		= Template.GetArea("EmptyLine");	
	//LastLineArea        = Template.GetArea("LastLine");
		
	AreasToBeChecked = New Array;
	
	Spreadsheet.Clear();
	PageNumber = 1;
	Index = 0;
	              	
	While Selection.Next() Do
		
		Object = Selection.Ref.GetObject();
		Inventory = Object.Inventory;
		Count = Inventory.Count();
		
		If Index = 0 Then
			
			InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.DocumentDate, ,);

		#Region PrintPROCESVERBALPreHeader1
			
			If ValueIsFilled(Selection.CompanyLogoFile) Then
				
				PictureData = AttachedFiles.GetBinaryFileData(Selection.CompanyLogoFile);
				If ValueIsFilled(PictureData) Then
					PreHeader.Drawings.Logo.Picture = New Picture(PictureData);
				EndIf;
			Else
				PreHeader.Drawings.Delete(PreHeader.Drawings.Logo);
			EndIf;
			
		#EndRegion
			
		#Region PrintPROCESVERBALPreHeader2
			
			PreHeader.Parameters["Company"] 			= InfoAboutCompany.FullDescr;
			PreHeader.Parameters["KPP"] 				= InfoAboutCompany.TIN;
			PreHeader.Parameters["RegistrationNumber"]	= InfoAboutCompany.RegistrationNumber;
			PreHeader.Parameters["Address"]				= InfoAboutCompany.LegalAddress;
			PreHeader.Parameters["Phone"]				= InfoAboutCompany.PhoneNumbers;
			PreHeader.Parameters["Email"]				= InfoAboutCompany.Email;
			PreHeader.Parameters["Webpage"]				= InfoAboutCompany.Webpage;
			
			Spreadsheet.Put(PreHeader);
		#EndRegion
			
		#Region PrintPROCESVERBALHeader
			
			//Header.Parameters.DocumentDate		= Format(Selection.DocumentDate,"DF=""dd.MM.yyyy""");
			Header.Parameters.CurrentDate = Format(CurrentDate(),"DF=""dd.MM.yyyy""");
			
			Contract = Selection.Contract.GetObject();
			ContractNo = Contract.ContractNo;
			ContractDate = Format(Contract.ContractDate,"DF=dd.MM.yyyy");
			Header.Parameters.ContractNo = ContractNo;
			Header.Parameters.ContractDate = ContractDate;
			
			Spreadsheet.Put(Header);
		#EndRegion
			
		#Region PrintPROCESVERBALCompanyInfo
			
			ResponsiblePersons = DriveServer.OrganizationalUnitsResponsiblePersons(Selection.Company, CurrentDate());
			CompanyContactPersonPosition = ResponsiblePersons.HeadPosition;
			CompanyContactPerson = ResponsiblePersons.Head;
			
			CompanyInfo.Parameters.Company 				= Selection.Company;
			CompanyInfo.Parameters.KPP 					= InfoAboutCompany.TIN;
			CompanyInfo.Parameters.RegistrationNumber	= TrimAll(InfoAboutCompany.RegistrationNumber);
			CompanyInfo.Parameters.Address				= InfoAboutCompany.LegalAddress;
			CompanyInfo.Parameters.Phone				= InfoAboutCompany.PhoneNumbers;
			CompanyInfo.Parameters.Email				= InfoAboutCompany.Email;
			CompanyInfo.Parameters.BankAccount			= InfoAboutCompany.AccountNo;
			CompanyInfo.Parameters.Bank 				= InfoAboutCompany.Bank;
			CompanyInfo.Parameters.CompanyContactPerson = CompanyContactPerson;
			CompanyInfo.Parameters.CompanyContactPersonPosition = CompanyContactPersonPosition;
			
			Spreadsheet.Put(CompanyInfo);
		#EndRegion
			
		#Region PrintPROCESVERBALCounterpartyInfo
			
			InfoAboutCounterparty =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, CurrentDate(), ,);
			CounterpartyInfo.Parameters.Counterparty = Selection.Counterparty;
			CounterpartyInfo.Parameters.CKPP 					= InfoAboutCounterparty.TIN;
			CounterpartyInfo.Parameters.CRegistrationNumber	= TrimAll(InfoAboutCounterparty.RegistrationNumber);
			CounterpartyInfo.Parameters.CAddress				= InfoAboutCounterparty.LegalAddress;
			CounterpartyInfo.Parameters.CPhone				= InfoAboutCounterparty.PhoneNumbers;
			CounterpartyInfo.Parameters.CEmail				= InfoAboutCounterparty.Email;
			CounterpartyInfo.Parameters.CBankAccount			= InfoAboutCounterparty.AccountNo;
			CounterpartyInfo.Parameters.CBank 				= InfoAboutCounterparty.Bank;
			
			//Counterparty = Selection.Counterparty.GetObject(); 				
			
			Counterparty = String(Selection.Counterparty);
			//InfoAboutContactPerson = Ext_PF_ATCommonServerModule.GetContactChoiceList(Counterparty); 
			InfoAboutContactPerson = ACRo_CommonServerModule.GetContactChoiceList(Counterparty);
			
			For Each ItemOfList In InfoAboutContactPerson Do
				
				ContactPerson = ItemOfList.Value.ContactPerson.Position;
				
				If String(ContactPerson) = "Administrator" Then
					
					CounterpartyInfo.Parameters.CounterpartyContactPerson = ItemOfList.Value.ContactPerson;
					CounterpartyInfo.Parameters.CounterpartyContactPersonPosition = ItemOfList.Value.ContactPerson.Position;
					 			
				EndIf;
			EndDo;
			
			Spreadsheet.Put(CounterpartyInfo);
		#EndRegion
			
		#Region PrintPROCESVERBALPreLine
			
			Characteristic = TrimAll(String(Selection.Characteristic));
			
			If  Find(Characteristic, "Licență de bază") OR Find(Characteristic, "Licenta de baza") Then
				
				Ind = StrFind(Characteristic, "-");
				MainProduct = TrimAll(Left(Characteristic, Ind-1));
				
				PreLine.Parameters["MainProduct"] = MainProduct;
				
			EndIf;
			
			PreLine.Parameters.ContractNo = ContractNo;
			PreLine.Parameters.ContractDate = ContractDate;
			
			Spreadsheet.Put(PreLine);
			
		#EndRegion

		EndIf;

	#Region PrintPROCESVERBALLine
		
		Line.Parameters.ProductName = Selection.Products;
		Line.Parameters.SerialNumber = Selection.SerialNumbers;
		
		Spreadsheet.Put(Line);
						
	#EndRegion
		
		//Regions after Tabular Section. Области после табличной части.
		
		If Index = Count-1  Then
			
		#Region PrintPROCESVERBALPostLine
			
			Query = New Query;
			Query.Text =
			
			"SELECT
			|	ExchangeRateSliceLast.Rate AS Rate
			|FROM
			|	InformationRegister.ExchangeRate.SliceLast(
			|			&Period,
			|			Company = &Company
			|				AND Currency.Code = &CurrencyCode) AS ExchangeRateSliceLast";
			
			Query.Parameters.Insert("CurrencyCode", "978");
			Query.Parameters.Insert("Period", Selection.DocumentDate);   //CurrentDate()
			Query.Parameters.Insert("Company", Selection.Company);
			
			QueryResult = Query.Execute();
			
			SelectionDetailRecords = QueryResult.Select();
			
			While SelectionDetailRecords.Next() Do
				
				CurrentEURORate = SelectionDetailRecords.Rate;
				
			EndDo;
			
			SubtotalRON = Selection.DocumentSubtotal;
			SubtotalEURO = SubtotalRON /CurrentEURORate; 
			
			PostLine.Parameters.SubtotalEURO =  Format(SubtotalEURO,"NFD=0;ЧРГ=.");
			PostLine.Parameters.SubtotalRON  = Format(SubtotalRON,"NFD=2;ЧРГ=.");
			
			//PostLine.Parameters.DocumentDate = Format(Selection.DocumentDate,"DF=""dd.MM.yyyy"""); 
			PostLine.Parameters.CurrentDate = Format(CurrentDate(),"DF=""dd.MM.yyyy""");
			Spreadsheet.Put(PostLine);
		#EndRegion
			
		#Region PrintPROCESVERBALSignaturesArea
			
			CompanyContactPersonPosition = ResponsiblePersons.HeadPosition;
			CompanyContactPerson = ResponsiblePersons.Head;
			
			SignaturesArea.Parameters.CompanyFullDescr = InfoAboutCompany.FullDescr;
			SignaturesArea.Parameters.CompanyContactPersonPosition = CompanyContactPersonPosition;
			SignaturesArea.Parameters.CompanyContactPerson = CompanyContactPerson;
				
			SignaturesArea.Parameters.CounterpartyFullDescr = InfoAboutCounterparty.FullDescr;
			
			For Each ItemOfList In InfoAboutContactPerson Do
				
				ContactPerson = ItemOfList.Value.ContactPerson.Position;
				
				If String(ContactPerson) = "Administrator" Then
					
					SignaturesArea.Parameters.CounterpartyContactPersonPosition = ItemOfList.Value.ContactPerson.Position;
					SignaturesArea.Parameters.CounterpartyContactPerson = ItemOfList.Value.ContactPerson;
				Else
					Message = New UserMessage;
					Message.Text = "Persoana implicita de contact nu are pozitia sau pozitia ei nu este 'Administrator'!";
					Message.Message();
				EndIf;
			EndDo;
			
			Spreadsheet.Put(SignaturesArea);
			
			#EndRegion

#Region PrintQuoteFooter

	Spreadsheet.Footer.Enabled = True;
	Spreadsheet.Footer.Font = new Font("Verdana", 11,,True);
	Spreadsheet.Footer.StartPage = 1;
	Spreadsheet.BottomMargin = 20;
	Spreadsheet.FooterSize = 20;
	Spreadsheet.АвтоМасштаб = Истина;
	Spreadsheet.ОтображатьЗаголовки = Ложь;
	Spreadsheet.ОтображатьСетку = Ложь;
	Spreadsheet.DuplexPrinting = DuplexPrintingType.None;
	
	Spreadsheet.Footer.LeftText  = "* Actualul proces verbal se va considera incheiat daca in decurs de 5 zile lucratoare nu primim obiectii din partea" + Символы.ПС + "Beneficiarului. Modificarile conform Anexei vor fi considerate primite de catre Beneficiar." + Символы.ПС + Символы.ПС + "Proces verbal de predare-primire";
	Spreadsheet.Footer.RightText = Символы.ПС + Символы.ПС + Символы.ПС + "[&PageNumber]"; //"Pagina [&PageNumber] din [&PagesTotal]";
	
#EndRegion
	
	Spreadsheet.PageSize = "A4";
	Spreadsheet.FitToPage = True;

		EndIf;
		
		Index = Index + 1;
	EndDo;
	
	Return Spreadsheet;

EndFunction

&After("Print")
Procedure ac_Print(ObjectsArray, PrintParameters, PrintFormsCollection, PrintObjects, OutputParameters)

	If PrintManagement.TemplatePrintRequired(PrintFormsCollection, "FacturaFiscalaDeVinzare") Then
		
		PrintManagement.OutputSpreadsheetDocumentToCollection(
			PrintFormsCollection,
			"FacturaFiscalaDeVinzare",
			NStr("en='Subcontractor report';ro='PROCES VERBAL (Factura de vânzare)'"),
			ac_PrintForm(ObjectsArray,
			PrintObjects, "FacturaFiscalaDeVinzare", OutputParameters));
	EndIf;
	
EndProcedure

&After("AddPrintCommands")
Procedure ac_AddPrintCommands(PrintCommands)
	
	PrintCommand = PrintCommands.Add();
	PrintCommand.ID							= "FacturaFiscalaDeVinzare"; //= "SalesInvoicePROCESVERBAL"; 
	PrintCommand.Presentation				= NStr("en='Invoice';pl='PROCES VERBAL';ro='PROCES VERBAL';ru='PROCES VERBAL'");
	PrintCommand.CheckPostingBeforePrint	= False;
	PrintCommand.Order						= 10;

EndProcedure

&Around("PrintForm")
Function ac_PrintForm(ObjectsArray, PrintObjects, TemplateName, PrintParams = Undefined)
	// Вставить содержимое метода.
	//Result = ProceedWithCall(ObjectsArray, PrintObjects, TemplateName, PrintParams);
	//Return Result;
	
	If TemplateName = "SalesInvoice" Then

		Return PrintSalesInvoice(ObjectsArray, PrintObjects, TemplateName, PrintParams)

	EndIf;

	If TemplateName = "SubcontractorReport" Then

		Return PrintSubcontractorReport(ObjectsArray, PrintObjects, TemplateName,PrintParams)

	EndIf;
	
	If TemplateName = "FacturaFiscalaDeVinzare" Then

		Return CreatePrintForm(ObjectsArray, PrintObjects, TemplateName)

	EndIf;

EndFunction

//&ChangeAndValidate("PrintForm")
//Function ac_PrintForm(ObjectsArray, PrintObjects, TemplateName, PrintParams)

//	//If TemplateName = "SalesInvoice" Then

//	//	Return PrintSalesInvoice(ObjectsArray, PrintObjects, TemplateName, PrintParams)

//	//EndIf;

//	//If TemplateName = "SubcontractorReport" Then

//	//	Return PrintSubcontractorReport(ObjectsArray, PrintObjects, TemplateName)

//	//EndIf;
//	//
//	//If TemplateName = "FacturaFiscalaDeVinzare" Then

//	//	Return CreatePrintForm(ObjectsArray, PrintObjects, TemplateName)

//	//EndIf; 	

//EndFunction
