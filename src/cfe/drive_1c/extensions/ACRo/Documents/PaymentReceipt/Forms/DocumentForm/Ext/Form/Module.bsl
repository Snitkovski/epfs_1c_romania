﻿////&AtClient
////Var LineNumber1;

&AtClient
Procedure ac_DocumentAmountOnChangeBefore(Item)
	
	CurrentRow=Items.PaymentDetails.CurrentData;

	Object.HoursPaidInAdvance = Object.DocumentAmount/GetCounterpartyContractPrice(CurrentRow.Contract);
	
EndProcedure

&AtServer
Function GetCounterpartyContractPrice(Contract)

	Price = Contract.Rate;
	
	Return  Price
	
EndFunction

&AtClient
Procedure ac_CompletePaidHoursAfter(Command)
	
	ac_CompletePaidHoursAfterAtServer();
	 	
EndProcedure

&AtServer
Procedure ac_CompletePaidHoursAfterAtServer()
	
	Object.PaidHours.Clear();
	
	Query = New Query;
	Query.Text =
	
	"SELECT
	|	PaymentDetails.Order AS Order
	|INTO ttPaymentDetails
	|FROM
	|	&PaymentDetails AS PaymentDetails
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	SalesOrderInventory.Ref AS Ref,
	|	SalesOrderInventory.Products AS Products,
	|	SalesOrderInventory.Characteristic AS Characteristic,
	|	SalesOrderInventory.Batch AS Batch,
	|	SalesOrderInventory.Quantity AS Quantity,
	|	SalesOrderInventory.Reserve AS Reserve,
	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
	|	SalesOrderInventory.Price AS Price,
	|	SalesOrderInventory.VATRate AS VATRate,
	|	SalesOrderInventory.VATAmount AS VATAmount,
	|	SalesOrderInventory.Amount AS Amount,
	|	SalesOrderInventory.InventoryGLAccount AS InventoryGLAccount,
	|	SalesOrderInventory.Total AS Total,
	|	SalesOrderInventory.Ref.Contract AS Contract
	|INTO TT
	|FROM
	|	ttPaymentDetails AS ttPaymentDetails
	|		INNER JOIN Document.SalesOrder.Inventory AS SalesOrderInventory
	|		ON ttPaymentDetails.Order = SalesOrderInventory.Ref
	|WHERE
	|	(SalesOrderInventory.MeasurementUnit.Description = &Description1
	|			OR SalesOrderInventory.MeasurementUnit.Description = &Description2)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ISNULL(TT.Quantity, 0) AS Quantity,
	|	TT.Contract AS DocumentContract,
	|	TT.Products AS Products,
	|	TT.Ref AS Ref,
	|	TT.Characteristic AS Characteristic,
	|	TT.Batch AS Batch,
	|	TT.Reserve AS Reserve,
	|	TT.MeasurementUnit AS MeasurementUnit,
	|	TT.Price AS Price,
	|	TT.VATRate AS VATRate,
	|	TT.VATAmount AS VATAmount,
	|	TT.Amount AS Amount,
	|	TT.InventoryGLAccount AS InventoryGLAccount,
	|	TT.Total AS Total
	|FROM
	|	InformationRegister.ac_AbonData.SliceLast(
	|			&Date,
	|			(Contract, Products) IN
	|				(SELECT
	|					TT.Contract AS Contract,
	|					TT.Products AS Products
	|				FROM
	|					TT AS TT)) AS ac_AbonDataSliceLast
	|		INNER JOIN TT AS TT
	|		ON (CASE
	|				WHEN TT.MeasurementUnit.Description = ""user""
	|					THEN TT.Contract = ac_AbonDataSliceLast.Contract
	|
	|			END)
	|			AND (CASE
	|				WHEN TT.MeasurementUnit.Description = ""user""
	|					THEN TT.Products = ac_AbonDataSliceLast.Products
	|			END)
	|WHERE
	|	ISNULL(TT.Quantity, 0) > 0
	|
	|UNION ALL
	|
	|SELECT
	|	ISNULL(TT.Quantity, 0),
	|	TT.Contract,
	|	TT.Products,
	|	TT.Ref,
	|	TT.Characteristic,
	|	TT.Batch,
	|	TT.Reserve,
	|	TT.MeasurementUnit,
	|	TT.Price,
	|	TT.VATRate,
	|	TT.VATAmount,
	|	TT.Amount,
	|	TT.InventoryGLAccount,
	|	TT.Total
	|FROM
	|	TT AS TT
	|WHERE
	|	ISNULL(TT.Quantity, 0) > 0
	|	AND TT.MeasurementUnit.Description = ""ora""" ;
	
	//Query.SetParameter("Ref", Object.BasisDocument);
	Query.SetParameter("PaymentDetails", Object.PaymentDetails.Unload());
	Query.SetParameter("Description1", "ora");
	Query.SetParameter("Description2", "user");
	Query.SetParameter("Date", Object.Date);
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		NewLine=Object.PaidHours.Add();
		NewLine.Document=SelectionDetailRecords.Ref;
		NewLine.Products=SelectionDetailRecords.Products;
		NewLine.Characteristic=SelectionDetailRecords.Characteristic;
		NewLine.Batch=SelectionDetailRecords.Batch;
		NewLine.Quantity=SelectionDetailRecords.Quantity;
		NewLine.Reserve=SelectionDetailRecords.Reserve;
		NewLine.MeasurementUnit=SelectionDetailRecords.MeasurementUnit;
		NewLine.Price=SelectionDetailRecords.Price;
		NewLine.VATRate=SelectionDetailRecords.VATRate;
		NewLine.VATAmount=SelectionDetailRecords.VATAmount;
		NewLine.Amount=SelectionDetailRecords.Amount;
		NewLine.Total=SelectionDetailRecords.Total;
		NewLine.InventoryGLAccount=SelectionDetailRecords.InventoryGLAccount;
				
	EndDo;

EndProcedure

&AtClient
Procedure ac_ClearAfter(Command)
	Object.PaidHours.Clear();
EndProcedure

&AtServer
Procedure ac_PagesOnCurrentPageChangeAfterAtServer()
	
	If Object.PaidHours.Count()= 0 Then
	Object.PaidHours.Clear();
	
	Query = New Query;
	Query.Text =
		
	"SELECT
	|	PaymentDetails.Order AS Order
	|INTO ttPaymentDetails
	|FROM
	|	&PaymentDetails AS PaymentDetails
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	SalesOrderInventory.Ref AS Ref,
	|	SalesOrderInventory.Products AS Products,
	|	SalesOrderInventory.Characteristic AS Characteristic,
	|	SalesOrderInventory.Batch AS Batch,
	|	SalesOrderInventory.Quantity AS Quantity,
	|	SalesOrderInventory.Reserve AS Reserve,
	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
	|	SalesOrderInventory.Price AS Price,
	|	SalesOrderInventory.VATRate AS VATRate,
	|	SalesOrderInventory.VATAmount AS VATAmount,
	|	SalesOrderInventory.Amount AS Amount,
	|	SalesOrderInventory.InventoryGLAccount AS InventoryGLAccount,
	|	SalesOrderInventory.Total AS Total,
	|	SalesOrderInventory.Ref.Contract AS Contract
	|INTO TT
	|FROM
	|	ttPaymentDetails AS ttPaymentDetails
	|		INNER JOIN Document.SalesOrder.Inventory AS SalesOrderInventory
	|		ON ttPaymentDetails.Order = SalesOrderInventory.Ref
	|WHERE
	|	(SalesOrderInventory.MeasurementUnit.Description = &Description1
	|			OR SalesOrderInventory.MeasurementUnit.Description = &Description2)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ISNULL(TT.Quantity, 0) AS Quantity,
	|	TT.Contract AS DocumentContract,
	|	TT.Products AS Products,
	|	TT.Ref AS Ref,
	|	TT.Characteristic AS Characteristic,
	|	TT.Batch AS Batch,
	|	TT.Reserve AS Reserve,
	|	TT.MeasurementUnit AS MeasurementUnit,
	|	TT.Price AS Price,
	|	TT.VATRate AS VATRate,
	|	TT.VATAmount AS VATAmount,
	|	TT.Amount AS Amount,
	|	TT.InventoryGLAccount AS InventoryGLAccount,
	|	TT.Total AS Total
	|FROM
	|	InformationRegister.ac_AbonData.SliceLast(
	|			&Date,
	|			(Contract, Products) IN
	|				(SELECT
	|					TT.Contract AS Contract,
	|					TT.Products AS Products
	|				FROM
	|					TT AS TT)) AS ac_AbonDataSliceLast
	|		INNER JOIN TT AS TT
	|		ON (CASE
	|				WHEN TT.MeasurementUnit.Description = ""user""
	|					THEN TT.Contract = ac_AbonDataSliceLast.Contract
	|
	|			END)
	|			AND (CASE
	|				WHEN TT.MeasurementUnit.Description = ""user""
	|					THEN TT.Products = ac_AbonDataSliceLast.Products
	|			END)
	|WHERE
	|	ISNULL(TT.Quantity, 0) > 0
	|
	|UNION ALL
	|
	|SELECT
	|	ISNULL(TT.Quantity, 0),
	|	TT.Contract,
	|	TT.Products,
	|	TT.Ref,
	|	TT.Characteristic,
	|	TT.Batch,
	|	TT.Reserve,
	|	TT.MeasurementUnit,
	|	TT.Price,
	|	TT.VATRate,
	|	TT.VATAmount,
	|	TT.Amount,
	|	TT.InventoryGLAccount,
	|	TT.Total
	|FROM
	|	TT AS TT
	|WHERE
	|	ISNULL(TT.Quantity, 0) > 0
	|	AND TT.MeasurementUnit.Description = ""ora""" ;
	
	//Query.SetParameter("Ref", Object.BasisDocument);
	Query.SetParameter("PaymentDetails", Object.PaymentDetails.Unload());
	Query.SetParameter("Description1", "ora");
	Query.SetParameter("Description2", "user");
	Query.SetParameter("Date", Object.Date);
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		NewLine=Object.PaidHours.Add();
		NewLine.Document=SelectionDetailRecords.Ref;
		NewLine.Products=SelectionDetailRecords.Products;
		NewLine.Characteristic=SelectionDetailRecords.Characteristic;
		NewLine.Batch=SelectionDetailRecords.Batch;
		NewLine.Quantity=SelectionDetailRecords.Quantity;
		NewLine.Reserve=SelectionDetailRecords.Reserve;
		NewLine.MeasurementUnit=SelectionDetailRecords.MeasurementUnit;
		NewLine.Price=SelectionDetailRecords.Price;
		NewLine.VATRate=SelectionDetailRecords.VATRate;
		NewLine.VATAmount=SelectionDetailRecords.VATAmount;
		NewLine.Amount=SelectionDetailRecords.Amount;
		NewLine.Total=SelectionDetailRecords.Total;
		NewLine.InventoryGLAccount=SelectionDetailRecords.InventoryGLAccount;
				
	EndDo;
	EndIf;
EndProcedure

&AtClient
Procedure ac_PagesOnCurrentPageChangeAfter(Item, CurrentPage)

	ac_PagesOnCurrentPageChangeAfterAtServer();
EndProcedure

////&AtServerNoContext
&AtServer
Procedure ac_BeforeWriteAfterAtServer()

	If Object.PaidHours.Count()= 0 Then
	
	Object.PaidHours.Clear();
	
	Query = New Query;
	Query.Text =
	"SELECT
	|	PaymentDetails.Order AS Order
	|INTO ttPaymentDetails
	|FROM
	|	&PaymentDetails AS PaymentDetails
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	SalesOrderInventory.Ref AS Ref,
	|	SalesOrderInventory.Products AS Products,
	|	SalesOrderInventory.Characteristic AS Characteristic,
	|	SalesOrderInventory.Batch AS Batch,
	|	SalesOrderInventory.Quantity AS Quantity,
	|	SalesOrderInventory.Reserve AS Reserve,
	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
	|	SalesOrderInventory.Price AS Price,
	|	SalesOrderInventory.VATRate AS VATRate,
	|	SalesOrderInventory.VATAmount AS VATAmount,
	|	SalesOrderInventory.Amount AS Amount,
	|	SalesOrderInventory.InventoryGLAccount AS InventoryGLAccount,
	|	SalesOrderInventory.Total AS Total,
	|	SalesOrderInventory.Ref.Contract AS Contract
	|INTO TT
	|FROM
	|	ttPaymentDetails AS ttPaymentDetails
	|		INNER JOIN Document.SalesOrder.Inventory AS SalesOrderInventory
	|		ON ttPaymentDetails.Order = SalesOrderInventory.Ref
	|WHERE
	|	(SalesOrderInventory.MeasurementUnit.Description = &Description1
	|			OR SalesOrderInventory.MeasurementUnit.Description = &Description2)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ISNULL(TT.Quantity, 0) AS Quantity,
	|	TT.Contract AS DocumentContract,
	|	TT.Products AS Products,
	|	TT.Ref AS Ref,
	|	TT.Characteristic AS Characteristic,
	|	TT.Batch AS Batch,
	|	TT.Reserve AS Reserve,
	|	TT.MeasurementUnit AS MeasurementUnit,
	|	TT.Price AS Price,
	|	TT.VATRate AS VATRate,
	|	TT.VATAmount AS VATAmount,
	|	TT.Amount AS Amount,
	|	TT.InventoryGLAccount AS InventoryGLAccount,
	|	TT.Total AS Total
	|FROM
	|	InformationRegister.ac_AbonData.SliceLast(
	|			&Date,
	|			(Contract, Products) IN
	|				(SELECT
	|					TT.Contract AS Contract,
	|					TT.Products AS Products
	|				FROM
	|					TT AS TT)) AS ac_AbonDataSliceLast
	|		INNER JOIN TT AS TT
	|		ON (CASE
	|				WHEN TT.MeasurementUnit.Description = ""user""
	|					THEN TT.Contract = ac_AbonDataSliceLast.Contract
	|
	|			END)
	|			AND (CASE
	|				WHEN TT.MeasurementUnit.Description = ""user""
	|					THEN TT.Products = ac_AbonDataSliceLast.Products
	|			END)
	|WHERE
	|	ISNULL(TT.Quantity, 0) > 0
	|
	|UNION ALL
	|
	|SELECT
	|	ISNULL(TT.Quantity, 0),
	|	TT.Contract,
	|	TT.Products,
	|	TT.Ref,
	|	TT.Characteristic,
	|	TT.Batch,
	|	TT.Reserve,
	|	TT.MeasurementUnit,
	|	TT.Price,
	|	TT.VATRate,
	|	TT.VATAmount,
	|	TT.Amount,
	|	TT.InventoryGLAccount,
	|	TT.Total
	|FROM
	|	TT AS TT
	|WHERE
	|	ISNULL(TT.Quantity, 0) > 0
	|	AND TT.MeasurementUnit.Description = ""ora""" ;
	
	//Query.SetParameter("Ref", Object.BasisDocument);
	Query.SetParameter("PaymentDetails", Object.PaymentDetails.Unload());
	Query.SetParameter("Description1", "ora");
	Query.SetParameter("Description2", "user");
	Query.SetParameter("Date", Object.Date);
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		NewLine=Object.PaidHours.Add();
		NewLine.Document=SelectionDetailRecords.Ref;
		NewLine.Products=SelectionDetailRecords.Products;
		NewLine.Characteristic=SelectionDetailRecords.Characteristic;
		NewLine.Batch=SelectionDetailRecords.Batch;
		NewLine.Quantity=SelectionDetailRecords.Quantity;
		NewLine.Reserve=SelectionDetailRecords.Reserve;
		NewLine.MeasurementUnit=SelectionDetailRecords.MeasurementUnit;
		NewLine.Price=SelectionDetailRecords.Price;
		NewLine.VATRate=SelectionDetailRecords.VATRate;
		NewLine.VATAmount=SelectionDetailRecords.VATAmount;
		NewLine.Amount=SelectionDetailRecords.Amount;
		NewLine.Total=SelectionDetailRecords.Total;
		NewLine.InventoryGLAccount=SelectionDetailRecords.InventoryGLAccount;
				
	EndDo;

	EndIf;
	
EndProcedure

&AtClient
Procedure ac_BeforeWriteAfter(Cancel, WriteParameters)
	
	//LineNumber2 = Object.PaymentDetails.Count();
	//
	//If LineNumber1 < LineNumber2 Then
	//	
	//	
	//	
	//EndIf;	
	
	ac_BeforeWriteAfterAtServer();
EndProcedure

//&AtClient
//Procedure ac_OnOpenAfter(Cancel)
//	//ac_OnOpenAfterAtServer();
//	
//	LineNumber1 = Object.PaymentDetails.Count();
//	
//EndProcedure

