﻿
&After("FillBySalesOrder")
Procedure ac_FillBySalesOrder(FillingData, LineNumber, Amount)

	Query = New Query;
	Query.Text =
		"SELECT
		|	SalesOrderInventory.Quantity AS Quantity
		|FROM
		|	Document.SalesOrder.Inventory AS SalesOrderInventory
		|WHERE
		|	NOT SalesOrderInventory.Ref.ac_BasisDocument IS NULL";
	
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		HoursPaidInAdvance = SelectionDetailRecords.Quantity;
		
	EndDo;

EndProcedure

&After("Posting")
Procedure ac_Posting(Cancel, PostingMode)
	//RegisterRecords.SubscriptionHours.Clear();
	
	Query = New Query;
	Query.Text =
	"SELECT
	|	PaymentReceiptPaidHours.Document.Contract AS DocumentContract,
	|	PaymentReceiptPaidHours.Products AS Products,
	|	PaymentReceiptPaidHours.Quantity AS Quantity,
	|	PaymentReceiptPaidHours.MeasurementUnit AS MeasurementUnit,
	|	PaymentReceiptPaidHours.Characteristic AS Characteristic,
	|	PaymentReceiptPaidHours.Ref.Date AS Date
	|INTO TT
	|FROM
	|	Document.PaymentReceipt.PaidHours AS PaymentReceiptPaidHours
	|WHERE
	|	PaymentReceiptPaidHours.Ref = &Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ISNULL(TT.Quantity, 0) AS Quantity,
	|	TT.DocumentContract AS DocumentContract,
	|	TT.Products AS Products,
	|	TT.MeasurementUnit AS MeasurementUnit,
	|	TT.Characteristic AS Characteristic,
	|	ac_AbonDataSliceLast.Quantity AS Quantity1
	|FROM
	|	InformationRegister.ac_AbonData.SliceLast(
	|			&Date,
	|			(Contract, Products) IN
	|				(SELECT
	|					TT.DocumentContract AS Contract,
	|					TT.Products AS Products
	|				FROM
	|					TT AS TT)) AS ac_AbonDataSliceLast
	|		LEFT JOIN TT AS TT
	|		ON (TT.DocumentContract = ac_AbonDataSliceLast.Contract)
	|			AND (TT.Products = ac_AbonDataSliceLast.Products)
	|WHERE
	|	ISNULL(TT.Quantity, 0) > 0" ;
	
	Query.SetParameter("Ref", Ref);
	Query.SetParameter("Date", Date);
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		If ValueIsFilled(SelectionDetailRecords.Characteristic) then
			
			//register SubscriptionHours
			RegisterRecords.SubscriptionHours.Write = True;
			
			Record = RegisterRecords.SubscriptionHours.Add();
			Record.RecordType = AccumulationRecordType.Receipt;
			Unit = SelectionDetailRecords.MeasurementUnit.Description;
			 
			StrDate = SelectionDetailRecords.Characteristic.Description;
			StrDate = StrReplace(StrDate,"-","");
			StrDate = StrDate+"01000000";
			NewDate = Date(StrDate);
			Record.Period = NewDate;
			Record.Hours = SelectionDetailRecords.Quantity1;
			Record.Company = Company;
			Record.Counterparty = Counterparty;
			Record.Contract = SelectionDetailRecords.DocumentContract;
			
			//register HoursPaidInAdvance
			RegisterRecords.HoursPaidInAdvance.Write = True;
			Record = RegisterRecords.HoursPaidInAdvance.Add();
			Record.RecordType = AccumulationRecordType.Receipt;
			
			NewDate = Date(StrDate);
			Record.Period = NewDate;
			Record.Hours = SelectionDetailRecords.Quantity1;
			Record.Company = Company;
			Record.Counterparty = Counterparty;
			Record.Contract = SelectionDetailRecords.DocumentContract;
			
		Else
			Message("Câmpul ""Varianta"" nu este completat pe fila ""Ore achitate"" ");
			Cancel = True;
		EndIf;
	EndDo;
	
	//register HoursPaidInAdvance
	
	RegisterRecords.HoursPaidInAdvance.Write = True;
	
	For each CurrentRowPaidHours In PaidHours Do
		
		//If NOT ValueIsFilled(CurrentRowPaidHours.Characteristic) AND CurrentRowPaidHours.MeasurementUnit.Description = "ora" Then
		If CurrentRowPaidHours.MeasurementUnit.Description = "ora" Then
			
			Record = RegisterRecords.HoursPaidInAdvance.Add();
			
			Record.Period = Date;
			Record.Hours = CurrentRowPaidHours.Quantity;
			Record.Company = Company;
			Record.Counterparty = Counterparty;
			Record.Contract = CurrentRowPaidHours.Document.Contract;
			
		EndIf;
	EndDo;
	
EndProcedure

