﻿
Procedure Posting(Cancel, Mode)
	Query = New Query;
	Query.Text = "SELECT
	             |	WorkCompleteTasks.Ref.Contract AS Contract,
	             |	SUM(WorkCompleteTasks.Quantity) AS Quantity,
	             |	WorkCompleteTasks.Ref.Company AS Company,
	             |	WorkCompleteTasks.Ref.Counterparty AS Counterparty
	             |INTO TT
	             |FROM
	             |	Document.WorkComplete.Tasks AS WorkCompleteTasks
	             |WHERE
	             |	WorkCompleteTasks.Ref.Date = &Date
	             |	AND WorkCompleteTasks.Ref = &Ref
	             |	AND WorkCompleteTasks.ZeroHoursChoise = FALSE
	             |
	             |GROUP BY
	             |	WorkCompleteTasks.Ref.Counterparty,
	             |	WorkCompleteTasks.Ref.Company,
	             |	WorkCompleteTasks.Ref.Contract
	             |;
	             |
	             |////////////////////////////////////////////////////////////////////////////////
	             |SELECT
	             |	CASE
	             |		WHEN ISNULL(SubscriptionHoursBalance.HoursBalance, 0) < TT.Quantity
	             |			THEN ISNULL(SubscriptionHoursBalance.HoursBalance, 0)
	             |		ELSE TT.Quantity
	             |	END AS Min,
	             |	TT.Company AS Company,
	             |	TT.Counterparty AS Counterparty,
	             |	TT.Contract AS Contract
	             |FROM
	             |	TT AS TT
	             |		LEFT JOIN AccumulationRegister.SubscriptionHours.Balance(
	             |				&Date,
	             |				Contract IN
	             |					(SELECT
	             |						TT.Contract AS Contract
	             |					FROM
	             |						TT AS TT)) AS SubscriptionHoursBalance
	             |		ON TT.Contract = SubscriptionHoursBalance.Contract
	             |WHERE
	             |	ISNULL(SubscriptionHoursBalance.HoursBalance, 0) > 0
	             |	AND TT.Quantity > 0" ;

	Query.SetParameter("Ref", Ref);
	Query.SetParameter("Date", Date);
	QueryResult = Query.Execute();

	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		RegisterRecords.SubscriptionHours.Write = True;
		
		Record = RegisterRecords.SubscriptionHours.Add();
		Record.RecordType = AccumulationRecordType.Expense;
		Record.Period = Date;
		Record.Company = Company;
		Record.Counterparty = Counterparty;
		Record.Contract = SelectionDetailRecords.Contract;
		Record.Hours = SelectionDetailRecords.Min;
		
	EndDo;
			
	// register HoursWorked Expense 
	RegisterRecords.HoursWorked.Write = True;
	RegisterRecords.HoursPaidInAdvance.Write = True;
	
	For each CurrentRowTasks In Tasks Do
		
		If  CurrentRowTasks.ZeroHoursChoise = False Then
			
			Record = RegisterRecords.HoursWorked.Add();
			Record.RecordType = AccumulationRecordType.Expense;
			Record.Period = Date;
			Record.Company = Company;
			Record.Counterparty = Counterparty;
			Record.Contract = Contract;
			Record.EmployeeTask = CurrentRowTasks.Task;
			Record.Hours = CurrentRowTasks.Quantity;

		// register HoursPaidInAdvance Expense
							
			Record = RegisterRecords.HoursPaidInAdvance.Add();
			Record.RecordType = AccumulationRecordType.Expense;
			Record.Period = Date;
			Record.Company = Company;
			Record.Counterparty = Counterparty;
			Record.Contract = Contract;
			Record.Hours = CurrentRowTasks.Quantity;
		EndIf;
	EndDo;

EndProcedure

