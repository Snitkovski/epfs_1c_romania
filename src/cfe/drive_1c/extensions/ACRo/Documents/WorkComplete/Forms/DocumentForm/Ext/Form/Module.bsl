﻿
&AtClient
Procedure RateOnChange(Item)
		
	//Object.Summ=Object.Tasks.Total("Summ");
	SummAndSummToBePaid();
	
EndProcedure

&AtClient
Procedure TasksQuantityOnChange(Item)
	
	CurrentRow=Items.Tasks.CurrentData;
	CurrentRow.Summ=CurrentRow.Quantity*CurrentRow.Rate;
	
	SummAndSummToBePaid();
	
EndProcedure

&AtClient
Procedure Command1(Command)
	TabDoc=Command1AtServer(False);
	
	TabDoc.Show();
	
EndProcedure

&AtServer
Function Command1AtServer(flagNoEmployee)
	
	TabDoc= New SpreadsheetDocument;
	Template = Documents.WorkComplete.GetTemplate("Template");
	
	//AreaPreHeader
	AreaPreHeader=Template.GetArea("PreHeader");
	
	Query = New Query;
	Query.Text =
	"SELECT
	|	Companies.Ref AS Ref,
	|	Companies.LogoFile AS CompanyLogoFile
	|FROM
	|	Catalog.Companies AS Companies
	|WHERE
	|	Companies.Ref = &Ref";
	
	Query.SetParameter("Ref",Object.Company);
	QuerySelect = Query.Execute().Select();
	
	While QuerySelect.Next() Do
		//AreaPreHeader.Parameters.CompLogo=QuerySelect.CompanyLogoFile;
		//LogoFile = QuerySelect.CompanyLogoFile; 
		
		If ValueIsFilled(QuerySelect.CompanyLogoFile) Then
			
			PictureData = AttachedFiles.GetBinaryFileData(QuerySelect.CompanyLogoFile);
			If ValueIsFilled(PictureData) Then
				AreaPreHeader.Drawings.Logo.Picture = New Picture(PictureData);
			EndIf;
		Else
			AreaPreHeader.Drawings.Delete(AreaPreHeader.Drawings.Logo);
		EndIf;
	EndDo;
	
	AreaPreHeader.Parameters.Company=Object.Company;
	AreaPreHeader.Parameters.CompFiscalCode = Object.Company.TIN;
	AreaPreHeader.Parameters.CompRegistrationNumber = Object.Company.RegistrationNumber;
	
	//AreaRequisites.Parameters.CompVATCode = Object.Company.VATNumber;
	InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Object.Company, Object.Date, ,);
	AreaPreHeader.Parameters.CompAdress=InfoAboutCompany.LegalAddress;
	AreaPreHeader.Parameters.CompPhone=InfoAboutCompany.PhoneNumbers;
	AreaPreHeader.Parameters.CompFax=InfoAboutCompany.Fax;
	AreaPreHeader.Parameters.CompEmail=InfoAboutCompany.Email;
	AreaPreHeader.Parameters.CompWebpage=InfoAboutCompany.Webpage;
	
	TabDoc.Put(AreaPreHeader);
	
	//AreaHeader
	AreaHeader=Template.GetArea("Header");
	
	//AreaHeader.Parameters.DateNumber=Format(Object.Date,"ДФ=ddMM")+" - "+ Format(Object.Number, "ЧВН=;ЧЦ=5");
	AreaHeader.Parameters.Company=Object.Company;
	AreaHeader.Parameters.Number=Format(Object.Number, "ЧВН=;ЧЦ=5");
	AreaHeader.Parameters.NContract=Object.Contract.ContractNo;
	AreaHeader.Parameters.Date=Format(Object.Contract.ContractDate,"DF=dd.MM.yyyy");
	TabDoc.Put(AreaHeader);
	
	//AreaHeaderTab 	

		AreaHeaderTab=Template.GetArea("Headertab");
		TabDoc.Put(AreaHeaderTab);

		For Each Line in Object.Tasks do

			If flagNoEmployee Then
				
				//	if Object.HoursToPay=0 then
				//		AreaLine=Template.GetArea("LineNoEmployeeNoPrepaid");
				//	else
				//		AreaLine=Template.GetArea("LineNoEmployee");
				//		//AreaLine.Parameters.Prepaid=Object.Prepaid;
				//	endif;
				//Else 
				//		if Object.HoursToPay=0 then
				//			AreaLine=Template.GetArea("LineNoPrepaid");
				//			AreaLine.Parameters.Employee=Line.Employee;
				//			//AreaLine=Template.GetArea("LineNoPrepaid1");
				//		else
				AreaLine=Template.GetArea("LineNoEmployee");
				//AreaLine=Template.GetArea("Line");
				//AreaLine.Parameters.Employee=Line.Employee;
				
				AreaLine.Parameters.Nr_p=Line.LineNumber;
				AreaLine.Parameters.Time=Line.quantity;
				AreaLine.Parameters.WorkDate=Format(Line.Date,"ДФ=dd.MM.yy");
				AreaLine.Parameters.Rate=Line.Rate;
				AreaLine.Parameters.Summ=Line.Summ;
				AreaLine.Parameters.Deal = Line.Comments;
			Else

				//AreaLine.Parameters.Nr_p=Line.LineNumber;
				//AreaLine.Parameters.WorkDate=Format(Line.Date,"ДФ=dd.MM.yy");
				
				//AreaLine.Parameters.TaskType = Line.TaskType;
				//////AreaLine.Parameters.Deal = Line.Comments;
				
				//IF Not object.UsePlannedTime Then 
				////AreaLine.Parameters.Time=Line.quantity;
				//////AreaLine.Parameters.Summ=Line.Summ ;
				////AreaLine.Parameters.Rate=Line.Rate;
				
				//Else 
				//	Quantity=Line.quantityPlan;
				//	Sum=Line.SummPlan ;
				//Endif;  		
				
				//STR1=?(INT(quantity)=0,"",String(Int(quantity))+" ore ");    
				//STR2=?((quantity-INT(quantity))=0,"",String(Round((quantity-INT(quantity))*60,0))+" min.");
				//STR=STR1+STR2;
				//AreaLine.Parameters.Time=STR;
				
				AreaLine=Template.GetArea("Line");
				//AreaLine.Parameters.Employee=Line.Employee; 	
				AreaLine.Parameters.Nr_p=Line.LineNumber;
				AreaLine.Parameters.Employee=Line.Employee;
				AreaLine.Parameters.Time=Line.quantity;
				AreaLine.Parameters.WorkDate=Format(Line.Date,"ДФ=dd.MM.yy");
				AreaLine.Parameters.Rate=Line.Rate;
				AreaLine.Parameters.Summ=Line.Summ;
				AreaLine.Parameters.Deal = Line.Comments;
				
			Endif;
			
			Tabdoc.put(AreaLine);
			
		Enddo;
		
	TotalSum=Object.Summ;
		
	TotalQuantity = Object.Tasks.Total("quantity");
	
	If Object.SummToBePaid < 0 Then
		//AreaHeaderTab=Template.GetArea("HeadertabNoPrepaid");
		AreaFooter=Template.GetArea("FooterHoursAvans");
		AreaActSum = Template.GetArea("ActSumHoursAvans");
		//TotalSum=Object.Summ;	
		//AreaActSum.Parameters.ActSum = Format (TotalSum,"ЧДЦ=2"); HoursToPay
		
		AreaFooter.Parameters.TotalSummToBePaid = "0";
		AreaFooter.Parameters.HoursToPay = -Object.HoursToPay;
		AreaFooter.Parameters.ActSum = TotalSum;
		AreaFooter.Parameters.TotalTime = TotalQuantity;
		AreaFooter.Parameters.rate =Object.Rate;
		AreaFooter.Parameters.AdditionalHours=AdditionalHours;
		AreaFooter.Parameters.SubscriptionHours=SubscriptionHours;

		AreaActSum.Parameters.ActSum = Format (TotalSum,"ЧДЦ=2");
		AreaActSum.Parameters.ActSum = TotalSum;
		AreaActSum.Parameters.HoursToPay = -Object.HoursToPay;
		AreaActSum.Parameters.TotalSummToBePaid = "0";
		Tabdoc.put(AreaFooter);
		TabDoc.Put(AreaActSum);
		
	Else
		
		AreaActSum = Template.GetArea("ActSum");
		
		//TotalSum=Object.Summ;	
		AreaActSum.Parameters.ActSum = Format (TotalSum,"ЧДЦ=2");
		AreaActSum.Parameters.ActSum = TotalSum;
		AreaActSum.Parameters.TotalSummToBePaid = Object.SummToBePaid;
		
		AreaFooter=Template.getArea("Footer");
		//AreaFooter.Parameters.TotalPrepaid =Object.Prepaid;
		//Format (Object.SummToBePaid,"ЧДЦ=2");
		AreaFooter.Parameters.TotalSummToBePaid = Object.SummToBePaid;
		AreaFooter.Parameters.HoursToPay = Object.HoursToPay;
		AreaFooter.Parameters.ActSum = TotalSum;
		AreaFooter.Parameters.TotalTime = TotalQuantity;
		AreaFooter.Parameters.rate =Object.Rate;
		AreaFooter.Parameters.AdditionalHours=AdditionalHours;
		AreaFooter.Parameters.SubscriptionHours=SubscriptionHours;
		Tabdoc.put(AreaFooter);
		TabDoc.Put(AreaActSum);

	Endif;

		//AreaFooter=Template.getArea("Footer");
		
		//IF Not object.UsePlannedTime Then 
		//TotalQuantity = Object.Tasks.Total("quantity");
		//TotalSum = Object.Tasks.Total("Summ");
		//Else 
		//	TotalQuantity =Object.Tasks.Total("QuantityPlan");
		//	TotalQuantity = Object.Tasks.Total("Summ");
		//Endif;
		//STR21=?(INT(TotalQuantity)=0,"",String(INT(TotalQuantity))+" ore ");    
		//STR22=?((TotalQuantity-INT(TotalQuantity))=0,"",String(Round((TotalQuantity-INT(TotalQuantity))*60,0,1))+" min.");
		//STR212=STR21+STR22;
		
		//AreaFooter.Parameters.TotalTime = TotalQuantity; 	
		//
		//////AreaFooter.Parameters.TotalSum = Object.Summ;
		//
		//AreaFooter.Parameters.rate =Object.Rate;
		//AreaFooter.Parameters.AdditionalHours=AdditionalHours;
		//AreaFooter.Parameters.SubscriptionHours=SubscriptionHours;

		//AreaFooter.Parameters.TotalTime = STR212; Object.Summ;
		//Tabdoc.put(AreaFooter);
		
		//AreaActSum = Template.GetArea("ActSum");
		
		//TotalSum=Object.Summ;
		//
		//AreaActSum.Parameters.ActSum = Format (TotalSum,"ЧДЦ=2");
		
		//AreaActSum.Parameters.Currency="Lei";
		
		//FromString = "Л = ro_Ro; ДП = Истина";
		//ObParam="leu, lei, M, ban, bani, W, 2"; 
		
		//AreaActSum.Parameters.SumLetters=NumberInWords(TotalSum,FromString,ObParam);
		//TabDoc.Put(AreaActSum);
		
		AreaRequisites = Template.GetArea("Requisites");
		AreaRequisites.Parameters.Company = Object.Company;
		AreaRequisites.Parameters.Counterparty=Object.Counterparty;
		
		TabDoc.Put(AreaRequisites);
		
		AreaSignature=Template.GetArea("Signature");
		
		Query = New Query(
		"SELECT TOP 1
		|	ResponsiblePersonsSliceLast.Employee AS Employee
		|FROM
		|	InformationRegister.ResponsiblePersons.SliceLast(
		|			&Date,
		|			Company = &Company
		|				AND ResponsiblePersonType = VALUE(Enum.ResponsiblePersonTypes.ChiefExecutiveOfficer)) AS ResponsiblePersonsSliceLast");
		
		Query.SetParameter("Company", Object.Company);
		Query.SetParameter("Date", Object.Date);
		
		Data = Query.Execute().Select();
		
		While Data.Next() Do
			AreaSignature.Parameters.CompResponsibleEmployee=Data.Employee;
		EndDo;
		
		If Object.Counterparty.ContactPerson.Position.Description = "Administrator" then
			
			AreaSignature.Parameters.CounterpartyRespEmployee= Object.Counterparty.ContactPerson;
			//TabDoc.Put(AreaSignature);
			
		EndIf;
		TabDoc.Put(AreaSignature);
		TabDoc.FitToPage = True;
		Return TabDoc;
		
EndFunction

	&AtClient
	Procedure Command2(Command)
		TabDoc=Command1AtServer(true);
		TabDoc.Show();
		
	EndProcedure

&AtServer
Procedure CommandCompleteTabAtServer()
		
	Query = New Query;
		Query.Text =
		    "SELECT
		    |	EmployeeTaskWorks.Ref AS Ref,
		    |	EmployeeTaskWorks.Day AS Day,
		    |	EmployeeTaskWorks.Ref.Employee AS Employee,
		    |	EmployeeTaskWorks.Ref.State AS State,
		    |	EmployeeTaskWorks.DurationInHours AS DurationInHours,
		    |	EmployeeTaskWorks.Price AS Price,
		    |	EmployeeTaskWorks.Amount AS Amount,
		    |	EmployeeTaskWorks.Comment AS Comment,
		    |	EmployeeTaskWorks.Customer AS Customer,
		    |	EmployeeTaskWorks.Customer.ContractByDefault AS CustomerContractByDefault,
		    |	EmployeeTaskWorks.Ref.Company AS Company,
		    |	EmployeeTaskWorks.Contract AS Contract
		    |INTO TT
		    |FROM
		    |	Document.EmployeeTask.Works AS EmployeeTaskWorks
		    |WHERE
		    |	EmployeeTaskWorks.Day BETWEEN &DateBegin AND &DateEnd
		    |;
		    |
		    |////////////////////////////////////////////////////////////////////////////////
		    |SELECT
		    |	TT.Ref AS Ref,
		    |	TT.Day AS Day,
		    |	TT.State AS State,
		    |	TT.Price AS Price,
		    |	TT.Amount AS Amount,
		    |	TT.Comment AS Comment,
		    |	TT.DurationInHours AS DurationInHours,
		    |	TT.CustomerContractByDefault AS CustomerContractByDefault,
		    |	TT.Customer AS Customer,
		    |	TT.Employee AS Employee,
		    |	TT.Company AS Company,
		    |	HoursWorkedBalance.HoursBalance AS HoursBalance,
		    |	HoursWorkedBalance.Company AS Company1,
		    |	HoursWorkedBalance.Counterparty AS Counterparty,
		    |	HoursWorkedBalance.Contract AS Contract,
		    |	HoursWorkedBalance.EmployeeTask AS EmployeeTask,
		    |	TT.Contract AS Contract1
		    |FROM
		    |	AccumulationRegister.HoursWorked.Balance(&DateEnd, ) AS HoursWorkedBalance
		    |		INNER JOIN TT AS TT
		    |		ON HoursWorkedBalance.EmployeeTask.Ref = TT.Ref
		    |			AND HoursWorkedBalance.Counterparty = TT.Customer
		    |			AND HoursWorkedBalance.Company = TT.Company
		    |WHERE
		    |	HoursWorkedBalance.Company = &Company
		    |	AND HoursWorkedBalance.Counterparty = &Counterparty
		    |	AND TT.Contract = &Contract
		    |
		    |ORDER BY
		    |	Day";
		
		Query.SetParameter("DateEnd", EndOfDay(Object.EndPeriod));
		Query.SetParameter("DateBegin", Object.StartPeriod);
		Query.SetParameter("Company", Object.Company);
		Query.SetParameter("Counterparty", Object.Counterparty);
		Query.SetParameter("Contract", Object.Contract);
		QueryResult = Query.Execute();
		
		SelectionDetailRecords = QueryResult.Select();
		
		While SelectionDetailRecords.Next() Do
			
				NewLine=Object.Tasks.Add();
				NewLine.Employee=SelectionDetailRecords.Employee;
				NewLine.Task=SelectionDetailRecords.Ref;
				NewLine.TaskType=SelectionDetailRecords.State;
				NewLine.Rate=SelectionDetailRecords.Price;
				NewLine.Date=SelectionDetailRecords.Day;
				NewLine.Quantity=SelectionDetailRecords.DurationInHours;
				NewLine.Summ=SelectionDetailRecords.Amount;
				NewLine.Comments=SelectionDetailRecords.Comment;
				Object.Rate= SelectionDetailRecords.Price;
			EndDo;
		Object.Tasks.Sort("Date Asc");
EndProcedure

&AtClient
Procedure CommandCompleteTab(Command)
	
	CommandCompleteTabAtServer();
	SummAndSummToBePaid();
	
EndProcedure

&AtClient
Procedure SummAndSummToBePaid()
	
	FreeHours = 0;
	ZeroHoursSum = 0;

	For each  CurrentRow In Object.Tasks Do
		
		If CurrentRow.ZeroHoursChoise = True then
			
			FreeHours = FreeHours + CurrentRow.Quantity;
			
		EndIf;
		
		 If CurrentRow.Quantity = 0 then
			
			ZeroHoursSum = ZeroHoursSum + CurrentRow.Summ;
			
		EndIf;
		
	EndDo;
	     		
	ActSumm =Object.Tasks.Total("Summ");
	Object.Summ = ActSumm;
	
	Structure = TasksOnActivateRowAtServer();
	Total_HoursPaidInAdvance = Structure.Total_HoursPaidInAdvance;
	//Hours =	Structure.Total_HoursToBePaid;
	HoursWorked = Object.Tasks.Total("Quantity");
	
	HoursToBePaid = HoursWorked - Total_HoursPaidInAdvance - FreeHours;
	Object.HoursToPay = HoursToBePaid;
	
	if HoursToBePaid>=0 then
		Items.Prepaid.TextColor = WebColors.Green;
	else
		Items.Prepaid.TextColor = WebColors.Red;
	endIf;
	
	Price = Object.Rate;
	
	SummToPay = HoursToBePaid*Price + ZeroHoursSum;
	Object.SummToBePaid = SummToPay;
	
	if SummToPay>=0 then
		Items.SummToBePaid.TextColor = WebColors.Red;
	else
		Items.SummToBePaid.TextColor = WebColors.Green;
	endIf;

	SubscriptionHoursPaid = SubscriptionHoursPaidAtServer();
	SubscriptionHours = SubscriptionHoursPaid;
	
	//Total_HoursPaidInAdvance = Structure.Total_HoursPaidInAdvance;
	
	//If SubscriptionHoursPaid < Total_HoursPaidInAdvance then
	
	AdditionalHours = Total_HoursPaidInAdvance - SubscriptionHoursPaid;
	
EndProcedure

&AtClient
Procedure ContractStartChoice(Item, ChoiceData, StandardProcessing)
	FormParameters = New Structure;

 FormParameters.Insert("Counterparty", Object.Counterparty);
 FormParameters.Insert("Company", Object.Company);
	FormParameters.Insert("ControlContractChoice", True);

		StandardProcessing = False;
		OpenForm("Catalog.CounterpartyContracts.Form.ChoiceForm", FormParameters, Item);

EndProcedure

&AtClient
Procedure Clear(Command)
	Object.Tasks.Clear();
EndProcedure

//&AtClient
//Procedure TasksTaskTypeOnChange(Item)
//	
//	 CurrentRow = Items.Tasks.CurrentData;
//	 Task = CurrentRow.Task;
//	 TaskType = CurrentRow.TaskType;
//	 
//	 If string(TaskType) <>  "Completed" Then
//		 
//		  TasksTaskOnChangeAtServer(TaskType,Task);
//		 
//	EndIf  
//	       	  
//EndProcedure

//&AtServer
//Procedure TasksTaskOnChangeAtServer(TaskType,Task)
//	
//	TaskObject=Task.GetObject();
//	TaskObject.State=TaskType;
//	 	  TaskObject.Write();
//EndProcedure

&AtClient
Procedure TasksTaskOnChange(Item)
	
	CurrentRow = Items.Tasks.CurrentData;
	 Task = CurrentRow.Task;
	 CurrentRow.TaskType = TasksTaskOnChangeAtServer(Task);
	 
 EndProcedure
 
 &AtServer
Function TasksTaskOnChangeAtServer(Task)
	
	TaskObject=Task.GetObject();
	TaskType = TaskObject.State;
	
	Return TaskType;
	 	  
EndFunction

&AtClient
Procedure TasksSummOnChange(Item)
		
	Object.Summ = Object.Tasks.Total("Summ");
	SummAndSummToBePaid();
	
	//Recount();
EndProcedure

//&AtClient
//Procedure Recount()
//	
//	Object.SummToBePaid = Object.Summ - Object.Prepaid;
//	//Object.SummToBePaid = Max(SummToBePaid,0);
//	  
//EndProcedure	

//&AtClient
//Procedure SummOnChange(Item)
//	 Object.SummToBePaid = Object.Summ - Object.Prepaid;
//EndProcedure

//&AtClient
//Procedure PrepaidOnChange(Item)
//	 Recount();
//EndProcedure

&AtServer
Procedure ContractOnChangeAtServer()
	if Object.Contract.owner <> Object.Counterparty Or Object.Contract.Company<>Object.Company then
		Message("This contract is not from this counterparty") ;
		//Object.rate=0;
		 //Object.Contract=Catalogs.CounterpartyContracts.EmptyRef();
		
		else
	
	//Object.Rate= Object.Contract.Rate; 	
	endif;
	
EndProcedure

&AtClient
Procedure ContractOnChange(Item)
	ContractOnChangeAtServer();
EndProcedure

///&AtClient
//Procedure TasksOnActivateRow(Item)

&AtServer
Function TasksOnActivateRowAtServer()

	Query = New Query;
	Query.Text =
	     "SELECT
	     |	HoursPaidInAdvanceBalance.HoursBalance AS HoursPaidInAdvanceBalance,
	     |	0 AS HoursWorkedBalance
	     |INTO TT
	     |FROM
	     |	AccumulationRegister.HoursPaidInAdvance.Balance(
		 //|			&BoundaryEnd,
		 |			&EndOfDate,
	     |			Company = &Company
	     |				AND Counterparty = &Counterparty
	     |				AND Contract = &Contract) AS HoursPaidInAdvanceBalance
	     |
	     |UNION ALL
	     |
	     |SELECT
	     |	0,
	     |	HoursWorkedBalance.HoursBalance
	     |FROM
	     |	AccumulationRegister.HoursWorked.Balance(
		 //|			&BoundaryEnd,
		 |			&EndOfDate,
	     |			Company = &Company
	     |				AND Counterparty = &Counterparty
	     |				AND Contract = &Contract) AS HoursWorkedBalance
	     |;
	     |
	     |////////////////////////////////////////////////////////////////////////////////
	     |SELECT
	     |	SUM(ISNULL(TT.HoursWorkedBalance, 0) - ISNULL(TT.HoursPaidInAdvanceBalance, 0)) AS Total_HoursToBePaid,
	     |	SUM(ISNULL(TT.HoursPaidInAdvanceBalance, 0)) AS Total_HoursPaidInAdvance
	     |FROM
	     |	TT AS TT";
	                  		 
	 //Boundary = New Boundary(Object.Date, BoundaryType.Excluding);
	 //Query.SetParameter("BoundaryEnd", Boundary); 
	 Query.SetParameter("Counterparty", Object.Counterparty);
	 //Query.SetParameter("EndOfDate", EndOfDay(Object.Date));
	 Query.SetParameter("EndOfDate", Object.Date);
	 
	 Query.SetParameter("Company", Object.Company);
	 Query.SetParameter("Contract", Object.Contract);
	 QueryResult = Query.Execute().Select();

	While QueryResult.Next() Do
		//Total_AvansOre = QueryResult.Total_AvansOre;
		
		Record = New Structure;
		Record.Insert("Total_HoursToBePaid", QueryResult.Total_HoursToBePaid);
		Record.Insert("Total_HoursPaidInAdvance", QueryResult.Total_HoursPaidInAdvance);
		
	EndDo;
	  
	Return  Record;
	
EndFunction

&AtServer
Function SubscriptionHoursPaidAtServer()
	
	Query = New Query;
	Query.Text =
	"SELECT
	|	SUM(ISNULL(SubscriptionHoursTurnovers.HoursReceipt, 0)) AS HoursReceipt
	|FROM
	|	Document.WorkComplete AS WorkComplete
	|		LEFT JOIN AccumulationRegister.SubscriptionHours.Turnovers(&BeginOfPeriod, &EndOfPeriod, , ) AS SubscriptionHoursTurnovers
	|		ON WorkComplete.Company = SubscriptionHoursTurnovers.Company
	|			AND WorkComplete.Counterparty = SubscriptionHoursTurnovers.Counterparty
	|			AND WorkComplete.Contract = SubscriptionHoursTurnovers.Contract
	|			AND (WorkComplete.Ref = &Ref)";
	
	BeginOfPeriod = BegOfMonth(Date(Object.Date) -1);
	Query.SetParameter("Ref",Object.Ref );

	Query.SetParameter("BeginOfPeriod",BeginOfPeriod );
	Query.SetParameter("EndOfPeriod", EndOfDay(Object.Date));
	QueryResult = Query.Execute();
	If QueryResult.IsEmpty() Then
		Return 0
	Else
		Return QueryResult.Unload()[0][0]
	EndIf;

EndFunction

&AtClient
Procedure TasksZeroHoursChoiseOnChange(Item)
	
	CurrentRow=Items.Tasks.CurrentData;
	If CurrentRow.ZeroHoursChoise = True Then
		CurrentRow.Summ = 0;
	Else
		CurrentRow.Summ=CurrentRow.Quantity*CurrentRow.Rate;
	EndIf;
		
	SummAndSummToBePaid();
		
EndProcedure

&AtClient
Procedure OnOpen(Cancel)
	SummAndSummToBePaid();
EndProcedure

