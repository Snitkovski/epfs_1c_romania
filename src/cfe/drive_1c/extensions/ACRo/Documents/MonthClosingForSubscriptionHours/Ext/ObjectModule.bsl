﻿
Procedure Posting(Cancel, PostingMode)

	If Not Company=Catalogs.Companies.EmptyRef() Then
		
		Query = New Query;
		Query.Text =
		"SELECT
		|	ISNULL(SubscriptionHoursBalance.HoursBalance, 0) AS HoursBalance,
		|	SubscriptionHoursBalance.Company AS Company,
		|	SubscriptionHoursBalance.Counterparty AS Counterparty,
		|	SubscriptionHoursBalance.Contract AS Contract
		|FROM
		|	AccumulationRegister.SubscriptionHours.Balance(&Date, Company = &Company) AS SubscriptionHoursBalance";
		
		//Query.SetParameter("Date", Date);
		Query.SetParameter("Date", EndOfDay(Date));
		Query.SetParameter("Company", Company);
		
		QueryResult = Query.Execute();
		
		SelectionDetailRecords = QueryResult.Select();
				
		While SelectionDetailRecords.Next() Do
			
			//register SubscriptionHours
			//RegisterRecords.SubscriptionHours.Clear();
			RegisterRecords.SubscriptionHours.Write = True;
			
			Record = RegisterRecords.SubscriptionHours.Add();
			Record.RecordType = AccumulationRecordType.Expense;
			
			//Record.Period = Date;
			Record.Period = EndOfDay(Date);
			Record.Company = SelectionDetailRecords.Company;
			Record.Counterparty = SelectionDetailRecords.Counterparty;
			Record.Contract = SelectionDetailRecords.Contract;
			Record.Hours = SelectionDetailRecords.HoursBalance;

			////register HoursPaidInAdvance
			////RegisterRecords.HoursPaidInAdvance.Clear();
			//RegisterRecords.HoursPaidInAdvance.Write = True; 				
			//
			//Record = RegisterRecords.HoursPaidInAdvance.Add();			                                                   			
			//Record.Period = EndOfDay(Date);			
			//Record.Company = SelectionDetailRecords.Company;
			//Record.Counterparty = SelectionDetailRecords.Counterparty;
			//Record.Contract = SelectionDetailRecords.Contract; 			
			//Record.Hours = -SelectionDetailRecords.HoursBalance; 	
			
			//register HoursPaidInAdvance
			RegisterRecords.HoursPaidInAdvance.Write = True;
			
			Record = RegisterRecords.HoursPaidInAdvance.Add();
			Record.RecordType = AccumulationRecordType.Expense;
			Record.Period = EndOfDay(Date);
			Record.Company = SelectionDetailRecords.Company;
			Record.Counterparty = SelectionDetailRecords.Counterparty;
			Record.Contract = SelectionDetailRecords.Contract;
			Record.Hours = SelectionDetailRecords.HoursBalance;
			
		EndDo;
		
	Else
		Message(NStr("ru= 'Заполните поле Организация'; en='Complete field Company'; ro='Completați câmpul Compania'"));
	EndIf;
	
EndProcedure

