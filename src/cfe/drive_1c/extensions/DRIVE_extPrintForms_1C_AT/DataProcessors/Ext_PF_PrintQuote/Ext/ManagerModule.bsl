﻿//////////////////////////////////////////////////////////////////////
//
//  When we add new DataProcessor'а - must include it 
//    in AttachableReportsAndDataProcessors subsystem
//  Go to Additional Properties in new DataProcessor
//    (Alt-Shift-Enter on new Object)
//
//  При добавлении нового DataProcessor'а - обязательно необходимо 
//    добавить его в подсистему AttachableReportsAndDataProcessors
//  Для этого заходить в Additional Properties нового DataProcessor'а
//    (Alt-Shift-Enter на новом Объекте)
//

Procedure OnDefineSettings(Settings) Export
	
	// К какому объекту привязывается. Лучше задавать в виде строки, чтобы не тянуть объект в Расширение!
	// Metadata.Object for link to. It's better way - as String, in this case we don't to pull Object into Extension
    Settings.Placement.Add("Document.Quote");
    Settings.AddPrintCommands = True;

EndProcedure

Procedure AddPrintCommands(PrintCommands) Export

	PrintFormsOwner = Ext_roAdditionalFunctionality.Ext_roAdditionalPrint();
	PrintFormsOwner = ?(IsBlankString(PrintFormsOwner), "Pers.", PrintFormsOwner);
	
	NewCommand = PrintCommands.Add();
	NewCommand.Presentation = "Oferta Comerciala" + " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_Quote"; 				 		 // Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True; 			 // Validity check Before print - Проверка проведения перед печатью
                                               
EndProcedure		// AddPrintCommands()

Procedure Print(ObjectsArray, PrintParameters, PrintFormsCollection, PrintObjects, OutputParameters) Export
    
	//If NOT PrintParameters.Property("Result") Then
	//	PrintParameters.Insert("Result", New Structure);
	//EndIf;
	
	If NOT PrintParameters.Property("Result") OR PrintParameters.Result = Undefined Then
		PrintParameters.Insert("Result", New Structure);
	EndIf;

	// "Ext_Quote" must be as NewCommand.ID above
    If PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_Quote") Then
        
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_OfertaComerc");
        
		// Call standart PrintHandler. You should pull the real Object into Extension
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintQuote(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        
		// Standart PrintForm output - Стандартный вывод печатной формы
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_Quote", "Oferta Comerciala", PrintForms);
    EndIf;
    
EndProcedure	//	Print()

//////////////////////////////////////////////////////////////////////
// Document printing procedure.
//
//
//Function PrintQuote(ObjectsArray, PrintObjects, TemplateName, PrintParams, AdditionalParameters = Undefined)
Function PrintQuote(ObjectsArray, PrintObjects, PrintParams, AdditionalParameters = Undefined)

	DisplayPrintOption = False;
	PrintParams.Insert("Copies", 1);
	
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_СommercOfer";
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_СommercOfer";
	
	Query = New Query();
	Query.SetParameter("ObjectsArray", ObjectsArray);
	Query.SetParameter("ReverseChargeAppliesRate", NStr("en = 'Reverse charge applies';
														|tr = 'Karşı ödemeli ücret uygulanır';
														|ro = 'Se aplică taxa inversă';
														|pl = 'Dotyczy odwrotnego obciążenia';
														|ru = 'Применяется агентский НДС';
														|de = 'Steuerschuldumkehr angewendet';
														|es_ES = 'Inversión impositiva aplica';
														|es_CL = 'Inversión impositiva aplica'"));
	//Query.SetParameter("AllVariants", StrFind(TemplateName, "AllVariants") > 0);
	Query.SetParameter("AllVariants", False);
	
	// Composing data Query with standart Function (modified lightly)
	//Query.Text = DataProcessors.PrintQuote.GetQuoteQueryTextForQuote(False);
	Query.Text = GetQueryTextForQuote(False);
	ResultArray = Query.ExecuteBatch();
	
	FirstDocument = True;

	LinesMaxNumber = 44;
	//isParameterizedPF = ?(AdditionalParameters = Undefined, False, True);
	//isParameterizedPF = True;
	
	HeaderVariants = ResultArray[4].Select(QueryResultIteration.ByGroups);
	
	// Bundles
	TableColumns = ResultArray[4].Columns;
	// End Bundles
	
	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
	
	// Reading ALL print areas  from Template received
	TitleArea = Template.GetArea("Title");
	CompanyInfoArea = Template.GetArea("CompanyInfo");
	CounterpartyInfoArea = Template.GetArea("CounterpartyInfo");
	
	LineHeaderArea = Template.GetArea("LineHeader");
	LineSectionArea	= Template.GetArea("LineSection");
	LineSectAddInfoArea = Template.GetArea("LineSectionAddInfo");
	LineSectManufactArea = Template.GetArea("LineSectionManufact");
	LineSectProdComntArea = Template.GetArea("LineSectionProdComnt");
	
	CommentArea = Template.GetArea("Comment");
	LineTotalArea = Template.GetArea("LineTotal");
	LineTotalEmptyArea = Template.GetArea("LineTotalEmpty");
	
	SeeNextPageArea	= Template.GetArea("SeeNextPage");
	EmptyLineArea	= Template.GetArea("EmptyLine");
	
	AreasToBeChecked = New Array;
	TotalsAreasArray = New Array;
	LineSectionAreasArray = New Array;
	
	NextPageAreasArray = New Array;
	NextPageAreasArray.Add(EmptyLineArea);
	NextPageAreasArray.Add(LineTotalEmptyArea);
	NextPageAreasArray.Add(SeeNextPageArea);

	//	HeaderVariants.Next()
	While HeaderVariants.Next() Do
		Header = HeaderVariants.Select(QueryResultIteration.ByGroups);
		
		// Header.Next() 
		While Header.Next() Do
			
			If Not FirstDocument Then
				SpreadsheetDocument.PutHorizontalPageBreak();
			EndIf;
			
			FirstDocument = False;
			FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
			
#Region PrintQuoteTitleArea
			TitleArea.Parameters.Fill(Header);
			
			If ValueIsFilled(Header.CompanyLogoFile) Then
				
				PictureData = AttachedFiles.GetBinaryFileData(Header.CompanyLogoFile);
				If ValueIsFilled(PictureData) Then
					TitleArea.Drawings.Logo.Picture = New Picture(PictureData);
				EndIf;
			Else
				TitleArea.Drawings.Delete(TitleArea.Drawings.Logo);
			EndIf;
			
			SpreadsheetDocument.Put(TitleArea);
#EndRegion
			
#Region PrintQuoteCompanyInfoArea
			InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Header.Company,
															Header.DocumentDate, ,
															Header.BankAccount,
															Header.CompanyVATNumber);
			CompanyInfoArea.Parameters.Fill(InfoAboutCompany);
			BarcodesInPrintForms.AddBarcodeToTableDocument(CompanyInfoArea, Header.Ref);
			SpreadsheetDocument.Put(CompanyInfoArea);
#EndRegion
			
#Region PrintQuoteCounterpartyInfoArea
			CounterpartyInfoArea.Parameters.Fill(Header);
			
			InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);
			CounterpartyInfoArea.Parameters.Fill(InfoAboutCounterparty);
			
			CounterpartyInfoArea.Parameters.PaymentTerms = PaymentTermsServer.TitlePaymentTerms(Header.Ref);
			If ValueIsFilled(CounterpartyInfoArea.Parameters.PaymentTerms) Then
				CounterpartyInfoArea.Parameters.PaymentTermsTitle = PaymentTermsServer.PaymentTermsPrintTitle();
			EndIf;
			
			SpreadsheetDocument.Put(CounterpartyInfoArea);
#EndRegion
			
#Region PrintQuoteCommentArea
			CommentArea.Parameters.SalesRepresentative = Common.ObjectAttributeValue(Header.Ref, "SalesRep");
			
			If Common.HasObjectAttribute("TermsAndConditions", Header.Ref.Metadata()) Then
				CommentArea.Parameters.TermsAndConditions = Common.ObjectAttributeValue(Header.Ref, "TermsAndConditions");
			Else
				CommentArea.Parameters.TermsAndConditions = Common.ObjectAttributeValue(Header.Ref, "Comment");
			EndIf;
			
			SpreadsheetDocument.Put(CommentArea);
#EndRegion
			
#Region PrintQuoteTotalsAreaPrefill
			LineTotalArea.Parameters.Fill(Header);
#EndRegion
			
#Region PrintQuoteLines
       		SpreadsheetDocument.Put(LineHeaderArea);

			TableInventoty = BundlesServer.AssemblyTableByBundles(Header.Ref, Header, TableColumns, LineTotalArea);
			EmptyColor = LineSectionArea.CurrentArea.TextColor;
			
/////////////  TableInventoty Cicle  ////////////////////
	#Region PrintQuoteTableInventotyCicle
			For Each TabSelection In TableInventoty Do
				
				LineSectionArea.Parameters.Fill(TabSelection);

				IsAddInfo = False;
				IsProdComnt = False;
				IsManufact = False;
				//If isParameterizedPF Then 
					LineSectAddInfoArea.Parameters.AdditionalInformation = TabSelection.AdditionalInformation;
					IsAddInfo = LineSectAddInfoArea.Parameters.AdditionalInformation <> Undefined  And
									Not IsBlankString(LineSectAddInfoArea.Parameters.AdditionalInformation);
					
					LineSectProdComntArea.Parameters.ProductComment = Common.ObjectAttributeValue(TabSelection.Products.Ref, "Comment");
					IsProdComnt = LineSectProdComntArea.Parameters.ProductComment <> Undefined  And
									Not IsBlankString(LineSectProdComntArea.Parameters.ProductComment);
					
					LineSectManufactArea.Parameters.Manufacturer = Common.ObjectAttributeValue(TabSelection.Products.Ref, "Manufacturer");
					IsManufact = LineSectManufactArea.Parameters.Manufacturer <> Undefined And
									Not IsBlankString(LineSectManufactArea.Parameters.Manufacturer);
					
				//EndIf;
				
				DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection);
				
				// Display selected codes if functional option is turned on.
				//If DisplayPrintOption Then
				//	CodesPresentation = PrintManagementServerCallDrive.GetCodesPresentation(PrintParams, TabSelection.Products);
				//	If PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.SeparateColumn Then
				//		LineSectionArea.Parameters.SKU = CodesPresentation;
				//	ElsIf PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.ProductColumn Then
				//		LineSectionArea.Parameters.ProductDescription = LineSectionArea.Parameters.ProductDescription + Chars.CR + CodesPresentation;                    
				//	EndIf;
				//EndIf;    
				
				// Bundles
				LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
				// End Bundles
				
				// Line Section Areas for checking
				LineSectionAreasArray.Clear();
				LineSectionAreasArray.Add(EmptyLineArea);
				
				LineSectionAreasArray.Add(LineSectionArea);
				
				If IsAddInfo Then
					LineSectionAreasArray.Add(LineSectAddInfoArea);
				EndIf;
				
				If IsProdComnt Then
					LineSectionAreasArray.Add(LineSectProdComntArea);
				EndIf;
				
				If IsManufact Then
					LineSectionAreasArray.Add(LineSectManufactArea);
				EndIf;

				//LineSectionAreasArray.Clear();
				LineSectionAreasArray.Add(LineTotalArea);
				LineSectionAreasArray.Add(SeeNextPageArea);
				
				If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, LineSectionAreasArray) Then

					//If isParameterizedPF Then 
						SpreadsheetDocument.Put(EmptyLineArea);
					//EndIf;
					
					If IsAddInfo Then
						SpreadsheetDocument.Put(LineSectAddInfoArea);
					EndIf;
					
					SpreadsheetDocument.Put(LineSectionArea);
					
					If IsProdComnt Then
						SpreadsheetDocument.Put(LineSectProdComntArea);
					EndIf;
					
					If IsManufact Then
						SpreadsheetDocument.Put(LineSectManufactArea);
					EndIf;
				Else
		#Region PrintQuoteEmptyLines
					For i = 1 To LinesMaxNumber Do
						If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, NextPageAreasArray) Or i >= LinesMaxNumber Then
							SpreadsheetDocument.Put(LineTotalEmptyArea);
							Break;
						Else
							SpreadsheetDocument.Put(EmptyLineArea);
						EndIf;
					EndDo;
		#EndRegion
					SpreadsheetDocument.Put(SeeNextPageArea);
					
					SpreadsheetDocument.PutHorizontalPageBreak();
					SpreadsheetDocument.Put(TitleArea);
					SpreadsheetDocument.Put(LineHeaderArea);

					SpreadsheetDocument.Put(EmptyLineArea);
					
					If IsAddInfo Then
						SpreadsheetDocument.Put(LineSectAddInfoArea);
					EndIf;
					
					SpreadsheetDocument.Put(LineSectionArea);

					If IsProdComnt Then
						SpreadsheetDocument.Put(LineSectProdComntArea);
					EndIf;
					
					If IsManufact Then
						SpreadsheetDocument.Put(LineSectManufactArea);
					EndIf;
				EndIf;
			EndDo;
	#EndRegion		// TableInventoty Cicle
/////////////  TableInventoty Cicle  ////////////////////
#EndRegion
			
#Region PrintQuoteTotalsArea
			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(LineTotalArea);
			
			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				SpreadsheetDocument.Put(LineTotalEmptyArea);
				SpreadsheetDocument.Put(SeeNextPageArea);
				SpreadsheetDocument.PutHorizontalPageBreak();
				
				SpreadsheetDocument.Put(TitleArea);
				SpreadsheetDocument.Put(LineHeaderArea);
			EndIf;

			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(EmptyLineArea);
			AreasToBeChecked.Add(LineTotalArea);
			
	#Region PrintQuoteEmptyLines
			For i = 1 To 99 Do
				If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(LineTotalArea);
					Break;
				Else
					SpreadsheetDocument.Put(EmptyLineArea);
				EndIf;
			EndDo;
	#EndRegion
#EndRegion

			//	DON'T MOVE THIS LINE - only INSIDE the While the Header.Ref is visible
			PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
		EndDo;
		// Header.Next() 
	EndDo;
	// HeaderVariants 

#Region PrintQuoteFooter
	SpreadsheetDocument.Footer.Enabled = True;
	SpreadsheetDocument.Footer.Font = new Font("Calibri", 9);
	SpreadsheetDocument.Footer.RightText = "Pagina [&PageNumber] din [&PagesTotal]";
#EndRegion
	
	SpreadsheetDocument.PageSize = "A4";
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;
	
EndFunction		// PrintQuote()

Function GetQueryTextForQuote(AllVariants)
	
	QueryText =
	"SELECT ALLOWED
	|	Quote.Ref AS Ref,
	|	Quote.Number AS Number,
	|	Quote.Date AS Date,
	|	Quote.Company AS Company,
	|	Quote.CompanyVATNumber AS CompanyVATNumber,
	|	Quote.Counterparty AS Counterparty,
	|	Quote.Contract AS Contract,
	|	Quote.BankAccount AS BankAccount,
	|	Quote.AmountIncludesVAT AS AmountIncludesVAT,
	|	Quote.DocumentCurrency AS DocumentCurrency,
	|	Quote.ValidUntil AS ValidUntil,
	|	CASE
	|		WHEN Quote.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN &ReverseChargeAppliesRate
	|		ELSE """"
	|	END AS ReverseChargeApplies,
	|	Quote.PreferredVariant AS PreferredVariant
	|INTO Quotes
	|FROM
	|	Document.Quote AS Quote
	|WHERE
	|	Quote.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Quote.Ref AS Ref,
	|	Quote.Number AS DocumentNumber,
	|	Quote.Date AS DocumentDate,
	|	Quote.Company AS Company,
	|	Quote.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	Quote.Counterparty AS Counterparty,
	|	Quote.Contract AS Contract,
	|	Quote.BankAccount AS BankAccount,
	|	CASE
	|		WHEN CounterpartyContracts.ContactPerson = VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN Counterparties.ContactPerson
	|		ELSE CounterpartyContracts.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	Quote.AmountIncludesVAT AS AmountIncludesVAT,
	|	Quote.DocumentCurrency AS DocumentCurrency,
	|	Quote.ValidUntil AS ValidUntil,
	|	Quote.ReverseChargeApplies AS ReverseChargeApplies,
	|	Quote.PreferredVariant AS PreferredVariant
	|INTO Header
	|FROM
	|	Quotes AS Quote
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON Quote.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON Quote.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON Quote.Contract = CounterpartyContracts.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	QuoteInventory.Ref AS Ref,
	|	QuoteInventory.LineNumber AS LineNumber,
	|	QuoteInventory.Products AS Products,
	|	QuoteInventory.Characteristic AS Characteristic,
	|	VALUE(Catalog.ProductsBatches.EmptyRef) AS Batch,
	|	QuoteInventory.Quantity AS Quantity,
	|	QuoteInventory.MeasurementUnit AS MeasurementUnit,
	|	QuoteInventory.Price * (QuoteInventory.Total - QuoteInventory.VATAmount) / QuoteInventory.Amount AS Price,
	|	QuoteInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	QuoteInventory.Total - QuoteInventory.VATAmount AS Amount,
	|	QuoteInventory.VATRate AS VATRate,
	|	QuoteInventory.VATAmount AS VATAmount,
	|	QuoteInventory.Total AS Total,
	|	QuoteInventory.Content AS Content,
	|	QuoteInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	QuoteInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	QuoteInventory.ConnectionKey AS ConnectionKey,
	|	QuoteInventory.Variant AS Variant,
	|	QuoteInventory.BundleProduct AS BundleProduct,
	|	QuoteInventory.BundleCharacteristic AS BundleCharacteristic,
	|	QuoteInventory.AdditionalInformation AS AdditionalInformation
	|INTO FilteredInventory
	|FROM
	|	Document.Quote.Inventory AS QuoteInventory
	|WHERE
	|	QuoteInventory.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.BankAccount AS BankAccount,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
//	|	MIN(FilteredInventory.LineNumber) AS LineNumber,						//	!!!
	|	FilteredInventory.LineNumber AS LineNumber,
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	FilteredInventory.Price AS Price,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SUM(FilteredInventory.Amount) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
	|	SUM(FilteredInventory.Total) AS Total,
	|	FilteredInventory.Price * SUM(FilteredInventory.Quantity) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.Batch AS Batch,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	Header.ValidUntil AS ValidUntil,
	|	Header.ReverseChargeApplies AS ReverseChargeApplies,
	|	FilteredInventory.Variant AS Variant,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic,
	|	FilteredInventory.AdditionalInformation AS AdditionalInformation		//	!!!
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|			AND (Header.PreferredVariant = FilteredInventory.Variant
	|				OR &AllVariants)
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	FilteredInventory.VATRate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Counterparty,
	|	Header.Contract,
	|	CatalogProducts.SKU,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	Header.CompanyLogoFile,
	|	Header.DocumentNumber,
	|	Header.DocumentCurrency,
	|	Header.Ref,
	|	Header.DocumentDate,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Products,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.Batch,
	|	FilteredInventory.MeasurementUnit,
	|	Header.ValidUntil,
	|	Header.ReverseChargeApplies,
	|	FilteredInventory.Variant,
	|	FilteredInventory.Price,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic,
	|	FilteredInventory.LineNumber,                 							//	!!!
	|	FilteredInventory.AdditionalInformation									//	!!!
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.BankAccount AS BankAccount,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountMarkupPercent
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Amount AS Amount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	Tabular.ValidUntil AS ValidUntil,
	|	Tabular.ReverseChargeApplies AS ReverseChargeApplies,
	|	Tabular.Variant AS Variant,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic,
	|	Tabular.Products AS Products,
	|	Tabular.AdditionalInformation AS AdditionalInformation
	|FROM
	|	Tabular AS Tabular
	|
	|ORDER BY
	|	DocumentNumber,
	|	Variant,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(CounterpartyContactPerson),
	|	MAX(BankAccount),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
	|	COUNT(LineNumber),
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(DiscountAmount),
	|	MAX(ValidUntil),
	|	MAX(ReverseChargeApplies)
	|BY
	|	Ref,
	|	Variant";
	
	Return QueryText;
	
EndFunction		//	GetQuoteQueryTextForQuote()
