﻿//////////////////////////////////////////////////////////////////////
//
//  When we add new DataProcessor'а - must include it 
//    in AttachableReportsAndDataProcessors subsystem
//  Go to Additional Properties in new DataProcessor
//    (Alt-Shift-Enter on new Object)
//
//  При добавлении нового DataProcessor'а - обязательно необходимо 
//    добавить его в подсистему AttachableReportsAndDataProcessors
//  Для этого заходить в Additional Properties нового DataProcessor'а
//    (Alt-Shift-Enter на новом Объекте)
//

Procedure OnDefineSettings(Settings) Export
    
	// К какому объекту привязывается. Лучше задавать в виде строки, чтобы не тянуть объект в Расширение!
	// Metadata.Object for link to. It's better way - as String, in this case we don't need to pull Object into Extension
    Settings.Placement.Add("Document.PurchaseOrder");
    Settings.AddPrintCommands = True;

EndProcedure

Procedure AddPrintCommands(PrintCommands) Export

	PrintFormsOwner = Ext_roAdditionalFunctionality.Ext_roAdditionalPrint();
	PrintFormsOwner = ?(IsBlankString(PrintFormsOwner), "Pers.", PrintFormsOwner);
	
	NewCommand = PrintCommands.Add();
	NewCommand.Presentation = "Comanda la Furnizor (cu pret)";// PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_PurchaseOrder1";			 // Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True;		 // Validity check Before print - Проверка проведения перед печатью
	
	NewCommand = PrintCommands.Add();
	NewCommand.Presentation = "Comanda la Furnizor (fara pret)";// PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_PurchaseOrder2";			 // Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True;		 // Validity check Before print - Проверка проведения перед печатью
	
EndProcedure

Procedure Print(ObjectsArray, PrintParameters, PrintFormsCollection, PrintObjects, OutputParameters) Export
	
	//If NOT PrintParameters.Property("Result") Then
	//	PrintParameters.Insert("Result", New Structure);
	//EndIf;
	
	If NOT PrintParameters.Property("Result") OR PrintParameters.Result = Undefined Then
		PrintParameters.Insert("Result", New Structure);
	EndIf;

	// "Ext_PurchaseOrder" must be as NewCommand.ID above
    If PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_PurchaseOrder1") Then
        
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate, WithPrice", "Document.Print.PF_MXL_ComandaFurnizor", True);
		
		// Call own PrintHandler - see below
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintPurchaseOrder(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        
		// Standart PrintForm output - Стандартный вывод печатной формы
		// "Ext_ProformaInvoice" must be as NewCommand.ID above
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_PurchaseOrder1", "", PrintForms);
		
	ElsIf PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_PurchaseOrder2") Then

        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate, WithPrice", "Document.Print.PF_MXL_ComandaFurnizor", False);
        
		// Call own PrintHandler - see below
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintPurchaseOrder(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        
		// Standart PrintForm output - Стандартный вывод печатной формы
		// "Ext_ProformaInvoice" must be as NewCommand.ID above
	    PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_PurchaseOrder2", "", PrintForms);

	EndIf;
	
EndProcedure

//////////////////////////////////////////////////////////////////////
// <Function description>
//
// Parameters:
//  <Parameter1>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//  <Parameter2>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//
// Returns:
//   <Type.Subtype>   - <returned value description>
//
//  copied from document PurchaseOrder (ManagerModule) and adapted
Function PrintPurchaseOrder(ObjectsArray, PrintObjects, PrintParams = Undefined, AdditionalParameters = Undefined)
    
    DisplayPrintOption = False;
	PrintParams.Insert("Copies", 1);
    
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_ComandaFurnizor";
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_ComandaFurnizor";
	SpreadsheetDocument.FitToPage = True;
	
	Query = New Query();
	Query.SetParameter("ObjectsArray", ObjectsArray);
	
#Region PrintPurchaseOrderQueryText
	Query.Text = Documents.PurchaseOrder.QueryTextInTermsOfSupplier();
#EndRegion
	
	ResultArray = Query.Execute();
	Header = ResultArray.Select(QueryResultIteration.ByGroupsWithHierarchy);

	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
	
	TitleArea = Template.GetArea("Title");
	If AdditionalParameters.WithPrice Then
		LineHeaderArea = Template.GetArea("LineHeaderWithPrice");
		LineSectionArea	= Template.GetArea("LineSectionWithPrice");
	Else
		LineHeaderArea = Template.GetArea("LineHeaderWithoutPrice");
		LineSectionArea	= Template.GetArea("LineSectionWithoutPrice");
	EndIf;

	EmptyLineArea = Template.GetArea("EmptyLine");
	SeeNextPageArea	= Template.GetArea("SeeNextPage");
	PageNumberArea	= Template.GetArea("PageNumber");

	FirstDocument = True;

	// Header.Next() 
	While Header.Next() Do
		
		If Not FirstDocument Then
			SpreadsheetDocument.PutHorizontalPageBreak();
		EndIf;
		
		FirstDocument = False;
		FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
		
#Region PrintPurchaseOrderTitleArea
		TitleArea.Parameters.Fill(Header);
		InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);
		TitleArea.Parameters.FullDescr = InfoAboutCounterparty.FullDescr;
#EndRegion
		
#Region PrintOrderConfirmationLinesArea
		LineHeaderArea.Parameters.Fill(Header);
       
        SpreadsheetDocument.Put(LineHeaderArea);
		
		PageNumber = 0;
		
		TabSelection = Header.Select();
/////////////  TableInventoty Cicle  ////////////////////
		While TabSelection.Next() Do
			
			LineSectionArea.Parameters.Fill(TabSelection);
			
			AreasToBeChecked = New Array;
			AreasToBeChecked.Add(LineSectionArea);
			AreasToBeChecked.Add(PageNumberArea);
			
			If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				SpreadsheetDocument.Put(LineSectionArea);
			Else
				
				SpreadsheetDocument.Put(SeeNextPageArea);
				
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(EmptyLineArea);
				AreasToBeChecked.Add(PageNumberArea);
				
				For i = 1 To 99 Do
					If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
						
						PageNumber = PageNumber + 1;
						PageNumberArea.Parameters.PageNumber = PageNumber;
						SpreadsheetDocument.Put(PageNumberArea);
						Break;
					Else
						SpreadsheetDocument.Put(EmptyLineArea);
					EndIf;
				EndDo;
				
				SpreadsheetDocument.PutHorizontalPageBreak();
				SpreadsheetDocument.Put(TitleArea);
				SpreadsheetDocument.Put(LineHeaderArea);
				SpreadsheetDocument.Put(LineSectionArea);
			EndIf;
		EndDo;
/////////////  TableInventoty Cicle  ////////////////////
#EndRegion
		
#Region PrintOrderConfirmationTotalsArea
		AreasToBeChecked.Clear();
		AreasToBeChecked.Add(EmptyLineArea);
		AreasToBeChecked.Add(PageNumberArea);
		
		For i = 1 To 99 Do
			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				
				PageNumber = PageNumber + 1;
				PageNumberArea.Parameters.PageNumber = PageNumber;
				SpreadsheetDocument.Put(PageNumberArea);
				Break;
			Else
				SpreadsheetDocument.Put(EmptyLineArea);
			EndIf;
		EndDo;
#EndRegion
		
		PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
	EndDo;
	// Header.Next() 
	
	SpreadsheetDocument.PageSize = "A4";
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;
	
EndFunction		//  PrintPurchaseOrder()
