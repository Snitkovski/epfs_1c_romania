﻿//////////////////////////////////////////////////////////////////////
//
//  When we add new DataProcessor'а - must include it 
//    in AttachableReportsAndDataProcessors subsystem
//  Go to Additional Properties in new DataProcessor
//    (Alt-Shift-Enter on new Object)
//
//  При добавлении нового DataProcessor'а - обязательно необходимо 
//    добавить его в подсистему AttachableReportsAndDataProcessors
//  Для этого заходить в Additional Properties нового DataProcessor'а
//    (Alt-Shift-Enter на новом Объекте)
//

Procedure OnDefineSettings(Settings) Export
    
	// К какому объекту привязывается. Лучше задавать в виде строки, чтобы не тянуть объект в Расширение!
	// Metadata.Object for link to. It's better way - as String, in this case we don't need to pull Object into Extension
    Settings.Placement.Add("Document.SalesInvoice");
    Settings.AddPrintCommands = True;

EndProcedure

Procedure AddPrintCommands(PrintCommands) Export

	PrintFormsOwner = Ext_roAdditionalFunctionality.Ext_roAdditionalPrint();
	PrintFormsOwner = ?(IsBlankString(PrintFormsOwner), "Pers.", PrintFormsOwner);
	
	NewCommand	= PrintCommands.Add();
	NewCommand.Presentation = "Factura Fiscala" + " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_FacturaFiscala";			// Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True;	// Validity check Before print - Проверка проведения перед печатью
												   
	NewCommand	= PrintCommands.Add();
	NewCommand.Presentation = "Garantia" + " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_Garantia";			// Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True;	// Validity check Before print - Проверка проведения перед печатью
	
	NewCommand	= PrintCommands.Add();
	NewCommand.Presentation = "Dispoziție de Livrare" + " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_DispozitieLivrare";			// Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True;	// Validity check Before print - Проверка проведения перед печатью
	
EndProcedure

Procedure Print(ObjectsArray, PrintParameters, PrintFormsCollection, PrintObjects, OutputParameters) Export
	
	//If NOT PrintParameters.Property("Result") Then
	//	PrintParameters.Insert("Result", New Structure);
	//EndIf;
	
	If NOT PrintParameters.Property("Result") OR PrintParameters.Result = Undefined Then
		PrintParameters.Insert("Result", New Structure);
	EndIf;

	// "Ext_FacturaFiscala" must be as NewCommand.ID above
    If PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_FacturaFiscala") Then
        
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_FacturaFiscala");
		
		// Call standart PrintHandler. You should pull the real Object into Extension
		// Name "TaxInvoice" should strictly as mentioned in PrintManagementServerCallDrive.IsDocumentInPrintOptionsList()
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintTaxInvoice(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
		
		// Standart PrintForm output - Стандартный вывод печатной формы
		// "Ext_FacturaFiscala" must be as NewCommand.ID above
		PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_FacturaFiscala", "", PrintForms);
		
	// "Ext_Garantia" must be as NewCommand.ID above
	ElsIf PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_Garantia") Then
		
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_Garantia");
        
		// Call own PrintHandler - see below
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintWarrantyCard(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        
		// Standart PrintForm output - Стандартный вывод печатной формы
		// Name "Ext_Garantia" doesn't matter - isn't typical PF
		// "Ext_Garantia" must be as NewCommand.ID above
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_Garantia", "", PrintForms);
		
	// "Ext_DispozitieLivrare" must be as NewCommand.ID above
	ElsIf PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_DispozitieLivrare") Then
		
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_DispozitieLivrare");
        
		// Call own PrintHandler - see below
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintDeliveryNote(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        
		// Standart PrintForm output - Стандартный вывод печатной формы
		// Name "Ext_Garantia" doesn't matter - isn't typical PF
		// "Ext_Garantia" must be as NewCommand.ID above
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_DispozitieLivrare", "", PrintForms);
		
    EndIf;
	
EndProcedure

///////////////////////////////////////////////////////////////////////////
// Procedure for generating DispozitieLivrare print form from Tax Invoice
//
Function PrintDeliveryNote(ObjectsArray, PrintObjects, PrintParams = Undefined, AdditionalParameters = Undefined)
    
    DisplayPrintOption = False;
	PrintParams.Insert("Copies", 1);
	
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_DispozitieLivrare";
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_DispozitieLivrare";
	SpreadsheetDocument.FitToPage = True;
	
	Query = New Query();
	Query.SetParameter("ObjectsArray", ObjectsArray);

	Query.Text = GetQueryText4DeliveryNote();
	ResultArray = Query.ExecuteBatch();
	
	LinesPerPage = 47;
	
	Header = ResultArray[4].Select(QueryResultIteration.ByGroupsWithHierarchy);
	SalesOrdersNumbersHeaderSel	= ResultArray[5].Select(QueryResultIteration.ByGroupsWithHierarchy);
	SerialNumbersSel = ResultArray[6].Select();
	
	// Bundles
	TableColumns = ResultArray[4].Columns;
	// End Bundles
	
	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
	
	TitleCompanyInfoArea = Template.GetArea("TitleCompanyInfo");
	CounterpartyInfoArea = Template.GetArea("CounterpartyInfo");
	CommentArea = Template.GetArea("Comment");
	
	SeeNextPageArea	= Template.GetArea("SeeNextPage");
	EmptyLineArea = Template.GetArea("EmptyLine");
	PageNumberArea = Template.GetArea("PageNumber");
	
	LineHeaderArea = Template.GetArea("LineHeader");
	LineSectionArea	= Template.GetArea("LineSection");
	
	LineTotalArea = Template.GetArea("LineTotal");
	
	FirstDocument = True;
	
	While Header.Next() Do
		
		If Not FirstDocument Then
			SpreadsheetDocument.PutHorizontalPageBreak();
		EndIf;
		
		FirstDocument = False;
		FirstLineNumber = SpreadsheetDocument.TableHeight + 1;

#Region PrintDeliveryNoteTitleArea
		TitleCompanyInfoArea.Parameters.Fill(Header);
        
        If DisplayPrintOption Then
            TitleCompanyInfoArea.Parameters.OriginalDuplicate = ?(PrintParams.OriginalCopy, NStr("en = 'ORIGINAL'"), NStr("en = 'COPY'"));
		EndIf;
#EndRegion
		
#Region PrintDeliveryNoteCompanyInfoArea
		InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Header.Company, Header.DocumentDate, , , Header.CompanyVATNumber);
		
		TitleCompanyInfoArea.Parameters.CompanyFullDescr = InfoAboutCompany.FullDescr;
		TitleCompanyInfoArea.Parameters.CompanyLegalAddress = InfoAboutCompany.LegalAddress;
		TitleCompanyInfoArea.Parameters.CompanyWebpage = InfoAboutCompany.Webpage;

		TitleCompanyInfoArea.Parameters.Depozit1 = Header.StructuralUnit;
		
		If Header.StructuralUnit.AdditionalAttributes.Count() > 0 Then
			TitleCompanyInfoArea.Parameters.Depozit2 = Header.StructuralUnit.AdditionalAttributes[0].Value;
		EndIf;
		
		SpreadsheetDocument.Put(TitleCompanyInfoArea);
#EndRegion
		
#Region PrintDeliveryNoteCounterpartyInfoArea
		CounterpartyInfoArea.Parameters.Fill(Header);
		
		InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);
		CounterpartyInfoArea.Parameters.Fill(InfoAboutCounterparty);
		
		CounterpartyContactPersonName = Header.Counterparty.ContactPerson.Description;
		CounterpartyContactPersonPosition = Header.Counterparty.ContactPerson.Position.Description;
		InfoAboutContactPerson = DriveServer.InfoAboutContactPerson(Header.Counterparty.ContactPerson);
		
		CounterpartyContactPerson = "";
		If NOT IsBlankString(CounterpartyContactPersonName) Then
			CounterpartyContactPerson = CounterpartyContactPerson + CounterpartyContactPersonName;
		EndIf;
		If NOT IsBlankString(CounterpartyContactPersonPosition) Then
			CounterpartyContactPerson = CounterpartyContactPerson +
										?(IsBlankString(CounterpartyContactPersonName), "", ", ") +
										CounterpartyContactPersonPosition;
		EndIf;
		
		CounterpartyInfoArea.Parameters.CounterpartyContactPerson = CounterpartyContactPerson;

		If NOT IsBlankString(InfoAboutContactPerson.PhoneNumbers) Then
			CounterpartyInfoArea.Parameters.ContactPersonPhoneNumber = InfoAboutContactPerson.PhoneNumbers;
		EndIf;
		
		If Header.DeliveryOption = Enums.DeliveryOptions.SelfPickup Then
			CommentArea.Parameters.DeliveryOption = "RIDICA CLIENTUL";
			
			// Our Warehouse, our WarehouseKeeper
			InfoAboutPickupLocation	= DriveServer.InfoAboutLegalEntityIndividual(Header.StructuralUnit, Header.DocumentDate);
			PickupLocResponsibleEmployee = InfoAboutPickupLocation.ResponsibleEmployee;
			
			If NOT IsBlankString(InfoAboutPickupLocation.DeliveryAddress) Then
				CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutPickupLocation.DeliveryAddress;
			EndIf;
			
		Else
			CommentArea.Parameters.DeliveryOption = "LIVRARE";
			
			InfoAboutShippingAddress = DriveServer.InfoAboutShippingAddress(Header.ShippingAddress);
			DeliveryAddressContactPersonName = Header.ShippingAddress.ContactPerson.Description;
			DeliveryAddressContactPersonPosition = Header.ShippingAddress.ContactPerson.Position.Description;
			
			DeliveryAddressContactPerson = "";
			If NOT IsBlankString(DeliveryAddressContactPersonName) Then
				DeliveryAddressContactPerson = DeliveryAddressContactPersonName;
			EndIf;
			If NOT IsBlankString(DeliveryAddressContactPersonPosition) Then
				DeliveryAddressContactPerson = DeliveryAddressContactPerson +
										?(IsBlankString(DeliveryAddressContactPersonName), "", ", ") +
										DeliveryAddressContactPersonPosition;
			EndIf;
			CounterpartyInfoArea.Parameters.DeliveryAddressContactPerson = DeliveryAddressContactPerson;
			
			If NOT IsBlankString(InfoAboutShippingAddress.DeliveryAddress) Then
				CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutShippingAddress.DeliveryAddress;
			Else
				If Not IsBlankString(InfoAboutCounterparty.LegalAddress) Then
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutCounterparty.LegalAddress;
				EndIf;
			EndIf;
			
			InfoAboutContactPerson = DriveServer.InfoAboutContactPerson(Header.ShippingAddress.ContactPerson);
			If NOT IsBlankString(InfoAboutContactPerson.PhoneNumbers) Then
				CounterpartyInfoArea.Parameters.DeliveryAddrPhoneNum = InfoAboutContactPerson.PhoneNumbers;
			EndIf;
		EndIf;
		
		SpreadsheetDocument.Put(CounterpartyInfoArea);
#EndRegion
		
#Region PrintDeliveryNoteCommentArea
		SalesOrdersNumbersHeaderSel.Reset();
		If SalesOrdersNumbersHeaderSel.FindNext(New Structure("Ref", Header.Ref)) Then
			
			SalesOrdersNumbersArray = New Array;
			SalesOrdersDatesArray = New Array;
			
			SalesOrdersNumbersSel = SalesOrdersNumbersHeaderSel.Select();
			While SalesOrdersNumbersSel.Next() Do
				SalesOrdersNumbersArray.Add(SalesOrdersNumbersSel.Number);
				SalesOrdersDatesArray.Add(Format(SalesOrdersNumbersSel.Date, "DLF=D"));
			EndDo;
			
			CommentArea.Parameters.SalesOrders = StringFunctionsClientServer.StringFromSubstringArray(SalesOrdersNumbersArray, " ");
			CommentArea.Parameters.SalesOrdersDates = StringFunctionsClientServer.StringFromSubstringArray(SalesOrdersDatesArray, " ");
		EndIf;
		
		// Responsible person FROM DOCUMENT !
		//CommentArea.Parameters.Responsible = Header.Ref.Responsible;
		// Responsible person FROM PARTNER
		//CommentArea.Parameters.Responsible = Header.Counterparty.Responsible;
		// Sales person FROM PARTNER
		CommentArea.Parameters.Responsible = Header.Counterparty.SalesRep;
		CommentArea.Parameters.PrintingDateTime = Left(CurrentDate(), StrLen(CurrentDate()) -3);
		SpreadsheetDocument.Put(CommentArea);
#EndRegion
		
		SpreadsheetDocument.Put(LineHeaderArea);
		PageNumber = 0;
		
		LineTotalArea.Parameters.Fill(Header);
		
		// Bundles
		TableInventoty = BundlesServer.AssemblyTableByBundles(Header.Ref, Header, TableColumns, LineTotalArea);
		EmptyColor = LineSectionArea.CurrentArea.TextColor;
		// End Bundles
		
#Region PrintDeliveryNoteLinesArea
/////////////  TableInventoty Cicle  ////////////////////
		For Each TabSelection In TableInventoty Do
			
			LineSectionArea.Parameters.Fill(TabSelection);
			
			DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection, SerialNumbersSel);

			LineSectionArea.Parameters.Vendor = TabSelection.Products.Manufacturer;
			
            // Display selected codes if functional option is turned on.
            If DisplayPrintOption Then
                CodesPresentation = PrintManagementServerCallDrive.GetCodesPresentation(PrintParams, TabSelection.Products);
                If PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.SeparateColumn Then
                    LineSectionArea.Parameters.SKU = CodesPresentation;
                ElsIf PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.ProductColumn Then
                    LineSectionArea.Parameters.ProductDescription = LineSectionArea.Parameters.ProductDescription + Chars.CR + CodesPresentation;
                EndIf;
            EndIf;
            
			// Bundles  
            If DisplayPrintOption And PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                LineSectionArea.Areas.LineSectionWithoutCode.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
            Else
                LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
            EndIf;
			// End Bundles

			//  MODDIFY HERE !!!
			
			AreasToBeChecked = New Array;
			AreasToBeChecked.Add(LineSectionArea);
			AreasToBeChecked.Add(LineTotalArea);
			AreasToBeChecked.Add(SeeNextPageArea);
			AreasToBeChecked.Add(EmptyLineArea);
			AreasToBeChecked.Add(PageNumberArea);
			
			//If TabSelection.LineNumber < ((PageNumber+1) * LinesPerPage)  Then
			If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
						And TabSelection.LineNumber < ((PageNumber+1) * LinesPerPage)  Then
				SpreadsheetDocument.Put(LineSectionArea);
			Else
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(LineTotalArea);
				AreasToBeChecked.Add(SeeNextPageArea);
				AreasToBeChecked.Add(EmptyLineArea);
				AreasToBeChecked.Add(EmptyLineArea);
				AreasToBeChecked.Add(EmptyLineArea);
				AreasToBeChecked.Add(PageNumberArea);
				
				For i = 1 To 99 Do
					If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
						
						PageNumber = PageNumber + 1;
						PageNumberArea.Parameters.PageNumber = PageNumber;
						
						SpreadsheetDocument.Put(SeeNextPageArea);
						SpreadsheetDocument.Put(EmptyLineArea);
						SpreadsheetDocument.Put(LineTotalArea);
						SpreadsheetDocument.Put(PageNumberArea);
						Break;
					Else
						SpreadsheetDocument.Put(EmptyLineArea);
					EndIf;
				EndDo;
				
				SpreadsheetDocument.PutHorizontalPageBreak();
				SpreadsheetDocument.Put(LineHeaderArea);
				SpreadsheetDocument.Put(LineSectionArea);
			EndIf;
		EndDo;
/////////////  TableInventoty Cicle  ////////////////////
#EndRegion
		
#Region PrintDeliveryNoteTotalsArea
		AreasToBeChecked.Clear();
		AreasToBeChecked.Add(LineTotalArea);
		AreasToBeChecked.Add(EmptyLineArea);
		AreasToBeChecked.Add(PageNumberArea);
        
		For i = 1 To 99 Do
			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked, False) Then
				
				PageNumber = PageNumber + 1;
				PageNumberArea.Parameters.PageNumber = PageNumber;
				
				SpreadsheetDocument.Put(LineTotalArea);
				SpreadsheetDocument.Put(PageNumberArea);
				Break;
			Else
				SpreadsheetDocument.Put(EmptyLineArea);
			EndIf;
		EndDo;
#EndRegion
		PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
	EndDo;
	
	PrintForms2Ex = New SpreadsheetDocument;
	CalorStruct = New Map;
	CalorStruct.Insert(1, New Structure("EXEMPLAR, FOR", "EXEMPLAR", "Client"));
	CalorStruct.Insert(2, New Structure("EXEMPLAR, FOR", "EXEMPLAR", "Gestiune"));
	
	ExemplarArea = SpreadsheetDocument.Area("R5C10");
	For Each ElemCalorStruct in CalorStruct Do
		ExemplarArea.Text = StrTemplate("%1: %2", ElemCalorStruct.Value.EXEMPLAR, ElemCalorStruct.Value.FOR);
		PrintForms2Ex.Put(SpreadsheetDocument);
	EndDo;
	
	PrintForms2Ex.PageSize = "A4";
	PrintForms2Ex.FitToPage = True;
	Return PrintForms2Ex;
	
EndFunction		// PrintDeliveryNote()

////////////////////////////////////////////////////////////////////////
// Procedure for generating FacturaFiscala print form from Tax Invoice
//
Function PrintTaxInvoice(ObjectsArray, PrintObjects, PrintParams = Undefined, AdditionalParameters = Undefined)
    
    DisplayPrintOption = False;
	PrintParams.Insert("Copies", 1);
    
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_FacturaFiscala";
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_FacturaFiscala";
	SpreadsheetDocument.FitToPage = True;
	
	// Composing data Query with standart Function (modified light)
	Query = New Query(GetQueryText4TaxInvoice());
	Query.SetParameter("ObjectsArray", ObjectsArray);
	Query.SetParameter("ReverseChargeAppliesRate", NStr("en='Reverse charge applies';
														|ro='Se aplică taxare inversă';
														|ru='Применяется взаимозачёт по НДС'"));
	ResultArray = Query.ExecuteBatch();
	
	LinesPerPage = 21;
	PrintableDocuments = New Array;
	
	Header = ResultArray[9].Select(QueryResultIteration.ByGroups);
	TaxesHeaderSel = ResultArray[10].Select(QueryResultIteration.ByGroups);
	SerialNumbersSel = ResultArray[11].Select();
	
	// Bundles
	TableColumns = ResultArray[9].Columns;
	// End Bundles
	
	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
	
	// Reading ALL print areas  from Template received
	TitleArea = Template.GetArea("Title");
	CompanyCounterpartyInfoArea = Template.GetArea("CompanyCounterpartyInfo");
	LineHeaderArea = Template.GetArea("LineHeader");
	LineSectionArea	= Template.GetArea("LineSection");
	SeeNextPageArea = Template.GetArea("SeeNextPage");
	EmptyLineArea = Template.GetArea("EmptyLine");
	CommentArea = Template.GetArea("Comment");
	
	LineTotalArea = Template.GetArea("LineTotalWithoutDiscount");
	LineTotalEmptyArea = Template.GetArea("LineTotalWithoutTotals");
	
	PageNumberArea = Template.GetArea("PageNumber");
	
	AreasToBeChecked = New Array;
	
	NextPageAreasArray = New Array;
	NextPageAreasArray.Add(EmptyLineArea);
	NextPageAreasArray.Add(CommentArea);
	NextPageAreasArray.Add(LineTotalArea);
	NextPageAreasArray.Add(PageNumberArea);
	
	TotalsAndTaxesAreasArray = New Array;
	TotalsAndTaxesAreasArray.Add(LineSectionArea);
	TotalsAndTaxesAreasArray.Add(EmptyLineArea);
	
	TotalsAndTaxesAreasArray.Add(CommentArea);
	TotalsAndTaxesAreasArray.Add(LineTotalEmptyArea);
	
	TotalsAndTaxesAreasArray.Add(SeeNextPageArea);
	TotalsAndTaxesAreasArray.Add(EmptyLineArea);
	TotalsAndTaxesAreasArray.Add(PageNumberArea);
	
	FirstDocument = True;
	
	While Header.Next() Do
		
		If Not FirstDocument Then
			SpreadsheetDocument.PutHorizontalPageBreak();
			FirstDocument = False;
		EndIf;
		
		FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
		
#Region PrintTaxInvoiceTitleArea
		TitleArea.Parameters.Fill(Header);
        
        If DisplayPrintOption Then
            TitleArea.Parameters.OriginalDuplicate = ?(PrintParams.OriginalCopy, NStr("en='ORIGINAL';ro='ORIGINAL';ru='ОРИГИНАЛ'"), NStr("en='COPY';ro='COPY';ru='КОПИЯ'"));
		EndIf;
		
		If ValueIsFilled(Header.CompanyLogoFile) Then
			
			PictureData = AttachedFiles.GetBinaryFileData(Header.CompanyLogoFile);
			If ValueIsFilled(PictureData) Then
				Try
					TitleArea.Drawings.Logo.Picture = New Picture(PictureData);
				Except
				EndTry;
			EndIf;
		Else
			TitleArea.Drawings.Delete(TitleArea.Drawings.Logo);
		EndIf;
		
		SpreadsheetDocument.Put(TitleArea);
#EndRegion
		
#Region PrintTaxInvoiceCompanyInfoArea
		CompanyCounterpartyInfoArea.Parameters.DocumentDate = Header.DocumentDate;
		CompanyCounterpartyInfoArea.Parameters.DocumentNumber = Header.DocumentNumber;
		
		InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Header.Company, Header.DocumentDate, , , Header.CompanyVATNumber);
		
		CompanyCounterpartyInfoArea.Parameters.CompanyFullDescr = InfoAboutCompany.FullDescr;
		CompanyCounterpartyInfoArea.Parameters.CompanyRegistrationNumber = TrimAll(InfoAboutCompany.RegistrationNumber);
		CompanyCounterpartyInfoArea.Parameters.CompanyVATNumber = InfoAboutCompany.TIN;
		CompanyCounterpartyInfoArea.Parameters.CompanyLegalAddress = InfoAboutCompany.LegalAddress;
		CompanyCounterpartyInfoArea.Parameters.CompanyCapitalSocial = InfoAboutCompany.CapitalSocial;
		
		CompanyCounterpartyInfoArea.Parameters.CompanyBank = InfoAboutCompany.Bank;
 		CompanyCounterpartyInfoArea.Parameters.CompanyIBAN = InfoAboutCompany.IBAN;
#EndRegion
		
#Region PrintTaxInvoiceCounterpartyInfoArea
		InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);
		
		CompanyCounterpartyInfoArea.Parameters.CounterpartyFullDescr = InfoAboutCounterparty.FullDescr;
		CompanyCounterpartyInfoArea.Parameters.CounterpartyVATNumber = InfoAboutCounterparty.TIN;
		CompanyCounterpartyInfoArea.Parameters.CounterpartyRegistrationNumber = InfoAboutCounterparty.RegistrationNumber;
		CompanyCounterpartyInfoArea.Parameters.CounterpartyLegalAddress = TrimAll(InfoAboutCounterparty.LegalAddress);
		CompanyCounterpartyInfoArea.Parameters.CounterpartyBank = InfoAboutCounterparty.Bank;
 		CompanyCounterpartyInfoArea.Parameters.CounterpartyIBAN = InfoAboutCounterparty.IBAN;
		
		SpreadsheetDocument.Put(CompanyCounterpartyInfoArea);
#EndRegion
		
#Region PrintTaxInvoiceTotalsAndTaxesAreaPrefill
		LineTotalArea.Parameters.Fill(Header);

		If Header.Ref.Metadata().Name = "TaxInvoiceIssued" Then
			If Header.Ref.BasisDocuments[0].BasisDocument.PaymentCalendar.Count() > 0  Then
				CommentArea.Parameters.ReferenceDate = Format(Header.Ref.BasisDocuments[0].BasisDocument.PaymentCalendar[0].PaymentDate, "DF=dd.MM.yyyy");
			Else
				CommentArea.Parameters.ReferenceDate = Format(Header.ReferenceDate, "DF=dd.MM.yyyy");
			EndIf;
		Else
			If Header.Ref.PaymentCalendar.Count() > 0  Then
				CommentArea.Parameters.ReferenceDate = Format(Header.Ref.PaymentCalendar[0].PaymentDate, "DF=dd.MM.yyyy");
			Else
				CommentArea.Parameters.ReferenceDate = Format(Header.ReferenceDate, "DF=dd.MM.yyyy");
			EndIf;
		EndIf;
#EndRegion
        // Line Header
   		SpreadsheetDocument.Put(LineHeaderArea);
		PageNumber = 0;
		
#Region PrintTaxInvoiceDocsArea
		DocumentSelection = Header.Select(QueryResultIteration.ByGroups);
		While DocumentSelection.Next() Do
			
			BasisDocumentMetadata = DocumentSelection.BasisDocument.Metadata();
			DocumentType = BasisDocumentMetadata.ExtendedObjectPresentation;
			If IsBlankString(DocumentType) Then
				DocumentType = BasisDocumentMetadata.ObjectPresentation;
			EndIf;
			If IsBlankString(DocumentType) Then
				DocumentType = BasisDocumentMetadata.Presentation();
			EndIf;
			
			// Bundles
			TableInventoty = BundlesServer.AssemblyTableByBundles(DocumentSelection.BasisDocument, DocumentSelection, TableColumns, LineTotalArea);
			EmptyColor = LineSectionArea.CurrentArea.TextColor;
			// End Bundles
			
	#Region PrintTaxInvoiceLinesArea
			FirstLine = True;

/////////////  TableInventoty Cicle  ////////////////////
			For Each TabSelection In TableInventoty Do
				
				If FirstLine Then
					FirstLine = False;
					DocumentVATRate = TabSelection.VATRate;
				EndIf;
				
				LineSectionArea.Parameters.Fill(TabSelection);
				
				DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection, SerialNumbersSel);
                
                // Bundles
				LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
				// End Bundles

			//  MODDIFY HERE !!!
			
				If TabSelection.LineNumber <= ((PageNumber+1) * LinesPerPage)  Then
				//If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, TotalsAndTaxesAreasArray) 
				//		And TabSelection.LineNumber < ((PageNumber+1) * LinesPerPage)  Then
					SpreadsheetDocument.Put(LineSectionArea);
				Else
					PageNumber = PageNumber + 1;
					PageNumberArea.Parameters.PageNumber = PageNumber;
					
					SpreadsheetDocument.Put(SeeNextPageArea);
					SpreadsheetDocument.Put(EmptyLineArea);
					
					SpreadsheetDocument.Put(CommentArea);
					SpreadsheetDocument.Put(LineTotalEmptyArea);

					SpreadsheetDocument.Put(PageNumberArea);
					SpreadsheetDocument.PutHorizontalPageBreak();
					
					SpreadsheetDocument.Put(TitleArea);
					SpreadsheetDocument.Put(CompanyCounterpartyInfoArea);
					
					SpreadsheetDocument.Put(LineHeaderArea);
					SpreadsheetDocument.Put(LineSectionArea);
					Continue;
				EndIf;
			EndDo;
/////////////  TableInventoty Cicle  ////////////////////
	#EndRegion
			PrintableDocuments.Add(DocumentSelection.BasisDocument);
		EndDo;
#EndRegion

#Region PrintTaxInvoiceTotalsAndTaxesArea
		
		//AreasToBeChecked.Clear();
		//AreasToBeChecked.Add(EmptyLineArea);
		//AreasToBeChecked.Add(CommentArea);
		//AreasToBeChecked.Add(LineTotalArea);
		//AreasToBeChecked.Add(PageNumberArea);
		
		LastLineNumber = TabSelection.LineNumber - (PageNumber * LinesPerPage);
		
		//For i = 1 To 99 Do
		For i = LastLineNumber To LinesPerPage +5 Do
			//If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
			//////If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, NextPageAreasArray) Then
			//////	
			//////	PageNumber = PageNumber + 1;
			//////	PageNumberArea.Parameters.PageNumber = PageNumber;

			//////	SpreadsheetDocument.Put(CommentArea);
			//////	SpreadsheetDocument.Put(LineTotalArea);
			//////	SpreadsheetDocument.Put(PageNumberArea);
			//////	Break;
			//////Else
				SpreadsheetDocument.Put(EmptyLineArea);
			//////EndIf;
		EndDo;
		
		PageNumber = PageNumber + 1;
		PageNumberArea.Parameters.PageNumber = PageNumber;

		SpreadsheetDocument.Put(CommentArea);
		SpreadsheetDocument.Put(LineTotalArea);
		SpreadsheetDocument.Put(PageNumberArea);
#EndRegion
		PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
	EndDo;
	
	If ObjectsArray.Count() <> PrintableDocuments.Count() Then
		Errors = Undefined;
		For Each Document In ObjectsArray Do
			If PrintableDocuments.Find(Document) = Undefined Then
				If TypeOf(Document) = Type("DocumentRef.TaxInvoiceIssued") then
					MessagePattern = NStr("en='Tax invoice %1 cannot be printed. Please fill in all the required fields and try again.';
										  |ro='Factura Fiscală %1 nu poate fi listată. Vă rugăm să completați toate câmpurile necesare și să încercați din nou.';
										  |ru='Налоговая Накладная %1 не может быть напечатана. Заполните все требуемые поля и попробуйте ещё раз.'");
				Else
					MessagePattern = NStr("en='Generate Tax invoice document for %1 before printing.';
										  |ro='Generați documentul cu factură fiscală %1 înainte de imprimare.';
										  |ru='Создайте документ Счет-фактура для %1 перед печатью.'");
				EndIf;
				
				MessageText = StringFunctionsClientServer.SubstituteParametersToString(MessagePattern, Document);
				CommonClientServer.AddUserError(Errors,, MessageText, Undefined);
			EndIf;
		EndDo;
		
		CommonClientServer.ReportErrorsToUser(Errors);
	EndIf;
	
	SpreadsheetDocument.PageSize = "A4";
	SpreadsheetDocument.FitToPage = True;
	
	//	Attention!
	//	SpreadsheetDocument.RepeatOnRowPrint()
	
	PrintForms3Ex = New SpreadsheetDocument;
	CalorStruct = New Map;
	CalorStruct.Insert(1, New Structure("COLOR, FOR", "Albastru", "Client"));
	CalorStruct.Insert(2, New Structure("COLOR, FOR", "Rosu", "Gestiune"));
	CalorStruct.Insert(3, New Structure("COLOR, FOR", "Verde", "Contabilitate"));
	
	For Each ElemCalorStruct in CalorStruct Do
		For Page = 1 To PageNumber Do
			Number4Line = 22 + (Page-1)*64;
			ExemplarArea = SpreadsheetDocument.Area("R" + Number4Line + "C11");
			SpreadsheetDocument.Area("R" + Number4Line + "C7").Text = "Cota TVA: " + DocumentVATRate;
			ExemplarArea.Text = StrTemplate("Ex. %1, Ex. %2, pt. %3", ElemCalorStruct.Key, ElemCalorStruct.Value.Color, ElemCalorStruct.Value.FOR);
		EndDo;
		PrintForms3Ex.PutHorizontalPageBreak();
		PrintForms3Ex.Put(SpreadsheetDocument);
	EndDo;
	
	PrintForms3Ex.PageSize = "A4";
	PrintForms3Ex.FitToPage = True;
	
	Return PrintForms3Ex;
	
EndFunction		// PrintTaxInvoice()

////////////////////////////////////////////////////////////
// Document printing procedure.
// 
// Parameters
//		Variant = "PerSerialNumber"  or  "Consolidated"   -  (typical, not for CLIENT)
//
Function PrintWarrantyCard(ObjectsArray, PrintObjects, PrintParams, AdditionalParameters = Undefined)
	
	PrintParams.Insert("Copies", 1);
	
	Variant = "Consolidated";
	DocumentType = "SalesInvoice";
	TableName = "Inventory";
	
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_WarrantyCard" + Variant;
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_" + DocumentType + "_WarrantyCard" + Variant;
	SpreadsheetDocument.FitToPage = True;

	// First Line Number for the second page
	LinesMaxNumber = 37;
	
	AreasToBeChecked = New Array;

	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
	
	// Reading ALL print areas  from Template received
	TitleArea = Template.GetArea("Title");
	CompanyInfoArea = Template.GetArea("CompanyInfo");
	CounterpartyInfoArea = Template.GetArea("CounterpartyInfo");
	CommentArea = Template.GetArea("Comment");
	ConsolidatedCardHeaderArea = Template.GetArea("ConsolidatedCardHeader");
	ConsolidatedLineArea = Template.GetArea("ConsolidatedLine");
	ConsolidatedFooterArea = Template.GetArea("ConsolidatedFooter");
	
	GarantyTextAreasArray = New Array;
	For i = 1  To 12 Do
		GarantyTextAreasArray.Add(Template.GetArea("GarantyText" + i));
	EndDo;
		
	EmptyLineArea = Template.GetArea("EmptyLine");
	
	// Composing data Query
	Query = New Query;
	Query.SetParameter("ObjectsArray", ObjectsArray);
	
	Query.Text = GetQueryText4WarrantyCard();
	Header = Query.Execute().Select(QueryResultIteration.ByGroups);
	
	FirstDocument = True;
	
	While Header.Next() Do
		
		If Not FirstDocument Then
			SpreadsheetDocument.PutHorizontalPageBreak();
		EndIf;
		
		FirstDocument = False;
		FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
		
#Region PrintGarantyTitle
		TitleArea.Parameters.Fill(Header);
		SpreadsheetDocument.Put(TitleArea);
#EndRegion
			
#Region PrintGarantyCompanyInfo
		InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Header.Company, Header.DocumentDate,, Header.BankAccount, Header.CompanyVATNumber);
		CompanyInfoArea.Parameters.Fill(InfoAboutCompany);
#EndRegion
			
#Region PrintGarantyCounterpartyInfo
		CounterpartyInfoArea.Parameters.Fill(Header);
		
		InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);
		CounterpartyInfoArea.Parameters.Fill(InfoAboutCounterparty);
		
		If ValueIsFilled(Header.CompanyLogoFile) Then
			
			PictureData = AttachedFiles.GetBinaryFileData(Header.CompanyLogoFile);
			If ValueIsFilled(PictureData) Then
				CounterpartyInfoArea.Drawings.Logo.Picture = New Picture(PictureData);
			EndIf;
		Else
			CounterpartyInfoArea.Drawings.Delete(TitleArea.Drawings.Logo);
		EndIf;
		
		If IsBlankString(CounterpartyInfoArea.Parameters.DeliveryAddress) Then
			
			If Not IsBlankString(InfoAboutCounterparty.ActualAddress) Then
				CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutCounterparty.ActualAddress;
			Else
				CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutCounterparty.LegalAddress;
			EndIf;
		EndIf;
		
		SpreadsheetDocument.Put(CounterpartyInfoArea);
#EndRegion
			
#Region PrintGarantyComment
		CommentArea.Parameters.Fill(Header);
		
		If Header.DeliveryOption = Enums.DeliveryOptions.SelfPickup Then
			CommentArea.Parameters.DeliveryOption = "RIDICA CLIENTUL";
		Else
			CommentArea.Parameters.DeliveryOption = "LIVRARE";
		EndIf;
		
		CommentArea.Parameters.PrintingDateTime = Left(CurrentDate(), StrLen(CurrentDate()) -3);
		
		SpreadsheetDocument.Put(CommentArea);
#EndRegion
			
#Region PrintGarantyTableHeader
		SpreadsheetDocument.Put(ConsolidatedCardHeaderArea);
#EndRegion
			
#Region PrintGarantyLines
		i = 0;
		Products = Header.Select();
		While Products.Next() Do
			i = i + 1;
			ConsolidatedLineArea.Parameters.Fill(Products);
			
			If i >= LinesMaxNumber Then
				i = 0;
				SpreadsheetDocument.Put(ConsolidatedFooterArea);
				SpreadsheetDocument.PutHorizontalPageBreak();
				SpreadsheetDocument.Put(ConsolidatedCardHeaderArea);
				SpreadsheetDocument.Put(ConsolidatedLineArea);
			Else
				SpreadsheetDocument.Put(ConsolidatedLineArea);
			EndIf;
		EndDo;
#EndRegion
		
#Region PrintGarantyTexts
		For i = 0 To 11 Do
			AreasToBeChecked.Clear();
			If i = 11 Then
				AreasToBeChecked.Add(GarantyTextAreasArray[i]);
				AreasToBeChecked.Add(ConsolidatedFooterArea);
			Else
				AreasToBeChecked.Add(GarantyTextAreasArray[i]);
				AreasToBeChecked.Add(GarantyTextAreasArray[i+1]);
				AreasToBeChecked.Add(ConsolidatedFooterArea);
			EndIf;
			
			If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				SpreadsheetDocument.Put(GarantyTextAreasArray[i]);
			Else
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(GarantyTextAreasArray[i]);
				AreasToBeChecked.Add(ConsolidatedFooterArea);
				
				If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(GarantyTextAreasArray[i]);
				Else
					
					AreasToBeChecked.Clear();
					AreasToBeChecked.Add(EmptyLineArea);
					AreasToBeChecked.Add(ConsolidatedFooterArea);
					
					For j = 1 To LinesMaxNumber Do
						
						If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
							Or j >= LinesMaxNumber Then
							
							SpreadsheetDocument.Put(ConsolidatedFooterArea);
							SpreadsheetDocument.PutHorizontalPageBreak();
							SpreadsheetDocument.Put(ConsolidatedCardHeaderArea);
							SpreadsheetDocument.Put(GarantyTextAreasArray[i]);
							Break;
						Else
							SpreadsheetDocument.Put(EmptyLineArea);
						EndIf;
					EndDo;
				EndIf;
			EndIf;
		EndDo;
#EndRegion

#Region PrintGarantyEmptyLines
		AreasToBeChecked.Clear();
		AreasToBeChecked.Add(EmptyLineArea);
		AreasToBeChecked.Add(ConsolidatedFooterArea);

		For i = 1 To 99 Do
			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				SpreadsheetDocument.Put(ConsolidatedFooterArea);
				Break;
			Else
				SpreadsheetDocument.Put(EmptyLineArea);
			EndIf;
		EndDo;
#EndRegion
		PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
	EndDo;
	
#Region PrintGarantyFooter
	SpreadsheetDocument.Footer.Enabled = True;
	SpreadsheetDocument.Footer.Font = new Font("Calibri", 8);
	
	SpreadsheetDocument.Footer.LeftText = "Calor SRL";
	SpreadsheetDocument.Footer.CenterText = "http://www.calor.ro  mail: calor@calor.ro";
	SpreadsheetDocument.Footer.RightText = "Ia calitatea la bani marunti!";
#EndRegion

	SpreadsheetDocument.PageSize = "A4";
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;
	
EndFunction		// PrintWarrantyCard()

/////////////////////////////////////////////////////
// Composing data Query - 
//      standart Function copied and modified light:
// 		unGrouping by LineNumber
//
Function GetQueryText4TaxInvoice()
	
	QueryText =
	"SELECT ALLOWED
	|	TaxInvoiceIssued.Ref AS Ref
	|INTO TaxInvoices
	|FROM
	|	Document.TaxInvoiceIssued AS TaxInvoiceIssued
	|WHERE
	|	TaxInvoiceIssued.Ref IN(&ObjectsArray)
	|
	|UNION ALL
	|
	|SELECT
	|	TaxInvoiceIssuedBasisDocuments.Ref
	|FROM
	|	Document.TaxInvoiceIssued.BasisDocuments AS TaxInvoiceIssuedBasisDocuments
	|WHERE
	|	TaxInvoiceIssuedBasisDocuments.BasisDocument IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	TaxInvoiceIssuedBasisDocuments.BasisDocument AS BasisDocument,
	|	TaxInvoiceIssued.Number AS Number,
	|	TaxInvoiceIssued.Date AS Date,
	|	TaxInvoices.Ref AS Ref
	|INTO BasisDocumentsWithTaxInvoice
	|FROM
	|	TaxInvoices AS TaxInvoices
	|		INNER JOIN Document.TaxInvoiceIssued.BasisDocuments AS TaxInvoiceIssuedBasisDocuments
	|		ON TaxInvoices.Ref = TaxInvoiceIssuedBasisDocuments.Ref
	|		INNER JOIN Document.TaxInvoiceIssued AS TaxInvoiceIssued
	|		ON TaxInvoices.Ref = TaxInvoiceIssued.Ref
	|			AND (TaxInvoiceIssued.Posted)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesInvoice.Ref AS Ref,
	|	SalesInvoice.Number AS Number,
	|	SalesInvoice.Date AS Date,
	|	SalesInvoice.Company AS Company,
	|	SalesInvoice.CompanyVATNumber AS CompanyVATNumber,
	|	SalesInvoice.Counterparty AS Counterparty,
	|	SalesInvoice.Contract AS Contract,
	|	SalesInvoice.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesInvoice.DocumentCurrency AS DocumentCurrency,
	|	CAST(SalesInvoice.Comment AS STRING(1024)) AS Comment,
	|	SalesInvoice.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT) AS ReverseCharge,
	|	SalesInvoice.StructuralUnit AS StructuralUnit,
	|	SalesInvoice.Ref AS BasisDocument,
	|	TRUE AS RegisterVATEntriesBySourceDocuments,
	|	SalesInvoice.Number AS ReferenceNumber,
	|	SalesInvoice.Date AS ReferenceDate,
	|	SalesInvoice.DocumentCurrency AS ReferenceCurrency,
	|	SalesInvoice.ExchangeRate AS ReferenceRate
	|INTO Documents
	|FROM
	|	Document.SalesInvoice AS SalesInvoice
	|WHERE
	|	SalesInvoice.Ref IN(&ObjectsArray)
	|
	|UNION ALL
	|
	|SELECT
	|	CreditNote.Ref,
	|	CreditNote.Number,
	|	CreditNote.Date,
	|	CreditNote.Company,
	|	CreditNote.CompanyVATNumber,
	|	CreditNote.Counterparty,
	|	CreditNote.Contract,
	|	CreditNote.AmountIncludesVAT,
	|	CreditNote.DocumentCurrency,
	|	CAST(CreditNote.Comment AS STRING(1024)),
	|	CreditNote.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT),
	|	CreditNote.StructuralUnit,
	|	CreditNote.Ref,
	|	TRUE,
	|	CreditNote.Number,
	|	CreditNote.Date,
	|	CreditNote.DocumentCurrency,
	|	CreditNote.ExchangeRate
	|FROM
	|	Document.CreditNote AS CreditNote
	|WHERE
	|	CreditNote.Ref IN(&ObjectsArray)
	|
	|UNION ALL
	|
	|SELECT
	|	BasisDocumentsWithTaxInvoice.Ref,
	|	BasisDocumentsWithTaxInvoice.Number,
	|	BasisDocumentsWithTaxInvoice.Date,
	|	SalesInvoice.Company,
	|	SalesInvoice.CompanyVATNumber,
	|	SalesInvoice.Counterparty,
	|	SalesInvoice.Contract,
	|	SalesInvoice.AmountIncludesVAT,
	|	SalesInvoice.DocumentCurrency,
	|	CAST(SalesInvoice.Comment AS STRING(1024)),
	|	SalesInvoice.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT),
	|	SalesInvoice.StructuralUnit,
	|	SalesInvoice.Ref,
	|	FALSE,
	|	SalesInvoice.Number,
	|	SalesInvoice.Date,
	|	SalesInvoice.DocumentCurrency,
	|	SalesInvoice.ExchangeRate
	|FROM
	|	BasisDocumentsWithTaxInvoice AS BasisDocumentsWithTaxInvoice
	|		INNER JOIN Document.SalesInvoice AS SalesInvoice
	|		ON BasisDocumentsWithTaxInvoice.BasisDocument = SalesInvoice.Ref
	|
	|UNION ALL
	|
	|SELECT
	|	BasisDocumentsWithTaxInvoice.Ref,
	|	BasisDocumentsWithTaxInvoice.Number,
	|	BasisDocumentsWithTaxInvoice.Date,
	|	CreditNote.Company,
	|	CreditNote.CompanyVATNumber,
	|	CreditNote.Counterparty,
	|	CreditNote.Contract,
	|	CreditNote.AmountIncludesVAT,
	|	CreditNote.DocumentCurrency,
	|	CAST(CreditNote.Comment AS STRING(1024)),
	|	CreditNote.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT),
	|	CreditNote.StructuralUnit,
	|	CreditNote.Ref,
	|	FALSE,
	|	CreditNote.Number,
	|	CreditNote.Date,
	|	CreditNote.DocumentCurrency,
	|	CreditNote.ExchangeRate
	|FROM
	|	BasisDocumentsWithTaxInvoice AS BasisDocumentsWithTaxInvoice
	|		INNER JOIN Document.CreditNote AS CreditNote
	|		ON BasisDocumentsWithTaxInvoice.BasisDocument = CreditNote.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Documents.Ref AS Ref,
	|	Documents.Number AS Number,
	|	Documents.Date AS Date,
	|	Documents.Company AS Company,
	|	Documents.CompanyVATNumber AS CompanyVATNumber,
	|	Documents.Counterparty AS Counterparty,
	|	Documents.Contract AS Contract,
	|	Documents.AmountIncludesVAT AS AmountIncludesVAT,
	|	Documents.DocumentCurrency AS DocumentCurrency,
	|	Documents.Comment AS Comment,
	|	Documents.ReverseCharge AS ReverseCharge,
	|	Documents.StructuralUnit AS StructuralUnit,
	|	Documents.BasisDocument AS BasisDocument,
	|	Documents.RegisterVATEntriesBySourceDocuments AS RegisterVATEntriesBySourceDocuments,
	|	MAX(AccountingPolicy.Period) AS Period,
	|	Documents.ReferenceDate AS ReferenceDate,
	|	Documents.ReferenceNumber AS ReferenceNumber,
	|	Documents.ReferenceCurrency AS ReferenceCurrency,
	|	Documents.ReferenceRate AS ReferenceRate,
	|	MAX(ExchangeRate.Period) AS PeriodExchangeRate
	|INTO DocumentsMaxAccountingPolicy
	|FROM
	|	Documents AS Documents
	|		LEFT JOIN InformationRegister.AccountingPolicy AS AccountingPolicy
	|		ON Documents.Company = AccountingPolicy.Company
	|			AND Documents.Date >= AccountingPolicy.Period
	|		LEFT JOIN InformationRegister.ExchangeRate AS ExchangeRate
	|		ON Documents.DocumentCurrency = ExchangeRate.Currency
	|			AND Documents.Company = ExchangeRate.Company
	|			AND Documents.Date >= ExchangeRate.Period
	|
	|GROUP BY
	|	Documents.Counterparty,
	|	Documents.DocumentCurrency,
	|	Documents.RegisterVATEntriesBySourceDocuments,
	|	Documents.ReverseCharge,
	|	Documents.Comment,
	|	Documents.AmountIncludesVAT,
	|	Documents.StructuralUnit,
	|	Documents.Contract,
	|	Documents.BasisDocument,
	|	Documents.Ref,
	|	Documents.Number,
	|	Documents.Company,
	|	Documents.CompanyVATNumber,
	|	Documents.Date,
	|	Documents.ReferenceDate,
	|	Documents.ReferenceNumber,
	|	Documents.ReferenceCurrency,
	|	Documents.ReferenceRate
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	DocumentsMaxAccountingPolicy.Ref AS Ref,
	|	DocumentsMaxAccountingPolicy.Number AS Number,
	|	DocumentsMaxAccountingPolicy.Date AS Date,
	|	DocumentsMaxAccountingPolicy.Company AS Company,
	|	DocumentsMaxAccountingPolicy.CompanyVATNumber AS CompanyVATNumber,
	|	DocumentsMaxAccountingPolicy.Counterparty AS Counterparty,
	|	DocumentsMaxAccountingPolicy.Contract AS Contract,
	|	DocumentsMaxAccountingPolicy.AmountIncludesVAT AS AmountIncludesVAT,
	|	DocumentsMaxAccountingPolicy.DocumentCurrency AS DocumentCurrency,
	|	DocumentsMaxAccountingPolicy.Comment AS Comment,
	|	DocumentsMaxAccountingPolicy.ReverseCharge AS ReverseCharge,
	|	DocumentsMaxAccountingPolicy.StructuralUnit AS StructuralUnit,
	|	DocumentsMaxAccountingPolicy.BasisDocument AS BasisDocument,
	|	DocumentsMaxAccountingPolicy.RegisterVATEntriesBySourceDocuments AS RegisterVATEntriesBySourceDocuments,
	|	DocumentsMaxAccountingPolicy.ReferenceNumber AS ReferenceNumber,
	|	DocumentsMaxAccountingPolicy.ReferenceDate AS ReferenceDate,
	|	DocumentsMaxAccountingPolicy.ReferenceCurrency AS ReferenceCurrency,
	|	DocumentsMaxAccountingPolicy.ReferenceRate AS ReferenceRate,
	|	ExchangeRate.Rate AS ExchangeRate,
	|	ExchangeRate.Repetition AS Multiplicity
	|INTO FilteredDocuments
	|FROM
	|	DocumentsMaxAccountingPolicy AS DocumentsMaxAccountingPolicy
	|		INNER JOIN InformationRegister.AccountingPolicy AS AccountingPolicy
	|		ON DocumentsMaxAccountingPolicy.Company = AccountingPolicy.Company
	|			AND DocumentsMaxAccountingPolicy.Period = AccountingPolicy.Period
	|			AND DocumentsMaxAccountingPolicy.RegisterVATEntriesBySourceDocuments = AccountingPolicy.PostVATEntriesBySourceDocuments
	|		INNER JOIN InformationRegister.ExchangeRate AS ExchangeRate
	|		ON DocumentsMaxAccountingPolicy.DocumentCurrency = ExchangeRate.Currency
	|			AND DocumentsMaxAccountingPolicy.Company = ExchangeRate.Company
	|			AND DocumentsMaxAccountingPolicy.PeriodExchangeRate = ExchangeRate.Period
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	FilteredDocuments.Ref AS Ref,
	|	FilteredDocuments.Number AS DocumentNumber,
	|	FilteredDocuments.Date AS DocumentDate,
	|	FilteredDocuments.Company AS Company,
	|	FilteredDocuments.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	FilteredDocuments.Counterparty AS Counterparty,
	|	FilteredDocuments.Contract AS Contract,
	|	FilteredDocuments.AmountIncludesVAT AS AmountIncludesVAT,
	|	FilteredDocuments.DocumentCurrency AS DocumentCurrency,
	|	FilteredDocuments.Comment AS Comment,
	|	FilteredDocuments.ReverseCharge AS ReverseCharge,
	|	FilteredDocuments.StructuralUnit AS StructuralUnit,
	|	FilteredDocuments.BasisDocument AS BasisDocument,
	|	FilteredDocuments.ReferenceNumber AS ReferenceNumber,
	|	FilteredDocuments.ReferenceDate AS ReferenceDate,
	|	FilteredDocuments.ReferenceCurrency AS ReferenceCurrency,
	|	FilteredDocuments.ReferenceRate AS ReferenceRate,
	|	Companies.PresentationCurrency AS PresentationCurrency,
	|	Companies.ExchangeRateMethod AS ExchangeRateMethod,
	|	FilteredDocuments.ExchangeRate AS ExchangeRate,
	|	FilteredDocuments.Multiplicity AS Multiplicity
	|INTO Header
	|FROM
	|	FilteredDocuments AS FilteredDocuments
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON FilteredDocuments.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON FilteredDocuments.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON FilteredDocuments.Contract = CounterpartyContracts.Ref
	|
	|GROUP BY
	|	FilteredDocuments.Number,
	|	FilteredDocuments.Date,
	|	FilteredDocuments.Counterparty,
	|	FilteredDocuments.Company,
	|	FilteredDocuments.CompanyVATNumber,
	|	Companies.LogoFile,
	|	FilteredDocuments.Ref,
	|	FilteredDocuments.Comment,
	|	FilteredDocuments.DocumentCurrency,
	|	FilteredDocuments.AmountIncludesVAT,
	|	FilteredDocuments.ReverseCharge,
	|	FilteredDocuments.Contract,
	|	FilteredDocuments.StructuralUnit,
	|	FilteredDocuments.BasisDocument,
	|	FilteredDocuments.ReferenceNumber,
	|	FilteredDocuments.ReferenceDate,
	|	FilteredDocuments.ReferenceCurrency,
	|	Companies.PresentationCurrency,
	|	Companies.ExchangeRateMethod,
	|	FilteredDocuments.ReferenceRate,
	|	FilteredDocuments.ExchangeRate,
	|	FilteredDocuments.Multiplicity
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	FilteredDocuments.Ref AS Ref,
	|	SalesInvoiceInventory.LineNumber AS LineNumber,
	|	SalesInvoiceInventory.Products AS Products,
	|	SalesInvoiceInventory.Characteristic AS Characteristic,
	|	SalesInvoiceInventory.Batch AS Batch,
	|	SalesInvoiceInventory.Quantity AS Quantity,
	|	SalesInvoiceInventory.Reserve AS Reserve,
	|	SalesInvoiceInventory.MeasurementUnit AS MeasurementUnit,
	|	SalesInvoiceInventory.Price AS Price,
	|	SalesInvoiceInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SalesInvoiceInventory.Amount AS Amount,
	|	SalesInvoiceInventory.VATRate AS VATRate,
	|	SalesInvoiceInventory.VATAmount AS VATAmount,
	|	SalesInvoiceInventory.Total AS Total,
	|	SalesInvoiceInventory.Order AS Order,
	|	SalesInvoiceInventory.Content AS Content,
	|	SalesInvoiceInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	SalesInvoiceInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	SalesInvoiceInventory.ConnectionKey AS ConnectionKey,
	|	FilteredDocuments.BasisDocument AS BasisDocument,
	|	CAST(SalesInvoiceInventory.Quantity * SalesInvoiceInventory.Price - SalesInvoiceInventory.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	SalesInvoiceInventory.BundleProduct AS BundleProduct,
	|	SalesInvoiceInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO FilteredInventory
	|FROM
	|	Document.SalesInvoice.Inventory AS SalesInvoiceInventory
	|		INNER JOIN FilteredDocuments AS FilteredDocuments
	|		ON SalesInvoiceInventory.Ref = FilteredDocuments.BasisDocument
	|
	|UNION ALL
	|
	|SELECT
	|	FilteredDocuments.Ref,
	|	CreditNoteInventory.LineNumber,
	|	CreditNoteInventory.Products,
	|	CreditNoteInventory.Characteristic,
	|	CreditNoteInventory.Batch,
	|	CreditNoteInventory.Quantity,
	|	0,
	|	CreditNoteInventory.MeasurementUnit,
	|	CASE
	|		WHEN CreditNoteInventory.Quantity = 0
	|			THEN 0
	|		ELSE CreditNoteInventory.Amount / CreditNoteInventory.Quantity
	|	END,
	|	0,
	|	CreditNoteInventory.Amount,
	|	CreditNoteInventory.VATRate,
	|	CreditNoteInventory.VATAmount,
	|	CreditNoteInventory.Total,
	|	CreditNoteInventory.Order,
	|	"""",
	|	0,
	|	0,
	|	CreditNoteInventory.ConnectionKey,
	|	FilteredDocuments.BasisDocument,
	|	0,
	|	CreditNoteInventory.BundleProduct,
	|	CreditNoteInventory.BundleCharacteristic
	|FROM
	|	Document.CreditNote.Inventory AS CreditNoteInventory
	|		INNER JOIN FilteredDocuments AS FilteredDocuments
	|		ON CreditNoteInventory.Ref = FilteredDocuments.BasisDocument
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
	|	Header.Comment AS Comment,
	|	Header.ReverseCharge AS ReverseCharge,
	|	FilteredInventory.LineNumber AS LineNumber,							//	!!!
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	CAST(FilteredInventory.Price * CASE
	|			WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN Header.Multiplicity / Header.ExchangeRate
	|			WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN Header.ExchangeRate / Header.Multiplicity
	|		END AS NUMBER(15, 2)) AS Price,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountRate,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	SUM(CAST(FilteredInventory.Amount * CASE
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN Header.Multiplicity / Header.ExchangeRate
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN Header.ExchangeRate / Header.Multiplicity
	|			END AS NUMBER(15, 2))) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(CAST(FilteredInventory.VATAmount * CASE
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN Header.Multiplicity / Header.ExchangeRate
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN Header.ExchangeRate / Header.Multiplicity
	|			END AS NUMBER(15, 2))) AS VATAmount,
	|	SUM(CAST(FilteredInventory.Total * CASE
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN Header.Multiplicity / Header.ExchangeRate
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN Header.ExchangeRate / Header.Multiplicity
	|			END AS NUMBER(15, 2))) AS Total,
	|	SUM(CASE
	|			WHEN Header.AmountIncludesVAT
	|				THEN CAST((FilteredInventory.Amount - FilteredInventory.VATAmount + FilteredInventory.DiscountAmount) * CASE
	|							WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|								THEN Header.Multiplicity / Header.ExchangeRate
	|							WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|								THEN Header.ExchangeRate / Header.Multiplicity
	|						END AS NUMBER(15, 2))
	|			ELSE CAST(FilteredInventory.Quantity * FilteredInventory.Price * CASE
	|						WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|							THEN Header.Multiplicity / Header.ExchangeRate
	|						WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|							THEN Header.ExchangeRate / Header.Multiplicity
	|					END AS NUMBER(15, 2))
	|		END) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	FilteredInventory.Batch AS Batch,
	|	Header.StructuralUnit AS StructuralUnit,
	|	Header.BasisDocument AS BasisDocument,
	|	Header.ReferenceNumber AS ReferenceNumber,
	|	Header.ReferenceDate AS ReferenceDate,
	|	Header.ReferenceCurrency AS ReferenceCurrency,
	|	Header.ReferenceRate AS ReferenceRate,
	|	SUM(CAST(FilteredInventory.DiscountAmount * CASE
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN Header.Multiplicity / Header.ExchangeRate
	|				WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN Header.ExchangeRate / Header.Multiplicity
	|			END AS NUMBER(15, 2))) AS DiscountAmount,
	|	Header.PresentationCurrency AS PresentationCurrency,
	|	FilteredInventory.Total AS TotalCur,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.BasisDocument = FilteredInventory.BasisDocument
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	Header.DocumentNumber,
	|	Header.DocumentDate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.ExchangeRateMethod,
	|	Header.Ref,
	|	Header.Counterparty,
	|	Header.CompanyLogoFile,
	|	Header.Contract,
	|	Header.AmountIncludesVAT,
	|	Header.DocumentCurrency,
	|	Header.Comment,
	|	Header.ReverseCharge,
	|	CatalogProducts.SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	FilteredInventory.LineNumber,										//	!!!
	|	FilteredInventory.VATRate,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.Products,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.MeasurementUnit,
	|	FilteredInventory.Batch,
	|	Header.StructuralUnit,
	|	Header.BasisDocument,
	|	Header.ReferenceNumber,
	|	Header.ReferenceDate,
	|	Header.ReferenceCurrency,
	|	Header.PresentationCurrency,
	|	Header.ReferenceRate,
	|	CAST(FilteredInventory.Price * CASE
	|			WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN Header.Multiplicity / Header.ExchangeRate
	|			WHEN Header.ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN Header.ExchangeRate / Header.Multiplicity
	|		END AS NUMBER(15, 2)),
	|	FilteredInventory.Total,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	SUM(Tabular.Total) AS TotalForCount
	|INTO TotalTable
	|FROM
	|	Tabular AS Tabular
	|
	|GROUP BY
	|	Tabular.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.Comment AS Comment,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	Tabular.Amount AS TaxableAmount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	Tabular.DiscountAmount AS DiscountAmount,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountRate
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Products AS Products,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.MeasurementUnit AS MeasurementUnit,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	Tabular.StructuralUnit AS StructuralUnit,
	|	Tabular.BasisDocument AS BasisDocument,
	|	Tabular.ReferenceNumber AS ReferenceNumber,
	|	Tabular.ReferenceDate AS ReferenceDate,
	|	Tabular.ReferenceCurrency AS ReferenceCurrency,
	|	Tabular.ReferenceRate AS ReferenceRate,
	|	Tabular.PresentationCurrency AS PresentationCurrency,
	|	Tabular.TotalCur AS TotalCur,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic
	|FROM
	|	Tabular AS Tabular
	|		LEFT JOIN TotalTable AS TotalTable
	|		ON Tabular.Ref = TotalTable.Ref
	|
	|ORDER BY
	|	Tabular.DocumentNumber,
	|	ReferenceNumber,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
	|	MAX(Comment),
	|	max(LineNumber),			//	!!!
	|	COUNT(SKU),					//	!!!
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(DiscountAmount),
	|	MAX(StructuralUnit),
	|	MAX(ReferenceNumber),
	|	MAX(ReferenceDate),
	|	MAX(ReferenceCurrency),
	|	MAX(ReferenceRate),
	|	MAX(PresentationCurrency),
	|	SUM(TotalCur)
	|BY
	|	Ref,
	|	BasisDocument
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	CASE
	|		WHEN Tabular.ReverseCharge
	|				AND Tabular.VATRate = VALUE(Catalog.VATRates.ZeroRate)
	|			THEN &ReverseChargeAppliesRate
	|		ELSE Tabular.VATRate
	|	END AS VATRate,
	|	SUM(Tabular.Amount) AS TaxableAmount,
	|	SUM(Tabular.VATAmount) AS VATAmount
	|FROM
	|	Tabular AS Tabular
	|
	|GROUP BY
	|	Tabular.Ref,
	|	CASE
	|		WHEN Tabular.ReverseCharge
	|				AND Tabular.VATRate = VALUE(Catalog.VATRates.ZeroRate)
	|			THEN &ReverseChargeAppliesRate
	|		ELSE Tabular.VATRate
	|	END
	|TOTALS BY
	|	Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Ref AS Ref,
	|	SerialNumbers.Description AS SerialNumber
	|FROM
	|	FilteredInventory AS FilteredInventory
	|		INNER JOIN Tabular AS Tabular
	|		ON FilteredInventory.Products = Tabular.Products
	|			AND FilteredInventory.DiscountMarkupPercent = Tabular.DiscountRate
	|			AND FilteredInventory.Price = Tabular.Price
	|			AND FilteredInventory.VATRate = Tabular.VATRate
	|			AND (NOT Tabular.ContentUsed)
	|			AND FilteredInventory.Ref = Tabular.Ref
	|			AND FilteredInventory.Characteristic = Tabular.Characteristic
	|			AND FilteredInventory.MeasurementUnit = Tabular.MeasurementUnit
	|			AND FilteredInventory.Batch = Tabular.Batch
	|		INNER JOIN Document.SalesInvoice.SerialNumbers AS SalesInvoiceSerialNumbers
	|			LEFT JOIN Catalog.SerialNumbers AS SerialNumbers
	|			ON SalesInvoiceSerialNumbers.SerialNumber = SerialNumbers.Ref
	|		ON (SalesInvoiceSerialNumbers.ConnectionKey = FilteredInventory.ConnectionKey)
	|			AND FilteredInventory.Ref = SalesInvoiceSerialNumbers.Ref";
	
	Return QueryText;
	
EndFunction		//	GetQueryText4TaxInvoice()

/////////////////////////////////////////////////////
// Composing data Query - 
//      standart Function copied and modified light:
// 		unGrouping by LineNumber
//
Function GetQueryText4DeliveryNote()
	
	QueryText =
	"SELECT ALLOWED
	|	SalesInvoice.Ref AS Ref,
	|	SalesInvoice.Number AS Number,
	|	SalesInvoice.Date AS Date,
	|	SalesInvoice.Company AS Company,
	|	SalesInvoice.CompanyVATNumber AS CompanyVATNumber,
	|	SalesInvoice.Counterparty AS Counterparty,
	|	SalesInvoice.Contract AS Contract,
	|	CAST(SalesInvoice.Comment AS STRING(1024)) AS Comment,
	|	SalesInvoice.Order AS Order,
	|	SalesInvoice.SalesOrderPosition AS SalesOrderPosition,
	|	SalesInvoice.ContactPerson AS ContactPerson,
	|	SalesInvoice.ShippingAddress AS ShippingAddress,
	|	SalesInvoice.DeliveryOption AS DeliveryOption,
	|	SalesInvoice.StructuralUnit AS StructuralUnit
	|INTO SalesInvoice
	|FROM
	|	Document.SalesInvoice AS SalesInvoice
	|WHERE
	|	SalesInvoice.Ref IN(&ObjectsArray)
	|	AND NOT SalesInvoice.AdvanceInvoicing
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesInvoice.Ref AS Ref,
	|	SalesInvoice.Number AS DocumentNumber,
	|	SalesInvoice.Date AS DocumentDate,
	|	SalesInvoice.Company AS Company,
	|	SalesInvoice.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	SalesInvoice.Counterparty AS Counterparty,
	|	SalesInvoice.Contract AS Contract,
	|	CASE
	|		WHEN SalesInvoice.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN SalesInvoice.ContactPerson
	|		WHEN CounterpartyContracts.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN CounterpartyContracts.ContactPerson
	|		ELSE Counterparties.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	SalesOrder.Ref AS SalesOrder,
	|	ISNULL(SalesOrder.Number, """") AS SalesOrderNumber,
	|	ISNULL(SalesOrder.Date, DATETIME(1, 1, 1)) AS SalesOrderDate,
	|	SalesInvoice.Comment AS Comment,
	|	SalesInvoice.ShippingAddress AS ShippingAddress,
	|	SalesInvoice.DeliveryOption AS DeliveryOption,
	|	SalesInvoice.StructuralUnit AS StructuralUnit
	|INTO Header
	|FROM
	|	SalesInvoice AS SalesInvoice
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON SalesInvoice.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON SalesInvoice.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON SalesInvoice.Contract = CounterpartyContracts.Ref
	|		LEFT JOIN Document.SalesOrder AS SalesOrder
	|		ON SalesInvoice.Order = SalesOrder.Ref
	|			AND (SalesInvoice.SalesOrderPosition = VALUE(Enum.AttributeStationing.InHeader))
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesInvoiceInventory.Ref AS Ref,
	|	SalesInvoiceInventory.LineNumber AS LineNumber,
	|	SalesInvoiceInventory.Products AS Products,
	|	SalesInvoiceInventory.Characteristic AS Characteristic,
	|	SalesInvoiceInventory.Batch AS Batch,
	|	SalesInvoiceInventory.Quantity AS Quantity,
	|	SalesInvoiceInventory.MeasurementUnit AS MeasurementUnit,
	|	SalesInvoiceInventory.Order AS Order,
	|	SalesInvoiceInventory.ConnectionKey AS ConnectionKey,
	|	SalesInvoiceInventory.BundleProduct AS BundleProduct,
	|	SalesInvoiceInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO FilteredInventory
	|FROM
	|	Document.SalesInvoice.Inventory AS SalesInvoiceInventory
	|WHERE
	|	SalesInvoiceInventory.Ref IN(&ObjectsArray)
	|	AND SalesInvoiceInventory.ProductsTypeInventory
	|	AND SalesInvoiceInventory.GoodsIssue = VALUE(Document.GoodsIssue.EmptyRef)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.Comment AS Comment,
	|	FilteredInventory.LineNumber AS LineNumber,						//  !!!   MIN
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	ISNULL(SalesOrders.Ref, Header.SalesOrder) AS SalesOrder,
	|	ISNULL(SalesOrders.Number, Header.SalesOrderNumber) AS SalesOrderNumber,
	|	ISNULL(SalesOrders.Date, Header.SalesOrderDate) AS SalesOrderDate,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	FilteredInventory.Batch AS Batch,
	|	Header.ShippingAddress AS ShippingAddress,
	|	Header.DeliveryOption AS DeliveryOption,
	|	Header.StructuralUnit AS StructuralUnit,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|		LEFT JOIN Document.SalesOrder AS SalesOrders
	|		ON (FilteredInventory.Order = SalesOrders.Ref)
	|			AND (Header.SalesOrderNumber = """")
	|
	|GROUP BY
	|	Header.DocumentNumber,
	|	Header.DocumentDate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Ref,
	|	Header.Counterparty,
	|	Header.CompanyLogoFile,
	|	Header.Contract,
	|	Header.CounterpartyContactPerson,
	|	Header.Comment,
	|	CatalogProducts.SKU,
	|	CASE
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	ISNULL(SalesOrders.Ref, Header.SalesOrder),
	|	ISNULL(SalesOrders.Date, Header.SalesOrderDate),
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	ISNULL(SalesOrders.Number, Header.SalesOrderNumber),
	|	CatalogProducts.UseSerialNumbers,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.Products,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	FilteredInventory.LineNumber,						//  !!!   
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.MeasurementUnit,
	|	FilteredInventory.Batch,
	|	Header.ShippingAddress,
	|	Header.DeliveryOption,
	|	Header.StructuralUnit,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.Comment AS Comment,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.Quantity AS Quantity,
	|	SalesOrdersTurnovers.QuantityReceipt AS QuantityOrdered,
	|	Tabular.Products AS Products,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.MeasurementUnit AS MeasurementUnit,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	FALSE AS ContentUsed,
	|	Tabular.ShippingAddress AS ShippingAddress,
	|	Tabular.DeliveryOption AS DeliveryOption,
	|	Tabular.StructuralUnit AS StructuralUnit,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic
	|FROM
	|	Tabular AS Tabular
	|		LEFT JOIN AccumulationRegister.SalesOrders.Turnovers(
	|				,
	|				,
	|				,
	|				(SalesOrder, Products, Characteristic) IN
	|					(SELECT
	|						Tabular.SalesOrder,
	|						Tabular.Products,
	|						Tabular.Characteristic
	|					FROM
	|						Tabular)) AS SalesOrdersTurnovers
	|		ON Tabular.SalesOrder = SalesOrdersTurnovers.SalesOrder
	|			AND Tabular.Products = SalesOrdersTurnovers.Products
	|			AND Tabular.Characteristic = SalesOrdersTurnovers.Characteristic
	|
	|ORDER BY
	|	Tabular.DocumentNumber,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(CounterpartyContactPerson),
	|	MAX(Comment),
	|	MAX(LineNumber),
	|	SUM(Quantity),
	|	SUM(QuantityOrdered),
	|	MAX(ShippingAddress),
	|	MAX(DeliveryOption),
	|	MAX(StructuralUnit)
	|BY
	|	Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT DISTINCT
	|	Tabular.Ref AS Ref,
	|	Tabular.SalesOrderNumber AS Number,
	|	Tabular.SalesOrderDate AS Date
	|FROM
	|	Tabular AS Tabular
	|WHERE
	|	Tabular.SalesOrderNumber <> """"
	|
	|ORDER BY
	|	Tabular.SalesOrderNumber
	|TOTALS BY
	|	Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Ref AS Ref,
	|	SerialNumbers.Description AS SerialNumber
	|FROM
	|	FilteredInventory AS FilteredInventory
	|		INNER JOIN Tabular AS Tabular
	|		ON FilteredInventory.Products = Tabular.Products
	|			AND FilteredInventory.Ref = Tabular.Ref
	|			AND FilteredInventory.Characteristic = Tabular.Characteristic
	|			AND FilteredInventory.MeasurementUnit = Tabular.MeasurementUnit
	|			AND FilteredInventory.Batch = Tabular.Batch
	|		INNER JOIN Document.SalesInvoice.SerialNumbers AS GoodsIssueSerialNumbers
	|			LEFT JOIN Catalog.SerialNumbers AS SerialNumbers
	|			ON GoodsIssueSerialNumbers.SerialNumber = SerialNumbers.Ref
	|		ON FilteredInventory.Ref = GoodsIssueSerialNumbers.Ref
	|			AND FilteredInventory.ConnectionKey = GoodsIssueSerialNumbers.ConnectionKey";

	Return QueryText;
	
EndFunction		//	GetQueryText4DeliveryNote()

/////////////////////////////////////////////////////
// Composing data Query - 
//      standart Function copied and modified light:
// 		.........
//
Function GetQueryText4WarrantyCard()

	QueryText =
	"SELECT ALLOWED
	|	ProductsTable.Ref AS Ref,
	|	ProductsTable.Ref.Number AS DocumentNumber,
	|	ProductsTable.Ref.Date AS DocumentDate,
	|	ProductsTable.Ref.Date AS PurchaseDate,
	|	ProductsTable.Ref.Company AS Company,
	|	ProductsTable.Ref.CompanyVATNumber AS CompanyVATNumber,
	|	ProductsTable.Ref.Company.LogoFile AS CompanyLogoFile,
	|	ProductsTable.Ref.BankAccount AS BankAccount,
	|	ProductsTable.Ref.Responsible AS Responsible,                             //   !!!
	|	ProductsTable.Ref.DeliveryOption AS DeliveryOption,
	|	ProductsTable.Ref.ShippingAddress AS ShippingAddress,
	|	ProductsTable.Ref.Counterparty AS Counterparty,
	|	ProductsTable.Ref.Counterparty.DescriptionFull AS CounterpartyFullDescr,
	|	ProductsTable.Ref.Counterparty.ContactInformation.City AS City,
	|	ProductsTable.Ref.Counterparty.ContactInformation.PhoneNumber AS PhoneNumber,
	|	ProductsTable.Ref.Counterparty.ContactInformation.EMAddress AS DeliveryAddress,
	|	ProductsTable.LineNumber AS LineNumber,
	|	ProductsTable.Products AS Product,
	|	ProductsTable.Products.SKU AS SKU,
	|	ProductsTable.Products.Vendor AS Vendor,
	|	ProductsTable.Products.GuaranteePeriod AS WarrantyPeriod,
	|	ProductsTable.MeasurementUnit AS MeasurementUnit,
	|	ProductsTable.Quantity AS Quantity
	|FROM
	|	Document.SalesInvoice.Inventory AS ProductsTable
	|WHERE
	|	ProductsTable.Ref IN(&ObjectsArray)
	|TOTALS BY
	|	Ref";
	
	Return QueryText;

EndFunction // GetQueryText4WarrantyCard()()
