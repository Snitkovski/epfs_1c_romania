﻿//////////////////////////////////////////////////////////////////////
//
//  When we add new DataProcessor'а - must include it 
//    in AttachableReportsAndDataProcessors subsystem
//  Go to Additional Properties in new DataProcessor
//    (Alt-Shift-Enter on new Object)
//
//  При добавлении нового DataProcessor'а - обязательно необходимо 
//    добавить его в подсистему AttachableReportsAndDataProcessors
//  Для этого заходить в Additional Properties нового DataProcessor'а
//    (Alt-Shift-Enter на новом Объекте)
//

Procedure OnDefineSettings(Settings) Export
    
	// К какому объекту привязывается. Лучше задавать в виде строки, чтобы не тянуть объект в Расширение!
	// Metadata.Object for link to. It's better way - as String, in this case we don't to pull Object into Extension
    Settings.Placement.Add("Document.SalesOrder");
    Settings.AddPrintCommands = True;

EndProcedure

Procedure AddPrintCommands(PrintCommands) Export

	//PrintFormsOwner = Ext_roAdditionalFunctionality.Ext_roAdditionalPrint();
	//PrintFormsOwner = ?(IsBlankString(PrintFormsOwner), "Pers.", PrintFormsOwner);

	NewCommand	= PrintCommands.Add();
	NewCommand.Presentation = "Anexa la Contract (deservire/abonament)";  //+ " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_AnexaContract";				// Identifier - Идентификатор
	NewCommand.CheckPostingBeforePrint = True;			// Validity check Before print - Проверка проведения перед печатью
	//NewCommand.Handler = "SetAditionalPrintParam";// Method Name from DataProcessor's FormModule (in this case - in our Extension),
	//											   //    which realise PrintForm output
	//											   // Имя клиентского метода в модуле формы Обработки (в данном случае - 
	//											   //    в нашем Расширении), реализующего вывод печатной формы
	
EndProcedure

Procedure Print(ObjectsArray, PrintParameters, PrintFormsCollection, PrintObjects,OutputParameters) Export
	
	//If NOT PrintParameters.Property("Result") Then
	//	PrintParameters.Insert("Result", New Structure);
	//EndIf;
	
	If NOT PrintParameters.Property("Result") OR PrintParameters.Result = Undefined Then
		PrintParameters.Insert("Result", New Structure);
	EndIf;

		If PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_AnexaContract") Then
		
		// PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
		// Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
		    
		  AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_AnexaContract_Deservire(Abonament)");
		// Call own PrintHandler - see below
		// Name "AnexaContract" doesn't matter - isn't typical PF
		// Вызываем НЕстандартный обработчик печати - собственный, см.ниже
		PrintForms = PrintAnexaContract(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
		
		// Standart PrintForm output - Стандартный вывод печатной формы
		// "Ext_AnexaContract" must be as NewCommand.ID above
		PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_AnexaContract", "Anexa la Contract", PrintForms);
		
	EndIf;
	
EndProcedure

//////////////////////////////////////////////////////////////////////
// Document printing procedure AnexaContract
//  copied Function PrintProformaInvoice() and adapted
//
// Parameters:
//  <Parameter1>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//  <Parameter2>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//
// Returns:
//   <Type.Subtype>   - <returned value description>
//
Function PrintAnexaContract(ObjectsArray, PrintObjects, PrintParams, AdditionalParameters = Undefined)
	
    DisplayPrintOption = False;
	PrintParams.Insert("Copies", 1);
    
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_AnexaContract";
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_AnexaContract";
	SpreadsheetDocument.FitToPage = True;
	
	Query = New Query();
	Query.SetParameter("ObjectsArray", ObjectsArray);

	// Composing data Query with standart Function (modified light)
	Query.Text = GetQueryTextFromSalesOrder();
	ResultArray = Query.ExecuteBatch();
	
	FirstDocument = True;
	//LinesMaxNumber = 42;

	HeaderVariants = ResultArray[4].Select(QueryResultIteration.ByGroups);
	TaxesHeaderSelVariants = ResultArray[5].Select(QueryResultIteration.ByGroups);
	TotalLineNumber = ResultArray[6].Unload();
	
	// Bundles
	TableColumns = ResultArray[4].Columns;
	// End Bundles
	
	// 
	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
	
	// Reading ALL print areas  from Template received
	
	TitleArea = Template.GetArea("Title");
	TotalGeneralArea = Template.GetArea("TotalGeneral");
	SignaturesArea = Template.GetArea("Signatures");
	LineHeaderArea = Template.GetArea("LineHeader");
	LineSectionArea	= Template.GetArea("LineSection");
	//EmptyLineArea = Template.GetArea("EmptyLine");
	
	SeeNextPageArea	= Template.GetArea("SeeNextPage");
	
	AreasToBeChecked = New Array;
	
	// HeaderVariants 
	While HeaderVariants.Next() Do
		
		Header = HeaderVariants.Select(QueryResultIteration.ByGroups);
		 
		While Header.Next() Do
			
			If Not FirstDocument Then
				SpreadsheetDocument.PutHorizontalPageBreak();
			EndIf;
			FirstDocument = False;
			
			FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
			
#Region PrintAnexaContractTitleArea
			
			Contract = Header.Contract.GetObject();
			ContractNo = Contract.ContractNo;
			ContractDate = Format(Contract.ContractDate,"DF=dd.MM.yyyy");
			
			TitleArea.Parameters.DocumentNumber = Header.DocumentNumber;
			TitleArea.Parameters.DocumentDate = Format(Header.DocumentDate,"DF=dd.MM.yyyy");
			TitleArea.Parameters.ContractNo = ContractNo;
			TitleArea.Parameters.ContractDate = ContractDate;
			//TitleArea.Parameters.Fill(Header);
			SpreadsheetDocument.Put(TitleArea);
#EndRegion

			InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Header.Company,
																		Header.DocumentDate, ,
																		Header.BankAccount,
																		Header.CompanyVATNumber);
			
			InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);

#Region PrintAnexaContractLinesArea
       		SpreadsheetDocument.Put(LineHeaderArea);
			
			// Bundles
			LineTotalArea = Undefined;
  			TableInventoty = BundlesServer.AssemblyTableByBundles(Header.Ref, Header, TableColumns, LineTotalArea);
			EmptyColor = LineSectionArea.CurrentArea.TextColor;
			// End Bundles
			
/////////////  TableInventoty Cicle  ////////////////////
			For Each TabSelection In TableInventoty Do
				
				If TabSelection.IsFreightService = True Then
					Continue;
				EndIf;
				
				LineSectionArea.Parameters.Fill(TabSelection);
				
				DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection);
                
				// Bundles
			    LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
				// End Bundles
				
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(LineSectionArea);
				//AreasToBeChecked.Add(EmptyLineArea);
				//AreasToBeChecked.Add(EmptyLineArea);
				//AreasToBeChecked.Add(EmptyLineArea);
				//AreasToBeChecked.Add(LineTotalEmptyArea);
				AreasToBeChecked.Add(SeeNextPageArea);
				
				If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(LineSectionArea);
					//Message(TabSelection.LineNumber);
				Else
					//AreasToBeChecked.Clear();
					//AreasToBeChecked.Add(LineSectionArea);
					//AreasToBeChecked.Add(LineTotalEmptyArea);
					//
					//If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
					//		AND TabSelection.LineNumber < LinesMaxNumber Then
					//	SpreadsheetDocument.Put(LineSectionArea);
						//Message(TabSelection.LineNumber);
					//	Continue;
					//EndIf;
					
					//SpreadsheetDocument.Put(EmptyLineArea);
					//SpreadsheetDocument.Put(EmptyLineArea);
					//SpreadsheetDocument.Put(LineTotalEmptyArea);
					SpreadsheetDocument.Put(SeeNextPageArea);
					SpreadsheetDocument.PutHorizontalPageBreak();
					
					SpreadsheetDocument.Put(TitleArea);
					SpreadsheetDocument.Put(LineHeaderArea);
					SpreadsheetDocument.Put(LineSectionArea);
				EndIf;
			EndDo;
/////////////  TableInventoty Cicle  ////////////////////
#EndRegion

#Region PrintAnexaContractTotalGeneralArea
			TotalGeneralArea.Parameters.Total = Header.Subtotal;
			SpreadsheetDocument.Put(TotalGeneralArea);

#EndRegion

#Region PrintAnexaContractSignaturesArea
  
	ResponsiblePersons = DriveServer.OrganizationalUnitsResponsiblePersons(Header.Company, CurrentDate());
	CompanyContactPersonPosition = ResponsiblePersons.HeadPosition;
	CompanyContactPerson = ResponsiblePersons.Head;
	
	SignaturesArea.Parameters.CompanyFullDescr = InfoAboutCompany.FullDescr;
	SignaturesArea.Parameters.CompanyContactPersonPosition = CompanyContactPersonPosition;
	SignaturesArea.Parameters.CompanyContactPerson = CompanyContactPerson;
			
	SignaturesArea.Parameters.CounterpartyFullDescr = InfoAboutCounterparty.FullDescr;
		
	Counterparty = String(Header.Counterparty);
	InfoAboutContactPerson = Ext_PF_ATCommonServerModule.GetContactChoiceList(Counterparty);
	
	For Each ItemOfList In InfoAboutContactPerson Do
		
		ContactPerson = ItemOfList.Value.ContactPerson.Position;
		
		If String(ContactPerson) = "Administrator" Then
			
			SignaturesArea.Parameters.CounterpartyContactPersonPosition = ItemOfList.Value.ContactPerson.Position;
			SignaturesArea.Parameters.CounterpartyContactPerson = ItemOfList.Value.ContactPerson;
		Else
			Message = New UserMessage;
			Message.Text = "Persoana implicita de contact nu are pozitia sau pozitia ei nu este 'Administrator'!";
			Message.Message();
		EndIf;
			
	EndDo;
	
	SpreadsheetDocument.Put(SignaturesArea);

#EndRegion

#Region PrintAnexaContractTotalsAndTaxesArea
			AreasToBeChecked.Clear();
			//AreasToBeChecked.Add(LineTotalArea);
			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				
				SpreadsheetDocument.Put(SeeNextPageArea);
				SpreadsheetDocument.PutHorizontalPageBreak();
				
				SpreadsheetDocument.Put(TitleArea);
				SpreadsheetDocument.Put(LineHeaderArea);
			EndIf;

			AreasToBeChecked.Clear();
			//AreasToBeChecked.Add(EmptyLineArea);
			AreasToBeChecked.Add(SignaturesArea);
			
	//#Region PrintQuoteEmptyLines
	//		For i = 1 To 99 Do
	//			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
	//				SpreadsheetDocument.Put(LineTotalArea);
	//				Break;
	//			//Else
	//			//	SpreadsheetDocument.Put(EmptyLineArea);
	//			EndIf;
	//		EndDo;
	//#EndRegion
#EndRegion

		PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
		
		EndDo;
		// Header.Next() 
	EndDo;
	// HeaderVariants 
	
	//PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
	
#Region PrintQuoteFooter
	SpreadsheetDocument.Footer.Enabled = True;
	SpreadsheetDocument.Footer.Font = new Font("Calibri", 8);
	
	SpreadsheetDocument.Footer.LeftText = "1C-Account Timbal SRL – Contract deservire/abonament";
	
	SpreadsheetDocument.Footer.RightText = "www.1c.ro";
#EndRegion
	
	SpreadsheetDocument.PageSize = "A4";
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;

EndFunction // PrintAnexaContract()

Function GetQueryTextFromSalesOrder()
	
	//SalesOrder.Contract.ContractNo AS ContractContractNo,
	//SalesOrder.Contract.ContractDate AS ContractContractDate,

	QueryText =
	"SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS Number,
	|	SalesOrder.Date AS Date,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	//|	SalesOrder.Contract.ContractNo AS ContractContractNo,
	//|	SalesOrder.Contract.ContractDate AS ContractContractDate,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.EstimateIsCalculated AS EstimateIsCalculated,
	|	SalesOrder.ContactPerson AS ContactPerson,
	|	SalesOrder.ShippingAddress AS ShippingAddress,
	|	SalesOrder.StructuralUnitReserve AS StructuralUnit,
	|	SalesOrder.DeliveryOption AS DeliveryOption
	|INTO SalesOrders
	|FROM
	|	Document.SalesOrder AS SalesOrder
	|WHERE
	|	SalesOrder.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS DocumentNumber,
	|	SalesOrder.Date AS DocumentDate,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	//|	SalesOrder.Contract.ContractNo AS ContractContractNo,
	//|	SalesOrder.Contract.ContractDate AS ContractContractDate,
	|	CASE
	|		WHEN SalesOrder.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN SalesOrder.ContactPerson
	|		WHEN CounterpartyContracts.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN CounterpartyContracts.ContactPerson
	|		ELSE Counterparties.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.ShippingAddress AS ShippingAddress,
	|	SalesOrder.StructuralUnit AS StructuralUnit,
	|	SalesOrder.DeliveryOption AS DeliveryOption
	|INTO Header
	|FROM
	|	SalesOrders AS SalesOrder
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON SalesOrder.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON SalesOrder.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON SalesOrder.Contract = CounterpartyContracts.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrderInventory.Ref AS Ref,
	|	SalesOrderInventory.LineNumber AS LineNumber,
	|	SalesOrderInventory.Products AS Products,
	|	SalesOrderInventory.Characteristic AS Characteristic,
	|	SalesOrderInventory.Batch AS Batch,
	|	SalesOrderInventory.Quantity AS Quantity,
	|	SalesOrderInventory.Reserve AS Reserve,
	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
	|	CASE
	|		WHEN SalesOrderInventory.Quantity = 0
	|			THEN 0
	|		ELSE SalesOrderInventory.Price + (SalesOrderInventory.Total - SalesOrderInventory.Amount - SalesOrderInventory.VATAmount) / SalesOrderInventory.Quantity
	|	END AS Price,
	|	SalesOrderInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SalesOrderInventory.Total - SalesOrderInventory.VATAmount AS Amount,
	|	SalesOrderInventory.VATRate AS VATRate,
	|	SalesOrderInventory.VATAmount AS VATAmount,
	|	SalesOrderInventory.Total AS Total,
	|	SalesOrderInventory.Content AS Content,
	|	SalesOrderInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	SalesOrderInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	SalesOrderInventory.ConnectionKey AS ConnectionKey,
	|	SalesOrderInventory.BundleProduct AS BundleProduct,
	|	SalesOrderInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO FilteredInventory
	|FROM
	|	Document.SalesOrder.Inventory AS SalesOrderInventory
	|WHERE
	|	SalesOrderInventory.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	//|	Header.Contract.ContractNo AS ContractContractNo,
	//|	Header.Contract.ContractDate AS ContractContractDate,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.BankAccount AS BankAccount,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
//	|	MIN(FilteredInventory.LineNumber) AS LineNumber,
	|	FilteredInventory.LineNumber AS LineNumber,        //   !!!
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	FilteredInventory.Price AS Price,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountRate,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	SUM(FilteredInventory.Amount) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN FilteredInventory.Quantity
	|			ELSE 0
	|		END) AS Freight,
	|	SUM(FilteredInventory.Total) AS Total,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN 0
	|			ELSE FilteredInventory.Quantity
	|		END) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	FilteredInventory.Batch AS Batch,
	|	Header.ShippingAddress AS ShippingAddress,
	|	Header.StructuralUnit AS StructuralUnit,
	|	Header.DeliveryOption AS DeliveryOption,
	|	CatalogProducts.IsFreightService AS IsFreightService,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	Header.DocumentNumber,
	|	Header.DocumentDate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Ref,
	|	Header.Counterparty,
	|	Header.CompanyLogoFile,
	|	Header.Contract,
	//|	Header.Contract.ContractNo AS ContractContractNo,
	//|	Header.Contract.ContractDate AS ContractContractDate,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	Header.DocumentCurrency,
	|	CatalogProducts.SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	FilteredInventory.VATRate,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.Products,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	FilteredInventory.Price,
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.MeasurementUnit,
	|	FilteredInventory.Batch,
	|	FilteredInventory.LineNumber,     // !!!
	|	Header.ShippingAddress,
	|	Header.StructuralUnit,
	|	Header.DeliveryOption,
	|	CatalogProducts.IsFreightService,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	//|	Tabular.Contract.ContractNo AS ContractContractNo,
	//|	Tabular.Contract.ContractDate AS ContractContractDate,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.BankAccount AS BankAccount,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	Tabular.Amount AS Amount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	Tabular.Freight AS FreightTotal,
	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountRate
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Products AS Products,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.MeasurementUnit AS MeasurementUnit,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	Tabular.ShippingAddress AS ShippingAddress,
	|	Tabular.StructuralUnit AS StructuralUnit,
	|	Tabular.DeliveryOption AS DeliveryOption,
	|	0 AS Variant,
	|	Tabular.IsFreightService AS IsFreightService,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic
	|FROM
	|	Tabular AS Tabular
	|
	|ORDER BY
	|	Tabular.DocumentNumber,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	//|	MAX(ContractContractNo),
	//|	MAX(ContractContractDate),	
	|	MAX(CounterpartyContactPerson),
	|	MAX(BankAccount),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
//	|	COUNT(LineNumber),				//	!!!
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(FreightTotal),
	|	SUM(DiscountAmount),
	|	MAX(ShippingAddress),
	|	MAX(StructuralUnit),
	|	MAX(DeliveryOption)
	|BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	0 AS Variant,
	|	Tabular.VATRate AS VATRate,
	|	SUM(Tabular.Amount) AS Amount,
	|	SUM(Tabular.VATAmount) AS VATAmount
	|FROM
	|	Tabular AS Tabular
	|
	|GROUP BY
	|	Tabular.Ref,
	|	Tabular.VATRate
	|TOTALS BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	COUNT(Tabular.LineNumber) AS LineNumber,
	|	Tabular.Ref AS Ref,
	|	SUM(Tabular.Quantity) AS Quantity,
	|	0 AS Variant
	|FROM
	|	Tabular AS Tabular
	|WHERE
	|	NOT Tabular.IsFreightService
	|
	|GROUP BY
	|	Tabular.Ref";
	
	Return QueryText;

//	QueryText = 
//	"SELECT ALLOWED
//	|	SalesOrder.Ref AS Ref,
//	|	SalesOrder.Number AS Number,
//	|	SalesOrder.Date AS Date,
//	|	SalesOrder.Company AS Company,
//	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
//	|	SalesOrder.Counterparty AS Counterparty,
//	|	SalesOrder.Contract AS Contract,
//	|	SalesOrder.BankAccount AS BankAccount,
//	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
//	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
//	|	SalesOrder.EstimateIsCalculated AS EstimateIsCalculated,
//	|	SalesOrder.ContactPerson AS ContactPerson,
//	|	SalesOrder.ShippingAddress AS ShippingAddress,
//	|	SalesOrder.StructuralUnitReserve AS StructuralUnit,
//	|	SalesOrder.DeliveryOption AS DeliveryOption
//	|INTO SalesOrders
//	|FROM
//	|	Document.SalesOrder AS SalesOrder
//	|WHERE
//	|	SalesOrder.Ref IN(&ObjectsArray)
//	|;
//	|
//	|////////////////////////////////////////////////////////////////////////////////
//	|SELECT ALLOWED
//	|	SalesOrder.Ref AS Ref,
//	|	SalesOrder.Number AS DocumentNumber,
//	|	SalesOrder.Date AS DocumentDate,
//	|	SalesOrder.Company AS Company,
//	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
//	|	Companies.LogoFile AS CompanyLogoFile,
//	|	SalesOrder.Counterparty AS Counterparty,
//	|	SalesOrder.Contract AS Contract,
//	|	CASE
//	|		WHEN SalesOrder.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
//	|			THEN SalesOrder.ContactPerson
//	|		WHEN CounterpartyContracts.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
//	|			THEN CounterpartyContracts.ContactPerson
//	|		ELSE Counterparties.ContactPerson
//	|	END AS CounterpartyContactPerson,
//	|	SalesOrder.BankAccount AS BankAccount,
//	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
//	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
//	|	SalesOrder.ShippingAddress AS ShippingAddress,
//	|	SalesOrder.StructuralUnit AS StructuralUnit,
//	|	SalesOrder.DeliveryOption AS DeliveryOption
//	|INTO Header
//	|FROM
//	|	SalesOrders AS SalesOrder
//	|		LEFT JOIN Catalog.Companies AS Companies
//	|		ON SalesOrder.Company = Companies.Ref
//	|		LEFT JOIN Catalog.Counterparties AS Counterparties
//	|		ON SalesOrder.Counterparty = Counterparties.Ref
//	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
//	|		ON SalesOrder.Contract = CounterpartyContracts.Ref
//	|;
//	|
//	|////////////////////////////////////////////////////////////////////////////////
//	|SELECT ALLOWED
//	|	SalesOrderInventory.Ref AS Ref,
//	|	SalesOrderInventory.LineNumber AS LineNumber,
//	|	SalesOrderInventory.Products AS Products,
//	|	SalesOrderInventory.Characteristic AS Characteristic,
//	|	SalesOrderInventory.Batch AS Batch,
//	|	SalesOrderInventory.Quantity AS Quantity,
//	|	SalesOrderInventory.Reserve AS Reserve,
//	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
//	|	CASE
//	|		WHEN SalesOrderInventory.Quantity = 0
//	|			THEN 0
//	|		ELSE SalesOrderInventory.Price + (SalesOrderInventory.Total - SalesOrderInventory.Amount - SalesOrderInventory.VATAmount) / SalesOrderInventory.Quantity
//	|	END AS Price,
//	|	SalesOrderInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
//	|	SalesOrderInventory.Total - SalesOrderInventory.VATAmount AS Amount,
//	|	SalesOrderInventory.VATRate AS VATRate,
//	|	SalesOrderInventory.VATAmount AS VATAmount,
//	|	SalesOrderInventory.Total AS Total,
//	|	SalesOrderInventory.Content AS Content,
//	|	SalesOrderInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
//	|	SalesOrderInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
//	|	SalesOrderInventory.ConnectionKey AS ConnectionKey,
//	|	SalesOrderInventory.BundleProduct AS BundleProduct,
//	|	SalesOrderInventory.BundleCharacteristic AS BundleCharacteristic
//	|INTO FilteredInventory
//	|FROM
//	|	Document.SalesOrder.Inventory AS SalesOrderInventory
//	|WHERE
//	|	SalesOrderInventory.Ref IN(&ObjectsArray)
//	|;
//	|
//	|////////////////////////////////////////////////////////////////////////////////
//	|SELECT
//	|	Header.Ref AS Ref,
//	|	Header.DocumentNumber AS DocumentNumber,
//	|	Header.DocumentDate AS DocumentDate,
//	|	Header.Company AS Company,
//	|	Header.CompanyVATNumber AS CompanyVATNumber,
//	|	Header.CompanyLogoFile AS CompanyLogoFile,
//	|	Header.Counterparty AS Counterparty,
//	|	Header.Contract AS Contract,
//	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
//	|	Header.BankAccount AS BankAccount,
//	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
//	|	Header.DocumentCurrency AS DocumentCurrency,
////	|	MIN(FilteredInventory.LineNumber) AS LineNumber,
//	|	FilteredInventory.LineNumber AS LineNumber,        //   !!!
//	|	CatalogProducts.SKU AS SKU,
//	|	CASE
//	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
//	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
//	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
//	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
//	|		ELSE CatalogProducts.Description
//	|	END AS ProductDescription,
//	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
//	|	CASE
//	|		WHEN CatalogProducts.UseCharacteristics
//	|			THEN CatalogCharacteristics.Description
//	|		ELSE """"
//	|	END AS CharacteristicDescription,
//	|	CASE
//	|		WHEN CatalogProducts.UseBatches
//	|			THEN CatalogBatches.Description
//	|		ELSE """"
//	|	END AS BatchDescription,
//	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
//	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
//	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
//	|	SUM(FilteredInventory.Quantity) AS Quantity,
//	|	FilteredInventory.Price AS Price,
//	|	FilteredInventory.DiscountMarkupPercent AS DiscountRate,
//	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
//	|	SUM(FilteredInventory.Amount) AS Amount,
//	|	FilteredInventory.VATRate AS VATRate,
//	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
//	|	FilteredInventory.Price * SUM(CASE
//	|			WHEN CatalogProducts.IsFreightService
//	|				THEN FilteredInventory.Quantity
//	|			ELSE 0
//	|		END) AS Freight,
//	|	SUM(FilteredInventory.Total) AS Total,
//	|	FilteredInventory.Price * SUM(CASE
//	|			WHEN CatalogProducts.IsFreightService
//	|				THEN 0
//	|			ELSE FilteredInventory.Quantity
//	|		END) AS Subtotal,
//	|	FilteredInventory.Products AS Products,
//	|	FilteredInventory.Characteristic AS Characteristic,
//	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
//	|	FilteredInventory.Batch AS Batch,
//	|	Header.ShippingAddress AS ShippingAddress,
//	|	Header.StructuralUnit AS StructuralUnit,
//	|	Header.DeliveryOption AS DeliveryOption,
//	|	CatalogProducts.IsFreightService AS IsFreightService,
//	|	FilteredInventory.BundleProduct AS BundleProduct,
//	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic
//	|INTO Tabular
//	|FROM
//	|	Header AS Header
//	|		INNER JOIN FilteredInventory AS FilteredInventory
//	|		ON Header.Ref = FilteredInventory.Ref
//	|		LEFT JOIN Catalog.Products AS CatalogProducts
//	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
//	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
//	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
//	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
//	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
//	|		LEFT JOIN Catalog.UOM AS CatalogUOM
//	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
//	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
//	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
//	|
//	|GROUP BY
//	|	Header.DocumentNumber,
//	|	Header.DocumentDate,
//	|	Header.Company,
//	|	Header.CompanyVATNumber,
//	|	Header.Ref,
//	|	Header.Counterparty,
//	|	Header.CompanyLogoFile,
//	|	Header.Contract,
//	|	Header.CounterpartyContactPerson,
//	|	Header.BankAccount,
//	|	Header.AmountIncludesVAT,
//	|	Header.DocumentCurrency,
//	|	CatalogProducts.SKU,
//	|	CASE
//	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
//	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
//	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
//	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
//	|		ELSE CatalogProducts.Description
//	|	END,
//	|	CASE
//	|		WHEN CatalogProducts.UseCharacteristics
//	|			THEN CatalogCharacteristics.Description
//	|		ELSE """"
//	|	END,
//	|	CatalogProducts.UseSerialNumbers,
//	|	FilteredInventory.VATRate,
//	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
//	|	FilteredInventory.Products,
//	|	CASE
//	|		WHEN CatalogProducts.UseBatches
//	|			THEN CatalogBatches.Description
//	|		ELSE """"
//	|	END,
//	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
//	|	FilteredInventory.Price,
//	|	FilteredInventory.DiscountMarkupPercent,
//	|	FilteredInventory.Characteristic,
//	|	FilteredInventory.MeasurementUnit,
//	|	FilteredInventory.Batch,
//	|	FilteredInventory.LineNumber,     // !!!
//	|	Header.ShippingAddress,
//	|	Header.StructuralUnit,
//	|	Header.DeliveryOption,
//	|	CatalogProducts.IsFreightService,
//	|	FilteredInventory.BundleProduct,
//	|	FilteredInventory.BundleCharacteristic
//	|;
//	|
//	|////////////////////////////////////////////////////////////////////////////////
//	|SELECT
//	|	Tabular.Ref AS Ref,
//	|	Tabular.DocumentNumber AS DocumentNumber,
//	|	Tabular.DocumentDate AS DocumentDate,
//	|	Tabular.Company AS Company,
//	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
//	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
//	|	Tabular.Counterparty AS Counterparty,
//	|	Tabular.Contract AS Contract,
//	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
//	|	Tabular.BankAccount AS BankAccount,
//	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
//	|	Tabular.DocumentCurrency AS DocumentCurrency,
//	|	Tabular.LineNumber AS LineNumber,
//	|	Tabular.SKU AS SKU,
//	|	Tabular.ProductDescription AS ProductDescription,
//	|	Tabular.ContentUsed AS ContentUsed,
//	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
//	|	Tabular.Quantity AS Quantity,
//	|	Tabular.Price AS Price,
//	|	Tabular.Amount AS Amount,
//	|	Tabular.VATRate AS VATRate,
//	|	Tabular.VATAmount AS VATAmount,
//	|	Tabular.Total AS Total,
//	|	Tabular.Subtotal AS Subtotal,
//	|	Tabular.Freight AS FreightTotal,
//	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
//	|	CASE
//	|		WHEN Tabular.AutomaticDiscountAmount = 0
//	|			THEN Tabular.DiscountRate
//	|		WHEN Tabular.Subtotal = 0
//	|			THEN 0
//	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
//	|	END AS DiscountRate,
//	|	Tabular.Products AS Products,
//	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
//	|	Tabular.BatchDescription AS BatchDescription,
//	|	Tabular.ConnectionKey AS ConnectionKey,
//	|	Tabular.Characteristic AS Characteristic,
//	|	Tabular.MeasurementUnit AS MeasurementUnit,
//	|	Tabular.Batch AS Batch,
//	|	Tabular.UOM AS UOM,
//	|	Tabular.ShippingAddress AS ShippingAddress,
//	|	Tabular.StructuralUnit AS StructuralUnit,
//	|	Tabular.DeliveryOption AS DeliveryOption,
//	|	0 AS Variant,
//	|	Tabular.IsFreightService AS IsFreightService,
//	|	Tabular.BundleProduct AS BundleProduct,
//	|	Tabular.BundleCharacteristic AS BundleCharacteristic
//	|FROM
//	|	Tabular AS Tabular
//	|
//	|ORDER BY
//	|	Tabular.DocumentNumber,
//	|	LineNumber
//	|TOTALS
//	|	MAX(DocumentNumber),
//	|	MAX(DocumentDate),
//	|	MAX(Company),
//	|	MAX(CompanyVATNumber),
//	|	MAX(CompanyLogoFile),
//	|	MAX(Counterparty),
//	|	MAX(Contract),
//	|	MAX(CounterpartyContactPerson),
//	|	MAX(BankAccount),
//	|	MAX(AmountIncludesVAT),
//	|	MAX(DocumentCurrency),
////	|	COUNT(LineNumber),				//	!!!
//	|	SUM(Quantity),
//	|	SUM(VATAmount),
//	|	SUM(Total),
//	|	SUM(Subtotal),
//	|	SUM(FreightTotal),
//	|	SUM(DiscountAmount),
//	|	MAX(ShippingAddress),
//	|	MAX(StructuralUnit),
//	|	MAX(DeliveryOption)
//	|BY
//	|	Ref,
//	|	Variant
//	|;
//	|
//	|////////////////////////////////////////////////////////////////////////////////
//	|SELECT
//	|	Tabular.Ref AS Ref,
//	|	0 AS Variant,
//	|	Tabular.VATRate AS VATRate,
//	|	SUM(Tabular.Amount) AS Amount,
//	|	SUM(Tabular.VATAmount) AS VATAmount
//	|FROM
//	|	Tabular AS Tabular
//	|
//	|GROUP BY
//	|	Tabular.Ref,
//	|	Tabular.VATRate
//	|TOTALS BY
//	|	Ref,
//	|	Variant
//	|;
//	|
//	|////////////////////////////////////////////////////////////////////////////////
//	|SELECT
//	|	COUNT(Tabular.LineNumber) AS LineNumber,
//	|	Tabular.Ref AS Ref,
//	|	SUM(Tabular.Quantity) AS Quantity,
//	|	0 AS Variant
//	|FROM
//	|	Tabular AS Tabular
//	|WHERE
//	|	NOT Tabular.IsFreightService
//	|
//	|GROUP BY
//	|	Tabular.Ref";
//	
//	Return QueryText;
	
EndFunction		//	GetQueryTextFromSalesOrder()

