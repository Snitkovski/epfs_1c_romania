﻿
&AtClient
Procedure CRL_PT_Plus10DaysAfter(Command)
	
	Object.PaymentCalendar[0].PaymentDate = Object.Date + 10 * 24 * 60 * 60;
	
EndProcedure

&AtClient
Procedure CRL_PT_Plus15DaysAfter(Command)
	// Inserare conţinut procesare.
	Object.PaymentCalendar[0].PaymentDate = Object.Date + 15 * 24 * 60 * 60;
EndProcedure

&AtClient
Procedure CRL_PT_Plus30DaysAfter(Command)
	// Inserare conţinut procesare.
	Object.PaymentCalendar[0].PaymentDate = Object.Date + 30 * 24 * 60 * 60;
EndProcedure
