﻿
&Around("SetQueryTextList")
Function CALOR_SetQueryTextListAround()
    
    QSchema = New QuerySchema;
    
    Query = QSchema.QueryBatch[0];
    
    Operator = Query.Operators[0];
    
    srcCatalog = Operator.Sources.Add("Catalog.Products", "ct");
    
    If UsePricesInList Then
        srcPriceList = Operator.Sources.Add("InformationRegister.Prices.SliceLast", "regPL");
        srcVATRates = Operator.Sources.Add("Catalog.VATRates", "VATRates");
    EndIf;
    
    If UseQuantityInList Then
        srSstock = Operator.Sources.Add("AccumulationRegister.InventoryInWarehouses.Balance", "regStock");
        srcReserve = Operator.Sources.Add("AccumulationRegister.ReservedProducts.Balance", "regReserve");
    EndIf;
    
    sf = Operator.SelectedFields;
    
    sf.Add("ct.Ref");
    sf.Add("ct.Ref", 1);
    sf.Add("ct.DeletionMark");
    sf.Add("ct.Parent");
    sf.Add("ct.IsFolder");
    sf.Add("ct.Code");
    sf.Add("ct.Description");
    sf.Add("ct.SKU");
    sf.Add("ct.ChangeDate");
    sf.Add("CAST(ct.DescriptionFull AS STRING(1000))", 9);
    sf.Add("ct.BusinessLine");
    sf.Add("ct.ProductsCategory");
    sf.Add("ct.Vendor");
    sf.Add("ct.Manufacturer");
    sf.Add("ct.Warehouse");
    sf.Add("ct.ReplenishmentMethod");
    sf.Add("ct.ReplenishmentDeadline");
    sf.Add("ct.VATRate");
    sf.Add("ct.ProductsType");
    sf.Add("ct.PriceGroup");
    sf.Add("ct.UseCharacteristics");
    sf.Add("ct.UseBatches", 22);
    sf.Add("ct.UseBatches", 23);
    sf.Add("ct.IsBundle");
    sf.Add("ct.OrderCompletionDeadline");
    sf.Add("ct.TimeNorm");
    sf.Add("ct.CountryOfOrigin");
    sf.Add("ct.UseSerialNumbers");
    sf.Add("ct.GuaranteePeriod");
    sf.Add("ct.WriteOutTheGuaranteeCard");
    sf.Add("Substring(ct.Comment, 1, 1000)", 31);
    sf.Add("&Period");
    sf.Add("&PriceType");
    sf.Add("CASE WHEN ct.UseCharacteristics OR ct.UseBatches THEN 2 ELSE 0	END +
                                |CASE WHEN ct.DeletionMark THEN 1 ELSE 0 END +
                                |CASE WHEN ct.IsBundle THEN 6 ELSE 0 END", 34);
    
    If UsePricesInList Then
        sf.Add("ISNULL(regPL.MeasurementUnit, ct.MeasurementUnit)", 35);
        sf.Add("ISNULL(regPL.MeasurementUnit.Factor, 1)", 36);
        sf.Add("CAST(CASE WHEN regPL.PriceKind.PriceIncludesVAT THEN ISNULL(regPL.Price, 0) ELSE ISNULL(regPL.Price, 0)*(1+ISNULL(VATRates.Rate, 0)/100) END AS NUMBER(12, 2))", 37);
        sf.Add("CAST(CASE WHEN NOT regPL.PriceKind.PriceIncludesVAT OR ISNULL(VATRates.Rate, 0)=0
               |THEN ISNULL(regPL.Price, 0)
               |ELSE ISNULL(regPL.Price, 0)-ISNULL(regPL.Price, 0)*((100+ISNULL(VATRates.Rate, 0))/ISNULL(VATRates.Rate, 0)) 
               |END AS NUMBER(12, 2))", 38);
    Else
        sf.Add("ct.MeasurementUnit", 35);
        sf.Add("1", 36);
        sf.Add("0", 37);
        sf.Add("0", 38);
    EndIf;
    
    If UseQuantityInList Then
        If UsePricesInList Then
            sf.Add("CASE WHEN regPL.MeasurementUnit REFS Catalog.UOM
                                        |THEN ISNULL(regStock.QuantityBalance, 0) / regPL.MeasurementUnit.Factor
                                        |ELSE ISNULL(regStock.QuantityBalance, 0)
                                        |END", 39);
            sf.Add("CASE WHEN regPL.MeasurementUnit REFS Catalog.UOM
                                        |THEN ISNULL(regReserve.QuantityBalance, 0) / regPL.MeasurementUnit.Factor
                                        |ELSE ISNULL(regReserve.QuantityBalance, 0)
                                        |END", 40);
            sf.Add("CASE WHEN regPL.MeasurementUnit REFS Catalog.UOM
                                        |THEN ISNULL(regStock.QuantityBalance, 0) / regPL.MeasurementUnit.Factor
                                        |ELSE ISNULL(regStock.QuantityBalance, 0)
                                        |END -
                                        |CASE WHEN regPL.MeasurementUnit REFS Catalog.UOM
                                        |THEN ISNULL(regReserve.QuantityBalance, 0) / regPL.MeasurementUnit.Factor
                                        |ELSE ISNULL(regReserve.QuantityBalance, 0)
                                        |END", 41);
        Else
            sf.Add("ISNULL(regStock.QuantityBalance, 0)", 39);
            sf.Add("ISNULL(regReserve.QuantityBalance, 0)", 40);
            sf.Add("ISNULL(regStock.QuantityBalance, 0) - ISNULL(regReserve.QuantityBalance, 0)", 41)
        Endif
    Else
        sf.Add("0", 39);
        sf.Add("0", 40);
        sf.Add("0", 41);
    EndIf;
    
    For Each Source In Operator.Sources Do
        Source.Joins.Clear();
    EndDo;
    
    If UsePricesInList Then
        srcCatalog.Joins.Add(srcPriceList, "ct.Ref=regPL.Products AND regPL.Characteristic = VALUE(Catalog.ProductsCharacteristics.EmptyRef)");
        srcCatalog.Joins.Add(srcVATRates, "ct.VATRate=VATRates.Ref");
    EndIf;
    
    If UseQuantityInList Then
        srcCatalog.Joins.Add(srSstock, "ct.Ref=regStock.Products");
        srcCatalog.Joins.Add(srcReserve, "ct.Ref=regReserve.Products");
    EndIf;
    
    If UsePricesInList Then
        srcPriceList.Source.Parameters[0].Expression = New QuerySchemaExpression("&Period");
        srcPriceList.Source.Parameters[1].Expression = New QuerySchemaExpression("PriceKind = &PriceType");
    EndIf;
    
    If UseQuantityInList Then
        srSstock.Source.Parameters[1].Expression = New QuerySchemaExpression("&AllWarehouses OR StructuralUnit = &Warehouse");
        srcReserve.Source.Parameters[1].Expression = New QuerySchemaExpression("&AllWarehouses OR StructuralUnit = &Warehouse");
    EndIf;
     
     Operator.Filter.Add("NOT ct.IsFolder");
     Operator.Filter.Add("&AllTypes OR ct.ProductsType = VALUE(Enum.ProductsTypes.InventoryItem)");
     
     Query.Columns[1].Alias  = "Products";
     Query.Columns[9].Alias  = "DescriptionFull";
     Query.Columns[22].Alias = "UseReservation";
     Query.Columns[23].Alias = "UseBatches";
     Query.Columns[31].Alias = "Comment";
     Query.Columns[34].Alias = "PictureIndex";
     Query.Columns[35].Alias = "MeasurementUnit";
     Query.Columns[36].Alias = "Factor";
     Query.Columns[37].Alias = "Price";
     Query.Columns[38].Alias = "PricewoVAT";
     Query.Columns[39].Alias = "QuantityBalance";
     Query.Columns[40].Alias = "ReserveBalance";
     Query.Columns[41].Alias = "FreeBalance";
     
     List.QueryText = QSchema.GetQueryText()
    
 EndFunction
 
 &After("SetVisibleEnabled")
Procedure CALOR_SetVisibleEnabled()
	
    If NOT Items.Find("ReserveBalance") = Undefined Then
        Items.ReserveBalance.Visible = ShowBalances And (FilterBalances < 2);
    EndIf;
    
    If NOT Items.Find("FreeBalance") = Undefined Then
        Items.FreeBalance.Visible = ShowBalances And (FilterBalances < 2);
    EndIf;
    
    If NOT Items.Find("PricewoVAT") = Undefined Then
        Items.PricewoVAT.Visible = ShowPrices;
    EndIf;
    
    If  NOT Items.Find("FreeBalance") = Undefined and
        NOT Items.Find("PricewoVAT") = Undefined Then
        Items.Move(Items.Price, Items.List, Items.PricewoVAT)
    EndIf;
	
EndProcedure

&AtServer
Procedure AddNewItem(ItemName, DataPath, ItemTitle)
    
    If Items.Find("MeasurementUnit") = Undefined Then
        NewItem = Items.Add(ItemName, Type("FormField"), Items.List);
    Else
        NewItem = Items.Insert(ItemName, Type("FormField"), Items.List, Items.MeasurementUnit);
    EndIf;

    NewItem.DataPath = DataPath;
    NewItem.Type = FormFieldType.InputField;
    NewItem.Visible = False;
    NewItem.Title = ItemTitle;
    
EndProcedure

&AtServer
Procedure CALOR_OnCreateAtServerAfter(Cancel, StandardProcessing)
	
	AddNewItem("ReserveBalance", "List.ReserveBalance", NStr("en = 'Reserved'; ro = 'Rezervat'; ru = 'Резерв'; bg = 'Reserved'; es_CL = 'Reserved'; de = 'Reserved'; pl = 'Reserved'; es_ES = 'Reserved'; tr = 'Reserved'; vi = 'Reserved'"));
	AddNewItem("FreeBalance", "List.FreeBalance", NStr("en = 'Free'; ro = 'Disponibil'; ru = 'Доступно'; bg = 'Free'; es_CL = 'Free'; de = 'Free'; pl = 'Free'; es_ES = 'Free'; tr = 'Free'; vi = 'Free'"));
	AddNewItem("PricewoVAT", "List.PricewoVAT", NStr("en = 'Price w/o VAT'; ro = 'Pret fara TVA'; ru = 'Цена без НДС'; bg = 'Price w/o VAT'; es_CL = 'Price w/o VAT'; de = 'Price w/o VAT'; pl = 'Price w/o VAT'; es_ES = 'Price w/o VAT'; tr = 'Price w/o VAT'; vi = 'Price w/o VAT'"));
	
	Items.Price.Title = NStr("en = 'Price w.VAT'; ro = 'Pret cu TVA'; ru = 'Цена с НДС'");
	Items.Balance.Title = NStr("en = 'Stock'; ro = 'Stoc'; ru = 'Остаток'");
EndProcedure
