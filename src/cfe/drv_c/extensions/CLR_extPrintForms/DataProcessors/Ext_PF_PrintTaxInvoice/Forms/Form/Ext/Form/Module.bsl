﻿
&AtClient
Function SetAditionalPrintParam(PrintParameters) Export
    
    Form = PrintParameters.Form;
    PrintParameters.Delete("Form");
    TemplateName = PrintParameters.ID;
	
	// Заменяем идентификатор на стандартный для документа "SalesOrder"
    // Список стандарных идентификаторов можно увидеть в функции PrintManagementServerCallDrive.IsDocumentInPrintOptionsList()
	// "Standart" print form identifier for Documents.SalesOrder
	// List with "Standart" names you can find in PrintManagementServerCallDrive.IsDocumentInPrintOptionsList()
    PrintParameters.Insert("ID", "SalesInvoice");
    
    //Adding equal ID name set in procedure "AddPrintCommands" in the manager module
    PrintParameters.Insert("PrintTemplateName", "Ext_FacturaFiscala");
	
    PrintManagementClient.ExecutePrintCommand(PrintParameters.PrintManager,
													TemplateName,
													PrintParameters.PrintObjects,
													Form,
													PrintParameters);
EndFunction

