﻿//////////////////////////////////////////////////////////////////////
//
//  When we add new DataProcessor'а - must include it 
//    in AttachableReportsAndDataProcessors subsystem
//  Go to Additional Properties in new DataProcessor
//    (Alt-Shift-Enter on new Object)
//
//  При добавлении нового DataProcessor'а - обязательно необходимо 
//    добавить его в подсистему AttachableReportsAndDataProcessors
//  Для этого заходить в Additional Properties нового DataProcessor'а
//    (Alt-Shift-Enter на новом Объекте)
//

Procedure OnDefineSettings(Settings) Export
    
	// К какому объекту привязывается. Лучше задавать в виде строки, чтобы не тянуть объект в Расширение!
	// Metadata.Object for link to. It's better way - as String, in this case we don't to pull Object into Extension
    Settings.Placement.Add("Document.SalesOrder");
    Settings.AddPrintCommands = True;

EndProcedure

Procedure AddPrintCommands(PrintCommands) Export

	PrintFormsOwner = roAdditionalFunctionalityServer.roAdditionalPrint();
	PrintFormsOwner = ?(IsBlankString(PrintFormsOwner), "Pers.", PrintFormsOwner);
	
	NewCommand	= PrintCommands.Add();
	NewCommand.Presentation = "Proforma" + " " + PrintFormsOwner;		// PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_ProformaInvoice";			// Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True;		// Validity check Before print - Проверка проведения перед печатью
	//NewCommand.Handler = "SetAditionalPrintParam";	// Method Name from DataProcessor's FormModule (in this case - in our Extension),
	//												//    which realise PrintForm output
	//												// Имя клиентского метода в модуле формы Обработки (в данном случае - 
	//												//    в нашем Расширении), реализующего вывод печатной формы
	
	NewCommand	= PrintCommands.Add();
	NewCommand.Presentation = "Anexa la Contract" + " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_AnexaContract";				// Identifier - Идентификатор
	NewCommand.CheckPostingBeforePrint = True;			// Validity check Before print - Проверка проведения перед печатью
	//NewCommand.Handler = "SetAditionalPrintParam";// Method Name from DataProcessor's FormModule (in this case - in our Extension),
	//											   //    which realise PrintForm output
	//											   // Имя клиентского метода в модуле формы Обработки (в данном случае - 
	//											   //    в нашем Расширении), реализующего вывод печатной формы
	
EndProcedure

Procedure Print(ObjectsArray, PrintParameters, PrintFormsCollection, PrintObjects,OutputParameters) Export
	
	If NOT PrintParameters.Property("Result") Then
		PrintParameters.Insert("Result", New Structure);
	Else
		PrintParameters.Result = New Structure;
	EndIf;
	
	// "Ext_ProformaInvoice" must be as NewCommand.ID above
    If PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_ProformaInvoice") Then
        
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_Proforma");
        
		// Call own PrintHandler - see below
		// Name "AnexaContract" doesn't matter - isn't typical PF
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintProformaInvoice(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        
		// Standart PrintForm output - Стандартный вывод печатной формы
		// "Ext_ProformaInvoice" must be as NewCommand.ID above
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_ProformaInvoice", "Proforma", PrintForms);
		
	// "Ext_AnexaContract" must be as NewCommand.ID above
	ElsIf PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_AnexaContract") Then
		
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_AnexaContract");
        
		// Call own PrintHandler - see below
		// Name "AnexaContract" doesn't matter - isn't typical PF
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        PrintForms = PrintAnexaContract(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        
		// Standart PrintForm output - Стандартный вывод печатной формы
		// "Ext_AnexaContract" must be as NewCommand.ID above
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_AnexaContract", "Anexa la Contract", PrintForms);
		
    EndIf;
	
EndProcedure

//////////////////////////////////////////////////////////////////////
// Document printing procedure ProformaInvoice
//
Function PrintProformaInvoice(ObjectsArray, PrintObjects, PrintParams, AdditionalParameters = Undefined)

	DisplayPrintOption = False;
	PrintParams.Insert("Copies", 1);
	
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_ProformaInvoice";
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_ProformaInvoice";
	
	Query = New Query();
	Query.SetParameter("ObjectsArray", ObjectsArray);
	Query.SetParameter("AllVariants", False);
	
	// Composing data Query with standart Function (modified light)
	//Query.Text = DataProcessors.PrintQuote.GetProformaInvoiceQueryTextForSalesOrder();
	Query.Text = GetQueryTextFromSalesOrder();
	ResultArray = Query.ExecuteBatch();
	
	FirstDocument = True;
	LinesMaxNumber = 43;
	isParameterizedPF = True;

	HeaderVariants = ResultArray[4].Select(QueryResultIteration.ByGroups);
	TaxesHeaderSelVariants = ResultArray[5].Select(QueryResultIteration.ByGroups);
	TotalLineNumber = ResultArray[6].Unload();
	
	// Bundles
	TableColumns = ResultArray[4].Columns;
	// End Bundles
	
	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
			
	// Reading ALL print areas  from Template received
	TitleArea = Template.GetArea("Title");
	CompanyInfoArea = Template.GetArea("CompanyInfo");
	CounterpartyInfoArea = Template.GetArea("CounterpartyInfo");
	CommentArea = Template.GetArea("Comment");
	EmptyLineArea = Template.GetArea("EmptyLine");
	LineTotalEmptyArea = Template.GetArea("LineTotalEmpty");
	SeeNextPageArea	= Template.GetArea("SeeNextPage");
	
	// HeaderVariants 
	While HeaderVariants.Next() Do
		
		Header = HeaderVariants.Select(QueryResultIteration.ByGroups);
		// Header.Next() 
		While Header.Next() Do
			
			If Not FirstDocument Then
				SpreadsheetDocument.PutHorizontalPageBreak();
			EndIf;
			FirstDocument = False;
			FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
			
#Region PrintProformaInvoiceTitleArea
			TitleArea.Parameters.Fill(Header);
            
            If DisplayPrintOption Then
                TitleArea.Parameters.OriginalDuplicate = ?(PrintParams.OriginalCopy, NStr("en = 'ORIGINAL'"), NStr("en = 'COPY'"));
    		EndIf;
            
			If ValueIsFilled(Header.CompanyLogoFile) Then
				
				PictureData = AttachedFiles.GetBinaryFileData(Header.CompanyLogoFile);
				
				If ValueIsFilled(PictureData) Then
					TitleArea.Drawings.Logo.Picture = New Picture(PictureData);
				EndIf;
			Else
				TitleArea.Drawings.Delete(TitleArea.Drawings.Logo);
			EndIf;
			
			SpreadsheetDocument.Put(TitleArea);
#EndRegion
			
#Region PrintProformaInvoiceCompanyInfoArea
			InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Header.Company,
																Header.DocumentDate, ,
																Header.BankAccount,
																Header.CompanyVATNumber);
			CompanyInfoArea.Parameters.Fill(InfoAboutCompany);
			BarcodesInPrintForms.AddBarcodeToTableDocument(CompanyInfoArea, Header.Ref);
			SpreadsheetDocument.Put(CompanyInfoArea);
#EndRegion
			
#Region PrintProformaInvoiceCounterpartyInfoArea
			CounterpartyInfoArea.Parameters.Fill(Header);
			
			InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);
			CounterpartyInfoArea.Parameters.Fill(InfoAboutCounterparty);
			
			TitleParameters = New Structure;
			TitleParameters.Insert("TitleShipTo", NStr("en = 'Ship to'"));
			TitleParameters.Insert("TitleShipDate", NStr("en = 'Ship date'"));
			
			If Header.DeliveryOption = Enums.DeliveryOptions.SelfPickup Then
				
				InfoAboutPickupLocation	= DriveServer.InfoAboutLegalEntityIndividual(Header.StructuralUnit, Header.DocumentDate);
				ResponsibleEmployee = InfoAboutPickupLocation.ResponsibleEmployee;
				
				If NOT IsBlankString(InfoAboutPickupLocation.FullDescr) Then
					CounterpartyInfoArea.Parameters.FullDescrShipTo = InfoAboutPickupLocation.FullDescr;
				EndIf;
				
				If NOT IsBlankString(InfoAboutPickupLocation.DeliveryAddress) Then
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutPickupLocation.DeliveryAddress;
				EndIf;
				
				If ValueIsFilled(ResponsibleEmployee) Then
					CounterpartyInfoArea.Parameters.CounterpartyContactPerson = ResponsibleEmployee.Description;
				EndIf;
				
				If NOT IsBlankString(InfoAboutPickupLocation.PhoneNumbers) Then
					CounterpartyInfoArea.Parameters.PhoneNumbers = InfoAboutPickupLocation.PhoneNumbers;
				EndIf;
				
				TitleParameters.TitleShipTo = NStr("en = 'Pickup location'");
				TitleParameters.TitleShipDate = NStr("en = 'Pickup date'");
			Else
				InfoAboutShippingAddress = DriveServer.InfoAboutShippingAddress(Header.ShippingAddress);
				InfoAboutContactPerson = DriveServer.InfoAboutContactPerson(Header.CounterpartyContactPerson);
			
				If NOT IsBlankString(InfoAboutShippingAddress.DeliveryAddress) Then
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutShippingAddress.DeliveryAddress;
				EndIf;
				
				If NOT IsBlankString(InfoAboutContactPerson.PhoneNumbers) Then
					CounterpartyInfoArea.Parameters.PhoneNumbers = InfoAboutContactPerson.PhoneNumbers;
				EndIf;
			EndIf;
			
			CounterpartyInfoArea.Parameters.Fill(TitleParameters);
			
			If IsBlankString(CounterpartyInfoArea.Parameters.DeliveryAddress) Then
				
				If Not IsBlankString(InfoAboutCounterparty.ActualAddress) Then
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutCounterparty.ActualAddress;
				Else
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutCounterparty.LegalAddress;
				EndIf;
			EndIf;
			
			//CounterpartyInfoArea.Parameters.PaymentTerms = PaymentTermsServer.TitlePaymentTerms(Header.Ref);
			CounterpartyInfoArea.Parameters.PaymentTerms = PaymentTermsServer.TitleStagesOfPayment(Header.Ref);
			If ValueIsFilled(CounterpartyInfoArea.Parameters.PaymentTerms) Then
				CounterpartyInfoArea.Parameters.PaymentTermsTitle = PaymentTermsServer.PaymentTermsPrintTitle();
			EndIf;
			
			SpreadsheetDocument.Put(CounterpartyInfoArea);
#EndRegion
			
#Region PrintProformaInvoiceCommentArea
			CommentArea.Parameters.SalesRepresentative = Common.ObjectAttributeValue(Header.Ref, "SalesRep");
			
			CommentArea.Parameters.Comment = Common.ObjectAttributeValue(Header.Ref, "Comment");
			SpreadsheetDocument.Put(CommentArea);
#EndRegion
			
#Region PrintProformaInvoiceTotalsAndTaxesAreaPrefill
			TotalsAndTaxesAreasArray = New Array;
            
    		If DisplayPrintOption and PrintParams.Discount Then
    		    LineTotalArea = Template.GetArea("LineTotal");
                LineTotalArea.Parameters.Fill(Header);
            Else
                LineTotalArea = Template.GetArea("LineTotalWithoutDiscount");
                LineTotalArea.Parameters.Fill(Header);
                
                // When the "Discount" column is hidden, the results calculate by subtracting the subtotal and the discount.
                LineTotalArea.Parameters.Subtotal = Header.Subtotal - Header.DiscountAmount;
            EndIf;
			
			SearchStructure = New Structure("Ref, Variant", Header.Ref, Header.Variant);
			
			SearchArray = TotalLineNumber.FindRows(SearchStructure);
			If SearchArray.Count() > 0 Then
				LineTotalArea.Parameters.Quantity = SearchArray[0].Quantity;
				LineTotalArea.Parameters.LineNumber	= SearchArray[0].LineNumber;
			Else
				LineTotalArea.Parameters.Quantity = 0;
				LineTotalArea.Parameters.LineNumber	= 0;
			EndIf;
			
			If isParameterizedPF Then
////////////////////////////////////////////////////////////////////			
				TotalsAndTaxesAreasArray.Add(LineTotalArea);
////////////////////////////////////////////////////////////////////			
			Else
				TaxesHeaderSelVariants.Reset();
				If TaxesHeaderSelVariants.FindNext(New Structure("Ref", Header.Ref)) Then
					
					TaxesHeaderSel = TaxesHeaderSelVariants.Select(QueryResultIteration.ByGroups);
					
					If TaxesHeaderSel.FindNext(New Structure("Variant", Header.Variant)) Then
						
						TaxSectionHeaderArea = Template.GetArea("TaxSectionHeader");
						TotalsAndTaxesAreasArray.Add(TaxSectionHeaderArea);
						
						TaxesSel = TaxesHeaderSel.Select();
						
						While TaxesSel.Next() Do
							TaxSectionLineArea = Template.GetArea("TaxSectionLine");
							TaxSectionLineArea.Parameters.Fill(TaxesSel);
							TotalsAndTaxesAreasArray.Add(TaxSectionLineArea);
						EndDo;
					EndIf;
				EndIf;
			EndIf;
#EndRegion
			
#Region PrintProformaInvoiceLinesArea
            If DisplayPrintOption Then
                If PrintParams.Discount Then
                    If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                        // Template 1: Hide "Intem #", show "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutCode");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutCode");
                    Else
                        // Template 2: Show all columns
                        LineHeaderArea = Template.GetArea("LineHeader");
                        LineSectionArea	= Template.GetArea("LineSection");
                    EndIf;
                Else
                    If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                        // Template 3: Hide "Intem #", hide "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutItemAndDiscount");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutItemAndDiscount");
                    Else
                        // Template 4: Show "Intem #", hide "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutDiscount");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutDiscount");
                    EndIf;
                EndIf;
            Else
                LineHeaderArea = Template.GetArea("LineHeader");
        		LineSectionArea	= Template.GetArea("LineSection");
            EndIf;
            
       		SpreadsheetDocument.Put(LineHeaderArea);
            
			AreasToBeChecked = New Array;
			
			// Bundles
  			TableInventoty = BundlesServer.AssemblyTableByBundles(Header.Ref, Header, TableColumns, LineTotalArea);
			EmptyColor = LineSectionArea.CurrentArea.TextColor;
			// End Bundles
			
/////////////  TableInventoty Cicle  ////////////////////
			For Each TabSelection In TableInventoty Do
				
				If TabSelection.IsFreightService Then
					Continue;
				EndIf;
				
				LineSectionArea.Parameters.Fill(TabSelection);
				
				DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection);
                
                // Display selected codes if functional option is turned on.
                If DisplayPrintOption Then
                    CodesPresentation = PrintManagementServerCallDrive.GetCodesPresentation(PrintParams, TabSelection.Products);
                    If PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.SeparateColumn Then
                        LineSectionArea.Parameters.SKU = CodesPresentation;
                    ElsIf PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.ProductColumn Then
                        LineSectionArea.Parameters.ProductDescription = LineSectionArea.Parameters.ProductDescription + Chars.CR + CodesPresentation;
                    EndIf;
                EndIf;
                
				// Bundles
                If DisplayPrintOption Then
                    If PrintParams.Discount Then
                        If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                            LineSectionArea.Areas.LineSectionWithoutCode.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        Else
                            LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        EndIf;
                    Else
                        // Recalculate the price to display the correct information with a hidden discount.
                        LineSectionArea.Parameters.Price = TabSelection.Amount / TabSelection.Quantity;
                        
                        If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                            LineSectionArea.Areas.LineSectionWithoutItemAndDiscount.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        Else
                            LineSectionArea.Areas.LineSectionWithoutDiscount.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        EndIf;
                    EndIf;
                Else
                    LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                EndIf;
				// End Bundles
				
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(LineSectionArea);
				AreasToBeChecked.Add(LineTotalEmptyArea);
				
				AreasToBeChecked.Add(SeeNextPageArea);
				
				If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(LineSectionArea);
				Else
					//AreasToBeChecked.Clear();
					//AreasToBeChecked.Add(LineSectionArea);
					//
					//If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) AND
					//										TabSelection.LineNumber < LinesMaxNumber Then
					//	
					//	SpreadsheetDocument.Put(LineSectionArea);
					//	Continue;
					//EndIf;
					
					SpreadsheetDocument.Put(LineTotalEmptyArea);
					SpreadsheetDocument.Put(SeeNextPageArea);
					SpreadsheetDocument.PutHorizontalPageBreak();
					
					SpreadsheetDocument.Put(TitleArea);
					SpreadsheetDocument.Put(LineHeaderArea);
					SpreadsheetDocument.Put(LineSectionArea);
				EndIf;
			EndDo;
/////////////  TableInventoty Cicle  ////////////////////
#EndRegion

#Region PrintProformaInvoiceTotalsAndTaxesArea
	#Region PrintAdditionalAttributes
			If DisplayPrintOption And
					PrintParams.AdditionalAttributes And
					PrintManagementServerCallDrive.HasAdditionalAttributes(Header.Ref) Then
                
                SpreadsheetDocument.Put(EmptyLineArea);
                
                AddAttribHeader = Template.GetArea("AdditionalAttributesStaticHeader");
                SpreadsheetDocument.Put(AddAttribHeader);
                
                SpreadsheetDocument.Put(EmptyLineArea);
                
                AddAttribHeader = Template.GetArea("AdditionalAttributesHeader");
                SpreadsheetDocument.Put(AddAttribHeader);
                
                AddAttribRow = Template.GetArea("AdditionalAttributesRow");
                
                For each Attr In Header.Ref.AdditionalAttributes Do
                    AddAttribRow.Parameters.AddAttributeName = Attr.Property.Title;
                    AddAttribRow.Parameters.AddAttributeValue = Attr.Value;
                    SpreadsheetDocument.Put(AddAttribRow);
                EndDo;
            EndIf;
    #EndRegion
            
			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(LineTotalArea);
			
			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				
				SpreadsheetDocument.Put(SeeNextPageArea);
				SpreadsheetDocument.PutHorizontalPageBreak();
				
				SpreadsheetDocument.Put(TitleArea);
				SpreadsheetDocument.Put(LineHeaderArea);
			EndIf;

			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(EmptyLineArea);
			AreasToBeChecked.Add(LineTotalArea);
			
	#Region PrintProformaInvoiceEmptyLines
			For i = 1 To 99 Do
				If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(LineTotalArea);
					Break;
				Else
					SpreadsheetDocument.Put(EmptyLineArea);
				EndIf;
			EndDo;
	#EndRegion
#EndRegion
			
			//	DON'T MOVE THIS LINE - only INSIDE the While the Header.Ref is visible
			PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
		EndDo;
		// Header.Next() 
	EndDo;
	// HeaderVariants 
	
#Region PrintProformaInvoiceFooter
	SpreadsheetDocument.Footer.Enabled = True;
	SpreadsheetDocument.Footer.Font = new Font("Calibri", 9);
	SpreadsheetDocument.Footer.RightText = "Pagina [&PageNumber] din [&PagesTotal]";
#EndRegion
	
	SpreadsheetDocument.PageSize = "A4";
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;

EndFunction		// PrintProformaInvoice()

//////////////////////////////////////////////////////////////////////
// Document printing procedure AnexaContract
//  copied Function PrintProformaInvoice() and adapted
//
// Parameters:
//  <Parameter1>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//  <Parameter2>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//
// Returns:
//   <Type.Subtype>   - <returned value description>
//
Function PrintAnexaContract(ObjectsArray, PrintObjects, PrintParams, AdditionalParameters = Undefined)
	
    DisplayPrintOption = False;
	PrintParams.Insert("Copies", 1);
    
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_AnexaContract";
	SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_AnexaContract";
	SpreadsheetDocument.FitToPage = True;
	
	Query = New Query();
	Query.SetParameter("ObjectsArray", ObjectsArray);

	// Composing data Query with standart Function (modified light)
	Query.Text = GetQueryTextFromSalesOrder();
	ResultArray = Query.ExecuteBatch();
	
	FirstDocument = True;
	//LinesMaxNumber = 42;

	HeaderVariants = ResultArray[4].Select(QueryResultIteration.ByGroups);
	TaxesHeaderSelVariants = ResultArray[5].Select(QueryResultIteration.ByGroups);
	TotalLineNumber = ResultArray[6].Unload();
	
	// Bundles
	TableColumns = ResultArray[4].Columns;
	// End Bundles
	
	// 
	Template = PrintManagement.PrintFormTemplate(AdditionalParameters.UserPrintTemplate);
	
	// Reading ALL print areas  from Template received
	TitleArea = Template.GetArea("Title");
	CompanyInfoArea = Template.GetArea("CompanyInfo");
	CounterpartyInfoArea = Template.GetArea("CounterpartyInfo");
	
	LineTotalArea = Template.GetArea("LineTotal");
	LineTotalEmptyArea = Template.GetArea("LineTotalEmpty");
	
	LineHeaderArea = Template.GetArea("LineHeader");
	LineSectionArea	= Template.GetArea("LineSection");
	EmptyLineArea = Template.GetArea("EmptyLine");
	
	SeeNextPageArea	= Template.GetArea("SeeNextPage");
	
	AreasToBeChecked = New Array;
	
	// HeaderVariants 
	While HeaderVariants.Next() Do
		
		Header = HeaderVariants.Select(QueryResultIteration.ByGroups);
		// Header.Next() 
		While Header.Next() Do
			
			If Not FirstDocument Then
				SpreadsheetDocument.PutHorizontalPageBreak();
			EndIf;
			FirstDocument = False;
			
			FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
			
#Region PrintAnexaContractTitleArea
			TitleArea.Parameters.Fill(Header);
			SpreadsheetDocument.Put(TitleArea);
#EndRegion
			
#Region PrintAnexaContractCompanyInfoArea
			InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(Header.Company,
																		Header.DocumentDate, ,
																		Header.BankAccount,
																		Header.CompanyVATNumber);
			CompanyInfoArea.Parameters.Fill(InfoAboutCompany);
#EndRegion
			
#Region PrintAnexaContractCounterpartyInfoArea
			CounterpartyInfoArea.Parameters.Fill(Header);
			
			InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(Header.Counterparty, Header.DocumentDate, ,);
			CounterpartyInfoArea.Parameters.Fill(InfoAboutCounterparty);
			
			CounterpartyInfoArea.Parameters.CounterpartyFullDescr = InfoAboutCounterparty.FullDescr;
			
			If ValueIsFilled(Header.CompanyLogoFile) Then
				
				PictureData = AttachedFiles.GetBinaryFileData(Header.CompanyLogoFile);
				If ValueIsFilled(PictureData) Then
					CounterpartyInfoArea.Drawings.Logo.Picture = New Picture(PictureData);
				EndIf;
			Else
				CounterpartyInfoArea.Drawings.Delete(TitleArea.Drawings.Logo);
			EndIf;
			
			SpreadsheetDocument.Put(CounterpartyInfoArea);
#EndRegion
			
#Region PrintAnexaContractTotalsAndTaxesAreaPrefill
            LineTotalArea.Parameters.Fill(Header);
			
			LineTotalArea.Parameters.CompanyFullDescr = InfoAboutCompany.FullDescr;
			LineTotalArea.Parameters.CounterpartyFullDescr = InfoAboutCounterparty.FullDescr;
			LineTotalArea.Parameters.CounterpartyContactPersonPosition = Header.CounterpartyContactPerson.Position;
			
			LineTotalEmptyArea.Parameters.CompanyFullDescr = InfoAboutCompany.FullDescr;
			LineTotalEmptyArea.Parameters.CounterpartyFullDescr = InfoAboutCounterparty.FullDescr;
			LineTotalEmptyArea.Parameters.CounterpartyContactPersonPosition = Header.CounterpartyContactPerson.Position;
#EndRegion
			
#Region PrintAnexaContractLinesArea
       		SpreadsheetDocument.Put(LineHeaderArea);
			
			// Bundles
  			TableInventoty = BundlesServer.AssemblyTableByBundles(Header.Ref, Header, TableColumns, LineTotalArea);
			EmptyColor = LineSectionArea.CurrentArea.TextColor;
			// End Bundles
			
/////////////  TableInventoty Cicle  ////////////////////
			For Each TabSelection In TableInventoty Do
				
				If TabSelection.IsFreightService = True Then
					Continue;
				EndIf;
				
				LineSectionArea.Parameters.Fill(TabSelection);
				
				DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection);
                
				// Bundles
			    LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
				// End Bundles
				
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(LineSectionArea);
				AreasToBeChecked.Add(EmptyLineArea);
				AreasToBeChecked.Add(EmptyLineArea);
				AreasToBeChecked.Add(EmptyLineArea);
				AreasToBeChecked.Add(LineTotalEmptyArea);
				AreasToBeChecked.Add(SeeNextPageArea);
				
				If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(LineSectionArea);
					//Message(TabSelection.LineNumber);
				Else
					//AreasToBeChecked.Clear();
					//AreasToBeChecked.Add(LineSectionArea);
					//AreasToBeChecked.Add(LineTotalEmptyArea);
					//
					//If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
					//		AND TabSelection.LineNumber < LinesMaxNumber Then
					//	SpreadsheetDocument.Put(LineSectionArea);
						//Message(TabSelection.LineNumber);
					//	Continue;
					//EndIf;
					
					SpreadsheetDocument.Put(EmptyLineArea);
					SpreadsheetDocument.Put(EmptyLineArea);
					SpreadsheetDocument.Put(LineTotalEmptyArea);
					SpreadsheetDocument.Put(SeeNextPageArea);
					SpreadsheetDocument.PutHorizontalPageBreak();
					
					SpreadsheetDocument.Put(TitleArea);
					SpreadsheetDocument.Put(LineHeaderArea);
					SpreadsheetDocument.Put(LineSectionArea);
				EndIf;
			EndDo;
/////////////  TableInventoty Cicle  ////////////////////
#EndRegion

#Region PrintAnexaContractTotalsAndTaxesArea
			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(LineTotalArea);
			If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
				
				SpreadsheetDocument.Put(SeeNextPageArea);
				SpreadsheetDocument.PutHorizontalPageBreak();
				
				SpreadsheetDocument.Put(TitleArea);
				SpreadsheetDocument.Put(LineHeaderArea);
			EndIf;

			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(EmptyLineArea);
			AreasToBeChecked.Add(LineTotalArea);
			
	#Region PrintQuoteEmptyLines
			For i = 1 To 99 Do
				If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(LineTotalArea);
					Break;
				Else
					SpreadsheetDocument.Put(EmptyLineArea);
				EndIf;
			EndDo;
	#EndRegion
#EndRegion
			
			//	DON'T MOVE THIS LINE - only INSIDE the While the Header.Ref is visible
			PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
		EndDo;
		// Header.Next() 
	EndDo;
	// HeaderVariants 
	
#Region PrintQuoteFooter
	SpreadsheetDocument.Footer.Enabled = True;
	SpreadsheetDocument.Footer.Font = new Font("Calibri", 8);
	
	SpreadsheetDocument.Footer.LeftText = "Calor SRL";
	SpreadsheetDocument.Footer.CenterText = "http://www.calor.ro  mail:calor@calor.ro";
	SpreadsheetDocument.Footer.RightText = "Ia calitatea la bani marunti!";
#EndRegion
	
	SpreadsheetDocument.PageSize = "A4";
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;

EndFunction // PrintAnexaContract()

Function GetQueryTextFromSalesOrder()
	
	QueryText =
	"SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS Number,
	|	SalesOrder.Date AS Date,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.EstimateIsCalculated AS EstimateIsCalculated,
	|	SalesOrder.ContactPerson AS ContactPerson,
	|	SalesOrder.ShippingAddress AS ShippingAddress,
	|	SalesOrder.StructuralUnitReserve AS StructuralUnit,
	|	SalesOrder.DeliveryOption AS DeliveryOption
	|INTO SalesOrders
	|FROM
	|	Document.SalesOrder AS SalesOrder
	|WHERE
	|	SalesOrder.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS DocumentNumber,
	|	SalesOrder.Date AS DocumentDate,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	|	CASE
	|		WHEN SalesOrder.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN SalesOrder.ContactPerson
	|		WHEN CounterpartyContracts.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN CounterpartyContracts.ContactPerson
	|		ELSE Counterparties.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.ShippingAddress AS ShippingAddress,
	|	SalesOrder.StructuralUnit AS StructuralUnit,
	|	SalesOrder.DeliveryOption AS DeliveryOption
	|INTO Header
	|FROM
	|	SalesOrders AS SalesOrder
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON SalesOrder.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON SalesOrder.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON SalesOrder.Contract = CounterpartyContracts.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrderInventory.Ref AS Ref,
	|	SalesOrderInventory.LineNumber AS LineNumber,
	|	SalesOrderInventory.Products AS Products,
	|	SalesOrderInventory.Characteristic AS Characteristic,
	|	SalesOrderInventory.Batch AS Batch,
	|	SalesOrderInventory.Quantity AS Quantity,
	|	SalesOrderInventory.Reserve AS Reserve,
	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
	|	CASE
	|		WHEN SalesOrderInventory.Quantity = 0
	|			THEN 0
	|		ELSE SalesOrderInventory.Price + (SalesOrderInventory.Total - SalesOrderInventory.Amount - SalesOrderInventory.VATAmount) / SalesOrderInventory.Quantity
	|	END AS Price,
	|	SalesOrderInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SalesOrderInventory.Total - SalesOrderInventory.VATAmount AS Amount,
	|	SalesOrderInventory.VATRate AS VATRate,
	|	SalesOrderInventory.VATAmount AS VATAmount,
	|	SalesOrderInventory.Total AS Total,
	|	SalesOrderInventory.Content AS Content,
	|	SalesOrderInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	SalesOrderInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	SalesOrderInventory.ConnectionKey AS ConnectionKey,
	|	SalesOrderInventory.BundleProduct AS BundleProduct,
	|	SalesOrderInventory.BundleCharacteristic AS BundleCharacteristic,
	|	CASE
	|		WHEN SalesOrderInventory.DiscountMarkupPercent + SalesOrderInventory.AutomaticDiscountsPercent > 100
	|			THEN 100
	|		ELSE SalesOrderInventory.DiscountMarkupPercent + SalesOrderInventory.AutomaticDiscountsPercent
	|	END AS DiscountPercent,
	|	SalesOrderInventory.Amount AS PureAmount,
	|	ISNULL(VATRates.Rate, 0) AS NumberVATRate,
	|	CAST(SalesOrderInventory.Quantity * SalesOrderInventory.Price - SalesOrderInventory.Amount AS NUMBER(15, 2)) AS DiscountAmount
	|INTO FilteredInventory
	|FROM
	|	Document.SalesOrder.Inventory AS SalesOrderInventory
	|		LEFT JOIN Catalog.VATRates AS VATRates
	|		ON SalesOrderInventory.VATRate = VATRates.Ref
	|WHERE
	|	SalesOrderInventory.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.BankAccount AS BankAccount,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
	|	FilteredInventory.LineNumber AS LineNumber,
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	FilteredInventory.Price AS Price,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountRate,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	SUM(FilteredInventory.Amount) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN FilteredInventory.Quantity
	|			ELSE 0
	|		END) AS Freight,
	|	SUM(FilteredInventory.Total) AS Total,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN 0
	|			ELSE FilteredInventory.Quantity
	|		END) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	FilteredInventory.Batch AS Batch,
	|	Header.ShippingAddress AS ShippingAddress,
	|	Header.StructuralUnit AS StructuralUnit,
	|	Header.DeliveryOption AS DeliveryOption,
	|	CatalogProducts.IsFreightService AS IsFreightService,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic,
	|	FilteredInventory.DiscountPercent AS DiscountPercent,
	|	CASE
	|		WHEN Header.AmountIncludesVAT
	|			THEN CAST(FilteredInventory.DiscountAmount / (1 + FilteredInventory.NumberVATRate / 100) AS NUMBER(15, 2))
	|		ELSE FilteredInventory.DiscountAmount
	|	END AS DiscountAmount,
	|	CASE
	|		WHEN Header.AmountIncludesVAT
	|			THEN CAST(FilteredInventory.PureAmount / (1 + FilteredInventory.NumberVATRate / 100) AS NUMBER(15, 2))
	|		ELSE FilteredInventory.PureAmount
	|	END AS NetAmount
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	Header.DocumentNumber,
	|	Header.DocumentDate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Ref,
	|	Header.Counterparty,
	|	Header.CompanyLogoFile,
	|	Header.Contract,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	Header.DocumentCurrency,
	|	CatalogProducts.SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	FilteredInventory.VATRate,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.Products,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	FilteredInventory.Price,
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.MeasurementUnit,
	|	FilteredInventory.Batch,
	|	FilteredInventory.LineNumber,
	|	Header.ShippingAddress,
	|	Header.StructuralUnit,
	|	Header.DeliveryOption,
	|	CatalogProducts.IsFreightService,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic,
	|	FilteredInventory.DiscountPercent,
	|	CASE
	|		WHEN Header.AmountIncludesVAT
	|			THEN CAST(FilteredInventory.DiscountAmount / (1 + FilteredInventory.NumberVATRate / 100) AS NUMBER(15, 2))
	|		ELSE FilteredInventory.DiscountAmount
	|	END,
	|	CASE
	|		WHEN Header.AmountIncludesVAT
	|			THEN CAST(FilteredInventory.PureAmount / (1 + FilteredInventory.NumberVATRate / 100) AS NUMBER(15, 2))
	|		ELSE FilteredInventory.PureAmount
	|	END
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.BankAccount AS BankAccount,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	Tabular.Amount AS Amount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	Tabular.Freight AS FreightTotal,
	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountRate
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Products AS Products,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.MeasurementUnit AS MeasurementUnit,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	Tabular.ShippingAddress AS ShippingAddress,
	|	Tabular.StructuralUnit AS StructuralUnit,
	|	Tabular.DeliveryOption AS DeliveryOption,
	|	0 AS Variant,
	|	Tabular.IsFreightService AS IsFreightService,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic,
	|	Tabular.DiscountPercent AS DiscountPercent,
	|	Tabular.NetAmount AS NetAmount
	|FROM
	|	Tabular AS Tabular
	|
	|ORDER BY
	|	Tabular.DocumentNumber,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(CounterpartyContactPerson),
	|	MAX(BankAccount),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(FreightTotal),
	|	SUM(DiscountAmount),
	|	MAX(ShippingAddress),
	|	MAX(StructuralUnit),
	|	MAX(DeliveryOption)
	|BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	0 AS Variant,
	|	Tabular.VATRate AS VATRate,
	|	SUM(Tabular.Amount) AS Amount,
	|	SUM(Tabular.VATAmount) AS VATAmount
	|FROM
	|	Tabular AS Tabular
	|
	|GROUP BY
	|	Tabular.Ref,
	|	Tabular.VATRate
	|TOTALS BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	COUNT(Tabular.LineNumber) AS LineNumber,
	|	Tabular.Ref AS Ref,
	|	SUM(Tabular.Quantity) AS Quantity,
	|	0 AS Variant
	|FROM
	|	Tabular AS Tabular
	|WHERE
	|	NOT Tabular.IsFreightService
	|
	|GROUP BY
	|	Tabular.Ref";
	
	Return QueryText;
	
EndFunction		//	GetQueryTextFromSalesOrder()

