﻿
&AtClient
Procedure CommandProcessing(CommandParameter, CommandExecuteParameters)
    
	FormParameters = New Structure("VariantKey, UsePurposeKey, Filter, GenerateOnOpen, ReportVariantCommandVisible",
							"Statement",
							CommandParameter,
							New Structure("Products", CommandParameter),
							True,
							True);
	
	OpenForm("Report.FisaProdus.Form",
							FormParameters,
							,
							"Products=" + CommandParameter,
							CommandExecuteParameters.Window);
    
EndProcedure
