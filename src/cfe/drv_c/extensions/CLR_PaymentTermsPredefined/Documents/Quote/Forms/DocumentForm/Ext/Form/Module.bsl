﻿
&AtServer
&After("OnCreateAtServer")
Procedure CLR_PT_OnCreateAtServer(Cancel, StandardProcessing)
	CLR_PT_AdditionalFunctionalityServer.PaymentTermsPredefined(ThisForm);
EndProcedure

&AtClient
Procedure CLR_PT_Plus10Days(Command) Export
	//CLR_PT_PlusXXDaysPayment(10);
	CLR_PT_AdditionalFunctionalityClient.CLR_PT_PlusXXDaysPayment(10);
EndProcedure

&AtClient
Procedure CLR_PT_Plus15Days(Command) Export
	//CLR_PT_PlusXXDaysPayment(15);
	CLR_PT_AdditionalFunctionalityClient.CLR_PT_PlusXXDaysPayment(15);
EndProcedure

&AtClient
Procedure CLR_PT_Plus30Days(Command) Export
	//CLR_PT_PlusXXDaysPayment(30);
	CLR_PT_AdditionalFunctionalityClient.CLR_PT_PlusXXDaysPayment(30);
EndProcedure

//&AtClient
//Procedure CLR_PT_Plus40Days(Command) Export 
//	CLR_PT_PlusXXDaysPayment(40);
//EndProcedure

//&AtClient
//Procedure CLR_PT_PlusXXDaysPayment(Days) Export 
//	Object.PaymentCalendar[0].PaymentDate = Object.Date + Days * 24 * 60 * 60;
//	//AddMonth(Object.Date, 1)
//EndProcedure
