﻿////////////////////////////////////////////////////////////
// <Function description>
//
//
// Parameters:
//  <Parameter1>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>                                                                           
//  <Parameter2>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//
// Returns:
//   <Type.Subtype>   - <returned value description>
//
Procedure PaymentTermsPredefined(FormObject) Export

	FromDocument = FormObject.Object.Ref.Metadata().Name;
	
	If FromDocument = "Quote" Then
		// until repare this function  - до лучших времён
		Return;
		
		myPaymentTermsPredefinedGroup = FormObject.Items.Insert("PaymentTermsPredefined",
											Type("FormGroup"),
											FormObject.Items.GroupBillingCalendarString,
											FormObject.Items.PaymentCalendarPaymentPercent);
		
	ElsIf FromDocument = "SalesOrder" Or FromDocument = "SalesInvoice" Then
		myPaymentTermsPredefinedGroup = FormObject.Items.Insert("PaymentTermsPredefined",
											Type("FormGroup"),
											FormObject.Items.GroupPaymentCalendarWithoutSplitting,
											FormObject.Items.PaymentDate);
	Else
		Message(NStr("en = 'Unknown Document type'; ro = 'Unknown Document type'; ru = 'Unknown Document type'"));
	EndIf;
	
	myPaymentTermsPredefinedGroup.Type = FormGroupType.UsualGroup;
	myPaymentTermsPredefinedGroup.Group = ChildFormItemsGroup.AlwaysHorizontal;
	myPaymentTermsPredefinedGroup.ShowTitle = False;
	myPaymentTermsPredefinedGroup.Representation = UsualGroupRepresentation.None;
	myPaymentTermsPredefinedGroup.HorizontalStretch = True;
	
	If FromDocument = "Quote" Then
		FormObject.Items.Move(FormObject.Items.PaymentCalendarPaymentDate, FormObject.Items.PaymentTermsPredefined);
	ElsIf FromDocument = "SalesOrder" Or FromDocument = "SalesInvoice" Then
		FormObject.Items.Move(FormObject.Items.PaymentDate, FormObject.Items.PaymentTermsPredefined);
	Else
		Message(NStr("en = 'Unknown Document type'; ro = 'Unknown Document type'; ru = 'Unknown Document type'"));
	EndIf;
	
	CreateCommandAndButton(FormObject, myPaymentTermsPredefinedGroup, 10);
	CreateCommandAndButton(FormObject, myPaymentTermsPredefinedGroup, 15);
	CreateCommandAndButton(FormObject, myPaymentTermsPredefinedGroup, 30);
	//CreateCommandAndButton(FormObject, myPaymentTermsPredefinedGroup, 40);

EndProcedure // PaymentTermsPredefined()

// <Procedure description>
//
// Parameters:
//  <Parameter1>  - <Type.Subtype> - <parameter description>
//                  <parameter description continued>
//  <Parameter2>  - <Type.Subtype> - <parameter description>
//                  <parameter description continued>
//
Procedure CreateCommandAndButton(FormObject, myGroup, Days)

	myCommandXX = FormObject.Commands.Add("Plus" + Days + "Days");
	myCommandXX.Action = "CLR_PT_Plus" + Days + "Days";
	myCommandXX.Title = "+" + Days + " " + NStr("en = 'days'; ro = 'zile'; ru = 'дней'");
	myCommandXX.ToolTip = "Plus " + Days + " " + NStr("en = 'due days'; ro = 'zile scadență'; ru = 'дней отсрочки'");
	myCommandXX.ModifiesStoredData = True;
	myCommandXX.Representation = ButtonRepresentation.Text;
	
	myButtonXX = FormObject.Items.Add("Plus" + Days + "Days", Type("FormButton"), myGroup);
	//myButtonXX.Title = "+" + Days + " zile";
	myButtonXX.CommandName = "Plus" + Days + "Days";

EndProcedure // CreateCommandAndButton()
