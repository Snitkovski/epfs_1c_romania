﻿
&Вместо("ВыполнитьДействиеОбменаДляУзлаИнформационнойБазыЧерезWebСервис")
Процедура Маг1С_ВыполнитьДействиеОбменаДляУзлаИнформационнойБазыЧерезWebСервис(Отказ, УзелИнформационнойБазы, ДействиеПриОбмене, ПараметрыОбмена)
	
	УстановитьПривилегированныйРежим(Истина);
	
	ВидТранспорта = РегистрыСведений["НастройкиТранспортаОбменаДанными"].ВидТранспортаСообщенийОбменаПоУмолчанию(УзелИнформационнойБазы);
	
	Если Маг1СИнтеграцияСВебВитринойСлужебный.ЭтоУзелОбменаСВитриной(УзелИнформационнойБазы)
		И ВидТранспорта = Перечисления["ВидыТранспортаСообщенийОбмена"].WS Тогда
		
		ОтказЗапуска = Истина;
		Обработки.Маг1СОбменСВебВитриной.ПроверитьВозможностьЗапускаОбмена(УзелИнформационнойБазы, ОтказЗапуска);
		
		ВремяЗавершения = ТекущаяДатаСеанса() + 5 * 60;
		Пока ОтказЗапуска
			И ТекущаяДатаСеанса() < ВремяЗавершения Цикл
			Маг1СИнтеграцияСВебВитринойСлужебный.Пауза(30);
			
			ОтказЗапуска = Ложь;
			Обработки.Маг1СОбменСВебВитриной.ПроверитьВозможностьЗапускаОбмена(УзелИнформационнойБазы, ОтказЗапуска);
		КонецЦикла;
		
		ЛогинИПП = "";
		Если ОбщегоНазначения.РазделениеВключено() Тогда
			АутентификацияИППВыполнена = Истина;
		Иначе
			АутентификацияИППВыполнена = ИнтернетПоддержкаПользователей.ЗаполненыДанныеАутентификацииПользователяИнтернетПоддержки();
			
			Если АутентификацияИППВыполнена Тогда
				ЛогинИПП = ИнтернетПоддержкаПользователей.ДанныеАутентификацииПользователяИнтернетПоддержки().Логин;
			КонецЕсли;
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(ЛогинИПП) Тогда
			ВызватьИсключение НСтр("ru = 'Для выполнения синхронизации требуется подключиться к Порталу 1С:ИТС.'");
		КонецЕсли;
		
		НастройкиПодключения = РегистрыСведений["НастройкиТранспортаОбменаДанными"].НастройкиТранспортаWS(УзелИнформационнойБазы);
		
		ТокенАвторизации = Обработки.Маг1СНастройкаВебВитрины.ПолучитьТокенАвторизацииОбменаСВитриной(
			НастройкиПодключения.WSURLВебСервиса,
			ЛогинИПП);
		
		Если ДействиеПриОбмене = Перечисления["ДействияПриОбмене"].ЗагрузкаДанных Тогда
			ИдентификаторОперации = Обработки.Маг1СОбменСВебВитриной.ПодготовитьВыгрузкуДанныхССайта(
				НастройкиПодключения.WSURLВебСервиса,
				ТокенАвторизации);
				
			ИмяВременногоФайла    = ПолучитьИмяВременногоФайла("zip");
			ИмяВременногоКаталога = ПолучитьИмяВременногоФайла("");
			
			ЧтениеАрхива = Неопределено;
			
			Результат = Неопределено;
			ВремяЗавершения = ТекущаяДатаСеанса() + 5 * 60;
			Пока Результат = Неопределено
				И ТекущаяДатаСеанса() < ВремяЗавершения Цикл
				Маг1СИнтеграцияСВебВитринойСлужебный.Пауза(15);
				Результат = Обработки.Маг1СОбменСВебВитриной.ПолучитьДанныеССайта(
					НастройкиПодключения.WSURLВебСервиса,
					ТокенАвторизации,
					ИдентификаторОперации,
					ИмяВременногоФайла);
			КонецЦикла;
			
			Если Результат = Неопределено Тогда
				ВызватьИсключение НСтр("ru = 'Операция получения данных с сайта не завершилась за отведенное время.'");
			ИначеЕсли Результат = Истина Тогда
				Обработки.Маг1СОбменСВебВитриной.УдалитьФайлВыгрузкиНаСайте(
					НастройкиПодключения.WSURLВебСервиса,
					ТокенАвторизации,
					ИдентификаторОперации);
					
				Попытка
					СоздатьКаталог(ИмяВременногоКаталога);
						
					ЧтениеАрхива = Новый ЧтениеZipФайла(ИмяВременногоФайла);
					ЧтениеАрхива.ИзвлечьВсе(ИмяВременногоКаталога, РежимВосстановленияПутейФайловZIP.НеВосстанавливать);
					ЧтениеАрхива.Закрыть();
					
					ИмяИзвлеченногоФайла = НайтиФайлы(ИмяВременногоКаталога, "*.xml", Ложь).Получить(0).ПолноеИмя;
				
					Обработки.Маг1СОбменСВебВитриной.ЗагрузитьДанные(УзелИнформационнойБазы, ИмяИзвлеченногоФайла);
					
					УдалитьФайлы(ИмяВременногоФайла);
					УдалитьФайлы(ИмяВременногоКаталога);
				Исключение
					УдалитьФайлы(ИмяВременногоФайла);
					УдалитьФайлы(ИмяВременногоКаталога);
					ВызватьИсключение;
				КонецПопытки;
				
				Обработки.Маг1СОбменСВебВитриной.ПодтвердитьЗагрузкуДанныхССайта(
					НастройкиПодключения.WSURLВебСервиса,
					ТокенАвторизации,
					ИдентификаторОперации);
			Иначе
				Обработки.Маг1СОбменСВебВитриной.ЗафиксироватьНачалоОбмена(УзелИнформационнойБазы, ДействиеПриОбмене);
				Обработки.Маг1СОбменСВебВитриной.ЗафиксироватьЗавершениеОбмена(
					УзелИнформационнойБазы,
					ДействиеПриОбмене,
					Перечисления["РезультатыВыполненияОбмена"].Выполнено,
					0);
			КонецЕсли;
		ИначеЕсли ДействиеПриОбмене = Перечисления["ДействияПриОбмене"].ВыгрузкаДанных Тогда
			
			ИмяВременногоФайла = ПолучитьИмяВременногоФайла("xml");
			ИмяФайлаАрхива = ПолучитьИмяВременногоФайла("zip");
			
			ДанныеВыгружены = Ложь;
			Попытка
				ДанныеВыгружены = Обработки.Маг1СОбменСВебВитриной.ВыгрузитьДанные(УзелИнформационнойБазы, ИмяВременногоФайла);
				
				Если ДанныеВыгружены Тогда
					НомерСообщения = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(УзелИнформационнойБазы, "НомерОтправленного");
					
					ЗаписьАрхива = Новый ЗаписьZipФайла(ИмяФайлаАрхива, , , МетодСжатияZIP.Сжатие, УровеньСжатияZIP.Оптимальный);
					ЗаписьАрхива.Добавить(ИмяВременногоФайла, РежимСохраненияПутейZIP.НеСохранятьПути);
					ЗаписьАрхива.Записать();
				
					ИдентификаторОперации = Обработки.Маг1СОбменСВебВитриной.ОтправитьДанныеНаСайт(
						НастройкиПодключения.WSURLВебСервиса,
						ТокенАвторизации,
						ИмяФайлаАрхива);
				КонецЕсли;
				УдалитьФайлы(ИмяВременногоФайла);
				УдалитьФайлы(ИмяФайлаАрхива);
			Исключение
				УдалитьФайлы(ИмяВременногоФайла);
				УдалитьФайлы(ИмяФайлаАрхива);
				ВызватьИсключение;
			КонецПопытки;
			
			Если ДанныеВыгружены Тогда
				Результат = Неопределено;
				ВремяЗавершения = ТекущаяДатаСеанса() + 5 * 60;
				Пока Результат = Неопределено
					И ТекущаяДатаСеанса() < ВремяЗавершения Цикл
					Маг1СИнтеграцияСВебВитринойСлужебный.Пауза(15);
					Результат = Обработки.Маг1СОбменСВебВитриной.ПолучитьПодтверждениеЗагрузкиДанныхНаСайт(
						НастройкиПодключения.WSURLВебСервиса,
						ТокенАвторизации,
						ИдентификаторОперации);
				КонецЦикла;
					
				Если Результат = Неопределено Тогда
					ВызватьИсключение НСтр("ru = 'Операция загрузки данных на сайте не завершилась за отведенное время.'");
				КонецЕсли;
				
				Обработки.Маг1СОбменСВебВитриной.УдалитьРегистрациюИзменений(
					УзелИнформационнойБазы, НомерСообщения);
			КонецЕсли;
		КонецЕсли;
		
		Возврат;
	КонецЕсли;
	УстановитьПривилегированныйРежим(Ложь);
	
	ПродолжитьВызов(Отказ, УзелИнформационнойБазы, ДействиеПриОбмене, ПараметрыОбмена);
	
КонецПроцедуры
