﻿
&Before("FillCheckProcessing")
Procedure bFix_FillCheckProcessing(Cancel, CheckedAttributes)
    
     AttributeIndex=CheckedAttributes.Find("CompanyVATNumber");
     
     If AttributeIndex=Undefined Then
         Return
     EndIf;
     
     CheckedAttributes.Delete(AttributeIndex)
    
EndProcedure
