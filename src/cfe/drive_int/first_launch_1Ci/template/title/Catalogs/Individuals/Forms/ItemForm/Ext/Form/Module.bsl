﻿
&AtClient
Procedure TitleAndLastNamePrefix_BeforeWriteAfter(Cancel, WriteParameters)
	
	Object.Description = TrimAll(String(Object.TitleAndLastNamePrefix_Title) +
		?(ValueIsFilled(Object.FirstName), " " + Object.FirstName, "") +
		?(ValueIsFilled(Object.MiddleName), " " + Object.MiddleName, "") +
		?(ValueIsFilled(Object.TitleAndLastNamePrefix_LastNamePrefix), " " + Object.TitleAndLastNamePrefix_LastNamePrefix, "") +
		?(ValueIsFilled(Object.LastName), " " + Object.LastName, ""));

EndProcedure
