﻿
// Adds to the list of procedures (IB data update handlers)
// for all supported versions of the library or configuration.
// Appears before the start of IB data update to build up the update plan.
//
// Parameters:
//  Handlers - ValueTable - See description
// of the fields in the procedure UpdateResults.UpdateHandlersNewTable
//
// Example of adding the procedure-processor to the list:
//
//  Handler = Handlers.Add();
//  Handler.Version       = "1.0.0.0";
//  Handler.Procedure     = "IBUpdate.SwitchToVersion_1_0_0_0";
//  Handler.ExclusiveMode = False;
//  Handler.Optional      = True;
// 
Procedure OnAddUpdateExtensionHandlers(Handlers) Export
	
EndProcedure
