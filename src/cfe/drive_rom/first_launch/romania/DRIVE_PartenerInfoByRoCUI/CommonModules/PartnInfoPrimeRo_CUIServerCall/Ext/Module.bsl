﻿#Region Internal

Function ParsePhoneNumberRO(strPhoneNumber) Export
// see http://ro.wikipedia.org/wiki/Lista_prefixelor_telefonice_(România)
// or Google "prefixe telefonice"
	info  = New Structure("CountryCode, CityCode, PhoneNumber");
	info.CountryCode = "40";
	phone = "";
	For i = 1 To StrLen(strPhoneNumber) Do
		char = Mid(strPhoneNumber, i, 1);
		If char >= "0" And char <= "9" Then
			phone = phone + char;
		EndIf;
	EndDo;
	
	If Left(phone, 2) = info.CountryCode Then
		phone = Right(phone, strLen(phone) - 2)
	EndIf;
	
	CityCode = Left(phone, 1);
	If CityCode = "2" Or CityCode = "3" Then // we have city code
		CityCode = Left(phone, 2);
		If CityCode = "21" Or CityCode = "31" Then
			info.CityCode = CityCode; // city is Bucharest
			info.PhoneNumber = Mid(phone, 3); //Right(phone, StrLen(phone)-2);
		Else
			info.CityCode = Left(phone, 3);
			info.PhoneNumber = Mid(phone, 4); // Right(phone, StrLen(phone)-3);
		EndIf;
	Else
		info.CityCode = "";
		// there's an issue here if the starting 0 should be left or removed;
		// when calling from within the contry, it is required, for both fixed or mobile networks,
		// however, when calling from abroad, it is not allowed on either (it is substituted by the 0 in country prefix 40)
		// for Bucharest, (021) nnnnnnn from Ro, but (40)(21) nnnnnnn from abroad
		// for mobile, ex. 0740 nnnnnn from Ro, 40740 nnnnnn from abroad
		// still, country prefix is 40, not 4
		If Left(phone, 1) = "0" Then
			info.PhoneNumber = Right(phone, StrLen(phone)-1);
		Else
			info.PhoneNumber = phone;
		EndIf;
	EndIf;
	
	Return info;
EndFunction

Function ParseAddressMF(addr, strAddr)Export
// [<streetname>] <number> <locality>
// number is identified as first token from the right starting with a digit
// to avoid digits in street, like "Blvd 1 Decembrie 1918" and such
// some small localities don't have streetname, houses are numbered all together
// street includes streetname and number
	len = StrLen(strAddr);
	If len > 0 Then
		char = Right(strAddr, 1);
		For i2 = 1 To len-1 Do
			i = len - i2;
			char2 = char;
			char = Mid(strAddr, i, 1);
			If IsBlankString(char) And (char2>="0" And char2<="9") Then
				For j = i+1 To len Do
					char = Mid(strAddr, j, 1);
					If IsBlankString(char) Then
						addr.Street = TrimR(Left(strAddr, j-1));
						addr.City = TrimL(Mid(strAddr, j+1));
						Break;
					EndIf;
				EndDo;
			EndIf;
		EndDo;
	EndIf;
	
	Return addr;
EndFunction

Function ContactInformationByAttributeValues(Context, ContentType)Export
	
	Result = New Structure("Presentation, Value");
	
	Content = FillContactStructure(Context, ContentType);
	Result.Presentation = Content.value;
	
	JSONWriter = Новый JSONWriter();
	JSONWriter.SetString();
	WriteJSON(JSONWriter, Content);
	
	Result.Value  =  JSONWriter.Закрыть();
	
	Return Result;
EndFunction

#EndRegion

#Region Private

Function FillContactStructure(Context, ContactInformationType)
	
	ContactStructure = AddressManagerClientServer.NewContactInformationDetails(ContactInformationType);
	
	If ContactInformationType = PredefinedValue("Enum.ContactInformationTypes.Address") Then
		
		ContactStructure.Insert("type",			"Freeform");
		ContactStructure.Insert("AddressType",	"FreeForm");
		ContactStructure.Insert("AddressLine1", TrimAll(Context.Street));
		ContactStructure.Insert("AddressLine2", "");
		ContactStructure.Insert("City",			TrimAll(Context.City));
		ContactStructure.Insert("State",		TrimAll(Context.Region));
		ContactStructure.Insert("PostalCode",	TrimAll(Context.PostalCode));
		ContactStructure.Insert("Country",		TrimAll("ROMANIA"));
		ContactStructure.Insert("Value",		"");
		
		AddressManagerClientServer.UpdateAddressPresentation(ContactStructure, True);
		
	ElsIf  ContactInformationType = PredefinedValue("Enum.ContactInformationTypes.Phone") Then
				
		ContactStructure.AreaCode    = Context.CityCode;
		ContactStructure.Number   	 = Context.PhoneNumber;
		ContactStructure.CountryCode = Context.CountryCode;
		
		PhonePresentation = ContactsManagerClientServer.GeneratePhonePresentation(
			ContactStructure.CountryCode,
			ContactStructure.AreaCode,
			ContactStructure.Number,
			ContactStructure.ExtNumber,
			"");
		ContactStructure.Value = PhonePresentation;
		
	Else
		Raise "ContactInformationByAttributeValues: Unknown ContentType: " + ContactInformationType;
	EndIf;
	
	Return ContactStructure;
	
EndFunction // FillContactStructure()

#EndRegion
