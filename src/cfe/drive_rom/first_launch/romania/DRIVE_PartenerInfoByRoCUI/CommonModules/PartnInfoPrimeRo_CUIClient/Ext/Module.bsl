﻿#Region Private

&AtClient
Procedure WriteInfoContact(ThisForm, Object, Table) Export
	
	UpdateAddress = False;
	UpdatePhoneNumber = False;
	
	Address = New Structure("Region, City, Street, PostalCode");
	
	MyAddrStyle = False;
	AddressStringArray = New Array();
	AddrLine1 = "";
	AddrLine2 = "";
	
	For Each Row In Table Do
		
		If Not Row.ToUpdate Then
			Continue;
		EndIf;
		
		If Row.Name = "Denumire platitor:" Then
			ThisForm.Object.Description = Row.Value;
			ThisForm.Object.DescriptionFull = Row.Value;
			
			Description = Row.Value;
			
			NumberOfBeg = StrFind(Description, " ", SearchDirection.FromEnd);
			ShortDescription = Прав(Description, StrLen(Description) - NumberOfBeg);
			ShortDescriptionWithoutDots = StrReplace(ShortDescription, ".", "");
			ResultShortDescription = Upper(StrReplace(ShortDescriptionWithoutDots, " ", ""));
			
			LegalForm = PartnInfoPrimeRo_Server.InsertLegalForm(ResultShortDescription);
			ThisForm.Object.LegalForm = LegalForm;
			
			Modified = True;
			
		ElsIf Row.Name = "Numar de inmatriculare la Registrul Comertului:" Then
			ThisForm.Object.RegistrationNumber = StrReplace(Row.Value, " ", "");
			Modified = True;
			
		ElsIf Row.Name = "Stare societate:" AND TypeOf(ThisForm.Object.Ref) = Type("CatalogRef.Companies") Then
			
			Description = Row.Value;
			
			NumberOfBeg = StrFind(Description, "data", SearchDirection.FromEnd);
			Result = Прав(Description, StrLen(Description) - NumberOfBeg - 4);
						
			ThisForm.Object.RegistrationDate = TransformStringInData(Result);
			Modified = True;
			
		ElsIf Row.Name = "Taxa pe valoarea adaugata (data luarii in evidenta):" Then
			
			DataLuariiInEvidentaTVA = TransformStringInDataTVA(Row.Value);
			If Not IsEmptyValue(DataLuariiInEvidentaTVA) Then
				If  Not Find(Upper(ThisForm.Object.TIN), "RO") Then
					Message(NStr("en = 'Partnerul a fost trecut in categoria Platitor de TVA ! La CUI sa adugat Prefixul ';
								 |ru = 'Partnerul a fost trecut in categoria Platitor de TVA ! La CUI sa adugat Prefixul ';
								 |ro = 'Partnerul a fost trecut in categoria Platitor de TVA ! La CUI sa adugat Prefixul '") + """RO""",
							MessageStatus.VeryImportant);
					
					ThisForm.Object.TIN = "RO" + ThisForm.Object.TIN;
				Else
					//  Setting upper case for RONG !!!
					ThisForm.Object.TIN = Upper(ThisForm.Object.TIN);
				EndIf;
				
				If TypeOf(ThisForm.Object.Ref) = Type("CatalogRef.Companies") Then
					Object.VATNumbers[0].RegistrationDate = DataLuariiInEvidentaTVA;
					Object.VATNumbers[0].RegistrationCountry = PartnInfoPrimeRo_Server.InsertRegistrationCountry();
					Object.VATNumbers[0].VATNumber = ThisForm.Object.TIN;
				ElsIf TypeOf(ThisForm.Object.Ref) = Type("CatalogRef.Counterparties") Then
					ThisForm.Object.VATNumber = ThisForm.Object.TIN;
				Else
					//  SOMETING  WENT  WRONG !!!
				EndIf;
				
				Modified = True;
			EndIf;
				
		ElsIf Row.Name = "Adresa:" Then
			MyAddrStyle = ?(StrFind(Row.Value, ",") > 0, True, False);
			If MyAddrStyle Then
				AddressStringArray = StrWithDelimitersToArray(AddressStringArray, Row.Value);
			Else
				Address = PartnInfoPrimeRo_CUIServerCall.ParseAddressMF(Address, Row.Value);
			EndIf;
			UpdateAddress = True;
		ElsIf Row.Name = "Judetul:" Then
			Judetul = Row.Value;
			If MyAddrStyle Then
				AddressStringArray.Add(Judetul);
			Else
				Address.Region = Judetul;
			EndIf;
			UpdateAddress = True;
		ElsIf Row.Name = "Codul postal:" Then
			Address.PostalCode = Row.Value;
			UpdateAddress = True;
		ElsIf Row.Name = "Telefon:" Or Row.Name = "Fax:" Then
			PhoneNumber = PartnInfoPrimeRo_CUIServerCall.ParsePhoneNumberRO(Row.Value);
			UpdatePhoneNumber = True;
		EndIf;
	EndDo;
	
	//////////////////////////////////////////////////////////////
	If MyAddrStyle Then
		For Ind = 0 To AddressStringArray.Count() - 1 Do
			Addr = AddressStringArray[Ind];
			
			If StrFind(Lower(Addr), Lower("MUN.")) > 0 OR StrFind(Lower(Addr), Lower("MUNI")) > 0 Then
				Address.City = Addr;
			ElsIf StrFind(Lower(Addr), Lower("STR")) > 0 OR
					StrFind(Lower(Addr), Lower("SOS")) > 0 OR
					StrFind(Lower(Addr), Lower("INT")) > 0 OR
					StrFind(Lower(Addr), Lower("ALE")) > 0 OR
					StrFind(Lower(Addr), Lower("BVD")) > 0 OR
					StrFind(Lower(Addr), Lower("BLD")) > 0 OR
					StrFind(Lower(Addr), Lower("BUL")) > 0  Then
				Address.Street = Addr;
			ElsIf StrFind(Lower(Addr), Lower("SEC")) > 0 Then
				Address.Region = Addr;
			Else
				AddrLine2 = AddrLine2 + ?(IsBlankString(AddrLine2), "", ", ") + Addr;
			EndIf;
		EndDo;
		
		If Address.Region = Undefined Then
			Address.Region = Judetul;
			AddrLine2 = Left(AddrLine2, StrLen(AddrLine2) - StrLen(Judetul) -2);
		EndIf;
		
		If Address.Street = Undefined Then
			Address.Street = AddrLine2;
		Else
			Address.Street = Address.Street + ?(IsBlankString(Address.Street), "", ", ") + AddrLine2;
		EndIf;
	EndIf;
	//////////////////////////////////////////////////////////////
	
	ContInfTypeAddress = PredefinedValue("Enum.ContactInformationTypes.Address");
	ContInfTypePhone   = PredefinedValue("Enum.ContactInformationTypes.Phone");
	
	If TypeOf(ThisForm.Object.Ref) = Type("CatalogRef.Counterparties")	Then
		
		ContInfKindLegalAddress = PredefinedValue("Catalog.ContactInformationKinds.CounterpartyLegalAddress");
		ContInfKindPhone = PredefinedValue("Catalog.ContactInformationKinds.CounterpartyPhone");
		
	ElsIf TypeOf(ThisForm.Object.Ref) = Type("CatalogRef.Companies") Then
		
		ContInfKindLegalAddress = PredefinedValue("Catalog.ContactInformationKinds.CompanyLegalAddress");
		ContInfKindPhone = PredefinedValue("Catalog.ContactInformationKinds.CompanyPhone");
		
	EndIf;
	
	If UpdateAddress Then
		UpdateContactInfo(ThisForm, Address, ContInfKindLegalAddress, ContInfTypeAddress);
	EndIf;
	
	If UpdatePhoneNumber Then
		UpdateContactInfo(ThisForm, PhoneNumber, ContInfKindPhone, ContInfTypePhone);
	EndIf;
	
EndProcedure

&AtClient
Function UpdateContactInfo(ThisForm, SourceData, InfoKind, InfoType)
	
	Filter = New Structure("Kind", InfoKind);
	Rows = ThisForm.ContactInformationAdditionalAttributeDetails.FindRows(Filter);
	If Rows.Count() = 0 Then
		RaiseMessage = NStr("en = 'Object does not have a contact information type field %1. Please add it manually.';
							|ru = 'Object does not have a contact information type field %1. Please add it manually.';
							|ro = 'Object does not have a contact information type field %1. Please add it manually.'");
		RaiseMessage = StrReplace(RaiseMessage, "%1", InfoKind);
		Raise RaiseMessage;
	Else
		RowAddress = Rows[0];
	EndIf;
	
	SourceData.Insert("ContactInformationKind", InfoKind);
	
	ContInfValues = PartnInfoPrimeRo_CUIServerCall.ContactInformationByAttributeValues(SourceData, InfoType);
	
	FillPropertyValues(RowAddress, ContInfValues);
	
	ThisForm[RowAddress.AttributeName] = RowAddress.Presentation;
	
EndFunction // UpdateContactInfo()

Function TransformStringInDataTVA(StringDate)
	
	If StringDate <> "NU" Then
		
		Str = TrimAll(StrReplace(StringDate, "-", ""));
		
		YearPositionIsLeft = Left(Str, 2);
		YearPositionIsRight = Mid(Str, 5, 2);
		If YearPositionIsLeft = "19" Or YearPositionIsLeft = "20" Then
			YearOfDate = Left(Str, 4);
			MonthOfDate = Mid(Str, 5, 2);
			DayOdDate = Right(Str, 2);
		ElsIf YearPositionIsRight = "19" Or YearPositionIsRight = "20" Then
			YearOfDate = Right(Str, 4);
			MonthOfDate = Mid(Str, 3, 2);
			DayOdDate = Left(Str, 2);
		Else
			//  SOMETHING  WENT  WRONG !!!
		EndIf;
		
		DateResult = Date(YearOfDate, MonthOfDate, DayOdDate);
	Else
		Message = New UserMessage;
		Message.Text = NStr("en = 'Partenerul nu este înregistrat ca plătitor de TVA!';
							|ru = 'Partenerul nu este înregistrat ca plătitor de TVA!';
							|ro = 'Partenerul nu este înregistrat ca plătitor de TVA!'");
		Message.Message();
		DateResult = Date(1, 1, 1);
	EndIf;
	
	Return DateResult;
EndFunction

Function TransformStringInData(StringDate)
	
	If Upper(StringDate) = "NU" Then
		//Message = New UserMessage;
		//Message.Text = "Partenerul nu este înregistrat ca plătitor de TVA!";
		//Message.Message();
		DateResult = Date(1, 1, 1);
	Else
		StringDate = TrimAll(StrReplace(StrReplace(StringDate, " ", ""), "-", ""));
		
		YearOfDate = Right(StringDate, 4);
		DayOdDate = Left(StringDate, 2);
				
		MonthAndDayOfDate = StrReplace(StringDate, YearOfDate, "");
		MonthOfDate = Upper(StrReplace(MonthAndDayOfDate, DayOdDate, ""));
		
		If Find(MonthOfDate, "IANUARIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "IANUARIE", "01");
		ElsIf Find(MonthOfDate, "FEBRUARIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "FEBRUARIE", "02");
		ElsIf Find(MonthOfDate, "MARTIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "MARTIE", "03");
		ElsIf Find(MonthOfDate, "APRILIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "APRILIE", "04");
		ElsIf Find(MonthOfDate, "MAI") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "MAI", "05");
		ElsIf Find(MonthOfDate, "IUNIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "IUNIE", "06");
		ElsIf Find(MonthOfDate, "IULIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "IULIE", "07");
		ElsIf Find(MonthOfDate, "AUGUST") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "AUGUST", "08");
		ElsIf Find(MonthOfDate, "SEPTEMBRIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "SEPTEMBRIE", "09");
		ElsIf Find(MonthOfDate, "OCTOMBRIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "OCTOMBRIE", "10");
		ElsIf Find(MonthOfDate, "NOIEMBRIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "NOIEMBRIE", "11");
		ElsIf Find(MonthOfDate, "DECEMBRIE") > 0 Then
			MonthOfDate = StrReplace(MonthOfDate, "DECEMBRIE", "12");
		EndIf;
		
		DateResult = Date(YearOfDate, MonthOfDate, DayOdDate);
	
	EndIf;
	
	Return DateResult;
	
EndFunction

Function IsEmptyValue(Value)
	
	Result   = False;
	ValueType = TypeOf(Value);
	
	If ValueType = Type("Date") Then
		
		If Value = Date('00010101') Then
			Result = True;
		EndIf;
		
	EndIf;
	
	Return Result;
	
EndFunction

 #EndRegion

#Region NeedToTranslate

//&НаКлиентеНаСервереБезКонтекста
//Функция МассивВСтрокуСРазделителями(ИсходныйМассив, Разделитель = ",")
//	
//	СтрокаСРазделителями = "";
//	Для Каждого ЭлементМассива Из ИсходныйМассив Цикл
//		СтрокаСРазделителями = СтрокаСРазделителями + ?(ПустаяСтрока(СтрокаСРазделителями), "", Разделитель) + ЭлементМассива;
//	КонецЦикла;
//	
//	Возврат СокрЛП(СтрокаСРазделителями);
//	
//EndFunction // ()

Function StrWithDelimitersToArray(ArrayWithStrings, Val Str, Delimiter = ",")
 
    Strings = StrReplace(Str, Delimiter, Chars.LF);
	
	For IND = 1 To StrLineCount(Strings) Do                    				// СтрЧислоСтрок
		ArrayWithStrings.Add(TrimALL(StrGetLine(Strings, IND)));            // СтрПолучитьСтроку
	EndDo;
	
	If  IsBlankString(Strings) Then
	    ArrayWithStrings.Add(" ");
	EndIf;
	
	Return ArrayWithStrings;
	
EndFunction // (StrWithDelimitersToArray)

//IDL{
&AtClient
Procedure GetCompanyInfoByTIN_Full(Val TIN, Form) Export

	TIN = CIFFromCUI(TIN);
	
	Host = "mfinante.gov.ro";
    App_request = "/apps/infocodfiscal.html";
    
    Cookie_Site = "";
    Cookie_Host = "";
    
    SecureConnection = New OpenSSLSecureConnection;
    HTTPConnection = New HTTPConnection(Host,,,,,,SecureConnection);
    Request = New HTTPRequest(Host);
    
    Response = HTTPConnection.Get(Request);
    
    If Not Response.StatusCode = 200 AND Not Response.StatusCode = 404 Then
		//ShowMessageBox(,StrTemplate(NStr("en = 'Error code - %1'"),Response.StatusCode),5,NStr("en = 'Error'"));
        ShowMessageBox(, ErrorMessageText(Response.StatusCode), 5, ErrorMessageHead());
        Return
    EndIf;
    
    SetCookies = Response.Headers.Get("Set-Cookie");
    Cookies = StrSplit(SetCookies,",");
    
    For Each ArrayElement In Cookies Do
        
        If StrStartsWith(TrimAll(ArrayElement), "JSESSIONID") Then
            
            CookieContent = StrSplit(ArrayElement,";");
            
            Cookie_Site = StrReplace(CookieContent[0],"""","");
            Cookie_Site = StrReplace(Cookie_Site,"JSESSIONID","jsessionid");
        EndIf;
    EndDo;
    
    HTTPConnection = New HTTPConnection(Host,,,,,,SecureConnection);
    Request = New HTTPRequest(App_request);
    
    Response = HTTPConnection.Get(Request);
    
    If Not Response.StatusCode = 200 Then
		//ShowMessageBox(,StrTemplate(NStr("en = 'Error code - %1'"),Response.StatusCode),5,NStr("en = 'Error'"));
        ShowMessageBox(, ErrorMessageText(Response.StatusCode), 5, ErrorMessageHead());
        Return
    EndIf;
    
    SetCookies = Response.Headers.Get("Set-Cookie");
    Cookies = StrSplit(SetCookies,",");
    Cookies_Array = New Array;
    
    For Each ArrayElement In Cookies Do
        
        If StrStartsWith(TrimAll(ArrayElement), "JSESSIONID") Then
            
            CookieContent = StrSplit(ArrayElement,";");
            Cookies_Array.Add(CookieContent[0]);
            Cookie_Host = StrReplace(CookieContent[0],"""","");
            Cookie_Host = StrReplace(Cookie_Host,"JSESSIONID","jsessionid");
        Else
            Current_Cookie = StrSplit(ArrayElement,";");
            Cookies_Array.Add(TrimAll(Current_Cookie[0]));
        EndIf;
    EndDo;
    
    Cookies_Array.Add(Cookie_Site);
    Cookies_Array.Add("GUEST_LANGUAGE_ID=Ro_RO");
    Cookies_Array.Add("COOKIE_SUPPORT=true");
    
    Cookie_All = StrConcat(Cookies_Array,",");
    
    Notify = New NotifyDescription("GetInfoByKaptcha", ThisObject, New Structure("Form,Cookie,TIN,Cookie_Host",Form,Cookie_All,TIN,Cookie_Host));
    
	OpenForm("CommonForm.PartnInfoPrimeRo_KaptchaForm",
	        New Structure("Cookie",Cookie_All),
	        Form, Form.UUID, , , Notify,
	        FormWindowOpeningMode.LockOwnerWindow);

EndProcedure
        
&AtClient
Procedure GetInfoByKaptcha(ClosureResult,AdditionalParameters) Export
    
    If  ClosureResult = Undefined OR IsBlankString(ClosureResult) Then
        Return
    EndIf;
    
    Host = "mfinante.gov.ro";
    App_request = "/apps/infocodfiscal.html";
    
    SecureConnection = New OpenSSLSecureConnection;
    HTTPConnection = New HTTPConnection(Host,,,,,,SecureConnection);
    
    Headers = New Map;
    Headers.Insert("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
    Headers.Insert("Content-Type","application/x-www-form-urlencoded");
    Headers.Insert("Connection","keep-alive");
    Headers.Insert("Host","mfinante.gov.ro");
    Headers.Insert("Referer","https://mfinante.gov.ro/apps/infocodfiscal.html");
    Headers.Insert("Cookie",AdditionalParameters.Cookie);
    
    Request = New HTTPRequest(App_request+";"+AdditionalParameters.Cookie_Host,Headers);
    Request.SetBodyFromString(StrTemplate("pagina=domenii&cod=%1&captcha=%2&B1=VIZUALIZARE",AdditionalParameters.TIN,ClosureResult));
	Response = HTTPConnection.Post(Request);
    
    If Not Response.StatusCode = 200 Then
		//ShowMessageBox(,StrTemplate(NStr("en = 'Error code - %1'"),Response.StatusCode),5,NStr("en = 'Error'"));
        ShowMessageBox(, ErrorMessageText(Response.StatusCode), 5, ErrorMessageHead());
        Return
    EndIf;
    
    HTMLReader = New HTMLReader;
	HTMLReader.SetString(Response.GetBodyAsString());
	
    DOMBuilder = New DOMBuilder;
    HTMLDoc = DOMBuilder.Read(HTMLReader);
    
    HTMLReader.Close();
    
    Query = "{
    |""type"": ""hasattribute"",
    |""value"": {
    |  ""value"": ""row"",
    |  ""operation"": ""valuecontains""
    |}
    |}";
    
    QueryResult = HTMLDoc.FindByFilter(Query);
    
    If QueryResult.Count() < 25 Then
    	ShowMessageBox(,NStr("en = 'Wrong TIN number or wrong validation code';
							 |ru = 'Wrong TIN number or wrong validation code';
							 |ro = 'Wrong TIN number or wrong validation code'"), 5, ErrorMessageHead());
        Return
    EndIf;
    
    ArrayResult = New Array;
    
    For Each ArrayElrment In QueryResult Do
        
        ContentValue = New Structure("Name,Value","","");
        
        For Each ChildNode In ArrayElrment.ChildNodes Do
            
            TextContent = TrimAll(StrReplace(ChildNode.TextContent,Chars.CR+Chars.LF,""));
            TextContent = StrReplace(TextContent,Chars.NBSp,"");
            
            If IsBlankString(TextContent) Then
                Continue;
            EndIf;
            
            strArray = StrSplit(TrimAll(TextContent),Chars.Tab,False);
            
            i = 0;
            
            While i <= strArray.UBound() Do
                
                CurrentValue = TrimAll(strArray[i]);
                
                If IsBlankString(CurrentValue) Then
                    strArray.Delete(i)
                else
                    strArray[i] = CurrentValue
                EndIf;
                
                i = i + 1
            EndDo;
            
            ContentValue[?(IsBlankString(ContentValue.Name),"Name","Value")] = StrConcat(strArray," ");
        EndDo;
        
        ArrayResult.Add(ContentValue);
    EndDo;
    
    //Постобработка массива    
    
    Params = New Structure("CompanyInfo",ArrayResult);
    
    Notify = New NotifyDescription("PartnInfoPrimeRo_TINOpeningBeforeEnd", AdditionalParameters.Form);
    
    OpenForm("CommonForm.PartenerGetInfoForm",Params,,,,,Notify,FormWindowOpeningMode.LockOwnerWindow);
    
EndProcedure

&AtClient
Function GetCompanyInfoByTIN_ANAF(TIN) Export
    
    Host = "webservicesp.anaf.ro";
    App = "/PlatitorTvaRest/api/v4/ws/tva";
    SecureConnection = New OpenSSLSecureConnection;
    HTTPConnection = New HTTPConnection(Host,,,,,,SecureConnection);
    
    Headers = New Map;
    Headers.Insert("Content-Type","application/json");
    
    Request = New HTTPRequest;
    Request.ResourceAddress = App;
    Request.Headers = Headers;
    
    Query = New JSONWriter;
    Query.SetString();
    
    Query.WriteStartArray();
    Query.WriteStartObject();
    Query.WritePropertyName("cui");
    Query.WriteValue(Number(TIN));
    Query.WritePropertyName("data");
    Query.WriteValue(Format(CurrentDate(),"DF=yyyy-MM-dd"));
    Query.WriteEndObject();
    Query.WriteEndArray();
    
    QueryText = Query.Close();
    
	Request.SetBodyFromString(QueryText);
    Answer = HTTPConnection.Post(Request);
    Result = Answer.GetBodyAsString();
    
    If Answer.StatusCode = 200 Then
        
        ResultReader = New JSONReader;
        ResultReader.SetString(Result);
        
        DateFilds = New Array;
        
        DateFilds.Add("data");
        DateFilds.Add("data_inceput_ScpTVA");
        DateFilds.Add("data_sfarsit_ScpTVA");
        DateFilds.Add("data_anul_imp_ScpTVA");
        DateFilds.Add("dataInceputTvaInc");
        DateFilds.Add("dataSfarsitTvaInc");
        DateFilds.Add("dataActualizareTvaInc");
        DateFilds.Add("dataPublicareTvaInc");
        DateFilds.Add("dataInactivare");
        DateFilds.Add("dataReactivare");
        DateFilds.Add("dataPublicare");
        DateFilds.Add("dataRadiere");
        DateFilds.Add("dataInceputSplitTVA");
        DateFilds.Add("dataAnulareSplitTVA");
        
        ResultStructure = ReadJSON(ResultReader,,DateFilds,,"DateRestore",ThisObject,,DateFilds);
    EndIf;

EndFunction

Function DateRestore(Property,Value,AdditionalParameters) Export
    Return ?(IsBlankString(Value), '00010101', XMLValue(Type("Date"), Value))
EndFunction // ()
//}IDL

//	SAM  {
Function ErrorMessageText(ResponseStatusCode)
	Return StrTemplate(NStr("en = 'Error code - %1'; ru = 'Код ошибки - %1'; ro = 'Codul erorii - %1'"), ResponseStatusCode);
EndFunction // ErrorMessageText()

Function ErrorMessageHead()
	Return NStr("en = 'Error'; ru = 'Ошибка'; ro = 'Eroare'");
EndFunction // ErrorMessageText()

Function CIFFromCUI(CUI)
	CIF = CUI;
	If Upper(Left(TrimAll(CIF), 1)) = "R" Then
		CIF = Right(CIF, StrLen(TrimAll(CIF)) - 1);
	EndIf;
	If Upper(Left(TrimAll(CIF), 1)) = "O" Then
		CIF = Right(CIF, StrLen(TrimAll(CIF)) - 1);
	EndIf;
	
	Return TrimAll(CIF);
EndFunction
//	}  SAM

#EndRegion
