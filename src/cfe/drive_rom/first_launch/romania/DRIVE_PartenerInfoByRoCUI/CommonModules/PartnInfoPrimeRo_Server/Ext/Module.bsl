﻿
Function InsertLegalForm(ShortName)Export
		
	Query = New Query;
	Query.Text =
		"SELECT
		|	LegalForms.Ref AS Ref,
		|	LegalForms.ShortName AS ShortName
		|FROM
		|	Catalog.LegalForms AS LegalForms";
	
	QueryResult = Query.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	While SelectionDetailRecords.Next() Do
		
		NormalizedShortName = Normalisation(SelectionDetailRecords.ShortName);
		if (ShortName = NormalizedShortName)  then
			return SelectionDetailRecords.Ref
		EndIF;
		
	 EndDo;
				
EndFunction

Function InsertRegistrationCountry()Export
	
	Query = New Query;
	Query.Text =
	"SELECT
	|	WorldCountries.Description AS Description,
	|	WorldCountries.Ref AS Ref
	|FROM
	|	Catalog.WorldCountries AS WorldCountries
	|WHERE
	|	WorldCountries.Code = &Code";
	
	Query.SetParameter("Code", "642");
	QueryResult = Query.Execute();
	
	If NOT QueryResult.IsEmpty() Then
		
		Selection = QueryResult.Select();
		
		While Selection.Next() Do
			
			//LegalForm0 = Selection.Description;
			Description = Selection.Ref;
			
		EndDo;
		
	EndIf;
	
	Return Description
		
EndFunction

Function Normalisation(ShortName)
	
	ShortDescriptionWithoutDots = StrReplace(ShortName, ".", "");
	ResultShortDescription = Upper(StrReplace(ShortDescriptionWithoutDots, " ", ""));
		
	Return ResultShortDescription;
	
EndFunction
