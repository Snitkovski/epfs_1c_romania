﻿#Region FormHeaderItemsEventHandlers

&AtClient
Procedure PartnInfoPrimeRo_TINOpeningAround(Item, StandardProcessing)
	
	//IDL{
	StandardProcessing = False;
	Object.TIN = TrimAll(Item.EditText);
	PartnInfoPrimeRo_CUIClient.GetCompanyInfoByTIN_Full(Object.TIN,ThisObject);
	//}IDL
	
	//PartnInfoPrimeRo_CUIClient.GetCompanyInfoByTIN(Object.TIN);
	//	
	//StandardProcessing = False;
	//Params = New Structure;
	//Object.TIN = TrimAll(Item.EditText);
	//Params.Insert("TIN", Object.TIN);   	
	//
	//OpenForm("CommonForm.PartenerGetInfoForm", Params,,,,, New NotifyDescription("PartnInfoPrimeRo_TINOpeningBeforeEnd", ThisForm), FormWindowOpeningMode.LockWholeInterface);	
	
EndProcedure

&AtClient
Procedure PartnInfoPrimeRo_MFinanteAround(Item, StandardProcessing)
	
	StandardProcessing = False;
	Params = New Structure;
	Object.TIN = TrimAll(Items.TIN.EditText);
	Params.Insert("TIN", Object.TIN);
	
	OpenForm("CommonForm.PartenerGetInfoForm2", Params,,,,,New NotifyDescription("PartnInfoPrimeRo_TINOpeningBeforeEnd", ThisForm), FormWindowOpeningMode.LockWholeInterface);
	
EndProcedure

&AtClient
Procedure PartnInfoPrimeRo_TINOpeningBeforeEnd(Result, AdditionalParameters) Export
	
	If Result = Undefined OR Result.Count() = 0 Then
		Return;
	EndIf;
			
	Modified = True;
	PartnInfoPrimeRo_CUIClient.WriteInfoContact(ThisForm, Object, Result);
	
EndProcedure

#EndRegion
