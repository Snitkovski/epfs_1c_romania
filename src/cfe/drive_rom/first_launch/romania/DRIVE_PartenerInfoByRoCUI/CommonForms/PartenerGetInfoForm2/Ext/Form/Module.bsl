﻿&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
	
	valKPP = TrimAll(Parameters.TIN);
	
EndProcedure

&AtClient
Procedure OnOpen(Cancel)
	
	If Not VerificareCUI(valKPP) Then
		Cancel = True;
		ShowMessageBox(New NotifyDescription("OnOpenEnd", ThisForm),
					NStr("ro='Ați introdus CUI incorect!';
						  |en='Invalid Partner code!';
						  |ru='Неверный уникальный код фирмы!'"));
		Return;
	EndIf;
	
	selCUI = CIFFromCUI(valKPP);
	
	HTMLDocumentField = "https://www.mfinante.gov.ro/agenticod.html?pagina=domenii&cod=" + selCUI; 	
		
EndProcedure

&AtClient
Procedure OnOpenEnd(AdditionalParameters) Export
EndProcedure

&AtClient
Procedure MfinanteDocumentComplete(Item)

	Url = Items.HTMLDocumentField.Document.URL;

	If Find(Url, "https://www.mfinante.gov.ro/infocodfiscal.html") < 1  Then  				
		Return;
	EndIf;

	nodesTABLE = Items.HTMLDocumentField.Document.getElementsByTagName("TABLE");
	infoNodeTABLE = Undefined;
	
	For Each nodeTABLE In nodesTABLE Do
		info = TrimAll(nodeTABLE.TextContent);
		
		If Find(info, "Denumire platitor:") > 0 Then
			infoNodeTABLE = nodeTABLE;
			Break;
		EndIf;
	EndDo;
	
	If infoNodeTABLE <> Undefined Then
		
		vl	= New ValueList;
		vl.Add("Denumire platitor:");
		vl.Add("Adresa:");
		vl.Add("Judetul:");
		vl.Add("Numar de inmatriculare la Registrul Comertului:");
		vl.Add("Codul postal:");
		vl.Add("Telefon:");
		vl.Add("Fax:");
		vl.Add("Taxa pe valoarea adaugata (data luarii in evidenta):");
		vl.Add("Stare societate:");
		
		nodesTR = infoNodeTABLE.getElementsByTagName("TR");
		For Each nodeTR In nodesTR Do
			nodesTD	= nodeTR.getElementsByTagName("TD");
			col		= 1;
			For Each nodeTD In nodesTD Do
				If col = 1 Then
					strDesc		= HtmlTextNormalize(nodeTD.innerText);
					col			= col+1;
				ElsIf col = 2 Then
					strValue	= HtmlTextNormalize(nodeTD.innerText);
					Break;
				EndIf;
			EndDo;
			If vl.FindByValue(strDesc) <> Undefined Then // este una din informatiile cautate
				If strValue = "-" Then strValue = ""; EndIf;
				
				row				= TableData.Add();
				row.Name		= strDesc;
				row.Value		= strValue;
				row.ToUpdate	= (strValue <> "");
				
			EndIf;
		EndDo;
	EndIf;
	
EndProcedure

&AtClient
Function HtmlTextNormalize(text)
	str = TrimAll(text);
	str = StrReplace(str, Chars.CR,   " ");
	str = StrReplace(str, Chars.LF,   " ");
	str = StrReplace(str, Chars.Tab,  " ");
	str = StrReplace(str, Chars.NBSp, " ");
	
	// collapse spaces
	str1 = StrReplace(str, "  ", " "); // ex. 4 spaces become 2 spaces, so we need to replace over and over in a loop
	While StrLen(str1) < StrLen(str) Do
		str		= str1;
		str1	= StrReplace(str, "  ", " ");
	EndDo;
	return str;
EndFunction	//  HtmlTextNormalize()            

&AtClient
Function VerificareCUI(CUI)
	
	IsGood = False;
	
	CIF = CIFFromCUI(CUI);
	
	Try
		VerCIF = Number(TrimAll(CIF));
	Except
		Return IsGood;
	EndTry;
	
	CIF = TrimAll(CIF);
	For N = 1 To StrLen(CIF) - 1 Do
		InvCIF = Mid(CIF, N, 1) + InvCIF;
	EndDo;
	
	// "753217532";
	CodVer = "235712357";
	
	Try
		TestNum = Number(InvCIF);
	Except
		Return IsGood;
	EndTry;
	
	NumSum = 0;
	Try
		For N = 1 To StrLen(InvCIF) Do
			NumSum = NumSum + Number(Mid(InvCIF, N, 1)) * Number(Mid(CodVer, N, 1));
		EndDo;
	Except
		Return IsGood;
	EndTry;
	
	VerNum = NumSum * 10 - Int((NumSum * 10) / 11) * 11;
	
	Try
		IsGood = (?(VerNum = 10, 0, VerNum) = Number(Right(CIF, 1)));
	Except
		Return False;
	EndTry;
	
	Return IsGood;
	
EndFunction

&AtClient
Function CIFFromCUI(CUI)
	CIF = CUI;
	If Upper(Left(TrimAll(CIF), 1)) = "R" Then
		CIF = Right(CIF, StrLen(TrimAll(CIF)) - 1);
	EndIf;
	If Upper(Left(TrimAll(CIF), 1)) = "O" Then
		CIF = Right(CIF, StrLen(TrimAll(CIF)) - 1);
	EndIf;
	
	Return TrimAll(CIF);
EndFunction

&AtClient
Procedure FormGetInfo(Command)
	Close(TableData);
EndProcedure

