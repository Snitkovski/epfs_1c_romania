﻿
&AtClient
Procedure CommandOK(Command)
    Close(Kaptcha_Str);
EndProcedure

&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
    Cookie = Parameters.Cookie;
EndProcedure

&AtClient
Procedure OnOpen(Cancel)
    
    Host = "mfinante.gov.ro";
    App_kaptcha = "/apps/kaptcha.jpg";
    
    SecureConnection = New OpenSSLSecureConnection;
    HTTPConnection = New HTTPConnection(Host,,,,,,SecureConnection);
    
    Headers = New Map;
    Headers.Insert("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
    Headers.Insert("Content-Type","application/x-www-form-urlencoded");
    Headers.Insert("Connection","keep-alive");
    Headers.Insert("Host","mfinante.gov.ro");
    Headers.Insert("Referer","https://mfinante.gov.ro/apps/infocodfiscal.html");
    Headers.Insert("Cookie",Cookie);
    
    Request = New HTTPRequest(App_kaptcha,Headers);
    
    Response = HTTPConnection.Get(Request);
    
    Kaptcha_Pic = PutToTempStorage(Response.GetBodyAsBinaryData(), UUID);
    
EndProcedure
