﻿
&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
    
    //IDL{
    If Parameters.Property("CompanyInfo") And TypeOf(Parameters.CompanyInfo) = Type("Array") Then
        
        For Each ArrayElement In Parameters.CompanyInfo Do
			NewRow = TableInfo.Add();
            FillPropertyValues(NewRow, ArrayElement);
			NewRow.ToUpdate = True;
        EndDo;
    Else
        valKPP = TrimAll(Parameters.TIN);
    EndIf;
    //}IDL
	
EndProcedure

&AtClient
Procedure OnOpen(Cancel)
    
    //IDL{
    If TableInfo.Count() > 0 Then
        Return;
    EndIf;
    //}IDL
	
	If Not VerificareCUI(valKPP) Then
		Cancel = True;
		ShowMessageBox(New NotifyDescription("OnOpenEnd", ThisForm),
					NStr("ro='Ați introdus CUI incorect!';
						  |en='Invalid Partner code!';
						  |ru='Неверный уникальный код фирмы!'"));
		Return;
	EndIf;
	
	//selCUI = CIFFromCUI(valKPP);
	//Data = GetPartenerInfo2015(selCUI, TableInfo);

	//If Data <> Undefined Then
	//	//LoadData(Data);
	//Else
	//	Cancel = True;
	//EndIf;
	
EndProcedure

&AtClient
Procedure OnOpenEnd(AdditionalParameters) Export
EndProcedure

&AtClient
Function VerificareCUI(CUI)
	
	IsGood = False;
	CIF = CIFFromCUI(CUI);
	
	Try
		VerCIF = Number(TrimAll(CIF));
	Except
		Return IsGood;
	EndTry;
	
	CIF = TrimAll(CIF);
	For N = 1 To StrLen(CIF) - 1 Do
		InvCIF = Mid(CIF, N, 1) + InvCIF;
	EndDo;
	
	// "753217532";
	CodVer = "235712357";
	
	Try
		TestNum = Number(InvCIF);
	Except
		Return IsGood;
	EndTry;
	
	NumSum = 0;
	Try
		For N = 1 To StrLen(InvCIF) Do
			NumSum = NumSum + Number(Mid(InvCIF, N, 1)) * Number(Mid(CodVer, N, 1));
		EndDo;
	Except
		Return IsGood;
	EndTry;
	
	VerNum = NumSum * 10 - Int((NumSum * 10) / 11) * 11;
	
	Try
		IsGood = (?(VerNum = 10, 0, VerNum) = Number(Right(CIF, 1)));
	Except
		Return False;
	EndTry;
	
	Return IsGood;
EndFunction

&AtClient
Function CIFFromCUI(CUI)
	CIF = CUI;
	If Upper(Left(TrimAll(CIF), 1)) = "R" Then
		CIF = Right(CIF, StrLen(TrimAll(CIF)) - 1);
	EndIf;
	If Upper(Left(TrimAll(CIF), 1)) = "O" Then
		CIF = Right(CIF, StrLen(TrimAll(CIF)) - 1);
	EndIf;
	
	Return TrimAll(CIF);
EndFunction

&AtClient
Function BuildDOM(xml)
	
	XMLReader = New XMLReader;
	XMLReader.SetString(xml);
	DOMBuilder = New DOMBuilder;
	Return DOMBuilder.Read(XMLReader);

EndFunction

&AtClient
Function GetFirstElement_TextContent(DOMDocument, TagName)
	
	list = DOMDocument.GetElementByTagName(TagName);
	
	If list.Count() = 0 Then
		Return Undefined;
	EndIf;
	
	Return list[0].TextContent;
EndFunction

////////////////////////////////////////////////////////////
//  Import data from DB SagaSoft - doesn't work sub LINUX
//
//&AtClient
//Function GetPartenerInfo2015(CIF, TableInput) Export
//
//	#If ThinClient Then
//
//	rindCUI = CIFFromCUI(CIF);
//	
//	dateTime = Format(CurrentDate(), "DF=dd.MM.yyyy%HHmm:ss");
//	ResourceAddress = "infoTert.php?flag=0&cod_fiscal=" + rindCUI + "&denumire=%C2%AE_comert=&adresa=&dt=" + dateTime;
//	
//	Headers = New Map;
//	Headers.Insert("Accept", "*/*");
//	Headers.Insert("Accept-Encoding", "deflate");
//	Headers.Insert("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
//	Headers.Insert("Host", "www.sagasoft.ro");
//	Headers.Insert("Connection", "Keep-Alive");
//	
//	Request = New HTTPRequest(ResourceAddress, Headers);
//	ssl1 = New OpenSSLSecureConnection(
//				    New WindowsClientCertificate(WindowsCertificateSelectMode.Auto),
//				    New WindowsCertificationAuthorityCertificates());
//	
//	Try
//		HTTPConnection = New HTTPConnection("www.sagasoft.ro",,,,,,ssl1,);
//    	Response = HTTPConnection.Get(Request);
//	Except
//					 
//		MFMess = NStr("ru='Страница ';en='Page ';ro='Pagina '")+
//				 NStr("ru=' не отвечает или нет доступа в Internet!';
//		         	  |en=' do not answer or have not Internet access!';
//				 	  |ro=' nu a răspuns sau nu aveti Internet access!'"); 
//		Message(MFMess);
//		
//		Return Undefined;
//	EndTry;
//
//	Content = Response.GetBodyAsString();
//	dom	= BuildDOM(Content);
//	
//	denumire = GetFirstElement_TextContent(dom, "denumire");
//	adresa = GetFirstElement_TextContent(dom, "adresa");
//	judet = GetFirstElement_TextContent(dom, "judet");
//	reg_comert = GetFirstElement_TextContent(dom, "reg_comert");
//	telefon = GetFirstElement_TextContent(dom, "telefon");
//	
//	is_tva = GetFirstElement_TextContent(dom, "is_tva");
//	If is_tva = "1" Then
//		data_tva = GetFirstElement_TextContent(dom, "data_tva");
//	Else
//		data_tva = "";
//	EndIf;
//		
//	vl	= New Map;
//	vl.Insert("Denumire platitor:", denumire);
//	vl.Insert("Adresa:", adresa);
//	vl.Insert("Judetul:", judet);
//	vl.Insert("Numar de inmatriculare la Registrul Comertului:", reg_comert);
//	vl.Insert("Telefon:", telefon);
//	vl.Insert("Taxa pe valoarea adaugata (data luarii in evidenta):", data_tva);
//	
//	For Each v In vl Do
//
//		rows = TableInput.FindRows(New Structure("Name", v.Key));
//		
//		If rows.Count() = 0 Then
//			row = TableInput.Add();
//			row.Name = v.Key;
//			row.Value = v.Value;
//			row.ToUpdate = (v.Value <> "");
//		Else
//			row = rows[0];
//			row.ToUpdate = (v.Value <> "" And v.Value <> row.Value);
//			row.Value = v.Value;
//		EndIf
//	EndDo;
//	
//	Return TableInput;
//	#EndIf
//	
//EndFunction  

&AtClient
Procedure FormGetInfo(Command)
	Close(TableInfo);
EndProcedure

&AtServer
Procedure LoadData(Data)
	TableInfo.Load(Data.Unload());
EndProcedure

