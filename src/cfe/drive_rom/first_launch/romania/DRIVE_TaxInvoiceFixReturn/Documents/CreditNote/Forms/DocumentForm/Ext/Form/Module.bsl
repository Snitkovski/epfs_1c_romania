﻿
&AtClient
Procedure FIX01_BasisDocumentClearingBefore(Item, StandardProcessing)
    
    SetReadOnlyProperty(Items.Inventory.ChildItems)
    
EndProcedure

&AtClient
Procedure SetReadOnlyProperty(pItems)
    
    For Each fItem In pItems Do
        fItem.ReadOnly = False;
        If TypeOf(fItem) = Type("FormGroup") Then
            SetReadOnlyProperty(fItem.ChildItems)
        EndIf;
    EndDo;
    
EndProcedure

&AtServer
Procedure FIX01_OnCreateAtServerAfter(Cancel, StandardProcessing)
    
    Items.BasisDocument.ClearButton = True;

EndProcedure

&AtClient
Procedure FIX01_InventoryPriceOnChangeAfter(Item)
    
    Items.Inventory.CurrentData.Price = Items.Inventory.CurrentData.InitialPrice
    
EndProcedure

