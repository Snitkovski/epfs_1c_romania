﻿
&After("FillCheckProcessing")
Procedure FIX01_FillCheckProcessing(Cancel, CheckedAttributes)
    
    If ThisObject.OperationKind = Enums.OperationTypesCreditNote.SalesReturn Then
        IndexOfAtr = CheckedAttributes.Find("BasisDocument");
        If Not IndexOfAtr = Undefined Then
            CheckedAttributes.Delete(IndexOfAtr)
        EndIf;
    EndIf;
    
EndProcedure

&Around("CheckReturnedQuantity")
Procedure FIX01_CheckReturnedQuantity(Cancel)
    
    If Not ValueIsFilled(ThisObject.BasisDocument) Then
    	Return
    EndIf;
    
	Query = New Query;
	Query.Text =
	"SELECT ALLOWED
	|	Inventory.Products AS Products,
	|	Inventory.Characteristic AS Characteristic,
	|	Inventory.Quantity AS Quantity,
	|	Inventory.Batch AS Batch,
	|	Inventory.Recorder AS SalesInvoice
	|INTO ReturnBalance
	|FROM
	|	AccumulationRegister.Inventory AS Inventory
	|WHERE
	|	Inventory.Recorder IN(&InvoicesArray)
	|
	|UNION ALL
	|
	|SELECT
	|	Inventory.Products,
	|	Inventory.Characteristic,
	|	-Inventory.Quantity,
	|	Inventory.Batch,
	|	Inventory.SourceDocument
	|FROM
	|	AccumulationRegister.Inventory AS Inventory
	|WHERE
	|	Inventory.SourceDocument IN(&InvoicesArray)
	|	AND Inventory.Return
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ReturnBalance.Products AS Products,
	|	ReturnBalance.Characteristic AS Characteristic,
	|	SUM(ReturnBalance.Quantity) AS Quantity,
	|	ReturnBalance.Batch AS Batch,
	|	ReturnBalance.SalesInvoice AS SalesInvoice
	|INTO GroupedBalance
	|FROM
	|	ReturnBalance AS ReturnBalance
	|
	|GROUP BY
	|	ReturnBalance.Characteristic,
	|	ReturnBalance.Batch,
	|	ReturnBalance.SalesInvoice,
	|	ReturnBalance.Products
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	GroupedBalance.Products AS Products,
	|	GroupedBalance.Characteristic AS Characteristic,
	|	GroupedBalance.Quantity AS ReturnedQuantity,
	|	GroupedBalance.Batch AS Batch,
	|	GroupedBalance.SalesInvoice AS SalesInvoice
	|FROM
	|	GroupedBalance AS GroupedBalance
	|WHERE
	|	GroupedBalance.Quantity < 0";

	Query.SetParameter("InvoicesArray",	ThisObject.Inventory.UnloadColumn("SalesDocument"));
	Query.SetParameter("Ref",			ThisObject.Ref);
	
	QueryResult = Query.Execute();
	Selection = QueryResult.Select();
	
	While Selection.Next() Do
		If Selection.ReturnedQuantity > 0 Then
			MessageToUser = StringFunctionsClientServer.SubstituteParametersToString(
				NStr("pl='%1 %2 z %3 zostało wcześniej zwrócone na mocy innych dokumentów noty kredytowej.
				     |Do zwrócenia pozostało ci tylko %4 %2. Szczegóły znajdziesz w strukturze podporządkowania początkowej faktury sprzedaży.';
				     |ro='%1 %2 din %3 a fost returnat anterior de alte documente de notă de credit.
				     |n Ați plecat doar să%4 %2 vă întoarceți. Vedeți structura subordonată a facturii inițiale de vânzare pentru detalii.';
				     |en='%1 %2 of %3 was previously returned by other Credit note documents.
				     |You have only %4 %2 left to return. See subordinate structure of initial Sales invoice for details.';
				     |it='%1 %2 di %3 in precedenza è stato restituito da altri documenti nota di Credito.
				     |Avete solo %4 %2 rimaneti da restituire. Controllate la struttura subordinata della Fattura di vendita iniziale per i dettagli.';
				     |DE='%1 %2 von %3 wurde zuvor von anderen Gutschrift-Belegen zurückgegeben.
				     |Sie müssen nur noch %4 %2 zurückerstatten. Weitere Informationen finden Sie unter der untergeordneten Struktur der ursprünglichen Verkaufsrechnung.';
				     |es_ES='%1 %2 de %3 se ha devuelto previamente por otros documentos de la nota de Crédito.
				     |Usted tiene solo %4 %2 restante para devolver. Ver la estructura subordinada de la factura de Ventas inicial para detalles.';
				     |ru='%1 %2 %3 уже было возвращено другими документами Кредитовое авизо.
				     |Доступно для возврата %4 %2. Подробности см. в структуре подчиненности исходной Расходной накладной.'"),
				Selection.ReturnedQuantity,
				Selection.MeasurementUnit,
				String(Selection.Products) + ?(ValueIsFilled(Selection.Characteristic), ", " + Selection.Characteristic, "")
				 + ?(ValueIsFilled(Selection.Batch), ", " + Selection.Batch, ""),
				Selection.AvailableQuantity,);
		Else
			MessageToUser = StringFunctionsClientServer.SubstituteParametersToString(
				NStr("ro='Există mai multă cantitate de bunuri  ""%1"" returnate decât în factura inițială de vânzare.';
				     |DE='Es gibt eine größere Menge an zurückgegebenen Waren ""%1"" als in der ursprünglichen Verkaufsrechnung.';
				     |ru='По исходной Расходной накладной превышено количество возвращаемого товара ""%1"".';
				     |en='There is more quantity of returned goods ""%1"" than in initial Sales Invoice.';
				     |it='C''è una quantità maggiore di beni restituiti ""%1"" rispetto a quelli nella fattura di vendita iniziale.';
				     |es_ES='Hay más cantidad de mercancías devueltas ""%1"" que en la Factura de Ventas inicial.';
				     |pl='Ilość zwróconych towarów ""%1"" jest większa niż w początkowej fakturze sprzedaży.'"),
				String(Selection.Products) + ?(ValueIsFilled(Selection.Characteristic), ", " + Selection.Characteristic, "")
				 + ?(ValueIsFilled(Selection.Batch), ", " + Selection.Batch, ""));
		EndIf;
			
		CommonClientServer.MessageToUser(MessageToUser,,,,Cancel);
	EndDo;
    
EndProcedure
