﻿
&After("GenerateTableSales")
Procedure FIX01_GenerateTableSales(DocumentRefCreditNote, StructureAdditionalProperties)
    
    TableSales = StructureAdditionalProperties.TableForRegisterRecords.TableSales;
    
    i=0;
    While i<TableSales.Count() Do
        If  ValueIsFilled(TableSales[i].Document) Then
            i=i+1
        Else
            TableSales.Delete(i)
        EndIf;
    EndDo;
    
    StructureAdditionalProperties.TableForRegisterRecords.Insert("TableSales", TableSales);
    
EndProcedure

