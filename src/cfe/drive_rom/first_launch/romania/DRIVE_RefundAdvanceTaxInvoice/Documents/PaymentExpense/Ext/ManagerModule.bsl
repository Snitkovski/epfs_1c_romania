﻿
&ChangeAndValidate("GetGLAccountsStructure")
Function Ext1_GetGLAccountsStructure(StructureData)
	
	ObjectParameters = StructureData.ObjectParameters;
	GLAccountsForFilling = New Structure;
	
	If ObjectParameters.OperationKind = Enums.OperationTypesPaymentExpense.Vendor Then
		GLAccountsForFilling.Insert("AccountsPayableGLAccount", StructureData.AccountsPayableGLAccount);
		GLAccountsForFilling.Insert("AdvancesPaidGLAccount", StructureData.AdvancesPaidGLAccount);
		
		If StructureData.Property("ExistsEPD")
			And StructureData.ExistsEPD <> Undefined
			And StructureData.ExistsEPD Then
			
			GLAccountsForFilling.Insert("DiscountReceivedGLAccount", StructureData.DiscountReceivedGLAccount);
			
			If StructureData.EPDAmount > 0
				And StructureData.Property("Document")
				And ValueIsFilled(StructureData.Document)
				And TypeOf(StructureData.Document) = Type("DocumentRef.SupplierInvoice") Then
				
				ProvideEPD = Common.ObjectAttributeValue(StructureData.Document, "ProvideEPD");
				If ProvideEPD = Enums.VariantsOfProvidingEPD.PaymentDocumentWithVATAdjustment Then
					GLAccountsForFilling.Insert("VATInputGLAccount", StructureData.VATInputGLAccount);
				EndIf;
			EndIf;
			
		EndIf;
		
		#Delete
	ElsIf ObjectParameters.OperationKind = Enums.OperationTypesPaymentExpense.ToCustomer Then
		#EndDelete
		#Insert
	ElsIf ObjectParameters.OperationKind = Enums.OperationTypesPaymentExpense.ToCustomer
		OR ObjectParameters.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund Then
		#EndInsert
		GLAccountsForFilling.Insert("AccountsReceivableGLAccount", StructureData.AccountsReceivableGLAccount);
		GLAccountsForFilling.Insert("AdvancesReceivedGLAccount", StructureData.AdvancesReceivedGLAccount);
		
	ElsIf ObjectParameters.OperationKind = Enums.OperationTypesPaymentExpense.OtherSettlements Then
		GLAccountsForFilling.Insert("AccountsPayableGLAccount", ObjectParameters.Correspondence);
	EndIf;
	
	Return GLAccountsForFilling;
	
EndFunction

&ChangeAndValidate("GenerateTableCashAssets")
Procedure Ext1_GenerateTableCashAssets(DocumentRefPaymentExpense, StructureAdditionalProperties)

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;

	MainLanguageCode = CommonClientServer.DefaultLanguageCode();

	Query.SetParameter("Ref",					DocumentRefPaymentExpense);
	Query.SetParameter("PointInTime",			New Boundary(StructureAdditionalProperties.ForPosting.PointInTime, BoundaryType.Including));
	Query.SetParameter("ControlPeriod",			StructureAdditionalProperties.ForPosting.PointInTime.Date);
	Query.SetParameter("Company",				StructureAdditionalProperties.ForPosting.Company);
	Query.SetParameter("PresentationCurrency",  StructureAdditionalProperties.ForPosting.PresentationCurrency);
	
	//  SAM {
	Query.SetParameter("CashExpense",			QuerySetParameterCashExpense(MainLanguageCode));
	Query.SetParameter("ExchangeDifference",	QuerySetParameterExchangeDifference(MainLanguageCode));
	//  SAM {
	
	Query.SetParameter("ExchangeRateMethod",	StructureAdditionalProperties.ForPosting.ExchangeRateMethod);

	Query.Text =
	"SELECT
	|	1 AS LineNumber,
	|	&CashExpense AS ContentOfAccountingRecord,
	|	VALUE(AccumulationRecordType.Expense) AS RecordType,
	|	DocumentTable.Date AS Date,
	|	&Company AS Company,
	|	&PresentationCurrency AS PresentationCurrency,
	|	VALUE(Catalog.PaymentMethods.Electronic) AS PaymentMethod,
	|	VALUE(Enum.CashAssetTypes.Noncash) AS CashAssetType,
	|	DocumentTable.Item AS Item,
	|	DocumentTable.BankAccount AS BankAccountPettyCash,
	|	DocumentTable.CashCurrency AS Currency,
	|	SUM(CAST(DocumentTable.DocumentAmount * CASE
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition / (ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition)
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition / (AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition)
	|				END AS NUMBER(15, 2))) AS Amount,
	|	SUM(DocumentTable.DocumentAmount) AS AmountCur,
	|	-SUM(CAST(DocumentTable.DocumentAmount * CASE
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition / (ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition)
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition / (AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition)
	|				END AS NUMBER(15, 2))) AS AmountForBalance,
	|	-SUM(DocumentTable.DocumentAmount) AS AmountCurForBalance,
	|	DocumentTable.BankAccount.GLAccount AS GLAccount
	|INTO TemporaryTableCashAssets
	|FROM
	|	Document.PaymentExpense AS DocumentTable
	|		LEFT JOIN InformationRegister.ExchangeRate.SliceLast(
	|				&PointInTime,
	|				Currency = &PresentationCurrency
	|					AND Company = &Company) AS AccountingExchangeRates
	|		ON (TRUE)
	|		LEFT JOIN InformationRegister.ExchangeRate.SliceLast(&PointInTime, Company = &Company) AS ExchangeRateOfPettyCashe
	|		ON DocumentTable.CashCurrency = ExchangeRateOfPettyCashe.Currency
	|WHERE
	|	DocumentTable.Ref = &Ref
	|	AND (DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToAdvanceHolder)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Other)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.OtherSettlements)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.IssueLoanToEmployee)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.IssueLoanToCounterparty)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Taxes))
	|
	|GROUP BY
	|	DocumentTable.Date,
	|	DocumentTable.Item,
	|	DocumentTable.BankAccount,
	|	DocumentTable.CashCurrency,
	|	DocumentTable.BankAccount.GLAccount
	|
	|UNION ALL
	|
	|SELECT
	|	TemporaryTablePaymentDetails.LineNumber,
	|	&CashExpense,
	|	VALUE(AccumulationRecordType.Expense),
	|	TemporaryTablePaymentDetails.Date,
	|	&Company,
	|	&PresentationCurrency,
	|	VALUE(Catalog.PaymentMethods.Electronic),
	|	VALUE(Enum.CashAssetTypes.Noncash),
	|	TemporaryTablePaymentDetails.Item,
	|	TemporaryTablePaymentDetails.BankAccount,
	|	TemporaryTablePaymentDetails.CashCurrency,
	|	SUM(TemporaryTablePaymentDetails.AccountingAmount),
	|	SUM(TemporaryTablePaymentDetails.PaymentAmount),
	|	-SUM(TemporaryTablePaymentDetails.AccountingAmount),
	|	-SUM(TemporaryTablePaymentDetails.PaymentAmount),
	|	TemporaryTablePaymentDetails.BankAccountCashGLAccount
	|FROM
	|	TemporaryTablePaymentDetails AS TemporaryTablePaymentDetails
	#Delete
	|WHERE
	|	(TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Vendor)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.LoanSettlements))
	#EndDelete
	#Insert
	|WHERE
	|	(TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Vendor)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.LoanSettlements)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))
	#EndInsert
	|
	|GROUP BY
	|	TemporaryTablePaymentDetails.LineNumber,
	|	TemporaryTablePaymentDetails.Date,
	|	TemporaryTablePaymentDetails.Item,
	|	TemporaryTablePaymentDetails.BankAccount,
	|	TemporaryTablePaymentDetails.CashCurrency,
	|	TemporaryTablePaymentDetails.BankAccountCashGLAccount
	|
	|UNION ALL
	|
	|SELECT
	|	MAX(PayrollPayment.LineNumber),
	|	&CashExpense,
	|	VALUE(AccumulationRecordType.Expense),
	|	PayrollPayment.Ref.Date,
	|	&Company,
	|	&PresentationCurrency,
	|	VALUE(Catalog.PaymentMethods.Electronic),
	|	VALUE(Enum.CashAssetTypes.Noncash),
	|	PayrollPayment.Ref.Item,
	|	PayrollPayment.Ref.BankAccount,
	|	PayrollPayment.Ref.CashCurrency,
	|	SUM(CAST(PayrollSheetEmployees.PaymentAmount * CASE
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition / (ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition)
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition / (AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition)
	|				END AS NUMBER(15, 2))),
	|	MIN(PayrollPayment.Ref.DocumentAmount),
	|	-SUM(CAST(PayrollSheetEmployees.PaymentAmount * CASE
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition / (ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition)
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN ExchangeRateOfPettyCashe.Rate * AccountingExchangeRates.Repetition / (AccountingExchangeRates.Rate * ExchangeRateOfPettyCashe.Repetition)
	|				END AS NUMBER(15, 2))),
	|	-MIN(PayrollPayment.Ref.DocumentAmount),
	|	PayrollPayment.Ref.BankAccount.GLAccount
	|FROM
	|	Document.PayrollSheet.Employees AS PayrollSheetEmployees
	|		INNER JOIN Document.PaymentExpense.PayrollPayment AS PayrollPayment
	|			LEFT JOIN InformationRegister.ExchangeRate.SliceLast(&PointInTime, Company = &Company) AS ExchangeRateOfPettyCashe
	|			ON PayrollPayment.Ref.CashCurrency = ExchangeRateOfPettyCashe.Currency
	|		ON PayrollSheetEmployees.Ref = PayrollPayment.Statement
	|		LEFT JOIN InformationRegister.ExchangeRate.SliceLast(
	|				&PointInTime,
	|				Currency = &PresentationCurrency
	|					AND Company = &Company) AS AccountingExchangeRates
	|		ON (TRUE)
	|WHERE
	|	PayrollPayment.Ref = &Ref
	|	AND PayrollPayment.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Salary)
	|
	|GROUP BY
	|	PayrollPayment.Ref.Date,
	|	PayrollPayment.Ref.Item,
	|	PayrollPayment.Ref.BankAccount,
	|	PayrollPayment.Ref.CashCurrency,
	|	PayrollPayment.Ref.BankAccount.GLAccount
	|
	|UNION ALL
	|
	|SELECT
	|	1,
	|	TableBankCharges.PostingContent,
	|	VALUE(AccumulationRecordType.Expense),
	|	TableBankCharges.Period,
	|	TableBankCharges.Company,
	|	TableBankCharges.PresentationCurrency,
	|	VALUE(Catalog.PaymentMethods.Electronic),
	|	VALUE(Enum.CashAssetTypes.Noncash),
	|	TableBankCharges.Item,
	|	TableBankCharges.BankAccount,
	|	TableBankCharges.Currency,
	|	SUM(TableBankCharges.Amount),
	|	SUM(TableBankCharges.AmountCur),
	|	SUM(TableBankCharges.Amount),
	|	SUM(TableBankCharges.AmountCur),
	|	TableBankCharges.GLAccount
	|FROM
	|	TemporaryTableBankCharges AS TableBankCharges
	|WHERE
	|	(TableBankCharges.Amount <> 0
	|			OR TableBankCharges.AmountCur <> 0)
	|
	|GROUP BY
	|	TableBankCharges.PostingContent,
	|	TableBankCharges.Company,
	|	TableBankCharges.PresentationCurrency,
	|	TableBankCharges.Period,
	|	TableBankCharges.Item,
	|	TableBankCharges.BankAccount,
	|	TableBankCharges.GLAccount,
	|	TableBankCharges.Currency
	|
	|INDEX BY
	|	Company,
	|	PresentationCurrency,
	|	PaymentMethod,
	|	BankAccountPettyCash,
	|	Currency,
	|	GLAccount";

	Query.Execute();

	// Setting of the exclusive lock of the cash funds controlled balances.
	Query.Text =
	"SELECT
	|	TemporaryTableCashAssets.Company AS Company,
	|	TemporaryTableCashAssets.PresentationCurrency AS PresentationCurrency,
	|	TemporaryTableCashAssets.PaymentMethod AS PaymentMethod,
	|	TemporaryTableCashAssets.CashAssetType AS CashAssetType,
	|	TemporaryTableCashAssets.BankAccountPettyCash AS BankAccountPettyCash,
	|	TemporaryTableCashAssets.Currency AS Currency
	|FROM
	|	TemporaryTableCashAssets AS TemporaryTableCashAssets";

	QueryResult = Query.Execute();

	Block = New DataLock;
	LockItem = Block.Add("AccumulationRegister.CashAssets");
	LockItem.Mode = DataLockMode.Exclusive;
	LockItem.DataSource = QueryResult;

	For Each ColumnQueryResult In QueryResult.Columns Do
		LockItem.UseFromDataSource(ColumnQueryResult.Name, ColumnQueryResult.Name);
	EndDo;
	Block.Lock();

	QueryNumber = 0;
	Query.Text = DriveServer.GetQueryTextExchangeRateDifferencesCashAssets(Query.TempTablesManager, QueryNumber);
	ResultsArray = Query.ExecuteBatch();

	StructureAdditionalProperties.TableForRegisterRecords.Insert("TableCashAssets", ResultsArray[QueryNumber].Unload());

EndProcedure

&ChangeAndValidate("GenerateTableBankReconciliation")
Procedure Ext1_GenerateTableBankReconciliation(DocumentRefPaymentExpense, StructureAdditionalProperties)

	If Not GetFunctionalOption("UseBankReconciliation") Then
		StructureAdditionalProperties.TableForRegisterRecords.Insert("TableBankReconciliation", New ValueTable);
		Return;
	EndIf;

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;
	Query.SetParameter("Ref",					DocumentRefPaymentExpense);

	Query.Text =
	"SELECT
	|	VALUE(AccumulationRecordType.Receipt) AS RecordType,
	|	DocumentTable.Date AS Period,
	|	&Ref AS Transaction,
	|	DocumentTable.BankAccount AS BankAccount,
	|	VALUE(Enum.BankReconciliationTransactionTypes.Payment) AS TransactionType,
	|	-DocumentTable.DocumentAmount AS Amount
	|FROM
	|	Document.PaymentExpense AS DocumentTable
	|WHERE
	|	DocumentTable.Ref = &Ref
	|	AND (DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToAdvanceHolder)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Other)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.OtherSettlements)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.LoanSettlements)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.IssueLoanToEmployee)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.IssueLoanToCounterparty)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Taxes))
	|
	|UNION ALL
	|
	|SELECT
	|	VALUE(AccumulationRecordType.Receipt),
	|	TemporaryTablePaymentDetails.Date,
	|	&Ref,
	|	TemporaryTablePaymentDetails.BankAccount,
	|	VALUE(Enum.BankReconciliationTransactionTypes.Payment),
	|	-SUM(TemporaryTablePaymentDetails.PaymentAmount)
	|FROM
	|	TemporaryTablePaymentDetails AS TemporaryTablePaymentDetails
	|WHERE
	#Delete
	|	(TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Vendor)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer))
	#EndDelete
	#Insert
	|	(TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Vendor)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|			OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))
	#EndInsert
	|
	|GROUP BY
	|	TemporaryTablePaymentDetails.Date,
	|	TemporaryTablePaymentDetails.BankAccount
	|
	|UNION ALL
	|
	|SELECT
	|	VALUE(AccumulationRecordType.Receipt),
	|	SalaryPayment.Date,
	|	&Ref,
	|	SalaryPayment.BankAccount,
	|	VALUE(Enum.BankReconciliationTransactionTypes.Payment),
	|	-SalaryPayment.DocumentAmount
	|FROM
	|	Document.PaymentExpense AS SalaryPayment
	|WHERE
	|	SalaryPayment.Ref = &Ref
	|	AND SalaryPayment.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Salary)
	|
	|UNION ALL
	|
	|SELECT
	|	VALUE(AccumulationRecordType.Receipt),
	|	TableBankCharges.Period,
	|	&Ref,
	|	TableBankCharges.BankAccount,
	|	VALUE(Enum.BankReconciliationTransactionTypes.Fee),
	|	-SUM(TableBankCharges.AmountCur)
	|FROM
	|	TemporaryTableBankCharges AS TableBankCharges
	|WHERE
	|	TableBankCharges.AmountCur <> 0
	|
	|GROUP BY
	|	TableBankCharges.Period,
	|	TableBankCharges.BankAccount";

	QueryResult = Query.Execute();

	StructureAdditionalProperties.TableForRegisterRecords.Insert("TableBankReconciliation", QueryResult.Unload());

EndProcedure

&ChangeAndValidate("GenerateTableCustomerAccounts")
Procedure Ext1_GenerateTableCustomerAccounts(DocumentRefPaymentExpense, StructureAdditionalProperties)

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;

	// Setting the exclusive lock for the controlled balances of accounts receivable.
	Query.Text =
	"SELECT DISTINCT
	|	TemporaryTablePaymentDetails.Company AS Company,
	|	TemporaryTablePaymentDetails.PresentationCurrency AS PresentationCurrency,
	|	TemporaryTablePaymentDetails.Counterparty AS Counterparty,
	|	TemporaryTablePaymentDetails.Contract AS Contract,
	|	TemporaryTablePaymentDetails.Document AS Document,
	|	CASE
	|		WHEN TemporaryTablePaymentDetails.DoOperationsByOrders
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.SalesOrder.EmptyRef)
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.WorkOrder.EmptyRef)
	|			THEN TemporaryTablePaymentDetails.Order
	|		ELSE UNDEFINED
	|	END AS Order,
	|	TemporaryTablePaymentDetails.SettlementsType AS SettlementsType
	|FROM
	|	TemporaryTablePaymentDetails AS TemporaryTablePaymentDetails
	#Delete
	|WHERE
	|	TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)";
	#EndDelete
	#Insert
	|WHERE
	|	(TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|		OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))";
	#EndInsert

	QueryResult = Query.Execute();

	Block = New DataLock;
	LockItem = Block.Add("AccumulationRegister.AccountsReceivable");
	LockItem.Mode = DataLockMode.Exclusive;
	LockItem.DataSource = QueryResult;

	For Each ColumnQueryResult In QueryResult.Columns Do
		LockItem.UseFromDataSource(ColumnQueryResult.Name, ColumnQueryResult.Name);
	EndDo;
	Block.Lock();

	MainLanguageCode = CommonClientServer.DefaultLanguageCode();

	Query.SetParameter("Ref", DocumentRefPaymentExpense);
	Query.SetParameter("PointInTime", New Boundary(StructureAdditionalProperties.ForPosting.PointInTime, BoundaryType.Including));
	Query.SetParameter("ControlPeriod", StructureAdditionalProperties.ForPosting.PointInTime.Date);
	Query.SetParameter("Company", StructureAdditionalProperties.ForPosting.Company);
	
	//  SAM  {
	Query.SetParameter("CustomerAdvanceRepayment", QuerySetParameterCustomerAdvanceRepayment(MainLanguageCode));
	Query.SetParameter("AppearenceOfCustomerLiability", QuerySetParameterAppearenceOfCustomerLiability(MainLanguageCode));
	Query.SetParameter("ExchangeDifference", QuerySetParameterExchangeDifference(MainLanguageCode));
	//  }  SAM 
	
	Query.SetParameter("ExchangeRateMethod", StructureAdditionalProperties.ForPosting.ExchangeRateMethod);

	Query.Text =
	"SELECT DISTINCT
	|	TemporaryTablePaymentDetails.Company AS Company,
	|	TemporaryTablePaymentDetails.PresentationCurrency AS PresentationCurrency,
	|	TemporaryTablePaymentDetails.Counterparty AS Counterparty,
	|	TemporaryTablePaymentDetails.Contract AS Contract,
	|	TemporaryTablePaymentDetails.Document AS Document,
	|	CASE
	|		WHEN TemporaryTablePaymentDetails.DoOperationsByOrders
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.SalesOrder.EmptyRef)
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.WorkOrder.EmptyRef)
	|			THEN TemporaryTablePaymentDetails.Order
	|		ELSE UNDEFINED
	|	END AS Order,
	|	TemporaryTablePaymentDetails.SettlementsType AS SettlementsType
	|INTO TemporaryTableAccountsReceivableAdvances
	|FROM
	|	TemporaryTablePaymentDetails AS TemporaryTablePaymentDetails
	|WHERE
	#Delete
	|	TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	#EndDelete
	#Insert
	|	(TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|     OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))
	#EndInsert
	|	AND TemporaryTablePaymentDetails.AdvanceFlag
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	TableBalances.Company AS Company,
	|	TableBalances.PresentationCurrency AS PresentationCurrency,
	|	TableBalances.Counterparty AS Counterparty,
	|	TableBalances.Contract AS Contract,
	|	TableBalances.Document AS Document,
	|	TableBalances.Order AS Order,
	|	TableBalances.SettlementsType AS SettlementsType,
	|	SUM(TableBalances.AmountBalance) AS AmountBalance,
	|	SUM(TableBalances.AmountCurBalance) AS AmountCurBalance
	|INTO TemporaryTableAccountsReceivableAdvancesBalances
	|FROM
	|	(SELECT
	|		TableBalances.Company AS Company,
	|		TableBalances.PresentationCurrency AS PresentationCurrency,
	|		TableBalances.Counterparty AS Counterparty,
	|		TableBalances.Contract AS Contract,
	|		TableBalances.Document AS Document,
	|		TableBalances.Order AS Order,
	|		TableBalances.SettlementsType AS SettlementsType,
	|		TableBalances.AmountBalance AS AmountBalance,
	|		TableBalances.AmountCurBalance AS AmountCurBalance
	|	FROM
	|		AccumulationRegister.AccountsReceivable.Balance(
	|				&PointInTime,
	|				(Company, PresentationCurrency, Counterparty, Contract, Document, Order, SettlementsType) IN
	|					(SELECT
	|						TemporaryTableAccountsReceivableAdvances.Company,
	|						TemporaryTableAccountsReceivableAdvances.PresentationCurrency,
	|						TemporaryTableAccountsReceivableAdvances.Counterparty,
	|						TemporaryTableAccountsReceivableAdvances.Contract,
	|						TemporaryTableAccountsReceivableAdvances.Document,
	|						TemporaryTableAccountsReceivableAdvances.Order,
	|						TemporaryTableAccountsReceivableAdvances.SettlementsType
	|					FROM
	|						TemporaryTableAccountsReceivableAdvances)) AS TableBalances
	|
	|	UNION ALL
	|
	|	SELECT
	|		DocumentRegisterRecords.Company,
	|		DocumentRegisterRecords.PresentationCurrency,
	|		DocumentRegisterRecords.Counterparty,
	|		DocumentRegisterRecords.Contract,
	|		DocumentRegisterRecords.Document,
	|		DocumentRegisterRecords.Order,
	|		DocumentRegisterRecords.SettlementsType,
	|		CASE
	|			WHEN DocumentRegisterRecords.RecordType = VALUE(AccumulationRecordType.Receipt)
	|				THEN -DocumentRegisterRecords.Amount
	|			ELSE DocumentRegisterRecords.Amount
	|		END,
	|		CASE
	|			WHEN DocumentRegisterRecords.RecordType = VALUE(AccumulationRecordType.Receipt)
	|				THEN -DocumentRegisterRecords.AmountCur
	|			ELSE DocumentRegisterRecords.AmountCur
	|		END
	|	FROM
	|		AccumulationRegister.AccountsReceivable AS DocumentRegisterRecords
	|	WHERE
	|		DocumentRegisterRecords.Recorder = &Ref) AS TableBalances
	|
	|GROUP BY
	|	TableBalances.Document,
	|	TableBalances.SettlementsType,
	|	TableBalances.Company,
	|	TableBalances.Order,
	|	TableBalances.Counterparty,
	|	TableBalances.Contract,
	|	TableBalances.PresentationCurrency
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	TemporaryTablePaymentDetails.LineNumber AS LineNumber,
	|	TemporaryTablePaymentDetails.Document AS Document,
	|	&Company AS Company,
	|	TemporaryTablePaymentDetails.PresentationCurrency AS PresentationCurrency,
	|	VALUE(AccumulationRecordType.Receipt) AS RecordType,
	|	TemporaryTablePaymentDetails.BankAccount AS BankAccount,
	|	TemporaryTablePaymentDetails.Counterparty AS Counterparty,
	|	TemporaryTablePaymentDetails.Contract AS Contract,
	|	CASE
	|		WHEN TemporaryTablePaymentDetails.DoOperationsByOrders
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.SalesOrder.EmptyRef)
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.WorkOrder.EmptyRef)
	|			THEN TemporaryTablePaymentDetails.Order
	|		ELSE UNDEFINED
	|	END AS Order,
	|	TemporaryTablePaymentDetails.SettlementsType AS SettlementsType,
	|	TemporaryTablePaymentDetails.Date AS Date,
	|	TemporaryTablePaymentDetails.PaymentAmount AS PaymentAmount,
	|	CASE
	|		WHEN ISNULL(AdvancesBalances.AmountCurBalance, 0) = 0
	|			THEN TemporaryTablePaymentDetails.AccountingAmount
	|		ELSE TemporaryTablePaymentDetails.SettlementsAmount * AdvancesBalances.AmountBalance / AdvancesBalances.AmountCurBalance
	|	END AS Amount,
	|	TemporaryTablePaymentDetails.SettlementsAmount AS AmountCur,
	|	CASE
	|		WHEN ISNULL(AdvancesBalances.AmountCurBalance, 0) = 0
	|			THEN TemporaryTablePaymentDetails.AccountingAmount
	|		ELSE TemporaryTablePaymentDetails.SettlementsAmount * AdvancesBalances.AmountBalance / AdvancesBalances.AmountCurBalance
	|	END AS AmountForBalance,
	|	TemporaryTablePaymentDetails.SettlementsAmount AS AmountCurForBalance,
	|	CASE
	|		WHEN ISNULL(AdvancesBalances.AmountCurBalance, 0) = 0
	|			THEN TemporaryTablePaymentDetails.AccountingAmount
	|		ELSE TemporaryTablePaymentDetails.SettlementsAmount * AdvancesBalances.AmountBalance / AdvancesBalances.AmountCurBalance
	|	END AS AmountForPayment,
	|	TemporaryTablePaymentDetails.SettlementsAmount AS AmountForPaymentCur,
	|	TemporaryTablePaymentDetails.CustomerAdvancesGLAccount AS GLAccount,
	|	TemporaryTablePaymentDetails.SettlementsCurrency AS Currency,
	|	TemporaryTablePaymentDetails.CashCurrency AS CashCurrency,
	|	&CustomerAdvanceRepayment AS ContentOfAccountingRecord
	|INTO TemporaryTableAccountsReceivable
	|FROM
	|	TemporaryTablePaymentDetails AS TemporaryTablePaymentDetails
	|		LEFT JOIN TemporaryTableAccountsReceivableAdvancesBalances AS AdvancesBalances
	|		ON TemporaryTablePaymentDetails.Company = AdvancesBalances.Company
	|			AND TemporaryTablePaymentDetails.PresentationCurrency = AdvancesBalances.PresentationCurrency
	|			AND TemporaryTablePaymentDetails.Counterparty = AdvancesBalances.Counterparty
	|			AND TemporaryTablePaymentDetails.Contract = AdvancesBalances.Contract
	|			AND TemporaryTablePaymentDetails.Document = AdvancesBalances.Document
	|			AND (CASE
	|				WHEN TemporaryTablePaymentDetails.DoOperationsByOrders
	|						AND TemporaryTablePaymentDetails.Order <> VALUE(Document.SalesOrder.EmptyRef)
	|						AND TemporaryTablePaymentDetails.Order <> VALUE(Document.WorkOrder.EmptyRef)
	|					THEN TemporaryTablePaymentDetails.Order
	|				ELSE UNDEFINED
	|			END = AdvancesBalances.Order)
	|			AND TemporaryTablePaymentDetails.SettlementsType = AdvancesBalances.SettlementsType
	|WHERE
	#Delete
	|	TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	#EndDelete
	#Insert
	|	(TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|     OR TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))
	#EndInsert
	|	AND TemporaryTablePaymentDetails.AdvanceFlag
	|
	|UNION ALL
	|
	|SELECT
	|	TemporaryTablePaymentDetails.LineNumber,
	|	TemporaryTablePaymentDetails.Document,
	|	&Company,
	|	TemporaryTablePaymentDetails.PresentationCurrency,
	|	VALUE(AccumulationRecordType.Receipt),
	|	TemporaryTablePaymentDetails.BankAccount,
	|	TemporaryTablePaymentDetails.Counterparty,
	|	TemporaryTablePaymentDetails.Contract,
	|	CASE
	|		WHEN TemporaryTablePaymentDetails.DoOperationsByOrders
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.SalesOrder.EmptyRef)
	|				AND TemporaryTablePaymentDetails.Order <> VALUE(Document.WorkOrder.EmptyRef)
	|			THEN TemporaryTablePaymentDetails.Order
	|		ELSE UNDEFINED
	|	END,
	|	TemporaryTablePaymentDetails.SettlementsType,
	|	TemporaryTablePaymentDetails.Date,
	|	TemporaryTablePaymentDetails.PaymentAmount,
	|	TemporaryTablePaymentDetails.AccountingAmount,
	|	TemporaryTablePaymentDetails.SettlementsAmount,
	|	TemporaryTablePaymentDetails.AccountingAmount,
	|	TemporaryTablePaymentDetails.SettlementsAmount,
	|	TemporaryTablePaymentDetails.AccountingAmount,
	|	TemporaryTablePaymentDetails.SettlementsAmount,
	|	TemporaryTablePaymentDetails.GLAccountCustomerSettlements,
	|	TemporaryTablePaymentDetails.SettlementsCurrency,
	|	TemporaryTablePaymentDetails.CashCurrency,
	|	&AppearenceOfCustomerLiability
	|FROM
	|	TemporaryTablePaymentDetails AS TemporaryTablePaymentDetails
	|WHERE
	|	TemporaryTablePaymentDetails.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|	AND NOT TemporaryTablePaymentDetails.AdvanceFlag
	|
	|INDEX BY
	|	Company,
	|	PresentationCurrency,
	|	Counterparty,
	|	Contract,
	|	Currency,
	|	Document,
	|	Order,
	|	SettlementsType,
	|	GLAccount";

	Query.Execute();

	QueryNumber = 0;
	Query.Text = DriveServer.GetQueryTextCurrencyExchangeRateAccountsReceivable(Query.TempTablesManager, False, QueryNumber);
	ResultsArray = Query.ExecuteBatch();

	StructureAdditionalProperties.TableForRegisterRecords.Insert("TableAccountsReceivable", ResultsArray[QueryNumber].Unload());

EndProcedure

&ChangeAndValidate("GenerateTableUnallocatedExpenses")
Procedure Ext1_GenerateTableUnallocatedExpenses(DocumentRefPaymentExpense, StructureAdditionalProperties)

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;

	Query.SetParameter("Ref", DocumentRefPaymentExpense);
	Query.SetParameter("Company", StructureAdditionalProperties.ForPosting.Company);
	Query.SetParameter("PresentationCurrency",  StructureAdditionalProperties.ForPosting.PresentationCurrency);
	Query.SetParameter("IncomeAndExpensesAccountingCashMethod", StructureAdditionalProperties.AccountingPolicy.IncomeAndExpensesAccountingCashMethod);

	Query.Text =
	"SELECT
	|	1 AS Ordering,
	|	VALUE(AccumulationRecordType.Receipt) AS RecordType,
	|	DocumentTable.LineNumber AS LineNumber,
	|	DocumentTable.Date AS Period,
	|	&Company AS Company,
	|	&PresentationCurrency AS PresentationCurrency,
	|	&Ref AS Document,
	|	DocumentTable.Item AS Item,
	|	0 AS AmountIncome,
	|	DocumentTable.AccountingAmount AS AmountExpense
	|FROM
	|	TemporaryTablePaymentDetails AS DocumentTable
	|WHERE
	|	&IncomeAndExpensesAccountingCashMethod
	|	AND DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Vendor)
	|	AND DocumentTable.AdvanceFlag
	|
	|UNION ALL
	|
	|SELECT
	|	2,
	|	VALUE(AccumulationRecordType.Receipt),
	|	DocumentTable.LineNumber,
	|	DocumentTable.Date,
	|	&Company,
	|	&PresentationCurrency,
	|	&Ref,
	|	DocumentTable.Item,
	|	-DocumentTable.AccountingAmount,
	|	0
	|FROM
	|	TemporaryTablePaymentDetails AS DocumentTable
	|WHERE
	|	&IncomeAndExpensesAccountingCashMethod
	#Delete
	|	AND DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	#EndDelete
	#Insert
	|	AND (DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|        OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))
	#EndInsert
	|	AND DocumentTable.AdvanceFlag
	|
	|ORDER BY
	|	Ordering,
	|	LineNumber";

	QueryResult = Query.Execute();

	StructureAdditionalProperties.TableForRegisterRecords.Insert("TableUnallocatedExpenses", QueryResult.Unload());

EndProcedure

&ChangeAndValidate("GenerateTableIncomeAndExpensesCashMethod")
Procedure Ext1_GenerateTableIncomeAndExpensesCashMethod(DocumentRefPaymentExpense, StructureAdditionalProperties)

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;

	Query.SetParameter("Ref", 									DocumentRefPaymentExpense);
	Query.SetParameter("Company", 								StructureAdditionalProperties.ForPosting.Company);
	Query.SetParameter("PointInTime", 							New Boundary(StructureAdditionalProperties.ForPosting.PointInTime, BoundaryType.Including));
	Query.SetParameter("IncomeAndExpensesAccountingCashMethod", StructureAdditionalProperties.AccountingPolicy.IncomeAndExpensesAccountingCashMethod);
	Query.SetParameter("PresentationCurrency",					StructureAdditionalProperties.ForPosting.PresentationCurrency);
	Query.SetParameter("ExchangeRateMethod",					StructureAdditionalProperties.ForPosting.ExchangeRateMethod);

	Query.Text =
	"SELECT
	|	DocumentTable.Date AS Period,
	|	&Company AS Company,
	|	DocumentTable.PresentationCurrency AS PresentationCurrency,
	|	UNDEFINED AS BusinessLine,
	|	DocumentTable.Item AS Item,
	|	0 AS AmountIncome,
	|	DocumentTable.AccountingAmount AS AmountExpense
	|FROM
	|	TemporaryTablePaymentDetails AS DocumentTable
	|WHERE
	|	&IncomeAndExpensesAccountingCashMethod
	|	AND DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Vendor)
	|	AND DocumentTable.AdvanceFlag
	|
	|UNION ALL
	|
	|SELECT
	|	Table.Period,
	|	Table.Company,
	|	Table.PresentationCurrency,
	|	Table.BusinessLine,
	|	Table.Item,
	|	0,
	|	Table.AmountExpense
	|FROM
	|	TemporaryTableTableDeferredIncomeAndExpenditure AS Table
	|WHERE
	|	Table.AmountExpense > 0
	|
	|UNION ALL
	|
	|SELECT
	|	DocumentTable.Date,
	|	&Company,
	|	&PresentationCurrency,
	|	CASE
	|		WHEN DocumentTable.BusinessLine <> VALUE(Catalog.LinesOfBusiness.EmptyRef)
	|			THEN DocumentTable.BusinessLine
	|		ELSE VALUE(Catalog.LinesOfBusiness.Other)
	|	END,
	|	DocumentTable.Item,
	|	0,
	|	CAST(DocumentTable.DocumentAmount * CASE
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|					THEN AccountingExchangeRateSliceLast.Rate * SettlementsExchangeRate.Repetition / (SettlementsExchangeRate.Rate * AccountingExchangeRateSliceLast.Repetition)
	|				WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|					THEN SettlementsExchangeRate.Rate * AccountingExchangeRateSliceLast.Repetition / (AccountingExchangeRateSliceLast.Rate * SettlementsExchangeRate.Repetition)
	|				END AS NUMBER(15, 2))
	|FROM
	|	Document.PaymentExpense AS DocumentTable
	|		LEFT JOIN InformationRegister.ExchangeRate.SliceLast(
	|				&PointInTime,
	|				Currency = &PresentationCurrency AND Company = &Company) AS AccountingExchangeRateSliceLast
	|		ON (TRUE)
	|		LEFT JOIN InformationRegister.ExchangeRate.SliceLast(&PointInTime, Company = &Company) AS SettlementsExchangeRate
	|		ON DocumentTable.CashCurrency = SettlementsExchangeRate.Currency
	|WHERE
	|	&IncomeAndExpensesAccountingCashMethod
	|	AND DocumentTable.Ref = &Ref
	|	AND (DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Other)
	|			OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.OtherSettlements)
	|			OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Salary)
	|			OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Taxes)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.LoanSettlements)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.IssueLoanToEmployee)
	|			OR DocumentTable.Ref.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.IssueLoanToCounterparty))
	|
	|UNION ALL
	|
	|SELECT
	|	DocumentTable.Date,
	|	&Company,
	|	DocumentTable.PresentationCurrency,
	|	UNDEFINED,
	|	DocumentTable.Item,
	|	-DocumentTable.AccountingAmount,
	|	0
	|FROM
	|	TemporaryTablePaymentDetails AS DocumentTable
	|WHERE
	|	&IncomeAndExpensesAccountingCashMethod
	#Delete
	|	AND DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	#EndDelete
	#Insert
	|	AND (DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|        OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))
	#EndInsert
	|	AND DocumentTable.AdvanceFlag
	|
	|UNION ALL
	|
	|SELECT
	|	Table.Period,
	|	Table.Company,
	|	Table.PresentationCurrency,
	|	Table.BusinessLine,
	|	Table.Item,
	|	-Table.AmountIncome,
	|	0
	|FROM
	|	TemporaryTableTableDeferredIncomeAndExpenditure AS Table
	|WHERE
	|	Table.AmountIncome > 0
	|
	|UNION ALL
	|
	|SELECT
	|	Table.Period,
	|	Table.Company,
	|	Table.PresentationCurrency,
	|	VALUE(Catalog.LinesOfBusiness.Other),
	|	Table.Item,
	|	0,
	|	Table.Amount
	|FROM
	|	TemporaryTableBankCharges AS Table
	|WHERE
	|	&IncomeAndExpensesAccountingCashMethod
	|	AND Table.Amount <> 0";

	QueryResult = Query.Execute();

	StructureAdditionalProperties.TableForRegisterRecords.Insert("TableIncomeAndExpensesCashMethod", QueryResult.Unload());

EndProcedure

&ChangeAndValidate("GenerateTableInvoicesAndOrdersPayment")
Procedure Ext1_GenerateTableInvoicesAndOrdersPayment(DocumentRefPaymentExpense, StructureAdditionalProperties)

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;

	Query.SetParameter("Company", StructureAdditionalProperties.ForPosting.Company);
	Query.SetParameter("PointInTime", New Boundary(StructureAdditionalProperties.ForPosting.PointInTime, BoundaryType.Including));

	Query.Text =
	"SELECT
	|	DocumentTable.LineNumber AS LineNumber,
	|	DocumentTable.Date AS Period,
	|	&Company AS Company,
	|	DocumentTable.Order AS Quote,
	|	SUM(CASE
	|			WHEN NOT DocumentTable.AdvanceFlag
	|				THEN 0
	|			ELSE DocumentTable.SettlementsAmount
	|		END) * CASE
	|		WHEN DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|			THEN -1
	|		ELSE 1
	|	END AS AdvanceAmount,
	|	SUM(CASE
	|			WHEN DocumentTable.AdvanceFlag
	|				THEN 0
	|			ELSE DocumentTable.SettlementsAmount
	|		END) * CASE
	|		WHEN DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|			THEN -1
	|		ELSE 1
	|	END AS PaymentAmount
	|FROM
	|	TemporaryTablePaymentDetails AS DocumentTable
	|WHERE
	|	DocumentTable.DoOperationsByOrders
	|	AND (VALUETYPE(DocumentTable.Order) = TYPE(Document.SalesOrder)
	|				AND DocumentTable.Order <> VALUE(Document.SalesOrder.EmptyRef)
	|			OR VALUETYPE(DocumentTable.Order) = TYPE(Document.PurchaseOrder)
	|				AND DocumentTable.Order <> VALUE(Document.PurchaseOrder.EmptyRef)
	|			OR VALUETYPE(DocumentTable.Order) = TYPE(Document.SubcontractorOrderIssued)
	|				AND DocumentTable.Order <> VALUE(Document.SubcontractorOrderIssued.EmptyRef))
	|	AND (DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Vendor)
	#Delete
	|			OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer))
	#EndDelete
	#Insert
	|			OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToCustomer)
	|			OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund))
	#EndInsert
	|
	|GROUP BY
	|	DocumentTable.LineNumber,
	|	DocumentTable.Date,
	|	DocumentTable.Order,
	|	DocumentTable.OperationKind
	|
	|ORDER BY
	|	LineNumber";

	QueryResult = Query.Execute();

	StructureAdditionalProperties.TableForRegisterRecords.Insert("TableInvoicesAndOrdersPayment", QueryResult.Unload());

EndProcedure
