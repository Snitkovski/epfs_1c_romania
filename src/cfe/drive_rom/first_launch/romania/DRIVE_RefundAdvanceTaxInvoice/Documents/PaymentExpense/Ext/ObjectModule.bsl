﻿
&After("FillCheckProcessing")
Procedure Ext1_FillCheckProcessing(Cancel, CheckedAttributes)
	
	If OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund Then
		DriveServer.DeleteAttributeBeingChecked(CheckedAttributes, "TaxKind");
		CheckedAttributes.Add("PaymentDetails.Document");
	EndIf;
	
EndProcedure
