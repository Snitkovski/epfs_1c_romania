﻿
&AtServer
&After("SetVisibleByFOUseSubsystemPayroll")
Procedure Ext1_SetVisibleByFOUseSubsystemPayroll()
	
	Items.OperationKind.ChoiceList.Add(Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund);
	
EndProcedure

&AtServer
&After("SetVisibilityItemsDependenceOnOperationKindAndUseBankCharges")
Procedure Ext1_SetVisibilityItemsDependenceOnOperationKindAndUseBankCharges()
	
	If Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund Then
		
		Items.SettlementsWithCounterparty.Visible	= True;
		Items.PaymentDetailsPickup.Visible			= False;
		Items.PaymentDetailsFillDetails.Visible		= True;
		
		Items.Counterparty.Visible					= True;
		Items.Counterparty.Title					= NStr("en='Customer';
															|DE='Kunde';
															|es_ES='Cliente';
															|it='Acquirente';
															|pl='Klienta';
															|ro='Client';
															|ru='Клиента'");
		Items.CounterpartyAccount.Visible			= True;
		
		Items.PaymentAmount.Visible		= True;
		Items.PaymentAmount.Title		= NStr("en='Advance refund';
												|ro='Storno avans';
												|ru='Возврат аванса'");
		
		Items.PayrollPaymentTotalPaymentAmount.Visible	= False;
		
		Items.Item.Visible				 = False;
		Items.PaymentDetailsItem.Visible = True;
		
	EndIf;
	
EndProcedure

&AtServer
&After("SetVisibilityPlanningDocument")
Procedure Ext1_SetVisibilityPlanningDocument()
	
	If Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund
		OR Not GetFunctionalOption("PaymentCalendar") Then
		Items.PlanningDocuments.Visible = False;
	EndIf;
	
EndProcedure

&AtServer
&After("SetChoiceParameterLinksAvailableTypes")
Procedure Ext1_SetChoiceParameterLinksAvailableTypes()
	
	If Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund Then
		
		Array = New Array();
		Array.Add(Type("DocumentRef.CashReceipt"));
		Array.Add(Type("DocumentRef.PaymentReceipt"));
		Array.Add(Type("DocumentRef.ArApAdjustments"));
		Array.Add(Type("DocumentRef.SalesOrder"));
		Array.Add(Type("DocumentRef.AccountSalesFromConsignee"));
		Array.Add(Type("DocumentRef.FixedAssetSale"));
		Array.Add(Type("DocumentRef.SupplierInvoice"));
		Array.Add(Type("DocumentRef.SalesInvoice"));
		Array.Add(Type("DocumentRef.CreditNote"));
		
		ValidTypes = New TypeDescription(Array, ,);
		Items.PaymentDetailsDocument.TypeRestriction = ValidTypes;
		
		ValidTypes = New TypeDescription("DocumentRef.SalesOrder", , );
		Items.PaymentDetailsOrder.TypeRestriction = ValidTypes;
		
		Items.PaymentDetailsDocument.ToolTip = NStr("en='An advance payment document that should be returned.';
		                                            |DE='Ein Vorauszahlungsbeleg, der zurückgegeben werden soll.';
		                                            |es_ES='Un documento del pago anticipado que tiene que devolverse.';
		                                            |it='Pagamento di un anticipo, documento che deve essere restituito.';
		                                            |pl='Dokument płatności zaliczkowej do zwrotu.';
		                                            |ro='Un document de plată în avans care ar trebui returnat.';
		                                            |ru='Документ расчетов с контрагентом, по которому осуществляется возврат денежных средств'");
		
	EndIf;
	
EndProcedure

&AtServer
&After("SetCFItemWhenChangingTheTypeOfOperations")
Procedure Ext1_SetCFItemWhenChangingTheTypeOfOperations()
	
	If Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund
		AND (Object.Item = Catalogs.CashFlowItems.PaymentToVendor
		OR Object.Item = Catalogs.CashFlowItems.Other
		OR Object.Item = Catalogs.CashFlowItems.PaymentFromCustomers) Then
		Object.Item = Catalogs.CashFlowItems.PaymentFromCustomers;
	EndIf;
	
EndProcedure

&AtServer
&After("SetCFItem")
Procedure Ext1_SetCFItem()
	
	If Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund Then
		Object.Item = Catalogs.CashFlowItems.PaymentFromCustomers;
	EndIf;
	
EndProcedure

&AtServer
&After("FillVATRateByCompanyVATTaxation")
Procedure Ext1_FillVATRateByCompanyVATTaxation()
	
	TaxationBeforeChange = Object.VATTaxation;
	
	If Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund
		AND Not TaxationBeforeChange = Object.VATTaxation Then
		
		FillVATRateByVATTaxation();
		
	EndIf;

EndProcedure

&AtServer
&After("SetVisibleOfVATTaxation")
Procedure Ext1_SetVisibleOfVATTaxation()
	
	If Object.VATTaxation = Enums.VATTaxationTypes.SubjectToVAT Then
		
		If Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund Then
			Items.VATAmount.Visible = True;
		EndIf;
		
	EndIf;
	
EndProcedure

&AtClient
&After("ClearAttributesNotRelatedToOperation")
Procedure Ext1_ClearAttributesNotRelatedToOperation()
	
	If Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund") Then
		Object.Correspondence = Undefined;
		Object.TaxKind = Undefined;
		Object.CounterpartyAccount = Undefined;
		Object.AdvanceHolder = Undefined;
		Object.Document = Undefined;
		Object.PayrollPayment.Clear();
		Object.Department = Undefined;
		Object.BusinessLine = Undefined;
		Object.Order = Undefined;
		Object.LoanContract = Undefined;
		For Each TableRow In Object.PaymentDetails Do
			TableRow.Order = Undefined;
			TableRow.Document = Undefined;
			TableRow.AdvanceFlag = False;
			If Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund") Then
				TableRow.EPDAmount = 0;
				TableRow.SettlementsEPDAmount = 0;
				TableRow.ExistsEPD = False;
			EndIf;
		EndDo;
	EndIf;
	
EndProcedure

&AtClient
Procedure Ext1_FillDetailsAfter(Command)
	
	If Object.DocumentAmount = 0
		And Object.OperationKind <> PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund") Then
		ShowMessageBox(Undefined, NStr("en='Please specify the amount.';
		                               |DE='Bitte geben Sie den Betrag an.';
		                               |es_ES='Por favor, especifique el importe.';
		                               |it='Si prega di specificare l''importo.';
		                               |pl='Określ kwotę.';
		                               |ro='Specificați suma.';
		                               |ru='Укажите вначале сумму документа.'"));
		Return;
	EndIf;

EndProcedure

&AtClient
&After("FillDetailsEnd")
Procedure Ext1_FillDetailsEnd(Result, AdditionalParameters)
	
	If Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund") Then
		
		FillAdvancesPaymentDetails();
		
	EndIf;
	
	SetCurrentPage();
	
EndProcedure

&AtClient
Procedure Ext1_PaymentDetailsSignAdvanceOnChangeAfter(Item)
	
	TabularSectionRow = Items.PaymentDetails.CurrentData;
	
	If Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund") Then
		
		If TypeOf(TabularSectionRow.Document) = Type("DocumentRef.CashReceipt")
			Or TypeOf(TabularSectionRow.Document) = Type("DocumentRef.PaymentReceipt")
			Or TypeOf(TabularSectionRow.Document) = Type("DocumentRef.CreditNote") Then
			
			If Not TabularSectionRow.AdvanceFlag Then
				TabularSectionRow.AdvanceFlag = True;
				CommonClientServer.MessageToUser(
				NStr("en='Cannot cancel advance payment for this document type.';
				|DE='Für diese Belegart kann die Vorauszahlung nicht storniert werden.';
				|es_ES='No se puede cancelar el pago anticipado para este tipo de documento.';
				|it='Non è possibile annullare l''anticipo di pagamento per questo tipo di documento.';
				|pl='Nie można anulować płatności zaliczkowej dla tego typu dokumentu.';
				|ro='Nu se poate anula plata în avans pentru acest tip de document.';
				|ru='Для данного типа документа расчетов признак аванса всегда установлен!'"));
			EndIf;
		EndIf;
	EndIf;
	
EndProcedure

&AtClient
Procedure Ext1_PaymentDetailsDocumentStartChoiceAround(Item, ChoiceData, StandardProcessing)
	
	StandardProcessing = False;
	
	TabularSectionRow = Items.PaymentDetails.CurrentData;
	
	If TabularSectionRow.AdvanceFlag
		AND Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Vendor") Then
		
		ShowMessageBox(, NStr("en='The current document will be the billing document for the advance payment..';
		                      |DE='Der aktuelle Beleg ist der Abrechnungsbeleg für die Vorauszahlung.';
		                      |es_ES='El documento actual será documento de facturación para el pago anticipado..';
		                      |it='Il documento corrente sarà il documento di fatturazione per il pagamento di anticipo.';
		                      |pl='Bieżący dokument będzie dokumentem rozliczeń zaliczkowych..';
		                      |ro='Documentul curent va fi documentul de facturare pentru plata în avans.';
		                      |ru='Данный документ является документом расчетов для авансовых платежей.'"));
		
	Else
		
		ThisIsAccountsReceivable = (Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.ToCustomer")
		OR Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund"));
		
		StructureFilter = New Structure();
		StructureFilter.Insert("Company",		Object.Company);
		StructureFilter.Insert("Counterparty",	Object.Counterparty);
		
		If ValueIsFilled(TabularSectionRow.Contract) Then
			StructureFilter.Insert("Contract", TabularSectionRow.Contract);
		EndIf;
		
		ParameterStructure = New Structure("Filter, ThisIsAccountsReceivable, DocumentType",
			StructureFilter,
			ThisIsAccountsReceivable,
			TypeOf(Object.Ref)
		);
		
		OpenForm("CommonForm.SelectDocumentOfSettlements", ParameterStructure, Item);
		
	EndIf;
	
EndProcedure

&AtClient
&After("RunActionsOnAccountsDocumentChange")
Procedure Ext1_RunActionsOnAccountsDocumentChange()
	
	TabularSectionRow = Items.PaymentDetails.CurrentData;
	
	If Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund") Then
		
		If TypeOf(TabularSectionRow.Document) = Type("DocumentRef.CashReceipt")
			Or TypeOf(TabularSectionRow.Document) = Type("DocumentRef.PaymentReceipt")
			Or TypeOf(TabularSectionRow.Document) = Type("DocumentRef.CreditNote")
			Then
			
			TabularSectionRow.AdvanceFlag = True;
		EndIf;
		
	EndIf;
	
EndProcedure

&AtClient
Procedure Ext1_OperationKindOnChangeAfter(Item)
	
	SetTaxInvoiceLabelText()
	
EndProcedure

&AtServer
Procedure SetTaxInvoiceLabelText()
	
	WorkWithVAT.SetTextAboutAdvancePaymentInvoiceReceived(ThisForm);
	
EndProcedure

&AtClient
Procedure Ext1_TaxInvoiceTextClickAround(Item, StandardProcessing)
	
	StandardProcessing = False;
	ParametersFilter = New Structure("AdvanceFlag", True);
	AdvanceArray = Object.PaymentDetails.FindRows(ParametersFilter);
	
	Mode = Not (Object.OperationKind = PredefinedValue("Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund"));

	If AdvanceArray.Count() > 0 Then
		WorkWithVATClient.OpenTaxInvoice(ThisForm, Mode, True);
	Else
		CommonClientServer.MessageToUser(
			NStr("en='There are no rows with advance payments in the Payment details tab';
			     |DE='Auf der Registerkarte Zahlungsverbindungen gibt es keine Zeilen mit Vorauszahlungen.';
			     |es_ES='No hay filas con los pagos anticipados en la pestaña de los Detalles de pago';
			     |it='Non ci sono righe con anticipo pagamenti il nella scheda dettagli di pagamento';
			     |pl='Na karcie Szczegóły płatności nie ma wierszy z zaliczkami';
			     |ro='Nu există rânduri cu plăți în avans în fila Detalii plăți';
			     |ru='В табличной части Расшифровка платежа отсутствуют авансовые платежи'"));
	EndIf;
	
EndProcedure

