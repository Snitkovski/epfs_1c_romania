﻿
&Around("Filling")
Procedure Ext1_Filling(FillingData, FillingText, StandardProcessing)
	
	If TypeOf(FillingData) = Type("DocumentRef.SalesInvoice") Then
		
		If Not WorkWithVAT.GetUseTaxInvoiceForPostingVAT(FillingData.Date, FillingData.Company) Then
			
			Raise StringFunctionsClientServer.SubstituteParametersToString(
				NStr("en='Company %1 doesn''t use tax invoices at %2 (specify this option in accounting policy)';
				     |DE='Die Firma %1 verwendet keine Steuerrechnungen bei %2 (diese Option in der Bilanzierungsrichtlinie angeben)';
				     |es_ES='Empresa %1 no utiliza las facturas fiscales en %2 (especificar esta opción en la política de contabilidad)';
				     |it='L''azienda %1 non utilizza fatture fiscali a %2 (specificare questa opzione nella politica contabile)';
				     |pl='Firma %1 nie stosuje faktur VAT do %2 (określ tę opcję w zasadach rachunkowości)';
				     |ro='Compania %1 nu utilizează facturi fiscale la %2 (specificați această opțiune în politica contabilă)';
				     |ru='Организация %1 не использует счета фактуры на %2 (укажите данную опцию в учетной политике)'"),
				FillingData.Company,
				Format(FillingData.Date, "DLF=D"))
			
		EndIf;
			
		If FillingData.OperationKind = Enums.OperationTypesSalesInvoice.ZeroInvoice Then
		
			Raise NStr("en='Cannot generate a Tax invoice for an invoice with Zero invoice type.
			           |Select an invoice with another type.';
			           |DE='Eine Steuerrechnung für eine Rechnung mit dem Rechnungstyp Null kann nicht generiert werden.
			           |Wählen Sie eine Rechnung mit einem anderen Typ aus.';
			           |es_ES='No se puede generar una Factura de impuestos para una factura con el tipo de factura con importe Cero.
			           |Seleccione una factura de otro tipo.';
			           |it='Cannot generate a Tax invoice for an invoice with Zero invoice type.
			           |Select an invoice with another type.';
			           |pl='Nie można wygenerować Faktury VAT dla faktury z zerowym typem faktury.
			           |Wybierz fakturę z innym typem.';
			           |ro='Nu se poate genera o factură fiscală pentru o factură zero.<br/>
			           |Selectați o factură de alt tip...';
			           |ru='Невозможно создать счет-фактуру для счета нулевого типа.
			           |Выберите счет на оплату другого типа.'");
		
		EndIf;
		
	EndIf;
	
	FillingStrategy = New Map;
	FillingStrategy[Type("Structure")]					= "FillByStructure";
	FillingStrategy[Type("DocumentRef.SalesInvoice")]	= "FillBySalesInvoice";
	FillingStrategy[Type("DocumentRef.CreditNote")]		= "FillByCreditNote";
	FillingStrategy[Type("DocumentRef.CashReceipt")]	= "FillByCashReceipt";
	FillingStrategy[Type("DocumentRef.PaymentReceipt")]	= "FillByPaymentReceipt";
	FillingStrategy[Type("DocumentRef.PaymentExpense")]	= "FillByPaymentReceipt";
	
	ObjectFillingDrive.FillDocument(ThisObject, FillingData, FillingStrategy);
	
EndProcedure

&Around("FillAdvancePayment")
Procedure Ext1_FillAdvancePayment(FillingData)
 
	If Not ValueIsFilled(FillingData)
		OR Not Common.RefTypeValue(FillingData) Then
		Return;
	EndIf;
	
	FillPropertyValues(ThisObject, FillingData,, "Number, Date");
	
	Currency = FillingData.CashCurrency;
	//OperationKind = Enums.OperationTypesTaxInvoiceIssued.AdvancePayment;
	
	If TypeOf(FillingData) = Type("DocumentRef.PaymentExpense") Then
		OperationKind = Enums.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn;
	else
		OperationKind = Enums.OperationTypesTaxInvoiceIssued.AdvancePayment;
	EndIf;
	
	//BasisDocuments.Clear();
	//NewRow = BasisDocuments.Add();
	//NewRow.BasisDocument = FillingData;
	
	If TypeOf(FillingData) = Type("DocumentRef.PaymentExpense") Then
		For Each TabLine In FillingData.PaymentDetails Do
			
			NewRow = BasisDocuments.Add();
            //NewRow.BasisDocument = TabLine.Document;
            NewRow.BasisDocument = FillingData;
			FillDocumentAmounts(NewRow);
			
		EndDo;
	Else
		BasisDocuments.Clear();
		NewRow = BasisDocuments.Add();
		NewRow.BasisDocument = FillingData;
		FillDocumentAmounts(NewRow);
	EndIf;
	
EndProcedure

&Around("FillDocumentAmounts")
Procedure Ext1_FillDocumentAmounts(NewRow)
	
	Query = New Query;
	
	If OperationKind = Enums.OperationTypesTaxInvoiceIssued.Sale Then
		Query.Text =
		"SELECT ALLOWED
		|	SalesInvoiceInventory.Ref AS BasisDocument,
		|	SUM(SalesInvoiceInventory.VATAmount) AS VATAmount,
		|	SUM(SalesInvoiceInventory.Total) AS Amount
		|FROM
		|	Document.SalesInvoice.Inventory AS SalesInvoiceInventory
		|WHERE
		|	SalesInvoiceInventory.Ref IN(&Documents)
		|
		|GROUP BY
		|	SalesInvoiceInventory.Ref";
	//ElsIf OperationKind = Enums.OperationTypesTaxInvoiceIssued.AdvancePayment Then
	ElsIf OperationKind = Enums.OperationTypesTaxInvoiceIssued.AdvancePayment
		OR OperationKind = Enums.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn Then
		Query.Text =
		"SELECT ALLOWED
		|	AdvancePayment.Ref AS Ref,
		|	SUM(AdvancePayment.VATAmount) AS VATAmount,
		|	SUM(AdvancePayment.PaymentAmount) AS Amount
		|FROM
		|	Document.CashReceipt.PaymentDetails AS AdvancePayment
		|WHERE
		|	AdvancePayment.Ref IN(&Documents)
		|	AND AdvancePayment.AdvanceFlag
		|
		|GROUP BY
		|	AdvancePayment.Ref
		|
		|UNION ALL
		|
		|SELECT
		|	PaymentReceipt.Ref,
		|	SUM(PaymentReceipt.VATAmount),
		|	SUM(PaymentReceipt.PaymentAmount)
		|FROM
		|	Document.PaymentReceipt.PaymentDetails AS PaymentReceipt
		|WHERE
		|	PaymentReceipt.Ref IN(&Documents)
		|	AND PaymentReceipt.AdvanceFlag
		|
		|GROUP BY
		|	PaymentReceipt.Ref";
	Else
		Query.Text =
		"SELECT ALLOWED
		|	CreditNote.VATAmount AS VATAmount,
		|	CreditNote.DocumentAmount AS Amount
		|FROM
		|	Document.CreditNote AS CreditNote
		|WHERE
		|	CreditNote.Ref IN(&Documents)";
	EndIf;
	
	Query.SetParameter("Documents", NewRow.BasisDocument);
	Selection = Query.Execute().Select();
	
	While Selection.Next() Do
		FillPropertyValues(NewRow, Selection);
	EndDo;
	
EndProcedure

&Around("FillCheckProcessing")
Procedure Ext1_FillCheckProcessing(Cancel, CheckedAttributes)
    
	CheckDate = ?(ValueIsFilled(DateOfSupply), DateOfSupply, Date);
    If OperationKind = Enums.OperationTypesTaxInvoiceIssued.AdvancePayment OR
        OperationKind = Enums.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn Then
		WorkWithVATServerCall.CheckForAdvancePaymentInvoiceUse(CheckDate, Company, Cancel);
	Else
		WorkWithVATServerCall.CheckForTaxInvoiceUse(CheckDate, Company, Cancel);
	EndIf;
	
	BasisCount = BasisDocuments.Count();
	
	If BasisCount = 0 Then
		MessageText = NStr("en = 'No base documents are available.'");
		CommonClientServer.MessageToUser(MessageText, , "BasisDocuments", , Cancel);
	EndIf;
	
	DocumentsNotPosted = False;
	
	If BasisDocuments.Count() > 0 Then
		
		Query = New Query("SELECT ALLOWED
		|	BasisDocuments.BasisDocument.Posted AS BasisDocumentPosted
		|FROM
		|	Document.TaxInvoiceIssued.BasisDocuments AS BasisDocuments
		|WHERE
		|	BasisDocuments.Ref = &Ref
		|");
		Query.SetParameter("Ref", Ref);
		SetPrivilegedMode(True);
		ResultSelection = Query.Execute().Select();
		SetPrivilegedMode(False);
		
		While ResultSelection.Next() Do
			
			If Not ResultSelection.BasisDocumentPosted Then
				DocumentsNotPosted = True;
				Break;
			EndIf;
			
		EndDo;
		
	EndIf;
	
	If DocumentsNotPosted Then
		If BasisCount > 1 Then
			MessageText = NStr("en = 'Please post all of the base documents of the tax invoice.'");
		Else
			MessageText = NStr("en = 'Please post the base document of the tax invoice.'");
		EndIf;
		CommonClientServer.MessageToUser(MessageText, , "BasisDocuments", , Cancel);
	EndIf;
	
	For Each Row In BasisDocuments Do
		
		FilterParameters = New Structure;
		FilterParameters.Insert("BasisDocument", Row.BasisDocument);
		
		SearchArray = BasisDocuments.FindRows(FilterParameters);
		
		For Each ArrayItem In SearchArray Do
			If ArrayItem.LineNumber <> Row.LineNumber Then
				CommonClientServer.MessageToUser(
					StringFunctionsClientServer.SubstituteParametersToString(
						NStr("en = 'The document %1 already exists in the list.'"),
						Row.BasisDocument),,
					"BasisDocuments",,
					Cancel);
				Break;
			EndIf;
		EndDo;
		
	EndDo;
    
EndProcedure

