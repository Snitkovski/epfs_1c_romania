﻿
&Around("FillInitializationParameters")
Procedure Ext1_FillInitializationParameters(DocumentRef, StructureAdditionalProperties)

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;
	Query.SetParameter("Ref"                  , DocumentRef);
	Query.SetParameter("PointInTime"          , New Boundary(StructureAdditionalProperties.ForPosting.PointInTime, BoundaryType.Including));
	Query.SetParameter("PresentationCurrency" , StructureAdditionalProperties.ForPosting.PresentationCurrency);
	Query.SetParameter("CashCurrency"         , DocumentRef.Currency);
	Query.SetParameter("Company"              , StructureAdditionalProperties.ForPosting.Company);
	Query.SetParameter("ExchangeRateMethod"   ,	StructureAdditionalProperties.ForPosting.ExchangeRateMethod);
	
	OperationKind = Common.ObjectAttributeValue(DocumentRef,"OperationKind");
	Query.SetParameter("AdvanceStorno",?(OperationKind=Enums.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn,-1,1));

	Query.Text =
	"SELECT
	|	TaxInvoiceIssuedHeader.Ref AS Ref,
	|	TaxInvoiceIssuedHeader.Date AS Date,
	|	TaxInvoiceIssuedHeader.Number AS Number,
	|	TaxInvoiceIssuedHeader.Company AS Company,
	|	&PresentationCurrency AS PresentationCurrency,
	|	TaxInvoiceIssuedHeader.Counterparty AS Counterparty,
	|	TaxInvoiceIssuedHeader.Currency AS Currency,
	|	TaxInvoiceIssuedHeader.Date AS Period,
	|	TaxInvoiceIssuedHeader.Department AS Department,
	|	TaxInvoiceIssuedHeader.Responsible AS Responsible,
	|	TaxInvoiceIssuedHeader.OperationKind AS OperationKind
	|INTO TaxInvoiceIssuedHeader
	|FROM
	|	Document.TaxInvoiceIssued AS TaxInvoiceIssuedHeader
	|WHERE
	|	TaxInvoiceIssuedHeader.Ref = &Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	BasisDocuments.BasisDocument AS BasisDocument,
	|	Header.OperationKind AS OperationKind
	|INTO BasisDocuments
	|FROM
	|	TaxInvoiceIssuedHeader AS Header
	|		INNER JOIN Document.TaxInvoiceIssued.BasisDocuments AS BasisDocuments
	|		ON Header.Ref = BasisDocuments.Ref
	|
	|INDEX BY
	|	BasisDocument
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	BasisDocuments.BasisDocument AS BasisDocument,
	|	BasisDocuments.OperationKind AS OperationKind,
	|	CreditNoteHeader.IncludeVATInPrice AS IncludeVATInPrice,
	|	CreditNoteHeader.BasisDocument AS SourceDocument,
	|	CreditNoteHeader.VATRate AS VATRate,
	|	CreditNoteHeader.DocumentCurrency AS DocumentCurrency,
	|	CreditNoteHeader.VATTaxation AS VATTaxation,
	|	CreditNoteHeader.Date AS Date,
	|	CreditNoteHeader.VATAmount AS VATAmount,
	|	CreditNoteHeader.DocumentAmount AS DocumentAmount
	|INTO BasisDocumentsCreditNotes
	|FROM
	|	BasisDocuments AS BasisDocuments
	|		INNER JOIN Document.CreditNote AS CreditNoteHeader
	|		ON BasisDocuments.BasisDocument = CreditNoteHeader.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ExchangeRate.Currency AS Currency,
	|	ExchangeRate.Rate AS ExchangeRate,
	|	ExchangeRate.Repetition AS Multiplicity
	|INTO TemporaryTableExchangeRatesSliceLatest
	|FROM
	|	InformationRegister.ExchangeRate.SliceLast(
	|			&PointInTime,
	|			Currency IN (&PresentationCurrency, &CashCurrency)
	|				AND Company = &Company) AS ExchangeRate
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	BasisDocuments.BasisDocument AS BasisDocument,
	|	FALSE AS IncludeVATInPrice,
	|	Payments.VATRate AS VATRate,
	|	CashReceiptHeader.CashCurrency AS CashCurrency,
	|	CashReceiptHeader.Date AS Date,
	|	CashReceiptHeader.Company AS Company,
	|	CashReceiptHeader.CompanyVATNumber AS CompanyVATNumber,
	|	&PresentationCurrency AS PresentationCurrency,
	|	CashReceiptHeader.Counterparty AS Counterparty,
	|	CAST(Payments.VATAmount * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
//	|		END AS NUMBER(15, 2)) AS VATAmount,
	|		END AS NUMBER(15, 2)) * &AdvanceStorno AS VATAmount,
	|	CAST((Payments.PaymentAmount - Payments.VATAmount) * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
//	|		END AS NUMBER(15, 2)) AS PaymentAmount,
	|		END AS NUMBER(15, 2)) * &AdvanceStorno AS PaymentAmount,
	|	BasisDocuments.OperationKind AS OperationKind
	|INTO BasisDocumentsCashReceipt
	|FROM
	|	BasisDocuments AS BasisDocuments
	|		INNER JOIN Document.CashReceipt.PaymentDetails AS Payments
	|		ON BasisDocuments.BasisDocument = Payments.Ref
	|		INNER JOIN Document.CashReceipt AS CashReceiptHeader
	|		ON BasisDocuments.BasisDocument = CashReceiptHeader.Ref
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS PC_ExchangeRates
	|		ON (PC_ExchangeRates.Currency = &PresentationCurrency)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS DC_ExchangeRates
	|		ON (DC_ExchangeRates.Currency = &CashCurrency)
	|WHERE
//	|	BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	|	(BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	|		OR BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn))
	|	AND Payments.AdvanceFlag
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	BasisDocuments.BasisDocument AS BasisDocument,
	|	FALSE AS IncludeVATInPrice,
	|	Payments.VATRate AS VATRate,
	|	PaymentReceiptHeader.CashCurrency AS CashCurrency,
	|	PaymentReceiptHeader.Date AS Date,
	|	PaymentReceiptHeader.Company AS Company,
	|	PaymentReceiptHeader.CompanyVATNumber AS CompanyVATNumber,
	|	&PresentationCurrency AS PresentationCurrency,
	|	PaymentReceiptHeader.Counterparty AS Counterparty,
	|	CAST(Payments.VATAmount * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
//	|		END AS NUMBER(15, 2)) AS VATAmount,
	|		END AS NUMBER(15, 2)) * &AdvanceStorno AS VATAmount,
	|	CAST((Payments.PaymentAmount - Payments.VATAmount) * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
//	|		END AS NUMBER(15, 2)) AS PaymentAmount,
	|		END AS NUMBER(15, 2)) * &AdvanceStorno AS PaymentAmount,
	|	BasisDocuments.OperationKind AS OperationKind
	|INTO BasisDocumentsPaymentReceipt
	|FROM
	|	BasisDocuments AS BasisDocuments
//	|		INNER JOIN Document.PaymentReceipt.PaymentDetails AS Payments
    |       INNER JOIN Document."+?(Query.Parameters.AdvanceStorno>0,"PaymentReceipt","PaymentExpense")+".PaymentDetails AS Payments
	|		ON BasisDocuments.BasisDocument = Payments.Ref
//  |       INNER JOIN Document.PaymentReceipt AS PaymentReceiptHeader
	|		INNER JOIN Document."+?(Query.Parameters.AdvanceStorno>0,"PaymentReceipt","PaymentExpense")+" AS PaymentReceiptHeader
	|		ON BasisDocuments.BasisDocument = PaymentReceiptHeader.Ref
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS PC_ExchangeRates
	|		ON (PC_ExchangeRates.Currency = &PresentationCurrency)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS DC_ExchangeRates
	|		ON (DC_ExchangeRates.Currency = &CashCurrency)
	|WHERE
//	|	BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	|	(BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	|     OR BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn))
	|	AND Payments.AdvanceFlag
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	BasisDocuments.BasisDocument AS BasisDocument,
	|	BasisDocuments.OperationKind AS OperationKind,
	|	SalesInvoiceHeader.IncludeVATInPrice AS IncludeVATInPrice,
	|	SalesInvoiceHeader.DocumentCurrency AS DocumentCurrency,
	|	SalesInvoiceHeader.VATTaxation AS VATTaxation,
	|	SalesInvoiceHeader.Date AS Date,
	|	SalesInvoiceHeader.Company AS Company,
	|	SalesInvoiceHeader.CompanyVATNumber AS CompanyVATNumber,
	|	&PresentationCurrency AS PresentationCurrency,
	|	SalesInvoiceHeader.Counterparty AS Counterparty
	|INTO BasisDocumentsSalesInvoices
	|FROM
	|	BasisDocuments AS BasisDocuments
	|		INNER JOIN Document.SalesInvoice AS SalesInvoiceHeader
	|		ON BasisDocuments.BasisDocument = SalesInvoiceHeader.Ref
	|WHERE
	|	BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Sale)
	|
	|INDEX BY
	|	BasisDocument
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Prepayment.Ref AS Ref,
	|	Prepayment.Document AS ShipmentDocument,
	|	Prepayment.VATRate AS VATRate,
	|	SalesInvoices.DocumentCurrency AS DocumentCurrency,
	|	SalesInvoices.Date AS Date,
	|	SalesInvoices.Company AS Company,
	|	SalesInvoices.CompanyVATNumber AS CompanyVATNumber,
	|	SalesInvoices.PresentationCurrency AS PresentationCurrency,
	|	SalesInvoices.Counterparty AS Counterparty,
	|	SalesInvoices.OperationKind AS OperationKind,
	|	SUM(Prepayment.VATAmount) AS VATAmount,
	|	SUM(Prepayment.AmountExcludesVAT) AS AmountExcludesVAT
	|INTO SalesInvoicesPrepaymentVAT
	|FROM
	|	BasisDocumentsSalesInvoices AS SalesInvoices
	|		INNER JOIN Document.SalesInvoice.PrepaymentVAT AS Prepayment
	|		ON SalesInvoices.BasisDocument = Prepayment.Ref
	|WHERE
	|	SalesInvoices.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Sale)
	|
	|GROUP BY
	|	Prepayment.Ref,
	|	Prepayment.Document,
	|	Prepayment.VATRate,
	|	SalesInvoices.DocumentCurrency,
	|	SalesInvoices.Date,
	|	SalesInvoices.Company,
	|	SalesInvoices.CompanyVATNumber,
	|	SalesInvoices.PresentationCurrency,
	|	SalesInvoices.Counterparty,
	|	SalesInvoices.OperationKind
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Inventory.Ref AS BasisRef,
	|	Inventory.VATRate AS VATRate,
	|	SalesInvoices.DocumentCurrency AS DocumentCurrency,
	|	SalesInvoices.Date AS Date,
	|	CAST(Inventory.VATAmount * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2)) AS VATAmount,
	|	CAST(CASE
	|			WHEN SalesInvoices.IncludeVATInPrice
	|				THEN Inventory.Total
	|			ELSE Inventory.Total - Inventory.VATAmount
	|		END * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2)) AS AmountExcludesVAT,
	|	TaxInvoiceIssuedHeader.Company AS Company,
	|	TaxInvoiceIssuedHeader.PresentationCurrency AS PresentationCurrency,
	|	TaxInvoiceIssuedHeader.Counterparty AS Customer,
	|	CASE
	|		WHEN SalesInvoices.VATTaxation = VALUE(Enum.VATTaxationTypes.ForExport)
	|				OR SalesInvoices.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN VALUE(Enum.VATOperationTypes.Export)
	|		ELSE VALUE(Enum.VATOperationTypes.Sales)
	|	END AS OperationType,
	|	CatalogProducts.ProductsType AS ProductType,
	|	TaxInvoiceIssuedHeader.Period AS Period
	|INTO BasisDocumentsData
	|FROM
	|	BasisDocumentsSalesInvoices AS SalesInvoices
	|		INNER JOIN Document.SalesInvoice.Inventory AS Inventory
	|		ON SalesInvoices.BasisDocument = Inventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (Inventory.Products = CatalogProducts.Ref)
	|		INNER JOIN TaxInvoiceIssuedHeader AS TaxInvoiceIssuedHeader
	|		ON (TRUE)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS PC_ExchangeRates
	|		ON (PC_ExchangeRates.Currency = &PresentationCurrency)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS DC_ExchangeRates
	|		ON (DC_ExchangeRates.Currency = &CashCurrency)
	|
	|UNION ALL
	|
	|SELECT
	|	CreditNoteInventory.SalesDocument,
	|	CreditNoteInventory.VATRate,
	|	CreditNoteHeader.DocumentCurrency,
	|	CreditNoteHeader.Date,
	|	-(CAST(CreditNoteInventory.VATAmount * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2))),
	|	-(CAST((CreditNoteInventory.Total - CreditNoteInventory.VATAmount) * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2))),
	|	TaxInvoiceIssuedHeader.Company,
	|	TaxInvoiceIssuedHeader.PresentationCurrency,
	|	TaxInvoiceIssuedHeader.Counterparty,
	|	CASE
	|		WHEN CreditNoteHeader.VATTaxation = VALUE(Enum.VATTaxationTypes.ForExport)
	|				OR CreditNoteHeader.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN VALUE(Enum.VATOperationTypes.Export)
	|		ELSE VALUE(Enum.VATOperationTypes.SalesReturn)
	|	END,
	|	CatalogProducts.ProductsType,
	|	TaxInvoiceIssuedHeader.Period
	|FROM
	|	BasisDocumentsCreditNotes AS CreditNoteHeader
	|		INNER JOIN Document.CreditNote.Inventory AS CreditNoteInventory
	|		ON CreditNoteHeader.BasisDocument = CreditNoteInventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (CreditNoteInventory.Products = CatalogProducts.Ref)
	|		INNER JOIN TaxInvoiceIssuedHeader AS TaxInvoiceIssuedHeader
	|		ON (TRUE)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS PC_ExchangeRates
	|		ON (PC_ExchangeRates.Currency = &PresentationCurrency)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS DC_ExchangeRates
	|		ON (DC_ExchangeRates.Currency = &CashCurrency)
	|WHERE
	|	CreditNoteHeader.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.SalesReturn)
	|	AND (CreditNoteInventory.VATAmount <> 0
	|			OR CreditNoteInventory.Amount <> 0)
	|
	|UNION ALL
	|
	|SELECT
	|	CreditNote.BasisDocument,
	|	CreditNote.VATRate,
	|	CreditNote.DocumentCurrency,
	|	CreditNote.Date,
	|	-(CAST(CreditNote.VATAmount * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2))),
	|	-(CAST((CreditNote.DocumentAmount - CreditNote.VATAmount) * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2))),
	|	TaxInvoiceIssuedHeader.Company,
	|	TaxInvoiceIssuedHeader.PresentationCurrency,
	|	TaxInvoiceIssuedHeader.Counterparty,
	|	CASE
	|		WHEN CreditNote.VATTaxation = VALUE(Enum.VATTaxationTypes.ForExport)
	|				OR CreditNote.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN VALUE(Enum.VATOperationTypes.Export)
	|		ELSE VALUE(Enum.VATOperationTypes.OtherAdjustments)
	|	END,
	|	VALUE(Enum.ProductsTypes.EmptyRef),
	|	TaxInvoiceIssuedHeader.Period
	|FROM
	|	BasisDocumentsCreditNotes AS CreditNote
	|		INNER JOIN TaxInvoiceIssuedHeader AS TaxInvoiceIssuedHeader
	|		ON (TRUE)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS PC_ExchangeRates
	|		ON (PC_ExchangeRates.Currency = &PresentationCurrency)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS DC_ExchangeRates
	|		ON (DC_ExchangeRates.Currency = &CashCurrency)
	|WHERE
	|	CreditNote.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Adjustments)
	|
	|UNION ALL
	|
	|SELECT
	|	CreditNote.BasisDocument,
	|	CreditNote.VATRate,
	|	CreditNote.DocumentCurrency,
	|	CreditNote.Date,
	|	-(CAST(CreditNote.VATAmount * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2))),
	|	-(CAST((CreditNote.DocumentAmount - CreditNote.VATAmount) * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2))),
	|	TaxInvoiceIssuedHeader.Company,
	|	TaxInvoiceIssuedHeader.PresentationCurrency,
	|	TaxInvoiceIssuedHeader.Counterparty,
	|	CASE
	|		WHEN CreditNote.VATTaxation = VALUE(Enum.VATTaxationTypes.ForExport)
	|				OR CreditNote.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN VALUE(Enum.VATOperationTypes.Export)
	|		ELSE VALUE(Enum.VATOperationTypes.DiscountAllowed)
	|	END,
	|	VALUE(Enum.ProductsTypes.EmptyRef),
	|	TaxInvoiceIssuedHeader.Period
	|FROM
	|	BasisDocumentsCreditNotes AS CreditNote
	|		INNER JOIN TaxInvoiceIssuedHeader AS TaxInvoiceIssuedHeader
	|		ON (TRUE)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS PC_ExchangeRates
	|		ON (PC_ExchangeRates.Currency = &PresentationCurrency)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS DC_ExchangeRates
	|		ON (DC_ExchangeRates.Currency = &CashCurrency)
	|WHERE
	|	CreditNote.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.DiscountAllowed)";

    // begin Drive.FullVersion
	Query.Text = Query.Text + DriveClientServer.GetQueryDelimeter() +
	"SELECT
	|	BasisDocuments.BasisDocument AS BasisDocument,
	|	BasisDocuments.OperationKind AS OperationKind,
	|	SubcontractorInvoiceIssuedHeader.IncludeVATInPrice AS IncludeVATInPrice,
	|	SubcontractorInvoiceIssuedHeader.DocumentCurrency AS DocumentCurrency,
	|	SubcontractorInvoiceIssuedHeader.VATTaxation AS VATTaxation,
	|	SubcontractorInvoiceIssuedHeader.Date AS Date,
	|	SubcontractorInvoiceIssuedHeader.Company AS Company,
	|	SubcontractorInvoiceIssuedHeader.CompanyVATNumber AS CompanyVATNumber,
	|	&PresentationCurrency AS PresentationCurrency,
	|	SubcontractorInvoiceIssuedHeader.Counterparty AS Counterparty
	|INTO BasisDocumentsSubcontractorInvoicesIssued
	|FROM
	|	BasisDocuments AS BasisDocuments
	|		INNER JOIN Document.SubcontractorInvoiceIssued AS SubcontractorInvoiceIssuedHeader
	|		ON BasisDocuments.BasisDocument = SubcontractorInvoiceIssuedHeader.Ref
	|WHERE
	|	BasisDocuments.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Sale)
	|
	|INDEX BY
	|	BasisDocument
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Prepayment.Ref AS Ref,
	|	Prepayment.Document AS ShipmentDocument,
	|	Prepayment.VATRate AS VATRate,
	|	SubcontractorInvoicesIssued.DocumentCurrency AS DocumentCurrency,
	|	SubcontractorInvoicesIssued.Date AS Date,
	|	SubcontractorInvoicesIssued.Company AS Company,
	|	SubcontractorInvoicesIssued.CompanyVATNumber AS CompanyVATNumber,
	|	SubcontractorInvoicesIssued.PresentationCurrency AS PresentationCurrency,
	|	SubcontractorInvoicesIssued.Counterparty AS Counterparty,
	|	SubcontractorInvoicesIssued.OperationKind AS OperationKind,
	|	SUM(Prepayment.VATAmount) AS VATAmount,
	|	SUM(Prepayment.AmountExcludesVAT) AS AmountExcludesVAT
	|INTO SubcontractorInvoicesIssuedPrepaymentVAT
	|FROM
	|	BasisDocumentsSubcontractorInvoicesIssued AS SubcontractorInvoicesIssued
	|		INNER JOIN Document.SubcontractorInvoiceIssued.PrepaymentVAT AS Prepayment
	|		ON SubcontractorInvoicesIssued.BasisDocument = Prepayment.Ref
	|WHERE
	|	SubcontractorInvoicesIssued.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Sale)
	|
	|GROUP BY
	|	Prepayment.Ref,
	|	Prepayment.Document,
	|	Prepayment.VATRate,
	|	SubcontractorInvoicesIssued.DocumentCurrency,
	|	SubcontractorInvoicesIssued.Date,
	|	SubcontractorInvoicesIssued.Company,
	|	SubcontractorInvoicesIssued.CompanyVATNumber,
	|	SubcontractorInvoicesIssued.PresentationCurrency,
	|	SubcontractorInvoicesIssued.Counterparty,
	|	SubcontractorInvoicesIssued.OperationKind
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	SubcontractorInvoiceIssuedProducts.Ref AS BasisRef,
	|	SubcontractorInvoiceIssuedProducts.VATRate AS VATRate,
	|	SubcontractorInvoicesIssued.DocumentCurrency AS DocumentCurrency,
	|	SubcontractorInvoicesIssued.Date AS Date,
	|	CAST(SubcontractorInvoiceIssuedProducts.VATAmount * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2)) AS VATAmount,
	|	CAST(CASE
	|			WHEN SubcontractorInvoicesIssued.IncludeVATInPrice
	|				THEN SubcontractorInvoiceIssuedProducts.Total
	|			ELSE SubcontractorInvoiceIssuedProducts.Total - SubcontractorInvoiceIssuedProducts.VATAmount
	|		END * CASE
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Multiplier)
	|				THEN DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity)
	|			WHEN &ExchangeRateMethod = VALUE(Enum.ExchangeRateMethods.Divisor)
	|				THEN 1 / (DC_ExchangeRates.ExchangeRate * PC_ExchangeRates.Multiplicity / (PC_ExchangeRates.ExchangeRate * DC_ExchangeRates.Multiplicity))
	|		END AS NUMBER(15, 2)) AS AmountExcludesVAT,
	|	TaxInvoiceIssuedHeader.Company AS Company,
	|	TaxInvoiceIssuedHeader.PresentationCurrency AS PresentationCurrency,
	|	TaxInvoiceIssuedHeader.Counterparty AS Customer,
	|	CASE
	|		WHEN SubcontractorInvoicesIssued.VATTaxation = VALUE(Enum.VATTaxationTypes.ForExport)
	|				OR SubcontractorInvoicesIssued.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN VALUE(Enum.VATOperationTypes.Export)
	|		ELSE VALUE(Enum.VATOperationTypes.Sales)
	|	END AS OperationType,
	|	CatalogProducts.ProductsType AS ProductType,
	|	TaxInvoiceIssuedHeader.Period AS Period
	|INTO AdditionBasisDocumentsData
	|FROM
	|	BasisDocumentsSubcontractorInvoicesIssued AS SubcontractorInvoicesIssued
	|		INNER JOIN Document.SubcontractorInvoiceIssued.Products AS SubcontractorInvoiceIssuedProducts
	|		ON SubcontractorInvoicesIssued.BasisDocument = SubcontractorInvoiceIssuedProducts.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (SubcontractorInvoiceIssuedProducts.Products = CatalogProducts.Ref)
	|		INNER JOIN TaxInvoiceIssuedHeader AS TaxInvoiceIssuedHeader
	|		ON (TRUE)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS PC_ExchangeRates
	|		ON (PC_ExchangeRates.Currency = &PresentationCurrency)
	|		LEFT JOIN TemporaryTableExchangeRatesSliceLatest AS DC_ExchangeRates
	|		ON (DC_ExchangeRates.Currency = &CashCurrency)";
	// end Drive.FullVersion 
    
	Query.ExecuteBatch();

	GenerateTableVATOutput(DocumentRef, StructureAdditionalProperties);
	GenerateTableAccountingJournalEntries(DocumentRef, StructureAdditionalProperties);

EndProcedure

&ChangeAndValidate("GenerateTableAccountingJournalEntries")
Procedure Ext1_GenerateTableAccountingJournalEntries(DocumentRefTaxInvoiceIssued, StructureAdditionalProperties)

	Query = New Query;
	Query.TempTablesManager = StructureAdditionalProperties.ForPosting.StructureTemporaryTables.TempTablesManager;
	
	MainLanguageCode = CommonClientServer.DefaultLanguageCode();
	
	Query.SetParameter("Company",					StructureAdditionalProperties.ForPosting.Company);
	Query.SetParameter("Date",						StructureAdditionalProperties.ForPosting.Date);
	
	//  SAM  {
	Query.SetParameter("ContentVATOnAdvance",		QuerySetParameterContentVATOnAdvance(MainLanguageCode));
	Query.SetParameter("ContentVATRevenue",			QuerySetParameterContentVATRevenue(MainLanguageCode));
	//  }  SAM 
	
	Query.SetParameter("VATAdvancesFromCustomers",	Catalogs.DefaultGLAccounts.GetDefaultGLAccount("VATAdvancesFromCustomers"));
	Query.SetParameter("VATOutput",					Catalogs.DefaultGLAccounts.GetDefaultGLAccount("VATOutput"));
	
	Query.SetParameter("PostAdvancePaymentsBySourceDocuments", StructureAdditionalProperties.AccountingPolicy.PostAdvancePaymentsBySourceDocuments);
	Query.SetParameter("PostVATEntriesBySourceDocuments", StructureAdditionalProperties.AccountingPolicy.PostVATEntriesBySourceDocuments);
	
	Query.Text =
	"SELECT
	|	DocumentTable.Date AS Period,
	|	DocumentTable.Company AS Company,
	|	VALUE(Catalog.PlanningPeriods.Actual) AS PlanningPeriod,
	|	&VATAdvancesFromCustomers AS AccountDr,
	|	&VATOutput AS AccountCr,
	|	UNDEFINED AS CurrencyDr,
	|	UNDEFINED AS CurrencyCr,
	|	0 AS AmountCurDr,
	|	0 AS AmountCurCr,
	|	SUM(DocumentTable.VATAmount) AS Amount,
	|	&ContentVATOnAdvance AS Content,
	|	FALSE AS OfflineRecord
	|FROM
	|	BasisDocumentsCashReceipt AS DocumentTable
	|WHERE
	#Delete
	|	DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	#EndDelete
	#Insert
	|	(DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	|    OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn))
	#EndInsert
	|	AND NOT &PostAdvancePaymentsBySourceDocuments
	|
	|GROUP BY
	|	DocumentTable.Date,
	|	DocumentTable.Company
	|
	|UNION ALL
	|
	|SELECT
	|	DocumentTable.Date,
	|	DocumentTable.Company,
	|	VALUE(Catalog.PlanningPeriods.Actual),
	|	&VATAdvancesFromCustomers,
	|	&VATOutput,
	|	UNDEFINED,
	|	UNDEFINED,
	|	0,
	|	0,
	|	SUM(DocumentTable.VATAmount),
	|	&ContentVATOnAdvance,
	|	FALSE
	|FROM
	|	BasisDocumentsPaymentReceipt AS DocumentTable
	|WHERE
	#Delete
	|	DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	#EndDelete
	#Insert
	|	(DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
	|     OR DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn))
	#EndInsert
	|	AND NOT &PostAdvancePaymentsBySourceDocuments
	|
	|GROUP BY
	|	DocumentTable.Date,
	|	DocumentTable.Company
	|
	|UNION ALL
	|
	|SELECT
	|	DocumentTable.Date,
	|	DocumentTable.Company,
	|	VALUE(Catalog.PlanningPeriods.Actual),
	|	&VATOutput,
	|	&VATAdvancesFromCustomers,
	|	UNDEFINED,
	|	UNDEFINED,
	|	0,
	|	0,
	|	SUM(DocumentTable.VATAmount),
	|	&ContentVATRevenue,
	|	FALSE
	|FROM
	|	SalesInvoicesPrepaymentVAT AS DocumentTable
	|WHERE
	|	DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Sale)
	|	AND NOT &PostVATEntriesBySourceDocuments
	|
	|GROUP BY
	|	DocumentTable.Date,
	|	DocumentTable.Company";
	
	// begin Drive.FullVersion
	
	Query.Text = Query.Text + DriveClientServer.GetQueryUnion() +
	"SELECT
	|	DocumentTable.Date,
	|	DocumentTable.Company,
	|	VALUE(Catalog.PlanningPeriods.Actual),
	|	&VATOutput,
	|	&VATAdvancesFromCustomers,
	|	UNDEFINED,
	|	UNDEFINED,
	|	0,
	|	0,
	|	SUM(DocumentTable.VATAmount),
	|	&ContentVATRevenue,
	|	FALSE
	|FROM
	|	SubcontractorInvoicesIssuedPrepaymentVAT AS DocumentTable
	|WHERE
	|	DocumentTable.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.Sale)
	|	AND NOT &PostVATEntriesBySourceDocuments
	|
	|GROUP BY
	|	DocumentTable.Date,
	|	DocumentTable.Company";
	
	// end Drive.FullVersion 
	
	QueryResult = Query.Execute();
	
	StructureAdditionalProperties.TableForRegisterRecords.Insert("TableAccountingJournalEntries", QueryResult.Unload());

EndProcedure
