﻿
&AtClient
&ChangeAndValidate("BasisDocumentsBasisDocumentStartChoice")
Procedure Ext1_BasisDocumentsBasisDocumentStartChoice(Item, ChoiceData, StandardProcessing)

    StandardProcessing = False;

    TabularSectionRow = Items.BasisDocuments.CurrentData;

    StructureFilter = New Structure("Counterparty, Company, Currency",
    Object.Counterparty, Object.Company, Object.Currency);

    ParameterStructure = New Structure("Filter, DocumentType", StructureFilter, TypeOf(Object.Ref));

    If Object.OperationKind = PredefinedValue("Enum.OperationTypesTaxInvoiceIssued.AdvancePayment") Then
        ParameterStructure.Insert("ThisIsAdvancePaymentsIssued", True);
        #Insert
    ElsIf Object.OperationKind = PredefinedValue("Enum.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn") Then
        ParameterStructure.Insert("ThisIsAdvancePaymentsRefund", True);
        #EndInsert
    Else
        ParameterStructure.Insert("ThisIsTaxInvoiceIssued", True);
    EndIf;
    
    OpenForm("CommonForm.SelectDocumentOfSettlements", ParameterStructure, Item);

EndProcedure

