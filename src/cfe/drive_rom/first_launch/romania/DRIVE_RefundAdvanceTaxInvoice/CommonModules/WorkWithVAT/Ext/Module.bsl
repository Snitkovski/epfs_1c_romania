﻿
&Around("SetTextAboutAdvancePaymentInvoice")
Procedure Ext1_SetTextAboutAdvancePaymentInvoice(DocumentForm, Received)
	
	AdvancePaymentInvoiceFound = GetSubordinateTaxInvoice(DocumentForm.Object.Ref, Received, True);
	
	If ValueIsFilled(AdvancePaymentInvoiceFound) Then
		DocumentForm.TaxInvoiceText = WorkWithVATClientServer.AdvancePaymentInvoicePresentation(AdvancePaymentInvoiceFound.Date, AdvancePaymentInvoiceFound.Number);
	Else
		If TypeOf(DocumentForm.Object.Ref) = Type("DocumentRef.PaymentExpense") Then
			
			DocumentForm.TaxInvoiceText = NStr("en='Create Advance refund invoice';
												|ro='Creați Factură storno avans';
												|ru='Создать счет-фактуру (возврат аванса)'");
			
		Else
			
			DocumentForm.TaxInvoiceText = NStr("en='Create Advance payment invoice';
												|DE='Vorauszahlungsrechnung erstellen';
												|es_ES='Crear la Factura de pago anticipado';
												|it='Creare una fattura di pagamento anticipato';
												|pl='Utwórz fakturę zaliczkową';
												|ro='Creați Factură avans';
												|ru='Создать счет-фактуру (аванс)'");
			
		EndIf;
		
	EndIf;
	
EndProcedure

&Around("SetTextAboutAdvancePaymentInvoiceReceived")
Procedure Ext1_SetTextAboutAdvancePaymentInvoiceReceived(DocumentForm)
	
	NotAdvanceRefund = Not (DocumentForm.Object.OperationKind = Enums.OperationTypesPaymentExpense.Ext1_AdvanceRefund);
	SetTextAboutAdvancePaymentInvoice(DocumentForm, NotAdvanceRefund)
	
EndProcedure

&Around("GetSubordinateTaxInvoice")
Function Ext1_GetSubordinateTaxInvoice(BasisDocument, Received, Advance)
		If NOT ValueIsFilled(BasisDocument) Then
		Return Undefined;
	ElsIf NOT Advance AND NOT GetUseTaxInvoiceForPostingVAT(BasisDocument.Date, BasisDocument.Company) Then
		Return Undefined;
	ElsIf Advance AND GetPostAdvancePaymentsBySourceDocuments(BasisDocument.Date, BasisDocument.Company) Then
		Return Undefined;
	EndIf;
	
	If Received Then
		
		QueryText =
		"SELECT ALLOWED
		|	TaxInvoiceBasisDocuments.Ref AS Ref
		|INTO TaxInvoiceBasisDocuments
		|FROM
		|	Document.TaxInvoiceReceived.BasisDocuments AS TaxInvoiceBasisDocuments
		|WHERE
		|	TaxInvoiceBasisDocuments.BasisDocument = &BasisDocument
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|SELECT ALLOWED
		|	Header.Ref AS Ref,
		|	Header.Number AS Number,
		|	Header.Date AS Date
		|FROM
		|	TaxInvoiceBasisDocuments AS TaxInvoiceBasisDocuments
		|		INNER JOIN Document.TaxInvoiceReceived AS Header
		|		ON TaxInvoiceBasisDocuments.Ref = Header.Ref
		|WHERE
		|	NOT Header.DeletionMark"
		
	ElsIf TypeOf(BasisDocument) = Type("DocumentRef.PaymentExpense") Then
		
		QueryText =
		"SELECT
        |   TaxInvoiceIssuedBasisDocuments.Ref AS Ref,
        |   TaxInvoiceIssuedBasisDocuments.Ref.Number AS Number,
        |   TaxInvoiceIssuedBasisDocuments.Ref.Date AS Date
        |FROM
        |   Document.PaymentExpense.PaymentDetails AS PaymentExpensePaymentDetails
        |       INNER JOIN Document.TaxInvoiceIssued.BasisDocuments AS TaxInvoiceIssuedBasisDocuments
        |       ON PaymentExpensePaymentDetails.Ref = TaxInvoiceIssuedBasisDocuments.BasisDocument
        |           AND (PaymentExpensePaymentDetails.Ref = &BasisDocument)
        |           AND (TaxInvoiceIssuedBasisDocuments.Ref.OperationKind = &OperationKind)
        |           AND (NOT TaxInvoiceIssuedBasisDocuments.Ref.DeletionMark)
        |
        |GROUP BY
        |   TaxInvoiceIssuedBasisDocuments.Ref,
        |   TaxInvoiceIssuedBasisDocuments.Ref.Number,
        |   TaxInvoiceIssuedBasisDocuments.Ref.Date"
		
	Else
		
		QueryText =
		"SELECT ALLOWED
		|	TaxInvoiceBasisDocuments.Ref AS Ref
		|INTO TaxInvoiceBasisDocuments
		|FROM
		|	Document.TaxInvoiceIssued.BasisDocuments AS TaxInvoiceBasisDocuments
		|WHERE
		|	TaxInvoiceBasisDocuments.BasisDocument = &BasisDocument
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|SELECT ALLOWED
		|	Header.Ref AS Ref,
		|	Header.Number AS Number,
		|	Header.Date AS Date
		|FROM
		|	TaxInvoiceBasisDocuments AS TaxInvoiceBasisDocuments
		|		INNER JOIN Document.TaxInvoiceIssued AS Header
		|		ON TaxInvoiceBasisDocuments.Ref = Header.Ref
		|WHERE
		|	NOT Header.DeletionMark"
		
	EndIf;
	
	Query = New Query(QueryText);
	Query.SetParameter("BasisDocument", BasisDocument);
	Query.SetParameter("OperationKind",Enums.OperationTypesTaxInvoiceIssued.Ext1_AdvanceReturn);
	
	Result = Undefined;
	
	Selection = Query.Execute().Select();
	If Selection.Next() Then
		Result = New Structure("Ref, Number, Date");
		FillPropertyValues(Result, Selection);
	EndIf;
	
	Return Result;

EndFunction
