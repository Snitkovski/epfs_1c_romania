﻿
&AtServer
&ChangeAndValidate("OnCreateAtServer")
Procedure Ext1_OnCreateAtServer(Cancel, StandardProcessing)

    ThisIsAccountsReceivable = Parameters.ThisIsAccountsReceivable;

    ThisIsBankStatementProcessing = Parameters.Property("ThisIsBankStatementProcessing");
    ThisIsAdvancePaymentsReceived = Parameters.Property("ThisIsAdvancePaymentsReceived");
    ThisIsAdvancePaymentsIssued = Parameters.Property("ThisIsAdvancePaymentsIssued");
    ThisIsTaxInvoiceReceived = Parameters.Property("ThisIsTaxInvoiceReceived");
    ThisIsTaxInvoiceIssued = Parameters.Property("ThisIsTaxInvoiceIssued");
    ThisIsThirdPartyPayment = Parameters.Property("ThisIsThirdPartyPayment");
    #Insert
    ThisIsAdvancePaymentsRefund = Parameters.Property("ThisIsAdvancePaymentsRefund");
    #EndInsert

    DocumentType = Parameters.DocumentType;

    If DocumentType = Type("DocumentRef.PaymentReceipt")
        OR DocumentType = Type("DocumentRef.CashReceipt")
        OR DocumentType = Type("DocumentRef.DebitNote")
        OR DocumentType = Type("DocumentRef.DirectDebit") Then

        If ThisIsAccountsReceivable Then
            List.QueryText = GetQueryTextAccountDocumentsOfAccountsReceivableReceipt();
        ElsIf ThisIsThirdPartyPayment Then
            List.QueryText = GetQueryTextAccountDocumentsOfThirdPartyPayment();
        Else
            List.QueryText = GetQueryTextDocumentsOfAccountsPayableReceipt();
        EndIf;

    Else

        If ThisIsAccountsReceivable Then
            List.QueryText = GetQueryTextAccountDocumentsOfAccountsReceivableWriteOff();
            List.Parameters.SetParameterValue("SelectCreditNote", DocumentType <> Type("DocumentRef.CreditNote"));
        ElsIf ThisIsBankStatementProcessing Then
            List.QueryText = GetQueryTextDocumentForBankStatementProcessing();
        ElsIf ThisIsAdvancePaymentsReceived Then
            List.QueryText = GetQueryTextAdvancePaymentsReceived();
        ElsIf ThisIsAdvancePaymentsIssued Then
            List.QueryText = GetQueryTextAdvancePaymentsIssued();
        ElsIf ThisIsTaxInvoiceReceived Then
            List.QueryText = GetQueryTextTaxInvoiceReceived();
        ElsIf ThisIsTaxInvoiceIssued Then
            List.QueryText = GetQueryTextTaxInvoiceIssued();
            #Insert
        ElsIf ThisIsAdvancePaymentsRefund Then
            List.QueryText = GetQueryTextAdvancePaymentsRefund();
            #EndInsert
        Else
            List.QueryText = GetQueryTextDocumentsOfAccountsPayableWriteOff();
        EndIf;

    EndIf;

    Items.Company.Visible = Not Parameters.Filter.Property("Company");
    Items.Employee.Visible = ThisIsTaxInvoiceReceived;

    If Parameters.Filter.Property("Counterparty") Then
        Items.Counterparty.Visible = True;
        List.Parameters.SetParameterValue("CounterpartyByDefault", Parameters.Filter.Counterparty);
    Else
        Items.Counterparty.Visible = False;
        List.Parameters.SetParameterValue("CounterpartyByDefault", Catalogs.Counterparties.EmptyRef());
    EndIf;

    If Parameters.Filter.Property("Contract") Then
        List.Parameters.SetParameterValue("ContractByDefault", Parameters.Filter.Contract);
    Else
        List.Parameters.SetParameterValue("ContractByDefault", Catalogs.CounterpartyContracts.EmptyRef());
    EndIf;

    If Parameters.Filter.Property("DirectDebitMandate") Then
        List.Parameters.SetParameterValue("DirectDebitMandateByDefault", Parameters.Filter.DirectDebitMandate);
    Else
        List.Parameters.SetParameterValue("DirectDebitMandateByDefault", Catalogs.DirectDebitMandates.EmptyRef());
    EndIf;

    If Parameters.Filter.Property("Currency") Then
        List.Parameters.SetParameterValue("Currency", Parameters.Filter.Currency);
    ElsIf Parameters.Filter.Property("DocumentCurrency") Then
        List.Parameters.SetParameterValue("Currency", Parameters.Filter.DocumentCurrency);
    Else
        List.Parameters.SetParameterValue("Currency", Catalogs.Currencies.EmptyRef());
    EndIf;

    //Conditional appearance
    SetConditionalAppearance();

EndProcedure

&AtServerNoContext
Function GetQueryTextAdvancePaymentsRefund()
    
	QueryText =
	"SELECT ALLOWED
	|	UNDEFINED AS Ref,
	|	DATETIME(1, 1, 1) AS Date,
	|	""000000000000"" AS Number,
	|	VALUE(Catalog.Companies.EmptyRef) AS Company,
	|	&CounterpartyByDefault AS Counterparty,
	|	&ContractByDefault AS Contract,
	|	&DirectDebitMandateByDefault AS DirectDebitMandate,
	|	0 AS Amount,
	|	&Currency AS Currency,
	|	UNDEFINED AS Type,
	|	0 AS DocumentStatus
	|WHERE
	|	FALSE
	|";
	
	If AccessRight("Read", Metadata.Documents.CashVoucher) Then
		
		QueryText = QueryText + "UNION ALL";
		
		QueryText = QueryText +
		"
		|SELECT
		|	DocumentData.Ref,
		|	DocumentData.Date,
		|	DocumentData.Number,
		|	DocumentData.Company,
		|	DocumentData.Counterparty,
		|	&ContractByDefault,
		|	&DirectDebitMandateByDefault,
		|	DocumentData.DocumentAmount,
		|	DocumentData.CashCurrency,
		|	VALUETYPE(DocumentData.Ref),
		|	CASE
		|		WHEN DocumentData.Posted
		|			THEN 1
		|		WHEN DocumentData.DeletionMark
		|			THEN 2
		|		ELSE 0
		|	END
		|FROM
		|	Document.CashVoucher AS DocumentData
		|		LEFT JOIN Document.TaxInvoiceReceived.BasisDocuments AS BasisDocuments
		|		ON DocumentData.Ref = BasisDocuments.BasisDocument
		|WHERE
		|	BasisDocuments.BasisDocument IS NULL
		|	AND DocumentData.OperationKind = VALUE(Enum.OperationTypesCashVoucher.Vendor)
		|	AND DocumentData.Posted
		|	AND DocumentData.CashCurrency = &Currency
		|";
		
	EndIf;
	
	If AccessRight("Read", Metadata.Documents.PaymentExpense) Then
		
		QueryText = QueryText + "UNION ALL";
		
		QueryText = QueryText +
		"
		|SELECT
		|	DocumentData.Ref,
		|	DocumentData.Date,
		|	DocumentData.Number,
		|	DocumentData.Company,
		|	DocumentData.Counterparty,
		|	&ContractByDefault,
		|	&DirectDebitMandateByDefault,
		|	DocumentData.DocumentAmount,
		|	DocumentData.CashCurrency,
		|	VALUETYPE(DocumentData.Ref),
		|	CASE
		|		WHEN DocumentData.Posted
		|			THEN 1
		|		WHEN DocumentData.DeletionMark
		|			THEN 2
		|		ELSE 0
		|	END
		|FROM
		|	Document.PaymentExpense AS DocumentData
		|		LEFT JOIN Document.TaxInvoiceReceived.BasisDocuments AS BasisDocuments
		|		ON DocumentData.Ref = BasisDocuments.BasisDocument
		|WHERE
		|	BasisDocuments.BasisDocument IS NULL
		|	AND DocumentData.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.Ext1_AdvanceRefund)
		|	AND DocumentData.Posted
		|	AND DocumentData.CashCurrency = &Currency
		|";
		
	EndIf;
	
	If Left(QueryText, 10) = "UNION" Then
		QueryText = Mid(QueryText, 14);
	EndIf;
	
	Return QueryText;
    
EndFunction
