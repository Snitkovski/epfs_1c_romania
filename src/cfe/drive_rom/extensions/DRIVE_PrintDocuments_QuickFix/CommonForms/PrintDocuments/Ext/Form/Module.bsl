﻿&AtServer
&Вместо ("ImportCopiesCountSettings")
Procedure Ext1_ImportCopiesCountSettings()
    
    DisplayPrintOption = DriveServer.GetFunctionalOptionValue("DisplayPrintOptionsBeforePrinting");
    
    If DisplayPrintOption And Parameters.PrintParameters.AdditionalParameters.Property("Result") Then
        
        PrintFormSettingsStructure = New Structure;

//        PrintFormSettingsStructure.Insert("Count", Parameters.PrintParameters.AdditionalParameters.Result.Copies);
        PrintFormSettingsStructure.Insert("Quantity", Parameters.PrintParameters.AdditionalParameters.Result.Copies);
//        PrintFormSettingsStructure.Insert("DefaultPosition", 0);
        PrintFormSettingsStructure.Insert("PositionByDefault", 0);
        PrintFormSettingsStructure.Insert("TemplateName", Parameters.TemplatesNames);

        SavedPrintFormsSettings = New Array;
        SavedPrintFormsSettings.Add(PrintFormSettingsStructure);
        
        RestorePrintFormsSettings(SavedPrintFormsSettings);
        
        Copies = Parameters.PrintParameters.AdditionalParameters.Result.Copies;
	Else
    	SavedPrintFormsSettings = New Array;
    	
    	UseSavedSettings = True;
    	If TypeOf(Parameters.PrintParameters) = Type("Structure") AND Parameters.PrintParameters.Property("OverrideCopiesUserSetting") Then
    		UseSavedSettings = Not Parameters.PrintParameters.OverrideCopiesUserSetting;
    	EndIf;
    	
    	If UseSavedSettings Then
    		If ValueIsFilled(Parameters.DataSource) Then
    			SettingsKey = String(Parameters.DataSource.UUID()) + "-" + Parameters.SourceParameters.CommandID;
    		Else
    			TemplatesNames = Parameters.TemplatesNames;
    			If TypeOf(TemplatesNames) = Type("Array") Then
    				TemplatesNames = StrConcat(TemplatesNames, ",");
    			EndIf;
    			
    			SettingsKey = Parameters.PrintManagerName + "-" + TemplatesNames;
    		EndIf;
    		SavedPrintFormsSettings = Common.CommonSettingsStorageLoad("PrintFormsSettings", SettingsKey, New Array);
    	EndIf;

    	RestorePrintFormsSettings(SavedPrintFormsSettings);
    	
    	If IsSetPrinting() Then
    		Copies = 1;
    	Else
    		If PrintFormsSettings.Count() > 0 Then
    			Copies = PrintFormsSettings[0].Count;
    		EndIf;
    	EndIf;
	EndIf
EndProcedure

&AtServer
&Вместо ("RestorePrintFormsSettings")
Procedure Ext1_RestorePrintFormsSettings(SavedPrintFormsSettings = Undefined)
	If SavedPrintFormsSettings = Undefined Then
		SavedPrintFormsSettings = DefaultSetSettings;
	EndIf;
	
	If SavedPrintFormsSettings = Undefined Then
		Return;
	EndIf;
	
    For Each SavedSetting In SavedPrintFormsSettings Do
        
//		FoundSettings = PrintFormsSettings.FindRows(New Structure("DefaultPosition", SavedSetting.DefaultPosition));
		FoundSettings = PrintFormsSettings.FindRows(New Structure("DefaultPosition", SavedSetting.PositionByDefault));
		
		For Each PrintFormSetting In FoundSettings Do
			RowIndex = PrintFormsSettings.IndexOf(PrintFormSetting);
			PrintFormsSettings.Move(RowIndex, PrintFormsSettings.Count()-1 - RowIndex); // Moving to the end
//            PrintFormSetting.Count = SavedSetting.Count; 
            PrintFormSetting.Count = SavedSetting.Quantity;
			PrintFormSetting.Print = PrintFormSetting.Count > 0;
		EndDo;
	EndDo;
EndProcedure
