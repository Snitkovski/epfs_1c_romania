﻿
&After("BeforeWrite")
Procedure CPT_BeforeWrite(Cancel)
    
    OwnerAttributes=Common.ObjectAttributesValues(Owner,"SupplierPriceTypes,Supplier");
    
    If OwnerAttributes.Supplier AND GetFunctionalOption("UseCounterpartiesPricesTracking") Then
        
        If Not ValueIsFilled(SupplierPriceTypes) Then
            SupplierPriceTypes=OwnerAttributes.SupplierPriceTypes;
        EndIf;
        
        PriceCurrency=Common.ObjectAttributeValue(SupplierPriceTypes,"PriceCurrency");
        
        If ValueIsFilled(PriceCurrency) AND NOT SettlementsCurrency=PriceCurrency Then
        
            SupplierPriceTypes=Catalogs.SupplierPriceTypes.FindSupplierPriceType(Owner,SettlementsCurrency);
            
            If NOT ValueIsFilled(SupplierPriceTypes) Then
                
                SupplierPriceTypes=Catalogs.SupplierPriceTypes.CreateSupplierPriceTypes(Owner,SettlementsCurrency)
                
            EndIf;
            
        EndIf;
        
        AdditionalProperties.Insert("UpdatePriceCurrency", True);
        
    Endif;
    
EndProcedure

&After("OnWrite")
Procedure CPT_OnWrite(Cancel)
    
    If AdditionalProperties.Property("UpdatePriceCurrency")
        AND AdditionalProperties.UpdatePriceCurrency Then
        
        PriceCurrency=Common.ObjectAttributeValue(SupplierPriceTypes,"PriceCurrency");
        
        If ValueIsFilled(PriceCurrency) Then
            Return
        EndIf;
        
        sptObject=SupplierPriceTypes.GetObject();
        
        sptObject.PriceCurrency=SettlementsCurrency;
        
        sptObject.Write();
        
    EndIf;
    
EndProcedure
