﻿
&After("BeforeWrite")
Procedure CPT_BeforeWrite(Cancel)
	
	If Not IsFolder AND
			Supplier AND
			NOT ValueIsFilled(SupplierPriceTypes) AND
			GetFunctionalOption("UseCounterpartiesPricesTracking") Then
        
        SupplierPriceTypes = Catalogs.SupplierPriceTypes.GetRef();
        
        AdditionalProperties.Insert("NewSupplierPriceTypeRef", SupplierPriceTypes);
        
    EndIf;
    
EndProcedure

&Before("OnWrite")
Procedure CPT_OnWrite(Cancel)
    
    Var NewSupplierPriceTypeRef;
    
    If AdditionalProperties.Property("NewSupplierPriceTypeRef",NewSupplierPriceTypeRef) AND Not NewSupplierPriceTypeRef=Undefined Then
        
        Counterparty=New Structure;
        Counterparty.Insert("NewSupplierPriceTypeRef",NewSupplierPriceTypeRef);
        Counterparty.Insert("CounterpartySupplier",Ref);
        
        Catalogs.SupplierPriceTypes.CreateSupplierPriceTypes(Counterparty,SettlementsCurrency);
        
    EndIf;
    
EndProcedure
