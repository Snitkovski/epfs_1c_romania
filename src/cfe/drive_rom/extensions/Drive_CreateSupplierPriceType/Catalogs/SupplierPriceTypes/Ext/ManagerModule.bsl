﻿
&Around("CreateSupplierPriceTypes")
Function CPT_CreateSupplierPriceTypes(Counterparty,SettlementsCurrency)
    
    Var NewSupplierPriceTypeRef,CounterpartySupplier;
    
    If TypeOf(Counterparty)=Type("Structure") Then
        Counterparty.Property("NewSupplierPriceTypeRef",NewSupplierPriceTypeRef);
        Counterparty.Property("CounterpartySupplier",CounterpartySupplier);
    Else
        CounterpartySupplier=Counterparty
    EndIf;
    
    SetPrivilegedMode(True);
    
    If Not ValueIsFilled(CounterpartySupplier)
		OR Not ValueIsFilled(SettlementsCurrency) Then
		
		Return Catalogs.SupplierPriceTypes.EmptyRef();
		
    EndIf;
    
    DescriptionSupplierPriceType = StrTemplate(NStr("en = 'Prices from %1'; pl = 'Prices from %1'; ro = 'Preturi de la %1'; ru = 'Цены от %1'"),Common.ObjectAttributeValue(CounterpartySupplier,"Description"));
    
    ctObject=Catalogs.SupplierPriceTypes.CreateItem();
    ctObject.Counterparty   = CounterpartySupplier;
    ctObject.PriceCurrency  = SettlementsCurrency;
    ctObject.Description    = DescriptionSupplierPriceType;
    
    If Not NewSupplierPriceTypeRef=Undefined Then
        ctObject.SetNewObjectRef(NewSupplierPriceTypeRef)
    EndIf;
    
    ctObject.Write();
    
    Return ctObject.Ref
    
EndFunction

Function FindSupplierPriceType(Counterparty,SettlementsCurrency) Export
    
    SetPrivilegedMode(True);
    
    If Not ValueIsFilled(Counterparty)
		OR Not ValueIsFilled(SettlementsCurrency) Then
		
		Return Catalogs.SupplierPriceTypes.EmptyRef();
		
	EndIf;
    
    Query = new Query("SELECT TOP 1 Ref FROM Catalog.SupplierPriceTypes WHERE NOT DeletionMark AND PriceCurrency = &PriceCurrency AND Counterparty = &Counterparty");
    
    Query.SetParameter("PriceCurrency",SettlementsCurrency);
    Query.SetParameter("Counterparty",Counterparty);
    
    Result = Query.Execute();
    
    If Result.IsEmpty() Then
        Return Catalogs.SupplierPriceTypes.EmptyRef()
    Endif;
    
    ResultSelection = Result.Select();
    ResultSelection.Next();
    
    Return ResultSelection.Ref
    
EndFunction

&Around("PresentationFieldsGetProcessing")
Procedure CPT_PresentationFieldsGetProcessing(Fields, StandardProcessing)
    
    StandardProcessing = False;
    
    Fields = New Array;
    
    Fields.Add("PriceCurrency");
    Fields.Add("Counterparty");
    
EndProcedure

&Around("PresentationGetProcessing")
Procedure CPT_PresentationGetProcessing(Data, Presentation, StandardProcessing)
    
    StandardProcessing = False;
    
    Presentation = StrTemplate(NStr("en = '(%1) Prices from %2'; pl = '(%1) Prices from %2'; ro = '(%1) Preturi de la %2'; ru = '(%1) Цены от %2'"), Data.PriceCurrency, Data.Counterparty)
    
EndProcedure
