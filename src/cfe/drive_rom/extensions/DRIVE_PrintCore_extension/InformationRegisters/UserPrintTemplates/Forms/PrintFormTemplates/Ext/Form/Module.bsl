﻿
&AtClient
Procedure ReplaceByExternalTemplate()
    
	CurrentData=Items.PrintFormTemplates.CurrentData;
	
	If CurrentData=Undefined Then
		Return
    EndIf;
    
	ND=New NotifyDescription("CompletePutFile",ThisObject,CurrentData);
	
	FileChooserDialogue=New FileDialog(FileDialogMode.Open);
	FileChooserDialogue.Title=NStr("en='Choose template file';ro='Choose template file';pl='Choose template file'");
    FileChooserDialogue.Filter=StrTemplate("%1(*.mxl)|*.mxl",NStr("en='Templates';ro='Templates';pl='Templates'"));
	
	BeginPutFile(ND,,FileChooserDialogue,True,UUID);
    
EndProcedure

&AtClient
Procedure CompletePutFile(Result,Address,SelectedFileName,AdditionalParameters) Export
	
	If Not Result Then
		Return
	EndIf;
	
    Param=New Structure;
    
    DescrArray=StrSplit(AdditionalParameters.TemplateMetadataObjectName,".",False);
    
    Param.Insert("TemplateName",DescrArray[DescrArray.UBound()]);
    
    DescrArray.Delete(DescrArray.UBound());
    
    Param.Insert("Object",StrConcat(DescrArray,"."));
    Param.Insert("UsagePic",AdditionalParameters.UsagePicture);
    
    Message=SaveTemplate(Address,Param);
    AdditionalParameters.UsagePicture=Param.UsagePic;
    ShowMessageBox(,Message);
	
EndProcedure

&AtServer
Function SaveTemplate(Address,AdditionalParameters)
	
	SetPrivilegedMode(True);
	
	Message="";
	
	Template=New SpreadsheetDocument;
	
	tmpFile=GetTempFileName(".mxl");
	
	GetFromTempStorage(Address).Write(tmpFile);
	
	Template.Read(tmpFile,,SpreadsheetDocumentFileType.MXL);
	
	DeleteFiles(tmpFile);
	
	RM=InformationRegisters.UserPrintTemplates.CreateRecordManager();
	
	RM.TemplateName = AdditionalParameters.TemplateName;
	RM.Object       = AdditionalParameters.Object;
	
	TemplateVS      = New ValueStorage(Template);
	
	RM.Read();
	
    If RM.Selected() Then
        RM.Template=TemplateVS;
    Else
        RM.TemplateName = AdditionalParameters.TemplateName;
        RM.Object       = AdditionalParameters.Object;
        RM.Template     = TemplateVS;
        RM.Use          = True;
    EndIf;
    Try
        RM.Write();
        Message=NStr("en='Template was changed successfully';ro='Template was changed successfully';pl='Template was changed successfully'");
        AdditionalParameters.UsagePic=2;
    Except
        Message=NStr("en = 'Template wasn't changed'")
    EndTry;
    
    Return Message
	
EndFunction

&AtClient
Procedure PrintCoreExt_Command_SwitchToExpertModeAfter(Command)
    
    OpenForm("InformationRegister.UserPrintTemplates.Form.PrintCoreExt_ListForm",,,UUID,Window);
    
EndProcedure

&AtClient
Procedure PrintCoreExt_ChoiceProcessingAround(SelectedValue, ChoiceSource)
    
    If Upper(ChoiceSource.FormName) = Upper("InformationRegister.UserPrintTemplates.Form.SelectTemplateOpeningMode") Then
        
        If TypeOf(SelectedValue) <> Type("Structure") Then
            Return;
        EndIf;
        
        If SelectedValue.OpeningModeView=2 Then
            ReplaceByExternalTemplate()
        Else
            ProceedWithCall(SelectedValue,ChoiceSource)
        EndIf;
        
    EndIf;
    
    SelectedValue=Undefined;

EndProcedure
