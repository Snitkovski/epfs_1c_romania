﻿
&AtClient
Procedure OnOpen(Cancel)
    
    If not ValueIsFilled(TemplateFile) Then
        TemplateFile = New FormattedString(NStr("en = 'Empty'; pl = 'Empty'; ro = 'Gol'; ru = 'Пустой'"),,,,"FileRef");
    EndIf;
    
EndProcedure

&AtClient
Procedure TemplateURLProcessing(Item, FormattedStringURL, StandardProcessing)
    
    StandardProcessing = False;
    
    ND = New NotifyDescription("CompleteChooseFile",ThisObject);
    
	FileChooserDialogue = New FileDialog(FileDialogMode.Open);
	FileChooserDialogue.Title = NStr("en = 'Choose template file'; pl = 'Choose template file'; ro = 'Selectati fisierul cu sablon'; ru = 'Выберите файл Макета'");
    FileChooserDialogue.Filter = StrTemplate("%1 (*.mxl)|*.mxl",NStr("en='Templates';ro='Templates';pl='Templates'"));
    FileChooserDialogue.Multiselect = False;
    
    FileChooserDialogue.Show(ND);
    
EndProcedure

&AtClient
Procedure CompleteChooseFile(SelectedFiles,AdditionalParameters) Export
	
	If SelectedFiles = Undefined Then
		Return
	EndIf;
    
    TemplateFile = New FormattedString(SelectedFiles[0],,,, "FileRef");
    
    ND = New NotifyDescription("CompletePutFile", ThisObject);
    
    BeginPutFile(ND, , SelectedFiles[0], False, UUID);
	
EndProcedure

&AtServer
Procedure CompletePutFile(Result,Address,SelectedFileName,AdditionalParameters) Export
	
	If Result = Undefined Then
		Return
    EndIf;
	
	tmpFile = GetTempFileName(".mxl");
	
	GetFromTempStorage(Address).Write(tmpFile);
	
	Template.Read(tmpFile, , SpreadsheetDocumentFileType.MXL);
	
	DeleteFiles(tmpFile);
	
EndProcedure

&AtServer
Procedure BeforeWriteAtServer(Cancel, CurrentObject, WriteParameters)
    
    CurrentObject.Template = New ValueStorage(Template);
    
EndProcedure

&AtClient
Procedure BeforeWrite(Cancel, WriteParameters)
    
    Cancel = Not CheckFilling();
    
EndProcedure

&AtServer
Procedure OnReadAtServer(CurrentObject)
    
    Template = CurrentObject.Template.Get();
    
    TemplateFile = New FormattedString(NStr("en = 'Open...'; pl = 'Open...'; ro = 'Deschideti...'; ru = 'Открыть...'"),,,,"FileRef");
    
EndProcedure
