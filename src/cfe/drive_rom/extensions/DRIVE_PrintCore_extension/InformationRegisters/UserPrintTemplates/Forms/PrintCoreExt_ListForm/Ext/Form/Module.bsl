﻿
&AtClient
Procedure OnOpen(Cancel)
    
    OnCloseNotifyDescription=New NotifyDescription("OpenDefaultForm",ThisObject);
    
EndProcedure

&AtClient
Procedure OpenDefaultForm(ClosureResult,AdditionalParameters) Export
    
    OpenForm("InformationRegister.UserPrintTemplates.Form.PrintFormTemplates",,,UUID,Window)
    
EndProcedure

&AtClient
Procedure ListBeforeAddRow(Item, Cancel, Clone, Parent, Folder, Parameter)
    
      Cancel=Clone
    
EndProcedure

&AtClient
Procedure CommandChange(Command)
    
    If Items.List.CurrentData=Undefined Then
        Return
    EndIf;
    
    Ntfy=New NotifyDescription("GetAnswer",ThisObject);
    
    Question=Nstr("en = 'You want to...'; ro = 'You want to...'; ru = 'You want to...'");
    Buttons=New ValueList;
    Buttons.Add("Edit",NStr("en = 'Edit template'; ro = 'Edit'; ru = 'Edit template'"));
    Buttons.Add("Export",NStr("en = 'Export template'; ro = 'Export template'; ru = 'Export template'"));
    Buttons.Add("Cancel",NStr("en = 'Nothing'; ro = 'Nothing'; ru = 'Nothing'"));
    
    ShowQueryBox(Ntfy,Question,Buttons);
    
EndProcedure

&AtClient
Procedure GetAnswer(QuestionResult,AdditionalParameters) Export
    
    If QuestionResult=Undefined or
        QuestionResult="Cancel" Then
        Return
    EndIf;
    
    CurrentData = Items.List.CurrentData;
    
    OpeningParameters = New Structure;
    OpeningParameters.Insert("TemplateMetadataObjectName", StrTemplate("%1.%2",CurrentData.Object,CurrentData.TemplateName));
    OpeningParameters.Insert("TemplateType", "MXL");
    OpeningParameters.Insert("DocumentName",CurrentData.Object);
    If QuestionResult="Edit" Then
        OpeningParameters.Insert("Edit", True);
    ElsIf QuestionResult="Export" Then
        ExportTemplate(StrTemplate("%1.%2",CurrentData.Object,CurrentData.TemplateName));
        Return
    EndIf;
    
    OpenForm("CommonForm.EditSpreadsheetDocument", OpeningParameters, ThisObject);
    
EndProcedure

&AtClient
Procedure ExportTemplate(FileName)
    
   Ntnfy=New NotifyDescription("SaveTemplate",ThisObject);
   FileChoseDialog=New FileDialog(FileDialogMode.Save);
   FileChoseDialog.FullFileName=StrTemplate("%1.MXL",StrReplace(FileName,".","_"));
   
   FileChoseDialog.Show(Ntnfy);
    
EndProcedure

&AtClient
Procedure SaveTemplate(SelectedFiles,AdditionalParameters) Export
    
    If SelectedFiles=Undefined Then
        Return
    EndIf;
    
    Template=GetTemplate();
    
    Template.Write(SelectedFiles[0])
    
EndProcedure
    
&AtServer
Function GetTemplate()
    
   SetPrivilegedMode(True);
   
   RM=InformationRegisters.UserPrintTemplates.CreateRecordManager();
   
   RM.TemplateName=Items.List.CurrentRow.TemplateName;
   RM.Object=Items.List.CurrentRow.Object;
   
   RM.Read();
   
   If RM.Selected() Then
      Return RM.Template.Get()
   EndIf;
   
EndFunction

&AtServerNoContext
Procedure WriteTemplate(Val RecordKey, TemplateAddressInTempStorage)
    
   SetPrivilegedMode(True);
   
   RM=InformationRegisters.UserPrintTemplates.CreateRecordManager();
   
   FillPropertyValues(RM,RecordKey);
   
   RM.Template=New ValueStorage(GetFromTempStorage(TemplateAddressInTempStorage));
   
   RM.Write()
    
EndProcedure

&AtClient
Procedure NotificationProcessing(EventName, Parameter, Source)
    
    If EventName = "Write_SpreadsheetDocument" AND Source.FormOwner = ThisObject Then
		If Not ReadOnly Then
			Template = Parameter.SpreadsheetDocument;
			TemplateAddressInTempStorage = PutToTempStorage(Template);
			WriteTemplate(Items.List.CurrentData, TemplateAddressInTempStorage);
		EndIf;
	EndIf;
    
EndProcedure
