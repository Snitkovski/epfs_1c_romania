﻿
&AtServer
Procedure PrintCoreExt_OnCreateAtServerAfter(Cancel, StandardProcessing)
    
    DontAskAgain = False;
    
EndProcedure

&AtClient
Procedure PrintCoreExt_OKBefore(Command)
    
NotifyChoice(New Structure("DontAskAgain, OpeningModeView",False,HowToOpen));

EndProcedure
