﻿// <Procedure description>
//
// Parameters:
//  <Parameter1>  - <Type.Subtype> - <parameter description>
//                  <parameter description continued>
//  <Parameter2>  - <Type.Subtype> - <parameter description>
//                  <parameter description continued>
//
#Region AdditionalProceduresAndFunctions

&AtServer
Procedure SplitDateElementOnForm(FormElementDate, FormObject) Export
	
	ElementName = FormElementDate.Name;
	
	DateGroup = FormObject.Items.Add("Group_" + ElementName + "_DateTime", Type("FormGroup"), FormElementDate.Parent);
	DateGroup.Type = FormGroupType.UsualGroup;
	DateGroup.ShowTitle = False;
	DateGroup.Group = ChildFormItemsGroup.AlwaysHorizontal;

	FormElementDate.Format = "DF='dd.MM.yyyy'";
	FormElementDate.EditFormat = FormElementDate.Format;
	FormObject.Items.Move(FormElementDate, DateGroup);
	
	FormElementTime = FormObject.Items.Add(ElementName + "_Time", Type("FormField"), DateGroup);
	FormElementTime.DataPath = FormElementDate.DataPath;
	FormElementTime.Type = FormFieldType.InputField;
	FormElementTime.Format = "DF='HH:mm'";
	FormElementTime.EditFormat = FormElementTime.Format;
	FormElementTime.TitleLocation = FormItemTitleLocation.None;
	FormElementTime.SpinButton = True;
	FormElementTime.ChoiceButton = False;
	
EndProcedure // SplitDateElementOnForm()

#EndRegion

