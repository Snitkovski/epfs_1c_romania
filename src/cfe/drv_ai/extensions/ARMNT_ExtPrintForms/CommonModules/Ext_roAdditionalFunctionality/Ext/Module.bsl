﻿
// <Function description>
//
//
// Parameters:
//  <Parameter1>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//  <Parameter2>  - <Type.Subtype> - <parameter description>
//                 <parameter description continued>
//
// Returns:
//   <Type.Subtype>   - <returned value description>
//
Function Ext_roAdditionalPrint() Export
	
	PrintFormsOwner = "AROMINT";
	Return PrintFormsOwner

EndFunction // Ext_roAdditionalPrint()
