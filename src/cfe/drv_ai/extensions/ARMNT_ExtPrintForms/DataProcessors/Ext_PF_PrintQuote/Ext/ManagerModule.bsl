﻿#If Server Or ThickClientOrdinaryApplication Or ExternalConnection Then
    
Procedure OnDefineSettings(Settings) Export
	
	// К какому объекту привязывается. Лучше задавать в виде строки, чтобы не тянуть объект в Расширение!
	// Metadata.Object for link to. It's better way - as String, in this case we don't to pull Object into Extension
    Settings.Placement.Add("Document.Quote");
    Settings.Placement.Add("Document.SalesOrder");
    Settings.AddPrintCommands = True;

EndProcedure

Procedure AddPrintCommands(PrintCommands) Export

	PrintFormsOwner = Ext_roAdditionalFunctionality.Ext_roAdditionalPrint();
	PrintFormsOwner = ?(IsBlankString(PrintFormsOwner), "Pers.", PrintFormsOwner);
	
	//NewCommand = PrintCommands.Add();
    NewCommand = PrintCommands.Insert(0);
	NewCommand.Presentation = "Oferta Comerciala" + " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_Quote"; 				 		 // Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True; 			 // Validity check Before print - Проверка проведения перед печатью

	//NewCommand = PrintCommands.Add();
    NewCommand = PrintCommands.Insert(1);
	NewCommand.Presentation = "Factură proformă" + " " + PrintFormsOwner; // PrintButton name - Наименование кнопки печати
	NewCommand.ID = "Ext_proforma"; 				 		 // Identifier - Идентификатор
    NewCommand.CheckPostingBeforePrint = True; 			 // Validity check Before print - Проверка проведения перед печатью
    
    For Each Row In PrintCommands Do
    	Row.Order = PrintCommands.IndexOf(Row)+1
    EndDo;
                                               
EndProcedure		// AddPrintCommands()

Procedure Print(ObjectsArray, PrintParameters, PrintFormsCollection, PrintObjects, OutputParameters) Export
    
	// "Ext_Quote" must be as NewCommand.ID above
    If PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_Quote") Then
        
        // PrintForm template "virtual" name - you can set any, but conform rule <Name>.<Name>.<Name>
        // Определяем виртуальное имя шаблона печатной формы. Может быть вообще любым. Но должно удовлетворять шаблону <Имя>.<Имя>.<Имя>
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_Quote");
        
        // Call standart PrintHandler. You should pull the real Object into Extension
        // Вызываем НЕстандартный обработчик печати - собственный, см.ниже
        //PrintForms = PrintQuote(ObjectsArray, PrintObjects, PrintParameters.Result, AdditionalParameters);
        PrintForms = PrintQuote(ObjectsArray, PrintObjects, "Quote", AdditionalParameters);
        
        // Standart PrintForm output - Стандартный вывод печатной формы
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_Quote", "Oferta Comerciala", PrintForms);

    ElsIf PrintManagement.TemplatePrintRequired(PrintFormsCollection, "Ext_proforma") Then
        
        AdditionalParameters = New Structure("UserPrintTemplate", "Document.Print.PF_MXL_ProformaInvoice");
        PrintForms = PrintProformaInvoice(ObjectsArray, PrintObjects, "ProformaInvoice", AdditionalParameters);
        PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection, "Ext_proforma", "Factură proformă", PrintForms);
        
    EndIf;
    
EndProcedure	//	Print()

#Region PrintInterface

Function GetQueryText(ObjectsArray, TemplateName)
	
	QueryText = "";
	If ObjectsArray.Count() > 0 Then
		If TypeOf(ObjectsArray[0]) = Type("DocumentRef.Quote") And TemplateName = "Quote" Then
			QueryText = GetQuoteQueryTextForQuote(False);
		ElsIf TypeOf(ObjectsArray[0]) = Type("DocumentRef.Quote") And TemplateName = "QuoteAllVariants" Then
			QueryText = GetQuoteQueryTextForQuote(True);
		ElsIf TypeOf(ObjectsArray[0]) = Type("DocumentRef.Quote") And TemplateName = "ProformaInvoice" Then
			QueryText = GetProformaInvoiceQueryTextForQuote(False);
		ElsIf TypeOf(ObjectsArray[0]) = Type("DocumentRef.Quote") And TemplateName = "ProformaInvoiceAllVariants" Then
			QueryText = GetProformaInvoiceQueryTextForQuote(True);
		ElsIf TypeOf(ObjectsArray[0]) = Type("DocumentRef.SalesOrder") And TemplateName = "Quote" Then
			QueryText = GetQuoteQueryTextForSalesOrder();
		ElsIf TypeOf(ObjectsArray[0]) = Type("DocumentRef.SalesOrder") And TemplateName = "ProformaInvoice" Then
			QueryText = GetProformaInvoiceQueryTextForSalesOrder();
		EndIf;
	EndIf;
	
	Return QueryText;
	
EndFunction

Function GetProformaInvoiceQueryTextForQuote(AllVariants)
	
	QueryText =
	"SELECT ALLOWED
	|	Quote.Ref AS Ref,
	|	Quote.Number AS Number,
	|	Quote.Date AS Date,
	|	Quote.Company AS Company,
	|	Quote.CompanyVATNumber AS CompanyVATNumber,
	|	Quote.Counterparty AS Counterparty,
	|	Quote.Contract AS Contract,
	|	Quote.BankAccount AS BankAccount,
	|	Quote.AmountIncludesVAT AS AmountIncludesVAT,
	|	Quote.DocumentCurrency AS DocumentCurrency,
	|	Quote.PreferredVariant AS PreferredVariant
	|INTO Quotes
	|FROM
	|	Document.Quote AS Quote
	|WHERE
	|	Quote.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Quote.Ref AS Ref,
	|	Quote.Number AS DocumentNumber,
	|	Quote.Date AS DocumentDate,
	|	Quote.Company AS Company,
	|	Quote.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	Quote.Counterparty AS Counterparty,
	|	Quote.Contract AS Contract,
	|	Quote.BankAccount AS BankAccount,
	|	CASE
	|		WHEN CounterpartyContracts.ContactPerson = VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN Counterparties.ContactPerson
	|		ELSE CounterpartyContracts.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	Quote.AmountIncludesVAT AS AmountIncludesVAT,
	|	Quote.DocumentCurrency AS DocumentCurrency,
	|	Quote.PreferredVariant AS PreferredVariant
	|INTO Header
	|FROM
	|	Quotes AS Quote
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON Quote.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON Quote.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON Quote.Contract = CounterpartyContracts.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	QuoteInventory.Ref AS Ref,
	|	QuoteInventory.LineNumber AS LineNumber,
	|	QuoteInventory.Products AS Products,
	|	QuoteInventory.Characteristic AS Characteristic,
	|	VALUE(Catalog.ProductsBatches.EmptyRef) AS Batch,
	|	QuoteInventory.Quantity AS Quantity,
	|	QuoteInventory.MeasurementUnit AS MeasurementUnit,
	|	CASE
	|		WHEN QuoteInventory.Quantity = 0
	|			THEN 0
	|		ELSE QuoteInventory.Price + (QuoteInventory.Total - QuoteInventory.Amount - QuoteInventory.VATAmount) / QuoteInventory.Quantity
	|	END AS Price,
	|	QuoteInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	QuoteInventory.Total - QuoteInventory.VATAmount AS Amount,
	|	QuoteInventory.VATRate AS VATRate,
	|	QuoteInventory.VATAmount AS VATAmount,
	|	QuoteInventory.Total AS Total,
	|	QuoteInventory.Content AS Content,
	|	QuoteInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	QuoteInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	QuoteInventory.ConnectionKey AS ConnectionKey,
	|	QuoteInventory.Variant AS Variant,
	|	QuoteInventory.BundleProduct AS BundleProduct,
	|	QuoteInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO FilteredInventory
	|FROM
	|	Document.Quote.Inventory AS QuoteInventory
	|WHERE
	|	QuoteInventory.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.BankAccount AS BankAccount,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
	|	MIN(FilteredInventory.LineNumber) AS LineNumber,
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	FilteredInventory.Price AS Price,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountRate,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	SUM(FilteredInventory.Amount) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN FilteredInventory.Quantity
	|			ELSE 0
	|		END) AS Freight,
	|	SUM(FilteredInventory.Total) AS Total,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN 0
	|			ELSE FilteredInventory.Quantity
	|		END) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	FilteredInventory.Batch AS Batch,
	|	FilteredInventory.Variant AS Variant,
	|	CatalogProducts.IsFreightService AS IsFreightService,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|			AND (Header.PreferredVariant = FilteredInventory.Variant
	|				OR &AllVariants)
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	Header.DocumentNumber,
	|	Header.DocumentDate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Ref,
	|	Header.Counterparty,
	|	Header.CompanyLogoFile,
	|	Header.Contract,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	Header.DocumentCurrency,
	|	CatalogProducts.SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	FilteredInventory.VATRate,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.Products,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	FilteredInventory.Price,
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.MeasurementUnit,
	|	FilteredInventory.Batch,
	|	FilteredInventory.Variant,
	|	CatalogProducts.IsFreightService,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.BankAccount AS BankAccount,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	Tabular.Amount AS Amount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	Tabular.Freight AS FreightTotal,
	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountRate
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Products AS Products,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.MeasurementUnit AS MeasurementUnit,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	VALUE(Catalog.ShippingAddresses.EmptyRef) AS ShippingAddress,
	|	VALUE(Catalog.BusinessUnits.EmptyRef) AS StructuralUnit,
	|	VALUE(Enum.DeliveryOptions.EmptyRef) AS DeliveryOption,
	|	Tabular.Variant AS Variant,
	|	Tabular.IsFreightService AS IsFreightService,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic
	|FROM
	|	Tabular AS Tabular
	|
	|ORDER BY
	|	Tabular.DocumentNumber,
	|	Variant,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(CounterpartyContactPerson),
	|	MAX(BankAccount),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
	|	COUNT(LineNumber),
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(FreightTotal),
	|	SUM(DiscountAmount),
	|	MAX(ShippingAddress),
	|	MAX(StructuralUnit),
	|	MAX(DeliveryOption)
	|BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.Variant AS Variant,
	|	Tabular.VATRate AS VATRate,
	|	SUM(Tabular.Amount) AS Amount,
	|	SUM(Tabular.VATAmount) AS VATAmount
	|FROM
	|	Tabular AS Tabular
	|
	|GROUP BY
	|	Tabular.Ref,
	|	Tabular.Variant,
	|	Tabular.VATRate
	|TOTALS BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	COUNT(Tabular.LineNumber) AS LineNumber,
	|	Tabular.Ref AS Ref,
	|	SUM(Tabular.Quantity) AS Quantity,
	|	Tabular.Variant AS Variant
	|FROM
	|	Tabular AS Tabular
	|WHERE
	|	NOT Tabular.IsFreightService
	|
	|GROUP BY
	|	Tabular.Ref,
	|	Tabular.Variant";
	
	Return QueryText;
	
EndFunction

Function GetProformaInvoiceQueryTextForSalesOrder() Export
	
	QueryText =
	"SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS Number,
	|	SalesOrder.Date AS Date,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.EstimateIsCalculated AS EstimateIsCalculated,
	|	SalesOrder.ContactPerson AS ContactPerson,
	|	SalesOrder.ShippingAddress AS ShippingAddress,
	|	SalesOrder.StructuralUnitReserve AS StructuralUnit,
	|	SalesOrder.DeliveryOption AS DeliveryOption
	|INTO SalesOrders
	|FROM
	|	Document.SalesOrder AS SalesOrder
	|WHERE
	|	SalesOrder.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS DocumentNumber,
	|	SalesOrder.Date AS DocumentDate,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	|	CASE
	|		WHEN SalesOrder.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN SalesOrder.ContactPerson
	|		WHEN CounterpartyContracts.ContactPerson <> VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN CounterpartyContracts.ContactPerson
	|		ELSE Counterparties.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.ShippingAddress AS ShippingAddress,
	|	SalesOrder.StructuralUnit AS StructuralUnit,
	|	SalesOrder.DeliveryOption AS DeliveryOption
	|INTO Header
	|FROM
	|	SalesOrders AS SalesOrder
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON SalesOrder.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON SalesOrder.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON SalesOrder.Contract = CounterpartyContracts.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrderInventory.Ref AS Ref,
	|	SalesOrderInventory.LineNumber AS LineNumber,
	|	SalesOrderInventory.Products AS Products,
	|	SalesOrderInventory.Characteristic AS Characteristic,
	|	SalesOrderInventory.Batch AS Batch,
	|	SalesOrderInventory.Quantity AS Quantity,
	|	SalesOrderInventory.Reserve AS Reserve,
	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
	|	CASE
	|		WHEN SalesOrderInventory.Quantity = 0
	|			THEN 0
	|		ELSE SalesOrderInventory.Price + (SalesOrderInventory.Total - SalesOrderInventory.Amount - SalesOrderInventory.VATAmount) / SalesOrderInventory.Quantity
	|	END AS Price,
	|	SalesOrderInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SalesOrderInventory.Total - SalesOrderInventory.VATAmount AS Amount,
	|	SalesOrderInventory.VATRate AS VATRate,
	|	SalesOrderInventory.VATAmount AS VATAmount,
	|	SalesOrderInventory.Total AS Total,
	|	SalesOrderInventory.Content AS Content,
	|	SalesOrderInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	SalesOrderInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	SalesOrderInventory.ConnectionKey AS ConnectionKey,
	|	SalesOrderInventory.BundleProduct AS BundleProduct,
	|	SalesOrderInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO FilteredInventory
	|FROM
	|	Document.SalesOrder.Inventory AS SalesOrderInventory
	|WHERE
	|	SalesOrderInventory.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.BankAccount AS BankAccount,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
//	|	MIN(FilteredInventory.LineNumber) AS LineNumber,
	|	FilteredInventory.LineNumber AS LineNumber,        //   SAM !!!
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	FilteredInventory.Price AS Price,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountRate,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	SUM(FilteredInventory.Amount) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN FilteredInventory.Quantity
	|			ELSE 0
	|		END) AS Freight,
	|	SUM(FilteredInventory.Total) AS Total,
	|	FilteredInventory.Price * SUM(CASE
	|			WHEN CatalogProducts.IsFreightService
	|				THEN 0
	|			ELSE FilteredInventory.Quantity
	|		END) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	FilteredInventory.Batch AS Batch,
	|	Header.ShippingAddress AS ShippingAddress,
	|	Header.StructuralUnit AS StructuralUnit,
	|	Header.DeliveryOption AS DeliveryOption,
	|	CatalogProducts.IsFreightService AS IsFreightService,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	Header.DocumentNumber,
	|	Header.DocumentDate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Ref,
	|	Header.Counterparty,
	|	Header.CompanyLogoFile,
	|	Header.Contract,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	Header.DocumentCurrency,
	|	CatalogProducts.SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	FilteredInventory.VATRate,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.Products,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	FilteredInventory.Price,
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.MeasurementUnit,
	|	FilteredInventory.Batch,
	|	FilteredInventory.LineNumber,     //  SAM !!!
	|	Header.ShippingAddress,
	|	Header.StructuralUnit,
	|	Header.DeliveryOption,
	|	CatalogProducts.IsFreightService,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.BankAccount AS BankAccount,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	Tabular.Amount AS Amount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	Tabular.Freight AS FreightTotal,
	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountRate
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Products AS Products,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.MeasurementUnit AS MeasurementUnit,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	Tabular.ShippingAddress AS ShippingAddress,
	|	Tabular.StructuralUnit AS StructuralUnit,
	|	Tabular.DeliveryOption AS DeliveryOption,
	|	0 AS Variant,
	|	Tabular.IsFreightService AS IsFreightService,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic
	|FROM
	|	Tabular AS Tabular
	|
	|ORDER BY
	|	Tabular.DocumentNumber,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(CounterpartyContactPerson),
	|	MAX(BankAccount),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
//	|	COUNT(LineNumber),				//  SAM !!!
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(FreightTotal),
	|	SUM(DiscountAmount),
	|	MAX(ShippingAddress),
	|	MAX(StructuralUnit),
	|	MAX(DeliveryOption)
	|BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	0 AS Variant,
	|	Tabular.VATRate AS VATRate,
	|	SUM(Tabular.Amount) AS Amount,
	|	SUM(Tabular.VATAmount) AS VATAmount
	|FROM
	|	Tabular AS Tabular
	|
	|GROUP BY
	|	Tabular.Ref,
	|	Tabular.VATRate
	|TOTALS BY
	|	Ref,
	|	Variant
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	COUNT(Tabular.LineNumber) AS LineNumber,
	|	Tabular.Ref AS Ref,
	|	SUM(Tabular.Quantity) AS Quantity,
	|	0 AS Variant
	|FROM
	|	Tabular AS Tabular
	|WHERE
	|	NOT Tabular.IsFreightService
	|
	|GROUP BY
	|	Tabular.Ref";
	
	Return QueryText;
	
EndFunction

Function GetQuoteQueryTextForQuote(AllVariants) Export
	
	QueryText =
	"SELECT ALLOWED
	|	Quote.Ref AS Ref,
	|	Quote.Number AS Number,
	|	Quote.Date AS Date,
	|	Quote.Company AS Company,
	|	Quote.CompanyVATNumber AS CompanyVATNumber,
	|	Quote.Counterparty AS Counterparty,
	|	Quote.Contract AS Contract,
	|	Quote.BankAccount AS BankAccount,
	|	Quote.AmountIncludesVAT AS AmountIncludesVAT,
	|	Quote.DocumentCurrency AS DocumentCurrency,
	|	Quote.ValidUntil AS ValidUntil,
	|	Quote.ExchangeRate AS ExchangeRate,                                       //  SAM
	|	CASE
	|		WHEN Quote.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN &ReverseChargeAppliesRate
	|		ELSE """"
	|	END AS ReverseChargeApplies,
	|	Quote.PreferredVariant AS PreferredVariant
	|INTO Quotes
	|FROM
	|	Document.Quote AS Quote
	|WHERE
	|	Quote.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Quote.Ref AS Ref,
	|	Quote.Number AS DocumentNumber,
	|	Quote.Date AS DocumentDate,
	|	Quote.Company AS Company,
	|	Quote.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	Quote.Counterparty AS Counterparty,
	|	Quote.Contract AS Contract,
	|	Quote.BankAccount AS BankAccount,
	|	CASE
	|		WHEN CounterpartyContracts.ContactPerson = VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN Counterparties.ContactPerson
	|		ELSE CounterpartyContracts.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	Quote.AmountIncludesVAT AS AmountIncludesVAT,
	|	Quote.DocumentCurrency AS DocumentCurrency,
	|	Quote.ValidUntil AS ValidUntil,
	|	Quote.ReverseChargeApplies AS ReverseChargeApplies,
	|	Quote.PreferredVariant AS PreferredVariant,
	|	Quote.ExchangeRate AS ExchangeRate                                       //  SAM
	|INTO Header
	|FROM
	|	Quotes AS Quote
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON Quote.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON Quote.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON Quote.Contract = CounterpartyContracts.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	QuoteInventory.Ref AS Ref,
	|	QuoteInventory.LineNumber AS LineNumber,
	|	QuoteInventory.Products AS Products,
	|	QuoteInventory.Characteristic AS Characteristic,
	|	VALUE(Catalog.ProductsBatches.EmptyRef) AS Batch,
	|	QuoteInventory.Quantity AS Quantity,
	|	QuoteInventory.MeasurementUnit AS MeasurementUnit,
	|	CASE
	|		WHEN QuoteInventory.Quantity = 0
	|			THEN 0
	|		ELSE QuoteInventory.Price + (QuoteInventory.Total - QuoteInventory.Amount - QuoteInventory.VATAmount) / QuoteInventory.Quantity
	|	END AS Price,
	|	QuoteInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	QuoteInventory.Total - QuoteInventory.VATAmount AS Amount,
	|	QuoteInventory.VATRate AS VATRate,
	|	QuoteInventory.VATAmount AS VATAmount,
	|	QuoteInventory.Total AS Total,
	|	QuoteInventory.Content AS Content,
	|	QuoteInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	QuoteInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	QuoteInventory.ConnectionKey AS ConnectionKey,
	|	QuoteInventory.Variant AS Variant,
	|	QuoteInventory.BundleProduct AS BundleProduct,
	|	QuoteInventory.BundleCharacteristic AS BundleCharacteristic,
	|	QuoteInventory.AdditionalInformation AS AdditionalInformation            //  SAM
	|INTO FilteredInventory
	|FROM
	|	Document.Quote.Inventory AS QuoteInventory
	|WHERE
	|	QuoteInventory.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.BankAccount AS BankAccount,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
	|	MIN(FilteredInventory.LineNumber) AS LineNumber,
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	FilteredInventory.Price AS Price,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SUM(FilteredInventory.Amount) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
	|	SUM(FilteredInventory.Total) AS Total,
	|	FilteredInventory.Price * SUM(FilteredInventory.Quantity) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.Batch AS Batch,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	Header.ValidUntil AS ValidUntil,
	|	Header.ReverseChargeApplies AS ReverseChargeApplies,
	|	Header.ExchangeRate AS ExchangeRate,                                     //  SAM
	|	FilteredInventory.Variant AS Variant,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic,
	|	FilteredInventory.AdditionalInformation AS AdditionalInformation         //  SAM
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|			AND (Header.PreferredVariant = FilteredInventory.Variant
	|				OR &AllVariants)
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	FilteredInventory.VATRate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Counterparty,
	|	Header.Contract,
	|	CatalogProducts.SKU,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	Header.CompanyLogoFile,
	|	Header.DocumentNumber,
	|	Header.DocumentCurrency,
	|	Header.Ref,
	|	Header.DocumentDate,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Products,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.Batch,
	|	FilteredInventory.MeasurementUnit,
	|	Header.ValidUntil,
	|	Header.ReverseChargeApplies,
	|	Header.ExchangeRate,                                                     //  SAM
	|	FilteredInventory.Variant,
	|	FilteredInventory.Price,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic,
	|	FilteredInventory.AdditionalInformation                                  //  SAM
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.BankAccount AS BankAccount,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountMarkupPercent
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Amount AS Amount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	Tabular.ValidUntil AS ValidUntil,
	|	Tabular.ReverseChargeApplies AS ReverseChargeApplies,
	|	Tabular.Variant AS Variant,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic,
	|	Tabular.Products AS Products,
	|	Tabular.AdditionalInformation AS AdditionalInformation,                  //  SAM
	|	Tabular.ExchangeRate AS ExchangeRate                                     //  SAM
	|FROM
	|	Tabular AS Tabular
	|
	|ORDER BY
	|	DocumentNumber,
	|	Variant,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(CounterpartyContactPerson),
	|	MAX(BankAccount),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
	|	COUNT(LineNumber),
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(DiscountAmount),
	|	MAX(ValidUntil),
	|	MAX(ReverseChargeApplies),
	|	MAX(ExchangeRate)                                                        //  SAM
	|BY
	|	Ref,
	|	Variant";
	
	Return QueryText;
	
EndFunction

Function GetQuoteQueryTextForSalesOrder()
	
	QueryText =
	"SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS Number,
	|	SalesOrder.Date AS Date,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.ShipmentDate AS ShipmentDate,
	|	CASE
	|		WHEN SalesOrder.VATTaxation = VALUE(Enum.VATTaxationTypes.ReverseChargeVAT)
	|			THEN &ReverseChargeAppliesRate
	|		ELSE """"
	|	END AS ReverseChargeApplies
	|INTO SalesOrders
	|FROM
	|	Document.SalesOrder AS SalesOrder
	|WHERE
	|	SalesOrder.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrder.Ref AS Ref,
	|	SalesOrder.Number AS DocumentNumber,
	|	SalesOrder.Date AS DocumentDate,
	|	SalesOrder.Company AS Company,
	|	SalesOrder.CompanyVATNumber AS CompanyVATNumber,
	|	Companies.LogoFile AS CompanyLogoFile,
	|	SalesOrder.Counterparty AS Counterparty,
	|	SalesOrder.Contract AS Contract,
	|	CASE
	|		WHEN CounterpartyContracts.ContactPerson = VALUE(Catalog.ContactPersons.EmptyRef)
	|			THEN Counterparties.ContactPerson
	|		ELSE CounterpartyContracts.ContactPerson
	|	END AS CounterpartyContactPerson,
	|	SalesOrder.BankAccount AS BankAccount,
	|	SalesOrder.AmountIncludesVAT AS AmountIncludesVAT,
	|	SalesOrder.DocumentCurrency AS DocumentCurrency,
	|	SalesOrder.ShipmentDate AS ShipmentDate,
	|	SalesOrder.ReverseChargeApplies AS ReverseChargeApplies
	|INTO Header
	|FROM
	|	SalesOrders AS SalesOrder
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON SalesOrder.Company = Companies.Ref
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON SalesOrder.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.CounterpartyContracts AS CounterpartyContracts
	|		ON SalesOrder.Contract = CounterpartyContracts.Ref
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	SalesOrderInventory.Ref AS Ref,
	|	SalesOrderInventory.LineNumber AS LineNumber,
	|	SalesOrderInventory.Products AS Products,
	|	SalesOrderInventory.Characteristic AS Characteristic,
	|	SalesOrderInventory.Batch AS Batch,
	|	SalesOrderInventory.Quantity AS Quantity,
	|	SalesOrderInventory.MeasurementUnit AS MeasurementUnit,
	|	CASE
	|		WHEN SalesOrderInventory.Quantity = 0
	|			THEN 0
	|		ELSE SalesOrderInventory.Price + (SalesOrderInventory.Total - SalesOrderInventory.Amount - SalesOrderInventory.VATAmount) / SalesOrderInventory.Quantity
	|	END AS Price,
	|	SalesOrderInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SalesOrderInventory.Total - SalesOrderInventory.VATAmount AS Amount,
	|	SalesOrderInventory.VATRate AS VATRate,
	|	SalesOrderInventory.VATAmount AS VATAmount,
	|	SalesOrderInventory.Total AS Total,
	|	SalesOrderInventory.Content AS Content,
	|	SalesOrderInventory.AutomaticDiscountsPercent AS AutomaticDiscountsPercent,
	|	SalesOrderInventory.AutomaticDiscountAmount AS AutomaticDiscountAmount,
	|	SalesOrderInventory.ConnectionKey AS ConnectionKey,
	|	SalesOrderInventory.BundleProduct AS BundleProduct,
	|	SalesOrderInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO FilteredInventory
	|FROM
	|	Document.SalesOrder.Inventory AS SalesOrderInventory
	|WHERE
	|	SalesOrderInventory.Ref IN(&ObjectsArray)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT ALLOWED
	|	Header.Ref AS Ref,
	|	Header.DocumentNumber AS DocumentNumber,
	|	Header.DocumentDate AS DocumentDate,
	|	Header.Company AS Company,
	|	Header.CompanyVATNumber AS CompanyVATNumber,
	|	Header.CompanyLogoFile AS CompanyLogoFile,
	|	Header.Counterparty AS Counterparty,
	|	Header.Contract AS Contract,
	|	Header.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Header.BankAccount AS BankAccount,
	|	Header.AmountIncludesVAT AS AmountIncludesVAT,
	|	Header.DocumentCurrency AS DocumentCurrency,
	|	Header.ShipmentDate AS ShipmentDate,
	|	MIN(FilteredInventory.LineNumber) AS LineNumber,
	|	CatalogProducts.SKU AS SKU,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END AS ProductDescription,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """" AS ContentUsed,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END AS CharacteristicDescription,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END AS BatchDescription,
	|	CatalogProducts.UseSerialNumbers AS UseSerialNumbers,
	|	MIN(FilteredInventory.ConnectionKey) AS ConnectionKey,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description) AS UOM,
	|	SUM(FilteredInventory.Quantity) AS Quantity,
	|	FilteredInventory.Price AS Price,
	|	SUM(FilteredInventory.AutomaticDiscountAmount) AS AutomaticDiscountAmount,
	|	FilteredInventory.DiscountMarkupPercent AS DiscountMarkupPercent,
	|	SUM(FilteredInventory.Amount) AS Amount,
	|	FilteredInventory.VATRate AS VATRate,
	|	SUM(FilteredInventory.VATAmount) AS VATAmount,
	|	SUM(FilteredInventory.Total) AS Total,
	|	FilteredInventory.Price * SUM(FilteredInventory.Quantity) AS Subtotal,
	|	FilteredInventory.Products AS Products,
	|	FilteredInventory.Characteristic AS Characteristic,
	|	FilteredInventory.Batch AS Batch,
	|	FilteredInventory.MeasurementUnit AS MeasurementUnit,
	|	Header.ReverseChargeApplies AS ReverseChargeApplies,
	|	FilteredInventory.BundleProduct AS BundleProduct,
	|	FilteredInventory.BundleCharacteristic AS BundleCharacteristic
	|INTO Tabular
	|FROM
	|	Header AS Header
	|		INNER JOIN FilteredInventory AS FilteredInventory
	|		ON Header.Ref = FilteredInventory.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (FilteredInventory.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (FilteredInventory.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.ProductsBatches AS CatalogBatches
	|		ON (FilteredInventory.Batch = CatalogBatches.Ref)
	|		LEFT JOIN Catalog.UOM AS CatalogUOM
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOM.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (FilteredInventory.MeasurementUnit = CatalogUOMClassifier.Ref)
	|
	|GROUP BY
	|	FilteredInventory.VATRate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.Counterparty,
	|	Header.Contract,
	|	CatalogProducts.SKU,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	CASE
	|		WHEN (CAST(FilteredInventory.Content AS STRING(1024))) <> """"
	|			THEN CAST(FilteredInventory.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	Header.ShipmentDate,
	|	(CAST(FilteredInventory.Content AS STRING(1024))) <> """",
	|	Header.CompanyLogoFile,
	|	Header.DocumentNumber,
	|	Header.DocumentCurrency,
	|	Header.Ref,
	|	Header.DocumentDate,
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	CASE
	|		WHEN CatalogProducts.UseBatches
	|			THEN CatalogBatches.Description
	|		ELSE """"
	|	END,
	|	CatalogProducts.UseSerialNumbers,
	|	ISNULL(CatalogUOM.Description, CatalogUOMClassifier.Description),
	|	FilteredInventory.DiscountMarkupPercent,
	|	FilteredInventory.Products,
	|	FilteredInventory.Characteristic,
	|	FilteredInventory.Batch,
	|	FilteredInventory.MeasurementUnit,
	|	Header.ReverseChargeApplies,
	|	FilteredInventory.Price,
	|	FilteredInventory.BundleProduct,
	|	FilteredInventory.BundleCharacteristic
	|
	|UNION ALL
	|
	|SELECT
	|	Header.Ref,
	|	Header.DocumentNumber,
	|	Header.DocumentDate,
	|	Header.Company,
	|	Header.CompanyVATNumber,
	|	Header.CompanyLogoFile,
	|	Header.Counterparty,
	|	Header.Contract,
	|	Header.CounterpartyContactPerson,
	|	Header.BankAccount,
	|	Header.AmountIncludesVAT,
	|	Header.DocumentCurrency,
	|	Header.ShipmentDate,
	|	SalesOrderWorks.LineNumber,
	|	CatalogProducts.SKU,
	|	CASE
	|		WHEN (CAST(SalesOrderWorks.Content AS STRING(1024))) <> """"
	|			THEN CAST(SalesOrderWorks.Content AS STRING(1024))
	|		WHEN (CAST(CatalogProducts.DescriptionFull AS STRING(1024))) <> """"
	|			THEN CAST(CatalogProducts.DescriptionFull AS STRING(1024))
	|		ELSE CatalogProducts.Description
	|	END,
	|	(CAST(SalesOrderWorks.Content AS STRING(1024))) <> """",
	|	CASE
	|		WHEN CatalogProducts.UseCharacteristics
	|			THEN CatalogCharacteristics.Description
	|		ELSE """"
	|	END,
	|	"""",
	|	CatalogProducts.UseSerialNumbers,
	|	SalesOrderWorks.ConnectionKey,
	|	CatalogUOMClassifier.Description,
	|	CAST(SalesOrderWorks.Quantity * SalesOrderWorks.Factor * SalesOrderWorks.Multiplicity AS NUMBER(15, 3)),
	|	SalesOrderWorks.Price,
	|	SalesOrderWorks.AutomaticDiscountAmount,
	|	SalesOrderWorks.DiscountMarkupPercent,
	|	SalesOrderWorks.Amount,
	|	SalesOrderWorks.VATRate,
	|	SalesOrderWorks.VATAmount,
	|	SalesOrderWorks.Total,
	|	CAST(SalesOrderWorks.Quantity * SalesOrderWorks.Price AS NUMBER(15, 2)),
	|	CatalogProducts.Ref,
	|	CatalogCharacteristics.Ref,
	|	VALUE(Catalog.ProductsBatches.EmptyRef),
	|	CatalogUOMClassifier.Ref,
	|	NULL,
	|	NULL,
	|	NULL
	|FROM
	|	Header AS Header
	|		INNER JOIN Document.SalesOrder.Works AS SalesOrderWorks
	|		ON Header.Ref = SalesOrderWorks.Ref
	|		LEFT JOIN Catalog.Products AS CatalogProducts
	|		ON (SalesOrderWorks.Products = CatalogProducts.Ref)
	|		LEFT JOIN Catalog.ProductsCharacteristics AS CatalogCharacteristics
	|		ON (SalesOrderWorks.Characteristic = CatalogCharacteristics.Ref)
	|		LEFT JOIN Catalog.UOMClassifier AS CatalogUOMClassifier
	|		ON (CatalogProducts.MeasurementUnit = CatalogUOMClassifier.Ref)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	Tabular.Ref AS Ref,
	|	Tabular.DocumentNumber AS DocumentNumber,
	|	Tabular.DocumentDate AS DocumentDate,
	|	Tabular.Company AS Company,
	|	Tabular.CompanyVATNumber AS CompanyVATNumber,
	|	Tabular.CompanyLogoFile AS CompanyLogoFile,
	|	Tabular.Counterparty AS Counterparty,
	|	Tabular.Contract AS Contract,
	|	Tabular.CounterpartyContactPerson AS CounterpartyContactPerson,
	|	Tabular.BankAccount AS BankAccount,
	|	Tabular.AmountIncludesVAT AS AmountIncludesVAT,
	|	Tabular.DocumentCurrency AS DocumentCurrency,
	|	Tabular.ShipmentDate AS ValidUntil,
	|	Tabular.LineNumber AS LineNumber,
	|	Tabular.SKU AS SKU,
	|	Tabular.ProductDescription AS ProductDescription,
	|	Tabular.ContentUsed AS ContentUsed,
	|	Tabular.UseSerialNumbers AS UseSerialNumbers,
	|	Tabular.ConnectionKey AS ConnectionKey,
	|	Tabular.Quantity AS Quantity,
	|	Tabular.Price AS Price,
	|	CASE
	|		WHEN Tabular.AutomaticDiscountAmount = 0
	|			THEN Tabular.DiscountMarkupPercent
	|		WHEN Tabular.Subtotal = 0
	|			THEN 0
	|		ELSE CAST((Tabular.Subtotal - Tabular.Amount) / Tabular.Subtotal * 100 AS NUMBER(15, 2))
	|	END AS DiscountRate,
	|	Tabular.Amount AS Amount,
	|	Tabular.VATRate AS VATRate,
	|	Tabular.VATAmount AS VATAmount,
	|	Tabular.Total AS Total,
	|	Tabular.Subtotal AS Subtotal,
	|	CAST(Tabular.Quantity * Tabular.Price - Tabular.Amount AS NUMBER(15, 2)) AS DiscountAmount,
	|	Tabular.CharacteristicDescription AS CharacteristicDescription,
	|	Tabular.BatchDescription AS BatchDescription,
	|	Tabular.Characteristic AS Characteristic,
	|	Tabular.Batch AS Batch,
	|	Tabular.UOM AS UOM,
	|	Tabular.ReverseChargeApplies AS ReverseChargeApplies,
	|	0 AS Variant,
	|	Tabular.BundleProduct AS BundleProduct,
	|	Tabular.BundleCharacteristic AS BundleCharacteristic,
	|	Tabular.Products AS Products
	|FROM
	|	Tabular AS Tabular
	|
	|ORDER BY
	|	DocumentNumber,
	|	LineNumber
	|TOTALS
	|	MAX(DocumentNumber),
	|	MAX(DocumentDate),
	|	MAX(Company),
	|	MAX(CompanyVATNumber),
	|	MAX(CompanyLogoFile),
	|	MAX(Counterparty),
	|	MAX(Contract),
	|	MAX(CounterpartyContactPerson),
	|	MAX(BankAccount),
	|	MAX(AmountIncludesVAT),
	|	MAX(DocumentCurrency),
	|	MAX(ValidUntil),
	|	COUNT(LineNumber),
	|	SUM(Quantity),
	|	SUM(VATAmount),
	|	SUM(Total),
	|	SUM(Subtotal),
	|	SUM(DiscountAmount),
	|	MAX(ReverseChargeApplies)
	|BY
	|	Ref,
	|	Variant";
	
	Return QueryText;
	
EndFunction

// Document printing procedure.
//
Function PrintQuote(ObjectsArray, PrintObjects, TemplateName, PrintParams) Export
    
    //DisplayPrintOption = GetFunctionalOption("DisplayPrintOptionsBeforePrinting");
    
    DisplayPrintOption = False;
	
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_Quote_A";
	
	Query = New Query();
	Query.Text = GetQueryText(ObjectsArray, TemplateName);
    
    //Query.Text = GetQuoteQueryTextForQuote(False);
	
	// MultilingualSupport
    //If PrintParams = Undefined Then
    //	LanguageCode = NationalLanguageSupportClientServer.DefaultLanguageCode();
    //Else
    //	LanguageCode = PrintParams.LanguageCode;
    //EndIf;
    //
    //If LanguageCode <> CurrentLanguage().LanguageCode Then 
    //	SessionParameters.LanguageCodeForOutput = LanguageCode;
    //EndIf;
    //
    //DriveServer.ChangeQueryTextForCurrentLanguage(Query.Text, LanguageCode);
	// End MultilingualSupport
    
    LanguageCode = "ro";
	
	Query.SetParameter("ObjectsArray", ObjectsArray);
	Query.SetParameter("ReverseChargeAppliesRate", NStr("en='Reverse charge applies';
	                                                    |DE='Steuerschuldumkehr angewendet';
	                                                    |es_ES='Inversión impositiva aplica';
	                                                    |it='Applicare l''inversione di caricamento';
	                                                    |pl='Dotyczy odwrotnego obciążenia';
	                                                    |ro='Se aplică taxa inversă';
	                                                    |ru='Применяется агентский НДС'", LanguageCode));
	Query.SetParameter("AllVariants", StrFind(TemplateName, "AllVariants") > 0);
	ResultArray = Query.ExecuteBatch();
	
	FirstDocument = True;
	
	HeaderVariants = ResultArray[4].Select(QueryResultIteration.ByGroups);
	                                                    
	// Bundles
	TableColumns = ResultArray[4].Columns;
	// End Bundles
	
	While HeaderVariants.Next() Do
		
		Header = HeaderVariants.Select(QueryResultIteration.ByGroups);
		While Header.Next() Do
			
			If Not FirstDocument Then
				SpreadsheetDocument.PutHorizontalPageBreak();
			EndIf;
			FirstDocument = False;
			
			FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
			
			SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_Quote_A";
			
			//Template = PrintManagement.PrintFormTemplate("DataProcessor.PrintQuote.PF_MXL_Quote", LanguageCode);
            
           Template = PrintManagement.PrintFormTemplate(PrintParams.UserPrintTemplate);
			
			#Region PrintQuoteTitleArea
			
			TitleArea = Template.GetArea("Title");
			TitleArea.Parameters.Fill(Header);
			
			If ValueIsFilled(Header.CompanyLogoFile) Then
				
				PictureData = AttachedFiles.GetBinaryFileData(Header.CompanyLogoFile);
				If ValueIsFilled(PictureData) Then
					
					TitleArea.Drawings.Logo.Picture = New Picture(PictureData);
					
				EndIf;
				
			Else
				
				TitleArea.Drawings.Delete(TitleArea.Drawings.Logo);
				
			EndIf;
			
			SpreadsheetDocument.Put(TitleArea);
			
			#EndRegion
			
			#Region PrintQuoteCompanyInfoArea
			
			CompanyInfoArea = Template.GetArea("CompanyInfo");
			
			InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(
				Header.Company, Header.DocumentDate, ,Header.BankAccount, Header.CompanyVATNumber, LanguageCode);
			CompanyInfoArea.Parameters.Fill(InfoAboutCompany);
			BarcodesInPrintForms.AddBarcodeToTableDocument(CompanyInfoArea, Header.Ref);
			SpreadsheetDocument.Put(CompanyInfoArea);
			
			#EndRegion
			
			#Region PrintQuoteCounterpartyInfoArea
			
			CounterpartyInfoArea = Template.GetArea("CounterpartyInfo");
			CounterpartyInfoArea.Parameters.Fill(Header);
			
			InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(
				Header.Counterparty,
				Header.DocumentDate,
				,
				,
				,
				LanguageCode);
			CounterpartyInfoArea.Parameters.Fill(InfoAboutCounterparty);
			
			CounterpartyInfoArea.Parameters.PaymentTerms = PaymentTermsServer.TitleStagesOfPayment(Header.Ref);
			If ValueIsFilled(CounterpartyInfoArea.Parameters.PaymentTerms) Then
				CounterpartyInfoArea.Parameters.PaymentTermsTitle = PaymentTermsServer.PaymentTermsPrintTitle();
			EndIf;
			
			SpreadsheetDocument.Put(CounterpartyInfoArea);
			
			#EndRegion
			
			#Region PrintQuoteCommentArea
			
			CommentArea = Template.GetArea("Comment");
			If Common.HasObjectAttribute("TermsAndConditions", Header.Ref.Metadata()) Then
				CommentArea.Parameters.TermsAndConditions = Common.ObjectAttributeValue(Header.Ref, "TermsAndConditions");
			Else
				CommentArea.Parameters.TermsAndConditions = Common.ObjectAttributeValue(Header.Ref, "Comment");
			EndIf;
			
			SpreadsheetDocument.Put(CommentArea);
			
			#EndRegion
			
			#Region PrintQuoteTotalsAreaPrefill
			
			TotalsAreasArray = New Array;
            
    		If DisplayPrintOption and PrintParams.Discount Then
    		    LineTotalArea = Template.GetArea("LineTotal");
                LineTotalArea.Parameters.Fill(Header);
            Else
                LineTotalArea = Template.GetArea("LineTotalWithoutDiscount");
                LineTotalArea.Parameters.Fill(Header);
                
                // When the "Discount" column is hidden, the results calculate by subtracting the subtotal and the discount.
                LineTotalArea.Parameters.Subtotal = Header.Subtotal-Header.DiscountAmount;
            EndIf;
			
			TotalsAreasArray.Add(LineTotalArea);
			
			#EndRegion
			
			#Region PrintQuoteLinesArea
			
            If DisplayPrintOption Then
                If PrintParams.Discount Then
                    If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                        // Template 1: Hide "Intem #", show "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutCode");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutCode");
                    Else
                        // Template 2: Show all columns
                        LineHeaderArea = Template.GetArea("LineHeader");
                        LineSectionArea	= Template.GetArea("LineSection");
                    EndIf;
                Else
                    If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                        // Template 3: Hide "Intem #", hide "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutItemAndDiscount");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutItemAndDiscount");
                    Else
                        // Template 4: Show "Intem #", hide "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutDiscount");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutDiscount");
                    EndIf;
                EndIf;
            Else
                LineHeaderArea = Template.GetArea("LineHeader");
        		LineSectionArea	= Template.GetArea("LineSection");
            EndIf;
            
       		SpreadsheetDocument.Put(LineHeaderArea);
			
			SeeNextPageArea	= Template.GetArea("SeeNextPage");
			EmptyLineArea	= Template.GetArea("EmptyLine");
			PageNumberArea	= Template.GetArea("PageNumber");
			
			PageNumber = 0;
			
			AreasToBeChecked = New Array;
			
			// Bundles
			TableInventoty = BundlesServer.AssemblyTableByBundles(Header.Ref, Header, TableColumns, LineTotalArea);
			EmptyColor = LineSectionArea.CurrentArea.TextColor;
			// End Bundles
			
			For Each TabSelection In TableInventoty Do
				
				LineSectionArea.Parameters.Fill(TabSelection);
				
				DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection);
                
                // Display selected codes if functional option is turned on.
                If DisplayPrintOption Then
                    CodesPresentation = PrintManagementServerCallDrive.GetCodesPresentation(PrintParams, TabSelection.Products);
                    If PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.SeparateColumn Then
                        LineSectionArea.Parameters.SKU = CodesPresentation;
                    ElsIf PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.ProductColumn Then
                        LineSectionArea.Parameters.ProductDescription = LineSectionArea.Parameters.ProductDescription + Chars.CR + CodesPresentation;
                    EndIf;
                EndIf;
                
				// Bundles
                If DisplayPrintOption Then
                    If PrintParams.Discount Then
                        If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                            LineSectionArea.Areas.LineSectionWithoutCode.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        Else
                            LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        EndIf;
                    Else
                        // Recalculate the price to display the correct information with a hidden discount.
                        LineSectionArea.Parameters.Price = TabSelection.Amount/TabSelection.Quantity;
                        
                        If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                            LineSectionArea.Areas.LineSectionWithoutItemAndDiscount.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        Else
                            LineSectionArea.Areas.LineSectionWithoutDiscount.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        EndIf;
                    EndIf;
                Else
                    LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                EndIf;
				// End Bundles
				
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(LineSectionArea);
				For Each Area In TotalsAreasArray Do
					AreasToBeChecked.Add(Area);
				EndDo;
				AreasToBeChecked.Add(PageNumberArea);
				
				If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					SpreadsheetDocument.Put(LineSectionArea);
					
				Else
					
					SpreadsheetDocument.Put(SeeNextPageArea);
					
					AreasToBeChecked.Clear();
					AreasToBeChecked.Add(EmptyLineArea);
					AreasToBeChecked.Add(PageNumberArea);
					
					For i = 1 To 50 Do
						
						If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
							Or i = 50 Then
							
							PageNumber = PageNumber + 1;
							PageNumberArea.Parameters.PageNumber = PageNumber;
							SpreadsheetDocument.Put(PageNumberArea);
							Break;
							
						Else
							
							SpreadsheetDocument.Put(EmptyLineArea);
							
						EndIf;
						
					EndDo;
					
					SpreadsheetDocument.PutHorizontalPageBreak();
					SpreadsheetDocument.Put(TitleArea);
					SpreadsheetDocument.Put(LineHeaderArea);
					SpreadsheetDocument.Put(LineSectionArea);
					
				EndIf;
				
			EndDo;
			
			#EndRegion
			
			#Region PrintQuoteTotalsArea
			
			For Each Area In TotalsAreasArray Do
				
				SpreadsheetDocument.Put(Area);
				
            EndDo;
            
            #Region PrintAdditionalAttributes
            If DisplayPrintOption And PrintParams.AdditionalAttributes And PrintManagementServerCallDrive.HasAdditionalAttributes(Header.Ref) Then
                
                SpreadsheetDocument.Put(EmptyLineArea);
                
                AddAttribHeader = Template.GetArea("AdditionalAttributesStaticHeader");
                SpreadsheetDocument.Put(AddAttribHeader);
                
                SpreadsheetDocument.Put(EmptyLineArea);
                
                AddAttribHeader = Template.GetArea("AdditionalAttributesHeader");
                SpreadsheetDocument.Put(AddAttribHeader);
                
                AddAttribRow = Template.GetArea("AdditionalAttributesRow");
                
                For each Attr In Header.Ref.AdditionalAttributes Do
                    AddAttribRow.Parameters.AddAttributeName = Attr.Property.Title;
                    AddAttribRow.Parameters.AddAttributeValue = Attr.Value;
                    SpreadsheetDocument.Put(AddAttribRow);
                EndDo;
            EndIf;
            #EndRegion
            
			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(EmptyLineArea);
			AreasToBeChecked.Add(PageNumberArea);
			
			For i = 1 To 50 Do
				
				If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
					Or i = 50 Then
					
					PageNumber = PageNumber + 1;
					PageNumberArea.Parameters.PageNumber = PageNumber;
					SpreadsheetDocument.Put(PageNumberArea);
					Break;
					
				Else
					
					SpreadsheetDocument.Put(EmptyLineArea);
					
				EndIf;
				
			EndDo;
			
			#EndRegion
			
			PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
			
		EndDo;
		
	EndDo;
	
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;
	
EndFunction

// Document printing procedure
//
Function PrintProformaInvoice(ObjectsArray, PrintObjects, TemplateName, PrintParams) Export
    
    //DisplayPrintOption = GetFunctionalOption("DisplayPrintOptionsBeforePrinting");
    
    LanguageCode = "ro";
    DisplayPrintOption = False;
    
	SpreadsheetDocument = New SpreadsheetDocument;
	SpreadsheetDocument.PrintParametersKey = "PrintParameters_ProformaInvoice_A";
	
	Query = New Query();
	Query.SetParameter("ObjectsArray", ObjectsArray);
	Query.SetParameter("AllVariants", StrFind(TemplateName, "AllVariants") > 0);
	
	Query.Text = GetQueryText(ObjectsArray, TemplateName);
	
	// MultilingualSupport
	
    //If PrintParams = Undefined Then
    //	LanguageCode = NationalLanguageSupportClientServer.DefaultLanguageCode();
    //Else
    //	LanguageCode = PrintParams.LanguageCode;
    //EndIf;
    //
    //If LanguageCode <> CurrentLanguage().LanguageCode Then 
    //	SessionParameters.LanguageCodeForOutput = LanguageCode;
    //EndIf;
    //
    //DriveServer.ChangeQueryTextForCurrentLanguage(Query.Text, LanguageCode);
	
	// End MultilingualSupport
	
	ResultArray = Query.ExecuteBatch();
	
	FirstDocument = True;
	
	HeaderVariants			= ResultArray[4].Select(QueryResultIteration.ByGroups);
	TaxesHeaderSelVariants	= ResultArray[5].Select(QueryResultIteration.ByGroups);
	TotalLineNumber			= ResultArray[6].Unload();
	
	// Bundles
	TableColumns = ResultArray[4].Columns;
	// End Bundles
	
	While HeaderVariants.Next() Do
		
		Header = HeaderVariants.Select(QueryResultIteration.ByGroups);
		While Header.Next() Do
			
			If Not FirstDocument Then
				SpreadsheetDocument.PutHorizontalPageBreak();
			EndIf;
			FirstDocument = False;
			
			FirstLineNumber = SpreadsheetDocument.TableHeight + 1;
			
			SpreadsheetDocument.PrintParametersName = "PRINT_PARAMETERS_ProformaInvoice_A";
			
			//Template = PrintManagement.PrintFormTemplate("DataProcessor.PrintQuote.PF_MXL_ProformaInvoice", LanguageCode);
            Template = PrintManagement.PrintFormTemplate(PrintParams.UserPrintTemplate);
			
			#Region PrintProformaInvoiceTitleArea
			
			TitleArea = Template.GetArea("Title");
			TitleArea.Parameters.Fill(Header);
            
            If DisplayPrintOption Then
                TitleArea.Parameters.OriginalDuplicate = ?(PrintParams.OriginalCopy,
					NStr("en='ORIGINAL';
					     |DE='ORIGINAL';
					     |es_ES='ORIGINAL';
					     |it='ORIGINALE';
					     |pl='ORYGINAŁ';
					     |ro='ORIGINAL';
					     |ru='ОРИГИНАЛ'", LanguageCode),
					NStr("en='COPY';
					     |DE='KOPIE';
					     |es_ES='COPIA';
					     |it='COPIA';
					     |pl='KOPIA';
					     |ro='COPY';
					     |ru='КОПИЯ'", LanguageCode));
    		EndIf;
            
			If ValueIsFilled(Header.CompanyLogoFile) Then
				
				PictureData = AttachedFiles.GetBinaryFileData(Header.CompanyLogoFile);
				If ValueIsFilled(PictureData) Then
					
					TitleArea.Drawings.Logo.Picture = New Picture(PictureData);
					
				EndIf;
				
			Else
				
				TitleArea.Drawings.Delete(TitleArea.Drawings.Logo);
				
			EndIf;
			
			SpreadsheetDocument.Put(TitleArea);
			
			#EndRegion
			
			#Region PrintProformaInvoiceCompanyInfoArea
			
			CompanyInfoArea = Template.GetArea("CompanyInfo");
			
			InfoAboutCompany = DriveServer.InfoAboutLegalEntityIndividual(
				Header.Company, Header.DocumentDate, , Header.BankAccount, Header.CompanyVATNumber, LanguageCode);
			CompanyInfoArea.Parameters.Fill(InfoAboutCompany);
			BarcodesInPrintForms.AddBarcodeToTableDocument(CompanyInfoArea, Header.Ref);
			SpreadsheetDocument.Put(CompanyInfoArea);
			
			#EndRegion
			
			#Region PrintProformaInvoiceCounterpartyInfoArea
			
			CounterpartyInfoArea = Template.GetArea("CounterpartyInfo");
			CounterpartyInfoArea.Parameters.Fill(Header);
			
			InfoAboutCounterparty = DriveServer.InfoAboutLegalEntityIndividual(
				Header.Counterparty,
				Header.DocumentDate,
				,
				,
				,
				LanguageCode);
			CounterpartyInfoArea.Parameters.Fill(InfoAboutCounterparty);
			
			TitleParameters = New Structure;
			TitleParameters.Insert("TitleShipTo", NStr("en='Ship to';
			                                           |DE='Versand an';
			                                           |es_ES='Enviar a';
			                                           |it='Spedire a';
			                                           |pl='Dostawa do';
			                                           |ro='Expediat către ';
			                                           |ru='Грузополучатель'", LanguageCode));
			TitleParameters.Insert("TitleShipDate", NStr("en='Ship date';
			                                             |DE='Versanddatum';
			                                             |es_ES='Fecha de envío';
			                                             |it='Data di spedizione';
			                                             |pl='Data wysyłki';
			                                             |ro='Data expedierii';
			                                             |ru='Дата доставки'", LanguageCode));
			
			If Header.DeliveryOption = Enums.DeliveryOptions.SelfPickup Then
				
				InfoAboutPickupLocation	= DriveServer.InfoAboutLegalEntityIndividual(
					Header.StructuralUnit,
					Header.DocumentDate,
					,
					,
					,
					LanguageCode);
				ResponsibleEmployee		= InfoAboutPickupLocation.ResponsibleEmployee;
				
				If NOT IsBlankString(InfoAboutPickupLocation.FullDescr) Then
					CounterpartyInfoArea.Parameters.FullDescrShipTo = InfoAboutPickupLocation.FullDescr;
				EndIf;
				
				If NOT IsBlankString(InfoAboutPickupLocation.DeliveryAddress) Then
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutPickupLocation.DeliveryAddress;
				EndIf;
				
				If ValueIsFilled(ResponsibleEmployee) Then
					CounterpartyInfoArea.Parameters.CounterpartyContactPerson = ResponsibleEmployee.Description;
				EndIf;
				
				If NOT IsBlankString(InfoAboutPickupLocation.PhoneNumbers) Then
					CounterpartyInfoArea.Parameters.PhoneNumbers = InfoAboutPickupLocation.PhoneNumbers;
				EndIf;
				
				TitleParameters.TitleShipTo		= NStr("en='Pickup location';
						                                  |DE='Abholort';
						                                  |es_ES='Ubicación de recogida';
						                                  |it='Punto di presa';
						                                  |pl='Wybierz miejsce';
						                                  |ro='Punctul de preluare';
						                                  |ru='Место самовывоза'", LanguageCode);
				TitleParameters.TitleShipDate	= NStr("en='Pickup date';
					                                    |DE='Abholdatum';
					                                    |es_ES='Fecha de recogida';
					                                    |it='Data di presa';
					                                    |pl='Wybierz datę';
					                                    |ro='Data ridicării';
					                                    |ru='Дата самовывоза'", LanguageCode);
				
			Else
				
				InfoAboutShippingAddress	= DriveServer.InfoAboutShippingAddress(Header.ShippingAddress);
				InfoAboutContactPerson		= DriveServer.InfoAboutContactPerson(Header.CounterpartyContactPerson);
			
				If NOT IsBlankString(InfoAboutShippingAddress.DeliveryAddress) Then
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutShippingAddress.DeliveryAddress;
				EndIf;
				
				If NOT IsBlankString(InfoAboutContactPerson.PhoneNumbers) Then
					CounterpartyInfoArea.Parameters.PhoneNumbers = InfoAboutContactPerson.PhoneNumbers;
				EndIf;
				
			EndIf;
			
			CounterpartyInfoArea.Parameters.Fill(TitleParameters);
			
			If IsBlankString(CounterpartyInfoArea.Parameters.DeliveryAddress) Then
				
				If Not IsBlankString(InfoAboutCounterparty.ActualAddress) Then
					
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutCounterparty.ActualAddress;
					
				Else
					
					CounterpartyInfoArea.Parameters.DeliveryAddress = InfoAboutCounterparty.LegalAddress;
					
				EndIf;
				
			EndIf;
			
			CounterpartyInfoArea.Parameters.PaymentTerms = PaymentTermsServer.TitleStagesOfPayment(Header.Ref);
			If ValueIsFilled(CounterpartyInfoArea.Parameters.PaymentTerms) Then
				CounterpartyInfoArea.Parameters.PaymentTermsTitle = PaymentTermsServer.PaymentTermsPrintTitle();
			EndIf;
			
			SpreadsheetDocument.Put(CounterpartyInfoArea);
			
			#EndRegion
			
			#Region PrintProformaInvoiceCommentArea
			
			CommentArea = Template.GetArea("Comment");
			CommentArea.Parameters.Comment = Common.ObjectAttributeValue(Header.Ref, "Comment");
			SpreadsheetDocument.Put(CommentArea);
			
			#EndRegion
			
			#Region PrintProformaInvoiceTotalsAndTaxesAreaPrefill
			
			TotalsAndTaxesAreasArray = New Array;
            
    		If DisplayPrintOption and PrintParams.Discount Then
    		    LineTotalArea = Template.GetArea("LineTotal");
                LineTotalArea.Parameters.Fill(Header);
            Else
                LineTotalArea = Template.GetArea("LineTotalWithoutDiscount");
                LineTotalArea.Parameters.Fill(Header);
                
                // When the "Discount" column is hidden, the results calculate by subtracting the subtotal and the discount.
                LineTotalArea.Parameters.Subtotal = Header.Subtotal-Header.DiscountAmount;
            EndIf;
			
			SearchStructure = New Structure("Ref, Variant", Header.Ref, Header.Variant);
			
			SearchArray = TotalLineNumber.FindRows(SearchStructure);
			If SearchArray.Count() > 0 Then
				LineTotalArea.Parameters.Quantity	= SearchArray[0].Quantity;
				LineTotalArea.Parameters.LineNumber	= SearchArray[0].LineNumber;
			Else
				LineTotalArea.Parameters.Quantity	= 0;
				LineTotalArea.Parameters.LineNumber	= 0;
			EndIf;
			
			TotalsAndTaxesAreasArray.Add(LineTotalArea);
			
			TaxesHeaderSelVariants.Reset();
			If TaxesHeaderSelVariants.FindNext(New Structure("Ref", Header.Ref)) Then
				
				TaxesHeaderSel = TaxesHeaderSelVariants.Select(QueryResultIteration.ByGroups);
				If TaxesHeaderSel.FindNext(New Structure("Variant", Header.Variant)) Then
					
					TaxSectionHeaderArea = Template.GetArea("TaxSectionHeader");
					TotalsAndTaxesAreasArray.Add(TaxSectionHeaderArea);
					
					TaxesSel = TaxesHeaderSel.Select();
					While TaxesSel.Next() Do
						
						TaxSectionLineArea = Template.GetArea("TaxSectionLine");
						TaxSectionLineArea.Parameters.Fill(TaxesSel);
						TotalsAndTaxesAreasArray.Add(TaxSectionLineArea);
						
					EndDo;
					
				EndIf;
				
			EndIf;
			
			#EndRegion
			
			#Region PrintProformaInvoiceLinesArea
			
            If DisplayPrintOption Then
                If PrintParams.Discount Then
                    If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                        // Template 1: Hide "Intem #", show "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutCode");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutCode");
                    Else
                        // Template 2: Show all columns
                        LineHeaderArea = Template.GetArea("LineHeader");
                        LineSectionArea	= Template.GetArea("LineSection");
                    EndIf;
                Else
                    If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                        // Template 3: Hide "Intem #", hide "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutItemAndDiscount");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutItemAndDiscount");
                    Else
                        // Template 4: Show "Intem #", hide "Disc.rate"
                        LineHeaderArea = Template.GetArea("LineHeaderWithoutDiscount");
                        LineSectionArea	= Template.GetArea("LineSectionWithoutDiscount");
                    EndIf;
                EndIf;
            Else
                LineHeaderArea = Template.GetArea("LineHeader");
        		LineSectionArea	= Template.GetArea("LineSection");
            EndIf;
            
       		SpreadsheetDocument.Put(LineHeaderArea);
            
            SeeNextPageArea	= Template.GetArea("SeeNextPage");
			EmptyLineArea	= Template.GetArea("EmptyLine");
			PageNumberArea	= Template.GetArea("PageNumber");
			
			PageNumber = 0;
			
			AreasToBeChecked = New Array;
			
			// Bundles
			TableInventoty = BundlesServer.AssemblyTableByBundles(Header.Ref, Header, TableColumns, LineTotalArea);
			EmptyColor = LineSectionArea.CurrentArea.TextColor;
			// End Bundles
			
			For Each TabSelection In TableInventoty Do
				
				If TabSelection.IsFreightService = True Then
					Continue;
				EndIf;
				
				LineSectionArea.Parameters.Fill(TabSelection);
				
				DriveClientServer.ComplimentProductDescription(LineSectionArea.Parameters.ProductDescription, TabSelection);
                
                // Display selected codes if functional option is turned on.
                If DisplayPrintOption Then
                    CodesPresentation = PrintManagementServerCallDrive.GetCodesPresentation(PrintParams, TabSelection.Products);
                    If PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.SeparateColumn Then
                        LineSectionArea.Parameters.SKU = CodesPresentation;
                    ElsIf PrintParams.CodesPosition = Enums.CodesPositionInPrintForms.ProductColumn Then
                        LineSectionArea.Parameters.ProductDescription = LineSectionArea.Parameters.ProductDescription + Chars.CR + CodesPresentation;
                    EndIf;
                EndIf;
                
				// Bundles
                If DisplayPrintOption Then
                    If PrintParams.Discount Then
                        If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                            LineSectionArea.Areas.LineSectionWithoutCode.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        Else
                            LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        EndIf;
                    Else
                        // Recalculate the price to display the correct information with a hidden discount.
                        LineSectionArea.Parameters.Price = TabSelection.Amount/TabSelection.Quantity;
                        
                        If PrintParams.CodesPosition <> Enums.CodesPositionInPrintForms.SeparateColumn Then
                            LineSectionArea.Areas.LineSectionWithoutItemAndDiscount.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        Else
                            LineSectionArea.Areas.LineSectionWithoutDiscount.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                        EndIf;
                    EndIf;
                Else
                    LineSectionArea.Areas.LineSection.TextColor = BundlesServer.GetBundleComponentsColor(TabSelection, EmptyColor);
                EndIf;
				// End Bundles
				
				AreasToBeChecked.Clear();
				AreasToBeChecked.Add(LineSectionArea);
				For Each Area In TotalsAndTaxesAreasArray Do
					AreasToBeChecked.Add(Area);
				EndDo;
				AreasToBeChecked.Add(PageNumberArea);
				
				If Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked) Then
					
					SpreadsheetDocument.Put(LineSectionArea);
					
				Else
					
					SpreadsheetDocument.Put(SeeNextPageArea);
					
					AreasToBeChecked.Clear();
					AreasToBeChecked.Add(EmptyLineArea);
					AreasToBeChecked.Add(PageNumberArea);
					
					For i = 1 To 50 Do
						
						If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
							Or i = 50 Then
							
							PageNumber = PageNumber + 1;
							PageNumberArea.Parameters.PageNumber = PageNumber;
							SpreadsheetDocument.Put(PageNumberArea);
							Break;
							
						Else
							
							SpreadsheetDocument.Put(EmptyLineArea);
							
						EndIf;
						
					EndDo;
					
					SpreadsheetDocument.PutHorizontalPageBreak();
					SpreadsheetDocument.Put(TitleArea);
					SpreadsheetDocument.Put(LineHeaderArea);
					SpreadsheetDocument.Put(LineSectionArea);
					
				EndIf;
				
			EndDo;
			
			#EndRegion
			
			#Region PrintProformaInvoiceTotalsAndTaxesArea
			
			For Each Area In TotalsAndTaxesAreasArray Do
				
				SpreadsheetDocument.Put(Area);
				
			EndDo;
            
            #Region PrintAdditionalAttributes
            If DisplayPrintOption And PrintParams.AdditionalAttributes And PrintManagementServerCallDrive.HasAdditionalAttributes(Header.Ref) Then
                
                SpreadsheetDocument.Put(EmptyLineArea);
                
                AddAttribHeader = Template.GetArea("AdditionalAttributesStaticHeader");
                SpreadsheetDocument.Put(AddAttribHeader);
                
                SpreadsheetDocument.Put(EmptyLineArea);
                
                AddAttribHeader = Template.GetArea("AdditionalAttributesHeader");
                SpreadsheetDocument.Put(AddAttribHeader);
                
                AddAttribRow = Template.GetArea("AdditionalAttributesRow");
                
                For each Attr In Header.Ref.AdditionalAttributes Do
                    AddAttribRow.Parameters.AddAttributeName = Attr.Property.Title;
                    AddAttribRow.Parameters.AddAttributeValue = Attr.Value;
                    SpreadsheetDocument.Put(AddAttribRow);
                EndDo;
            EndIf;
            #EndRegion
            
			AreasToBeChecked.Clear();
			AreasToBeChecked.Add(EmptyLineArea);
			AreasToBeChecked.Add(PageNumberArea);
			
			For i = 1 To 50 Do
				
				If Not Common.SpreadsheetDocumentFitsPage(SpreadsheetDocument, AreasToBeChecked)
					Or i = 50 Then
					
					PageNumber = PageNumber + 1;
					PageNumberArea.Parameters.PageNumber = PageNumber;
					SpreadsheetDocument.Put(PageNumberArea);
					Break;
					
				Else
					
					SpreadsheetDocument.Put(EmptyLineArea);
					
				EndIf;
				
			EndDo;
			
			#EndRegion
			
			PrintManagement.SetDocumentPrintArea(SpreadsheetDocument, FirstLineNumber, PrintObjects, Header.Ref);
			
		EndDo;
		
	EndDo;
	
	SpreadsheetDocument.FitToPage = True;
	
	Return SpreadsheetDocument;

EndFunction

#EndRegion

#EndIf
