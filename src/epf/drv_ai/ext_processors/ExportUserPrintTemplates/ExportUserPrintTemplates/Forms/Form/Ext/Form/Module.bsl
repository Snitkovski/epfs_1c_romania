﻿&AtClient
Procedure ExportClient(Command)
    
    If Not CheckFilling() Then
        Return
    EndIf;
    
    Files = ExportAtServer();
    
    If Files = Undefined Then
        
        MessageText = NStr("en = 'There no user templates for export!'; ro = 'There no user templates for export!'; ru = 'Нет пользовательских шаблонов для выгрузки!'");
        
        ShowMessageBox(Undefined,MessageText);
        
    Else
        
       ND = New NotifyDescription("EndGetFiles",ThisObject);
        
       BeginGetFilesFromServer(ND,Files,LocalDestination,New GetFilesArchiveParameters("UserTemplates.zip",GetFilesArchiveMode.GetArchiveAlways));
        
    EndIf;

EndProcedure

&AtClient
Procedure EndGetFiles(ReceivedFiles,AdditionalParameters) Export
    
    MessageText = NStr("en = 'User templates stored to '; ro = 'User templates stored to '; ru = 'Пользовательские шаблоны сохранены в '");
    
    ShowMessageBox(Undefined, MessageText + ReceivedFiles[0].FullName)
    
EndProcedure

&AtServer
Function ExportAtServer()
    
   Query = New Query;
   
   Query.Text = "SELECT
                |   UserPrintTemplates.TemplateName AS TemplateName,
                |   UserPrintTemplates.Object AS Object,
                |   UserPrintTemplates.Template AS Template
                |FROM
                |   InformationRegister.UserPrintTemplates AS UserPrintTemplates";

   Result = Query.Execute();
   
   If Result.IsEmpty() Then
       
       Return Undefined
       
   Else
       
     ListFiles = New Array;
       
     Selection = Result.Select();
     
     While Selection.Next() Do
     
     	FileDescription = New TransferableFileDescription;
        
        TD = Selection.Template.Get();
        
        tmpFile = GetTempFileName(".mxl");
        
        TD.Write(tmpFile);
        
        FileDescription.Location = PutToTempStorage(New BinaryData(tmpFile),UUID);
        FileDescription.Name = StrTemplate("%1_%2.mxl",Selection.Object,Selection.TemplateName);
        
        ListFiles.Add(FileDescription);
        
        DeleteFiles(tmpFile);
     
     EndDo;
     
     Return ListFiles
     
   EndIf;
    
EndFunction

&AtClient
Procedure LocalDestinationStartChoice(Item, ChoiceData, StandardProcessing)
    
    StandardProcessing = False;
    
    Dialog = New FileDialog(FileDialogMode.ChooseDirectory);
    Dialog.Multiselect = False;
    
    ND = New NotifyDescription("EndChooseDirectory",ThisObject);
    
    Dialog.Show(ND);
    
EndProcedure

&AtClient
Procedure EndChooseDirectory(SelectedFiles,AdditionalParameters) Export
    
    If SelectedFiles = Undefined Then
        Return
    EndIf;
    
    LocalDestination = SelectedFiles[0];
    
    If Not Right(LocalDestination,1) = "\" Then
    	LocalDestination = LocalDestination + "\"
    EndIf;
    
EndProcedure
