﻿Function ExternalDataProcessorInformation() Export
	
	Info	= New Structure("Kind, Commands, SafeMode, Purpose, Description, Version, Information, StandardSubsystemLibraryVersion",
							"AdditionalDataProcessor", New ValueTable, True, New Array);
	Info.SafeMode		= False;
	Info.Description	= NStr("en='Uploading products with pictures and accompanying data into the database and any documents from MS Excel, LO Calc, CSV';pl='Ładowanie nomenklatury ze zdjęciami i powiązanych danych do bazy danych oraz wszelkiich dokumentów z MS Excel, LO Calc, CSV';ru='(УНФ1.6) Загрузка номенклатуры c картинками и сопутствующими данными в базу и любые документы из MS Excel, LO Calc, CSV';ro='Uploading products with pictures and accompanying data into the database and any documents from MS Excel, LO Calc, CSV'");
	Info.Version		= "5.14";
	Info.Information	= NStr("en='Uploading products with pictures and accompanying data into the database and any documents from MS Excel, LO Calc, CSV';pl='Ładowanie nomenklatury ze zdjęciami i powiązanych danych do bazy danych oraz wszelkiich dokumentów z MS Excel, LO Calc, CSV';ru='(УНФ1.6) Загрузка номенклатуры c картинками и сопутствующими данными в базу и любые документы из MS Excel, LO Calc, CSV';ro='Uploading products with pictures and accompanying data into the database and any documents from MS Excel, LO Calc, CSV'");
	
	Columns	= Info.Commands.Columns;
	TypeString	= New TypeDescription("String");
	Columns.Add("Presentation", TypeString);
	Columns.Add("ID", TypeString);
	Columns.Add("Use", TypeString);
	Columns.Add("Modifier",   TypeString);
	Columns.Add("ShowNotification", New TypeDescription("Boolean"));
	
	// The only command to do - we define by type transferred
	Command	= Info.Commands.Add();
	Command.Presentation	= NStr("en='Uploading products with pictures and accompanying data into the database and any documents from MS Excel, LO Calc, CSV';pl='Ładowanie nomenklatury ze zdjęciami i powiązanych danych do bazy danych oraz wszelkiich dokumentów z MS Excel, LO Calc, CSV';ru='(УНФ1.6) Загрузка номенклатуры c картинками и сопутствующими данными в базу и любые документы из MS Excel, LO Calc, CSV';ro='Uploading products with pictures and accompanying data into the database and any documents from MS Excel, LO Calc, CSV'");
	Command.ID	= "OpenFormFormManaged";
	Command.Use	= "OpeningForm";
	
	Return Info;
	
EndFunction

