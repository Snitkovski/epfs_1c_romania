﻿
&AtServer
Var FolderValueTable;

&Server
Var DataListFromDatabase;

&AtClient
Var FTP;

#Region BasicFormHandlers

&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
	
	Items.Pages.PagesRepresentation	= FormPagesRepresentation.None;
	
	FillServiceData();
		
	SystemInfo	= New SystemInfo;
	IsLinux		= ?(SystemInfo.PlatformType = PlatformType.Linux_x86 OR SystemInfo.PlatformType = PlatformType.Linux_x86, True, False);
	
	SheetNumber		= 1;
	LineNumber		= 1;
	LineNumberEnd	= 65536;
	GroupSeparator	= "//";
	
EndProcedure

&AtClient
Procedure BeforeClose(Cancel, Exit, WarningText, StandardProcessing)
	
	If NOT CanCloseForm Then
		
		Cancel			= True;
		Notification	= New NotifyDescription("QuestionClosingFormEnd",ThisObject);
		Text			= Nstr("en='Are you sure you want to close the DataProcessor? All unsaved settings will be lost!';pl='Czy na pewno chcesz zamknąć opracowanie? Wszystkie niezapisane ustawienia zostaną utracone!';ru='Вы уверены, что хотите закрыть обработку? Все несохраненные настройки будут утеряны!';ro='Are you sure you want to close the DataProcessor? All unsaved settings will be lost!'");
		ShowQueryBox(Notification,Text,QuestionDialogMode.YesNo);
		
	EndIf;
	
EndProcedure

&AtClient
Procedure QuestionClosingFormEnd (Result,Parameters) Export
	
	If Result		= DialogReturnCode.No Then Return; EndIf;
	CanCloseForm	= True;
	Close();
	
EndProcedure

&AtServer
Procedure FillServiceData()
	
	If Metadata.Constants.Find("UseCharacteristics") <> Undefined Then
		UseCharacteristics	= Constants.UseCharacteristics.Get();
	EndIf;
	
	If Metadata.Constants.Find("UseSeveralUnitsForProduct") <> Undefined Then
		UsePackings	= Constants.UseSeveralUnitsForProduct.Get();
	EndIf;

	If Metadata.Constants.Find("UseSerialNumber") <> Undefined Then
		UseSeries	= Constants.UseSerialNumber.Get();
	EndIf;
	
	If Metadata.Constants.Find("FunctionalOptionCustomsDeclarationAccounting") <> Undefined Then
		UseCustomsDeclaration	= Constants.FunctionalOptionCustomsDeclarationAccounting.Get();
	EndIf;
	
	If Metadata.Constants.Find("ToStoreFilesInVolumesOnDisk") <> Undefined Then
		ToStoreFilesInVolumes	= Constants.ToStoreFilesInVolumesOnDisk.Get();
	EndIf;
	
	If Metadata.Constants.Find("UseExchangeOfElectronicDocuments") <> Undefined Then
		UseSupplierProducts		= Constants.UseExchangeOfElectronicDocuments.Get();
	EndIf;
	
	//Fill the map structure of metadata object name in the database and code
	FullNameStructure	= New Structure;
	FullNameStructure.Insert("Products",				"Catalog.Products");
	FullNameStructure.Insert("AdditionalAttributes",	"ChartOfCharacteristicTypes.AdditionalAttributesAndInformation");
	FullNameStructure.Insert("Characteristics",			"Catalog.ProductsCharacteristics");
	FullNameStructure.Insert("Series",					"Catalog.SerialNumbers");
	FullNameStructure.Insert("CustomsDeclaration",		"Catalog.CustomsDeclarationNumbers");
	FullNameStructure.Insert("Barcodes",				"InformationRegister.Barcodes");
	FullNameStructure.Insert("Packings",				"Catalog.UOM");
	FullNameStructure.Insert("Pictures",				"Catalog.ProductsAttachedFiles");
	FullNameStructure.Insert("PropertyValues",			"Catalog.AdditionalValues");
	FullNameStructure.Insert("AdditionalAttributeSets",	"Catalog.AdditionalAttributesAndInfoSets");
	FullNameStructure.Insert("SupplierProducts",		"Catalog.SuppliersProducts");
	FullNameStructure.Insert("Document",				"Document.");
	FullNameStructure.Insert("Prices",					"InformationRegister.Prices");
	FullNameStructure.Insert("PriceKind",				"CatalogRef.PriceTypes");
	
	//Series
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.Series;
	NewValueTableRow.Name		= "Owner";
	
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.Series;
	NewValueTableRow.Name		= "Description";
	
	//PropertyValues
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.PropertyValues;
	NewValueTableRow.Name		= "Description";
	
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.PropertyValues;
	NewValueTableRow.Name		= "Owner";
	
	//CustomsDeclaration
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.CustomsDeclaration;
	NewValueTableRow.Name		= "Code";
	
	//Packings
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.Packings;
	NewValueTableRow.Name		= "Description";
	
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.Packings;
	NewValueTableRow.Name		= "Owner";
	
	//Pictures
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.Pictures;
	NewValueTableRow.Name		= "Description";
	
	NewValueTableRow			= SearchFieldTable.Add();
	NewValueTableRow.FullName	= FullNameStructure.Pictures;
	NewValueTableRow.Name		= "FileOwner";

EndProcedure
 
#EndRegion

#Region SettingAndLoadingData

&AtClient
Procedure PathToFileStartChoice(Item, ChoiceData, StandardProcessing)
	
	FileDialog			= New FileDialog(FileDialogMode.Open);
	FileDialog.Filter	= "Accepted file|*.xls;*.xlsx;*.xlsm;*.xlsm;*.ods;*.ots;*.csv";
	Notification		= New NotifyDescription("PathToFileStartChoiceEnd",ThisObject);
	
	FileDialog.Show(Notification);
	
EndProcedure

&AtClient
Procedure PathToFileStartChoiceEnd (FileArray,Parameters) Export
	
	If FileArray	= Undefined Then Return; EndIf;
	
	PathToFile		= FileArray[0];
	File			= New File(PathToFile);
	FileExtention	= Lower(File.Extension);
	FileDataRead	= False;
	
EndProcedure

//Reading file data
&AtClient
Function CheckProgramAvailabilityToReadFile()
	
	IsMSExcel				= False;
	IsLibreoffice			= False;
	IsFileWithDelimiters	= False;

	Excel			= Undefined;
	ServiceManager	= Undefined;
	
	If FileExtention	= ".xls" OR FileExtention = ".xlsx" OR FileExtention = ".xlsm" Then
		
		Try
			Excel		= New COMObject ("Excel.Application");
			IsMSExcel	= True;
		Except EndTry;
		
		If Excel		= Undefined Then
			Try
				ServiceManager	= New COMObject("com.sun.star.ServiceManager");
			Except EndTry;
		EndIf;
		
	EndIf;
	
	If FileExtention	= ".ods" OR FileExtention	= ".ots" Then
		
		Try
			ServiceManager	= New COMObject("com.sun.star.ServiceManager");
			IsLibreoffice	= True;
		Except EndTry;
		
	EndIf;
	
	If FileExtention	= ".csv" Then
		IsFileWithDelimiters	=True;
	EndIf;
	
	If NOT IsLibreoffice AND NOT IsMSExcel AND NOT IsFileWithDelimiters Then
		Message(Nstr("en='No programs found that can read the format file ';pl='Nie znaleziono programu, który może odczytać plik formatu ';ru='Не найдено программы, которая может открыть данный тип файла ';ro='No programs found that can read the format file '") + FileExtention + "!");
		Return Undefined;
	EndIf;

	ResponseStructure	= New Structure("Excel, ServiceManager, IsFileWithDelimiters", Excel, ServiceManager, IsFileWithDelimiters);
	Return ResponseStructure;
	
EndFunction

&AtClient
Procedure ReadDataFromFile()
	
	//Check what programs are on the computer / server and, depending on this, choose the method of reading
	IsMSExcel				= False;
	IsLibreoffice			= False;
	Excel					= Undefined;
	IsFileWithDelimiters	= False;
	ServiceManager			= Undefined;
	RowNumber				= 0;
	ResponseStructure		= CheckProgramAvailabilityToReadFile();
	
	If ResponseStructure	= Undefined Then Return; EndIf;
	
	If ResponseStructure.Excel <> Undefined Then
		Excel		= ResponseStructure.Excel;
		IsMSExcel	= True;
	ElsIf ResponseStructure.ServiceManager <> Undefined Then
		ServiceManager	= ResponseStructure.ServiceManager;
		IsLibreoffice	= True;
	ElsIf ResponseStructure.IsFileWithDelimiters Then
		IsFileWithDelimiters	= True;
	EndIf;
	
	If IsMSExcel Then
		
		Status("Status", , Nstr("en = 'Opening file for reading ...; pl = 'Otwieranie pliku do odczytu ...'; ru = 'Открытие файла для чтения...'"));
		
		Try
			WorkBook	= Excel.Workbooks.Open(PathToFile, , True);
			ExcelSheet	= WorkBook.Worksheets(SheetNumber);
			Status("Status", , Nstr("en='Reading data from a file...';pl='Odczyt danych z pliku ...';ru='Чтение данных из файла...';ro='Reading data from a file...'") + Format(CurrentDate(), "DF=HH:mm"));
		Except
			Message(Nstr("en='Perhaps incorrectly specified sheet number of Excel workbook!';pl='Ewentualnie nieprawidłowo określony numer arkusza Excel!';ru='Возможно неправильно указан номер листа книги Excel';ro='Perhaps incorrectly specified sheet number of Excel workbook!'"));
			CompleteWorkMSExcel(Excel);
			Return;
		EndTry;
		
		//Determine how many columns you need to load
		TotalColumns	= ExcelSheet.Cells.SpecialCells(11).Column;
		EndString		= MIN(LineNumberEnd,Number(ExcelSheet.UsedRange.Rows.Count));
		
		Try
			SheetArea		= ExcelSheet.Range(ExcelSheet.Cells(LineNumber,1), ExcelSheet.Cells(EndString,TotalColumns));
			FileDataArray	= SheetArea.Value.Unload();
		Except
			Message(ErrorDescription());
			CompleteWorkMSExcel(Excel);
			Return;
		EndTry;
	EndIf;
	
	If IsLibreoffice Then
		
		Status("Status", , Nstr("en = 'Opening file for reading ...; pl = 'Otwieranie pliku do odczytu ...'; ru = 'Открытие файла для чтения ...'") + Format(CurrentDate(), "DF=HH:mm"));
		
		Try
			Desktop			= ServiceManager.CreateInstance("com.sun.star.frame.Desktop");
			Arguments		= New COMSafeArray("VT_VARIANT", 2);
			Property1		= ServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue");
			Property1.Name	= "AsFile";
			Property1.Value	= true;
			
			Property2		= ServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue");
			Property2.Name	= "Hidden";
			Property2.Value	= true;
			
			Arguments.SetValue(0,Property1);
			Arguments.SetValue(1,Property2);
			
			Document			= Desktop.LoadComponentFromURL(ConvertToURL(PathToFile), "_blank", 0, Arguments);
			Status("Status", , Nstr("en='Reading data from a file...';pl='Odczyt danych z pliku ...';ru='Чтение данных из файла...';ro='Reading data from a file...'") + Format(CurrentDate(), "DF=HH:mm"));
			Sheets				= Document.GetSheets();
			SheetLibreoffice	= Sheets.GetByIndex(SheetNumber - 1);
			
			If SheetLibreoffice	= Undefined Then
				CompleteWorkLibreoffice(Desktop,ServiceManager);
				Return;
			EndIf;
			
			Cursor			= SheetLibreoffice.CreateCursor();
			Cursor.GotoEndOfUsedArea(True);
			
			TotalColumns	= Cursor.Columns.Count - 1;
			EndString		= MIN(LineNumberEnd,Number(Cursor.Rows.Count));
			Region			= SheetLibreoffice.GetCellRangeByPosition(0, LineNumber - 1, TotalColumns, EndString - 1);
			FileDataArray	= Region.GetDataArray().Unload();
			
		Except
			Message(ErrorDescription());
			CompleteWorkLibreoffice(Desktop,ServiceManager);
			Return;
		EndTry;
		
	EndIf;
	
	If IsFileWithDelimiters Then
		
		If DataDelimiterInRow	= "" Then
			Message(Nstr("en = 'No field separator is specified in the csv file lines!; pl = 'W wierszach pliku csv nie określono separatora pól!'; ru = 'В строках файла csv не указано разделителя полей'"));
			Return;
		EndIf;
		
		TextFile	= New TextDocument;
		TextFile.Read(PathToFile);
		EndString	= MIN(LineNumberEnd,TextFile.LineCount());
		
		FileDataArray	= New Array;
		
		For Number	= LineNumber To EndString Do
			TextString	= TextFile.GetLine(Number);
			WordArray	= ExpandStringInArrayOfSubstrings(TextString,DataDelimiterInRow); //08.04.2017
			FileDataArray.Add(WordArray);
		EndDo;
		
	EndIf;
	
	If FileDataArray.Count() = 0 Then
		Message(Nstr("en='No data to download!';pl='Brak danych do pobrania!';ru='Отсутствуют данные для загрузки';ro='No data to download!'"));
		Return;
	EndIf;
		
	Loaded	= 0;
	Counter	= 1000;
	
	If IsMSExcel Then
		RowNumber		= FileDataArray[0].Count() - 1;
		TotalColumns	= FileDataArray.Count() - 1;
	Else
		RowNumber		= FileDataArray.Count() - 1;
		TotalColumns	= FileDataArray[0].Count() - 1;
	EndIf;
	
	FolderTableFromFile.Clear();

	TotalColumns	= ?(TotalColumns > 300, 300, TotalColumns); //The restriction on the count of columns
	CreateTableColumnsLoaded(TotalColumns);
	
	For Count = 0 To RowNumber Do
		
		AllEmptyColumns	= True;
		
		//Blank line filter
		For ColumnNumber = 0 To TotalColumns Do
			
			ColumnName	= "K"+String(ColumnNumber);
			
			If IsMSExcel Then
				Value	= FileDataArray[ColumnNumber][Count];
			ElsIf IsLibreoffice Then
				Value	= FileDataArray[Count][ColumnNumber];
			ElsIf IsFileWithDelimiters Then
				
				Try
					Value	= FileDataArray[Count][ColumnNumber];
				Except
					Continue;
				EndTry;
				
			EndIf;
			
			If ValueIsFilled(Value) Then
				AllEmptyColumns	= False;
				Break;
			EndIf;
			
		EndDo;
		
		If AllEmptyColumns Then Continue; EndIf;
		
		TreeRow	= TableOfLoadedData.Add();
		TreeRow.LineNumber	= Count + LineNumber;
		
		For ColumnNumber	= 0 To TotalColumns Do
			ColumnName	= "K" + String(ColumnNumber);
			
			If IsMSExcel Then
				Value	= FileDataArray[ColumnNumber][Count];
			ElsIf IsLibreoffice Then
				Value	= FileDataArray[Count][ColumnNumber];
			ElsIf IsFileWithDelimiters Then
				Try
					Value	= FileDataArray[Count][ColumnNumber];
				Except
					Continue;
				EndTry;
			EndIf;
					
			//Code for converting a hierarchy from one column to a separate column. Supported hierarchical load for Excel
			If HierarchyType = 1 AND IsMSExcel Then
				Try
					HierarchyLevel	= Number(ExcelSheet.ROWs(Count+LineNumber).OutlineLevel); //???
				Except
					HierarchyLevel	= 0;
				EndTry;
				
				If HierarchyLevel <> 0 Then
					TreeRow["HierarchyLevel"]	= HierarchyLevel;
				EndIf;
			EndIf;
			//
			
			If NOT IsBlankString(ColumnNumberForDirectReading) AND IsMSExcel Then
				
				LineNumberFull		= Count+LineNumber;
				ColumnNumbersArray	= ExpandStringInArrayOfSubstrings(ColumnNumberForDirectReading, ",");
				
				For each ColumnNumberOfDirectReading In ColumnNumbersArray Do
					
					If Number(ColumnNumberOfDirectReading) = ColumnNumber + 1 Then
						
						Cell	= ExcelSheet.Cells(LineNumberFull, ColumnNumber + 1);
						
						If Cell.Hyperlinks.Count = 1 Then
							Value	= TrimAll(Cell.Hyperlinks.Item(1).Address);
						Else
							//5.09
							Formula = Cell.Formula;
							If Find(Lower(Formula),"http://") > 0 OR Find(Lower(Formula),"https://") > 0 OR Find(Lower(Formula), "ftp://") > 0 Then 
								Value = GetPictureAddressOnInternet(Formula);
							Else
								Value = Cell.Text;
							EndIf;
							//5.09
						EndIf;
						
					EndIf;
				EndDo;
			EndIf;
			
			TreeRow[ColumnName]	= TrimAll(Value);
			TreeRow.Check		= True;
			
		EndDo;
		
		//Check for the end of data file
		If NOT IsBlankString(ColumnNumbersToTrackFileEnd) Then
			
			ColumnNumbersArray	= ExpandStringInArrayOfSubstrings(ColumnNumbersToTrackFileEnd,",");
			ThisIsBlankString	= True;
			
			For each ColumnNumberOfFileEndTracking In ColumnNumbersArray Do
				
				ColumnName	= "K" + String(ColumnNumberOfFileEndTracking - 1);
				
				If ValueIsFilled(TrimAll(TreeRow[ColumnName])) Then
					ThisIsBlankString	= False;
					Break;
				EndIf;
			EndDo;
			
			If ThisIsBlankString Then
				TableOfLoadedData.Delete(TreeRow);
				Break;
			EndIf;
			
		EndIf;
		//
		
		Loaded	= Loaded + 1;
		
		If Loaded = Counter Then
			Status("Status", , Nstr("en='Data lines loaded: ';pl='Wierszy danych załadowane: ';ru='Строки данных загружены: ';ro='Data lines loaded: '") + String(Loaded) + " in " + String(RowNumber-LineNumber));
			Counter	= Counter + 1000;
		EndIf;
	EndDo;
	
	If IsMSExcel Then
		Try
			WorkBook.Close(false);
		Except 	EndTry;
		CompleteWorkMSExcel(Excel);
	ElsIf IsLibreoffice Then
		CompleteWorkLibreoffice(Desktop,ServiceManager);
	EndIf;
	
	FileDataRead	= True;
	
EndProcedure

//Creating and deleting loaded table columns
&AtServer
Procedure CreateTableColumnsLoaded(TotalColumns)
	
	DeleteTableColumnsLoaded();
	TableOfLoadedData.Clear();
	
	For ColumnNumber	= 0 To TotalColumns Do
		ColumnName		= "K" + String(ColumnNumber);
		AddColumn("TableOfLoadedData", ColumnName);
	EndDo;
	
EndProcedure

&AtServer
Procedure DeleteTableColumnsLoaded()
	
	FieldArray	= GetFieldList();
	ChangeAttributes( , FieldArray);
	
	For each Attribute In FieldArray Do
		
		FormItem	= Items.Find(StrReplace(Attribute, ".", ""));
		
		If FormItem <> Undefined Then
			Items.Delete(FormItem);
		EndIf;
		
	EndDo;
	
	FieldArray	= GetTableFieldListSelected();
	ChangeAttributes(,FieldArray);
	
	For each Attribute In FieldArray Do
		
		FormItem	= Items.Find(StrReplace(Attribute, ".", ""));
		
		If FormItem <> Undefined Then
			Items.Delete(FormItem);
		EndIf;
	EndDo;
	
EndProcedure

&AtServer
Function GetFieldList()
	
	ExceptionAttributeArray	= New Array;
	ExceptionAttributeArray.Add("Check");
	ExceptionAttributeArray.Add("HierarchyLevel");
	ExceptionAttributeArray.Add("LineNumber");
	
	FieldArray	= New Array;
	FullNameTableOfLoadedData	= FormAttributeToValue("TableOfLoadedData");
	
	For Each Column In FullNameTableOfLoadedData.Columns Do
		
		If ExceptionAttributeArray.Find(Column.Name)	= Undefined Then
			FieldArray.Add("TableOfLoadedData." + Column.Name);
		EndIf;
		
	EndDo;
	
	Return FieldArray;
	
EndFunction

&AtServer
Procedure AddColumn(TableName,ColumnName,HideColumns=False)
	
	TypeArray	= New Array;
	TypeArray.Add(Type("String"));
	
	If HideColumns Then
		TypeArray.Add(Type("CatalogRef.ProductsCategories"));
		TypeArray.Add(Type("CatalogRef.Counterparties"));
	EndIf;
	
	MyAttributes	= New Array;
	MyAttributes.Add(New FormAttribute(ColumnName, New TypeDescription(TypeArray), TableName, ColumnName, False));
	ChangeAttributes(MyAttributes);
	
	If HideColumns Then Return; EndIf;
	
	Try
		FormItem			= Items.Add(TableName + ColumnName, Type("FormField"), Items[TableName]);
		FormItem.DataPath	= TableName + "."+ ColumnName;
		FormItem.Title		= ColumnName;
		FormItem.Kind		= FormFieldType.InputField;
	Except
	EndTry;
	
EndProcedure
//

&AtClient
Procedure CompleteWorkMSExcel(Excel)
	
	Try
		Excel.Application.Quit();
	Except
	EndTry;
	
	Excel	= Undefined;
	
EndProcedure

&AtClient
Procedure CompleteWorkLibreoffice(Desktop,ServiceManager)
	
	Try
		Desktop.Terminate();
		Desktop	= Undefined;
		ServiceManager	= Undefined;
	Except
	EndTry;
	
EndProcedure

#EndRegion

#Region DownloadAndRestoreSettings

//Saving settings
&AtClient
Procedure SaveSettings(Command)
	
	Dialog				= New FileDialog(FileDialogMode.Save);
	Dialog.Title		= Nstr("en='Select file location';pl='Wybierz lokalizację pliku';ru='Выберите место расположения файла';ro='Select file location'");
	Dialog.Filter		= "(*.xml)|*.xml";
	Dialog.FullFileName	= GetSettingsFileName();
	Notification		= New NotifyDescription("SaveSettingsEnd",ThisObject);
	Dialog.Show(Notification);
	
EndProcedure

&AtClient
Procedure SaveSettingsEnd (PathArray,Parameters) Export
	
	If PathArray = Undefined Then Return; EndIf;
	
	SettingPath	= PathArray[0];
	Address		= SaveSettingsAtServer(SettingPath);
	GetFile(Address,SettingPath,False);
	Message(Nstr("en = 'Settings saved!'; pl = 'Ustawienia zapisane!'; ru = Настройки сохранены'"));
	
EndProcedure

&AtServer
Function SaveSettingsAtServer(SettingPath)
	
	FileName	= TempFilesDir() + "nastroyki.xml";
	XMLWriter	= New XMLWriter;
	XMLWriter.OpenFile(FileName);
	XMLWriter.WriteXMLDeclaration();
	XMLWriter.WriteStartElement("Settings");
	
	XDTOSerializer.WriteXML(XMLWriter,FormAttributeToValue("AttributeTable"));
	XDTOSerializer.WriteXML(XMLWriter,FormAttributeToValue("AttributeTableOfCreatedObjects"));
	XDTOSerializer.WriteXML(XMLWriter,FormAttributeToValue("MapTable"));
	
	//5.13
	SettingsStructure = New Structure("PathToFile, SheetNumber, LineNumber, LineNumberEnd,
									    |ColumnNumbersToTrackFileEnd, ColumnNumberForDirectReading, DataDelimiterInRow, GroupSeparator, HierarchyType, ActionKind, DocumentName,
										|TabularSectionName, CreateProductsWhenLoading, UpdateProductsAndPropertyData, WriteProductsInExchangeMode,
										|GenerateBarcodeForProductsIfNotSpecified, UpdateAttributeValuesForRelatedObjects, UploadPicturesWhenLoadingProducts,
										|UpdatePictureDataAvailableInBase, DeleteOldPicturesFROMBaseBeforeLoadingNew, DoNotPerformAmountCalculationInTabularSectionOfDocumentWhenLoading, OldPictureRecordFormat");
	
	FillPropertyValues(SettingsStructure, ThisForm);
	XDTOSerializer.WriteXML(XMLWriter,SettingsStructure);
	//
	
	XMLWriter.WriteEndElement();
	
	Try
		XMLWriter.Close();
	Except
		Message			= New UserMessage;
		Message.Text	= ErrorInfo().Description;
		Message.Message();
	EndTry;
	
	Return PutToTempStorage(New BinaryData(FileName),ThisForm.UUID);
EndFunction

&AtClient
Function GetSettingsFileName()
	
	If NOT ValueIsFilled(PathToFile) Then Return "" EndIf;
		
	WordArray			= ExpandStringInArrayOfSubstrings(PathToFile, "\");
	DownloadFileName	= WordArray[WordArray.Count() - 1];
	WordArray			= ExpandStringInArrayOfSubstrings(DownloadFileName, ".");
	SettingsFileName	= "";
	
	For N = 0 To WordArray.Count() - 2 Do
		 SettingsFileName	= SettingsFileName + ?(SettingsFileName = "", "", ".") + WordArray[N];
	 EndDo;
	 
	Return SettingsFileName + ".xml";
	
EndFunction

//LoadingSettings
&AtClient
Procedure DownloadSettings(Command)
	
	Notification	= New NotifyDescription("QuestionDownloadSettingsEnd", ThisObject);
	Text			= Nstr("en='Restore settings from file? All unsaved settings will be lost!';pl='Przywrócić ustawienia z pliku? Wszystkie niezapisane ustawienia zostaną utracone!';ru='Восстановить настройки из файла? Все несохраненные настройки будут утеряны!';ro='Restore settings from file? All unsaved settings will be lost!'");
	ShowQueryBox(Notification, Text, QuestionDialogMode.YesNo);
	
EndProcedure

&AtClient
Procedure QuestionDownloadSettingsEnd (Result, Parameters) Export
	
	If Result = DialogReturnCode.No Then Return; EndIf;
	
	Dialog				= New FileDialog(FileDialogMode.Open);
	Dialog.Title		= "SelectFile";
	Dialog.Filter		= "(*.xml)|*.xml";
	Dialog.FullFileName	= GetSettingsFileName();
	Notification		= New NotifyDescription("DownloadSettingsEnd", ThisObject);
	Dialog.Show(Notification);
	
EndProcedure

&AtClient
Procedure DownloadSettingsEnd (PathArray, Parameters) Export
	
	If PathArray = Undefined Then Return; EndIf;
	
	SettingPath		= PathArray[0];
	Address			= "";
	Notification	= New NotifyDescription("PutFileEnd", ThisObject);
	
	Try
		BeginPutFile(Notification, Address, SettingPath, False, ThisForm.UUID);
	Except
		Message(Nstr("en='File with settings not found along the way: ';pl='Nie znaleziono pliku z ustawieniami według określonej ścieżki: ';ru='Файли с настройками по заданному пути не найден: ';ro='File with settings not found along the way: '") + SettingPath);
		Message(ErrorDescription());
	EndTry;
	
EndProcedure

&AtClient
Procedure PutFileEnd (Result,Address,SelectedName,Parameters) Export
	
	If Address = Undefined Then Return; EndIf;
	
	DownloadSettingsAtServer(Address, ThisForm.UUID);
	
	File			= New File(PathToFile);
	FileExtention	= Lower(File.Extension);
	
	UploadPicturesWhenLoadingProductsOnChange("");
	
	If WorkMode = 0 Then
		Message(Nstr("en='Settings loaded successfully!';pl='Ustawienia załadowane pomyślnie!';ru='Настройки загружены удачно!';ro='Settings loaded successfully!'"));
	Else
		ContinueDownloadingInAutomaticMode();
	EndIf;
	
EndProcedure

&AtServer
Procedure DownloadSettingsAtServer(Address,UUID)
	
	XMLReader		= New XMLReader;
	TemporaryName	= TempFilesDir() + "settings_temp.xml";
	BinaryFile		= GetFromTempStorage(Address);
	
	BinaryFile.Write(TemporaryName);
	XMLReader.OpenFile(TemporaryName);
	XMLReader.Read();
	XMLReader.Read();
	
	File			= New File(PathToFile);
	FileExtention	= Lower(File.Extension);
	
	Try
		FullNameAttributeTable	= XDTOSerializer.ReadXML(XMLReader);
	Except
		Message(Nstr("en='Most likely, the attributes in the configuration have changed since the last time the settings were saved!';pl='Najprawdopodobniej atrybuty w konfiguracji się zmieniły od czasu ostatniego zapisania ustawień!';ru='Скорее всего, атрибуты в конфигурации были изменены с момента последнего сохранения настроек!';ro='Most likely, the attributes in the configuration have changed since the last time the settings were saved!'"));
	EndTry;
	
	Try
		FullNameAttributeTableCreated	= XDTOSerializer.ReadXML(XMLReader);
	Except
		Message(Nstr("en='Most likely, the attributes in the configuration have changed since the last time the settings were saved!';pl='Najprawdopodobniej atrybuty w konfiguracji się zmieniły od czasu ostatniego zapisania ustawień!';ru='Скорее всего, атрибуты в конфигурации были изменены с момента последнего сохранения настроек!';ro='Most likely, the attributes in the configuration have changed since the last time the settings were saved!'"));
	EndTry;
	
	Try
		FullNameMapTable	= XDTOSerializer.ReadXML(XMLReader);
	Except
		Message(Nstr("en='Most likely, the attributes in the configuration have changed since the last time the settings were saved!';pl='Najprawdopodobniej atrybuty w konfiguracji się zmieniły od czasu ostatniego zapisania ustawień!';ru='Скорее всего, атрибуты в конфигурации были изменены с момента последнего сохранения настроек!';ro='Most likely, the attributes in the configuration have changed since the last time the settings were saved!'"));
	EndTry;

	WereErrors	= False;
	Try
		If WorkMode	= 0 Then
			SettingsStructure	= XDTOSerializer.ReadXML(XMLReader);
			PathToFile			= SettingsStructure;
		Else
			SettingsStructure	=XDTOSerializer.ReadXML(XMLReader);
			PathToFileOld		= SettingsStructure;
		EndIf;
		
		//5.13
		If TypeOf(SettingsStructure)	= Type("Structure") Then
			FillPropertyValues(ThisForm, SettingsStructure);
			If WorkMode <> 0 Then
				PathToFileOld	= SettingsStructure.PathToFile;
			EndIf;
		Else
			
			SheetNumber		= XDTOSerializer.ReadXML(XMLReader);
			LineNumber		= XDTOSerializer.ReadXML(XMLReader);
			LineNumberEnd	= XDTOSerializer.ReadXML(XMLReader);
			
			ColumnNumbersToTrackFileEnd		= XDTOSerializer.ReadXML(XMLReader);
			ColumnNumberForDirectReading	= XDTOSerializer.ReadXML(XMLReader);
			
			DataDelimiterInRow	= XDTOSerializer.ReadXML(XMLReader);
			GroupSeparator		= XDTOSerializer.ReadXML(XMLReader);
			HierarchyType		= XDTOSerializer.ReadXML(XMLReader);
			ActionKind			= XDTOSerializer.ReadXML(XMLReader);
			DocumentName		= XDTOSerializer.ReadXML(XMLReader);
			TabularSectionName	= XDTOSerializer.ReadXML(XMLReader);
			
			CreateProductsWhenLoading		= XDTOSerializer.ReadXML(XMLReader);
			UpdateProductsAndPropertyData	= XDTOSerializer.ReadXML(XMLReader);
			WriteProductsInExchangeMode		= XDTOSerializer.ReadXML(XMLReader);
			
			GenerateBarcodeForProductsIfNotSpecified	= XDTOSerializer.ReadXML(XMLReader);
			UpdateAttributeValuesForRelatedObjects		= XDTOSerializer.ReadXML(XMLReader);
			UploadPicturesWhenLoadingProducts			= XDTOSerializer.ReadXML(XMLReader);
			UpdatePictureDataAvailableInBase			= XDTOSerializer.ReadXML(XMLReader);
			DeleteOldPicturesFROMBaseBeforeLoadingNew	= XDTOSerializer.ReadXML(XMLReader);
			
		EndIf;
		//
	Except
		Message(Nstr("en='Most likely, the data processor settings have changed, so some settings have not been loaded!';pl='Najprawdopodobniej ustawienia opracowania się zmieniły, dlatego niektóre parametry nie zostały załadowane!';ru='Скорее всего, настройки обработки были изменены, поэтому некоторые параметры не были загружены';ro='Most likely, the data processor settings have changed, so some settings have not been loaded!'"));
		XMLReader.Close();
		WereErrors=True;
	EndTry;
	
	If NOT WereErrors Then
		
		XMLReader.Close();
		
		For each ValueTableRow In FullNameAttributeTable Do
			
			FilterByKey	= New Structure("ActionName,KeyName");
			FillPropertyValues(FilterByKey,ValueTableRow);
			StringArray	= AttributeTable.FindRows(FilterByKey);
			
			If StringArray.Count() > 0 Then
				FillPropertyValues(StringArray[0],ValueTableRow,,"Type, RefChartOfCharacteristicTypes");
			Else
				NewValueTableRow	= AttributeTable.Add();
				FillPropertyValues(NewValueTableRow,ValueTableRow);
			EndIf;
			
		EndDo;
		
		For each ValueTableRow In FullNameAttributeTableCreated Do
			
			FilterByKey	= New Structure("KeyName,FullName,Name");
			FillPropertyValues(FilterByKey,ValueTableRow);
			StringArray	= AttributeTableOfCreatedObjects.FindRows(FilterByKey);
			
			If StringArray.Count() > 0 Then
				FillPropertyValues(StringArray[0], ValueTableRow, , "Type");
			Else
				NewValueTableRow	= AttributeTableOfCreatedObjects.Add();
				FillPropertyValues(NewValueTableRow,ValueTableRow, , "Type");
			EndIf;
		EndDo;
		
		For each ValueTableRow In FullNameMapTable Do
			
			FilterByKey	= New Structure("FullName, DataFromColumn");
			FillPropertyValues(FilterByKey, ValueTableRow);
			StringArray	= MapTable.FindRows(FilterByKey);
			
			If StringArray.Count() > 0 Then
				FillPropertyValues(StringArray[0],ValueTableRow, , "Type");
			Else
				NewValueTableRow	= MapTable.Add();
				FillPropertyValues(NewValueTableRow,ValueTableRow);
			EndIf;
		EndDo;
	EndIf;
	
	Items.DocumentName.ChoiceList.Clear();
	Items.TabularSectionName.ChoiceList.Clear();
	FillDocumentListWithProducts();
	
EndProcedure

#EndRegion

#Region FillingTableWithObjectAttributes

&AtClient
Procedure ActionKindOnChange(Item)
	
	FillAttributesByActionKind();
	Items.MapPagesAndObjectAttributes.CurrentPage	= Items.PageObjectAttributes;
	
EndProcedure

&AtServer
Procedure FillAttributesByActionKind()
	
	Items.ProductsKindForFilter.Visible	= False;
	Items.DocumentName.Visible			= False;
	Items.TabularSectionName.Visible	= False;
	
	If ActionKind	= "Products" Then
		
		FullNameOfCurrentAction	= FullNameStructure.Products + "__";
		FillProductsAttributes();
		FixedFilter	= New FixedStructure("ActionName", FullNameOfCurrentAction);
		
	ElsIf ActionKind = "AdditionalProductsAttributes" Then
		
		FullNameOfCurrentAction	= FullNameStructure.AdditionalAttributes + "__";
		Items.ProductsKindForFilter.Visible	= True;
		FillAdditionalProperties();
		FixedFilter	= New FixedStructure("ActionName, Name", FullNameOfCurrentAction, ProductsKindForFilter);
		
	ElsIf ActionKind = "AdditionalCharacteristicsAttributes" Then
		
		FullNameOfCurrentAction	= FullNameStructure.AdditionalAttributes + "____";
		Items.ProductsKindForFilter.Visible	= True;
		FillAdditionalProperties(True);
		FixedFilter	= New FixedStructure("ActionName,Name",FullNameOfCurrentAction,ProductsKindForFilter);
		
	ElsIf ActionKind = "Barcodes" Then
		
		FullNameOfCurrentAction	= FullNameStructure.Barcodes + "__";
		FillBarcodeAttributes();
		FixedFilter	= New FixedStructure("ActionName", FullNameOfCurrentAction);
		
	ElsIf ActionKind	= "Prices" Then
		
		FullNameOfCurrentAction	= FullNameStructure.Prices + "__";
		FillPriceAttributes();
		FixedFilter	= New FixedStructure("ActionName",FullNameOfCurrentAction);
		
	ElsIf ActionKind	= "SupplierProducts" Then
		
		FullNameOfCurrentAction	= FullNameStructure.SupplierProducts + "__";
		FillAttributesWithSupplierProducts();
		FixedFilter	= New FixedStructure("ActionName", FullNameOfCurrentAction);
		
	ElsIf ActionKind	= "Document" Then
		
		FullNameOfCurrentAction	= DocumentName + "__";
		FillDocumentListWithProducts();
		FixedFilter	= New FixedStructure("ActionName, TabularSectionName", FullNameOfCurrentAction, TabularSectionName);
		Items.DocumentName.Visible	= True;
		Items.TabularSectionName.Visible	= True;
		
	EndIf;
	
	AttributeTable.Sort("RequiredForFilling DESC, Name");
	Items.AttributeTable.RowFilter	= FixedFilter;
	
EndProcedure

&AtClient
Procedure ProductsKindForFilterOnChange(Item)
	
	FixedFilter	= New FixedStructure("ActionName,Name",FullNameOfCurrentAction,ProductsKindForFilter);
	Items.AttributeTable.RowFilter = FixedFilter;
	
EndProcedure

&AtServer
Procedure AddAttributeToAttributesTable(MetadataObject, AttributeName, IsSimpleType = Undefined, KeyName = Undefined, Synonym = "", TabularSectionName = "")
	
	FullName	= MetadataObject.FullName();

	SimpleTypeArray	= New Array;
	SimpleTypeArray.Add(Type("String"));
	SimpleTypeArray.Add(Type("Number"));
	SimpleTypeArray.Add(Type("Date"));
	SimpleTypeArray.Add(Type("Boolean"));
	SimpleTypeArray.Add(Type("ValueStorage"));
	SimpleTypeArray.Add(Type("UUID"));
	
	ArrayOfExcludedAttributes	= New Array;
	ArrayOfExcludedAttributes.Add("Ref");
	ArrayOfExcludedAttributes.Add("ValueType");
	ArrayOfExcludedAttributes.Add("Predefined");
	ArrayOfExcludedAttributes.Add("PredefinedDataName");
	ArrayOfExcludedAttributes.Add("IsFolder");
	ArrayOfExcludedAttributes.Add("DeletionMark");
	
	If ArrayOfExcludedAttributes.Find(AttributeName) <> Undefined AND Synonym = "" Then Return; EndIf;

	If Find(FullName, "InformationRegister.") > 0 Then
		
		Attribute	= MetadataObject.Dimensions.Find(AttributeName);
		
		If Attribute = Undefined Then
			Attribute	= MetadataObject.Resources.Find(AttributeName);
		EndIf;
		
		If Attribute = Undefined Then
			Attribute	= MetadataObject.Attributes.Find(AttributeName);
		EndIf;
	Else
		Try
			Attribute	= MetadataObject.StandardAttributes[AttributeName];
		Except EndTry;
		
		If Attribute = Undefined Then
			
			Attribute	= MetadataObject.Attributes.Find(AttributeName);
			
			If Attribute = Undefined AND TabularSectionName <> "" Then
				Attribute	= MetadataObject.TabularSections[TabularSectionName].Attributes.Find(AttributeName);
			EndIf;
			
		EndIf;
		
	EndIf;
	
	If Attribute = Undefined Then Return; EndIf;
	
	AttributeType	= Attribute.Type.Types()[0];
	
	If KeyName = Undefined Then
		KeyName		= FullName + "." + AttributeName;
		ForСreating	= False;
	Else
		ForСreating	= True;
	EndIf;
	
	If TabularSectionName <> "" Then
		FilterByAttributes	= New Structure("KeyName, FullName, Name, TabularSectionName", KeyName, FullName, AttributeName, TabularSectionName);
	Else
		FilterByAttributes	= New Structure("KeyName, FullName, Name", KeyName, FullName, AttributeName);
	EndIf;
	
	If NOT ForСreating Then
		StringArray	= AttributeTable.FindRows(FilterByAttributes);
	Else
		StringArray	= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
	EndIf;
	
	If StringArray.Count() <> 0 Then Return; EndIf;
	
	If IsSimpleType = Undefined Then
		
		If SimpleTypeArray.Find(AttributeType) <> Undefined Then
			IsSimpleType	= True;
		Else
			IsSimpleType	= False;
		EndIf;
		
	EndIf;
	
	If NOT ForСreating Then
		NewValueTableRow					= AttributeTable.Add();
		NewValueTableRow.ActionName			= FullNameOfCurrentAction;
		NewValueTableRow.TabularSectionName	= TabularSectionName;
	Else
		
		NewValueTableRow	= AttributeTableOfCreatedObjects.Add();
		StringArrayReadOnly	= SearchFieldTable.FindRows(New Structure("FullName", FullName));
		
		If StringArrayReadOnly.Count() > 0 Then
			
			StringArrayReadOnly	= SearchFieldTable.FindRows(New Structure("FullName, Name", FullName, AttributeName));
			
			If StringArrayReadOnly.Count() > 0  Then
				
				If FullName<>FullNameStructure.Pictures Then
					NewValueTableRow.ColumnName = ?(IsSimpleType, SelectedColumn, "");
				EndIf;
				
				NewValueTableRow.SearchFieldInDatabase	= True;
				NewValueTableRow.FillingVariant			= "From column";
				NewValueTableRow.Use					= True;
				NewValueTableRow.ReadOnly				= True;
				
			EndIf;
			
		Else
			If AttributeName	= "Description" Then
				NewValueTableRow.ColumnName				= SelectedColumn;
				NewValueTableRow.SearchFieldInDatabase	= True;
				NewValueTableRow.FillingVariant			= "From column";
				NewValueTableRow.Use					= True;
				NewValueTableRow.ReadOnly				= True;
			EndIf;
		EndIf;
		
		If AttributeName = "Owner" Then
			
			NewValueTableRow.SearchFieldInDatabase	= True;
			NewValueTableRow.FillingVariant			= "From column";
			NewValueTableRow.Use					= True;
			AttributeTypeArray						= Attribute.Type.Types();
			
			For each ArrayAttributeType In AttributeTypeArray Do
				
				FullAttributeName	= Metadata.FindByType(ArrayAttributeType).FullName();
				
				If FullAttributeName = FullNameStructure.Products Then
					NewValueTableRow.ReadOnly	= True;
					Break;
				EndIf;
				
			EndDo;
			
		EndIf;
		
	EndIf;
		
	NewValueTableRow.FullName	= FullName;
	NewValueTableRow.Name		= AttributeName;
	NewValueTableRow.Type		= Attribute.Type;
	
	Synonym	= ?(Synonym ="", ?(NOT ValueIsFilled(Attribute.Synonym), AttributeName, Attribute.Synonym), Synonym);
	
	NewValueTableRow.Synonym		= Synonym;
	NewValueTableRow.KeyName		= KeyName;
	NewValueTableRow.IsSimpleType	= IsSimpleType;
	
	If Attribute.FillChecking = FillChecking.ShowError Then
		NewValueTableRow.RequiredForFilling	= True;
		NewValueTableRow.Synonym			= NewValueTableRow.Synonym + Nstr("en=' (fill to create)';pl=' (wypełnij, aby utworzyć)';ru=' (заполнить для создания)';ro=' (fill to create)'");
	EndIf;
	
	If NOT NewValueTableRow.IsSimpleType AND NOT ForСreating Then
		AttributeMetadataObject				= Metadata.FindByType(AttributeType);
		NewValueTableRow.FullAttributeName	= AttributeMetadataObject.FullName();
	EndIf;
	
	If NOT ForСreating AND ((FullName = FullNameStructure.Products AND AttributeName <> "Parent")
		OR FullName = FullNameStructure.SupplierProducts
		OR (FullName = FullNameStructure.Barcodes AND AttributeName = "Barcode")) Then
		
		NewValueTableRow.AllowSearch	= True;
		
	EndIf;
EndProcedure

&AtServer
Procedure FillProductsAttributes()
	
	MetadataObject	= Metadata.FindByFullName(FullNameStructure.Products);
	
	AddAttributeToAttributesTable(MetadataObject, "Code");
	AddAttributeToAttributesTable(MetadataObject, "SKU");
	AddAttributeToAttributesTable(MetadataObject, "Description");
	AddAttributeToAttributesTable(MetadataObject, "DescriptionFull");
	AddAttributeToAttributesTable(MetadataObject, "ProductsCategory");
	AddAttributeToAttributesTable(MetadataObject, "ProductsType");
	AddAttributeToAttributesTable(MetadataObject, "MeasurementUnit");
	AddAttributeToAttributesTable(MetadataObject, "Manufacturer");
	AddAttributeToAttributesTable(MetadataObject, "VATRate");
	AddAttributeToAttributesTable(MetadataObject, "UseCharacteristics");
	AddAttributeToAttributesTable(MetadataObject, "UseSerialNumber");
	
	AddAttributeToAttributesTable(MetadataObject, "Parent", , , Nstr("en='Group (for loading hierarchy)';pl='Grupa (do ładowania hierarchii)';ru='Группа (для загрузки иерархии)';ro='Group (for loading hierarchy)'"));
	AddAttributeToAttributesTable(MetadataObject, "PictureFile");
	
	If UseCharacteristics Then
		MetadataObjectCharacteristics	= Metadata.FindByFullName(FullNameStructure.Characteristics);
		AddAttributeToAttributesTable(MetadataObjectCharacteristics, "Ref", , , Nstr("en='Characteristic (for download)';pl='Charakterystyka (do pobrania)';ru='Характеристика (для загрузки)';ro='Characteristic (for download)'"));
	EndIf;
	
	If UseSeries Then
		MetadataObjectSeries	= Metadata.FindByFullName(FullNameStructure.Series);
		AddAttributeToAttributesTable(MetadataObjectSeries, "Ref", , , Nstr("en='Series (for download)';pl='Seria (do pobrania)';ru='Серия (для загрузки)';ro='Series (for download)'"));
	EndIf;
	
	If UseCustomsDeclaration Then
		MetadataObjectCustomsDeclaration	= Metadata.FindByFullName(FullNameStructure.CustomsDeclaration);
		AddAttributeToAttributesTable(MetadataObjectCustomsDeclaration, "Ref", , , Nstr("en='Customs declaration(for loading)';pl='Deklaracje celne ładunków (do pobrania)';ru='ГТД (для загрузки)';ro='Customs declaration(for loading)'"));
	EndIf;
	
	If UsePackings Then
		MetadataObjectPackings	= Metadata.FindByFullName(FullNameStructure.Packings);
		AddAttributeToAttributesTable(MetadataObjectPackings, "Ref", , , Nstr("en='Package (for download)';pl='Opakowanie (do pobrania)';ru='Упаковка (для загрузки)';ro='Package (for download)'"));
	EndIf;
EndProcedure

//Additional attributes
&AtServer
Procedure FillAdditionalProperties(OnCharacteristics = False)
	
	AdditionalAttributeManager	= GetObjectManagerByFullName(FullNameStructure.AdditionalAttributeSets);
	
	//5.07
	Query		= New Query;
	Query.Text	=
	"SELECT
	|	AdditionalAttributesAndInformationSetsAdditionalAttributes.Property.Description
	|	AS Description, AdditionalAttributesAndInformationSetsAdditionalAttributes.Property
	|	AS RefChartOfCharacteristicTypes, AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref.Description
	|AS
	|	SetDescription FROM Catalog.AdditionalAttributesAndInfoSets.AdditionalAttributes
	|AS
	|	AdditionalAttributesAndInformationSetsAdditionalAttributes WHERE AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref
	|	IN HIERARCHY(&Parent)
	|	AND NOT AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref.IsFolder AND NOT AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref.DeletionMark";
		
	If NOT OnCharacteristics Then
		Query.SetParameter("Parent", AdditionalAttributeManager.Catalog_Products);
	Else
		Query.SetParameter("Parent", AdditionalAttributeManager.Catalog_ProductsCharacteristics);
	EndIf;
	
	DataStructure	= New Structure("RefChartOfCharacteristicTypes, Description, FullName, SetDescription");
	DataStructure.Insert("FullName", StrReplace(FullNameOfCurrentAction, "__", ""));
	
	SetArray	= New Array;
	
	Selection	= Query.Execute().Select();
	
	While Selection.Next() Do
		
		FillPropertyValues(DataStructure,Selection);
		AddStringAdditionalAttributes(DataStructure);
		
		If SetArray.Find(Selection.SetDescription)	= Undefined Then
			SetArray.Add(Selection.SetDescription);
		EndIf;
		
	EndDo;
	
	Items.ProductsKindForFilter.ChoiceList.Clear();
	
	For each Set In SetArray Do
		Items.ProductsKindForFilter.ChoiceList.Add(Set + ".", Set);
	EndDo;
	
	If SetArray.Count() > 0 Then
		ProductsKindForFilter	= SetArray[0];
	Else
		ProductsKindForFilter = "";
	EndIf;
	
EndProcedure

&AtServer
Procedure AddStringAdditionalAttributes(DataStructure)
	
	KeyName		= StrReplace(FullNameOfCurrentAction, "__", "") + "." + DataStructure.SetDescription + "." + DataStructure.RefChartOfCharacteristicTypes.Description;
	StringArray	= AttributeTable.FindRows(New Structure("KeyName", KeyName));
	
	If StringArray.Count() <> 0 Then Return; EndIf;
	
	SimpleTypeArray	= New Array;
	SimpleTypeArray.Add(Type("String"));
	SimpleTypeArray.Add(Type("Number"));
	SimpleTypeArray.Add(Type("Date"));
	SimpleTypeArray.Add(Type("Boolean"));
	SimpleTypeArray.Add(Type("ValueStorage"));
	SimpleTypeArray.Add(Type("UUID"));
	
	FullName	= DataStructure.FullName;
	WordArray	= ExpandStringInArrayOfSubstringsServer(DataStructure.Description, "(");
	Synonym		= TrimAll(WordArray[0]);
	
	AttributeType	= DataStructure.RefChartOfCharacteristicTypes.ValueType.Types()[0];
	
	NewValueTableRow				= AttributeTable.Add();
	NewValueTableRow.FullName		= FullNameOfCurrentAction;
	NewValueTableRow.Name			= DataStructure.SetDescription + ".";
	NewValueTableRow.Synonym		= Synonym;
	NewValueTableRow.KeyName		= KeyName;
	NewValueTableRow.ActionName		= FullNameOfCurrentAction;
	NewValueTableRow.RefChartOfCharacteristicTypes	= DataStructure.RefChartOfCharacteristicTypes;
	
	If SimpleTypeArray.Find(AttributeType) = Undefined Then
		AttributeMetadataObject				= Metadata.FindByType(AttributeType);
		NewValueTableRow.FullAttributeName	= AttributeMetadataObject.FullName();
		NewValueTableRow.IsSimpleType		= False;
	Else
		NewValueTableRow.IsSimpleType	= True;
	EndIf;
	
	FullNameProperty		= DataStructure.RefChartOfCharacteristicTypes.GetObject();
	NewValueTableRow.Type	= FullNameProperty.ValueType;
	
EndProcedure

//Barcodes
&AtServer
Procedure FillBarcodeAttributes()
	
	FullNameRegisterBarcode	= Metadata.FindByFullName(FullNameStructure.Barcodes);
	
	AddAttributeToAttributesTable(FullNameRegisterBarcode, "Barcode", True);
	
	If UseCharacteristics Then
		AddAttributeToAttributesTable(FullNameRegisterBarcode, "Characteristic");
	EndIf;
	
	AddAttributeToAttributesTable(FullNameRegisterBarcode, "MeasurementUnit");
	
EndProcedure

//Barcodes
&AtServer
Procedure FillPriceAttributes()
	
	FullNamePriceRegister	= Metadata.FindByFullName(FullNameStructure.Prices);
	
	AddAttributeToAttributesTable(FullNamePriceRegister, "Price");
	
	If UseCharacteristics Then
		AddAttributeToAttributesTable(FullNamePriceRegister, "Characteristic");
	EndIf;
	
	AddAttributeToAttributesTable(FullNamePriceRegister, "MeasurementUnit");
	
EndProcedure

//Supplier products
&AtServer
Procedure FillAttributesWithSupplierProducts()
	
	MetadataObject	= Metadata.FindByFullName(FullNameStructure.SupplierProducts);
	
	AddAttributeToAttributesTable(MetadataObject, "Owner");
	AddAttributeToAttributesTable(MetadataObject, "Description");
	AddAttributeToAttributesTable(MetadataObject, "Parent");
	
	For each Attribute In MetadataObject.Attributes Do
		
		AttributeType	= Attribute.Type.Types()[0];
		AttributeObject	= Metadata.FindByType(AttributeType);
		
		If AttributeObject <> Undefined Then
			
			FullAttributeName	= AttributeObject.FullName();
			
			If FullAttributeName = FullNameStructure.Products Then
				Continue;
			ElsIf NOT UseCharacteristics AND FullAttributeName = FullNameStructure.Characteristics Then
				Continue;
			ElsIf NOT UsePackings AND FullAttributeName = FullNameStructure.Packings Then
				Continue;
			EndIf;
			
		EndIf;
		
		AddAttributeToAttributesTable(MetadataObject,Attribute.Name);
		
	EndDo;
EndProcedure

//Document list and its tabular sections and attributes
&AtServer
Procedure FillDocumentListWithProducts()
	
	If Items.DocumentName.ChoiceList.Count() <> 0 Then Return; EndIf;
		
	DocumentList	= New ValueList;
	
	For Each MetadataObject In Metadata.Documents Do
		
		FullName	= MetadataObject.FullName();
		Flag		= False;
		
		For Each TabularSection In MetadataObject.TabularSections Do
			
			For Each Attribute In TabularSection.Attributes Do
				
				TypeArray	= Attribute.Type.Types();
				
				If TypeArray.Count() > 1 Then Continue; EndIf;
				
				AttributeType	= TypeArray[0];
				
				If AttributeType = Type("CatalogRef.Products")Then
					DocumentList.Add(FullName, MetadataObject.Synonym);
					Flag	= True;
					Break;
				EndIf;
				
			EndDo;
			
			If Flag Then Break; EndIf;
			
		EndDo;
		
	EndDo;
	
	Items.DocumentName.ChoiceList.Clear();
	
	For each ListItem In DocumentList Do
		Items.DocumentName.ChoiceList.Add(ListItem.Value, ListItem.Presentation);
	EndDo;
	
	Items.DocumentName.ChoiceList.SortByPresentation();
	
	FullNameOfCurrentAction	= DocumentName + "__";
	FillDocumentTabularSectionList();
	
EndProcedure

&AtServer
Procedure FillDocumentTabularSectionList()
	
	TabularSectionList	= New ValueList;
	TabularSectionList.Add("<DocumentAttributes>", Nstr("en='<Document attributes>';pl='<Atrybuty dokumentu>';ru='<Реквизиты документа>';ro='<Document attributes>'"));
	MetadataObject	= Metadata.FindByFullName(DocumentName);
	
	If MetadataObject = Undefined Then Return; EndIf;
	
	For Each TabularSection In MetadataObject.TabularSections Do
		
		For Each Attribute In TabularSection.Attributes Do
			
			TypeArray	= Attribute.Type.Types();
			
			If TypeArray.Count() > 1 Then Continue; EndIf;
			
			AttributeType	= TypeArray[0];
			
			If AttributeType = Type("CatalogRef.Products")Then
				TabularSectionList.Add(TabularSection.Name, TabularSection.Synonym);
				Break;
			EndIf;
			
		EndDo;
		
	EndDo;
	
	Items.TabularSectionName.ChoiceList.Clear();
	
	For each ListItem In TabularSectionList Do
		Items.TabularSectionName.ChoiceList.Add(ListItem.Value,ListItem.Presentation);
	EndDo;
	
	Items.TabularSectionName.ChoiceList.SortByPresentation();

	FillDocumentAttributesList();
	
	FixedFilter	= New FixedStructure("ActionName, TabularSectionName", FullNameOfCurrentAction, TabularSectionName);
	Items.AttributeTable.RowFilter	= FixedFilter;
	
EndProcedure

&AtServer
Procedure FillDocumentAttributesList()
	
	MetadataObject	= Metadata.FindByFullName(DocumentName);
	
	If MetadataObject = Undefined Then Return; EndIf;
	
	If IsBlankString(TabularSectionName) Then Return; EndIf;
	
	AddAttributeToAttributesTable(MetadataObject, "Date");
	
	If TabularSectionName	= "<DocumentAttributes>" Then
		
		For Each Attribute In MetadataObject.Attributes Do
			
			AttributeType	= Attribute.Type.Types()[0];
			
			If AttributeType = Type("CatalogRef.Products")Then Continue; EndIf;

			AddAttributeToAttributesTable(MetadataObject, Attribute.Name, , , , TabularSectionName);
			
		EndDo;
		
	Else
		TabularSection	= MetadataObject.TabularSections[TabularSectionName];
		
		For each Attribute In TabularSection.Attributes Do
			
			AttributeType	= Attribute.Type.Types()[0];
			
			If AttributeType = Type("CatalogRef.Products")Then Continue; EndIf;
						
			AddAttributeToAttributesTable(MetadataObject, Attribute.Name, , , , TabularSectionName);
			
		EndDo;
		
	EndIf;
	
	AttributeTable.Sort("RequiredForFilling DESC,Name");
	
	FixedFilter	= New FixedStructure("ActionName, TabularSectionName", FullNameOfCurrentAction, TabularSectionName);
	Items.AttributeTable.RowFilter	= FixedFilter;
	
EndProcedure

&AtClient
Procedure DeleteOldDocumentAttributes()
	
	AttributeStringArray	= AttributeTable.FindRows(New Structure("FullName", DocumentName));
	
	For each ValueTableRowOfAttributes In AttributeStringArray Do
		
		FilterByAttributes	= New Structure("KeyName", ValueTableRowOfAttributes.KeyName);
		StringArray	= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
		LineCount	= StringArray.Count();
		
		For N = 1 To LineCount Do
			RowIndex	= LineCount - N;
			AttributeTableOfCreatedObjects.Delete(StringArray[RowIndex]);
		EndDo;
		
	EndDo;
	
	LineCount	= AttributeStringArray.Count();
	
	For N = 1 To LineCount Do
		RowIndex	= LineCount - N;
		AttributeTable.Delete(AttributeStringArray[RowIndex]);
	EndDo;
	
EndProcedure

#EndRegion

#Region GettingDataFromBaseAndCreatingValues

&AtServer
Function GetValueForAttribute(KeyName, DataString, FullName = "", AttributeName = "", DoNotCreate = False, SearchStructure = Undefined)
	
	TableForSearch					= AttributeTable;
	FilterByAttributes				= New Structure("KeyName",KeyName);
	IsTableOfObjectsBeingCreated	= False;

	If NOT IsBlankString(FullName) Then
		TableForSearch	= AttributeTableOfCreatedObjects;
		FilterByAttributes.Insert("FullName", FullName);
		FilterByAttributes.Insert("Name", AttributeName);
		IsTableOfObjectsBeingCreated	= True;
	EndIf;
	
	StringArray	= TableForSearch.FindRows(FilterByAttributes);
	
	If StringArray.Count() = 0 Then
		MessageText	= Nstr("en='Not found attribute ';pl='Nie znaleziono atrybutu ';ru='Атрибут не найден';ro='Not found attribute '") + "<" + AttributeName + "> in <" + FullName + ">!";
		Message(MessageText);
		Raise MessageText;
	EndIf;
	
	Value	= Undefined;
	ValueTableRow	= StringArray[0];
	FillingVariant	= ValueTableRow.FillingVariant;
	ColumnName		= ValueTableRow.ColumnName;
	Synonym			= ValueTableRow.Synonym;
	
	If FillingVariant = "Selected value" Then
		Value	= ValueTableRow.FillValue;
	ElsIf FillingVariant	= "From column" AND (ValueTableRow.Name="Parent" OR ValueTableRow.IsSimpleType) Then
		
		If IsBlankString(ColumnName) Then
			MessageText	= Nstr("en='No column is specified from which to get data for the attribute ';pl='Nie określono kolumny, z której można pobrać dane dla atrybutu ';ru='Не указано колонки, из которой можна получить данные для атрибута';ro='No column is specified from which to get data for the attribute '")
								+ "<" + Synonym + "> for <" + FullName + ">!";
			Message(MessageText);
			Raise MessageText;
		EndIf;
		
		Value		= TrimAll(DataString[ColumnName]);
		ValueString = StrReplace(Value, Chars.NBSp, "");
		
		If ValueTableRow.IsSimpleType Then
			Value	= ValueTableRow.Type.AdjustValue(ValueString);
		EndIf;
		
		If TypeOf(Value) = Type("Date") AND NOT ValueIsFilled(Value) Then
			Try
				DateYear	= Mid(ValueString, 7, 4);
				DateMonth	= Mid(ValueString, 4, 2);
				DateDay		= Left(ValueString, 2);
				Value		= Date(DateYear + DateMonth + DateDay);
			Except EndTry;
		EndIf;
		
	ElsIf FillingVariant = "Map" Then
		DataFromColumn	= DataString[ColumnName];
		FilterByMaps	= New Structure("FullName, DataFromColumn", ValueTableRow.FullName + "." + ValueTableRow.Name, DataFromColumn);
		MapStringArray	= MapTable.FindRows(FilterByMaps);
		
		If MapStringArray.Count() > 0 Then
			Value	= MapStringArray[0].DataFromDatabase;
		EndIf;
	EndIf;
	
	If NOT IsTableOfObjectsBeingCreated AND NOT ValueTableRow.IsSimpleType
		AND FillingVariant <> "Selected value" AND (NOT ValueIsFilled(Value) OR Value = Undefined) Then
		
		FilterOnAttributesOfCreatedObjects	= New Structure("KeyName, SearchFieldInDatabase", KeyName, True);
		AttributeStringArrayCreated			= AttributeTableOfCreatedObjects.FindRows(FilterOnAttributesOfCreatedObjects);
		ValueTable							= DataListFromDatabase.Get(ValueTableRow.FullAttributeName);
		
		If ValueTable = Undefined Then
			MessageText	= Nstr("en='The required attribute is not selected ';pl='Wymagany atrybut nie jest wybrany ';ru='Обязательный атрибут не выбран ';ro='The required attribute is not selected '")
								+ "<" + ValueTableRow.Name + "> object <" + ValueTableRow.FullName + ">!";
			Message(MessageText);
			Raise MessageText;
		EndIf;
					
		FilterOnValueTable	= New Structure;
		
		For Each ValueTableRowOfAttributesCreated In AttributeStringArrayCreated Do
			
			AttributeNameCreated	= ValueTableRowOfAttributesCreated.Name;
			Value					= GetValueForAttribute(KeyName, DataString, ValueTableRowOfAttributesCreated.FullName, AttributeNameCreated);
			
			If TypeOf(Value) = Type("String") Then
				Value	= Lower(Value);
			EndIf;
			
			FilterOnValueTable.Insert(AttributeNameCreated, Value);
			
		EndDo;
		
		If SearchStructure <> Undefined Then
			
			For Each StructureItem In SearchStructure Do
				FilterOnValueTable.Insert(StructureItem.Key, StructureItem.Value);
			EndDo;
			
		Else
			
			If ValueTableRow.FullAttributeName = FullNameStructure.Packings Then
				FilterOnValueTable.Insert("Owner", Catalogs.PackingSets.BaseMeasurementUnit);
			EndIf;
			
		EndIf;
		
		FoundRowArray	= ValueTable.FindRows(FilterOnValueTable);
		
		If FoundRowArray.Count() <> 0 Then
			Value	= FoundRowArray[0].Ref;
		Else
			Value	= ValueTableRow.Type.AdjustValue(Value);
		EndIf;
		
		Value		= CreateUpdateAttributeValue(KeyName, Value, DataString, SearchStructure);
		
		If FillingVariant = "Map" Then
			
			DataFromColumn	= DataString[ColumnName];
			FilterByMaps	=	New Structure("FullName, DataFromColumn", ValueTableRow.FullName + "." + ValueTableRow.Name, DataFromColumn);
			MapStringArray	= MapTable.FindRows(FilterByMaps);
			
			If MapStringArray.Count() > 0 Then
				MapStringArray[0].DataFromDatabase = Value;
			EndIf;
			
		EndIf;
		
	EndIf;
	
	Return Value;
EndFunction

&AtServer
Function CreateUpdateAttributeValue(KeyName,ObjectRef,DataString,SearchStructure=Undefined)
	
	If ValueIsFilled(ObjectRef) Then
		If NOT UpdateAttributeValuesForRelatedObjects Then
			Return	ObjectRef;
		EndIf;
	EndIf;
	
	SetPrivilegedMode(True);
	
	FilterByAttributes		= New Structure("KeyName, Use", KeyName, True);
	AttributeStringArray	= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
	
	If AttributeStringArray.Count() = 0 Then Return ObjectRef; EndIf;
	
	FullName		= AttributeStringArray[0].FullName;
	AttributeString	= AttributeStringArray[0];
	
	If Find(FullName,"Document.") > 0 Then Return ObjectRef; EndIf;
	
	NewObject	=Undefined;
	
	If ValueIsFilled(ObjectRef) Then
		NewObject	= ObjectRef.GetObject();
	Else
		NewObject	= GetObjectManagerByFullName(FullName).CreateItem();
		NewObject.Fill(Undefined);
		
		Try
			NewObject.SetNewCode();
		Except	EndTry;
	EndIf;
	
	For Each ValueTableRowOfAttributes In AttributeStringArray Do
		
		AttributeName	= ValueTableRowOfAttributes.Name;
		KeyName			= ValueTableRowOfAttributes.KeyName;
		
		AttributeValue	= GetValueForAttribute(KeyName, DataString, FullName, AttributeName);
		
		If AttributeValue = Undefined Then Continue; EndIf;
		
		Try
			NewObject[AttributeName]	= AttributeValue;
		Except EndTry;
		
	EndDo;
	
	If SearchStructure <> Undefined Then
		
		For Each StructureItem In SearchStructure Do
			NewObject[StructureItem.Key]	= StructureItem.Value;
		EndDo;
		
	Else
		If FullName = FullNameStructure.Packings Then
			NewObject.Owner	= Catalogs.PackingSets.BaseMeasurementUnit;
		EndIf;
	EndIf;
	
	Try
		NewObject.DataExchange.Load	= True;
	Except EndTry;
	
	//Filling in additional attributes of characteristics
	If FullName = FullNameStructure.Characteristics Then
		
		ValueTable_Products					= DataListFromDatabase.Get(FullNameStructure.Products);
		Filter								= New Structure("Ref",DataString.Products);
		ProductsStringArray					= ValueTable_Products.FindRows(Filter);
		ProductsKind						= ProductsStringArray[0].ProductsCategory;
		TabularSection_AdditionalAttributes	= NewObject.AdditionalAttributes;
		
		FillAdditionalAttributesObject(TabularSection_AdditionalAttributes, ProductsKind, DataString, False)
		
	EndIf;
		
	Try
		NewObject.Write();
		
		ObjectRef		= NewObject.Ref;
		CountCreated	= CountCreated + 1;
		
		ValueTable			= DataListFromDatabase.Get(FullName);
		NewValueTableRow	= ValueTable.Add();
		FillPropertyValues(NewValueTableRow, NewObject);
		
		For each Column In ValueTable.Columns Do
			
			Value	= NewValueTableRow[Column.Name];
			
			If TypeOf(Value) <> Type("String") Then Continue; EndIf;
			
			NewValueTableRow[Column.Name]	= TrimAll(Lower(Value));
			
		EndDo;
		
	Except
		Message(ErrorDescription());
	EndTry;
	
	Return ObjectRef;
	
EndFunction

&AtServer
Function GetObjectManagerByFullName(FullName)
	
	WordArray	= ExpandStringInArrayOfSubstringsServer(FullName, ".");
	ObjectName	= WordArray[1];
	
	If Find(FullName, "Catalog.") > 0 Then
		Manager	= Catalogs[ObjectName];
	ElsIf Find(FullName, "ChartOfCharacteristicTypes.") > 0 Then
		Manager	= ChartsOfCharacteristicTypes[ObjectName];
	ElsIf Find(FullName, "Document.") > 0 Then
		Manager	= Documents[ObjectName];
	ElsIf Find(FullName, "InformationRegister.") > 0 Then
		Manager	= InformationRegisters[ObjectName];
	EndIf;
	
	Return Manager;
	
EndFunction

&AtServer
Procedure FillDataListFromDatabase(FullNameSelected = Undefined)
	
	DataListFromDatabase	= New Map;
	FullNameArray			= New Array;
	
	StringArray	= AttributeTable.FindRows(New Structure("SearchFieldInDatabase", True));
	
	For each ValueTableRow In StringArray Do
		If FullNameArray.Find(ValueTableRow.FullName) = Undefined Then
			FullNameArray.Add(ValueTableRow.FullName);
		EndIf;
	EndDo;
	
	StringArray	= AttributeTableOfCreatedObjects.FindRows(New Structure("SearchFieldInDatabase", True));
	
	For each ValueTableRow In StringArray Do
		If FullNameArray.Find(ValueTableRow.FullName)	= Undefined Then
			FullNameArray.Add(ValueTableRow.FullName);
		EndIf;
	EndDo;
	
	//5.01
	StringArray	= AttributeTable.FindRows(New Structure("FullName, Use", FullNameStructure.AdditionalAttributes + "__", True));
	
	If StringArray.Count() > 0 Then
		FullNameArray.Add(FullNameStructure.PropertyValues);
	EndIf;
	//
	
	For Each FullName In FullNameArray Do
		
		If Find(FullName,"Document.") > 0 Then Continue; EndIf;
		If FullNameSelected <> Undefined AND FullNameSelected <> FullName Then Continue; EndIf;
		
		IsAttributesCreated	= (FullName = FullNameStructure.Barcodes);
		Filter				= New Structure("FullName, SearchFieldInDatabase", FullName, True);
		StringArray			= AttributeTable.FindRows(Filter);
		
		If StringArray.Count() = 0 Then
			StringArray	= AttributeTableOfCreatedObjects.FindRows(Filter);
		EndIf;
		
		QueryFields		= "";
		IndexingFields	= "";
		
		For each ValueTableRow In StringArray Do
			
			If Find(QueryFields, "FullName." + ValueTableRow.Name) > 0 Then Continue; EndIf;
			
			QueryFields 	= QueryFields + "FullName." + ValueTableRow.Name + " AS " + ValueTableRow.Name + "," + Chars.LF;
			IndexingFields	= IndexingFields + ValueTableRow.Name + ",";
			
		EndDo;
		
		QueryFields		= CutLastSymbol(QueryFields, ",");
		IndexingFields	= CutLastSymbol(IndexingFields, ",");
		MetadataObject	= Metadata.FindByFullName(FullName);
		
		If NOT IsAttributesCreated Then
			
			IsGroup	= (MetadataObject.Hierarchical AND MetadataObject.HierarchyType = Metadata.ObjectProperties.HierarchyType.HierarchyFoldersAndItems);
			
			If NOT IsGroup Then
				QueryText	= "SELECT FullName.Ref" + ?(QueryFields = "", "", "," + QueryFields) + " FROM &FullName AS FullName WHERE NOT FullName.DeletionMark"; //5.01
			ElsIf IsGroup Then
				QueryText	= "SELECT FullName.Ref" + ?(QueryFields = "", "", "," + QueryFields) + " FROM &FullName AS FullName WHERE NOT FullName.DeletionMark AND NOT FullName.IsFolder"; //5.01
			EndIf;
			
		Else
			QueryText	= "SELECT * FROM &FullName AS FullName";
			
			//5.04
			If FullNameArray.Find(FullNameStructure.Products) = Undefined Then
				FullNameArray.Add(FullNameStructure.Products);
			EndIf;
			//
		EndIf;
		
		If FullName = FullNameStructure.Products Then
			QueryText	= "SELECT FullName.Ref,FullName.Parent," + ?(IsBlankString(QueryFields), "", QueryFields + ",") + "  //5.04
			|FullName.UseCharacteristics AS UseCharacteristics,
			|FullName.ProductsCategory,
			|FullName.UseSerialNumbers
			|FROM &FullName AS FullName WHERE NOT FullName.DeletionMark AND NOT FullName.IsFolder";
		EndIf;
		
		If FullName = FullNameStructure.PropertyValues Then
			ValueTable	= GetProductsKindCustomizationTableAndAdditionalAttributes();
			DataListFromDatabase.Insert(FullNameStructure.AdditionalAttributes, ValueTable);
		EndIf;
		
		If FullName = FullNameStructure.SupplierProducts Then
			QueryText		= "SELECT FullName.Ref,FullName.Products," + QueryFields + " FROM &FullName AS FullName WHERE NOT FullName.DeletionMark AND NOT FullName.IsFolder";
			IndexingFields	= "Products," + IndexingFields;
		EndIf;
						
		QueryText	= StrReplace(QueryText, "&FullName", FullName);
		Query		= New Query(QueryText);
		ValueTable	= Query.Execute().Unload();
		
		For Each ValueTableRow In ValueTable Do
			
			For Each Column In ValueTable.Columns Do
				
				Value	= ValueTableRow[Column.Name];
				
				If TypeOf(Value) <> Type("String") Then Continue; EndIf;
				
				ValueTableRow[Column.Name]	= Lower(TrimAll(Value));
				
			EndDo;
			
		EndDo;
		
		ValueTable.Indexes.Add(IndexingFields);
		
		If FullName = FullNameStructure.Products Then
			ValueTable.Indexes.Add("Ref");
		EndIf;
		
		DataListFromDatabase.Insert(FullName, ValueTable);
		
	EndDo;
	
EndProcedure

&AtServer
Function GetProductsKindCustomizationTableAndAdditionalAttributes()
	
	//5.07s
	Query		= New Query;
	Query.Text	=
		"SELECT
		|	AdditionalAttributesAndInformationSetsAdditionalAttributes.Property AS RefChartOfCharacteristicTypes,
		|	AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref AS PropertySet
		|INTO TemporaryTable_Properties
		|FROM
		|	Catalog.AdditionalAttributesAndInfoSets.AdditionalAttributes AS AdditionalAttributesAndInformationSetsAdditionalAttributes
		|WHERE
		|	AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref IN HIERARCHY(&Parent)
		|	AND NOT AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref.IsFolder
		|	AND NOT AdditionalAttributesAndInformationSetsAdditionalAttributes.Ref.DeletionMark
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|SELECT
		|	ProductsKinds.Ref AS ProductsKind,
		|	TemporaryTable_Properties.RefChartOfCharacteristicTypes AS RefChartOfCharacteristicTypes,
		|	TemporaryTable_Properties.PropertySet.Parent = &SetParent AS AreProducts,
		|	TemporaryTable_Properties.PropertySet.Description AS Description,
		|	TemporaryTable_Properties.PropertySet AS PropertySet
		|FROM
		|	Catalog.ProductsCategories AS ProductsKinds
		|		LEFT JOIN TemporaryTable_Properties AS TemporaryTable_Properties
		|		ON (TemporaryTable_Properties.PropertySet = ProductsKinds.PropertySet
		|				OR TemporaryTable_Properties.PropertySet = &SetProductsCommon
		|				OR ProductsKinds.SetOfCharacteristicProperties = &CharacteristicSetCommon
		|				OR TemporaryTable_Properties.PropertySet = &CharacteristicSetCommon)";
	
	Query.SetParameter("SetProductsCommon", Catalogs.AdditionalAttributesAndInfoSets.Catalog_Products_Common);
	Query.SetParameter("SetParent", Catalogs.AdditionalAttributesAndInfoSets.Catalog_Products);
	Query.SetParameter("CharacteristicSetCommon", Catalogs.AdditionalAttributesAndInfoSets.Catalog_ProductsCharacteristics);
	
	SetArray	= New Array;
	SetArray.Add(Catalogs.AdditionalAttributesAndInfoSets.Catalog_Products);
	SetArray.Add(Catalogs.AdditionalAttributesAndInfoSets.Catalog_ProductsCharacteristics);
	Query.SetParameter("Parent", SetArray);
	
	ValueTable_ProductsKindAdditionalAttributes = Query.Execute().Unload();
	ValueTable_ProductsKindAdditionalAttributes.Indexes.Add("ProductsKind,AreProducts");
	Return ValueTable_ProductsKindAdditionalAttributes;
EndFunction

//Filling the table with attributes of the object being created
&AtServer
Procedure FillTableWithAttributesOfCreatedObject(RowIndex)
	
	SimpleTypeArray	= New Array;
	SimpleTypeArray.Add(Type("String"));
	SimpleTypeArray.Add(Type("Number"));
	SimpleTypeArray.Add(Type("Date"));
	SimpleTypeArray.Add(Type("Boolean"));
	SimpleTypeArray.Add(Type("ValueStorage"));
	SimpleTypeArray.Add(Type("UUID"));
	
	ArrayOfExcludedAttributes	= New Array;
	ArrayOfExcludedAttributes.Add("Ref");
	ArrayOfExcludedAttributes.Add("ValueType");
	ArrayOfExcludedAttributes.Add("Predefined");
	ArrayOfExcludedAttributes.Add("PredefinedDataName");
	ArrayOfExcludedAttributes.Add("IsFolder");
	ArrayOfExcludedAttributes.Add("DeletionMark");
	
	ExceptionArray	= New Array;
	ExceptionArray.Add(FullNameStructure.Products);

	CurrentData		= AttributeTable.FindByID(RowIndex);
	KeyName			= CurrentData.KeyName;
	MetadataObject	= Metadata.FindByType(CurrentData.Type.Types()[0]);
	
	If MetadataObject = Undefined Then Return; EndIf;
	
	FullName	= MetadataObject.FullName();
	
	If Find(FullName,"Enum.")>0 Then Return; EndIf;
	
	If ExceptionArray.Find(FullName)<>Undefined Then Return; EndIf;
	
	For each Attribute In MetadataObject.StandardAttributes Do
		
		AttributeName	= Attribute.Name;
		
		If ArrayOfExcludedAttributes.Find(AttributeName) <> Undefined Then Continue; EndIf;
		
		FilterByAttributes	= New Structure("KeyName, FullName, Name", KeyName, FullName, AttributeName);
		StringArray			= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
		
		If StringArray.Count() > 0 Then Continue; EndIf;
		
		AddAttributeToAttributesTable(MetadataObject, AttributeName, , KeyName);
		
	EndDo;
	
	For each Attribute In MetadataObject.Attributes Do
		
		AttributeName	= Attribute.Name;
		
		If Find(AttributeName, "Delete") > 0 Then Continue; EndIf;
		
		CurrentType	= Attribute.Type.Types()[0];
		
		If SimpleTypeArray.Find(CurrentType) = Undefined Then
			AttributeMetadataObject	= Metadata.FindByType(CurrentType);
			FullAttributeName		= AttributeMetadataObject.FullName();
		EndIf;
		
		FilterByAttributes	= New Structure("KeyName, FullName, Name", KeyName, FullName, AttributeName);
		StringArray			= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
		
		If StringArray.Count() > 0 Then Continue; EndIf;
		
		AddAttributeToAttributesTable(MetadataObject, AttributeName, , KeyName);
		
	EndDo;
	
	AttributeTableOfCreatedObjects.Sort("SearchFieldInDatabase DESC, RequiredForFilling DESC, Synonym");
	
EndProcedure
 
#EndRegion

#Region SearchProductsBySearchFieldsAndTransferToSelected

&AtServer
Procedure MoveSelectedPositionsAndFindProductsInBase()
	
	CreateTableColumnsSelected();
	
	FullSourceName				= FullNameStructure.Products;
	AttributeNameOfProductsRef	= "Ref";
	FilterByAttributes			= New Structure("FullName, SearchFieldInDatabase", FullSourceName, True);
	AttributeStringArray		= AttributeTable.FindRows(FilterByAttributes);
	
	If AttributeStringArray.Count() = 0 Then
		
		FullSourceName				= FullNameStructure.Barcodes;
		FilterByAttributes			= New Structure("FullName, SearchFieldInDatabase", FullSourceName, True);
		AttributeStringArray		= AttributeTable.FindRows(FilterByAttributes);
		AttributeNameOfProductsRef	= "Products";
		
		If AttributeStringArray.Count() = 0 Then
			FullSourceName				= FullNameStructure.SupplierProducts;
			FilterByAttributes			= New Structure("FullName, SearchFieldInDatabase", FullSourceName, True);
			AttributeStringArray		= AttributeTable.FindRows(FilterByAttributes);
			AttributeNameOfProductsRef	= "Products";
		EndIf;
		
	EndIf;
	
	FillDataListFromDatabase();
	
	Filter						= New Structure;
	ValueTable_Products			= DataListFromDatabase.Get(FullSourceName);
	ValueTableFilter			= New Structure("Check",True);
	ValueTableStringArrayLoaded	= TableOfLoadedData.FindRows(ValueTableFilter);
	
	For Each ValueTableRowSource In ValueTableStringArrayLoaded Do
		
		NewValueTableRow	= TableSelected.Add();
		FillPropertyValues(NewValueTableRow, ValueTableRowSource);
		
		AllEmptySearchFields	= True;
		
		For each ValueTableRowOfAttributes In AttributeStringArray Do
			
			AttributeName	= ValueTableRowOfAttributes.Name;
			KeyName			= ValueTableRowOfAttributes.KeyName;
			
			Value			= GetValueForAttribute(KeyName, ValueTableRowSource);

			//5.06
			Try
				NewValueTableRow[AttributeName]	= Value;
			Except EndTry;
			
			If TypeOf(Value) = Type("String") Then
				Value	= Lower(Value);
			EndIf;
			//
			
			Filter.Insert(AttributeName, Value);
			
			If ValueIsFilled(Value) AND AllEmptySearchFields Then
				AllEmptySearchFields	= False;
			EndIf;
			
		EndDo;
		
		If AllEmptySearchFields AND HierarchyType = 0 Then
			Continue;
		EndIf;
		
		Items.TableSelectedParent.Visible	= (HierarchyType <> 0);
		
		KeyParent	= FullNameStructure.Products + ".Parent";
		
		If HierarchyType = 1 AND ValueTableRowSource.HierarchyLevel <> 0 Then
			
			GroupSeparator			= "//";
			ParentValue				= GetValueForAttribute(KeyParent,ValueTableRowSource);
			ParentValue				= GetHierarchyLine(ParentValue,ValueTableRowSource.HierarchyLevel);
			NewValueTableRow.Parent	= ParentValue;
			
		ElsIf HierarchyType = 2 Then
			ParentValue				= GetValueForAttribute(KeyParent,ValueTableRowSource);
			NewValueTableRow.Parent	= ParentValue;
		EndIf;
		
		StringArray	= ValueTable_Products.FindRows(Filter);
		
		If StringArray.Count() > 0 Then
			NewValueTableRow.Products	= StringArray[0][AttributeNameOfProductsRef];
		EndIf;
		
	EndDo;
	
	//Display the document download button if required
	If ValueIsFilled(DocumentName) OR FullNameOfCurrentAction = FullNameStructure.Prices + "__" Then
		
		If FullNameOfCurrentAction = FullNameStructure.Prices + "__" Then
			WordArray	= ExpandStringInArrayOfSubstringsServer(FullNameStructure.Prices, ".");
		Else
			WordArray	= ExpandStringInArrayOfSubstringsServer(DocumentName, ".");
		EndIf;
		
		Items.CreateDocument.Title		= Nstr("en='2. Load data in <';pl='2. Pobierz dane w <';ru='2. Загрузить данные в <';ro='2. Load data in <'") + WordArray[1] + ">";
		Items.CreateDocument.Visible	= True;
		
	Else
		Items.CreateDocument.Visible	= False;
	EndIf;
	
	ProcessSelectedData();
	
EndProcedure

&AtServer
Procedure ProcessSelectedData()
	
	FullNameTableSelected	= FormAttributeToValue("TableSelected");
	ValueTableCopy			= FullNameTableSelected.Copy();
	
	If ValueTableCopy.Columns.Find("Parent") <> Undefined Then
		ValueTableCopy.Indexes.Add("Parent");
	EndIf;

	FullNameTableSelected.Clear();
	
	For each ValueTableRow In ValueTableCopy Do
		
		If HierarchyType <> 0 AND FullNameTableSelected.Columns.Find("Parent") <> Undefined Then
			
			If IsBlankString(ValueTableRow.Parent) Then Continue; EndIf;
			
			//Determine whether a position is a group by checking it in the Parent column
			HierarchyString	= ValueTableRow.Parent + GroupSeparator + ValueTableRow.Description;
			Filter			= New Structure("Parent", HierarchyString);
			StringArray		= ValueTableCopy.FindRows(Filter);
			
			If StringArray.Count() > 0 Then Continue; EndIf;
			
		EndIf;
		
		NewRow	= FullNameTableSelected.Add();
		FillPropertyValues(NewRow,ValueTableRow);
		
	EndDo;
	
	PutDownProductsGroup(FullNameTableSelected);
	ValueToFormAttribute(FullNameTableSelected, "TableSelected");
	
EndProcedure

&AtServer
Function GetDataProductsSearchStructureInDatabase(FullName)
	
	Filter				= New Structure;
	FilterByAttributes	= New Structure("FullName,SearchFieldInDatabase", FullName, True);
	StringArray			= AttributeTable.FindRows(FilterByAttributes);
	
	If StringArray.Count() = 0 Then
		StringArray	= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
	EndIf;
	
	For each ValueTableRow In StringArray Do
		Filter.Insert(ValueTableRow.Name,);
	EndDo;
	
	Return Filter;
	
EndFunction

&AtServer
Procedure CreateTableColumnsSelected()
	
	TableSelected.Clear();
	DeleteTableColumnsSelected();
	
	ArrayColumnNames	= New Array;
	GetArrayOfSelectedColumns(ArrayColumnNames);
	
	For Each ColumnName In ArrayColumnNames Do
		AddColumn("TableSelected", ColumnName);
	EndDo;
	
	AttributeStructure	= GetDataProductsSearchStructureInDatabase(FullNameStructure.Products);
	
	For each FilterItem In AttributeStructure Do
		
		If FilterItem.Key	= "Description" Then Continue; EndIf;
			
		AddColumn("TableSelected", FilterItem.Key, True);
		
	EndDo;
	
EndProcedure

&AtServer
Procedure DeleteTableColumnsSelected()
	
	FieldArray	= GetTableFieldListSelected();
	ChangeAttributes( , FieldArray);
	
	For Each Attribute In FieldArray Do
		
		FormItem	= Items.Find(StrReplace(Attribute, ".", ""));
		
		If FormItem <> Undefined Then
			Items.Delete(FormItem);
		EndIf;
		
	EndDo;
	
EndProcedure

&AtServer
Function GetTableFieldListSelected()
	
	ExceptionAttributeArray	= New Array;
	ExceptionAttributeArray.Add("Products");
	ExceptionAttributeArray.Add("Parent");
	ExceptionAttributeArray.Add("LineNumber");
	ExceptionAttributeArray.Add("ProductGroup");
	ExceptionAttributeArray.Add("Description");

	FieldArray				= New Array;
	FullNameTableSelected	= FormAttributeToValue("TableSelected");
	
	For each Column In FullNameTableSelected.Columns Do
		
		If ExceptionAttributeArray.Find(Column.Name)	= Undefined Then
			FieldArray.Add("TableSelected." + Column.Name);
		EndIf;
		
	EndDo;
	
	Return FieldArray;
	
EndFunction

&AtServer
Procedure GetArrayOfSelectedColumns(ArrayColumnNames)
	
	For each ValueTableRow In AttributeTable Do
		
		If NOT ValueTableRow.Use Then Continue; EndIf;
		
		ColumnName	= ValueTableRow.ColumnName;
		WordArray	= ExpandStringInArrayOfSubstringsServer(ColumnName, ",");
		
		For each ColumnName In WordArray Do
			
			If ArrayColumnNames.Find(ColumnName) = Undefined AND NOT IsBlankString(ColumnName) Then
				ArrayColumnNames.Add(ColumnName);
			EndIf;
			
		EndDo;
		
	EndDo;
	
	For each ValueTableRow In AttributeTableOfCreatedObjects Do
		
		If NOT ValueTableRow.Use Then Continue; EndIf;
		
		ColumnName	= ValueTableRow.ColumnName;
		WordArray	= ExpandStringInArrayOfSubstringsServer(ColumnName, ",");
		
		For each ColumnName In WordArray Do
			If ArrayColumnNames.Find(ColumnName) = Undefined AND NOT IsBlankString(ColumnName) Then
				ArrayColumnNames.Add(ColumnName);
			EndIf;
		EndDo
		
	EndDo;
	
EndProcedure

#EndRegion

#Region CreatingProductsAndLoadingPicturesPacksAdditionalAttributesCharacteristicsSeries

&AtServer
Function CheckWhetherRequiredAttributesAreSelected()
	
	HasErrors = False;
		
	Return HasErrors;
	
EndFunction

&AtClient
Procedure LoadProducts(Command)
	
	Notification	= New NotifyDescription("LoadProductsEnd", ThisObject);
	Text			= "";
	
	If CreateProductsWhenLoading Then
		Text	= Nstr("en='For all selected positions from the current table, for which the products list is NOT found  "
"in the database using the search fields, a new products list will be created!';pl='Dla wszystkich wybranych pozycji z bieżącej tabeli, dla których lista nomenklatury NIE zostanie znaleziona "
"w bazie danych za pomocą pól wyszukiwania, zostanie utworzona nowa lista nomenklatury!';ru='Для всех выбранных позиций из текущей таблицы, для которых список номенклатуры НЕ будет найден в базе данных"
"с помощью полей поиска, будет создан новый список номенклатуры';ro='For all selected positions from the current table, for which the products list is NOT found  "
"in the database using the search fields, a new products list will be created!'");
	EndIf;
	
	If UpdateProductsAndPropertyData Then
		Text	= Text + Chars.LF + Nstr("en='For all selected positions from the current table for which the FOUND products is in the database, "
"the selected details will be updated with the data from the file! Continue?';pl='Dla wszystkich wybranych pozycji z bieżącej tabeli, dla których ZNALIEZIONA nomenklatura znajduje się w bazie danych, "
"wybrane szczegóły zostaną zaktualizowane danymi z pliku! Dalej?';ru='Для всех выбранных позиций из текущей таблицы, для которых НАЙДЕНННАЯ номенклатура находится в базе,"
"выбранные данные будут обновлены данными из файла. Продолжить?';ro='For all selected positions from the current table for which the FOUND products is in the database, "
"the selected details will be updated with the data from the file! Continue?'");
	EndIf;
	
	If CreateProductsWhenLoading OR UpdateProductsAndPropertyData Then
		ShowQueryBox(Notification, Text, QuestionDialogMode.YesNo);
	Else
		ExecuteNotifyProcessing(Notification);
	EndIf;
	
EndProcedure

&AtClient
Procedure LoadProductsEnd (Result,Parameters) Export
	
	If Result	= DialogReturnCode.No Then Return; EndIf;

	CreatedProductsCount	= 0;
	CountCreated			= 0;
	
	Text	= String(CurrentDate()) + Nstr("en=' Started the process of loading data into the database ...';pl=' Rozpoczęto proces ładowania danych do bazy danych ...';ru=' Начат процесс загрузки данных в базу данных ...';ro=' Started the process of loading data into the database ...'");
	Status("Status", , Text);
	Message(Text);
	
	CreateProductsServer();
	
	Message(String(CurrentDate()) + Nstr("en=' Loading is complete! Created / updated products: ';pl=' Pobieranie zakończone! Utworzona / zaktualizowana nomenklatura: ';ru=' Загрузка завершена! Создана / обновлена номенклатура: ';ro=' Loading is complete! Created / updated products: '") 
								  + String(CreatedProductsCount)
								  + Nstr("en=' position created / updated related data: ';pl=' utworzone pozycje / zaktualizowane powiązane dane: ';ru=' созданные позиции / обновленные связанные данные: ';ro=' position created / updated related data: '") 
								  + String(CountCreated));
EndProcedure

//Create products
&AtServer
Procedure CreateProductsServer()
	
	DeleteProductsPictures();
	FillDataListFromDatabase();
	
	FullNameTableSelected		= FormAttributeToValue("TableSelected");
	FullProductsName			= FullNameStructure.Products;
	ProductsManager				= GetObjectManagerByFullName(FullNameStructure.Products);
	MetadataObjectProducts		= Metadata.FindByFullName(FullProductsName);
	SearchAttributeStructure	= GetDataProductsSearchStructureInDatabase(FullProductsName);
	
	ValueTable_Products							= DataListFromDatabase.Get(FullProductsName);
	ValueTable_ProductsKindAdditionalAttributes	= DataListFromDatabase.Get(FullNameStructure.AdditionalAttributes);
	NeedToDownloadAdditionalAttributes			= (ValueTable_ProductsKindAdditionalAttributes <> Undefined);
	
	StringArray						= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.Characteristics, "Ref", True));
	NeedToDownloadCharacteristics	= (StringArray.Count() <> 0);
	
	StringArray						= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.Series, "Ref", True));
	NeedDownloadSeries				= (StringArray.Count() <> 0);
	
	StringArray						= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.CustomsDeclaration, "Ref", True));
	NeedDownloadCustomsDeclaration	= (StringArray.Count() <> 0);
	
	StringArray						= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.Packings, "Ref", True));
	NeedToLoadPackages				= (StringArray.Count() <> 0);
	
	StringArray						= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.Barcodes, "Barcode", True));
	NeedDownloadBarcode				= (StringArray.Count() <> 0);
	
	StringArray						= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.SupplierProducts, "Owner", True));
	NeedDownloadSuppliersProducts	= (StringArray.Count() <> 0);
	
	FullNameAttributeTable			= FormAttributeToValue("AttributeTable");
	FillFoldersTable();
	
	For each ValueTableRowSelected In FullNameTableSelected Do
		
		FullNameProducts	= Undefined;
		ProductsRef			= ValueTableRowSelected.Products;
		IsNewObject			= False;
		
		If CreateProductsWhenLoading AND ProductsRef.IsEmpty() Then
			
			FullNameProducts	= ProductsManager.CreateItem();
			KeyName				= FullProductsName + ".ProductsCategory";
			ProductsKind		= GetValueForAttribute(KeyName, ValueTableRowSelected);
			FillingData			= New Structure("ProductsCategory", ProductsKind);
			
			FullNameProducts.Fill(FillingData);
			FullNameProducts.SetNewCode();
			FullNameProducts.DataExchange.Load	= WriteProductsInExchangeMode;
			
			IsNewObject	= True;
			
		EndIf;
		
		If (UpdateProductsAndPropertyData OR UploadPicturesWhenLoadingProducts) AND NOT ProductsRef.IsEmpty() Then
			
			FullNameProducts	= ProductsRef.GetObject();
			
			If FullNameProducts = Undefined Then Continue; EndIf;
			
			FullNameProducts.DataExchange.Load	= WriteProductsInExchangeMode;
			
		EndIf;
		
		If FullNameProducts <> Undefined Then
						
			FilterByAttributes		= New Structure("FullName, Use", FullProductsName, True);
			AttributeStringArray	= AttributeTable.FindRows(FilterByAttributes);
			
			For each ValueTableRowOfAttributes In AttributeStringArray Do
				
				AttributeName		= ValueTableRowOfAttributes.Name;
				KeyName				= ValueTableRowOfAttributes.KeyName;
				FullAttributeName	= ValueTableRowOfAttributes.FullAttributeName;
				
				If FullAttributeName = FullNameStructure.Pictures Then Continue; EndIf;
				
				AttributeValue	= GetValueForAttribute(KeyName, ValueTableRowSelected);
				
				If AttributeValue = Undefined Then Continue; EndIf;
				
				If HierarchyType <> 0 AND AttributeName = "Parent" Then
					PathFoldersString	= ValueTableRowSelected["Parent"];
					AttributeValue		= GetProductsParent(FullNameAttributeTable, PathFoldersString);
				EndIf;
				
				Try
					FullNameProducts[AttributeName]	= AttributeValue;
				Except
				EndTry;
				
			EndDo;
			
			WriteProducts	= True;
			
			//5.04
			If SearchAttributeStructure.Count() > 0 Then
				
				FillPropertyValues(SearchAttributeStructure, FullNameProducts);
				ProductsStringArray	= ValueTable_Products.FindRows(SearchAttributeStructure);
				
				If IsNewObject AND ProductsStringArray.Count() > 0 Then
					ValueTableRowSelected.Products		= ProductsStringArray[0].Ref;
					CreatedProductsCount				= CreatedProductsCount + 1;
					WriteProducts						= False;
					FullNameProducts					= ProductsStringArray[0].Ref.GetObject();
					FullNameProducts.DataExchange.Load	= WriteProductsInExchangeMode;
				EndIf;
				
			EndIf;
			//
			
			If WriteProducts Then
				
				If NeedToDownloadAdditionalAttributes Then
					ProductsKind						= FullNameProducts.ProductsCategory;
					TabularSection_AdditionalAttributes	= FullNameProducts.AdditionalAttributes;
					FillAdditionalAttributesObject(TabularSection_AdditionalAttributes, ProductsKind, ValueTableRowSelected);
				EndIf;
				
				Try
					FullNameProducts.Write();
					ValueTableRowSelected.Products	= FullNameProducts.Ref;
					CreatedProductsCount			= CreatedProductsCount + 1;
					
					If IsNewObject Then
						NewValueTableRow	= ValueTable_Products.Add();
						FillPropertyValues(NewValueTableRow, FullNameProducts);
					ElsIf UpdateProductsAndPropertyData Then
						ArrayOfRowsFound	= ValueTable_Products.FindRows(New Structure("Ref", FullNameProducts.Ref));
						FillPropertyValues(ArrayOfRowsFound[0], FullNameProducts);
					EndIf;
					
				Except
					
					LineNumber	= FullNameTableSelected.IndexOf(ValueTableRowSelected) + 1;
					Message(Nstr("en='In line ';pl='W wierszu ';ru='В строке ';ro='In line '") + LineNumber
								+ NStr("en=', when loading / updating the products ';pl=', podczas pobierania / aktualizacji nomenklatury ';ru=', во время загрузки / обновления номенклатуры ';ro=', when loading / updating the products '") 
								+ "<" + FullNameProducts.Description + ">" + NStr("en=' errors occurred!';pl=' wystąpiły błędy';ru=' возникли ошибки';ro=' errors occurred!'"));
								
					Message(ErrorDescription());
				
				EndTry;
				
			EndIf;
			
		EndIf;
		
		If ValueTableRowSelected.Products.IsEmpty() Then Continue; EndIf;
		
		If NeedToDownloadCharacteristics Then
			KeyName	= FullNameStructure.Characteristics + ".Ref";
			GetCreateCharacteristic(KeyName, ValueTableRowSelected);
		EndIf;
		
		If NeedDownloadSeries Then
			KeyName	= FullNameStructure.Series + ".Ref";
			GetCreateSeries(KeyName, ValueTableRowSelected);
		EndIf;
		
		If NeedDownloadCustomsDeclaration Then
			KeyName	= FullNameStructure.CustomsDeclaration + ".Ref";
			GetCreateCustomsDeclaration(KeyName, ValueTableRowSelected);
		EndIf;
		
		If NeedToLoadPackages Then
			KeyName	= FullNameStructure.Packings + ".Ref";
			GetCreatePackage(KeyName, ValueTableRowSelected);
		EndIf;
		
		If NeedDownloadBarcode Then
			RecordBarcodeIntoDatabase(ValueTableRowSelected);
		EndIf;
		
		If UploadPicturesWhenLoadingProducts AND FullNameProducts <> Undefined Then
			WritePictureToBase(FullNameProducts, ValueTableRowSelected)
		EndIf;
		
		If NeedDownloadSuppliersProducts Then
			CreateSupplierProducts(ValueTableRowSelected);
		EndIf;
		
	EndDo;

	PutDownProductsGroup(FullNameTableSelected);
	ValueToFormAttribute(FullNameTableSelected, "TableSelected");
	
EndProcedure

//Loading additional attributes
&AtServer
Procedure FillAdditionalAttributesObject(TabularSection_AdditionalAttributes, ProductsKind, ValueTableRowSelected, AreProducts = True)
	
	ValueTable_ProductsKindAdditionalAttributes	= DataListFromDatabase.Get(FullNameStructure.AdditionalAttributes);
	
	If ValueTable_ProductsKindAdditionalAttributes = Undefined Then Return; EndIf;
	
	FilterByType						= New Structure("ProductsKind, AreProducts", ProductsKind, AreProducts);
	StringArrayOfAdditionalAttribute	= ValueTable_ProductsKindAdditionalAttributes.FindRows(FilterByType);
	
	For each ValueTableRowOfAdditionalAttributes In StringArrayOfAdditionalAttribute Do
		
		Property	= ValueTableRowOfAdditionalAttributes.RefChartOfCharacteristicTypes;
		SetName		= ValueTableRowOfAdditionalAttributes.Description + ".";
		
		FilterByAttributes		= New Structure("Name, RefChartOfCharacteristicTypes, Use", SetName, Property, True);
		AttributeStringArray	= AttributeTable.FindRows(FilterByAttributes);
		
		If AttributeStringArray.Count() = 0 Then Continue; EndIf;
		
		KeyName	= AttributeStringArray[0].KeyName;
		
		//5.05
		AttributeValue	= GetValueForAttribute(KeyName, ValueTableRowSelected, , , , New Structure("Owner", Property));
		
		If AttributeValue	= Undefined Then Continue; EndIf;
		
		Try
			ValueDescription	= GetObjectAttributeValueServer(AttributeValue, "Description");
			
			If IsBlankString(ValueDescription) Then
				
				FullNameAttributeValue	= AttributeValue.GetObject();
				
				//5.10				
				If FullNameAttributeValue	= Undefined Then
					AttributeValue	= "";
				Else
					FullNameAttributeValue.DataExchange.Load	= True;
					FullNameAttributeValue.Delete();
					AttributeValue	= "";
				EndIf;
				//
				
			EndIf;
			
		Except
		EndTry;
		
		FilterByAdditionalAttribute	= New Structure("Property", Property);
		StringArrayOfTabularSection	= TabularSection_AdditionalAttributes.FindRows(FilterByAdditionalAttribute);
		
		If StringArrayOfTabularSection.Count() > 0 Then
			LineOfATabularSection	= StringArrayOfTabularSection[0];
		Else
			LineOfATabularSection	= TabularSection_AdditionalAttributes.Add();
		EndIf;
	
		LineOfATabularSection.Property	= Property;
		LineOfATabularSection.Value		= AttributeValue;
		//
		
	EndDo;
	
EndProcedure

&AtServer
Procedure PutDownProductsGroup(FullNameTableSelected)
	
	Query		= New Query;
	Query.Text	=
	"SELECT
	|	Products.Ref,
	|	Products.Parent
	|FROM
	|	Catalog.Products AS Products
	|WHERE
	|	Products.Ref IN (&RefArray)";
	
	RefArray	= FullNameTableSelected.UnloadColumn("Products");
	Query.SetParameter("RefArray",RefArray);
	
	Selection	= Query.Execute().Select();
	
	While Selection.Next() Do
		
		Filter					= New Structure("Products", Selection.Ref);
		ValueTableStringArray	= FullNameTableSelected.FindRows(Filter);
		
		For each ValueTableRow In ValueTableStringArray Do
			ValueTableRow.ProductGroup	= Selection.Parent;
		EndDo;
		
	EndDo;
	
EndProcedure

//Creation of characteristics
&AtServer
Function GetCreateCharacteristic(KeyName,ValueTableRowSelected)
	
	CharacteristicRef	= Undefined;
	ValueTable_Products	= DataListFromDatabase.Get(FullNameStructure.Products);
	
	If ValueTable_Products = Undefined Then Return CharacteristicRef; EndIf;

	Filter				= New Structure("Ref", ValueTableRowSelected.Products);
	ProductsStringArray	= ValueTable_Products.FindRows(Filter);
	
	CharacteristicsUse	= False;
	ProductsCategory	= Undefined;
	
	If ProductsStringArray.Count() > 0 Then
		CharacteristicsUse	= ProductsStringArray[0].UseCharacteristics;
		ProductsCategory	= ProductsStringArray[0].ProductsCategory;
	Else
		Return CharacteristicRef;
	EndIf;
	
	If NOT CharacteristicsUse Then Return CharacteristicRef; EndIf;
	
	Owner				= ValueTableRowSelected.Products;
	CharacteristicRef	= GetValueForAttribute(KeyName, ValueTableRowSelected, , , , New Structure("Owner", Owner));
	
	Return CharacteristicRef;
	
EndFunction

//CreatingSeries
&AtServer
Function GetCreateSeries(KeyName,ValueTableRowSelected)
	
	SeriesRef			= Undefined;
	ValueTable_Products	= DataListFromDatabase.Get(FullNameStructure.Products);
	
	If ValueTable_Products = Undefined Then Return SeriesRef; EndIf;
	
	Filter				= New Structure("Ref", ValueTableRowSelected.Products);
	ProductsStringArray	= ValueTable_Products.FindRows(Filter);
	UseSerialNumber		= False;
	
	If ProductsStringArray.Count() > 0 Then
		UseSerialNumber	= ProductsStringArray[0].UseSerialNumber;
	Else
		Return SeriesRef;
	EndIf;

	If UseSerialNumber Then
		Owner		= ValueTableRowSelected.Products;
		SeriesRef	= GetValueForAttribute(KeyName, ValueTableRowSelected, , , , New Structure("Owner", Owner));
	EndIf;
	
	Return SeriesRef;
	
EndFunction

//Creating CustomsDeclaration
&AtServer
Function GetCreateCustomsDeclaration(KeyName,ValueTableRowSelected)
	
	CustomsDeclarationRef	= Undefined;
	CustomsDeclarationRef	= GetValueForAttribute(KeyName, ValueTableRowSelected);
	
	Return CustomsDeclarationRef;
	
EndFunction

//Creating packages and measurement unit
&AtServer
Function GetCreatePackage(KeyName,ValueTableRowSelected)
	
	PackingRef			= Undefined;
	ValueTable_Products	= DataListFromDatabase.Get(FullNameStructure.Products);
	
	If ValueTable_Products = Undefined Then Return PackingRef; EndIf;
	
	If NOT UsePackings Then Return PackingRef; EndIf;
	
	Owner		= ValueTableRowSelected.Products;
	PackingRef	= GetValueForAttribute(KeyName,ValueTableRowSelected, , , , New Structure("Owner", Owner));
	
	Return PackingRef;
	
EndFunction
//

//Writing pictures to the database when creating/updating the products
&AtServer
Procedure WritePictureToBase(FullNameProducts,ValueTableRowSelected)
	
	ValueTableStringArray	= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.Products, "PictureFile", True));
	
	If ValueTableStringArray.Count() = 0 Then Return; EndIf;
	
	Value	= ValueTableStringArray[0].ColumnName;
	ColumnNameArray	= ExpandStringInArrayOfSubstringsServer(Value,",");
	
	MetadataObject				= Metadata.FindByFullName(FullNameStructure.Pictures);
	FullName					= FullNameStructure.Pictures;
	ValueTable_AttachedFiles	= DataListFromDatabase.Get(FullName);
	Owner						= FullNameProducts.Ref;
	PictureExtension			= ".jpg";
	StringOfKnownFormats		= ".jpg,.png,.bmp,.emf,.gif,.icon,.svg,.tiff,.wmf";
	FormatArray					= ExpandStringInArrayOfSubstringsServer(StringOfKnownFormats, ",");
	
	If ToStoreFilesInVolumes Then
		
		VolumeAttributeName	= "FullPathWindows";
		
		If IsLinux Then
			VolumeAttributeName	= "FullPathLinux";
		EndIf;
		
		KeyName	= FullNameStructure.Products+".PictureFile";
		Volume	= GetValueForAttribute(KeyName, ValueTableRowSelected, FullNameStructure.Pictures, "Volume");
		
		If NOT ValueIsFilled(Volume) Then
			
			MessageText="!";
			Message(MessageText);
			Raise MessageText;
			Return;
			
		EndIf;
		
		Data	= ObjectAttributeValuesServer(Volume,VolumeAttributeName);
		PathToVolume	= Data[VolumeAttributeName];
		DirectoryName	= Format(CurrentDate(), "DF=yyyymmdd");
		PathToDirectoryInVolume	= PathToVolume + DirectoryName;
		CreateDirectory(PathToDirectoryInVolume);
		
	EndIf;
	
	For each ColumnName In ColumnNameArray Do
		
		TempStorageAddress	= ValueTableRowSelected[ColumnName];
		
		If NOT IsTempStorageURL(TempStorageAddress) Then Continue; EndIf;
		
		KeyName	= FullNameStructure.Products + ".PictureFile";
		Description	= GetValueForAttribute(KeyName, ValueTableRowSelected, FullNameStructure.Pictures, "Description");
		
		If NOT ValueIsFilled(Description) Then
			Description	= FullNameProducts.Description;
		EndIf;
		
		IndexOf	= ColumnNameArray.Find(ColumnName)+1;
		
		For each PictureFormatString In FormatArray Do
			Description	= StrReplace(Description,PictureFormatString,"");
		EndDo;
		
		Description	= ClearStringOfAllCharacters(Description,"_","_");
		Description	= Description + "_" + String(IndexOf) + PictureExtension;
		
		FileParameters = New Structure;
		FileParameters.Insert("Author", SessionParameters.CurrentUser);
		FileParameters.Insert("FilesOwner", FullNameProducts.Ref);
		FileParameters.Insert("BaseName", Description);
		FileParameters.Insert("ExtensionWithoutPoint", StrReplace(PictureExtension, ".", ""));
		FileParameters.Insert("ModificationTimeUniversal", CurrentUniversalDate());
		
		AttachedFileRef = FilesOperations.AppendFile(FileParameters,
										TempStorageAddress);

		//SearchStructure	= New Structure("Description, FileOwner", Lower(TrimAll(Description)), Owner);
		//AttachedFileRef	= GetValueForAttribute(KeyName,ValueTableRowSelected, , , , SearchStructure);
		//
		//If NOT ValueIsFilled(AttachedFileRef) OR AttachedFileRef = Undefined Then
		//	Message(Nstr("en = 'No item was created to store the file named '; 
		//				|pl = 'Nie utworzono żadnego elementu do przechowywania pliku z nazwą ';
		//				|ru = 'Не создано ни одного элемента для хранения файла с именем '") + Description + "!");
		//	Continue;
		//EndIf;
		//
		//FullNameAttachedFile	= AttachedFileRef.GetObject();
		//FullNameAttachedFile.DataExchange.Load	= True;
		//FullNameAttachedFile.CreationDate	= CurrentDate();
		
		//5.09
		//FullNameAttachedFile.Extension = StrReplace(PictureExtension, ".", "");
		//If FullNameAttachedFile.Metadata().Attributes.Find("ModificationDateUniversal") <> Undefined Then
		//	FullNameAttachedFile.ModificationDateUniversal = CurrentDate();
		//EndIf;
		//5.09
		
		//FileStorageType	= ?(ToStoreFilesInVolumes,Enums.FileStorageTypes.InVolumesOnDisk, Enums.FileStorageTypes.InInfobase);
		//FullNameAttachedFile.FileStorageType	= FileStorageType;
		//FullNameAttachedFile.Author	= SessionParameters.CurrentUser;
		
		//Create path to folder
		//If ToStoreFilesInVolumes Then				
		//	
		//	PicturePathInVolume	= PathToDirectoryInVolume + "\" + Description;
		//	Picture	= New Picture(GetFromTempStorage(TempStorageAddress));
		//	
		//	Try
		//		Picture.Write(PicturePathInVolume);
		//		FullNameAttachedFile.PathToFile	= DirectoryName + "\" + Description;
		//	Except
		//		Message("Could not write picture <" + Description + ">!");
		//		Message(ErrorDescription());
		//	EndTry;
		//	
		//	Try
		//		FullNameAttachedFile.Write();
		//	Except
		//		Message(ErrorDescription());
		//		Continue;
		//	EndTry;
		//	
		//EndIf;
		//
		//Try
		//	FullNameAttachedFile.Write();
		//Except
		//	Message(ErrorDescription());
		//	Continue;
		//EndTry;
		
		If CreateProductsWhenLoading OR UpdateProductsAndPropertyData Then
			
			If IndexOf = 1 Then //The first picture is always the main
				FullNameProducts.PictureFile	= AttachedFileRef;
				FullNameProducts.Write();
			EndIf;
			
		EndIf;
		
		//If NOT ToStoreFilesInVolumes AND (UpdatePictureDataAvailableInBase OR UploadPicturesWhenLoadingProducts) Then
		//	
		//	If Metadata.InformationRegisters.Find("AttachedFiles") <> Undefined Then
		//		
		//		Write				= InformationRegisters.AttachedFiles.CreateRecordManager();
		//		Write.AttachedFile	= AttachedFileRef;
		//		Write.StoredFile	= New ValueStorage(New Picture(GetFromTempStorage(TempStorageAddress)));
		//		
		//		Try
		//			Write.Write();
		//		Except
		//			Message(ErrorDescription());
		//		EndTry;
		//		
		//	ElsIf Metadata.InformationRegisters.Find("BinaryFileData") <> Undefined Then
		//		
		//		Write		= InformationRegisters.BinaryFileData.CreateRecordManager();
		//		Write.File	= AttachedFileRef;
		//		
		//		//5.14
		//		If OldPictureRecordFormat Then
		//			Write.BinaryFileData	= New ValueStorage(New Picture(GetFromTempStorage(TempStorageAddress))); 
		//		Else
		//			Write.BinaryFileData	= New ValueStorage(GetFromTempStorage(TempStorageAddress)); 
		//		EndIf;
		//		
		//		//5.14
		//		Try
		//			Write.Write();
		//		Except
		//			Message(ErrorDescription());				
		//		EndTry;
		//		
		//	EndIf;
		//	
		//EndIf;
		
	EndDo;
	
EndProcedure

&AtServer
Procedure DeleteProductsPictures()
	
	If NOT DeleteOldPicturesFROMBaseBeforeLoadingNew Then Return; EndIf;
	
	ArrayOfLinksToBeRemoved	= New Array;
	
	For each ValueTableRowSelected In TableSelected Do
		If ValueTableRowSelected.Products.IsEmpty() Then Continue; EndIf;
		ArrayOfLinksToBeRemoved.Add(ValueTableRowSelected.Products);
	EndDo;
	
	AttachedFilesManager	= GetObjectManagerByFullName(FullNameStructure.Pictures);
	
	Query		= New Query;
	Query.Text	=
		"SELECT ProductsAttachedFiles.Ref, ProductsAttachedFiles.Volume, ProductsAttachedFiles.PathToFile, ProductsAttachedFiles.FileStorageType, ProductsAttachedFiles.Description, ProductsAttachedFiles.FileOwner FROM
		|	" + FullNameStructure.Pictures + " AS
		|ProductsAttachedFiles
		|	WHERE ProductsAttachedFiles.FileOwner IN (&RefArray)";
	
	Query.SetParameter("RefArray", ArrayOfLinksToBeRemoved);
	Selection	= Query.Execute().Select();
	
	While Selection.Next() Do
		
		BeginTransaction();
		FullNameProducts	= Selection.FileOwner.GetObject();
		
		If FullNameProducts <> Undefined Then
			FullNameProducts.PictureFile		= AttachedFilesManager.EmptyRef();
			FullNameProducts.DataExchange.Load	= True;
			FullNameProducts.Write();
		EndIf;
		
		If Selection.FileStorageType	= Enums.FileStorageTypes.InVolumesOnDrive Then
			
			VolumeAttributeName	= "FullPathWindows";
			
			If IsLinux Then
				VolumeAttributeName	= "FullPathLinux";
			EndIf;
			
			Data			= ObjectAttributeValuesServer(Selection.Volume, VolumeAttributeName);
			PathToVolume	= Data[VolumeAttributeName];
			PathToPictureToBeRemoved	= PathToVolume + Selection.PathToFile;
			
			Try
				PictureFile	= New File(PathToPictureToBeRemoved); //set attributes to delete
				If PictureFile.GetReadOnly() Then
					PictureFile.SetReadOnly(False);
				EndIf;
				
				DeleteFiles(PathToPictureToBeRemoved);
				
				FullNamePicture	= Selection.Ref.GetObject();
				FullNamePicture.Delete();
				
			Except
				Message("Could not delete picture" + Selection.Description + " on path " + PathToPictureToBeRemoved + "!");
				Message(ErrorDescription());
			EndTry;
		Else
			Try
				FullNamePicture	= Selection.Ref.GetObject();
				FullNamePicture.Delete();
			Except
				Message("Could not delete picture" + Selection.Description + "!");
				Message(ErrorDescription());
			EndTry;
			
		EndIf;
		
		If TransactionActive() Then CommitTransaction(); EndIf;
		
	EndDo;
	
EndProcedure
//

//SupplierProductsLoading
&AtServer
Procedure CreateSupplierProducts(ValueTableRowSelected)
	
	FullName					= FullNameStructure.SupplierProducts;
	ValueTable_SupplierProducts	= DataListFromDatabase.Get(FullName);
	
	FilterByAttributes	= New Structure("FullName, Name, Use", FullName, "Owner", True);
	StringArray			= AttributeTable.FindRows(FilterByAttributes);
	
	If StringArray.Count()	= 0 Then
		MessageText	= NStr("en='Failed to get the value of the <Supplier> attribute! Products of suppliers will not be created!';pl='Nie udało się uzyskać wartości atrybutu <Dostawca>! Nomenklatura dostawców nie będe tworzona!';ru='Не удалось получить значение атрибута <Поставщик>! Номенклатура поставщиков не будет создана!';ro='Failed to get the value of the <Supplier> attribute! Products of suppliers will not be created!'");
		Message(MessageText);
		Raise MessageText;
	EndIf;
	
	ProductsRef				= ValueTableRowSelected.Products;
	FilterBySearchField		= New Structure("FullName, SearchFieldInDatabase", FullName, True);
	AttributeStringArray	= AttributeTable.FindRows(FilterBySearchField);
	
	FilterOnValueTable		= New Structure("Products", ProductsRef);
	
	For each ValueTableRowOfAttributes In AttributeStringArray Do
		
		KeyName				= ValueTableRowOfAttributes.KeyName;
		FullAttributeName	= ValueTableRowOfAttributes.FullAttributeName;
		
		If ValueTableRowOfAttributes.FullAttributeName = FullNameStructure.Characteristics AND UseCharacteristics Then
			AttributeValue	= GetCreateCharacteristic(KeyName, ValueTableRowSelected);
		ElsIf ValueTableRowOfAttributes.FullAttributeName	= FullNameStructure.Packings AND UsePackings Then
			AttributeValue	= GetCreatePackage(KeyName, ValueTableRowSelected);
		Else
			AttributeValue	= GetValueForAttribute(KeyName, ValueTableRowSelected);
		EndIf;
		
		If TypeOf(AttributeValue)	= Type("String") Then
			AttributeValue	= Lower(AttributeValue);
		EndIf;
		FilterOnValueTable.Insert(ValueTableRowOfAttributes.Name, AttributeValue);
		
	EndDo;
	
	FoundRowArray	= ValueTable_SupplierProducts.FindRows(FilterOnValueTable);
	
	If FoundRowArray.Count()		= 0 Then
		ManagerSupplierProducts		= GetObjectManagerByFullName(FullName);
		FullNameProductsOfSupplier	= ManagerSupplierProducts.CreateElement();
		FullNameProductsOfSupplier.Fill(Undefined);
		FullNameProductsOfSupplier.SetNewCode();
		FullNameProductsOfSupplier.Products	= ProductsRef;
	Else
		If NOT UpdateAttributeValuesForRelatedObjects Then Return; EndIf;
		
		FullNameProductsOfSupplier		= FoundRowArray[0].Ref.GetObject();
		
		If FullNameProductsOfSupplier	= Undefined Then Return; EndIf;
		
	EndIf;
	
	FullNameProductsOfSupplier.DataExchange.Load	= True;
	
	FilterByAttributes		= New Structure("FullName, Use", FullName, True);
	AttributeStringArray	= AttributeTable.FindRows(FilterByAttributes);
	
	For each ValueTableRowOfAttributes In AttributeStringArray Do
		
		AttributeName		= ValueTableRowOfAttributes.Name;
		KeyName				= ValueTableRowOfAttributes.KeyName;
		FullAttributeName	= ValueTableRowOfAttributes.FullAttributeName;
		
		If ValueTableRowOfAttributes.FullAttributeName = FullNameStructure.Characteristics AND UseCharacteristics Then
			AttributeValue	= GetCreateCharacteristic(KeyName,ValueTableRowSelected);
		ElsIf ValueTableRowOfAttributes.FullAttributeName = FullNameStructure.Packings AND UsePackings Then
			AttributeValue	= GetCreatePackage(KeyName, ValueTableRowSelected);
		Else
			AttributeValue	= GetValueForAttribute(KeyName, ValueTableRowSelected);
		EndIf;
		
		If AttributeValue	= Undefined Then Continue; EndIf;
		
		FullNameProductsOfSupplier[AttributeName]	= AttributeValue;
		
	EndDo;
	
	Try
		FullNameProductsOfSupplier.Write();
		
		NewRow	= ValueTable_SupplierProducts.Add();
		FillPropertyValues(NewRow, FullNameProductsOfSupplier);
	Except
		
		Message(Nstr("en = 'Failed to create supplier products '; pl = 'Nie udało się utworzyć nomenklatury dostawcy ; ru = 'Не удалось создать номенклатуру поставщика '")
				+ "<" + FullNameProductsOfSupplier.Description + ">!");
				
		Message(ErrorDescription());
		
	EndTry;
	
EndProcedure
 
#EndRegion

#Region CreatingProductsFolderHierarchy
 
&AtServer
Procedure FillFoldersTable()
	
	If HierarchyType = 0 Then Return; EndIf;

	Query		= New Query;
	Query.Text	=
		"SELECT
		|	Products.Description,
		|	Products.Ref,
		|	Products.Parent.Description AS Parent1,
		|	Products.Parent.Parent.Description AS Parent2
		|FROM
		|	Catalog.Products AS Products
		|WHERE
		|	NOT Products.DeletionMark
		|	AND Products.IsFolder";
	
	FolderValueTable	= Query.Execute().Unload();
	FolderValueTable.Indexes.Add("Description, Parent1, Parent2");
	
	For each ValueTableRow In FolderValueTable Do
		ValueTableRow.Description	= TrimAll(Lower(ValueTableRow.Description));
		ValueTableRow.Parent1	= TrimAll(Lower(ValueTableRow.Parent1));
		ValueTableRow.Parent2	= TrimAll(Lower(ValueTableRow.Parent2));
	EndDo;
	
EndProcedure

&AtServer
Function GetProductsParent(FullNameAttributeTable,PathFoldersString)
	
	NameArray	= ExpandStringInArrayOfSubstringsServer(PathFoldersString, GroupSeparator);
	ParentRoot	= Catalogs.Products.EmptyRef();
	
	Filter					= New Structure("FullName, Name, Use", FullNameStructure.Products, "Parent", True);
	AttributeStringArray	= FullNameAttributeTable.FindRows(Filter);
	
	If AttributeStringArray.Count()	> 0 Then
		ParentRoot		= AttributeStringArray[0].FillValue;
		DefaultParent	= AttributeStringArray[0].FillValue;
	EndIf;
	
	Parent1	= "";
	Parent2	= "";
	
	For Each FolderName In NameArray Do
		
		FolderName	= TrimAll(FolderName);
		
		IndexOf			= NameArray.Find(FolderName);
		CountOfItems	= NameArray.Count() - 1;
		
		If IndexOf = 0 Then
			Parent1	= TrimAll(Lower(ParentRoot));
		ElsIf IndexOf > 0 Then
			Parent2	= Parent1;
			Parent1	= TrimAll(Lower(NameArray[IndexOf - 1]));
		EndIf;
		
		If NameArray.Count() = 1 Then
			Filter	= New Structure("Description", Lower(FolderName));
		ElsIf NameArray.Count() = 2 Then
			Filter	= New Structure("Description, Parent1", Lower(FolderName), Parent1);
		Else
			Filter	= New Structure("Description, Parent1, Parent2", Lower(FolderName), Parent1, Parent2);
		EndIf;
		
		ArrayOfFoundBaseFolders	= FolderValueTable.FindRows(Filter);
		
		If ArrayOfFoundBaseFolders.Count()	<> 0 Then
			DefaultParent	= ArrayOfFoundBaseFolders[0].Ref;
			ParentRoot		= ArrayOfFoundBaseFolders[0].Ref;
		Else
			NewFolder	= Catalogs.Products.CreateFolder();
			
			NewFolder.Fill(Undefined);
			NewFolder.SetNewCode();
			
			NewFolder.Description	= FolderName;
			NewFolder.Parent		= ParentRoot;
			
			NewFolder.Write();
			
			DefaultParent	= NewFolder.Ref;
			ParentRoot		= NewFolder.Ref;
			
			NewValueTableRow				= FolderValueTable.Add();
			NewValueTableRow.Description	= Lower(FolderName);
			NewValueTableRow.Ref			= NewFolder.Ref;
			NewValueTableRow.Parent1		= Parent1;
			NewValueTableRow.Parent2		= Parent2;
		EndIf;
		
	EndDo;
	
	Return DefaultParent;
	
EndFunction

&AtServer
Function GetHierarchyLine(Value, HierarchyLevel)
	
	Filter	= New Structure("HierarchyLevel", HierarchyLevel);
	ParentFolderStringsArray	= FolderTableFromFile.FindRows(Filter);
	
	If ParentFolderStringsArray.Count()	= 0 Then
		NewFolderValueTableRow					= FolderTableFromFile.Add();
		NewFolderValueTableRow.HierarchyLevel	= HierarchyLevel;
	Else
		NewFolderValueTableRow	= ParentFolderStringsArray[0];
	EndIf;
	
	NewFolderValueTableRow.RowHierarchy	= Value;
	
	HierarchyString	= "";
	
	For each ValueTableRow In FolderTableFromFile Do
		
		If ValueTableRow.HierarchyLevel >= HierarchyLevel Then
			Continue;
		EndIf;
		
		HierarchyString	= HierarchyString + ?(HierarchyString = "", ValueTableRow.RowHierarchy, "//" + ValueTableRow.RowHierarchy);
		
	EndDo;
	
	Return HierarchyString;
	
EndFunction

#EndRegion

#Region ReadingAndLoadingPicturesInDatabase

&AtClient
Procedure ReadingPicturesFromFile()
	
	If NOT UploadPicturesWhenLoadingProducts Then
		
		If WorkMode	= 1 Then
			ContinueDownloadingInAutomaticModeAfterPictures();
		EndIf;
		Return;
		
	EndIf;
	
	Notification = New NotifyDescription("PuttingPicturesEnd", ThisObject);
	
	If NOT NeedToRereadPictures Then
		ExecuteNotifyProcessing(Notification);
		Return;
	EndIf;
	
	IsMSExcel		= False;
	IsLibreoffice	= False;
	Excel			= Undefined;
	ServiceManager	= Undefined;
	FTP				= Undefined;
	
	ResponseStructure	= CheckProgramAvailabilityToReadFile();
	
	If ResponseStructure = Undefined Then Return; EndIf;
	
	If ResponseStructure.Excel <> Undefined Then
		Excel		= ResponseStructure.Excel;
		IsMSExcel	= True;
	EndIf;
	
	If ResponseStructure.ServiceManager <> Undefined Then
		ServiceManager	= ResponseStructure.ServiceManager;
		IsLibreoffice	= True;
	EndIf;
	
	ValueTableAuxiliaryForPictureCache.Clear();
	PictureArray	= New Array;
	
	If TableSelected.Count() <> 0 Then
		TotalString		= TableSelected[TableSelected.Count() - 1].LineNumber;
	Else
		TotalString		= 0;
	EndIf;
		
	ValueTableStringArray	= AttributeTable.FindRows(New Structure("FullName, Name, Use", FullNameStructure.Products, "PictureFile", True));
	
	If ValueTableStringArray.Count() = 0 Then Return; EndIf;
	
	Value			= ValueTableStringArray[0].ColumnName;
	ColumnNameArray	= ExpandStringInArrayOfSubstringsServer(Value, ",");

	If IsMSExcel Then
		
		WorkBook	= Excel.Workbooks.Open(PathToFile, , True);
		ExcelSheet	= WorkBook.Worksheets(SheetNumber);
		WorkBook1	= Excel.WorkBooks.Add();
		
		Status("Status", , Nstr("en='Preliminary loading of products pictures in a cache ....';pl='Wstępne pobieranie zdjęć nomenklatury w pamięć podręczną ....';ru='Предварительная загрузка изображений номенклатуры в кэш....';ro='Preliminary loading of products pictures in a cache ....'")
								+ Format(CurrentDate(), "DF=HH:mm"));
		
		For Each GraphicObject In ExcelSheet.Shapes Do
			
			LineNumberWithPicture	= GraphicObject.TopLeftCell.Row;
			ColumnName				= "K" + String(GraphicObject.TopLeftCell.Column - 1);
			
			For Each PictureColumnNameFromSettings In ColumnNameArray Do
				
				If PictureColumnNameFromSettings <> ColumnName Then Continue; EndIf;
				
				Filter	= New Structure("LineNumber", LineNumberWithPicture);
				StringArraySelected	= TableSelected.FindRows(Filter);
				
				If StringArraySelected.Count() = 0 Then Continue; EndIf;
				
				If GraphicObject.Type = 13 Then
					
					FullNameOfPictureFile = TempFilesDir() + New UUID() + ".jpg";
					
					Try
						GraphicObject.ScaleHeight(1, 1);
						GraphicObject.ScaleWidth(1, 1);
						GraphicObject.Copy();
						
						Chart = Excel.ActiveSheet.ChartObjects().Add(0, 0, GraphicObject.Width, GraphicObject.Height).Chart();
						Chart.Paste();
						Chart.Export(FullNameOfPictureFile);
						
						Chart = Undefined;
						
						PictureDescription		= New TransferableFileDescription;
						PictureDescription.Name	= FullNameOfPictureFile;
						PictureArray.Add(PictureDescription);
						
						ValueTableRowPictures				= ValueTableAuxiliaryForPictureCache.Add();
						ValueTableRowPictures.LineNumber	= LineNumberWithPicture;
						ValueTableRowPictures.ColumnName	= ColumnName;
						ValueTableRowPictures.PicturePath	= FullNameOfPictureFile;
					Except
						
						Message(Nstr("en='Failed to export picture from a string';pl='Nie udało śię wyeksportować obrazu z wiersza';ru='Не удалось экспортировать изображение из строки';ro='Failed to export picture from a string'")
									+ String(LineNumberWithPicture) + Nstr("en=' in ';pl=' w ';ru=' в ';ro=' in '") + FullNameOfPictureFile);
						Message(ErrorDescription());
						Continue;
						
					EndTry;
					
				EndIf;
			EndDo;
		EndDo;
		
		Try
			WorkBook1.Close(false);
		Except
		EndTry;
		
		Try
			WorkBook.Close(false);
		Except
		EndTry;
		
		CompleteWorkMSExcel(Excel);
		
	EndIf;
	
	//Download pictures from Calc
	If IsLibreoffice Then
		
		Desktop	= ServiceManager.CreateInstance("com.sun.star.frame.Desktop");
		
		Arguments		= New COMSafeArray("VT_VARIANT", 2);
		Property1		= ServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue");
		Property1.Name	= "AsFile";
		Property1.Value	= True;
		
		Property2			= ServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue");
		Property2.Name		= "Hidden";
		Property2.Value		= True;
		
		Arguments.SetValue(0, Property1);
		Arguments.SetValue(1, Property2);
		
		Document			= Desktop.LoadComponentFromURL(ConvertToURL(PathToFile), "_blank", 0, Arguments);
		Sheets				= Document.getSheets();
		SheetLibreoffice	= Sheets.getByIndex(SheetNumber - 1);
		
		For each PictureColumnNameFromSettings In ColumnNameArray Do
			
			ColumnNumberWithPicture	= Number(StrReplace(PictureColumnNameFromSettings, "K", ""));
			ColumnName				= PictureColumnNameFromSettings;
			
			If SheetLibreoffice.Drawpage.getCount() <> 0 Then
				
				Status("Status", , Nstr("en='Preliminary loading of products pictures in a cache ....';pl='Wstępne pobieranie zdjęć nomenklatury w pamięć podręczną ....';ru='Предварительная загрузка изображений номенклатуры в кэш....';ro='Preliminary loading of products pictures in a cache ....'")
										+ Format(CurrentDate(), "DF=HH:mm"));
				
				For LineNumberWithPicture = LineNumber To TotalString Do
					
					Cell	= SheetLibreoffice.getCellByPosition(ColumnNumberWithPicture, LineNumberWithPicture - 1);
					Result	= "";
					
					For Number = 0 To SheetLibreoffice.Drawpage.GetCount() - 1 Do
						
						Try
							GraphicObject = SheetLibreoffice.Drawpage.GetByIndex(Number);
							
							If Cell.Position.X <= GraphicObject.Position.X AND (Cell.Position.X + Cell.Size.Width) >= GraphicObject.Position.X
								AND Cell.Position.Y <= GraphicObject.Position.Y AND (Cell.Position.Y + Cell.Size.Height) >= GraphicObject.Position.Y Then
								
								FullNameOfPictureFile	= TempFilesDir() + New UUID() + ".jpg";
								GraphicExportFilter		= ServiceManager.CreateInstance("com.sun.star.drawing.GraphicExportFilter");
								
								GraphicExportFilter.setSourceDocument(GraphicObject);
								
								// Declaration of properties and arguments.
								Properties1			= ServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue");
								Properties1.Name	= "URL";
								Properties1.Value	= "file:///" + StrReplace(FullNameOfPictureFile, "\", "/");
								// GraphicObject.GraphicURL
								
								Properties2			= ServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue");
								Properties2.Name	= "MediaType";
								Properties2.Value	= "image/jpeg";
								// GraphicObject.Graphic
								
								Arguments			= New COMSafeArray("VT_VARIANT", 2);
								Arguments.SetValue(0, Properties1);
								Arguments.SetValue(1, Properties2);
								
								GraphicExportFilter.Filter(Arguments);
								
								GraphicExportFilter	= Undefined;
								
								PictureFile	= New File(FullNameOfPictureFile);
								
								If PictureFile.Exist() Then
									
									PictureDescription		= New TransferableFileDescription;
									PictureDescription.Name	= FullNameOfPictureFile;
									PictureArray.Add(PictureDescription);
									
									ValueTableRowPictures				= ValueTableAuxiliaryForPictureCache.Add();
									ValueTableRowPictures.LineNumber	= LineNumberWithPicture;
									ValueTableRowPictures.ColumnName	= ColumnName;
									ValueTableRowPictures.PicturePath	= FullNameOfPictureFile;
									Break;
									
								Else
									Message(Nstr("en='Failed to export picture from a string';pl='Nie udało śię wyeksportować obrazu z wiersza';ru='Не удалось экспортировать изображение из строки';ro='Failed to export picture from a string'")
												+ LineNumberWithPicture + Nstr("en=' columns ';pl=' kolumny ';ru=' колонки ';ro=' columns '") + (ColumnNumberWithPicture + 1)
												+ Nstr("en=' in ';pl=' w ';ru=' в ';ro=' in '") + FullNameOfPictureFile);
								EndIf;
								
							EndIf;
							
						Except
							Message(ErrorDescription());
							Message(Nstr("en='Failed to export picture from a string';pl='Nie udało śię wyeksportować obrazu z wiersza';ro='Failed to export picture from a string'")
										+ LineNumberWithPicture + Nstr("en=' columns';pl=' kolumny';ru=' колонки ';ro=' columns'") + (ColumnNumberWithPicture + 1)
										+ Nstr("en=' in ';pl=' w ';ru=' в ';ro=' in '") + FullNameOfPictureFile);
						EndTry;
						
					EndDo;
					
				EndDo;
				
			EndIf;
			
		EndDo;
		
		CompleteWorkLibreoffice(Desktop,ServiceManager);
		
	EndIf;
	
	//Uploading pictures if the address of the picture or ref to the picture is found
	For Each ValueTableRowSelected In TableSelected Do
		
		For Each PictureColumnNameFromSettings In ColumnNameArray Do
			
			PictureAddress				= TrimAll(ValueTableRowSelected[PictureColumnNameFromSettings]);
			LineNumberWithPicture		= TableSelected.IndexOf(ValueTableRowSelected);
			LineNumberWithPictureInFile	= ValueTableRowSelected.LineNumber;
			
			If IsBlankString(PictureAddress) Then Continue; EndIf;
			
			If Find(Lower(PictureAddress),"http://") > 0 OR Find(Lower(PictureAddress), "https://") > 0 OR Find(Lower(PictureAddress), "ftp://") > 0 Then //4.26
				
				FullNameOfPictureFile	= GetPathToLoadedPicture(PictureAddress, Find(Lower(PictureAddress), "https://") > 0);
				
				If FullNameOfPictureFile = Undefined Then
					
					Message(Nstr("en='In line No ';pl='W wierszu Nr ';ru='В строке № ';ro='In line No '")
							+ String(LineNumberWithPicture + 1)
							+ Nstr("en=' - picture ';pl=' - obrazka ';ru=' - изображение ';ro=' - picture '")
							+ FullNameOfPictureFile
							+ Nstr("en=' not found';pl=' nie znaleziono';ru=' не найдено';ro=' not found'"));
							
					ValueTableRowSelected[PictureColumnNameFromSettings] = ""; //5.08
					
					Continue;
					
				EndIf;
				
			Else
				
				FullNameOfPictureFile	= PictureAddress;
				PictureFile				= New File(FullNameOfPictureFile);
				
				If NOT PictureFile.Exist() Then
					
					Message(Nstr("en='In line No ';pl='W wierszu Nr ';ru='В строке № ';ro='In line No '")
							+ String(LineNumberWithPicture + 1)
							+ Nstr("en=' - picture ';pl=' - obrazka ';ru=' - изображение ';ro=' - picture '")
							+ FullNameOfPictureFile
							+ Nstr("en=' not found';pl=' nie znaleziono';ru=' не найдено';ro=' not found'"));
							
					ValueTableRowSelected[PictureColumnNameFromSettings] = ""; //5.08
					
					Continue;
					
				EndIf;
				
			EndIf;
			
			PictureDescription		= New TransferableFileDescription;
			PictureDescription.Name	= FullNameOfPictureFile;
			PictureArray.Add(PictureDescription);
			
			ValueTableRowPictures				= ValueTableAuxiliaryForPictureCache.Add();
			ValueTableRowPictures.LineNumber	= LineNumberWithPictureInFile;
			ValueTableRowPictures.ColumnName	= PictureColumnNameFromSettings;
			ValueTableRowPictures.PicturePath	= FullNameOfPictureFile;
			
		EndDo;
		
	EndDo;
	
	Notification = New NotifyDescription("PuttingPicturesEnd", ThisObject);
	
	//Put the pictures in the temporary storage
	If PictureArray.Count() > 0 Then
		BeginPuttingFiles(Notification, PictureArray, , False, ThisForm.UUID);
	EndIf;
	
	FTP	= Undefined;
	
EndProcedure

&AtClient
Procedure PuttingPicturesEnd (PictureArrayPut, Parameters) Export
	
	ProcessingPicturesAtServer(PictureArrayPut);
	
	If WorkMode	= 1 Then
		ContinueDownloadingInAutomaticModeAfterPictures();
	EndIf;
	
EndProcedure

&AtServer
Procedure ProcessingPicturesAtServer(PictureArrayPut)
	
	If PictureArrayPut <> Undefined Then
		
		For Each PictureDescription In PictureArrayPut Do
			
			TempStorageAddress	= PictureDescription.Location;
			
			Filter		= New Structure("PicturePath", PictureDescription.FullName);
			StringArray	= ValueTableAuxiliaryForPictureCache.FindRows(Filter);
			
			For each ValueTableRowAuxiliary In StringArray Do
				
				ColumnName				= ValueTableRowAuxiliary.ColumnName;
				LineNumberInFile		= ValueTableRowAuxiliary.LineNumber;
				ValueTableRowNumber		= 0;
				Filter					= New Structure("LineNumber",LineNumberInFile);
				ValueTableStringArray	= TableSelected.FindRows(Filter);
				
				If ValueTableStringArray.Count() > 0 Then
					ValueTableRowNumber = TableSelected.IndexOf(ValueTableStringArray[0]);
				EndIf;
				
				If NOT ValueIsFilled(TempStorageAddress) OR ValueTableStringArray.Count() = 0 Then
					Message(Nstr("en='In Excel file in row ';pl='W pliku Excel w wierszu ';ru='В файле Excel в строке ';ro='In Excel file in row '") + String(ValueTableRowNumber + 1)
								+ Nstr("en=', the image is most likely larger in the size of the cell in which it is located!';pl=', obrazek jest prawdopodobnie większy niż rozmiar komórki, w której się znajduje!';ru=', изображение, скорее всего, имеет больший размер, нежели сам размер ячейки, в которой он находится';ro=', the image is most likely larger in the size of the cell in which it is located!'"));
					Message(Nstr("en='Change the size of the picture in the file in this row so that it fits in the cell!';pl='Zmień rozmiar obrazka w pliku w tym wierszu, aby pasował do komórki!';ru='Измените размер изображения в этой строке, что бы соответствовал размеру ячейки!';ro='Change the size of the picture in the file in this row so that it fits in the cell!'"));
					Break;
				EndIf;
				
				TableSelected[ValueTableRowNumber][ColumnName]	= TempStorageAddress;
				ValueTableRowAuxiliary.TempStorageAddress		= TempStorageAddress;
				
			EndDo;
			
		EndDo;
		
	Else
		For Each ValueTableRowAuxiliary In ValueTableAuxiliaryForPictureCache Do
			
			TempStorageAddress		= ValueTableRowAuxiliary.TempStorageAddress;
			ColumnName				= ValueTableRowAuxiliary.ColumnName;
			LineNumberInFile		= ValueTableRowAuxiliary.LineNumber;
			ValueTableRowNumber		= 0;
			Filter					= New Structure("LineNumber", LineNumberInFile);
			ValueTableStringArray	= TableSelected.FindRows(Filter);
			
			If ValueTableStringArray.Count() > 0 Then
				ValueTableRowNumber	= TableSelected.IndexOf(ValueTableStringArray[0]);
			EndIf;
			
			If NOT ValueIsFilled(TempStorageAddress) OR ValueTableStringArray.Count() = 0 Then
					Message(Nstr("en='In Excel file in row ';pl='W pliku Excel w wierszu ';ru='В файле Excel в строке ';ro='In Excel file in row '") + String(ValueTableRowNumber + 1)
								+ Nstr("en=', the image is most likely larger in the size of the cell in which it is located!';pl=', obrazek jest prawdopodobnie większy niż rozmiar komórki, w której się znajduje!';ru=', изображение, скорее всего, имеет больший размер, нежели сам размер ячейки, в которой он находится';ro=', the image is most likely larger in the size of the cell in which it is located!'"));
					Message(Nstr("en='Change the size of the picture in the file in this row so that it fits in the cell!';pl='Zmień rozmiar obrazka w pliku w tym wierszu, aby pasował do komórki!';ru='Измените размер изображения в этой строке, что бы соответствовал размеру ячейки!';ro='Change the size of the picture in the file in this row so that it fits in the cell!'"));
				Continue;
			EndIf;
			
			TableSelected[ValueTableRowNumber][ColumnName]	= TempStorageAddress;
			
		EndDo;
		
	EndIf;
	
	NeedToRereadPictures	= False;
	
EndProcedure

&AtClient
Function GetPathToLoadedPicture(PictureAddress, Secure = False)
	
	Try
		BinaryDataOfPicture	= GetAddressContent(PictureAddress);
	Except
		Message(Nstr("en='Invalid picture address! ';pl='Nieprawidłowy adres zdjęcia! ';ru='Некорректный адрес изображения ';ro='Invalid picture address! '") + PictureAddress);
		Message(ErrorDescription());
		Return Undefined;
	EndTry;
	
	//4.26
	If TypeOf(BinaryDataOfPicture) = Type("BinaryData") Then
		
		Try
			Picture	= New Picture(BinaryDataOfPicture);
			FormatOfLoadedPicture	= Picture.Format();
			
			If FormatOfLoadedPicture = PictureFormat.UnknownFormat Then
				FormatOfLoadedPicture	= PictureFormat.JPEG;
			EndIf;
			
			TemporaryPath	= TempFilesDir() + New UUID() + "." + FormatOfLoadedPicture;
			Picture.Write(TemporaryPath);
			
			Return TemporaryPath;
			
		Except
			Message(ErrorDescription());
			Return Undefined;
		EndTry;
	Else
		Return Undefined; //5.08
	EndIf;
	//
EndFunction

&AtClient
Function GetAddressContent(Val ServerHTTP, Val PageAddress = "", HeadersHTTP = Undefined,
						   Val GetAsBinaryData = False, Val SecureConnection = False) Export
	
	Var ResponseFileNameCoded, ResponseFileName, Port;
	
	If Not ValueIsFilled(ServerHTTP) Then Return Undefined; EndIf;
	
	If TypeOf(HeadersHTTP) <> Type("Map") Then HeadersHTTP = New Map; EndIf;
	
	If Find(Lower(ServerHTTP), "https://") = 1 Then SecureConnection = True; EndIf;
	
	//4.26
	If Find(Lower(ServerHTTP),"https://") > 0 Then
		Protocol	= "https://";
	ElsIf Find(Lower(ServerHTTP),"http://") > 0 Then
		Protocol	= "http://";
	ElsIf Find(Lower(ServerHTTP),"ftp://") > 0 Then
		Protocol	= "ftp://";
	EndIf;
	//	

	If Left(Lower(ServerHTTP), StrLen(Protocol)) = Protocol Then
		ServerHTTP = Mid(ServerHTTP, StrLen(Protocol) + 1);
	EndIf;
	
	If NOT ValueIsFilled(PageAddress) Then
		Position = Find(ServerHTTP, "/");
		
		If Position > 0 Then
			PageAddress		= Mid(ServerHTTP, Position, StrLen(ServerHTTP));
			ServerReceiver	= Left(ServerHTTP, Position - 1);
		Else
			PageAddress = "/";
		EndIf;
		
	EndIf;
	
	ServerReceiver = StrReplace(ServerReceiver, "/", "");
	
	//Select the port, login, password from
	//the domain name 4.26
	Login		= "";
	Password	= "";
	
	If Find(Protocol, "ftp://") > 0 Then
		
		AtSymbolPosition	= Find(ServerReceiver, "@");
		
		If AtSymbolPosition > 0 Then
			
			LoginPassword	= Mid(ServerReceiver,1,AtSymbolPosition-1);
			ServerReceiver	= Mid(ServerReceiver,AtSymbolPosition+1);
			
			ColonPosition	= Find(LoginPassword, ":");
			
			If ColonPosition > 0 Then
				Password	= Mid(LoginPassword, ColonPosition + 1);
				Login		= Left(LoginPassword, ColonPosition - 1);
			EndIf;
			
		EndIf;
		
		ColonPosition = Find(ServerReceiver, ":");
		
		If ColonPosition > 0 Then
			Port			= Mid(ServerReceiver, ColonPosition + 1);
			ServerReceiver	= Left(ServerReceiver, ColonPosition - 1);
		EndIf;
		
		If FTP	= Undefined OR ServerReceiver <> FTP.Server Then
			FTP	= New FTPConnection(ServerReceiver, Port, Login, Password, , True, 60);
		EndIf;
		
		TemporaryFile	= GetTempFileName("jpg");
		FTP.Get(PageAddress, TemporaryFile);
		
		Return TemporaryFile;
		
	EndIf;
	//
	
	ColonPosition = Find(ServerReceiver, ":");
	If ColonPosition > 0 Then
		Port			= Mid(ServerReceiver, ColonPosition + 1);
		ServerReceiver	= Left(ServerReceiver, ColonPosition - 1);
	EndIf;
	
	HTTP	= New HTTPConnection(ServerReceiver, Port, , , , ,?(SecureConnection, New OpenSSLSecureConnection(), Undefined));
	
	ResponseHTTP = HTTP.Get(New HTTPRequest(PageAddress, HeadersHTTP)); //
	QueryError = (ResponseHTTP.StatusCode >= 400);
	If ResponseHTTP.StatusCode = 301 or ResponseHTTP.StatusCode = 302 Then
		
		If ResponseHTTP.Headers.Count() > 0 Then
			
			PageAddress = ResponseHTTP.Headers["Location"]; //
			
			If ValueIsFilled(PageAddress) Then
				
				If Find(Lower(PageAddress), "http://") = 0 AND Find(Lower(PageAddress), "https://") = 0 Then
					
					PageAddress = ?(Left(PageAddress, 1) = "/", Mid(PageAddress, 2), PageAddress);
					
					If Find(PageAddress, ServerReceiver + "/") = 0  Then
						PageAddress = Protocol + ServerReceiver + ?(ValueIsFilled(Port), ":" + Port, "") + "/" + PageAddress;
					EndIf;
					
				EndIf;
				
				Cookies = ResponseHTTP.Headers["Set-Cookie"];//
				
				If ValueIsFilled(Cookies) Then HeadersHTTP.Insert("Cookie", Cookies); EndIf;
				
				Return GetAddressContent(PageAddress,,HeadersHTTP, GetAsBinaryData, SecureConnection);//
				
			EndIf;
		EndIf;
		
	ElsIf ResponseHTTP.StatusCode >= 100 AND ResponseHTTP.StatusCode <= 200 Then
		
		If ResponseHTTP.Headers.Count() > 0 Then
			
			ContentType = ResponseHTTP.Headers["Content-Type"];
			
			If Find(ContentType, "text/") = 1 OR Find(ContentType, "/javascript")
				OR Find(ContentType, "+xml") OR Find(ContentType, "/xml") <> 0 OR Find(ContentType, "/json") <> 0 Then
				
				GetAsBinaryData = False;
				
			ElsIf Find(ContentType, "image/") = 1 OR Find(ContentType, "video/") = 1 
				OR Find(ContentType, "application/") = 1 OR Find(ContentType, "audio/") = 1 Then
				
				GetAsBinaryData = True;
				
			EndIf;
			
			If ResponseHTTP.Headers["Content-Disposition"] <> Undefined Then
				ContentType = ResponseHTTP.Headers["Content-Disposition"];
			EndIf;
			
			ContentType = StrReplace(StrReplace(ContentType, """", ""), "'", "");
			
			FileNamePosition = Find(ContentType, "filename*=UTF-8");
			
			If FileNamePosition <> 0 Then
				
				ResponseFileNameCoded = Mid(ContentType, FileNamePosition + StrLen("filename*=UTF-8"));
				ColonPosition = Find(ResponseFileNameCoded, ";");
				
				If ColonPosition <> 0 Then
					ResponseFileNameCoded = Left(ResponseFileNameCoded, ColonPosition - 1);
				EndIf;
				
			EndIf;
			
			FileNamePosition = Find(ContentType, "name=");
			
			If FileNamePosition <> 0 Then
				
				ResponseFileName = Mid(ContentType, FileNamePosition + StrLen("name="));
				ColonPosition = Find(ResponseFileName, ";");
				
				If ColonPosition <> 0 Then
					ResponseFileName = Left(ResponseFileName, ColonPosition - 1);
				EndIf;
				
			EndIf;
			
		EndIf;
		
	EndIf;
	
	HeadersHTTP	= ResponseHTTP.Headers;
	HeadersHTTP.Insert("StatusCode", ResponseHTTP.StatusCode);
	
	If ValueIsFilled(ResponseFileName) Then HeadersHTTP.Insert("FileName", ResponseFileName); EndIf;
	
	If ValueIsFilled(ResponseFileNameCoded) Then
		HeadersHTTP.Insert("EncodeFileName", ResponseFileNameCoded);
	EndIf;
	
	If QueryError OR NOT GetAsBinaryData Then Return ResponseHTTP.GetBodyAsString(); EndIf;
	
	Return ResponseHTTP.GetBodyAsBinaryData();
	
EndFunction
 
#EndRegion

#Region LoadingAndBarcodeGeneration

&AtServer
Procedure RecordBarcodeIntoDatabase(ValueTableRowSelected)
	
	ProductsRef	= ValueTableRowSelected.Products;
	
	If ProductsRef.IsEmpty() Then Return; EndIf;
	
	BarcodeManager	= GetObjectManagerByFullName(FullNameStructure.Barcodes);
	Write			= BarcodeManager.CreateRecordManager();
	Write.Products	= ProductsRef;
	
	FilterByAttributes		= New Structure("FullName, Use", FullNameStructure.Barcodes, True);
	AttributeStringArray	= AttributeTable.FindRows(FilterByAttributes);
	
	AttributeStructure		= New Structure;//5.02
	AttributeStructure.Insert("Products", ProductsRef);
	
	For Each ValueTableRowOfAttributes In AttributeStringArray Do
		
		AttributeName		= ValueTableRowOfAttributes.Name;
		KeyName				= ValueTableRowOfAttributes.KeyName;
		FullAttributeName	= ValueTableRowOfAttributes.FullAttributeName;
		
		If ValueTableRowOfAttributes.FullAttributeName = FullNameStructure.Characteristics AND UseCharacteristics Then
			AttributeValue	= GetCreateCharacteristic(KeyName, ValueTableRowSelected);
		ElsIf ValueTableRowOfAttributes.FullAttributeName = FullNameStructure.Packings AND UsePackings Then
			AttributeValue	= GetCreatePackage(KeyName, ValueTableRowSelected);
		Else
			AttributeValue	= GetValueForAttribute(KeyName, ValueTableRowSelected);
		EndIf;
		
		If AttributeValue	= Undefined Then Continue; EndIf;
		
		Write[AttributeName]	= AttributeValue;
		AttributeStructure.Insert(AttributeName, AttributeValue); //5.02
		
	EndDo;
	
	If NOT IsBlankString(Write.Barcode) Then
		Set	= InformationRegisters.Barcodes.CreateRecordSet();
		Set.DataExchange.Load	= True;
		Set.Filter.Barcode.Set(Write.Barcode);
		Set.Write();
	EndIf;
	
	If IsBlankString(Write.Barcode) AND GenerateBarcodeForProductsIfNotSpecified Then
		//5.02
		Barcode	= CheckPresenceOfBarcodeInDatabase(AttributeStructure);
		
		If Barcode = Undefined Then
			Write.Barcode	= GenerateBarcodeEAN13();
		Else
			Write.Barcode	= Barcode;
		EndIf;
		//
		
		Filter	= New Structure("FullName,Name", FullNameStructure.Barcodes, "Barcode");
		AttributeStringArray	= AttributeTable.FindRows(Filter);
		
		If AttributeStringArray.Count() > 0 Then
			ColumnNameOfBarcode	= AttributeStringArray[0].ColumnName;
			ValueTableRowSelected[ColumnNameOfBarcode]	= Write.Barcode;
		EndIf;
		
	ElsIf IsBlankString(Write.Barcode) AND NOT GenerateBarcodeForProductsIfNotSpecified Then
		Message(Nstr("en='For products ';pl='Dla nomenklatury ';ru='Для номенклатуры ';ro='For products '") + "<" + ProductsRef + ">"
					+ Nstr("en=' no barcode specified!';pl=' nie określono kodu kreskowego!';ru=' не указан штрихкод';ro=' no barcode specified!'"));
		Return;
	EndIf;
	
	Try
		Write.Write();
	Except
		Message(ErrorDescription());
	EndTry;
EndProcedure

//GenerateProductsBarcode
Function GenerateBarcodeEAN13(PieceGoodPrefix = "0", InternalBarcodePrefix = "00", MaximumCode = 99999999) Export

	Code = Min(GetMaximumBarcodeCodeValueByNumber(PieceGoodPrefix, InternalBarcodePrefix) + 1, MaximumCode);

	Return GetBarcodeByCode(Code, PieceGoodPrefix, InternalBarcodePrefix);

EndFunction

Function GetBarcodeByCode(Code, PieceGoodPrefix = "0", InternalBarcodePrefix = "00") Export

	Barcode	= "2" + PieceGoodPrefix + InternalBarcodePrefix + Format(Code, "ND=8; NLZ=; NG=");
	Barcode	= Barcode + ControlSymbolEAN(Barcode, 13);
	
	Return Barcode;

EndFunction // GetBarcodeByCode()

Function ControlSymbolEAN(Barcode, Type) Export
	
	Even	= 0;
	Odd		= 0;
	
	IterationCount	= ?(Type = 13, 6, 4);
	
	For IndexOf = 1 To IterationCount Do
		
		If (Type = 8) and (IndexOf = IterationCount) Then
		Else
			Even   = Even + Mid(Barcode, 2 * IndexOf, 1);
		EndIf;
		
		Odd = Odd + Mid(Barcode, 2 * IndexOf - 1, 1);
		
	EndDo;
	
	If Type = 13 Then
		Even = Even * 3;
	Else
		Odd	= Odd * 3;
	EndIf;
	
	CheckDigit	= 10 - (Even + Odd) % 10;
	
	Return ?(CheckDigit = 10, "0", String(CheckDigit));
	
EndFunction // ControlSymbolEAN()

Function GetMaximumBarcodeCodeValueByNumber(PieceGoodPrefix = "0", InternalBarcodePrefix = "00") Export
	
	Query = New Query(
		"SELECT
		|	MAX(SUBSTRING(Barcodes.Barcode, 5, 8)) AS Code
		|FROM
		|	InformationRegister.Barcodes AS Barcodes
		|WHERE
		|	Barcodes.Barcode LIKE &TemplateBarcode"
	);
	
	TemplateBarcode	= "2" + PieceGoodPrefix + InternalBarcodePrefix + "_________";
	Query.SetParameter("TemplateBarcode", TemplateBarcode);
	
	Selection	= Query.Execute().Select();
	Selection.Next();
	
	NumberTypeDescription	= New TypeDescription("Number");
	CodeValueByNumber 		= NumberTypeDescription.AdjustValue(Selection.Code);
	
	Return CodeValueByNumber;
	
EndFunction

//5.02
&AtServer
Function CheckPresenceOfBarcodeInDatabase(AttributeStructure)
	
	FilterText	= "";
	
	For Each StructureItem In AttributeStructure Do
		
		AttributeName	= StructureItem.Key; //KeyName
		
		If AttributeName = "Barcode" Then Continue; EndIf;
		
		FilterText	= FilterText + ?(FilterText="", " WHERE Barcodes."
								 + AttributeName + "=&" + AttributeName," AND Barcodes."
								 + AttributeName + "=&" + AttributeName);
	EndDo;
	
	Query		= New Query;
	Query.Text	=
	"SELECT
	|	Barcodes.Barcode
	|FROM
	|	" + FullNameStructure.Barcodes + " AS Barcodes " + FilterText;
	
	For Each StructureItem In AttributeStructure Do
		Query.SetParameter(StructureItem.Key, StructureItem.Value); //KeyName
	EndDo;
	
	Selection = Query.Execute().Select();
	
	If Selection.Next() Then
		Return Selection.Barcode;
	Else
		Return Undefined;
	EndIf;
	
EndFunction
//
#EndRegion

#Region HandlersOfFormItems

&AtClient
Procedure WorkModeOnChange(Item)
	CustomizeVisibilityWithAutomaticDownload();
EndProcedure

//Drag Processing
&AtClient
Procedure AttributeTableDrag(Item, DragParameters, StandardProcessing, String, Field)
	
	StandardProcessing	= False;
	AttributeArray		= New Array;
	AttributeArray.Add("Price");
	
	Item.CurrentRow	= String;
	CurrentData		= Item.CurrentData;
	
	If CurrentData	= Undefined Then Return; EndIf;
	
	If AttributeArray.Find(CurrentData.Name) = Undefined
		AND CurrentData.FullAttributeName <> FullNameStructure.Pictures
		AND CurrentData.FullAttributeName <> FullNameStructure.Prices Then
		
		CurrentData.ColumnName	= SelectedColumn;
		
	Else
		
		If Find(CurrentData.ColumnName,SelectedColumn) = 0
			AND (CurrentData.FullAttributeName = FullNameStructure.Pictures
			OR FullNameOfCurrentAction = FullNameStructure.Prices + "__") Then
			
				CurrentData.ColumnName	= CurrentData.ColumnName + ?(IsBlankString(CurrentData.ColumnName), "", ",") + SelectedColumn;
				
			Else
				
			CurrentData.ColumnName	= SelectedColumn;
			
		EndIf;
		
	EndIf;
	
	CurrentData.Use				= True;
	CurrentData.FillingVariant	= "From column";
	CurrentIndex				= 999999;
	
	AttributeTableOnActivateRow("");
	
	If NOT CurrentData.IsSimpleType Then
		
		FillTableWithAttributesOfCreatedObject(CurrentData.GetID());
		
		If CurrentData.FullAttributeName = FullNameStructure.Pictures Then
			NeedToRereadPictures	= True;
			Return;
		EndIf;
		
		SearchStringArray	= SearchFieldTable.FindRows(New Structure("FullName", CurrentData.FullName));
				
		If SearchStringArray.Count() > 0 Then
			
			For Each ValueTableRowOfSearchField In SearchStringArray Do
				
				FilterByAttributes	= New Structure("KeyName, Name", CurrentData.KeyName, ValueTableRowOfSearchField.Name);
				StringArray			= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
				
				If StringArray.Count() > 0 AND StringArray[0].Use
					AND StringArray[0].IsSimpleType Then
					
					StringArray[0].ColumnName	= CurrentData.ColumnName;
					
				EndIf;
				
			EndDo;
			
		Else
			
			FilterByAttributes	= New Structure("KeyName, Name", CurrentData.KeyName, "Description");
			StringArray			= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
			
			If StringArray.Count() > 0 AND StringArray[0].Use
				AND StringArray[0].IsSimpleType Then
				
				StringArray[0].ColumnName	= CurrentData.ColumnName;
				
			EndIf;
			
		EndIf;
		
	EndIf;
	
	IsMap	= (CurrentData.FillingVariant = "Map");
	
	If IsMap Then
		AttachIdleHandler("Attached_FillMapTable", 0.1, True);
	EndIf;
	
EndProcedure

&AtClient
Procedure AttributeTableCreatedDrag(Item, DragParameters, StandardProcessing, String, Field)
	
	StandardProcessing	= False;
	Item.CurrentRow		= String;
	CurrentData			= Item.CurrentData;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	CurrentData.ColumnName	= SelectedColumn;
	
	If CurrentData.IsSimpleType Then
		CurrentData.FillingVariant	= "From column";
	Else
		CurrentData.FillingVariant	= "Map";
	EndIf;
	
	CurrentData.Use	= True;
	
	AttributeTableCreatedOnActivateRow("");
	
	IsMap	= (CurrentData.FillingVariant = "Map");
	
	If IsMap Then
		AttachIdleHandler("Attached_FillMapTable", 0.1, True);
	EndIf;
	
EndProcedure

&AtClient
Procedure TableOfLoadedDataDragStart(Item, DragParameters, Executing)
	
	If Item.CurrentItem <> Undefined Then
		SelectedColumn	= StrReplace(Item.CurrentItem.Name, "TableOfLoadedData", "");
	EndIf;
	
EndProcedure
//

//AddingAttributes
&AtClient
Procedure AttributeTableBeforeAddRow(Item, Cancel, Copy, Parent, Group, Parameter)
	
	If NOT ValueIsFilled(ActionKind) Then
		Message(Nstr("en='Not specified what you need to download!';pl='Nie określono, co musisz pobrać!';ru='Не указано что необходимо загрузить!';ro='Not specified what you need to download!'"));
		Return;
	EndIf;
	
	If Find(FullNameOfCurrentAction, FullNameStructure.Products) = 0 Then
		ShowMessageBox(, Nstr("en='In this section, attributes are added automatically!';pl='W tej sekcji atrybuty są dodawane automatycznie!';ru='В этой секции атрибуты добавлены автоматически!';ro='In this section, attributes are added automatically!'"));
		Return;
	EndIf;
	
	Cancel	= True;
	
	Notification	= New NotifyDescription("AddingAttributesEnd", ThisObject);
	FullName		= StrReplace(FullNameOfCurrentAction, "__", "");
	OpenForm(GetNameDataProcessor() + ".Form.AttributeChoiceForm", New Structure("FullName", FullName), , , , , Notification);
	
EndProcedure

&AtClient
Procedure AddingAttributesEnd (AttributeArray, Parameters) Export
	
	If AttributeArray = Undefined Then Return; EndIf;
	AddSelectedAttributesServer(AttributeArray);
	
EndProcedure

&AtServer
Procedure AddSelectedAttributesServer(AttributeArray)
	
	FullName=StrReplace(FullNameOfCurrentAction, "__", "");
	MetadataObject=Metadata.FindByFullName(FullName);
	
	For each FullAttributeName In AttributeArray Do
		
		AttributeName	= StrReplace(FullAttributeName, FullName + ".", "");
		KeyName			= FullAttributeName + "." + AttributeName;
		
		Filter			= New Structure("KeyName", KeyName);
		StringArray		= AttributeTable.FindRows(Filter);
		
		If StringArray.Count() = 0 Then
			AddAttributeToAttributesTable(MetadataObject, AttributeName);
		EndIf;
		
	EndDo;
	
	AttributeTable.Sort("RequiredForFilling DESC");
	
EndProcedure
//

&AtClient
Procedure DoNotUseAttribute(Command)
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		
		CurrentData			= Items.AttributeTable.CurrentData;
		FilterByAttributes	= New Structure("KeyName", CurrentData.KeyName);
		StringArray			= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
		LineCount			= StringArray.Count();
		
		For N = 1 To LineCount Do
			RowIndex	= LineCount - N;
			AttributeTableOfCreatedObjects.Delete(StringArray[RowIndex]);
		EndDo;
		
		If CurrentPageNameForMap = "PageObjectAttributes" Then
			CurrentData	= Items.AttributeTable.CurrentData;
		ElsIf  CurrentPageNameForMap = "PageObjectAttributesCreated" Then
			CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
		EndIf;
		
		FullName			= CurrentData.FullName + "." + CurrentData.Name;
		FilterByAttributes	= New Structure("FullName", FullName);
		StringArray			= MapTable.FindRows(FilterByAttributes);
		LineCount			= StringArray.Count();
		
		For N = 1 To LineCount Do
			RowIndex	= LineCount - N;
			MapTable.Delete(StringArray[RowIndex]);
		EndDo;
		
		Items.nAttributeObject.Title	= "";
		
	Else
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
	EndIf;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributesCreated
		AND CurrentData.ReadOnly Then
		
		CurrentData.ColumnName				= "";
		CurrentData.FillingVariant			= "From column";
		CurrentData.FillValue				= "";
		
	Else
		CurrentData.Use						= False;
		CurrentData.ColumnName				= "";
		CurrentData.FillingVariant			= "";
		CurrentData.FillValue				= "";
		CurrentData.SearchFieldInDatabase	= False;
	EndIf;
	
	ResetCommandChecksChoiceOptions();
	
EndProcedure

//Filling the fill options
&AtClient
Procedure FillOptionListForFillingAttributesTable()
	
	AreAttributesOfCreatedObjects=False;
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		CurrentData			= Items.AttributeTable.CurrentData;
		ItemFromColumn		= Items.FromColumn;
		ItemSelectedValue	= Items.SelectedValue;
		ItemMap				= Items.Map;
	Else
		CurrentData			= Items.AttributeTableOfCreatedObjects.CurrentData;
		ItemFromColumn		= Items.FromColumn_AttributesCreated;
		ItemSelectedValue	= Items.SelectedValue_AttributesCreated;
		ItemMap				= Items.Map_AttributesCreated;
		AreAttributesOfCreatedObjects	= True;
	EndIf;

	If CurrentData = Undefined Then Return; EndIf;
	
	ReadOnly	= False;
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributesCreated Then
		ReadOnly	= CurrentData.ReadOnly;
	EndIf;
	
	ResetCommandChecksChoiceOptions();
	
	ItemFromColumn.Enabled		= False;
	ItemSelectedValue.Enabled	= False;
	ItemMap.Enabled				= False;
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes OR CurrentData.IsSimpleType Then
		ItemFromColumn.Enabled	= True;
	EndIf;
	
	If CurrentData.Name <> "PictureFile" AND CurrentData.Name <> "MainImage" Then
		ItemSelectedValue.Enabled	= True;
		ItemMap.Enabled				= True;
	EndIf;
	
	If NOT AreAttributesOfCreatedObjects AND (CurrentData.FullAttributeName = FullNameStructure.Characteristics
		OR CurrentData.FullAttributeName 	= FullNameStructure.Series
		OR (CurrentData.FullAttributeName 	= FullNameStructure.Packings AND CurrentData.Name = "Ref")
		OR CurrentData.FullAttributeName	= FullNameStructure.CustomsDeclaration
		OR CurrentData.FullAttributeName	= FullNameStructure.PropertyValues) Then
		
		ItemMap.Enabled	= False;
		
	EndIf;
		
	If CurrentData.FillingVariant = "From column" Then
		ItemFromColumn.Check	= True;
	ElsIf CurrentData.FillingVariant = "Selected value" Then
		ItemSelectedValue.Check	= True;
	ElsIf CurrentData.FillingVariant = "Map" Then
		ItemMap.Check			= True;
	EndIf;
	
EndProcedure
//

&AtClient
Procedure FromColumn(Command)
	
	ResetCommandChecksChoiceOptions();
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		CurrentData				= Items.AttributeTable.CurrentData;
		Items.FromColumn.Check	= True;
	Else
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
		Items.FromColumn_AttributesCreated.Check	= True;
	EndIf;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	CurrentData.FillingVariant	= "From column";
	
	OnChangeFillingVariant();
	
EndProcedure

&AtClient
Procedure SelectedValue(Command)
	
	ResetCommandChecksChoiceOptions();
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		CurrentData	= Items.AttributeTable.CurrentData;
		Items.SelectedValue.Check	= True;
	Else
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
		Items.SelectedValue_AttributesCreated.Check	= True;
	EndIf;

	If CurrentData = Undefined Then Return; EndIf;

	CurrentData.FillingVariant	= "Selected value";
	CurrentData.ColumnName		= "";
	
	OnChangeFillingVariant();
	
EndProcedure

&AtClient
Procedure Map(Command)
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		CurrentPageNameForMap	= "PageObjectAttributes";
		ItemMap					= Items.Map;
	Else
		CurrentPageNameForMap	= "PageObjectAttributesCreated";
		ItemMap					= Items.Map_AttributesCreated;
	EndIf;
	
	If ItemMap.Check Then
		Items.MapPagesAndObjectAttributes.CurrentPage	= Items.MapPage;
		Attached_OnCurrentPageChange();
		Return;
	EndIf;

	ResetCommandChecksChoiceOptions();
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		CurrentData	= Items.AttributeTable.CurrentData;
	Else
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
	EndIf;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	CurrentData.FillingVariant	= "Map";
	Items.Map.Check				= True;
	
	OnChangeFillingVariant();
	
EndProcedure

&AtClient
Procedure ResetCommandChecksChoiceOptions()
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		Items.FromColumn.Check		= False;
		Items.SelectedValue.Check	= False;
		Items.Map.Check				= False;
	Else
		Items.FromColumn_AttributesCreated.Check	= False;
		Items.SelectedValue_AttributesCreated.Check	= False;
		Items.Map_AttributesCreated.Check			= False;
	EndIf;
	
EndProcedure
 
&AtClient
Procedure OnChangeFillingVariant()
	
	VariantArray	= New Array;
	VariantArray.Add("Map");
	VariantArray.Add("From column");
	
	If Items.MapPagesAndObjectAttributes.CurrentPage = Items.PageObjectAttributes Then
		
		CurrentData	= Items.AttributeTable.CurrentData;
		
		If VariantArray.Find(CurrentData.FillingVariant) <> Undefined Then
			Items.AttributeTable_FillValue.Enabled	= False;
		Else
			Items.AttributeTable_FillValue.Enabled	= True;
		EndIf;
		
		If CurrentData.FillingVariant <> "Selected value" AND CurrentData.Use AND NOT CurrentData.IsSimpleType Then
			FillTableWithAttributesOfCreatedObject(CurrentData.GetID());
		EndIf;
		
	Else
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
		
		If VariantArray.Find(CurrentData.FillingVariant) <> Undefined Then
			Items.AttributeTableCreated_FillValue.Enabled	= False;
		Else
			Items.AttributeTableCreated_FillValue.Enabled	= True;
		EndIf;
	EndIf;
	
	IsMap			= (CurrentData.FillingVariant = "Map");
	CurrentData.Use	= NOT IsBlankString(CurrentData.FillingVariant);
	
	If IsMap Then
		AttachIdleHandler("Attached_FillMapTable", 0.1, True);
	EndIf;
	
EndProcedure
//

&AtClient
Procedure AttributeTableOnActivateRow(Item)
	AttachIdleHandler("Attached_AttributeTableOnActivateRow", 0.1, True);
EndProcedure

&AtClient
Procedure Attached_AttributeTableOnActivateRow()
	
	CurrentData	= Items.AttributeTable.CurrentData;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	If CurrentIndex = CurrentData.GetID() Then Return; EndIf;
	
	CurrentIndex	= CurrentData.GetID();
	
	VariantArray	= New Array;
	VariantArray.Add("Map");
	VariantArray.Add("From column");
	
	If VariantArray.Find(CurrentData.FillingVariant) <> Undefined Then
		Items.AttributeTable_FillValue.Enabled	= False;
	Else
		Items.AttributeTable_FillValue.Enabled	= CurrentData.Use;
	EndIf;
	
	If CurrentData.Name	= "Parent"
		AND CurrentData.FullName = FullNameStructure.Products
		AND CurrentData.FillingVariant = "From column" Then
		
		Items.AttributeTable_FillValue.Enabled	= True;
		Items.AttributeTable_FillValue.ChoiceFoldersAndItems	= FoldersAndItems.Folders; //5.03
		
	Else
		Items.AttributeTable_FillValue.ChoiceFoldersAndItems	= FoldersAndItems.FoldersAndItems; //5.03
	EndIf;
	                       
	FillOptionListForFillingAttributesTable();
	
	CurrentPageNameForMap	= Items.PageObjectAttributes.Name;
	
EndProcedure

&AtClient
Procedure AttributeTableCreatedOnActivateRow(Item)
	AttachIdleHandler("Attached_AttributeTableCreatedOnActivateRow", 0.1, True);
EndProcedure

&AtClient
Procedure Attached_AttributeTableCreatedOnActivateRow()
	
	VariantArray	= New Array;
	VariantArray.Add("Map");
	VariantArray.Add("From column");
	
	CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	FillOptionListForFillingAttributesTable();

	If VariantArray.Find(CurrentData.FillingVariant) <> Undefined Then
		Items.AttributeTableCreated_FillValue.Enabled	= False;
	Else
		Items.AttributeTableCreated_FillValue.Enabled	= CurrentData.Use;
	EndIf;
	
	CurrentPageNameForMap	= Items.PageObjectAttributesCreated.Name;
	
EndProcedure

&AtClient
Procedure AttributeTableColumnNameOnChange(Item)
	
	CurrentData				= Items.AttributeTable.CurrentData;
	ColumnName				= StrReplace(Upper(CurrentData.ColumnName),"K","K");
	CurrentData.ColumnName	= ColumnName;
	
	If NOT IsBlankString(CurrentData.ColumnName) Then
		
		CurrentData.FillingVariant	= "From column";
		CurrentData.Use				= True;
		
		FillTableWithAttributesOfCreatedObject(CurrentData.GetID());
		
		If NOT CurrentData.IsSimpleType Then
			
			If CurrentData.FullAttributeName = FullNameStructure.Pictures Then
				NeedToRereadPictures	= True;
				Return;
			EndIf;
			
			SearchStringArray	= SearchFieldTable.FindRows(New Structure("FullName", CurrentData.FullName));
			
			If SearchStringArray.Count() > 0 Then
				
				For Each ValueTableRowOfSearchField In SearchStringArray Do
					
					FilterByAttributes	= New Structure("KeyName, Name", CurrentData.KeyName, ValueTableRowOfSearchField.Name);
					StringArray			= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
					
					If StringArray.Count() > 0 AND StringArray[0].Use
						AND StringArray[0].IsSimpleType Then
						
						StringArray[0].ColumnName	= CurrentData.ColumnName;
						
					EndIf;
					
				EndDo;
				
			Else
				
				FilterByAttributes	= New Structure("KeyName, Name", CurrentData.KeyName, "Description");
				StringArray			= AttributeTableOfCreatedObjects.FindRows(FilterByAttributes);
				
				If StringArray.Count() > 0 AND StringArray[0].Use
					AND StringArray[0].IsSimpleType Then
					
					StringArray[0].ColumnName	= CurrentData.ColumnName;
					
				EndIf;
				
			EndIf;
			
		EndIf;
		
	EndIf;
	
EndProcedure

&AtClient
Procedure AttributeTableCreatedColumnNameOnChange(Item)
	
	CurrentData				= Items.AttributeTableOfCreatedObjects.CurrentData;
	ColumnName				= StrReplace(Upper(CurrentData.ColumnName),"K","K");
	CurrentData.ColumnName	= ColumnName;
	
	If NOT IsBlankString(CurrentData.ColumnName) Then
		CurrentData.FillingVariant	= "From column";
		CurrentData.Use				= True;
	EndIf;
	
EndProcedure

&AtClient
Procedure Attached_FillMapTable()
	
	If CurrentPageNameForMap = "PageObjectAttributes" Then
		CurrentData	= Items.AttributeTable.CurrentData;
	ElsIf CurrentPageNameForMap = "PageObjectAttributesCreated" Then
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
	EndIf;
		
	ErrorFlag	= False;
	FullName	= CurrentData.FullName + "." + CurrentData.Name;
	Items.nCurrentAttribute.Title	= Nstr("en='Data matching for ';pl='Dopasowywanie danych dla ';ru='Сопоставление данных для ';ro='Data matching for '") + "<" + CurrentData.Name + ">";
	
	FixedFilter	= New FixedStructure("FullName", FullName);
	Items.MapTable.RowFilter	= FixedFilter;

	FilterByMap	= New Structure("FullName", FullName);
	StringArray	= MapTable.FindRows(FilterByMap);
	
	If StringArray.Count() <> 0 Then
		ErrorFlag	= True;
	EndIf;
	
	If IsBlankString(CurrentData.ColumnName) Then
		Message(Nstr("en='No table column is specified from which the system will take data to map!';pl='Nie określono kolumny tabeli, z której system pobierze dane dla porównania!';ru='Не указано колонки таблицы, из которой система должна получить данные для сопоставления!';ro='No table column is specified from which the system will take data to map!'"));
		ErrorFlag	= True;
	EndIf;
	
	If ErrorFlag Then Return; EndIf;
	
	FillMapTable(CurrentData.GetID());
	
	Items.MapPagesAndObjectAttributes.CurrentPage	= Items.MapPage;
	
EndProcedure
 
&AtClient
Procedure MapPagesAndObjectAttributesOnCurrentPageChange(Item, CurrentPage)
	 AttachIdleHandler("Attached_OnCurrentPageChange", 0.1, True);
EndProcedure

&AtClient
Procedure Attached_OnCurrentPageChange()
	
	CurrentPage	= Items.MapPagesAndObjectAttributes.CurrentPage;
	
	If CurrentPage = Items.PageObjectAttributes Then
		CurrentPageNameForMap = "PageObjectAttributes";
		AttributeTableOnActivateRow("");
	ElsIf CurrentPage = Items.PageObjectAttributesCreated Then
		
		CurrentData	= Items.AttributeTable.CurrentData;
		
		If CurrentData = Undefined Then Return; EndIf;
		
		CurrentPageNameForMap	= "PageObjectAttributesCreated";

		Items.nAttributeObject.Title	= Nstr("en='Attributes ';pl='Atrybuty ';ru='Атрибуты';ro='Attributes '") + "<" + CurrentData.FullAttributeName + ">";

		If Items.AttributeTableOfCreatedObjects.RowFilter <> Undefined
			AND CurrentData.KeyName = Items.AttributeTableOfCreatedObjects.RowFilter.KeyName Then Return; EndIf;
					
		ResetCommandChecksChoiceOptions();
		
		FixedFilter	= New FixedStructure("KeyName", CurrentData.KeyName);
		Items.AttributeTableOfCreatedObjects.RowFilter = FixedFilter;
		AttributeTableCreatedOnActivateRow("");
		
	ElsIf CurrentPage = Items.MapPage Then
		
		If CurrentPageNameForMap = "PageObjectAttributes" Then
			CurrentData	= Items.AttributeTable.CurrentData;
		ElsIf CurrentPageNameForMap = "PageObjectAttributesCreated" Then
			CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
		EndIf;
		
		If CurrentData	= Undefined Then Return; EndIf;
		
		FullName						= CurrentData.FullName + "." + CurrentData.Name;
		Items.nCurrentAttribute.Title	= Nstr("en='Data matching for ';pl='Dopasowywanie danych dla ';ru='Сопоставление данных для ';ro='Data matching for '") + "<" + CurrentData.Name + ">";
		
		FixedFilter						= New FixedStructure("FullName", FullName);
		Items.MapTable.RowFilter		= FixedFilter;
		
	EndIf;
	
EndProcedure

&AtClient
Procedure AttributeTableSelection(Item, SelectedString, Field, StandardProcessing)
	
	If Field.Name = "AttributeTableSynonym" Then
		
		CurrentData	= AttributeTable.FindByID(SelectedString);
		
		If CurrentData.FillingVariant <> "Selected value" AND CurrentData.Use AND NOT CurrentData.IsSimpleType Then
			Items.MapPagesAndObjectAttributes.CurrentPage	= Items.PageObjectAttributesCreated;
			Attached_OnCurrentPageChange();
		EndIf;
		
	EndIf;
	
EndProcedure

&AtClient
Procedure BackToAttributes(Command)
	
	If Items.MapPagesAndObjectAttributes.CurrentPage=Items.MapPage Then
		
		If CurrentPageNameForMap = "PageObjectAttributes" Then
			Items.MapPagesAndObjectAttributes.CurrentPage	= Items.PageObjectAttributes;
			CurrentPageNameForMap	= "PageObjectAttributes";
		ElsIf CurrentPageNameForMap = "PageObjectAttributesCreated" Then
			Items.MapPagesAndObjectAttributes.CurrentPage	= Items.PageObjectAttributesCreated;
			CurrentPageNameForMap	= "PageObjectAttributesCreated";
		EndIf;
		
	Else
		Items.MapPagesAndObjectAttributes.CurrentPage	= Items.PageObjectAttributes;
		CurrentPageNameForMap	= "PageObjectAttributes";
	EndIf;
	
EndProcedure

&AtClient
Procedure RefillMapList(Command)
	
	If CurrentPageNameForMap = "PageObjectAttributes" Then
		CurrentData	= Items.AttributeTable.CurrentData;
	ElsIf  CurrentPageNameForMap	= "PageObjectAttributesCreated" Then
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
	EndIf;
	
	If IsBlankString(CurrentData.ColumnName) Then
		Message(Nstr("en='No table column is specified from which the system will take data to map!';pl='Nie określono kolumny tabeli, z której system pobierze dane dlia porównania!';ru='Не указано колонки таблицы из которой система должна получить данные для сопоставления!';ro='No table column is specified from which the system will take data to map!'"));
		Return;
	EndIf;
	
	FillMapTable(CurrentData.GetID());
	
EndProcedure

&AtClient
Procedure ClearMapList(Command)
	Notification	= New NotifyDescription("QuestionBeforeCleaningMapEnd", ThisObject);
	Text			= Nstr("en='Are you sure you want to delete the map list for the current attribute?';pl='Czy na pewno chcesz usunąć listę map dla bieżącego atrybutu?';ru='Вы уверены, что хотите удалить список карт для текущего атрибута?';ro='Are you sure you want to delete the map list for the current attribute?'");
	ShowQueryBox(Notification, Text, QuestionDialogMode.YesNo);
EndProcedure

&AtClient
Procedure QuestionBeforeCleaningMapEnd(Result,Parameters) Export
	
	If Result = DialogReturnCode.No Then Return; EndIf;
	
	If CurrentPageNameForMap = "PageObjectAttributes" Then
		CurrentData	= Items.AttributeTable.CurrentData;
	ElsIf CurrentPageNameForMap = "PageObjectAttributesCreated" Then
		CurrentData	= Items.AttributeTableOfCreatedObjects.CurrentData;
	EndIf;
	
	FullName			= CurrentData.FullName + "." + CurrentData.Name;
	FilterByAttributes	= New Structure("FullName", FullName);
	StringArray			= MapTable.FindRows(FilterByAttributes);
	LineCount			= StringArray.Count();
	
	For N = 1 To LineCount Do
		RowIndex	= LineCount - N;
		MapTable.Delete(StringArray[RowIndex]);
	EndDo;
	
EndProcedure

&AtClient
Procedure nAttributeObjectClick(Item)
	FullAttributeName	= StrReplace(Items.nAttributeObject.Title, "Attributes <", "");
	FullAttributeName	= StrReplace(FullAttributeName, ">", "");
	OpenForm(FullAttributeName + ".ListForm");
EndProcedure

&AtClient
Procedure DataTreeCheckOnChange(Item)
	NeedToRereadPictures	= True;
EndProcedure

&AtClient
Procedure TableSelectedProductsStartChoice(Item, ChoiceData, StandardProcessing)
	
	StandardProcessing	= False;
	CurrentData			= Items.TableSelected.CurrentData;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	Description		= GetObjectAttributeValueServer(CurrentData.Products, "Description");
	Notification	= New NotifyDescription("ProductsSelectionEnd", ThisObject, CurrentData);
	OpenForm(GetNameDataProcessor() + ".Form.ProductsChoiceForm", New Structure("Description", Description), ThisForm, , , , Notification, FormWindowOpeningMode.LockOwnerWindow);
	
EndProcedure

&AtClient
Procedure ProductsSelectionEnd (RefProducts,CurrentData) Export
	
	If RefProducts	= Undefined Then Return; EndIf;
	
	If RefProducts <> Undefined Then
		CurrentData.Products	= RefProducts;
	EndIf;
	
EndProcedure

&AtClient
Procedure ShowImage(Command)
	
	CurrentData	= Items.TableSelected.CurrentData;
	
	If CurrentData = Undefined Then Return; EndIf;
	
	ColumnName	= Items.TableSelected.CurrentItem.Name;
	ColumnName	= StrReplace(ColumnName, "TableSelected", "");
	
	TempStorageAddress	= CurrentData[ColumnName];
	
	If NOT IsTempStorageURL(TempStorageAddress) Then Return; EndIf;
	
	If NOT ValueIsFilled(TempStorageAddress) Then Return; EndIf;
	
	OpenForm(GetNameDataProcessor() + ".Form.ImageForm", New Structure("TempStorageAddress", TempStorageAddress), ThisForm);
	
EndProcedure

&AtClient
Procedure AttributeTableBeforeDeleteRow(Item, Cancel)
	
	Cancel			= True;
	Notification	= New NotifyDescription("BeforeDeleteEnd", ThisObject);
	Text			= Nstr("en='Are you sure you want to delete this attribute?';pl='Czy na pewno chcesz usunąć ten atrybut?';ru='Вы уверены, что хотите удалить эот атрибут?';ro='Are you sure you want to delete this attribute?'");
	ShowQueryBox(Notification, Text, QuestionDialogMode.YesNo);
	
EndProcedure

&AtClient
Procedure BeforeDeleteEnd (Result, Parameters) Export
	
	If Result = DialogReturnCode.No Then Return; EndIf;
	AttributeTable.Delete(Items.AttributeTable.CurrentData);
	
EndProcedure

&AtClient
Procedure HierarchyTypeOnChange(Item)
	
	If TableOfLoadedData.Count() <> 0 Then
		Message(Nstr("en='Cleared uploaded data ...';pl='Przesłane dane wyczyszczono ...';ru='Загруженные данные очищены ...';ro='Cleared uploaded data ...'"));
	EndIf;
	
	TableOfLoadedData.Clear();
	FileDataRead	= False;
	
EndProcedure

&AtClient
Procedure OpenModalDataProcessor(Command)
	OpenForm(GetNameDataProcessor() + ".Form.Form", , , New UUID, , , , FormWindowOpeningMode.LockOwnerWindow);
EndProcedure

&AtClient
Procedure UploadPicturesWhenLoadingProductsOnChange(Item)
	
	Items.UpdatePictureDataAvailableInBase.Enabled			= UploadPicturesWhenLoadingProducts;
	Items.DeleteOldPicturesFROMBaseBeforeLoadingNew.Enabled	= UploadPicturesWhenLoadingProducts;
	Items.OldPictureRecordFormat.Enabled					= UploadPicturesWhenLoadingProducts;

	If NOT UploadPicturesWhenLoadingProducts Then
		UpdatePictureDataAvailableInBase			= False;
		DeleteOldPicturesFROMBaseBeforeLoadingNew	=	False;
	EndIf;
	
	NeedToRereadPictures	= True;
	
EndProcedure

&AtClient
Procedure Decoration1Click(Item)
	
	Notification	= New NotifyDescription("BrowserCompletion", ThisObject);
	BeginRunningApplication(Notification, "https://infostart.ru/profile/287329/");
	
EndProcedure

#Region ManageCheck

&AtServer
Procedure ChangeCheckStatus(Flag)
	
	For each ValueTableRow In TableOfLoadedData Do
		ValueTableRow.Check	= Flag;
	EndDo;
	
	NeedToRereadPictures	= True;
	
EndProcedure

&AtClient
Procedure SetAllCheck(Command)
	ChangeCheckStatus(True);
EndProcedure

&AtClient
Procedure UncheckAllItems(Command)
	ChangeCheckStatus(False);
EndProcedure

#EndRegion

&AtClient
Procedure DocumentNameOnChange(Item)
	
	Notification	= New NotifyDescription("QuestionBeforeChangingDocumentEnd", ThisObject);
	
	If NOT IsBlankString(TabularSectionName) Then
		Text	= Nstr("en='If you change all the attribute settings, the previous document will be reset! Continue?';pl='Jeśli zmienisz wszystkie ustawienia atrybutu, poprzedni dokument zostanie zresetowany! Dalej?';ru='Если Вы измените все настройки атрибута, предыдущий документ будет сброшен! Продолжить?';ro='If you change all the attribute settings, the previous document will be reset! Continue?'");
		ShowQueryBox(Notification, Text, QuestionDialogMode.YesNo);
	Else
		ExecuteNotifyProcessing(Notification);
	EndIf;
	
EndProcedure

&AtClient
Procedure QuestionBeforeChangingDocumentEnd (Result,Parameters) Export
	
	If Result = DialogReturnCode.No Then
		
		DocumentName	= StrReplace(FullNameOfCurrentAction, "__", "");
		Return;
		
	EndIf;
	
	CurrentDocumentName	= DocumentName;
	DocumentName		= StrReplace(FullNameOfCurrentAction, "__", "");
	DeleteOldDocumentAttributes();
	
	If IsBlankString(CurrentDocumentName) Then
		
		TabularSectionName	= "";
		DocumentName		= CurrentDocumentName;
		Return;
		
	EndIf;
	
	DocumentName			= CurrentDocumentName;
	FullNameOfCurrentAction	= DocumentName + "__";
	TabularSectionName		= "";
	
	FillDocumentTabularSectionList();
	
EndProcedure

&AtClient
Procedure TabularSectionNameOnChange(Item)
	FillDocumentAttributesList();
EndProcedure

#EndRegion

#Region NavigationCommands

&AtClient
Procedure Next(Command)
	
	HasErrors	= CheckSettingsAreCorrectAndAvailability();
	
	If HasErrors Then Return; EndIf;
	
	If Items.Pages.CurrentPage = Items.StartPage Then
		
		Items.Back.Enabled	= True;
		Items.FormDownloadSettings.Visible	= False;
		
		FillDownloadObjectKindList();
		
		If NOT FileDataRead Then
			ActionKind	= "Products";
			ReadDataFromFile();
		EndIf;
		
		FillAttributesByActionKind();
		
		CurrentIndex			= 999999;
		Items.Pages.CurrentPage	= Items.PageDataDownload;
		AttachIdleHandler("Attached_AttributeTableOnActivateRow", 0.1, True);
		
	ElsIf Items.Pages.CurrentPage = Items.PageDataDownload Then
		
		MoveSelectedPositionsAndFindProductsInBase();
		ReadingPicturesFromFile();
		Items.Pages.CurrentPage	= Items.PageSelectedData;
		Items.Next.Enabled		= False;
		
	EndIf;
	
EndProcedure

&AtClient
Procedure Back(Command)
	
	If Items.Pages.CurrentPage = Items.PageDataDownload Then
		Items.Back.Enabled	= False;
		Items.Next.Enabled	= True;
		Items.FormDownloadSettings.Visible	= True;
		Items.Pages.CurrentPage	= Items.StartPage;
	ElsIf Items.Pages.CurrentPage = Items.PageSelectedData Then
		Items.Pages.CurrentPage	= Items.PageDataDownload;
		Items.Next.Enabled		= True;
	EndIf;
	
EndProcedure

&AtClient
Procedure SetFillVariants(TreeRowParent)
	
	TypeArray	= New Array;
	TypeArray.Add(Type("String"));
	TypeArray.Add(Type("Number"));
	TypeArray.Add(Type("Date"));
	TypeArray.Add(Type("Boolean"));
	TypeArray.Add(Type("ValueStorage"));
	
	For Each TreeRow In TreeRowParent.GetItems() Do
		
		If NOT ValueIsFilled(TreeRow.FillingVariant) Then
			
			If TreeRow.Type.Types().Count() > 0 Then
				IsSimpleType	= (TypeArray.Find(TreeRow.Type.Types()[0]) <> Undefined);
			Else
				IsSimpleType	= True;
			EndIf;
			
			ColumnName	= TreeRow.ColumnName;
			Name		= TreeRow.Name;
			Synonym		= TreeRow.Synonym;
			FillValue	= TreeRow.FillValue;
			
			If IsBlankString(ColumnName) AND ValueIsFilled(FillValue) Then
				TreeRow.FillingVariant	= "SelectedValue";
			ElsIf NOT IsBlankString(ColumnName) AND NOT ValueIsFilled(FillValue) AND IsSimpleType
				OR (Name = "Parent" AND NOT IsBlankString(ColumnName))
				OR (Name = "PictureFile" AND NOT IsBlankString(ColumnName))
				OR (Find(Synonym, "Characteristic") > 0 AND NOT IsBlankString(ColumnName)) //characteristic in products
				OR (Name = "Series" AND NOT IsBlankString(ColumnName)) Then
				
				TreeRow.FillingVariant	= "FromColumn";
				
				If Find(Synonym, "Characteristic") > 0 OR Name = "Series" Then
					TreeRow.CreateIfNotFoundInDatabase	= True;
				EndIf;
				
			ElsIf  NOT IsBlankString(ColumnName) AND NOT ValueIsFilled(FillValue)
				AND (NOT IsBlankString(TreeRow.SerializedData) OR NOT IsSimpleType) Then
				
				TreeRow.FillingVariant	= "Map";
				
				If TreeRow.Name = "Packing" OR NOT TreeRow.RefChartOfCharacteristicTypes.IsEmpty() Then
					TreeRow.CreateIfNotFoundInDatabase	= True;
				EndIf;
				
			EndIf;
		EndIf;
		
		If NOT IsBlankString(TreeRow.FillingVariant) Then
			TreeRow.Use	= True;
		EndIf;
		
		SetFillVariants(TreeRow);
	EndDo;
	
EndProcedure

&AtServer
Function CheckSettingsAreCorrectAndAvailability()
	
	HasErrors	= False;
	
	If Items.Pages.CurrentPage = Items.StartPage Then
		
		If NOT ValueIsFilled(PathToFile) Then
			Message(Nstr("en='The path to the file is not specified!';pl='Ścieżka do pliku niezdefiniowana!';ru='Путь к файлу не определен!';ro='The path to the file is not specified!'"));
			HasErrors=True;
		EndIf;
				
		If HierarchyType <> 0 AND IsBlankString(GroupSeparator) Then
			HasErrors	= True;
			Message(Nstr("en='No group separator specified in the hierarchy row! By default this is // ';pl='W hierarchii wierszu nie określono separatora grup! Domyślnie jest to // ';ru='В иерархии строки не указан разделитель групп! По умолчанию используется // ';ro='No group separator specified in the hierarchy row! By default this is // '"));
		EndIf;
				
	ElsIf Items.Pages.CurrentPage = Items.PageDataDownload Then
		
		StringArraySelected	= TableOfLoadedData.FindRows(New Structure("Check", True));
		
		If StringArraySelected.Count() = 0 Then
			HasErrors	= True;
			Message(Nstr("en='No rows are selected in the loaded data table!';pl='W załadowanej tabeli danych brakuje wybranych wierszy!';ru='В загруженной таблице данных отсутствуют выбранные строки';ro='No rows are selected in the loaded data table!'"));
		EndIf;
		
		If HierarchyType > 0 Then
			
			FilterByAttributes		= New Structure("FullName, Name, Use", FullNameStructure.Products, "Parent", True);
			AttributeStringArray	= AttributeTable.FindRows(FilterByAttributes);
			
			If AttributeStringArray.Count() = 0 Then
				HasErrors	= True;
				Message(Nstr("en='Not selected attribute <Group (parent)>, which is involved in creating a hierarchy!';pl='Nie wybrano atrybutu <Grupa (rodzic)>, który bierze udział w tworzeniu hierarchii!';ru='Атрибут <Группа (родитель)> не выбран, который участвует в создании иерархии!';ro='Not selected attribute <Group (parent)>, which is involved in creating a hierarchy!'"));
			EndIf;
			
		EndIf;
		
		FilterByAttributes	= New Structure("SearchFieldInDatabase",	True);
		AttributeStringArray	= AttributeTable.FindRows(FilterByAttributes);
		
		If AttributeStringArray.Count() = 0 Then
			HasErrors=True;
			Message(Nstr("en='In the attribute table, no attribute is selected by which the system will search for an products in the database!';pl='W tabeli atrybutów nie wybrano żadnego atrybuta, dzięki któremu system będzie szukał nomenklaturu w bazie danych!';ru='В таблице атрибутов не выбрано ни одного атрибута, с помощью которого система будет искать номенклатуру в базе данных!';ro='In the attribute table, no attribute is selected by which the system will search for an products in the database!'"));
		EndIf;
		
		AttributeArray	= AttributeTable.FindRows(New Structure("Use", True));
		
		For each ValueTableRowOfAttributes In AttributeArray Do
			
			If IsBlankString(ValueTableRowOfAttributes.ColumnName)
				AND ValueTableRowOfAttributes.FillingVariant <> "Selected value"
				AND ValueTableRowOfAttributes.IsSimpleType Then
				
				Message(Nstr("en='For object ';pl='Dla obiektu ';ru='Для объекта ';ro='For object '") + "<" + ValueTableRowOfAttributes.FullName + ">"
						+ Nstr("en=', for attribute ';pl=', dla atrybutu ';ru=', для атрибута ';ro=', for attribute '") + "<" + ValueTableRowOfAttributes.Synonym + ">"
						+ Nstr("en=' no column is specified from which to take data!';pl=' nie określono żadnej kolumny, z której można pobierać dane!';ru=' не указано ни одной колонки, из которой можна получить данные!';ro=' no column is specified from which to take data!'"));
						
				HasErrors	= True;
				
			EndIf;
			
		EndDo;
		
		AttributeArray	= AttributeTableOfCreatedObjects.FindRows(New Structure("Use", True));
		
		For each ValueTableRowOfAttributes In AttributeArray Do
			
			If IsBlankString(ValueTableRowOfAttributes.ColumnName)
				AND ValueTableRowOfAttributes.FillingVariant <> "Selected value"
				AND ValueTableRowOfAttributes.IsSimpleType Then
				
				Message(Nstr("en='For object ';pl='Dla obiektu ';ru='Для объекта ';ro='For object '") + "<" + ValueTableRowOfAttributes.FullName + ">"
						+ Nstr("en=', for attribute ';pl=', dla atrybutu ';ru=', для атрибута ';ro=', for attribute '") + "<" + ValueTableRowOfAttributes.Synonym + ">"
						+ Nstr("en=' in the attribute table of the object being created, no column is specified from which to take data!';pl=' w tabeli atrybutów tworzonego obiektu nie określono żadnej kolumny, z której można pobierać dane!';ru=' в таблице атрибутов создаваемого объекта не указано ни одной колонки, из которой можна получить данные!';ro=' in the attribute table of the object being created, no column is specified from which to take data!'"));
				
				HasErrors	= True;
				
			EndIf;
			
		EndDo;
		
		If ToStoreFilesInVolumes AND UploadPicturesWhenLoadingProducts Then
			
			Filter			= New Structure("FullName, Name, Use", FullNameStructure.Pictures, "Volume", True);
			AttributeArray	= AttributeTableOfCreatedObjects.FindRows(Filter);
			
			If AttributeArray.Count() = 0 Then
				HasErrors	= True;
				Message(Nstr("en='The <Volume> attribute for storing pictures that will be loaded is not selected or configured!';pl='Atrybut <Wolumin> do przechowywania obrazów, które zostaną załadowane, nie jest wybrany ani skonfigurowany!';ru='Атрибут <Том> для хранения изображений, которые будут загружены, не выбран или не настроен!';ro='The <Volume> attribute for storing pictures that will be loaded is not selected or configured!'"));
			EndIf;
			
		EndIf;
		
	EndIf;
	
	Return HasErrors;
	
EndFunction

#EndRegion

#Region LoadingDataIntoDocument

&AtClient
Procedure CreateDocument(Command)
	CheckBeforeCreating();
EndProcedure

&AtClient
Procedure CheckBeforeCreating()
	
	If TableSelected.Count() = 0 Then
		Message(Nstr("en='No data to download!';pl='Brak danych do pobrania!';ru='Нет данных для загрузки!';ro='No data to download!'"));
		Return;
	EndIf;
			
	Text	= "";
	
	For Each ValueTableRowSelected In TableSelected Do
		
		If NOT ValueIsFilled(ValueTableRowSelected.Products) Then
			Text	= ?(Text = "", "", Text + Chars.LF)
					+ Nstr("en='In line ';pl='W kolejce ';ru='В строке ';ro='In line '") + String(ValueTableRowSelected.LineNumber)
					+ Nstr("en=' no products is specified! This row will not be loaded into the document!';pl=' nie podano nomenklatury! Ten wiersz nie zostanie załadowany do dokumentu!';ru=' не указана номенклатура! Данная строка не будет загружена в документ!';ro=' no products is specified! This row will not be loaded into the document!'");
		EndIf;
		
	EndDo;
	
	//5.14
	Notification = New NotifyDescription("DocumentCreationEnd", ThisObject);
	
	If NOT IsBlankString(Text) Then
		Message(Text);
		Text	= Nstr("en='There are rows with empty products! These strings will not be loaded into the document! Continue creating a document anyway?';pl='Są wiersze z pustą nomenklaturą! Te wiersze nie zostaną załadowane do dokumentu! Contynuować tworzenie dokumentu w każdym razie?';ru='Имеются строки с пустой номенклатурой! Данные строки не будут загружены в документ! Продолжить создание документа в любом случае?';ro='There are rows with empty products! These strings will not be loaded into the document! Continue creating a document anyway?'");
		ShowQueryBox(Notification, Text, QuestionDialogMode.YesNo);
	Else
		ExecuteNotifyProcessing(Notification);
	EndIf;
	//
	
EndProcedure

&AtClient
Procedure DocumentCreationEnd (Result,Parameters) Export
	
	If Result = DialogReturnCode.No Then Return; EndIf;
	
	If FullNameOfCurrentAction = FullNameStructure.Prices + "__" Then
		WritePricesInDatabase();
	Else
		DocumentRef	= CreateDocumentAtServer();
		
		If WorkMode = 1 Then Return; EndIf;
		
		If DocumentRef <> Undefined Then
			
			DocumentForm	= GetForm(DocumentName + ".ObjectForm", New Structure("Key", DocumentRef));
			GetObj(DocumentRef);
			
			DocumentForm.Modified	= True;
			DocumentForm.Open();
			
		EndIf;
		
	EndIf;
	
EndProcedure

&AtServer
Procedure GetObj(DocumentRef);
	Obj = DocumentRef.GetObject();
EndProcedure

&AtServer
Function CreateDocumentAtServer()
	
	FillDataListFromDatabase();
	
	DocumentManager		= GetObjectManagerByFullName(DocumentName);
	FullNameNewDocument	= DocumentManager.CreateDocument();
	FullNameNewDocument.Fill(Undefined);
	FullNameNewDocument.Date	= CurrentDate();
		
	ValueTableRowSelected	= TableSelected[0];
	TabularSectionName		= "<DocumentAttributes>";
	
	Filter			= New Structure("FullName, TabularSectionName, Use", DocumentName, TabularSectionName, True);
	AttributeArray	= AttributeTable.FindRows(Filter);
	
	For Each ValueTableRowOfAttributes In AttributeArray Do
		
		AttributeName		= ValueTableRowOfAttributes.Name;
  		KeyName				= ValueTableRowOfAttributes.KeyName;
		FullAttributeName	= ValueTableRowOfAttributes.FullAttributeName;
		AttributeValue		= GetValueForAttribute(KeyName, ValueTableRowSelected);
		
		If AttributeValue = Undefined Then Continue; EndIf;
		
		Try
			FullNameNewDocument[AttributeName]	= AttributeValue;
		Except
		EndTry;
		
	EndDo;
	
	For Each ListItem In Items.TabularSectionName.ChoiceList Do
		
		TabularSectionName	= ListItem.Value;
		
		If TabularSectionName = "<DocumentAttributes>" Then Continue; EndIf;
		
		Filter			= New Structure("FullName, TabularSectionName, Use", DocumentName, TabularSectionName, True);
		AttributeArray	= AttributeTable.FindRows(Filter);
		
		If AttributeArray.Count() = 0 Then Continue; EndIf;
		
		For Each ValueTableRowSelected In TableSelected Do
			AddRowToTabularSectionOfDocument(FullNameNewDocument, TabularSectionName, ValueTableRowSelected, AttributeArray);
		EndDo;
		
	EndDo;
	
	Try
		//FullNameNewDocument.DataExchange.Load	= True;
		FullNameNewDocument.Write();
		DocumentRef	= FullNameNewDocument.Ref;
		Message(Nstr("en='Created document ';pl='Utworzono dokument ';ru='Создан документ ';ro='Created document '") + "<" + DocumentRef + ">");
	Except
		Message(ErrorDescription());
	EndTry;
	
	Message(String(CurrentDate())
		+ Nstr("en=' Data loading in ';pl=' Pobieranie  danych  do ';ru=' Загрузка данных в ';ro=' Data loading in '") + "<" + DocumentName + ">"
		+ Nstr("en=' completed!';pl=' zakończone!';ru=' завершена!';ro=' completed!'"));
	
	Return DocumentRef;
	
EndFunction

&AtServer
Procedure AddRowToTabularSectionOfDocument(FullNameNewDocument,TabularSectionName,ValueTableRowSelected,AttributeArray)
	
	NewLineOfATabularSection	= FullNameNewDocument[TabularSectionName].Add();
	
	Try
		NewLineOfATabularSection["Products"]	= ValueTableRowSelected.Products;
	Except EndTry;
	
	For Each ValueTableRowOfAttributes In AttributeArray Do
		
		AttributeName		= ValueTableRowOfAttributes.Name;
		KeyName				= ValueTableRowOfAttributes.KeyName;
		FullAttributeName	= ValueTableRowOfAttributes.FullAttributeName;
		
		If AttributeName	= "Price" AND DocumentName = FullNameStructure.Prices Then Continue; EndIf;
		
		If FullAttributeName = FullNameStructure.Characteristics AND UseCharacteristics Then
			AttributeValue	= GetCreateCharacteristic(KeyName, ValueTableRowSelected);
		ElsIf FullAttributeName = FullNameStructure.Packings AND UsePackings Then
			AttributeValue	= GetCreatePackage(KeyName, ValueTableRowSelected);
		ElsIf FullAttributeName = FullNameStructure.Series AND UseSeries Then
			AttributeValue	= GetCreateSeries(KeyName, ValueTableRowSelected);
		ElsIf FullAttributeName = FullNameStructure.CustomsDeclaration AND UseCustomsDeclaration Then
			AttributeValue	= GetCreateCustomsDeclaration(KeyName, ValueTableRowSelected);
		Else
			AttributeValue	= GetValueForAttribute(KeyName, ValueTableRowSelected);
		EndIf;
		
		If AttributeValue = Undefined Then Continue; EndIf;
		
		Try
			NewLineOfATabularSection[AttributeName]	= AttributeValue;
			
		Except
		EndTry;
		
		//5.13
		If NOT DoNotPerformAmountCalculationInTabularSectionOfDocumentWhenLoading Then
			Try
				RecalculateAmounts(NewLineOfATabularSection, FullNameNewDocument.AmountIncludesVAT);
			Except
			EndTry;
			
		EndIf;
		//
	EndDo;
	
EndProcedure

&AtServer
Procedure RecalculateAmounts(LineOfATabularSection, AmountIncludesVAT, IsBalanceEntry = False)
	
	LineOfATabularSection.Amount	= LineOfATabularSection.Quantity * LineOfATabularSection.Price;
	
	If NOT IsBalanceEntry Then //4.22
		
		If LineOfATabularSection.PercentDiscountsMargins = 100 Then
			
			LineOfATabularSection.Amount	= 0;
			
		ElsIf NOT LineOfATabularSection.PercentDiscountsMargins = 0
			AND NOT LineOfATabularSection.Quantity = 0 Then
			
			LineOfATabularSection.Amount	= LineOfATabularSection.Amount * (1 - LineOfATabularSection.PercentDiscountsMargins / 100);
			
		EndIf;
		
		CalculateVATAmount(LineOfATabularSection,AmountIncludesVAT);
		LineOfATabularSection.Total	= LineOfATabularSection.Amount + ?(AmountIncludesVAT, 0, LineOfATabularSection.VATAmount);
		
	EndIf;
	//
	
EndProcedure

&AtServer
Procedure CalculateVATAmount(LineOfATabularSection,AmountIncludesVAT)
	
	VATRate = DriveReUse.GetVATRateValue(LineOfATabularSection.VATRate);
	
	LineOfATabularSection.VATAmount = ?(AmountIncludesVAT,
									  LineOfATabularSection.Amount - (LineOfATabularSection.Amount) / ((VATRate + 100) / 100),
									  LineOfATabularSection.Amount * VATRate / 100);
	
EndProcedure // CalculateVATAmount()

&AtServer
Function GetProductsPriceTable()
	Query		= New Query;
	Query.Text	=
		"SELECT
		|	PricesSliceLast.Products,
		|	PricesSliceLast.Characteristic,
		|	PricesSliceLast.PriceKind,
		|	PricesSliceLast.Price
		|FROM
		|	InformationRegister.Prices.SliceLast AS PricesSliceLast";
	
	ValueTable_Prices = Query.Execute().Unload();
	ValueTable_Prices.Indexes.Add("Products, Characteristic, PriceKind");
	
	Return ValueTable_Prices;
	
EndFunction

&AtServer
Procedure WritePricesInDatabase()
	
	FillDataListFromDatabase();

	//Filling kinds of prices
	PriceAttributeName	= "Price";
	Filter				= New Structure("FullName, Use, Name", FullNameStructure.Prices, True, PriceAttributeName);
	AttributeArray		= AttributeTable.FindRows(Filter);
	
	If AttributeArray.Count() = 0 Then
		Raise Nstr("en='Main attribute not selected ';pl='Nie wybrano głównego atrybutu ';ru='Не выбран главный атрибут ';ro='Main attribute not selected '") + "<" + PriceAttributeName + ">!";
	EndIf;
	
	ColumnNameArray	= ExpandStringInArrayOfSubstringsServer(AttributeArray[0].ColumnName, ",");
	
	MapStringArray	= MapTable.FindRows(New Structure("FullName", FullNameStructure.Prices + "." + PriceAttributeName));
	
	For Each ValueTableRowSelected In TableSelected Do
		
		If ValueTableRowSelected.Products.IsEmpty() Then Continue; EndIf;
		
		For Each ValueTableRowOfMap In MapStringArray Do
			
			ColumnName	= ValueTableRowOfMap.DataFromColumn;
			PriceKind	= ValueTableRowOfMap.DataFromDatabase;
			
			Try
				Price	= Number(ValueTableRowSelected[ColumnName]);
			Except
				Price	= 0;
			EndTry;
			
			If Price = 0 Then Continue; EndIf;
			
			PriceManager	= GetObjectManagerByFullName(FullNameStructure.Prices);
			Write			= PriceManager.CreateRecordManager();
			Write.Products	= ValueTableRowSelected.Products;
			
			FilterByAttributes		= New Structure("FullName, Use", FullNameStructure.Prices, True);
			AttributeStringArray	= AttributeTable.FindRows(FilterByAttributes);
			
			For Each ValueTableRowOfAttributes In AttributeStringArray Do
				
				AttributeName		= ValueTableRowOfAttributes.Name;
				KeyName				= ValueTableRowOfAttributes.KeyName;
				FullAttributeName	= ValueTableRowOfAttributes.FullAttributeName;
				
				If AttributeName = PriceAttributeName Then Continue; EndIf;
				
				If ValueTableRowOfAttributes.FullAttributeName = FullNameStructure.Characteristics AND UseCharacteristics Then
					AttributeValue	= GetCreateCharacteristic(KeyName, ValueTableRowSelected);
				ElsIf ValueTableRowOfAttributes.FullAttributeName = FullNameStructure.Packings AND UsePackings Then
					AttributeValue	= GetCreatePackage(KeyName,ValueTableRowSelected);
				Else
					AttributeValue	= GetValueForAttribute(KeyName, ValueTableRowSelected);
				EndIf;
				
				If AttributeValue = Undefined Then Continue; EndIf;
				
				Write[AttributeName]	= AttributeValue;
				
			EndDo;
			
			Write.Period	= CurrentDate();
			Write.PriceKind	= PriceKind;
			Write.Price		= Price;
			Write.Relevance	= True;
			Write.Author	= SessionParameters.CurrentUser;
			
			Try
				Write.Write();
			Except
				Message(ErrorDescription());
			EndTry;
			
		EndDo;
		
	EndDo;
	
	Message(String(CurrentDate()) + Nstr("en=' Download prices complete!';pl=' Pobieranie cen zakończone!';ru=' Загрузка цен завершена!';ro=' Download prices complete!'"));
	
EndProcedure

#EndRegion

#Region AutoDownloadCode

&AtClient
Procedure ExecuteDownloadInAutomaticMode(Command)
	
	If NOT ValueIsFilled(PathToFile) Then
		Message(Nstr("en='No path to the file to download!';pl='Brak ścieżki do pliku do pobrania!';ru='Отсутствует путь к файлу для загрузки!';ro='No path to the file to download!'"));
		Return;
	EndIf;
	
	SettingsFileName	= GetSettingsFileName();
	WordArray			= ExpandStringInArrayOfSubstrings(PathToFile, "\");
	FileName			= WordArray[WordArray.Count() - 1];
	PathToFolder		= StrReplace(PathToFile, FileName, "");
	FullPathToSettings	= PathToFolder + SettingsFileName;
	
	PathArray	= New Array;
	PathArray.Add(FullPathToSettings);
	DownloadSettingsEnd(PathArray, "");
	
EndProcedure

&AtClient
Procedure ContinueDownloadingInAutomaticMode()
	
	ActionKind	= "Products";
	ReadDataFromFile();
	
	For Each ValueTableRow In TableOfLoadedData Do
		ValueTableRow.Check	= True;
	EndDo;
	
	MoveSelectedPositionsAndFindProductsInBase();
	ReadingPicturesFromFile();
	
EndProcedure

&AtClient
Procedure ContinueDownloadingInAutomaticModeAfterPictures()
	
	LoadProductsEnd(DialogReturnCode.Yes, "");
	DocumentCreationEnd(DialogReturnCode.Yes, "");
	
EndProcedure

#EndRegion

#Region OtherProceduresAndFunctions

&AtClient
Procedure FillDownloadObjectKindList()
	
	Items.ActionKind.ChoiceList.Clear();
	Items.ActionKind.ChoiceList.Add("Products", Nstr("en='Products and related data';pl='Pozycje i powiązane dane';ru='Номенклатуру и связанные данные';ro='Products and related data'"));
	
	//If UseAdditionalAttributesAndInformation Then
		Items.ActionKind.ChoiceList.Add("AdditionalProductsAttributes", Nstr("en='Additional products attributes';pl='Dodatkowe atrybuty pozycji';ru='Доп. реквизиты номенклатуры';ro='Additional products attributes'"));
		If UseCharacteristics Then
			Items.ActionKind.ChoiceList.Add("AdditionalCharacteristicsAttributes", Nstr("en='Additional characteristics attributes';pl='Dodatkowe atrybuty charakterystyk';ru='Доп. реквизиты характеристик';ro='Additional characteristics attributes'"));
		EndIf;
	//EndIf;
	
	If UseSupplierProducts Then
		Items.ActionKind.ChoiceList.Add("SupplierProducts", Nstr("en='Supplier products';pl='Pozycje dostawcy';ru='Номенклатура поставщиков';ro='Supplier products'"));
	EndIf;

	Items.ActionKind.ChoiceList.Add("Barcodes",Nstr("en='Barcodes';pl='Kody kreskowe';ru='Штрихкоды';ro='Barcodes'"));
	//Items.ActionKind.ChoiceList.Add("Prices", Nstr("en='Prices';pl='Ceny';ru='Цены';ro='Prices'"));
	Items.ActionKind.ChoiceList.Add("Document", Nstr("en='Document';pl='Dokument';ru='Документ';ro='Document'"));
	
EndProcedure

&AtServer
Procedure CustomizeVisibilityWithAutomaticDownload()
	
	ViewFlag	= (WorkMode = 0);
	Items.PositionSettings.Visible					= ViewFlag;
	Items.HierarchySettings.Visible					= ViewFlag;
	Items.GroupProductsSettings.Visible				= ViewFlag;
	Items.GroupPictureCreationSettings.Visible		= ViewFlag;
	Items.ExecuteDownloadInAutomaticMode.Visible	= NOT ViewFlag;
	Items.Next.Enabled								= ViewFlag;
	
EndProcedure

//Fill the map table
&AtServer
Procedure FillMapTable(RowIndex)
	
	If CurrentPageNameForMap = "PageObjectAttributes" Then
		CurrentData	= AttributeTable.FindByID(RowIndex);
	ElsIf CurrentPageNameForMap = "PageObjectAttributesCreated" Then
		CurrentData	= AttributeTableOfCreatedObjects.FindByID(RowIndex);
	EndIf;
	
	ColumnNames	= "Check," + CurrentData.ColumnName;
	ValueTable	= FormAttributeToValue("TableOfLoadedData");
	ValueTable_Copy	= ValueTable.Copy();
	ValueTable_Copy.GroupBy(ColumnNames);
	FullName	= CurrentData.FullName + "." + CurrentData.Name;
	
	ColumnNameArray=ExpandStringInArrayOfSubstringsServer(CurrentData.ColumnName, ",");
	
	If CurrentData.FullName = FullNameStructure.Prices Then
		For each ColumnName In ColumnNameArray Do
			NewValueTableRow				= MapTable.Add();
			NewValueTableRow.FullName		= FullName;
			NewValueTableRow.DataFromColumn	= ColumnName;
			NewValueTableRow.Type			= New TypeDescription(FullNameStructure.PriceKind);
		EndDo;
	Else
		For each ValueTableRow In ValueTable_Copy Do
			If NOT ValueTableRow.Check Then Continue; EndIf;
			
			Value	= ValueTableRow[CurrentData.ColumnName];
			
			FilterByMap	= New Structure("FullName, DataFromColumn",FullName, Value);
			StringArray	= MapTable.FindRows(FilterByMap);
			
			If StringArray.Count() <>0 Then Continue; EndIf;
			
			NewValueTableRow				= MapTable.Add();
			NewValueTableRow.FullName		= FullName;
			NewValueTableRow.DataFromColumn = Value;
			NewValueTableRow.Type			= CurrentData.Type;
		EndDo;
	EndIf;
	
	//5.03
	If CurrentData.Name = "Parent" Then
		Items.MapTableDataFromDatabase.ChoiceFoldersAndItems = FoldersAndItems.Folders;
	Else
		Items.MapTableDataFromDatabase.ChoiceFoldersAndItems = FoldersAndItems.FoldersAndItems;
	EndIf;
	//
EndProcedure

&AtServer
Function GetObjectAttributeValueServer(Ref,Attribute)
	
	Source	= Ref.Metadata().FullName();
		
	Query	= New Query("SELECT " + Attribute +  " FROM " + Source + " WHERE Ref = &Ref");
	Query.SetParameter("Ref", Ref);
	Selection	= Query.Execute().Select();
	Selection.Next();
	
	Return Selection[StrReplace(Attribute, "." , "")];
	
EndFunction

&AtClient
Procedure OtherProcessings(Command)
	Notification	= New NotifyDescription("BrowserCompletion", ThisObject);
	BeginRunningApplication(Notification,"http://infostart.ru/community/profile/287329");
EndProcedure

&AtServer
Function GetNameDataProcessor()
	FullName=FormAttributeToValue("Object");
	Return FullName.Metadata().FullName();
EndFunction

&AtClient
Procedure PathToFileOpen(Item, StandardProcessing)
	StandardProcessing	= False;
	Notification		= New NotifyDescription("BrowserCompletion", ThisObject);
	BeginRunningApplication(Notification, "explorer.exe " + PathToFile);
EndProcedure

&AtClient
Procedure BrowserCompletion (Result,Parameter) Export
	
EndProcedure

&AtServer
Function ObjectAttributeValuesServer(Ref,ObjectAttributes) Export
	
	ValueStructure = New Structure(StrReplace(ObjectAttributes, "." , ""));
			
	Query		= New Query("SELECT " + ObjectAttributes +  " FROM " + Ref.Metadata().FullName() + " WHERE Ref = &Ref");
	Query.SetParameter("Ref", Ref);
	Selection	= Query.Execute().Select();
	Selection.Next();
	FillPropertyValues(ValueStructure, Selection);
	
	Return ValueStructure;
	
EndFunction

&AtClient
Function ExpandStringInArrayOfSubstrings(Val String, Val Separator = " ", Val SkipEmptyStrings = Undefined)
	
	Result = New Array;
	
	// for backward compatibility
	If SkipEmptyStrings = Undefined Then
		
		SkipEmptyStrings = ?(Separator = " ", True, False);
		
		If IsBlankString(String) Then
			
			If Separator = " " Then
				Result.Add("");
			EndIf;
			
			Return Result;
			
		EndIf;
		
	EndIf;
	//
	
	Position = Find(String, Separator);
	
	While Position > 0 Do
		
		Substring = Left(String, Position - 1);
		
		If Not SkipEmptyStrings Or Not IsBlankString(Substring) Then
			Result.Add(TrimAll(Substring));
		EndIf;
		
		String = Mid(String, Position + StrLen(Separator));
		Position = Find(String, Separator);
		
	EndDo;
	
	If Not SkipEmptyStrings Or Not IsBlankString(String) Then
		Result.Add(TrimAll(String));
	EndIf;
	
	Return Result;
	
EndFunction

&AtServer
Function ExpandStringInArrayOfSubstringsServer(Val String, Val Separator = " ", Val SkipEmptyStrings = Undefined)
	
	Result = New Array;
	
	// for backward compatibility
	If SkipEmptyStrings = Undefined Then
		
		SkipEmptyStrings = ?(Separator = " ", True, False);
		
		If IsBlankString(String) Then
			
			If Separator = " " Then
				Result.Add("");
			EndIf;
			
			Return Result;
			
		EndIf;
		
	EndIf;
	//
	
	Position = Find(String, Separator);
	
	While Position > 0 Do
		
		Substring = Left(String, Position - 1);
		
		If Not SkipEmptyStrings Or Not IsBlankString(Substring) Then
			Result.Add(TrimAll(Substring));
		EndIf;
		
		String = Mid(String, Position + StrLen(Separator));
		Position = Find(String, Separator);
		
	EndDo;
	
	If Not SkipEmptyStrings Or Not IsBlankString(String) Then
		Result.Add(TrimAll(String));
	EndIf;
	
	Return Result;
	
EndFunction

&AtServer
Function CutLastSymbol(Val StringSource,Char)
	
	StringSource	= TrimAll(StringSource);
	
	If Right(StringSource,1) = Char Then
		StringLength	= StrLen(StringSource) - 1;
		StringSource	= Left(StringSource, StringLength);
	EndIf;
	
	Return StringSource;
	
EndFunction

&AtClient
Function ConvertToURL(Val DataFile)
    
    DataFile = StrReplace(DataFile," ","%20");
    DataFile = StrReplace(DataFile,"\","/");
    
    Return "file:/" + "/localhost/" + DataFile;
    
EndFunction

&AtServer
Function ClearStringOfAllCharacters(Val SubstitutionString, Val StringExcept = "",Val ReplacedBy = "")
	
	SubstitutionString	= TrimAll(SubstitutionString);
	
	If ValueIsFilled(SubstitutionString) AND TypeOf(SubstitutionString) = Type("String") Then
		UnnecessaryCharacters = " .,-/\()+[]{}«»'*""<>_—:;!?^#%`¶№&=" + Chars.NBSp + Chars.Tab + Chars.LF; //08.04.2017
		
		For Counter = 1 to StrLen(UnnecessaryCharacters) Do
			UnnecessaryCharacters = StrReplace(UnnecessaryCharacters, Mid(StringExcept, Counter, 1), ReplacedBy);
		EndDo;

		For Counter = 1 to StrLen(UnnecessaryCharacters) Do
			SubstitutionString = StrReplace(SubstitutionString, Mid(UnnecessaryCharacters, Counter, 1), ReplacedBy);
		EndDo;
		
		SubstitutionString = StrReplace(SubstitutionString, "YO", "E");
		
	EndIf;
	
	Return SubstitutionString;
	
EndFunction

//5.10
&AtClient
Function GetPictureAddressOnInternet(Formula)
	
	PartArray = ExpandStringInArrayOfSubstrings(Formula, ",", True);
	
	If PartArray.Count() > 0 Then
		CellWordArray = ExpandStringInArrayOfSubstrings(PartArray[0], "/", True);
	EndIf;
	
	InternetAddress 	= "";
	AddressStartFound 	= False;
	FoundAddressEnd 	= False;
	
	For each CellWord In CellWordArray Do
		
		If Find(Lower(CellWord), "http:") > 0 OR Find(Lower(CellWord), "https:") > 0 OR Find(Lower(CellWord), "ftp:") > 0 Then
			
			PieceArray = ExpandStringInArrayOfSubstrings(CellWord, """", True);
			
			If PieceArray.Count() > 0 Then
				InternetAddress = PieceArray[PieceArray.Count() - 1] + "/";
			EndIf;
			
			AddressStartFound = True;
			
		ElsIf AddressStartFound Then
			
			If Find(CellWord, """") > 0 Then
				CellWord = StrReplace(CellWord, """", "");
				FoundAddressEnd = True;
			EndIf;
			
			InternetAddress = InternetAddress + "/" + CellWord;
			
			If FoundAddressEnd Then Break; EndIf;
			
		EndIf;
		
	EndDo;
	
	Return InternetAddress;
	
EndFunction

#EndRegion

//©Drive
 
