﻿
&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
	
	SimpleTypeArray=New Array;
	SimpleTypeArray.Add(Type("String"));
	SimpleTypeArray.Add(Type("Number"));
	SimpleTypeArray.Add(Type("Date"));
	SimpleTypeArray.Add(Type("Boolean"));
	
	FullName=Parameters.FullName;

	MetadataObject=Metadata.FindByFullName(FullName);
	ThisForm.Title=ThisForm.Title+" <"+MetadataObject.Synonym+">";
	
	If Find(FullName,"InformationRegister.")>0 Then
		For each Attribute In MetadataObject.Dimensions Do
			If Find(Attribute.Name,"Delete")>0 Then Continue; EndIf;
			
			CurrentType=Attribute.Type.Types()[0];
			If SimpleTypeArray.Find(CurrentType)=Undefined Then
				AttributeMetadataObject=Metadata.FindByType(CurrentType);
				FullAttributeName=AttributeMetadataObject.FullName();
				If FullAttributeName="Catalog.Products" Then Continue; EndIf;
			EndIf;
			
			AttributeList.Add(FullName+"."+Attribute.Name,Attribute.Synonym,,PictureLib.Dimension);
		EndDo;
		
		For each Attribute In MetadataObject.Resources Do
			If Find(Attribute.Name,"Delete")>0 Then Continue; EndIf;
			
			AttributeList.Add(FullName+"."+Attribute.Name,Attribute.Synonym,,PictureLib.Resource);
		EndDo;
	EndIf;
	
	For each Attribute In MetadataObject.Attributes Do
		If Find(Attribute.Name,"Delete")>0 Then Continue; EndIf;
		
		CurrentType=Attribute.Type.Types()[0];
		If SimpleTypeArray.Find(CurrentType)=Undefined Then
			AttributeMetadataObject=Metadata.FindByType(Attribute.Type.Types()[0]);
			FullAttributeName=AttributeMetadataObject.FullName();
			If FullAttributeName="Catalog.Products" Then Continue; EndIf;
		EndIf;
		
		FullNameWithoutTabularPart=StrReplace(FullName,".Inventory","");
		AttributeList.Add(FullNameWithoutTabularPart+"."+Attribute.Name,Attribute.Synonym,,PictureLib.Attribute);
	EndDo;
	
	AttributeList.SortByPresentation();
		
	If AttributeList.Count()=0 Then
		Message(Nstr("en='No attributes available to choose from!';pl='Brak dostępnych atrybutów do wyboru!';ro='No attributes available to choose from!'"));
	EndIf;
	
EndProcedure

&AtClient
Procedure SelectAttributes(Command)
	AttributeArray=New Array;
	For each Item In AttributeList Do
		If Item.Check Then
			AttributeArray.Add(Item.Value);
		EndIf;
	EndDo;
	
	Close(AttributeArray);
EndProcedure
