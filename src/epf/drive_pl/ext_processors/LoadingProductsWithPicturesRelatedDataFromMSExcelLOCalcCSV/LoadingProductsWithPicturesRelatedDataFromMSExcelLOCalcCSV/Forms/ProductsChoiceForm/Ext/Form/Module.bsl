﻿
&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
	If Parameters.Property("Description") Then
		Description=Parameters.Description;
		ProductsList.Filter.Items.Clear();
		OrGroup=ProductsList.Filter.Items.Add(Type("DataCompositionFilterItemGroup"));
		OrGroup.GroupType=DataCompositionFilterItemsGroupType.OrGroup;
		OrGroup.Use=True;
		
		FilterItem=OrGroup.Items.Add(Type("DataCompositionFilterItem"));
		FilterItem.ComparisonType=DataCompositionComparisonType.Contains;
		FilterItem.LeftValue=New DataCompositionField("Description");
		FilterItem.RightValue=TrimAll(Description);
		FilterItem.Use=True;
		SearchWords=Description;
	EndIf;
EndProcedure

&AtClient
Procedure ResetFilter(Command)
	ProductsList.Filter.Items.Clear();
	SearchWords="";
	Items.ProductsList.Representation=TableRepresentation.HierarchicalList;
EndProcedure

&AtClient
Procedure Select(Command)
	StandardProcessing=False;
	CurrentData=Items.ProductsList.CurrentData;
	If CurrentData=Undefined then
		Close(Undefined);
	Else
		Close(CurrentData.Ref);
	EndIf;
EndProcedure

&AtClient
Procedure ProductsListValueChoice(Item, Value, StandardProcessing)
	Select("");
EndProcedure

&AtClient
Procedure OnOpen(Cancel)
	Items.ProductsList.Representation=TableRepresentation.List;
EndProcedure

