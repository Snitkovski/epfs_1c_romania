﻿/////////////////////////////////// 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.SupplierInvoice");

	RegistrationParametrs.Insert("Presentation", DestinationArray);
	RegistrationParametrs.Insert("Description", "FL NIR - la doc. Cumparari marfuri si servicii");
	RegistrationParametrs.Insert("Version", "1.3.2.7");
	RegistrationParametrs.Insert("SafeMode", False);
	RegistrationParametrs.Insert("Information", "NIR - la doc. Cumparari marfuri si servicii");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
				"NIR",
				"NIR",
				"ServerMethodCall",  		// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				False,
				"MXLPrint");
		
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand = CommandTable.Add();
	NewCommand.Presentation     = Presentation;
	NewCommand.ID               = ID;
	NewCommand.Use              = Use;
	NewCommand.ShowNotification = ShowNotification;
	NewCommand.Modifier         = Modifier;
	
EndProcedure

Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "NIR";
	Except
		Message(NStr("en = 'TemplateName is empty!'; ro = 'TemplateName este goala!'; ru = 'TemplateName пустой!'"));
		Return;
	EndTry;
	TemplateSynonim = "NIR " + ObjectArray[0].Number + " din data " + Format(ObjectArray[0].Date, "DF=yyyy-MM-dd");
	PrintManagement.OutputSpreadsheetDocumentToCollection(
			PrintFormsCollection,
			TemplateName,
			TemplateSynonim,
			CreatePrintForm(ObjectArray, PrintObjects, TemplateName));
	
EndProcedure

Function CreatePrintForm(Ref, PrintObjects, TemplateName)
	
	Var Errors;
	
	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_InventoryReceipt";

	Template = ThisObject.GetTemplate(TemplateName);
	
	Query = New Query();
	Query.Text =
	"SELECT
	|	SupplierInvoice.Company AS Company,
	|	SupplierInvoice.Counterparty AS Counterparty,
	|	SupplierInvoice.Date AS Date,
	|	SupplierInvoice.Number AS Number,
	|	SupplierInvoice.BasisDocument AS BasisDocument,
	|	SupplierInvoice.IncomingDocumentDate AS IncomingDocumentDate,
	|	SupplierInvoice.IncomingDocumentNumber AS IDocNr,
	|	SupplierInvoice.Responsible AS Responsible,
	|	SupplierInvoice.ExchangeRate AS ExchangeRate,
	|	SupplierInvoice.DocumentCurrency AS DocumentCurrency,
	|	SupplierInvoice.Inventory.(
	|		LineNumber AS LineNumber,
	|		Products AS Products,
	|		MeasurementUnit AS MeasurementUnit,
	|		Price AS PriceVal,
	|		CAST(SupplierInvoice.Inventory.Quantity AS NUMBER(5,0)) AS Quantity,
	|		VATRate AS LineVATRate,
	|		VATAmount AS LineVATAmount,
	|		Amount AS LineAmountVal,
	|		Total AS LineTotalVal_VATIncl,
	|		AmountExpense AS LineAmountExpenseVal_VATExcl,
	|		CAST(SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate AS NUMBER(10,2)) AS SumaCuTVA,
	|		CAST(SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate /
	|            (100 + SupplierInvoice.Inventory.VATRate.Rate) * SupplierInvoice.Inventory.VATRate.Rate AS NUMBER(10,2)) AS TVA,
	|		CAST((SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate -
	|               SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate /
	|               (100 + SupplierInvoice.Inventory.VATRate.Rate) * SupplierInvoice.Inventory.VATRate.Rate) AS NUMBER(10,2)) AS SumaFaraTVA,
	|		CAST(((SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate -
	|                SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate /
	|                (100 + SupplierInvoice.Inventory.VATRate.Rate) *
	|                SupplierInvoice.Inventory.VATRate.Rate) /
	|                SupplierInvoice.Inventory.Quantity) AS NUMBER(10,2)) AS PriceVendorFaraTVA,
	|		CAST(SupplierInvoice.Inventory.AmountExpense * SupplierInvoice.ExchangeRate / SupplierInvoice.Inventory.Quantity AS NUMBER(10,2)) AS Expenses,
	|		CAST(((SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate -
	|              SupplierInvoice.Inventory.Total * SupplierInvoice.ExchangeRate /
	|              (100 + SupplierInvoice.Inventory.VATRate.Rate) * SupplierInvoice.Inventory.VATRate.Rate) / SupplierInvoice.Inventory.Quantity + 
	|              SupplierInvoice.Inventory.AmountExpense * SupplierInvoice.ExchangeRate / SupplierInvoice.Inventory.Quantity) AS NUMBER(10,2)) AS PriceUnitar  //  ( (Total * ExchangeRate - Total * ExchangeRate / (100 + VATRate.Rate) * VATRate.Rate) / Quantity + AmountExpense * ExchangeRate / Quantity)
	|	) AS Inventory,
	|	SupplierInvoice.DocumentAmount AS DocumentAmount,
	|	SupplierInvoice.AmountIncludesVAT AS AmountIncludesVAT
	|FROM
	|	Document.SupplierInvoice AS SupplierInvoice
	|WHERE
	|	SupplierInvoice.Ref IN(&Ref)
	|
	|ORDER BY
	|	LineNumber";

	Query.Parameters.Insert("Ref", Ref);
	Selection	= Query.Execute().Choose();
    Tab			= Query.Execute().Unload();
	
	AreaCaption	 		= Template.GetArea("Caption");
	Header 				= Template.GetArea("Header");
	Header1 			= Template.GetArea("Header1");
	AreaInventoryHeader = Template.GetArea("InventoryHeader");
	AreaInventory 		= Template.GetArea("Inventory");
	Footer 				= Template.GetArea("Footer");
	Totals 				= Template.GetArea("Totals");
	Gestionari			= Template.GetArea("Gestionari");
	
	Spreadsheet.Clear();

	InsertPageBreak	= False;
	
	While Selection.Next() Do
		
		DocumentTVAAmount_RON = 0;

		If InsertPageBreak Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;

		Spreadsheet.Put(AreaCaption);
		// Info About Buyer (= Our Company)
		InfoAboutBuyer  = DriveServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.Date, ,);
		
		// Info About Vendor / Seller / Furnizor (= Our Counterparty)
		InfoAboutVendor = DriveServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, Selection.Date, ,);
		
		Header.Parameters["Entity"] 	= InfoAboutBuyer.FullDescr;
		Header.Parameters["Vendor"] 	= InfoAboutVendor.FullDescr;
		
		// "CFE" = Cod Fiscal Entity (= Our Company)
		Header.Parameters["CFE"] 		= InfoAboutBuyer.TIN;
		
		// "CFC" = Cod Fiscal Counterparty (= Vendor = Seller)
		Header.Parameters["CFV"] 		= InfoAboutVendor.TIN;
		
		// "AdresaE" = Entity Legal Address
		Header.Parameters["AdresaE"]	= InfoAboutBuyer.LegalAddress;
		
		// "AdresaC" = Counterparty Legal Address
		Header.Parameters["AdresaV"] 	= InfoAboutVendor.LegalAddress;
		
		Header.Parameters.Fill(Selection);
		Spreadsheet.Put(Header, Selection.Level());

		Header1.Parameters["Number"] 		= Selection.Number;
		Header1.Parameters["Date"]			= Selection.Date;
		Header1.Parameters["DocNumber"] 	= Selection.IDocNr;
		Header1.Parameters["DocDate"] 		= Selection.IncomingDocumentDate;
		
		ExchRate 							= Selection.ExchangeRate;
		Header1.Parameters["ERate"]			= ExchRate;
		
		Spreadsheet.Put(Header1);
		Spreadsheet.Put(AreaInventoryHeader);
				
		SelectionInventory = Selection.Inventory.Choose();
		
		While SelectionInventory.Next() Do
			// LineNumber, Products, MeasurementUnit, Quantity, LineVATRate
			AreaInventory.Parameters.Fill(SelectionInventory);
		
			DocumentTVAAmount_RON = DocumentTVAAmount_RON + SelectionInventory.TVA;
			
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;

		Totals.Parameters["TotalFaraTVA"] = Round(Selection.DocumentAmount * ExchRate - DocumentTVAAmount_RON, 2);
		Totals.Parameters["TotalTVA"] = DocumentTVAAmount_RON;
		Totals.Parameters["Total"] = Round(Selection.DocumentAmount * ExchRate, 2);

		Spreadsheet.Put(Totals);
		
		Gestionari.Parameters.Fill(Selection);
		Spreadsheet.Put(Gestionari);
		Footer.Parameters.Fill(Selection);
		Spreadsheet.Put(Footer);
		InsertPageBreak = True;
	EndDo;
	
	Spreadsheet.PageSize = "A4";
	Spreadsheet.PageOrientation = PageOrientation.Landscape;
	//Spreadsheet.SetHorizontalStretch(True);
	Spreadsheet.FitToPage = True;
	Return Spreadsheet;

EndFunction
