﻿/////////////////////////////////// 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.CashReceipt");
	
	RegistrationParametrs.Insert("Presentation", DestinationArray);
	
	RegistrationParametrs.Insert("Description", "Chitanta - Intrare în casierie");
	RegistrationParametrs.Insert("Version", "1.3.1.5");
	RegistrationParametrs.Insert("SafeMode", False);
	RegistrationParametrs.Insert("Information", "Forma de listare Chitanta - la Doc.Intrare În Casierie");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
				"Chitanta (Intrare în casierie)",
				"Chitanta_Intrare",
				//"CallOfServerMethod",  	// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				"ServerMethodCall",  		// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				False,
				"MXLPrint");
	
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction
 
Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand					= CommandTable.Add();
	NewCommand.Presentation 	= Presentation;
	NewCommand.ID				= ID;
	NewCommand.Use				= Use;
	NewCommand.ShowNotification	= ShowNotification;
	NewCommand.Modifier			= Modifier;
	
EndProcedure

Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "Chitanta_Intrare";			//PrintFormsCollection[0].DesignName;
	Except
		Message(NStr("en='TemplateName is empty';
					  |ro='TemplateName este gol';
					  |ru='TemplateName не заполнено'"));
		Return;
	EndTry;
	
	TemplateSynonim = "Chitanta intrare " + ObjectArray[0].Number + " din data " + Format(ObjectArray[0].Date,"DF=yyyy-MM-dd");
	PrintManagement.OutputSpreadsheetDocumentToCollection(PrintFormsCollection,
															TemplateName,
															TemplateSynonim,
															CreatePrintForm(ObjectArray, PrintObjects, TemplateName));
	
EndProcedure

Function CreatePrintForm(ObjectsArray, PrintObjects, TemplateName)

	Spreadsheet						= New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey 	= "PrintParameters_PettyCashReceipt";
	Template	 					= ThisObject.GetTemplate(TemplateName);
	
	Query = New Query();
	Query.SetParameter("CurrentDocument", ObjectsArray);
	
	Query.Text =
	"SELECT
	|	PettyCashReceipt.Ref 								AS Ref,
	|	PettyCashReceipt.Number 							AS Number,
	|	PettyCashReceipt.Date 								AS DocumentDateRAW,
	|	PettyCashReceipt.Company 							AS Entity,
	|	PettyCashReceipt.Company.Prefix 					AS Prefix,
	|	PettyCashReceipt.Company.RegistrationNumber 		AS ONRCVendor,
	|	PettyCashReceipt.Company.TIN						AS KPPVendor,
	|	PettyCashReceipt.Company.DescriptionFull 			AS Vendor,
	|	PettyCashReceipt.PettyCash.GLAccount.Code 			AS DebitCode,
	|	PettyCashReceipt.Counterparty 						AS Customer,
	|	PettyCashReceipt.Counterparty.RegistrationNumber 	AS ONRCCustomer,
	|	PettyCashReceipt.Counterparty.TIN 					AS KPPCustomer,
	|	PettyCashReceipt.CashCurrency 						AS CashCurrency,
	|	PRESENTATION(PettyCashReceipt.CashCurrency) 		AS CurrencyPresentation,
	|	PettyCashReceipt.AcceptedFrom 						AS AcceptedFrom,
	|	PettyCashReceipt.Basis 								AS Basis,
	|	PettyCashReceipt.BasisDocument 						AS DocumentBasis,
	|	PettyCashReceipt.Application 						AS Application,
	|	PettyCashReceipt.DocumentAmount 					AS DocumentAmount,
	|		CASE
	|		WHEN PettyCashReceipt.OperationKind = VALUE(Enum.OperationTypesCashReceipt.Other)
	|		OR PettyCashReceipt.OperationKind = VALUE(Enum.OperationTypesCashReceipt.CurrencyPurchase)
	|			THEN PettyCashReceipt.Correspondence.Code
	|		ELSE CASE
	|		WHEN PettyCashReceipt.OperationKind = VALUE(Enum.OperationTypesCashReceipt.FromAdvanceHolder)
	|		THEN PettyCashReceipt.AdvanceHolder.AdvanceHoldersGLAccount.Code
	|		ELSE CASE
	|		WHEN PettyCashReceipt.OperationKind = VALUE(Enum.OperationTypesCashReceipt.FromCustomer)
	|			THEN PettyCashReceipt.Counterparty.CustomerAdvancesGLAccount.Code
	|		ELSE CASE
	|		WHEN PettyCashReceipt.OperationKind = VALUE(Enum.OperationTypesCashReceipt.FromVendor)
	|			THEN PettyCashReceipt.Counterparty.GLAccountVendorSettlements.Code
	|		ELSE UNDEFINED
	|		END
	|		END
	|		END
	|		END 											AS BalancedAccount,
	|		CASE
	|		WHEN PettyCashReceipt.OperationKind = VALUE(Enum.OperationTypesCashReceipt.FromCustomer)
	|			THEN PettyCashReceipt.Counterparty.CustomerAdvancesGLAccount.Code
	|		ELSE CASE
	|		WHEN PettyCashReceipt.OperationKind = VALUE(Enum.OperationTypesCashReceipt.FromVendor)
	|			THEN PettyCashReceipt.Counterparty.VendorAdvancesGLAccount.Code
	|		ELSE UNDEFINED
	|		END
	|		END 											AS CorAccountOfAdvances
	|FROM
	|	Document.CashReceipt 							AS PettyCashReceipt
	|WHERE
	|	PettyCashReceipt.Ref IN(&CurrentDocument)";
	
	Selection = Query.Execute().Select();
		
	FirstDocument = True;
		
	LineNumber = 1;
	While Selection.Next() Do
	///////////////////////////////////////////////////////////////////////////
	/////////////////////////////////HEADER/////Start//////////////////////////
	///////////////////////////////////////////////////////////////////////////

		TemplateArea = Template.GetArea("Header");
		
		If Not FirstDocument Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;
		
		FirstDocument  = False;
		
		FirstRowNumber = Spreadsheet.TableHeight + 1;
		
		//Currency = Selection.CashCurrency <> Constants.NationalCurrency.Get();
		Currency = Selection.CashCurrency <> Constants.FunctionalCurrency.Get();
		
		If Selection.DocumentDateRAW < Date('20110101') Then
			DocumentNo = DriveServer.GetNumberForPrinting(Selection.Number, Selection.Prefix);
		Else
			DocumentNo = ObjectPrefixationClientServer.GetNumberForPrinting(Selection.Number, True, True);
		EndIf;
			
		TemplateArea.Parameters.Fill(Selection);
		TemplateArea.Parameters.TipChitanta	= "CHITANȚĂ";
		TemplateArea.Parameters.DocumentNo = DocumentNo;
			
		TemplateArea.Parameters.DocumentDate = Format(Selection.DocumentDateRAW, "DF=dd/MM/yyyy");
		
		InfoAboutVendor = DriveServer.InfoAboutLegalEntityIndividual(Selection.Entity,
																		Selection.DocumentDateRAW, ,);
		InfoAboutCustomer = DriveServer.InfoAboutLegalEntityIndividual(Selection.Customer,
																		Selection.DocumentDateRAW, ,);
		TemplateArea.Parameters.VendorAddress		= InfoAboutVendor.LegalAddress;
		TemplateArea.Parameters.FurnizorBanca		= InfoAboutVendor.Bank;
		TemplateArea.Parameters.FurnizorContDecont	= InfoAboutVendor.AccountNo;
		TemplateArea.Parameters.CustomerAddress		= InfoAboutCustomer.LegalAddress;
		TemplateArea.Parameters.ClientBanca			= InfoAboutCustomer.Bank;
		TemplateArea.Parameters.ClientContDecont	= InfoAboutCustomer.AccountNo;
			
		Spreadsheet.Put(TemplateArea);

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////HEADER/////End//////////////////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////////////////////////STRING/////Start//////////////////////////
	///////////////////////////////////////////////////////////////////////////
		
		TemplateArea = Template.GetArea("String");
		TemplateArea.Parameters.Fill(Selection);
		TemplateArea.Parameters.LineNumber    		= LineNumber;
			PaymentAmount 							= Format(Selection.DocumentAmount, "ND=15; NFD=2") +
												    	  ?(Currency, " " + TrimAll(Selection.CashCurrency), "");
		TemplateArea.Parameters.PaymentAmount		= PaymentAmount;
		If ValueIsFilled(Selection.DocumentBasis)   Then
			TemplateArea.Parameters.DocumentBasis   = Selection.DocumentBasis;
		Else
			TemplateArea.Parameters.DocumentBasis   = "Avans";
		EndIf;
		Spreadsheet.Put(TemplateArea);
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////STRING/////End//////////////////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////////////////////////FOOTER/////Start//////////////////////////
	///////////////////////////////////////////////////////////////////////////

		TemplateArea = Template.GetArea("Footer");

		TemplateArea.Parameters.Fill(Selection);
		TemplateArea.Parameters.Amount 				= DriveServer.AmountsFormat(Selection.DocumentAmount, Selection.CashCurrency);
		TemplateArea.Parameters.AmountInWords 		= DriveServer.FormatPaymentDocumentAmountInWords(
																							Selection.DocumentAmount,
																							//Selection.CashCurrency,
																							"leu, lei, M, ban, bani, W, 2",
																							False,
																							"L=ro_RO") + " ";
		Heads = DriveServer.OrganizationalUnitsResponsiblePersons(Selection.Entity, Selection.DocumentDateRAW);
		TemplateArea.Parameters.CashierNameAndSurname = Heads.CashierNameAndSurname;

		Spreadsheet.Put(TemplateArea);

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////FOOTER/////End//////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//////////////////////////Two Per Page/////Start///////////////////////////
	/////////////////////////////////HEADER/////Start//////////////////////////
	///////////////////////////////////////////////////////////////////////////

		TemplateArea = Template.GetArea("Header");
		
		//Currency = Selection.CashCurrency <> Constants.NationalCurrency.Get();
		Currency = Selection.CashCurrency <> Constants.FunctionalCurrency.Get();
			
		If Selection.DocumentDateRAW < Date('20110101') Then
			DocumentNo = DriveServer.GetNumberForPrinting(Selection.Number, Selection.Prefix);
		Else
			DocumentNo = ObjectPrefixationClientServer.GetNumberForPrinting(Selection.Number, True, True);
		EndIf;
			
		TemplateArea.Parameters.Fill(Selection);
		TemplateArea.Parameters.TipChitanta   		= "CHITANȚĂ";
		TemplateArea.Parameters.DocumentNo   		= DocumentNo;
			
		TemplateArea.Parameters.DocumentDate		= Format(Selection.DocumentDateRAW, "DF=dd/MM/yyyy");
		
		InfoAboutVendor   						= DriveServer.InfoAboutLegalEntityIndividual(Selection.Entity,
																							Selection.DocumentDateRAW, ,);
		InfoAboutCustomer 						= DriveServer.InfoAboutLegalEntityIndividual(Selection.Customer,
																							Selection.DocumentDateRAW, ,);
		TemplateArea.Parameters.VendorAddress		= InfoAboutVendor.LegalAddress;
		TemplateArea.Parameters.FurnizorBanca		= InfoAboutVendor.Bank;
		TemplateArea.Parameters.FurnizorContDecont	= InfoAboutVendor.AccountNo;
		TemplateArea.Parameters.CustomerAddress		= InfoAboutCustomer.LegalAddress;
		TemplateArea.Parameters.ClientBanca			= InfoAboutCustomer.Bank;
		TemplateArea.Parameters.ClientContDecont	= InfoAboutCustomer.AccountNo;
		
		Spreadsheet.Put(TemplateArea);

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////HEADER/////End//////////////////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////////////////////////STRING/////Start//////////////////////////
	///////////////////////////////////////////////////////////////////////////
		
		TemplateArea = Template.GetArea("String");
		
		TemplateArea.Parameters.Fill(Selection);
		TemplateArea.Parameters.LineNumber    		= LineNumber;
			PaymentAmount 							= Format(Selection.DocumentAmount, "ND=15; NFD=2") +
												    	  ?(Currency, " " + TrimAll(Selection.CashCurrency), "");
		TemplateArea.Parameters.PaymentAmount		= PaymentAmount;
		
		If ValueIsFilled(Selection.DocumentBasis)   Then
			TemplateArea.Parameters.DocumentBasis   = Selection.DocumentBasis;
		Else
			TemplateArea.Parameters.DocumentBasis   = "Avans";
		EndIf;
		
		Spreadsheet.Put(TemplateArea);
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////STRING/////End//////////////////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////////////////////////FOOTER/////Start//////////////////////////
	///////////////////////////////////////////////////////////////////////////

		TemplateArea = Template.GetArea("Footer");

		TemplateArea.Parameters.Fill(Selection);
		TemplateArea.Parameters.Amount 			= DriveServer.AmountsFormat(Selection.DocumentAmount, Selection.CashCurrency);
		TemplateArea.Parameters.AmountInWords 	= DriveServer.FormatPaymentDocumentAmountInWords(
																			Selection.DocumentAmount,
																			//Selection.CashCurrency,
																			"leu, lei, M, ban, bani, W, 2",
																			False,
																			"L=ro_RO") + " ";
		Heads = DriveServer.OrganizationalUnitsResponsiblePersons(Selection.Entity,
																	Selection.DocumentDateRAW);
		TemplateArea.Parameters.CashierNameAndSurname = Heads.CashierNameAndSurname;
			
		Spreadsheet.Put(TemplateArea);

	////////////////////////////Two Per Page/////End///////////////////////////
	///////////////////////////////////HEADER/////End//////////////////////////
	///////////////////////////////////////////////////////////////////////////
				
		PrintManagement.SetDocumentPrintArea(Spreadsheet, FirstRowNumber, PrintObjects, Selection.Ref);

	EndDo;
	
	Spreadsheet.FitToPage = True;
	
	Return Spreadsheet;

EndFunction
