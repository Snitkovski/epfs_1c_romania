﻿/////////////////////////////////// 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.Production");

	RegistrationParametrs.Insert("Presentation", DestinationArray);
	RegistrationParametrs.Insert("Description", "Production form");
	RegistrationParametrs.Insert("Version", "1.3.1.5");
	RegistrationParametrs.Insert("SafeMode", False);
	RegistrationParametrs.Insert("Information", "Production form");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
		"Production form",
		"Production",
		//"CallOfServerMethod",  	// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
		"ServerMethodCall",  		// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
		False,
		"Production");
		
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand = CommandTable.Add();
	NewCommand.Presentation     = Presentation;
	NewCommand.ID               = ID;
	NewCommand.Use              = Use;
	NewCommand.ShowNotification = ShowNotification;
	NewCommand.Modifier         = Modifier;
	
EndProcedure

Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "Production";
	Except
		Message(NStr("en = 'TemplateName is empty!'; ro = 'TemplateName este goala!'; ru = 'TemplateName пустой!'"));
		Return;
	EndTry;
	TemplateSynonim = "Production form " + ObjectArray[0].Number + " from date " + Format(ObjectArray[0].Date,"DF=yyyy-MM-dd");
	PrintManagement.OutputSpreadsheetDocumentToCollection(
			PrintFormsCollection,
			TemplateName,
			TemplateSynonim,
			CreatePrintForm(ObjectArray, PrintObjects, TemplateName));
	
EndProcedure

Function CreatePrintForm(Ref, PrintObjects, TemplateName)
	
	Var Errors;
	
	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_Production";

	Template = ThisObject.GetTemplate(TemplateName);
	
	Query = New Query();
	Query.Text =
	"SELECT
	|	Production.Date AS DocumentDate,
	|	Production.StructuralUnit AS WarehousePresentation,
	|	Production.Cell AS CellPresentation,
	|	Production.Number AS Number,
	|	Production.Company.Prefix AS Prefix,
	|	Production.InventoryStructuralUnit AS InventoryStructuralUnit,
	|	Production.DisposalsStructuralUnit AS DisposalsStructuralUnit,
	|	Production.Products.(
	|		LineNumber AS LineNumber,
	|		Products.Warehouse AS Warehouse,
	|		Products.Cell AS Cell,
	|		CASE
	|			WHEN (CAST(Production.Products.Products.DescriptionFull AS STRING(100))) = """"
	|				THEN Production.Products.Products.Description
	|			ELSE Production.Products.Products.DescriptionFull
	|		END AS InventoryItem,
	|		Products.SKU AS SKU,
	|		Products.Code AS Code,
	|		MeasurementUnit.Description AS MeasurementUnit,
	|		Quantity AS Quantity,
	|		Characteristic AS Characteristic,
	|		Products.ProductsType AS ProductsType,
	|		ConnectionKey AS ConnectionKey,
	|		Products.SKU AS SKU1
	|	) AS Products,
	|	Production.SerialNumbersProducts.(
	|		SerialNumber AS SerialNumber,
	|		ConnectionKey AS ConnectionKey
	|	) AS SerialNumbersProducts,
	|	Production.Inventory.(
	|		LineNumber AS LineNumber,
	|		CASE
	|			WHEN (CAST(Production.Inventory.Products.DescriptionFull AS STRING(100))) = """"
	|				THEN Production.Inventory.Products.Description
	|			ELSE Production.Inventory.Products.DescriptionFull
	|		END AS InventoryItem,
	|		Products.SKU AS SKU,
	|		Products AS Products,
	|		Products.Code AS Code,
	|		Characteristic AS Characteristic,
	|		Batch AS Batch,
	|		Quantity AS Quantity,
	//|		Reserve AS Reserve,
	|		MeasurementUnit AS MeasurementUnit,
	|		Specification AS Specification,
	|		CostPercentage AS CostPercentage,
	|		SerialNumbers AS SerialNumbers,
	|		Products.ProductsType AS ProductsType,
	|		ConnectionKey AS ConnectionKey,
	|		Products.SKU AS SKU2
	|	) AS Inventory,
	|	Production.Disposals.(
	|		LineNumber AS LineNumber,
	|		CASE
	|			WHEN (CAST(Production.Disposals.Products.DescriptionFull AS STRING(100))) = """"
	|				THEN Production.Disposals.Products.Description
	|			ELSE Production.Disposals.Products.DescriptionFull
	|		END AS InventoryItem,
	|		Products.SKU AS SKU,
	|		Products AS Products,
	|		Products.Code AS Code,
	|		Characteristic AS Characteristic,
	|		Batch AS Batch,
	|		Quantity AS Quantity,
	|		Products.ProductsType AS ProductsType,
	|		MeasurementUnit AS MeasurementUnit,
	|		Products.SKU AS SKU3
	|	) AS Disposals,
	|	Production.OperationKind AS OperationKind,
	|	Production.ProductsStructuralUnit AS ProductsStructuralUnit
	|FROM
	|	Document.Production AS Production
	|WHERE
	|	Production.Ref = &CurrentDocument";
	
	Query.Parameters.Insert("CurrentDocument", Ref[0]);
	Header = Query.Execute().Select();
	Header.Next();

	LinesSelectionInventory = Header.Products.Select();
	LinesSelectionSerialNumbers = Header.SerialNumbersProducts.Select();
	LinesSelectionMaterials = Header.Inventory.Select();
	LinesSelectionDisposals = Header.Disposals.Select();
				
	If Header.DocumentDate < Date('20110101') Then
		DocumentNumber = DriveServer.GetNumberForPrinting(Header.Number, Header.Prefix);
	Else
		DocumentNumber = ObjectPrefixationClientServer.GetNumberForPrinting(Header.Number, True, True);
	EndIf;
	
	//ByCells = Constants.FunctionalOptionAccountingByCells.Get();
	ByCells = Constants.UseStorageBins.Get();
	
	//If Header.OperationKind = enums.OperationKindsInventoryAssembly.Assembly Then
	If Header.OperationKind = Enums.OperationTypesProduction.Assembly Then
		FirstUnit = Header.ProductsStructuralUnit;
		SecondUnit = Header.InventoryStructuralUnit;
	Else
		FirstUnit = Header.InventoryStructuralUnit;
		SecondUnit = Header.ProductsStructuralUnit;
	EndIf;
	ThirdUnit = Header.DisposalsStructuralUnit;
	
	TemplateArea = Template.GetArea(?(ByCells, "Title", "Title|Main"));
	TemplateArea.Parameters.HeaderText = "Production No "
											+ DocumentNumber
											+ " from "
											+ Format(Header.DocumentDate, "DLF=DD");
											
	Spreadsheet.Put(TemplateArea);
		
	TemplateArea = Template.GetArea(?(ByCells, "PrintingTime", "PrintingTime|Main"));
	TemplateArea.Parameters.PrintingTime = "Date and time of printing: "
										 	+ CurrentDate()
											+ ". User: "
											+ Users.CurrentUser();
	TemplateArea.Parameters.OperationKind = Header.OperationKind;
	Spreadsheet.Put(TemplateArea);

	TemplateArea = Template.GetArea(?(ByCells, "Warehouse", "Warehouse|Main"));
	TemplateArea.Parameters.WarehousePresentation = FirstUnit;//Header.WarehousePresentation;
	TemplateArea.Parameters.TableName = Metadata.Documents.Production.TabularSections.Products.Synonym;
	Spreadsheet.Put(TemplateArea);

	If ByCells Then
		TemplateArea = Template.GetArea("Cell");
		TemplateArea.Parameters.CellPresentation = Header.CellPresentation;
		Spreadsheet.Put(TemplateArea);
	EndIf;

	TemplateArea = Template.GetArea(?(ByCells, "TableHeader", "TableHeader|Main"));
	Spreadsheet.Put(TemplateArea);
	TemplateArea = Template.GetArea(?(ByCells, "Row", "Row|Main"));
	
	While LinesSelectionInventory.Next() Do

		If Not LinesSelectionInventory.ProductsType = Enums.ProductsTypes.InventoryItem Then
			Continue;
		EndIf;
		
		TemplateArea.Parameters.Fill(LinesSelectionInventory);
		StringSerialNumbers = WorkWithSerialNumbers.SerialNumbersStringFromSelection(LinesSelectionSerialNumbers, LinesSelectionInventory.ConnectionKey);
		TemplateArea.Parameters.InventoryItem = DriveServer.GetProductsPresentationForPrinting(
			LinesSelectionInventory.InventoryItem,
			LinesSelectionInventory.Characteristic,
			LinesSelectionInventory.SKU1,
			StringSerialNumbers);
			
		Spreadsheet.Put(TemplateArea);
		
	EndDo;
	
	TemplateArea = Template.GetArea(?(ByCells, "Total", "Total|Main"));
	Spreadsheet.Put(TemplateArea);
	TemplateArea = Template.GetArea(?(ByCells, "Warehouse", "Warehouse|Main"));
	TemplateArea.Parameters.WarehousePresentation = SecondUnit;//Header.InventoryStructuralUnit;
	TemplateArea.Parameters.TableName = Metadata.Documents.Production.TabularSections.Inventory.Synonym;
	Spreadsheet.Put(TemplateArea);
	TemplateArea = Template.GetArea(?(ByCells, "TableHeader", "TableHeader|Main"));
	Spreadsheet.Put(TemplateArea);
	TemplateArea = Template.GetArea(?(ByCells, "Row", "Row|Main"));
	
	While LinesSelectionMaterials.Next() Do

		If Not LinesSelectionMaterials.ProductsType = Enums.ProductsTypes.InventoryItem Then
			Continue;
		EndIf;
		
		TemplateArea.Parameters.Fill(LinesSelectionMaterials);
		StringSerialNumbers = WorkWithSerialNumbers.SerialNumbersStringFromSelection(LinesSelectionSerialNumbers, LinesSelectionMaterials.ConnectionKey);
		TemplateArea.Parameters.InventoryItem = DriveServer.GetProductsPresentationForPrinting(
			LinesSelectionMaterials.InventoryItem,
			LinesSelectionMaterials.Characteristic,
			LinesSelectionMaterials.SKU2,
			StringSerialNumbers);
			
		Spreadsheet.Put(TemplateArea);
		
	EndDo;
	
	TemplateArea = Template.GetArea(?(ByCells, "Total", "Total|Main"));
	Spreadsheet.Put(TemplateArea);
	
	If LinesSelectionDisposals.Count() = 0 Then
		Return Spreadsheet;
	EndIf;
	
	TemplateArea = Template.GetArea(?(ByCells, "Warehouse", "Warehouse|Main"));
	TemplateArea.Parameters.WarehousePresentation = ThirdUnit;//Header.DisposalsStructuralUnit;
	TemplateArea.Parameters.TableName = Metadata.Documents.Production.TabularSections.Disposals.Synonym;
	Spreadsheet.Put(TemplateArea);
	TemplateArea = Template.GetArea(?(ByCells, "TableHeader", "TableHeader|Main"));
	Spreadsheet.Put(TemplateArea);
	TemplateArea = Template.GetArea(?(ByCells, "Row", "Row|Main"));
	
	While LinesSelectionDisposals.Next() Do

		If Not LinesSelectionDisposals.ProductsType = Enums.ProductsTypes.InventoryItem Then
			Continue;
		EndIf;
		
		TemplateArea.Parameters.Fill(LinesSelectionDisposals);
		TemplateArea.Parameters.InventoryItem = DriveServer.GetProductsPresentationForPrinting(
			LinesSelectionDisposals.InventoryItem,
			LinesSelectionDisposals.Characteristic,
			LinesSelectionDisposals.SKU3);
			
		Spreadsheet.Put(TemplateArea);
		
	EndDo;
	
	TemplateArea = Template.GetArea(?(ByCells, "Total", "Total|Main"));
	Spreadsheet.Put(TemplateArea);
	
	Return Spreadsheet;

EndFunction
