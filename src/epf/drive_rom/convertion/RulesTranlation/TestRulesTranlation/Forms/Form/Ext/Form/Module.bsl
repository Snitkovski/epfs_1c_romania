﻿
&AtClient
Procedure ChoiseFile(Filter,Item, FileDialogMode)
    
    NotifyDescription = New NotifyDescription("EndFileChoise",ThisObject,Item);

	Dialog = New FileDialog(FileDialogMode);
    
    Dialog.Multiselect = False;
    Dialog.Filter = Filter;
    
    Dialog.Show(NotifyDescription);

EndProcedure

&AtClient
Procedure EndFileChoise(SelectedFiles,AdditionalParameters) Export
    
    If SelectedFiles = Undefined
        OR SelectedFiles.Count() = 0 Then
        Return
    EndIf;
    
    If IsBlankString(AdditionalParameters) Then
        
       Result.Write(SelectedFiles[0],TextEncoding.UTF8);
       
       CommonClientServer.MessageToUser(SelectedFiles[0])
    
    Else
        
         ThisObject[AdditionalParameters] = SelectedFiles[0]
    
    EndIf;

EndProcedure

&AtClient
Procedure RulesStartChoice(Item, ChoiceData, StandardProcessing)
    
    StandardProcessing = False;
    
    ChoiseFile("Rules(*.xml)|*.xml","Rules",FileDialogMode.Open);
    
EndProcedure

&AtClient
Procedure TransformatorStartChoice(Item, ChoiceData, StandardProcessing)
    
    StandardProcessing = False;
    
    ChoiseFile("Transformators(*.xslt)|*.xslt","Transformator",FileDialogMode.Open);
    
EndProcedure

&AtClient
Procedure Transform(Command)
    
    Result.Clear();
    
    Transform = New XSLTransform;
    Transform.LoadXSLStylesheetFromFile(Transformator);
    
    Result.AddLine(Transform.TransformFromFile(Rules)) ;
    
EndProcedure

&AtClient
Procedure Save(Command)
    
    ChoiseFile("Rules(*.xml)|*.xml","",FileDialogMode.Save);
    
EndProcedure
 
