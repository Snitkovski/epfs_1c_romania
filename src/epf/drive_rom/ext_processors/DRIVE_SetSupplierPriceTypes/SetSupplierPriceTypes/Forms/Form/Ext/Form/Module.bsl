﻿
&AtClient
Procedure SetPraceTypes(Command)
    
	Description = New NotifyDescription("RunBackgroundJobEnd",ThisObject);
	
	LongOperation = RunBackgroundJobAtServer("SetPriceTypes","Set price types");
	
	If LongOperation=Undefined Then
		Return
    EndIf;
    
	WaitSettings = TimeConsumingOperationsClient.IdleParameters(ThisObject);
	WaitSettings.OutputProgressBar  = True;
	WaitSettings.Interval           = 1;
    
    TimeConsumingOperationsClient.WaitForCompletion(LongOperation,Description,WaitSettings);
    
EndProcedure

&AtServer
Function RunBackgroundJobAtServer(ProcName,JobName)
	
	RunningProc = "TimeConsumingOperations.RunDataProcessorObjectModuleProcedure";
	
	ExtDPFileNam=GetExternalDataProcessorFileName();
	
	If IsBlankString(ExtDPFileNam) Тогда
		CommonClientServer.MessageToUser(НСтр("en = 'Internal error!'"));
		Return Undefined
	EndIf;
    
    ExecutionParameters=New Structure;
    
	JobParameters = New Structure;
	JobParameters.Insert("DataProcessorName",ExtDPFileNam);
	JobParameters.Insert("MethodName", ProcName);
	JobParameters.Insert("IsExternalDataProcessor", True);
	JobParameters.Insert("AdditionalDataProcessorRef",PredefinedValue("Catalog.AdditionalReportsAndDataProcessors.EmptyRef"));
	JobParameters.Insert("ExecutionParameters",ExecutionParameters);
	JobParameters.Insert("SafeMode",False);
    
    RunningParam=TimeConsumingOperations.BackgroundExecutionParameters(UUID);
    RunningParam.BackgroundJobDescription=JobName;
    
    Result=TimeConsumingOperations.ExecuteInBackground(RunningProc,JobParameters,RunningParam);
	
	Возврат Result;
	
EndFunction

&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
    
    UsedFileName=FormAttributeToValue("Object").UsedFileName;
    
EndProcedure

&AtClient
Procedure OnOpen(Cancel)
    
	ND=Новый NotifyDescription("EndPuttingFiles",ThisObject);
	
	BeginPutFile(ND,,UsedFileName,False,UUID)
    
EndProcedure

&AtClient
Procedure EndPuttingFiles(Result,Adress,Files,AdditionalParameters) Export
	
	TransferredFileDescription=New TransferableFileDescription(Files,Adress)
	
EndProcedure

&AtServer
Function GetExternalDataProcessorFileName()
	
	FileEDP=Новый File(TransferredFileDescription.Name);
	
	TempFile=GetTempFileName(FileEDP.Extension);
	
	Try
		GetFromTempStorage(TransferredFileDescription.Location).Write(TempFile)
	Except
    EndTry;
    
    FileEDP=Новый File(TempFile);
	
	Return ?(FileEDP.Exist(),FileEDP.FullName,"")
	
EndFunction

&НаКлиенте
Procedure RunBackgroundJobEnd(Result, AdditionalParameters) Export
	ClearMessages();
	If Result = Undefined Then
		Return;
	ElsIf Result.Status = "Error" Then
			CommonClientServer.MessageToUser(Result.BriefErrorDescription);
	ElsIf Result.Status = "Completed" Then
		ShowUserNotification(НСтр("en = 'Operation completed successfully'"), ,, PictureLib.Success);
	КонецЕсли;
EndProcedure

