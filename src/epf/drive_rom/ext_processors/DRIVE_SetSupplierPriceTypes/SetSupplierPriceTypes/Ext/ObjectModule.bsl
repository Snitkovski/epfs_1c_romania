﻿
Procedure SetPriceTypes(IncommingParam,ResultAdress) Export
    
    Query=New Query;
    
    Query.Text="SELECT
               |    CP.Ref AS CP,
               |    PRESENTATION(CP.Ref) AS RefPresentation
               |FROM
               |    Catalog.Counterparties AS CP
               |WHERE
               |    NOT CP.IsFolder
               |    AND CP.Supplier
               |    AND CP.SupplierPriceTypes = VALUE(Catalog.SupplierPriceTypes.EmptyRef)";
    
    Result=Query.Execute();
    
    If Result.IsEmpty() Then
        Return
    EndIf;
    
    ResultSelection=Result.Select();
    
    While ResultSelection.Next() Do
        
        TimeConsumingOperations.ReportProgress(,ResultSelection.RefPresentation);
        
        Object=ResultSelection.CP.GetObject();
        
        AddHandler Object.OnWrite, RewriteCounterpartyContracts;
        
        Object.Write();
        
        RemoveHandler Object.OnWrite, RewriteCounterpartyContracts;
        
    EndDo;
    
EndProcedure

Procedure RewriteCounterpartyContracts(Source,Cancel)
    
    CatakogSelection=Catalogs.CounterpartyContracts.Select(,Source.Ref);
    
    While CatakogSelection.Next() Do
        
        Object=CatakogSelection.GetObject();
        
        Object.SupplierPriceTypes=Source.SupplierPriceTypes;
        
        Object.Write();
        
    EndDo;
   
EndProcedure
