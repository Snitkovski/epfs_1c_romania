﻿
&AtServer
Procedure Command1AtServer()
	// Insert handler content.
	Common.CommonSettingsStorageDelete("PrintFormsSettings", Undefined, Undefined);
EndProcedure

&AtClient
Procedure Command1(Command)
	Command1AtServer();
EndProcedure
