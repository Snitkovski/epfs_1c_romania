﻿#Region ProgramInterface

Function ExternalDataProcessorInfo() Export

	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind = AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindAdditionalDataProcessor();
	RegistrationParameters.Version		= "2.0.0.0";
	RegistrationParameters.Description	= "Import currency exchange rates BNR";
	RegistrationParameters.Information	= NStr("ru = 'Импорт курсов валют с BNR';
												|ro = 'Import cursuri valutare BNR';
												|en = 'Import currency exchange rates BNR'");
	RegistrationParameters.SafeMode		= False;
	
	Permissions = New Array;
	RegistrationParameters.Insert("Permissions", Permissions);
	
    Return RegistrationParameters;
	
EndFunction

#EndRegion

#Region ServiceProgramInterface

Function ImportCurrencyRatesByParameters(Val Currencies, Val Company, Val ImportBeginOfPeriod, Val ImportEndOfPeriod, ErrorsOccuredOnImport = False) Export
    
    StateOfLoad = New Array;
    ErrorsOccuredOnLoad = False;
    
    XSLTSchema = GetTemplate("XSLTValueTatle");
    
    LoadCurrentRates = NOT (ValueIsFilled(ImportBeginOfPeriod) AND ValueIsFilled(ImportEndOfPeriod));
    
    AdresTemplate = "https://www.bnr.ro/%1nbrfxrates%2.xml";
    
    ExchangeDateAddress = StrTemplate(AdresTemplate,?(LoadCurrentRates,"","files/xml/years/"),?(LoadCurrentRates,"",Format(ImportBeginOfPeriod,"DF=yyyy")));
    
    Result = GetFilesFromInternet.DownloadFileAtServer(ExchangeDateAddress);
    
    If NOT Result.Status Then
        
        crtDay = CurrentSessionDate();
    
    		ExplainingMessage= NStr("en='Impossible to get file with exchange currency rates for date %1:
    			|%2
    			|Access to site might be denied, or date is a bank holiday.';
    			|ro='Imposible de preluat fisierul cu datele despre cursul valutar %1:
    			|%2
    			|'Accesul poate fi restrictional sau e zi de odihna.';
    			|ru='Невозможно получить файл данных с курсами валют на %1:
    			|%2
    			|Возможно, нет доступа к веб сайту с курсами валют или нерабочий день.'");
    		ExplainingMessage = StringFunctionsClientServer.SubstituteParametersInString(
    											ExplainingMessage,
    											Format(crtDay, ""),
                                                Result.ErrorInfo);
            OperationStatus = False;
            ErrorsOccuredOnLoad = True;
            
            StateOfLoad.Add(New Structure("Date, OperationStatus, Message", crtDay, OperationStatus, ExplainingMessage));
            
    EndIf;
    
    XSLTransformer = New XSLTransform;
    XSLTransformer.LoadFromString(XSLTSchema.GetText());
    
    XMLRecord = New XMLWriter;
    XMLRecord.SetString();
    
    XSLTransformer.TransformFromFile(Result.Path,XMLRecord);
    
    XMLReader = New XMLReader;
    XMLReader.SetString(XMLRecord.Close());
    
    TableRates = XDTOSerializer.ReadXML(XMLReader);
    
    QueryBuilder = New QueryBuilder;
    QueryBuilder.DataSource = New DataSourceDescription(TableRates);
    
    FilterItem = QueryBuilder.Filter.Add("Period");
    FilterItem.ComparisonType = ComparisonType.IntervalIncludingBounds;
    FilterItem.ValueFrom = ImportBeginOfPeriod;
    FilterItem.ValueTo = ImportEndOfPeriod;
    FilterItem.Use = NOT LoadCurrentRates;
    
    CurrencyArray = New Array;
    
    For Each StructuraItem In Currencies Do
        CurrencyArray.Add(StructuraItem.Currency);
    EndDo;
    
    Map = Common.ObjectsAttributeValue(CurrencyArray,"Description");
    
    FilterItem = QueryBuilder.Filter.Add("CurrencyName");
    FilterItem.ComparisonType = ComparisonType.InList;
    For Each MapItem In Map Do
        FilterItem.Value.Add(MapItem.Value)
    EndDo;
    FilterItem.Use = True;
    
    VTResult = QueryBuilder.Result.Unload();
    
    VTResult.Columns.Add("Company",New TypeDescription("CatalogRef.Companies"));
    VTResult.Columns.Add("Currency",New TypeDescription("CatalogRef.Currencies"));
    
    VTResult.FillValues(Company,"Company");
    
    CurrencyFilter = New Structure("CurrencyName");
    
    For Each MapItem In Map Do
        
        CurrencyFilter.CurrencyName = MapItem.Value;
        
        TabItems = VTResult.FindRows(CurrencyFilter);
        
        For Each TabItem In TabItems Do
            TabItem.Currency = MapItem.Key
        EndDo;
        
    EndDo;
    
    For Each TabItem In VTResult Do
    
    	RM = InformationRegisters.ExchangeRate.CreateRecordManager();
        
        FillPropertyValues(RM,TabItem);
        
        RM.Write()
    
    EndDo;
    
    OperationStatus = True;
    ErrorsOccuredOnLoad = False;
   
    Return StateOfLoad;
	
EndFunction

#EndRegion
