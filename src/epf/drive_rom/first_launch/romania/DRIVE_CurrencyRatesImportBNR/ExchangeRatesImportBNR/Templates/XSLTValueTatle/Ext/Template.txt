﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:x="http://www.bnr.ro/xsd">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<ValueTable xmlns="http://v8.1c.ru/8.1/data/core" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<column>
				<Name xsi:type="xs:string">Period</Name>
				<ValueType>
					<Type>xs:dateTime</Type>
					<DateQualifiers>
						<DateFractions>DateTime</DateFractions>
					</DateQualifiers>
				</ValueType>
			</column>
			<column>
				<Name xsi:type="xs:string">CurrencyName</Name>
				<ValueType>
					<Type>xs:string</Type>
					<StringQualifiers>
						<Length>10</Length>
						<AllowedLength>Variable</AllowedLength>
					</StringQualifiers>
				</ValueType>
			</column>
			<column>
				<Name xsi:type="xs:string">Rate</Name>
				<ValueType>
					<Type>xs:decimal</Type>
					<NumberQualifiers>
						<Digits>10</Digits>
						<FractionDigits>4</FractionDigits>
						<AllowedSign>Any</AllowedSign>
					</NumberQualifiers>
				</ValueType>
			</column>
			<column>
				<Name xsi:type="xs:string">Repetition</Name>
				<ValueType>
					<Type>xs:decimal</Type>
					<NumberQualifiers>
						<Digits>10</Digits>
						<FractionDigits>0</FractionDigits>
						<AllowedSign>Any</AllowedSign>
					</NumberQualifiers>
				</ValueType>
			</column>
			<xsl:for-each select=".//x:Rate">
				<row>
					<Value>
						<xsl:value-of select="concat(parent::node()/@date,'T00:00:00')"/>
					</Value>
					<Value>
						<xsl:value-of select="current()/@currency"/>
					</Value>
					<Value>
						<xsl:value-of select="current()"/>
					</Value>
					<Value>
						<xsl:choose>
							<xsl:when test="current()/@multiplier">
								<xsl:value-of select="current()/@multiplier"/>
							</xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</Value>
				</row>
			</xsl:for-each>
		</ValueTable>
	</xsl:template>
</xsl:stylesheet>
