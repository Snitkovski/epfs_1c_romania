﻿
&AtClient
Procedure Import(Command)
	
	If IsBlankString(ImportText) Then
		CommonClientServer.MessageToUser("Import text is empty");
		Return;
	EndIf;
	
	If NOT ValueIsFilled(Settings) Then
		CommonClientServer.MessageToUser("Settings is empty");
		Return;
	EndIf;
	
	ImportAtServer();
	
EndProcedure

&AtServer
Procedure ImportAtServer()
	
	ResultStructure = New Structure("Done, Errors", True, Undefined);
	
	Obj = FormAttributeToValue("Object");
	Obj.ReadTXTFileToTable(ImportText, BankStatementProc.Import, Settings, ResultStructure);
	
	CommonClientServer.ReportErrorsToUser(ResultStructure.Errors);
	
EndProcedure
