﻿
#Region ProgramInterface

Function ExternalDataProcessorInfo() Export

	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind			= AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindAdditionalDataProcessor();
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= "Beeline";
	RegistrationParameters.Information	= NStr("en = 'Example of SMS provider - Beeline'; ru = 'Пример СМС провайдера - Билайн'");
	RegistrationParameters.SafeMode		= False;
		
	Return RegistrationParameters;
	
EndFunction

#EndRegion

#Region ServiceProgramInterface

// Procedure - Send SMS
//
// Fill this procedure to send SMS
// 
// Parameters:
//  CommandParameters	 - Structure 	 - See method SendingSMS.SendSMS
//  Result				 - Structure	 - Procedure result in structure
//    * SentMessages - Array - structure array:
//      ** RecipientNumber - String.
//      ** MessageID - String.
//    * ErrorDescription - String - user's view of an error,
//                                  if the row is empty, then there is no error.
//
Procedure SendSMS(CommandParameters, Result) Export
	
	Result = New Structure("SentMessages,ErrorDescription", New Array, "");
	
	// Preparation of the recipients string.
	RecipientsString = RecipientsArrayAsString(CommandParameters.RecipientsNumbers);
	
	// Check on filling the mandatory parameters.
	If IsBlankString(RecipientsString) Or IsBlankString(CommandParameters.Text) Then
		Result.ErrorDescription = NStr("en='Invalid message parameters';ru='Неверные параметры сообщения'");
		Return;
	EndIf;
	
	// Preparation of the query parameters.
	QueryParameters = New Structure;
	QueryParameters.Insert("user",		CommandParameters.Username);
	QueryParameters.Insert("pass",		CommandParameters.Password);
	QueryParameters.Insert("gzip",		"none");
	QueryParameters.Insert("action",	"post_sms");
	QueryParameters.Insert("message",	CommandParameters.Text);
	QueryParameters.Insert("target",	RecipientsString);
	QueryParameters.Insert("sender",	CommandParameters.SenderName);
	
	// query sending
	FileNameResponse = ExecuteQuery(QueryParameters);
	If IsBlankString(FileNameResponse) Then
		Result.ErrorDescription = Result.ErrorDescription + NStr("en='Connection failed';ru='Соединение не установлено'");
		Return;
	EndIf;
	
	// Processing of the query result (receiving the message identifiers).
	AnswerStructure = New XMLReader;
	AnswerStructure.OpenFile(FileNameResponse);
	ErrorDescription = "";
	While AnswerStructure.Read() Do
		If AnswerStructure.NodeType = XMLNodeType.StartElement Then
			If AnswerStructure.Name = "sms" Then
				MessageID = "";
				RecipientNumber = "";
				While AnswerStructure.ReadAttribute() Do
					If AnswerStructure.Name = "id" Then
						MessageID = AnswerStructure.Value;
					ElsIf AnswerStructure.Name = "phone" Then
						RecipientNumber = AnswerStructure.Value;
					EndIf;
				EndDo;
				If Not IsBlankString(RecipientNumber) Then
					SentMessage = New Structure("RecipientNumber,MessageID",
														 RecipientNumber,MessageID);
					Result.SentMessages.Add(SentMessage);
				EndIf;
			ElsIf AnswerStructure.Name = "error" Then
				AnswerStructure.Read();
				ErrorDescription = ErrorDescription + AnswerStructure.Value + Chars.LF;
			EndIf;
		EndIf;
	EndDo;
	AnswerStructure.Close();
	DeleteFiles(FileNameResponse);
	
	Result.ErrorDescription = TrimR(ErrorDescription);
		
EndProcedure

// Procedure - Delivery status
//
// Parameters:
//  CommandParameters	 - Structure - See method SendingSMS.DeliveryStatus 
//  Result				 - String - message delivery status which was returned by service provider:
//           "DidNotSend"   - message wasn't processed yet by service provider (in queue);
//           "BeingSent"    - message stands in a queue on sending at provider;
//           "Sent"         - message is sent, confirmation on delivery is expected;
//           "NotSent"      - message is not sent (account is locked, operator's network is overloaded);
//           "Delivered"    - message is delivered to the addressee;
//           "NotDelivered" - failed to deliver the message (subscriber is
//                              not available, delivery confirmation from the subscriber waiting time expired);
//           "Error"        - failed to receive the status from service provider (status is unknown).
//
Procedure DeliveryStatus(CommandParameters, Result) Export
	
	// Preparation of the query parameters.
	QueryParameters = New Structure;
	QueryParameters.Insert("user",		CommandParameters.Username);
	QueryParameters.Insert("pass",		CommandParameters.Password);
	QueryParameters.Insert("gzip",		"none");
	QueryParameters.Insert("action",	"status");
	QueryParameters.Insert("sms_id",	CommandParameters.MessageID);
	
	// query sending
	FileNameResponse = ExecuteQuery(QueryParameters);
	If IsBlankString(FileNameResponse) Then
		Result = "Error";
		Return;
	EndIf;
	
	// Processing of the query result.
	SMSSTS_CODE = "";
	CurrentSMS_ID = "";
	AnswerStructure = New XMLReader;
	AnswerStructure.OpenFile(FileNameResponse);
	While AnswerStructure.Read() Do
		If AnswerStructure.NodeType = XMLNodeType.StartElement Then
			If AnswerStructure.Name = "MESSAGE" Then
				While AnswerStructure.ReadAttribute() Do
					If AnswerStructure.Name = "SMS_ID" Then
						CurrentSMS_ID = AnswerStructure.Value;
					EndIf;
				EndDo;
			ElsIf AnswerStructure.Name = "SMSSTC_CODE" AND CommandParameters.MessageID = CurrentSMS_ID Then
				AnswerStructure.Read();
				SMSSTS_CODE = AnswerStructure.Value;
			EndIf;
		EndIf;
	EndDo;
	AnswerStructure.Close();
	DeleteFiles(FileNameResponse);
	
	Result = SMSDeliveryStatus(SMSSTS_CODE);
	
EndProcedure

Function SMSDeliveryStatus(StatusAsString)
	StatusesCorrespondence = New Map;
	StatusesCorrespondence.Insert("", "HaveNotSent");
	StatusesCorrespondence.Insert("queued", "HaveNotSent");
	StatusesCorrespondence.Insert("wait", "Dispatched");
	StatusesCorrespondence.Insert("accepted", "Sent");
	StatusesCorrespondence.Insert("delivered", "Delivered");
	StatusesCorrespondence.Insert("failed", "NotDelivered");
	
	Result = StatusesCorrespondence[Lower(StatusAsString)];
	Return ?(Result = Undefined, "Error", Result);
EndFunction

// Procedure - Validate SMS settings
//
// Checks the correctness of saved settings of SMS sending.
//
// Parameters:
//  SendingSMSSettings	 - Structure - See method SendingSMS.SMSSendSettingFinished  
//  Result				 - Boolean - Set true if settings are ok
//
Procedure ValidateSMSSettings(SendingSMSSettings, Cancel) Export
	
	Cancel = IsBlankString(SendingSMSSettings.Username) AND IsBlankString(SendingSMSSettings.Password);
	
EndProcedure

Function ExecuteQuery(QueryParameters)
	
	Result = "";
	
	QueryFileName = GenerateFileForPOSTQuery(QueryParameters);
	FileNameResponse = GetTempFileName("xml");
	
	// generating the header
	Title = New Map;
	Title.Insert("Content-Type", "application/x-www-form-urlencoded");
	Title.Insert("Content-Length", XMLString(FileSize(QueryFileName)));
	
	// Sending query and receiving response.
	Try
		Join = New HTTPConnection("beeline.amega-inform.en", , , , GetFilesFromInternetClientServer.GetProxy("http"), 60);
		Join.Post(QueryFileName, "/sendsms/", FileNameResponse, Title);
		Result = FileNameResponse;
	Except
		WriteLogEvent(
			NStr("en='Send SMS';ru='Отправка SMS'", CommonClientServer.DefaultLanguageCode()),
			EventLogLevel.Error,
			,
			,
			DetailErrorDescription(ErrorInfo()));
	EndTry;
	
	DeleteFiles(QueryFileName);
	
	Return Result;
	
EndFunction
	
Function GenerateFileForPOSTQuery(QueryParameters)
	QueryString = "";
	For Each Parameter IN QueryParameters Do
		If Not IsBlankString(QueryString) Then
			QueryString = QueryString + "&";
		EndIf;
		QueryString = QueryString + Parameter.Key + "=" + EncodeString(Parameter.Value, StringEncodingMethod.URLEncoding);
	EndDo;
	
	QueryFileName = GetTempFileName("txt");
	
	QueryFile = New TextWriter(QueryFileName, TextEncoding.ANSI);
	QueryFile.Write(QueryString);
	QueryFile.Close();
	
	Return QueryFileName;
EndFunction

Function FileSize(FileName)
    File = New File(FileName);
    Return File.Size();
EndFunction

Function RecipientsArrayAsString(Array)
	Result = "";
	For Each Item IN Array Do
		Number = FormatNumber(Item);
		If Not IsBlankString(Number) Then
			If Not IsBlankString(Result) Then
				Result = Result + ",";
			EndIf;
			Result = Result + Number;
		EndIf;
	EndDo;
	Return Result;
EndFunction

Function FormatNumber(Number)
	Result = "";
	AllowedChars = "+1234567890";
	For Position = 1 To StrLen(Number) Do
		Char = Mid(Number,Position,1);
		If Find(AllowedChars, Char) > 0 Then
			Result = Result + Char;
		EndIf;
	EndDo;
	Return Result;
EndFunction

#EndRegion
