﻿Function ExternalDataProcessorInfo() Export
	
	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo();
	
	RegistrationParameters.Kind			= "AdditionalDataProcessor";
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= "Import bank transactions";
	RegistrationParameters.Information	= NStr("en = 'Data processor / bank transactions import'; ru = 'Внешняя обработка / импорт банковских транзакций'");
	RegistrationParameters.SafeMode		= False;
	RegistrationParameters.SSLVersion	= "2.2.5.19";
	
	AddCommand(RegistrationParameters.Commands, "Import bank transactions", "ImportBankTransactions", "OpeningForm", True, "");
	
	Return RegistrationParameters;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowAlert = False, Modifier = "")
	
	NewCommand = CommandTable.Add();
	NewCommand.Presentation  = Presentation;
	NewCommand.ID            = ID;
	NewCommand.Use           = Use;
	NewCommand.ShowNotification = ShowAlert;
	NewCommand.Modifier      = Modifier;
	
EndProcedure
