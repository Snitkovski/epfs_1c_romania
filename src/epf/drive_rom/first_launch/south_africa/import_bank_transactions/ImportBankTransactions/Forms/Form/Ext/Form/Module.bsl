﻿
&AtClient
Procedure BankTransactionsUnacceptedBeforeDeleteRow(Item, Cancel)
	Cancel = True;
EndProcedure

&AtClient
Procedure FilenameStartChoice(Item, ChoiceData, StandardProcessing)
	StandardProcessing = False;
	Dialog = New FileDialog(FileDialogMode.Open);
	Dialog.Title = "Select file for import";
	Dialog.FullFileName = "";
	Dialog.Filter = "(*.ofx)|*.ofx";
	Dialog.Multiselect = False;
	Dialog.Directory = "С:\";
	If Dialog.Choose() Then
		Object.Filename = Dialog.FullFileName;
	EndIf;
EndProcedure

&AtClient
Procedure UploadFromFile(Command)
	
	If Not ValueIsFilled(Object.BankAccount) Then
		CommonClientServer.MessageToUser("Please, fill in bank account",, "BankAccount");
		Return;
	EndIf;
	
	Notify = New NotifyDescription("FileUpload", ThisForm);

	BeginPutFile(Notify, "", "*.qif; *.qbo; *.qfx; *.ofx; *.csv; *.iif", True, ThisForm.UUID);
			
EndProcedure

&AtClient
Procedure FileUpload(Result, Address, SelectedFileName, AdditionalParameters) Export

	If ValueIsFilled(Address) Then
		OFX_UploadTransactionsAtServer(Address);
	EndIf;

EndProcedure

&AtServer
Procedure OFX_UploadTransactionsAtServer(TempStorageAddress)
	
	BinaryData = GetFromTempStorage(TempStorageAddress);
	TempFileName = GetTempFileName("ofx");
	BinaryData.Write(TempFileName);
	
	Try
		SourceText.Read(TempFileName);
	Except
		TextMessage = NStr("en = 'Can not read the file.'");
		CommonClientServer.MessageToUser(TextMessage);
		Return;
	EndTry;

	Object.BankTransactionsUnaccepted.Clear();
	LineCountTotal = SourceText.LineCount();
	
	NewSTMTTRN        = False;
	NumberTransaction = 0;
	
	For LineNumber = 1 To LineCountTotal Do
		
		CurrentLine = SourceText.GetLine(LineNumber);
		CurrentLine = TrimAll(CurrentLine);
		
		//<STMTTRN>
		If Not NewSTMTTRN And Find(CurrentLine, "<STMTTRN>") > 0 Then
			NumberTransaction = NumberTransaction + 1;
			NewSTMTTRN        = True;
			NewRow            = Object.BankTransactionsUnaccepted.Add();
		EndIf;
		
		//<DTPOSTED>
		If NewSTMTTRN And Find(CurrentLine, "<DTPOSTED>") > 0 Then
			CurrentLine   = StrReplace(CurrentLine, "</DTPOSTED>", "");
			StartPosition = Find(CurrentLine, "<DTPOSTED>") + 10;
			Year  = Mid(CurrentLine, StartPosition, 4);
			Month = Mid(CurrentLine, StartPosition + 4, 2);
			Day   = Mid(CurrentLine, StartPosition + 4+ 2, 2);
			
			NewRow.TransactionDate = Date(Year, Month, Day);
		EndIf;
		
		//<TRNAMT>
		If NewSTMTTRN And Find(CurrentLine, "<TRNAMT>") > 0 Then
			CurrentLine   = StrReplace(CurrentLine, "</TRNAMT>", "");
			StartPosition = Find(CurrentLine, "<TRNAMT>") + 8;
			CountOfCharacters = StrLen(CurrentLine) - StartPosition + 1;
			Amount = Number(Mid(CurrentLine, StartPosition, CountOfCharacters));
			NewRow.Amount = Amount;
			If Amount > 0 Then
				NewRow.OperationKind = Enums.OperationKindsPaymentReceipt.FromCustomer;
			ElsIf Amount < 0 Then
				NewRow.OperationKind = Enums.OperationKindsPaymentExpense.Vendor;
			EndIf;
			
		EndIf;
		
		//<CHECKNUM>
		If NewSTMTTRN And Find(CurrentLine, "<CHECKNUM>") > 0 Then
			CurrentLine   = StrReplace(CurrentLine, "</CHECKNUM>", "");
			StartPosition = Find(CurrentLine, "<CHECKNUM>") + 10;
			CountOfCharacters = StrLen(CurrentLine) - StartPosition + 1;
			
			NewRow.CheckNumber = Mid(CurrentLine, StartPosition, CountOfCharacters);
		// Rise { Popov N 2017-04-17
		//<FITID>
		ElsIf NewSTMTTRN And Find(CurrentLine, "<FITID>") > 0 Then
			CurrentLine   = StrReplace(CurrentLine, "</FITID>", "");
			StartPosition = Find(CurrentLine, "<FITID>") + 7;
			CountOfCharacters = StrLen(CurrentLine) - StartPosition + 1;
			
			NewRow.CheckNumber = Mid(CurrentLine, StartPosition, CountOfCharacters);
		// Rise } Popov N 2017-04-17
		EndIf;
		
		//<NAME>
		If NewSTMTTRN And Find(CurrentLine, "<NAME>") > 0 Then
			CurrentLine   = StrReplace(CurrentLine, "</NAME>", "");
			StartPosition = Find(CurrentLine, "<NAME>") + 6;
			CountOfCharacters = StrLen(CurrentLine) - StartPosition + 1;
			
			NewRow.Description = Mid(CurrentLine, StartPosition, CountOfCharacters);
			
		// Rise { Popov N 2017-04-17
		//<MEMO>
		ElsIf NewSTMTTRN And Find(CurrentLine, "<MEMO>") > 0 Then
			CurrentLine   = StrReplace(CurrentLine, "</MEMO>", "");
			StartPosition = Find(CurrentLine, "<MEMO>") + 6;
			CountOfCharacters = StrLen(CurrentLine) - StartPosition + 1;
			
			NewRow.Description = Mid(CurrentLine, StartPosition, CountOfCharacters);
		// Rise } Popov N 2017-04-17
		EndIf;
	
		//</STMTTRN>
		If NewSTMTTRN And Find(CurrentLine, "</STMTTRN>") > 0 Then
			NewSTMTTRN = False;
			
			//NewRow.BankAccount = AccountInBank;
			//NewRow.Hide 	   = "Hide";
			
			RecordTransaction(NewRow, NumberTransaction);
			
		EndIf;
				
	EndDo;
	
	//
	Object.BankTransactionsUnaccepted.Sort("TransactionDate DESC, Description, Counterparty, TransactionID");
	
	CommonClientServer.MessageToUser(NStr("en = 'The uploading of bank transactions is complete!'"));

EndProcedure

&AtServer
Procedure RecordTransaction(NewRow, NumberTransaction)
	
	If (Not ValueIsFilled(NewRow.TransactionDate))
		OR (NewRow.TransactionDate < Object.ProcessingPeriod.StartDate)
		OR (NewRow.TransactionDate > Object.ProcessingPeriod.EndDate) Then
		
		Object.BankTransactionsUnaccepted.Delete(NewRow);
		
		TextMessage = StringFunctionsClientServer.SubstituteParametersInString(NStr("en = 'The bank transaction #%1 (%2 %3) does not belong to the processing period (%4)!'"), NumberTransaction, NewRow.Description, NewRow.Amount, Format(NewRow.TransactionDate, "DLF=D"));
		CommonClientServer.MessageToUser(TextMessage);
		
	Else
		//Try to match an uploaded transaction with an existing document
		DocumentFound = FindAnExistingDocument(NewRow.Description, NewRow.Amount, Object.BankAccount, NewRow.TransactionDate, NewRow.CheckNumber);
		If DocumentFound <> Undefined Then
			NewRow.Document = DocumentFound;
		EndIf;
		
	EndIf;
	
EndProcedure

Function FindAnExistingDocument(Description, DocumentAmount, BankAccount, IncomingDocumentDate, IncomingDocumentNumber)
	
	If DocumentAmount > 0 Then
		DocType = "PaymentReceipt"
	ElsIf DocumentAmount < 0 Then
		DocType = "PaymentExpense"
	Else
		Return Undefined;
	EndIf;
	
	PaymentQuery = New Query;
	PaymentQuery.Text =
		"SELECT
		|	PaymentDocument.Ref
		|FROM
		|	Document." + DocType + " AS PaymentDocument
		|WHERE
		|	PaymentDocument.DocumentAmount = &DocumentAmount
		|	AND PaymentDocument.IncomingDocumentDate = &IncomingDocumentDate
		|	AND PaymentDocument.IncomingDocumentNumber = &IncomingDocumentNumber
		|	AND PaymentDocument.BankAccount = &BankAccount
		|	AND PaymentDocument.Comment LIKE &Comment
		|	";
	
	PaymentQuery.SetParameter("BankAccount", BankAccount);
	PaymentQuery.SetParameter("Comment", "%" + Description + "%");
	PaymentQuery.SetParameter("DocumentAmount", ABSLocal(DocumentAmount));
	PaymentQuery.SetParameter("IncomingDocumentDate", IncomingDocumentDate);
	PaymentQuery.SetParameter("IncomingDocumentNumber", IncomingDocumentNumber);
	
	QueryResult = PaymentQuery.Execute();
	
	SelectionDetailRecords = QueryResult.Select();
	
	If SelectionDetailRecords.Next() Then
		Return SelectionDetailRecords.Ref;
	Else
		Return Undefined;
	EndIf;
	 	
EndFunction

&AtServer
Procedure RecordAcceptedTransactionsAtServer()
	
	//BeginTransaction();
	For Each Row In Object.BankTransactionsUnaccepted Do
		If Row.Accept Then
			If Not ValueIsFilled(Row.Document) Then
				// IN the IB the document is not found, it is required to create the new one.
				If Row.Amount > 0 Then
					ObjectOfDocument = Documents.PaymentReceipt.CreateDocument();
				ElsIf Row.Amount < 0 Then
					ObjectOfDocument = Documents.PaymentExpense.CreateDocument();
				EndIf;
				IsNewDocument = True;
			Else
				// IN the IB the document is found, it is required to get its object.
				ObjectOfDocument = Row.Document.GetObject();
				IsNewDocument = False;
			EndIf;
				              
			// We fill all document attributes.
			DocumentType = ObjectOfDocument.Metadata().Name;
			If DocumentType = "PaymentExpense" Then
				FillAttributesPaymentExpense(ObjectOfDocument, Row, IsNewDocument);
			ElsIf DocumentType = "PaymentReceipt" Then
				FillAttributesPaymentReceipt(ObjectOfDocument, Row, IsNewDocument);
			EndIf;
				
			If ObjectOfDocument.DeletionMark Then
				SetMarkToDelete(ObjectOfDocument, False);
			EndIf;
				
			WriteObject(ObjectOfDocument, Row, IsNewDocument);
			
			If Not ObjectOfDocument.IsNew() Then
				If Not ValueIsFilled(Row.Document) Then
					Row.Document = ObjectOfDocument.Ref;
				EndIf;
			EndIf;
			
		EndIf;
	EndDo;
	//CommitTransaction();
EndProcedure

&AtClient
Procedure RecordAcceptedTransactions(Command)
	RecordAcceptedTransactionsAtServer();
EndProcedure

// Procedure sets a property.
//
Procedure SetProperty(Object, PropertyName, PropertyValue, RequiredReplacementOfOldValues = False, IsNewDocument)
	
	If PropertyValue <> Undefined AND ValueIsFilled(PropertyValue)
	   AND Object[PropertyName] <> PropertyValue Then
		If IsNewDocument
		 OR (NOT ValueIsFilled(Object[PropertyName])
		 OR RequiredReplacementOfOldValues)
		 OR TypeOf(Object[PropertyName]) = Type("Boolean")
		 OR TypeOf(Object[PropertyName]) = Type("Date") Then
			Object[PropertyName] = PropertyValue;
		EndIf
	EndIf;
	
EndProcedure // SetProperty()

// Procedure fills the PaymentExpense document attributes.
//
Procedure FillAttributesPaymentExpense(ObjectOfDocument, SourceData, IsNewDocument)
	
	// Filling out a document header.
	SetProperty(
		ObjectOfDocument,
		"Date",
		SourceData.TransactionDate,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"OperationKind",
		SourceData.OperationKind,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"Company",
		Object.BankAccount.Owner,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"BankAccount",
		Object.BankAccount,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"CashCurrency",
		Object.BankAccount.CashCurrency,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"Item",
		SourceData.CFItem,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"DocumentAmount",
		ABSLocal(SourceData.Amount),
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"IncomingDocumentNumber",
		SourceData.CheckNumber,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"IncomingDocumentDate",
		SourceData.TransactionDate,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"Comment",
		SourceData.Description,
		,
		IsNewDocument
	);
	
	If IsNewDocument Then
		ObjectOfDocument.SetNewNumber();
		If SourceData.OperationKind = Enums.OperationKindsPaymentExpense.ToCustomer Then
			ObjectOfDocument.VATTaxation = DriveServer.VATTaxation(Object.BankAccount.Owner, , SourceData.TransactionDate);
		Else
			ObjectOfDocument.VATTaxation = Enums.VATTaxationTypes.SubjectToVAT;
		EndIf;
	EndIf;
	
	// Filling document tabular section.
	If SourceData.OperationKind = Enums.OperationKindsPaymentExpense.Vendor
	 OR SourceData.OperationKind = Enums.OperationKindsPaymentExpense.ToCustomer Then
		
		//If TypeOf(SourceData.CounterpartyAccount) <> Type("String") Then
		//	SetProperty(
		//		ObjectOfDocument,
		//		"CounterpartyAccount",
		//		SourceData.CounterpartyAccount,
		//		,
		//		IsNewDocument
		//	);
		//EndIf;
			
		SetProperty(
			ObjectOfDocument,
			"Counterparty",
			SourceData.Counterparty,
			True,
			IsNewDocument
		);
		
		If ObjectOfDocument.PaymentDetails.Count() = 0 Then
			RowOfDetails = ObjectOfDocument.PaymentDetails.Add();
		Else
			RowOfDetails = ObjectOfDocument.PaymentDetails[0];
		EndIf;
		
		OneRowInDecipheringPayment = ObjectOfDocument.PaymentDetails.Count() = 1;
		
		SetProperty(
			RowOfDetails,
			"Contract",
			?(SourceData.Contract = "Not found", Undefined, SourceData.Contract),
			True,
			IsNewDocument
		);
		
		//SetProperty(
		//	RowOfDetails,
		//	"AdvanceFlag",
		//	SourceData.AdvanceFlag,
		//	True,
		//	IsNewDocument
		//);
	
		If IsNewDocument
		 OR OneRowInDecipheringPayment
		   AND RowOfDetails.PaymentAmount <> ObjectOfDocument.DocumentAmount Then
		
			RowOfDetails.PaymentAmount = ObjectOfDocument.DocumentAmount;
			DateOfFilling = ObjectOfDocument.Date;
			SettlementsCurrency = RowOfDetails.Contract.SettlementsCurrency;
			
			CalculateRateAndAmountOfAccounts(
				RowOfDetails,
				SettlementsCurrency,
				DateOfFilling,
				ObjectOfDocument,
				IsNewDocument
			);
			
			If RowOfDetails.ExchangeRate = 0 Then
				
				SetProperty(
					RowOfDetails,
					"ExchangeRate",
					1,
					,
					IsNewDocument
				);
				
				SetProperty(
					RowOfDetails,
					"Multiplicity",
					1,
					,
					IsNewDocument
				);
				
				SetProperty(
					RowOfDetails,
					"SettlementsAmount",
					RowOfDetails.PaymentAmount,
					,
					IsNewDocument
				);
				
			EndIf;
			
			If ObjectOfDocument.VATTaxation = Enums.VATTaxationTypes.SubjectToVAT Then
				
				DefaultVATRate = ObjectOfDocument.Company.DefaultVATRate;
				VATRateValue = DriveReUse.GetVATRateValue(DefaultVATRate);
				
				RowOfDetails.VATRate = DefaultVATRate;
				RowOfDetails.VATAmount = RowOfDetails.PaymentAmount
					- (RowOfDetails.PaymentAmount)
					/ ((VATRateValue + 100) / 100);
				
			Else
				
				If ObjectOfDocument.VATTaxation = Enums.VATTaxationTypes.NotSubjectToVAT Then
					DefaultVATRate = DriveReUse.GetVATRateWithoutVAT();
				Else
					DefaultVATRate = DriveReUse.GetVATRateZero();
				EndIf;
				
				RowOfDetails.VATRate = DefaultVATRate;
				RowOfDetails.VATAmount = 0;
				
			EndIf;
			
		EndIf;
		
	EndIf;
	
EndProcedure // FillAttributesPaymentExpense()

// Procedure fills the PaymentReceipt document attributes.
//
Procedure FillAttributesPaymentReceipt(ObjectOfDocument, SourceData, IsNewDocument)
	
	SetProperty(
		ObjectOfDocument,
		"Date",
		SourceData.TransactionDate,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"OperationKind",
		SourceData.OperationKind,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"Company",
		Object.BankAccount.Owner,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"BankAccount",
		Object.BankAccount,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"CashCurrency",
		Object.BankAccount.CashCurrency,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"Item",
		SourceData.CFItem,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"DocumentAmount",
		SourceData.Amount,
		True,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"IncomingDocumentNumber",
		SourceData.CheckNumber,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"IncomingDocumentDate",
		SourceData.TransactionDate,
		,
		IsNewDocument
	);
	
	SetProperty(
		ObjectOfDocument,
		"Comment",
		SourceData.Description,
		,
		IsNewDocument
	);
	
	If IsNewDocument Then
		ObjectOfDocument.SetNewNumber();
		If ObjectOfDocument.OperationKind = Enums.OperationKindsPaymentReceipt.FromCustomer Then
			ObjectOfDocument.VATTaxation = DriveServer.VATTaxation(Object.BankAccount.Owner, , SourceData.TransactionDate);
		Else
			ObjectOfDocument.VATTaxation = Enums.VATTaxationTypes.SubjectToVAT;
		EndIf;
	EndIf;
	
	// Filling document tabular section.
	If SourceData.OperationKind = Enums.OperationKindsPaymentReceipt.FromCustomer
	 OR SourceData.OperationKind = Enums.OperationKindsPaymentReceipt.FromVendor Then
	 
		//If TypeOf(SourceData.CounterpartyAccount) <> Type("String") Then
		//	SetProperty(
		//		ObjectOfDocument,
		//		"CounterpartyAccount",
		//		SourceData.CounterpartyAccount,
		//		,
		//		IsNewDocument
		//	);
		//EndIf;
		
		SetProperty(
			ObjectOfDocument,
			"Counterparty",
			SourceData.Counterparty,
			True,
			IsNewDocument
		);
		
		If ObjectOfDocument.PaymentDetails.Count() = 0 Then
			RowOfDetails = ObjectOfDocument.PaymentDetails.Add();
		Else
			RowOfDetails = ObjectOfDocument.PaymentDetails[0];
		EndIf;
		
		OneRowInDecipheringPayment = ObjectOfDocument.PaymentDetails.Count() = 1;
		
		SetProperty(
			RowOfDetails,
			"Contract",
			?(SourceData.Contract = "Not found", Undefined, SourceData.Contract),
			True,
			IsNewDocument
		);
		
		//SetProperty(
		//	RowOfDetails,
		//	"AdvanceFlag",
		//	SourceData.AdvanceFlag,
		//	True,
		//	IsNewDocument
		//);
		
		// Filling document tabular section.
		If IsNewDocument
		 OR OneRowInDecipheringPayment
		   AND RowOfDetails.PaymentAmount <> ObjectOfDocument.DocumentAmount Then
			
			RowOfDetails.PaymentAmount = ObjectOfDocument.DocumentAmount;
			DateOfFilling = ObjectOfDocument.Date;
			SettlementsCurrency = RowOfDetails.Contract.SettlementsCurrency;
			
			CalculateRateAndAmountOfAccounts(
				RowOfDetails,
				SettlementsCurrency,
				DateOfFilling,
				ObjectOfDocument,
				IsNewDocument
			);
			
			If RowOfDetails.ExchangeRate = 0 Then
				
				SetProperty(
					RowOfDetails,
					"ExchangeRate",
					1,
					,
					IsNewDocument
				);
				
				SetProperty(
					RowOfDetails,
					"Multiplicity",
					1,
					,
					IsNewDocument
				);
				
				SetProperty(
					RowOfDetails,
					"SettlementsAmount",
					RowOfDetails.PaymentAmount,
					,
					IsNewDocument
				);
				
			EndIf;
			
			If ObjectOfDocument.VATTaxation = Enums.VATTaxationTypes.SubjectToVAT Then
				
				DefaultVATRate = ObjectOfDocument.Company.DefaultVATRate;
				VATRateValue = DriveReUse.GetVATRateValue(DefaultVATRate);
				
				RowOfDetails.VATRate = DefaultVATRate;
				RowOfDetails.VATAmount = RowOfDetails.PaymentAmount
					- (RowOfDetails.PaymentAmount)
					/ ((VATRateValue + 100) / 100);
				
			Else
				
				If ObjectOfDocument.VATTaxation = Enums.VATTaxationTypes.NotSubjectToVAT Then
					DefaultVATRate = DriveReUse.GetVATRateWithoutVAT();
				Else
					DefaultVATRate = DriveReUse.GetVATRateZero();
				EndIf;
				
				RowOfDetails.VATRate = DefaultVATRate;
				RowOfDetails.VATAmount = 0;
				
			EndIf;
			
		EndIf;
		
	EndIf;
	
EndProcedure // FillAttributesPaymentReceipt()

// Procedure sets the deletion mark.
//
Procedure SetMarkToDelete(ObjectForMark, Check)
	
	NameObject = GetObjectPresentation(ObjectForMark);
	NameOfAction = ?(Check, NStr("en=' Marked for deletion';ru=' помечен на удаление'"), NStr("en=' deletion mark was cleared';ru=' отменена пометка на удаление'"));
	Try
		ObjectForMark.Write(DocumentWriteMode.Write);
		ObjectForMark.SetDeletionMark(Check);
		MessageText = NStr("en='%ObjectNameLeft%%NameObjectMid%: %NameOfAction%.';ru='%НазваниеОбъектаЛев% %НазваниеОбъектаСред%: %НазваниеДействия%.'");
		MessageText = StrReplace(MessageText, "%ObjectNameLeft%", Upper(Left(NameObject, 1)));
		MessageText = StrReplace(MessageText, "%NameObjectMid%", Mid(NameObject, 2));
		MessageText = StrReplace(MessageText, "%NameOfAction%", NameOfAction);
		DriveServer.ShowMessageAboutError(ThisObject, MessageText);
	Except
		MessageText = NStr("en='%ObjectNameLeft%%NameObjectMid%: not %NameOfAction%. Errors occurred while writing.';ru='%НазваниеОбъектаЛев% %НазваниеОбъектаСред%: не %НазваниеДействия%! Произошли ошибки при записи!'");
		MessageText = StrReplace(MessageText, "%ObjectNameLeft%", Upper(Left(NameObject, 1)));
		MessageText = StrReplace(MessageText, "%NameObjectMid%", Mid(NameObject, 2));
		MessageText = StrReplace(MessageText, "%NameOfAction%", NameOfAction);
		DriveServer.ShowMessageAboutError(ThisObject, MessageText);
	EndTry
	
EndProcedure // SetMarkToDelete()

// Procedure writes the object.
//
Procedure WriteObject(ObjectToWrite, SectionRow, IsNewDocument)
	
	DocumentType = ObjectToWrite.Metadata().Name;
	If DocumentType = "PaymentExpense" Then
		DocumentName = "Payment expense";
		If Object.FillDebtsAutomatically AND SectionRow.OperationKind = Enums.OperationKindsPaymentExpense.Vendor Then
			DriveServer.FillPaymentDetailsExpense(ObjectToWrite,,,,, SectionRow.Contract);
		EndIf;
	ElsIf DocumentType = "PaymentReceipt" Then
		DocumentName = "Payment receipt";
		If Object.FillDebtsAutomatically AND SectionRow.OperationKind = Enums.OperationKindsPaymentReceipt.FromCustomer Then
			DriveServer.FillPaymentDetailsReceipt(ObjectToWrite,,,,, SectionRow.Contract);
		EndIf;
	EndIf;
	//SetProperty(
	//	ObjectToWrite,
	//	"PaymentDestination",
	//	SectionRow.PaymentDestination,
	//	,
	//	IsNewDocument
	//);
	SetProperty(
		ObjectToWrite,
		"Author",
		Users.CurrentUser(),
		True,
		IsNewDocument
	);
	ObjectModified = ObjectToWrite.Modified();
	ObjectPosted = ObjectToWrite.Posted;
	NameObject = GetObjectPresentation(ObjectToWrite);
	
	If ObjectModified Then
		Try
			If ObjectPosted Then
				ObjectToWrite.Write(DocumentWriteMode.UndoPosting);
				SectionRow.Posted = ObjectToWrite.Posted;
			Else
				ObjectToWrite.Write(DocumentWriteMode.Write);
			EndIf;
			//( elmi Lost in translation
			//MessageText = NStr("en='%Status% %ObjectName%.';ru='%Статус% %НазваниеОбъекта%.'");
			MessageText = NStr("en='%NameObject% was %Status%.';ru='%Status% %NameObject%.'");
			//) elmi
			MessageText = StrReplace(MessageText, "%Status%" , ?(IsNewDocument, NStr("en='created';ru='Создан'"), NStr("en='overwritten';ru='Перезаписан'")));
			MessageText = StrReplace(MessageText, "%NameObject%", NameObject);
			DriveServer.ShowMessageAboutError(ThisObject, MessageText);
		Except
			//( elmi Lost in translation
			//MessageText = NStr("en='%ObjectNameLeft%%NameObjectMid% %Status%! Errors occurred while writing!';ru='%НазваниеОбъектаЛев% %НазваниеОбъектаСред% %Статус%! Произошли ошибки при записи!'");
			MessageText = NStr("en='%ObjectNameLeft%%NameObjectMid% was %Status%. Errors occurred while writing.';ru='%ObjectNameLeft%%NameObjectMid% %Status%! Произошли ошибки при записи!'");
			//) elmi
			MessageText = StrReplace(MessageText, "%ObjectNameLeft%", Upper(Left(NameObject, 1)));
			MessageText = StrReplace(MessageText, "%NameObjectMid%", Mid(NameObject, 2));
			MessageText = StrReplace(MessageText, "%Status%", ?(ObjectToWrite.IsNew(), NStr("en='not created';ru=' не создан'"), NStr("en='not written';ru=' не записан'")));
			DriveServer.ShowMessageAboutError(ThisObject, MessageText);
			Return;
		EndTry;
	Else
		//( elmi Lost in translation
		//MessageText = NStr("en='%NameObject% already exists. Perhaps, import has been previously performed.';ru='Уже существует %НазваниеОбъекта%. Возможно загрузка производилась ранее.'");
		MessageText = NStr("en='%NameObject% already exists. Perhaps, import has been previously performed.';ru='Уже существует %NameObject%. Возможно загрузка производилась ранее.'");
		//) elmi
		MessageText = StrReplace(MessageText, "%NameObject%", NameObject);
		DriveServer.ShowMessageAboutError(ThisObject, MessageText);
	EndIf;
	
	If Object.PostImported AND (ObjectModified OR Not ObjectPosted) Then
		Try
			ObjectToWrite.Write(DocumentWriteMode.Posting);
			//( elmi Lost in translation
			//MessageText = NStr("en='%Status% %ObjectName% %Status%';ru='%Статус% %НазваниеОбъекта% %Статус%'");
			MessageText = NStr("en='%NameObject% was %Status%.';ru='%Status% %NameObject% %Status%'");
			//) elmi
			MessageText = StrReplace(MessageText, "%Status%", ?(ObjectPosted, NStr("en='reposted';ru='Перепроведен '"), NStr("en='posted';ru='posted'")));
			MessageText = StrReplace(MessageText, "%NameObject%", NameObject);
			DriveServer.ShowMessageAboutError(ThisObject, MessageText);
			SectionRow.Posted = ObjectToWrite.Posted;
		Except
			//( elmi Lost in translation
			//MessageText = NStr("en='%ObjectNameLeft%%NameObjectMid% is failed! Errors occurred while posting!';ru='%НазваниеОбъектаЛев% %НазваниеОбъектаСред% не проведен! Произошли ошибки при проведении!'");
			MessageText = NStr("en='%ObjectNameLeft%%NameObjectMid% was not posted. Errors occurred while posting.';ru='%ObjectNameLeft%%NameObjectMid% не проведен! Произошли ошибки при проведении!'");
			//) elmi
			MessageText = StrReplace(MessageText, "%ObjectNameLeft%", Upper(Left(NameObject, 1)));
			MessageText = StrReplace(MessageText, "%NameObjectMid%", Mid(NameObject, 2));
			DriveServer.ShowMessageAboutError(ThisObject, MessageText);
		EndTry
	EndIf;
	
EndProcedure // WriteObject()

// Procedure calculates the rate and amount of document.
//
Procedure CalculateRateAndAmountOfAccounts(StringPayment, SettlementsCurrency, ExchangeRateDate, ObjectOfDocument, IsNewDocument)
	
	StructureRateCalculations = GetCurrencyRate(SettlementsCurrency, ExchangeRateDate);
	StructureRateCalculations.ExchangeRate = ?(StructureRateCalculations.ExchangeRate = 0, 1, StructureRateCalculations.ExchangeRate);
	StructureRateCalculations.Multiplicity = ?(StructureRateCalculations.Multiplicity = 0, 1, StructureRateCalculations.Multiplicity);
	
	SetProperty(
		StringPayment,
		"ExchangeRate",
		StructureRateCalculations.ExchangeRate,
		,
		IsNewDocument
	);
	SetProperty(
		StringPayment,
		"Multiplicity",
		StructureRateCalculations.Multiplicity,
		,
		IsNewDocument
	);
	DocumentRateStructure = GetCurrencyRate(ObjectOfDocument.CashCurrency, ExchangeRateDate);
	
	SettlementsAmount = RecalculateFromCurrencyToCurrency(
		StringPayment.PaymentAmount,
		DocumentRateStructure.ExchangeRate,
		StructureRateCalculations.ExchangeRate,
		DocumentRateStructure.Multiplicity,
		StructureRateCalculations.Multiplicity
	);
	
	SetProperty(
		StringPayment,
		"SettlementsAmount",
		SettlementsAmount,
		True,
		IsNewDocument
	);
	
EndProcedure // CalculateSettlementsRateAndAmount()

// Function gets the object presentation.
//
Function GetObjectPresentation(Object)
	
	If TypeOf(Object) = Type("DocumentObject.PaymentReceipt") Then
		NameObject = NStr("en='The ""Payment receipt"" document # %Number% dated %Date%';ru='документ ""Поступление на счет"" № %Number% от %Date%'"
		);
		NameObject = StrReplace(NameObject, "%Number%", String(TrimAll(Object.Number)));
		NameObject = StrReplace(NameObject, "%Date%", String(Object.Date));
	ElsIf TypeOf(Object) = Type("DocumentObject.PaymentExpense") Then
		NameObject = NStr("en='The ""Payment expense"" document # %Number% dated %Date%';ru='документ ""Расход со счета"" № %Number% от %Date%'"
		);
		NameObject = StrReplace(NameObject, "%Number%", String(TrimAll(Object.Number)));
		NameObject = StrReplace(NameObject, "%Date%", String(Object.Date));
	Else
		NameObject = NStr("en='object';ru='объект'");
	EndIf;
	
	Return NameObject;
	
EndFunction // GetObjectPresentation()

// Function recalculates the amount from one currency to another
//
// Parameters:      
// Amount         - Number - amount that should be recalculated.
// 	InitRate       - Number - currency rate from which you should recalculate.
// 	FinRate       - Number - currency rate to which you should recalculate.
// 	RepetitionBeg  - Number - multiplicity from which you
// should recalculate (by default = 1).
// 	RepetitionEnd  - Number - multiplicity in which
// it is required to recalculate (by default =1)
//
// Returns: 
//  Number - amount recalculated to another currency.
//
Function RecalculateFromCurrencyToCurrency(Amount, InitRate, FinRate,	RepetitionBeg = 1, RepetitionEnd = 1) Export
	
	If (InitRate = FinRate) AND (RepetitionBeg = RepetitionEnd) Then
		Return Amount;
	EndIf;
	
	If InitRate = 0
	 OR FinRate = 0
	 OR RepetitionBeg = 0
	 OR RepetitionEnd = 0 Then
		MessageText = NStr("en='Null exchange rate has been found. Recalculation is not executed.';ru='Обнаружен нулевой курс валюты. Пересчет не выполнен.'");
		DriveServer.ShowMessageAboutError(ThisObject, MessageText);
		Return Amount;
	EndIf;
	
	RecalculatedSumm = Round((Amount * InitRate * RepetitionEnd) / (FinRate * RepetitionBeg), 2);
	
	Return RecalculatedSumm;
	
EndFunction // RecalculateFromCurrencyToCurrency()

// Returns exchange rate for a date.
//
Function GetCurrencyRate(Currency, ExchangeRateDate)
	
	Structure = InformationRegisters.ExchangeRates.GetLast(ExchangeRateDate, New Structure("Currency", Currency));
	
	Return Structure;
	
EndFunction // GetCurrencyRate()

&AtClient
Procedure BankAccountClearing(Item, StandardProcessing)
	Object.Currency = Undefined;
EndProcedure

&AtServerNoContext
Function GetBankAccountData(BankAccount)
	
	StructureData = New Structure;
			
	StructureData.Insert(
		"CashCurrency",
		BankAccount.CashCurrency
	);
	
	StructureData.Insert(
		"Company",
		BankAccount.Owner
	);
	
	Return StructureData;
	
EndFunction

&AtClient
Procedure BankAccountOnChange(Item)
	
	StructureData = GetBankAccountData(Object.BankAccount);
	Object.Company = StructureData.Company;
	Object.Currency = StructureData.CashCurrency;
	
EndProcedure

Function ABSLocal(Value)
	Return ?(Value < 0, -1, 1) * Value;
EndFunction
