﻿
Function ExternalDataProcessorInfo() Export
	
	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo();
	
	//RegistrationParameters.Type = AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindCurrencyRatesImportProcessor();
	RegistrationParameters.Kind			 = "AdditionalDataProcessor";
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= "Import exchange rates";
	RegistrationParameters.Information	= NStr("en = 'Data processor / currency exchange rates import'; ru = 'Внешняя обработка / импорт курса валют'");
	RegistrationParameters.SafeMode		= False;
	RegistrationParameters.SSLVersion	= "2.2.5.19";
	
	Definition	= NStr("en='Import exchange rates from the file.';ru='Загрузка курсов валют из файла.'");
	
	AddCommand(RegistrationParameters.Commands, "Import exchange rates", "ImportExchangeRates", "OpeningForm", True, "");
	
	Return RegistrationParameters;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowAlert = False, Modifier = "")
	
	NewCommand = CommandTable.Add();
	NewCommand.Presentation  = Presentation;
	NewCommand.ID            = ID;
	NewCommand.Use           = Use;
	NewCommand.ShowNotification = ShowAlert;
	NewCommand.Modifier      = Modifier;
	
EndProcedure
