﻿&AtServer
Function ReadFile(TempStorageAddress)
	//File = TrimAll(PathToFile);
	//FileUpload = New File(File);
	//If FileUpload.Exist() = False Then
	//	MessageText = NStr("en = 'File %File% does not exist!'");
	//	MessageText = StrReplace(MessageText, "%File%", File);
	//	CommonUseClientServer.MessageToUser(MessageText, ,,, );
	//	Return Undefined;
	//EndIf;
	//
	//Try
	//	TextDoc.Read(File);
	//Except
	//	MessageText = NStr("en = 'File can not be read.'");
	//	CommonUseClientServer.MessageToUser(MessageText, ,,, );
	//	Return Undefined;
	//EndTry;
	//
	//
	//TextDoc.Read(File,TextEncoding.OEM);
	//
	//If TextDoc.LineCount() < 5 Then
	//	MessageText = NStr("en = 'The file is empty!'");
	//	CommonUseClientServer.MessageToUser(MessageText, ,,, );
	//	Return Undefined;
	//EndIf;
	//
	//If Find(TrimAll(TextDoc.GetLine(1)),"Description,Description2") = 0 Then
	//	
	//	MessageText = NStr("en = 'Specified file is not file of exchange rates downloaded from resbank.co.za!'");
	//	CommonUseClientServer.MessageToUser(MessageText, ,,, );
	//	Return Undefined;
	//EndIf;
	BinaryData = GetFromTempStorage(TempStorageAddress);
	TempFileName = GetTempFileName("csv");
	BinaryData.Write(TempFileName);
	TextDoc.Read(TempFileName);

	Return TextDoc.GetText();
	
EndFunction // ReadFile()

&AtServer
Procedure LoadRatesAtServer()
	If Currency.IsEmpty()then
		MessageText = NStr("en = 'Cannot find currency.Please, choose it.'");
		CommonClientServer.MessageToUser(MessageText, ,,, );
		Return;
	EndIf ;
	RecordManager = InformationRegisters.ExchangeRates.CreateRecordManager();
	For each Line In ExchangeDetails  Do
		RecordManager.Currency =Currency;
		RecordManager.Period = Line.ValueDate;
		
		RecordManager.Read();
		If RecordManager.Selected() Then
			
			RecordManager.ExchangeRate = ?(PerRand,1/Line.TheValue,Line.TheValue);
			
		Else
			
			RecordManager.Currency   = Currency;
			RecordManager.Period =Line.ValueDate;
			RecordManager.ExchangeRate = ?(PerRand,1/Line.TheValue,Line.TheValue);
			RecordManager.Multiplicity = 1;
		EndIf;
		RecordManager.Write();
	EndDo;
	
EndProcedure

&AtServer
Function FindCurrency(String)
	Num = Find(String,"per");
	If Num > 0 Then
		NameOfCurrency = Mid(String,Num+4);
		FindedCurrency = Catalogs.Currencies.FindByAttribute("DescriptionFull", NameOfCurrency);
	EndIf;
	If FindedCurrency = Catalogs.Currencies.FindByDescription("Rand") Then
		 
	NameOfCurrency = TrimAll(Mid(String,1,Num-1));
	FindedCurrency = Catalogs.Currencies.FindByAttribute("LongDescription",NameOfCurrency );
	PerRand = True;
	EndIf;
	Return FindedCurrency;
EndFunction

&AtClient
Procedure LoadRates(Command)
//Rand per US Dollar
	CurrencyStr = TextDoc.GetLine((2));
	ArrayOfCurrencyStr = StringFunctionsClientServer.SplitStringIntoWordArray(CurrencyStr, ",");

	If 	Currency <>  FindCurrency(ArrayOfCurrencyStr[0]) Then
		MessageText = NStr("en = 'The file currency and selected currency are different!'");
		CommonClientServer.MessageToUser(MessageText, ,,, );
		Return;

	EndIf;
	
	//---
	
	LoadRatesAtServer();
EndProcedure

&AtClient
Procedure DownloadFromFile()
	
	For LineNumber =5 to TextDoc.LineCount()  Do
		CurrentString = TextDoc.GetLine(LineNumber);
		If IsBlankString(CurrentString) Then
			Break;
		EndIf;
		ArrayOfStr = StringFunctionsClientServer.SplitStringIntoWordArray(CurrentString, ",");
		Newline = ExchangeDetails.Add();
		
		StringDate = ArrayOfStr[0];
		StringDate = StrReplace(StringDate,"-","");
		Newline.ValueDate   =Date(StringDate);
		Newline.TheValue = ArrayOfStr[1];
		UserInterruptProcessing();
		
	EndDo;
	
EndProcedure

&AtClient
Procedure FileUpload(Result, Address, SelectedFileName, AdditionalParameters) Export
		
		 ExchangeDetails.Clear();
	If (Find(Upper(SelectedFileName), ".CSV") = 0) And (Find(Upper(SelectedFileName), ".TXT") = 0) Then
		ShowMessageBox(, "Please upload a valid CSV file (.csv, .txt)");
		return;
	EndIf;
	File = SelectedFileName;
	If ReadFile(Address)= Undefined Then
		Currency = Undefined;
		ExchangeDetails.Clear();
		Return;
	EndIf;
	
	//Rand per US Dollar
	CurrencyStr = TextDoc.GetLine((2));
	ArrayOfCurrencyStr = StringFunctionsClientServer.SplitStringIntoWordArray(CurrencyStr, ",");

	If 	Currency <>  FindCurrency(ArrayOfCurrencyStr[0]) Then
		MessageText = NStr("en = 'The file currency and selected currency are different!'");
		CommonClientServer.MessageToUser(MessageText, ,,, );
		Return;

	EndIf;
	
	If Currency.IsEmpty()then
		MessageText = NStr("en = 'Cannot find currency.Please, choose it.'");
		CommonClientServer.MessageToUser(MessageText, ,,, );
		Return;
		
	EndIf ;

	Items.ExchangeDetails.Visible = true;
	DownloadFromFile();
EndProcedure

&AtClient
Procedure ChooseFile(Command)
	Notify = New NotifyDescription("FileUpload",ThisForm);

	BeginPutFile(Notify, "", "*.csv", True, ThisForm.UUID);
EndProcedure

&AtClient
Procedure LinkClick(Item)
	GotoURL("https://www.resbank.co.za/Research/Rates/Pages/SelectedHistoricalExchangeAndInterestRates.aspx"); 
EndProcedure

&AtClient
Procedure FileStartChoice(Item, ChoiceData, StandardProcessing)
	Notify = New NotifyDescription("FileUpload",ThisForm);

	BeginPutFile(Notify, "", "*.csv", True, ThisForm.UUID);

EndProcedure

