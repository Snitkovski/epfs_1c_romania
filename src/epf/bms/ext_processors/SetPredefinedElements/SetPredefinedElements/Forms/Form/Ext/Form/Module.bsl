﻿
&AtServer
Procedure SetPredefElemAtServer()

	myObjRef = AttrRef.GetObject();
	myObjRef.PredefinedDataName = PredefElemName;
	//myObjRef.Predefined = True;
	
	Try
		myObjRef.Write();
		Message("All Right!");
	Except
		Message("Error! Did not set Predefined Data Name!");
	EndTry;
	
EndProcedure

&AtClient
Procedure SetPredefElem(Command)
	SetPredefElemAtServer();
EndProcedure
