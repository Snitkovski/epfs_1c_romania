﻿/////////////////////////////////// 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.Quote");

	RegistrationParametrs.Insert("Presentation", DestinationArray);
	RegistrationParametrs.Insert("Description", "Proforma MDT (la doc. Quotation)");
	RegistrationParametrs.Insert("Version", "1.3.1.5"); 	// "1.0"
	RegistrationParametrs.Insert("SafeMode", False); 	// Variants: True, False / Варианты: Истина, Ложь 
	RegistrationParametrs.Insert("Information", "Proforma - la doc. Quotation");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
				"Proforma MD-T",		// what we will see under button PRINT
				"FacturaProformaEmisa",	// Name of Template 
				//"CallOfServerMethod",  	// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				"ServerMethodCall",  		// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				False,					// Показывать оповещение. Варианты Истина, Ложь / Variants: True, False
				"MXLPrint");           	// "MXLPrint" = for MXL / "" = for WORD !!! Модификатор 
	
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand	= CommandTable.Add();
	NewCommand.Presentation 	= Presentation;
	NewCommand.ID				= ID;
	NewCommand.Use				= Use;
	NewCommand.ShowNotification	= ShowNotification;
	NewCommand.Modifier			= Modifier;
	
EndProcedure

/////////////////////////////////// 
// Preparing of Print Form 
Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "FacturaProformaEmisa";			///PrintFormsCollection[0].DesignName;
	Except
		Message(NStr("en='TemplateName is empty!';
					  |ro='TemplateName este gol!';
					  |ru='TemplateName не заполнено!'"));
		Return;
	EndTry;
	TemplateSynonim = "Factura proforma emisa " + ObjectArray[0].Number +
						" din data " + Format(ObjectArray[0].Date, "DF=yyyy-MM-dd");
	PrintManagement.OutputSpreadsheetDocumentToCollection(
				PrintFormsCollection,
				TemplateName,  												// Template Name
				TemplateSynonim,   											// Template Synonim
				CreatePrintForm(ObjectArray, PrintObjects, TemplateName)  	// Function for Execution (in this Module) - исполняющая функция (в этом же модуле)
				);
	
EndProcedure

Function CreatePrintForm(ObjectArray, PrintObjects, TemplateName)

	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_Quote";  	// PrintParameters_ + Name_of_Document
	
	Template	= ThisObject.GetTemplate(TemplateName);
	Query		= New Query;
	
	Query.Text	=
	      "SELECT
	      |	Quote.Company,
	      |	Quote.Counterparty,
	      |	Quote.Number AS Number,
	      |	Quote.Date AS DocDate,
	      |	Quote.Author AS Author,
	      |	Quote.Contract,
		  //|	Quote.Contract.CustomerPaymentDueDate AS PaymentDueDate,
	      |	Quote.Counterparty.Code AS CodeC,
	      |	Quote.Responsible,
	      |	Quote.Counterparty.ContactPerson AS ContactPerson,
	      |	Quote.ExchangeRate AS Rate,
	      |	Quote.DocumentCurrency,
	      |	Quote.Inventory.(
	      |		LineNumber,
	      |		Products,
	      |		Characteristic,
	      |		Quantity,
	      |		MeasurementUnit,
	      |		Price,
	      |		DiscountMarkupPercent,
	      |		Amount,
	      |		VATRate,
	      |		VATAmount,
	      |		Total
	      |	),
	      |	Quote.Company.TIN,
	      |	Quote.Company.BankAccountByDefault.Bank AS Bank,
	      |	Quote.Company.BankAccountByDefault.AccountNo AS BankAccount,
	      |	Quote.Counterparty.TIN,
	      |	Quote.Counterparty.BankAccountByDefault.Bank AS BankC,
	      |	Quote.Counterparty.BankAccountByDefault.AccountNo AS BankAccountC,
	      |	Quote.AmountIncludesVAT AS IncludeVAT,
	      |	Quote.DocumentAmount AS DAmount
		  //|	Quote.BasisDocument
	      |FROM
	      |	Document.Quote AS Quote
	      |WHERE
	      |	Quote.Ref IN(&Ref)";
		  
	Query.Parameters.Insert("Ref", ObjectArray);
	Selection			= Query.Execute().Choose();
	
	AreaCaption			= Template.GetArea("Caption");
	Header				= Template.GetArea("Header");
	AreaInventoryHeader = Template.GetArea("InventoryHeader");
	AreaInventory		= Template.GetArea("Line");
	Footer				= Template.GetArea("Footer");
	Spreadsheet.Clear();
	
	InsertPageBreak = False;
	
	While Selection.Next() Do
		If InsertPageBreak Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;
	
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////
		
		//InfoAboutCompany      =  SmallBusinessServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.DocDate, ,);
		//InfoAboutCounterparty =  SmallBusinessServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, Selection.DocDate, ,);
		InfoAboutCompany      =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.DocDate, ,);
		InfoAboutCounterparty =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, Selection.DocDate, ,);
		
		Header.Parameters["Company"] 		= InfoAboutCompany.FullDescr;
		Header.Parameters["Counterparty"] 	= InfoAboutCounterparty.FullDescr;
	
		Header.Parameters["KPP"] 		= InfoAboutCompany.TIN;
		Header.Parameters["KPPC"] 		= InfoAboutCounterparty.TIN;
		
   		Header.Parameters["RegNumber"]  = InfoAboutCompany.RegistrationNumber;
		Header.Parameters["RegNumberC"] = InfoAboutCounterparty.RegistrationNumber;

		Header.Parameters["Address"]	= InfoAboutCompany.LegalAddress;
		Header.Parameters["AddressC"] 	= InfoAboutCounterparty.LegalAddress;

		Rate = Selection.Rate;
		
		Try
			Header.Parameters["NrComanda"] 		= "Nr. " + Selection.BasisDocument.Number + " " +  Format(Selection.BasisDocument.Date, "DF=""dd.MM.yyyy""");
		Except
			Header.Parameters["NrComanda"] 		= "Nr. ";
		EndTry;
		
		SelectionInventory	= Selection.Inventory.Choose();
		Spreadsheet.Put(AreaCaption);
		
		Header.Parameters.Fill(Selection);
			
		Try
			Header.Parameters["CurrencyRate"]	= Rate;
		Except
			
		EndTry;
				
		Header.Parameters["Number"]	= Selection.Number;
		Header.Parameters["Date"]	= Format(Selection.DocDate,"DF=""dd.MM.yyyy""");
		
		//AdditionalAttributes = PropertiesManagement.GetValuesOfProperties(Selection.Company);
		AdditionalAttributes = PropertyManager.GetValuesProperties(Selection.Company);
		For Each Attr in AdditionalAttributes Do
			If StrFind(TrimAll(Attr.Property), "Capital Social") > 0 Then
				Header.Parameters["Capital"] = Attr.Value;
			EndIf;
		EndDo;

	///////////////////////////////////////////////////////////////////////////////////////////
	// Data scadenta - este limita de data in care Clientul trebuie sa achite Proforma  =  sapte zile calend.
		//If ValueIsFilled(Selection.PaymentDueDate) Then
		//	Header.Parameters["DataLimita"] = Format(Selection.DocDate + (Selection.PaymentDueDate * 86400), "DF=""dd.MM.yyyy""");
		//Else 			
			Header.Parameters["DataLimita"] = Format(Selection.DocDate + (7 * 86400), "DF=""dd.MM.yyyy""");
		//EndIf;
	///////////////////////////////////////////////////////////////////////////////////////////
		
		While SelectionInventory.Next() Do
			Try
				Header.Parameters["VATRate"] = SelectionInventory.VATRate;
			Except
			EndTry;
		EndDo;
		Spreadsheet.Put(Header, Selection.Level());
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////END////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////
		//06 july 18
		AreDiscounts = Selection.Inventory.Unload().Total("DiscountMarkupPercent") <> 0;

		If AreDiscounts Then
			AreaInventoryHeader = Template.GetArea("InventoryHeaderWithDiscount");
			AreaInventory		= Template.GetArea("LineWithDiscount");
		EndIf;
		//
		Spreadsheet.Put(AreaInventoryHeader);
		SelectionInventory			= Selection.Inventory.Choose();
		//SelectionPaymentCalendar	= Selection.PaymentCalendar.Choose();
		TotalVAT	 = 0;
		TotalFaraTVA = 0;
		RowsNubmerInDoc = 35;
		While SelectionInventory.Next() Do
				
			TotalFaraTVA  =  TotalFaraTVA	+ SelectionInventory.Amount;
			TotalVAT      =  TotalVAT 	    + SelectionInventory.VATAmount;
			
			AreaInventory.Parameters["LineNumber"]		= SelectionInventory.LineNumber;
			AreaInventory.Parameters["Item"]			= SelectionInventory.Products;
			AreaInventory.Parameters["UnitOfMeasure"]	= SelectionInventory.MeasurementUnit;
			AreaInventory.Parameters["Quantity"]		= SelectionInventory.Quantity;
			
			PriceInLine	  		= SelectionInventory.Price;
			AmountInLine	 	= SelectionInventory.Amount;                                                // With Discount  
			DiscountRateInLine	= SelectionInventory.DiscountMarkupPercent;
			AmountWODiscount	= 100*AmountInLine/(100-DiscountRateInLine);
			
			DiscountOnPriceValue= Round(PriceInLine * DiscountRateInLine / 100, 2);
			PriceDiscounted     = PriceInLine - DiscountOnPriceValue;
			DiscountAmount		= Round(PriceInLine * DiscountRateInLine / 100 * SelectionInventory.Quantity, 2);
			
			If Selection.IncludeVAT	Then
			    PriceInLine    = PriceInLine - PriceInLine * SelectionInventory.VATRate.Rate / (100 + SelectionInventory.VATRate.Rate); 
				AmountWODiscount	= AmountWODiscount - AmountWODiscount * SelectionInventory.VATRate.Rate / (100 + SelectionInventory.VATRate.Rate);
				AmountInLine	 	= AmountInLine - SelectionInventory.VATAmount;
				DiscountAmount		= DiscountAmount - DiscountAmount * SelectionInventory.VATRate.Rate / (100 + SelectionInventory.VATRate.Rate);
			EndIf;
			
			AreaInventory.Parameters["Price"]	= Format(PriceInLine,"NFD=2");
			AreaInventory.Parameters["Amount"]	= Format(AmountInLine,"NFD=2");
			
			// 06 july 18
			If AreDiscounts Then
				AreaInventory.Parameters["DiscountAmount"]	= Format(DiscountAmount,"NFD=2");
				AreaInventory.Parameters["AmountWODiscount"]	= Format(AmountWODiscount,"NFD=2");
			EndIf;
			//
			
			//Price	  = Format(Round(SelectionInventory.Price * Rate,2),				"NFD=2");
			//Amount	  = Format(Round(Price	* SelectionInventory.Quantity, 2),			"NFD=2");
			//VATAmount = Format(Round(Amount	* SelectionInventory.VATRate.Rate /100, 2),	"NFD=2");

			//RataReducere = SelectionInventory.DiscountMarkupPercent;	
			//Price1       = Price - (Price / 100 * RataReducere);
			//Reducere     = Price / 100 * RataReducere;
			//VATAmount    = Format(Round(Amount	* SelectionInventory.VATRate.Rate /100, 2), "NFD=2");
			//Price2       = Price1 * SelectionInventory.VATRate.Rate / (100 + SelectionInventory.VATRate.Rate);
			//Amount1      = Amount - (Reducere * SelectionInventory.Quantity);
			//If Selection.IncludeVAT = True   Then     
			//	AreaInventory.Parameters["Price"] = Format(Round((Price1 - Price2), 2), "NFD=2");
			//Else		
			//	AreaInventory.Parameters["Price"] = Price - (Price / 100 * RataReducere);
			//EndIf;
			//
			//If Selection.IncludeVAT = True    Then     
			//	AreaInventory.Parameters["Amount"]	= Amount1 - SelectionInventory.VATAmount;
			//Else		
			//	AreaInventory.Parameters["Amount"]  = Amount - (Reducere * SelectionInventory.Quantity);
			//EndIf;
			
			AreaInventory.Parameters["VATAmount"] 	= Format(Round(SelectionInventory.VATAmount * Rate, 2), "NFD=2");
				
			i = SelectionInventory.LineNumber;
					
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
			If StrLen(TrimAll(SelectionInventory.Products)) > 49 Then
				RowsNubmerInDoc = RowsNubmerInDoc - 1;
			EndIf;
		EndDo;
		
		For i = i + 1 To RowsNubmerInDoc Do
			AreaInventory.Parameters["LineNumber"]		= i;
			AreaInventory.Parameters["Item"]			= Undefined;
			AreaInventory.Parameters["UnitOfMeasure"]	= Undefined;
			AreaInventory.Parameters["Quantity"]		= Undefined;
			AreaInventory.Parameters["Price"]			= Undefined;
			AreaInventory.Parameters["Amount"]			= Undefined;
			
			Try
				AreaInventory.Parameters["VATAmount"]	= Undefined;
			Except
					
			EndTry;
			// 06 july 18
			If AreDiscounts Then
				AreaInventory.Parameters["DiscountAmount"]	= Undefined;
				AreaInventory.Parameters["AmountWODiscount"]	= Undefined;
			EndIf;
			
			//				
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////START//////////////////////////////////////////////////////////////////
		Query		= New Query;
		Query.Text	=
		"SELECT
		|	UserEmployees.Employee,
		|	UserEmployees.User
		|FROM
		|	InformationRegister.UserEmployees AS UserEmployees
		|WHERE
		|	UserEmployees.User = &Author";
		
		Query.SetParameter("Author", Selection.Author);
			
		Result		= Query.Execute();
		SelectionD	= Result.Choose();
			
		While SelectionD.Next() Do
			Try
				Footer.Parameters["CNPU"]	= SelectionD.Employee.Ind.PersonalCode;
				Footer.Parameters["CIU"]	= SelectionD.Employee.Ind.IDCard;
			Except
					
			EndTry;
		EndDo;

		Footer.Parameters.Fill(Selection);
		//Footer.Parameters["Date"]		= Format(Selection.DocDate,"DF=""dd.MM.yyyy""");
		//Footer.Parameters["Driver"]			 = Selection.Driver;
		//Footer.Parameters["CI"]				 = Selection.DriverCode;
		//Footer.Parameters["MijlocDeTransport"] = Selection.Vehicle;
		//Footer.Parameters["CNP"]				 = Selection.Driver.PersonalCode;
		Footer.Parameters["User"]			 = Selection.Author;

///////////////////////////////////////////////////Totals///////////////////////////////////////////////
///////////////////////////////////////////////////Start/////////////////////////////////////////////
		Footer.Parameters["TotalTVA"]     = Format(Round(TotalVAT     * Rate, 2), "NFD=2");
		Footer.Parameters["TotalCuTVA"]	  = Format((Selection.DAmount * Rate),    "NFD=2");
		
		Footer.Parameters["TotalDePlataCurr"] = "Total de plată " + Selection.DocumentCurrency;
		
		If Selection.IncludeVAT = False Then
			Footer.Parameters["TotalFaraTVA"] = Format(Round(TotalFaraTVA * Rate, 2), "NFD=2");
		Else
			Footer.Parameters["TotalFaraTVA"] = Format(Round((Selection.DAmount - TotalVAT) * Rate, 2), "NFD=2");
		EndIf;
///////////////////////////////////////////////////Totals///////////////////////////////////////////////
////////////////////////////////////////////////////End////////////////////////////////////////////	
	  
		Spreadsheet.Put(Footer);
	
//////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////END///////////////////////////////////////////////////////////////////
	
		InsertPageBreak = True;
	
	EndDo;
	
	Return Spreadsheet;

EndFunction

