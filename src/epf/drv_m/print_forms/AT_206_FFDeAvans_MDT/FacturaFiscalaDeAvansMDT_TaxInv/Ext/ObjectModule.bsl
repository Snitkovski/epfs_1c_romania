﻿/////////////////////////////////// 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.TaxInvoiceIssued");

	RegistrationParametrs.Insert("Destination", DestinationArray);
	RegistrationParametrs.Insert("Description", "FF de Avans MDT (la doc.TaxInvIssued)");
	RegistrationParametrs.Insert("Version", "1.3.1.5"); 	// "1.0"
	RegistrationParametrs.Insert("SafeMode", False); 	// Variants: True, False / Варианты: Истина, Ложь 
	RegistrationParametrs.Insert("Information", "FF de Avans - la doc.TaxInvIssued");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
			"FF de Avans MD-T",				// what we will see under button PRINT
			"FacturaFiscalaDeVinzare",   	// Name of Template 
			//"CallOfServerMethod",  	// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
			"ServerMethodCall",  		// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
			False,							// Показывать оповещение. Варианты Истина, Ложь / Variants: True, False
			"MXLPrint");           			// "MXLPrint" = for MXL / "" = for WORD !!! Модификатор 
	
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand	= CommandTable.Add();
	NewCommand.Presentation 	= Presentation;
	NewCommand.ID				= ID;
	NewCommand.Use				= Use;
	NewCommand.ShowNotification	= ShowNotification;
	NewCommand.Modifier			= Modifier;
	
EndProcedure

/////////////////////////////////// 
// Preparing of Print Form 
Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "FacturaFiscalaDeVinzare";						///PrintFormsCollection[0].DesignName;
	Except
		Message(NStr("en='TemplateName is empty';
					  |ro='TemplateName este gol';
					  |ru='TemplateName не заполнено'"));
		Return;
	EndTry;
	TemplateSynonim = "Factura fiscala " + ObjectArray[0].Number + " din data " + Format(ObjectArray[0].Date, "DF=yyyy-MM-dd");
	PrintManagement.OutputSpreadsheetDocumentToCollection(
			PrintFormsCollection,
			TemplateName,  												// Template Name
			TemplateSynonim,   											// Template Synonim
			CreatePrintForm(ObjectArray, PrintObjects, TemplateName)  	// Function for Execution (in this Module) - исполняющая функция (в этом же модуле)
			);
	
EndProcedure

Function CreatePrintForm(ObjectArray, PrintObjects, TemplateName) Export

	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_TaxInvoiceIssued";  // PrintParameters_ + Name_of_Document
	
	Template	= ThisObject.GetTemplate(TemplateName);
	Query		= New Query;
	
	Query.Text =
		"SELECT ALLOWED
		|	TaxInvoiceIssuedBasisDocuments.Ref AS TaxInvoiceIssuedRef,
		|	TaxInvoiceIssuedBasisDocuments.BasisDocument AS BasisDocument
		|INTO BasisForTax
		|FROM
		|	Document.TaxInvoiceIssued.BasisDocuments AS TaxInvoiceIssuedBasisDocuments
		|WHERE
		|	TaxInvoiceIssuedBasisDocuments.Ref.Ref IN(&Ref)
		|	AND TaxInvoiceIssuedBasisDocuments.Ref.OperationKind = VALUE(Enum.OperationTypesTaxInvoiceIssued.AdvancePayment)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|SELECT ALLOWED
		|	BasisForTax.TaxInvoiceIssuedRef AS TaxInvoiceIssuedRef,
		|	BasisForTax.BasisDocument AS BasisDocument
		|INTO AdvancedPaymentBasis
		|FROM
		|	BasisForTax AS BasisForTax
		|		INNER JOIN Document.CashReceipt.PaymentDetails AS CashReceiptPaymentDetails
		|		ON BasisForTax.BasisDocument = CashReceiptPaymentDetails.Ref
		|			AND (CashReceiptPaymentDetails.AdvanceFlag = TRUE)
		|
		|GROUP BY
		|	BasisForTax.BasisDocument,
		|	BasisForTax.TaxInvoiceIssuedRef
		|
		|UNION ALL
		|
		|SELECT
		|	BasisForTax.TaxInvoiceIssuedRef,
		|	BasisForTax.BasisDocument
		|FROM
		|	BasisForTax AS BasisForTax
		|		INNER JOIN Document.PaymentReceipt.PaymentDetails AS PaymentReceiptPaymentDetails
		|		ON BasisForTax.BasisDocument = PaymentReceiptPaymentDetails.Ref
		|			AND (PaymentReceiptPaymentDetails.AdvanceFlag = TRUE)
		|
		|GROUP BY
		|	BasisForTax.BasisDocument,
		|	BasisForTax.TaxInvoiceIssuedRef
		|
		|UNION ALL
		|
		|SELECT
		|	BasisForTax.TaxInvoiceIssuedRef,
		|	BasisForTax.BasisDocument
		|FROM
		|	BasisForTax AS BasisForTax
		|		INNER JOIN Document.CreditNote.AmountAllocation AS CreditNoteAmountAllocation
		|		ON BasisForTax.BasisDocument = CreditNoteAmountAllocation.Ref
		|			AND (CreditNoteAmountAllocation.AdvanceFlag = TRUE)
		|
		|GROUP BY
		|	BasisForTax.BasisDocument,
		|	BasisForTax.TaxInvoiceIssuedRef
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|SELECT ALLOWED
		|	TaxInvoiceIssued.Ref AS Ref,
		|	TaxInvoiceIssued.Company AS Company,
		|	TaxInvoiceIssued.Counterparty AS Counterparty,
		|	TaxInvoiceIssued.Number AS DocNumber,
		|	TaxInvoiceIssued.Date AS DocDate,
		|	TaxInvoiceIssued.Counterparty.Code AS CodeC,
		|	TaxInvoiceIssued.Responsible AS Responsible,
		|	TaxInvoiceIssued.Counterparty.ContactPerson AS ContactPerson,
		|	TaxInvoiceIssued.Currency AS DocumentCurrency,
		|	TaxInvoiceIssued.Company.TIN AS CompanyTIN,
		|	TaxInvoiceIssued.Company.BankAccountByDefault.Bank AS Bank,
		|	TaxInvoiceIssued.Company.BankAccountByDefault.AccountNo AS BankAccount,
		|	TaxInvoiceIssued.Counterparty.TIN AS CounterpartyTIN,
		|	TaxInvoiceIssued.Counterparty.BankAccountByDefault.Bank AS BankC,
		|	TaxInvoiceIssued.Counterparty.BankAccountByDefault.AccountNo AS BankAccountC
		|FROM
		|	(SELECT
		|		AdvancedPaymentBasis.TaxInvoiceIssuedRef AS TaxInvoiceIssuedRef
		|	FROM
		|		AdvancedPaymentBasis AS AdvancedPaymentBasis
		|
		|	GROUP BY
		|		AdvancedPaymentBasis.TaxInvoiceIssuedRef) AS OnlyTaxInvIss
		|		INNER JOIN Document.TaxInvoiceIssued AS TaxInvoiceIssued
		|		ON OnlyTaxInvIss.TaxInvoiceIssuedRef = TaxInvoiceIssued.Ref
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|SELECT ALLOWED
		|	AdvancedPaymentBasis.TaxInvoiceIssuedRef AS TaxInvoiceIssuedRef,
		|	AdvancedPaymentBasis.BasisDocument AS BasisDocument,
		|	TaxInvoiceIssuedBasisDocuments.BasisDocument.Number AS BasisDocumentNumber,
		|	TaxInvoiceIssuedBasisDocuments.BasisDocument.Date AS BasisDocumentDate,
		|	TaxInvoiceIssuedBasisDocuments.LineNumber AS LineNumber,
		|	TaxInvoiceIssuedBasisDocuments.Amount AS AmountCuTVA,
		|	TaxInvoiceIssuedBasisDocuments.VATAmount AS AmountTVA,
		|	TaxInvoiceIssuedBasisDocuments.BasisDocument.BasisDocument AS BasisDocumentOfBasisDocument
		|FROM
		|	AdvancedPaymentBasis AS AdvancedPaymentBasis
		|		INNER JOIN Document.TaxInvoiceIssued.BasisDocuments AS TaxInvoiceIssuedBasisDocuments
		|		ON AdvancedPaymentBasis.TaxInvoiceIssuedRef = TaxInvoiceIssuedBasisDocuments.Ref
		|			AND AdvancedPaymentBasis.BasisDocument = TaxInvoiceIssuedBasisDocuments.BasisDocument";
		  
	Query.Parameters.Insert("Ref", ObjectArray);
	
	QueryResult      	= Query.ExecuteBatch();
	DocHeader         	= QueryResult.Get(2).Choose(); // doc Header
	DocBasisDocuments 	= QueryResult.Get(3).Unload(); // doc tabular "BasisDocuments"
	
	AreaCaption			= Template.GetArea("Caption");
	Header				= Template.GetArea("Header");
	Footer				= Template.GetArea("Footer");
	Spreadsheet.Clear();
	
	InsertPageBreak = False;
	While DocHeader.Next() Do          // per Documents
		If InsertPageBreak Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;
	
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////
		
		Spreadsheet.Put(AreaCaption);
	
		InfoAboutCompany =  DriveServer.InfoAboutLegalEntityIndividual(DocHeader.Company, DocHeader.DocDate, ,);
		InfoAboutCounterparty =  DriveServer.InfoAboutLegalEntityIndividual(DocHeader.Counterparty, DocHeader.DocDate, ,);
		
		Header.Parameters.Company = InfoAboutCompany.FullDescr;
		Header.Parameters.Counterparty = InfoAboutCounterparty.FullDescr;
	
		Header.Parameters.KPP = InfoAboutCompany.TIN;
		Header.Parameters.KPPC = InfoAboutCounterparty.TIN;
		
		Header.Parameters.RegistrationNumber = InfoAboutCompany.RegistrationNumber;
		Header.Parameters.RegistrationNumberC = InfoAboutCounterparty.RegistrationNumber;
	
		Header.Parameters.Address = InfoAboutCompany.LegalAddress;
		Header.Parameters.AddressC = InfoAboutCounterparty.LegalAddress;

		Header.Parameters.Fill(DocHeader);
		
		//Header.Parameters.OrderParams =   // Impossible to fill in Header - 
											// can we have many (for ex.) SalesOrders for each BasisDocument-Of-BasisDocument
		
		//CurrencyRateInDoc = InformationRegisters.ExchangeRates.GetLast(DocHeader.Ref.Date, 
		//											New Structure("Currency", DocHeader.DocumentCurrency)).ExchangeRate;
		CurrencyRateInDoc = InformationRegisters.ExchangeRate.GetLast(DocHeader.Ref.Date,
													New Structure("Currency", DocHeader.DocumentCurrency)).Rate;
		
		Header.Parameters.Currency = DocHeader.DocumentCurrency;
		Header.Parameters.CurrencyRate = CurrencyRateInDoc;
				
		Header.Parameters.Number = DocHeader.DocNumber;
		Header.Parameters.Date = Format(DocHeader.DocDate, "DF=""dd.MM.yyyy""");
													
		//AddAttr = PropertiesManagement.GetValuesOfProperties(DocHeader.Company);
		AdditionalAttributes = PropertyManager.GetValuesProperties(DocHeader.Company);
		For Each Attr in AdditionalAttributes Do
			If StrFind(TrimAll(Attr.Property), "Capital Social") > 0 Then
				Header.Parameters.Capital = String(Attr.Value) + " " + "RON";
			EndIf;
		EndDo;
		
		Spreadsheet.Put(Header, DocHeader.Level());
		
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////END////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////

		AreaInventoryHeader = Template.GetArea("InventoryHeader");
		Spreadsheet.Put(AreaInventoryHeader);
		
		TabularBasisDocuments = DocBasisDocuments.FindRows(New Structure("TaxInvoiceIssuedRef", DocHeader.Ref));
		
		TotalCuTVA      = 0;
		TotalFaraTVA 	= 0;
		TotalTVA	 	= 0;
		RowsNumberInDoc	= 35;
		
		AreaInventory = Template.GetArea("Line");
		
		////////////////////  per Lines in current Document
		For each LineWithAdvance In TabularBasisDocuments Do
			
			TotalCuTVA      = TotalCuTVA + LineWithAdvance.AmountCuTVA;
			TotalFaraTVA	= TotalFaraTVA + (LineWithAdvance.AmountCuTVA - LineWithAdvance.AmountTVA);
			TotalTVA		= TotalTVA + LineWithAdvance.AmountTVA;
			
			AreaInventory.Parameters.LineNumber			= LineWithAdvance.LineNumber;
			
		/////////////////////////////////////////////////////////////////////////////////////////////			
			// variant 1: AreaInventory.Parameters.Document			= LineWithAdvance.BasisDocument;		
			
			// variant 2: AreaInventory.Parameters.Document			= "Avans (" +
			//												LineWithAdvance.BasisDocument.Metadata().Synonym + 
			//												" din " +
			//												// LineWithAdvance.BasisDocumentNumber  + " " +
			//												Format(LineWithAdvance.BasisDocumentDate, "DF=""dd.MM.yyyy""") + ")";
														
			// variant 3: 
			//AreaInventory.Parameters.Document			= "Avans (" + LineWithAdvance.BasisDocumentOfBasisDocument;
															
			// variant 4: 
			AreaInventory.Parameters.Document	= "Avans";
		/////////////////////////////////////////////////////////////////////////////////////////////			
															
			AreaInventory.Parameters.Quantity = 1;
			AreaInventory.Parameters.Price = 0;
			AreaInventory.Parameters.AmountFaraTVA = LineWithAdvance.AmountCuTVA - LineWithAdvance.AmountTVA;
			AreaInventory.Parameters.AmountTVA = LineWithAdvance.AmountTVA;
			
			i = LineWithAdvance.LineNumber;
			Spreadsheet.Put(AreaInventory);
			
		EndDo;

		For i = i + 1 To RowsNumberInDoc Do
			AreaInventory.Parameters["LineNumber"] = i;
			AreaInventory.Parameters["Document"] = Undefined;
			AreaInventory.Parameters["Quantity"] = Undefined;
			AreaInventory.Parameters["Price"] = Undefined;
			AreaInventory.Parameters["AmountFaraTVA"] = Undefined;
			AreaInventory.Parameters["AmountTVA"] = Undefined;
			
			Spreadsheet.Put(AreaInventory);
		EndDo;

/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////START//////////////////////////////////////////////////////////////////

		Query		= New Query;
		Query.Text	=
			"SELECT
			|	UserEmployees.Employee,
			|	UserEmployees.User
			|FROM
			|	InformationRegister.UserEmployees AS UserEmployees
			|WHERE
			|	UserEmployees.Employee = &Author";
		
		Query.SetParameter("Author", DocHeader.Responsible);
			
		Result		= Query.Execute();
		SelectionD	= Result.Choose();
			
		While SelectionD.Next() Do
			Try
				Footer.Parameters.CNPU	= SelectionD.Employee.Ind.PersonalCode;
				Footer.Parameters.CIU	= SelectionD.Employee.Ind.IDCard;
			Except
			EndTry;
		EndDo;
		
		Footer.Parameters.Fill(DocHeader);
		Footer.Parameters.Date	= Format(DocHeader.DocDate, "DF=""dd.MM.yyyy""");
		Footer.Parameters.User	= DocHeader.Responsible;

///////////////////////////////////////////////////Totals///////////////////////////////////////////////
///////////////////////////////////////////////////Start/////////////////////////////////////////////
	
		Footer.Parameters.TotalFaraTVA = Format(Round(TotalFaraTVA, 2), "NFD=2; NZ=0,00");
		
		Footer.Parameters.TotalTVA     = Format(Round(TotalTVA * CurrencyRateInDoc, 2), "NFD=2; NZ=0,00");
		
		Footer.Parameters.TotalDePlataCurr = "Total de plată" + " " + DocHeader.DocumentCurrency;
		
		Footer.Parameters.TotalCuTVA	  = Format(Round((TotalFaraTVA + TotalTVA) * CurrencyRateInDoc, 2), "NFD=2; NZ=0,00");
		
///////////////////////////////////////////////////Totals///////////////////////////////////////////////
////////////////////////////////////////////////////End////////////////////////////////////////////	
		
		Spreadsheet.Put(Footer);
		
////////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////END///////////////////////////////////////////////////////////////////

		InsertPageBreak = True;
	
	EndDo;			// per Documents
	
	Return Spreadsheet;

EndFunction
