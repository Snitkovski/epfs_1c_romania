﻿/////////////////////////////////// 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.SalesInvoice");
	//DestinationArray.Add("Document.SalesInvoice");

	RegistrationParametrs.Insert("Presentation", DestinationArray);
	RegistrationParametrs.Insert("Description", "FF de vanzare MDT (la doc. Vinzare Bunuri)");
	RegistrationParametrs.Insert("Version", "1.3.1.5"); 	// "1.0"
	RegistrationParametrs.Insert("SafeMode", False); 	// Variants: True, False / Варианты: Истина, Ложь 
	RegistrationParametrs.Insert("Information", "FF - la doc. Vinzare Bunuri");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
			"FF de vânzare MD-T",				// what we will see under button PRINT
			"FacturaFiscalaDeVinzare",   	// Name of Template 
			//"CallOfServerMethod",  	// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
			"ServerMethodCall",  		// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
			False,							// Показывать оповещение. Варианты Истина, Ложь / Variants: True, False
			"MXLPrint");           			// "MXLPrint" = for MXL / "" = for WORD !!! Модификатор 
	
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand	= CommandTable.Add();
	NewCommand.Presentation 	= Presentation;
	NewCommand.ID				= ID;
	NewCommand.Use				= Use;
	NewCommand.ShowNotification	= ShowNotification;
	NewCommand.Modifier			= Modifier;
	
EndProcedure

/////////////////////////////////// 
// Preparing of Print Form 
Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "FacturaFiscalaDeVinzare";						///PrintFormsCollection[0].DesignName;
	Except
		Message(NStr("en='TemplateName is empty';
					  |ro='TemplateName este gol';
					  |ru='TemplateName не заполнено'"));
		Return;
	EndTry;
	TemplateSynonim = "Factura fiscala " + ObjectArray[0].Number + " din data " + Format(ObjectArray[0].Date,"DF=yyyy-MM-dd");
	PrintManagement.OutputSpreadsheetDocumentToCollection(
			PrintFormsCollection,
			TemplateName,  												// Template Name
			TemplateSynonim,   											// Template Synonim
			CreatePrintForm(ObjectArray, PrintObjects, TemplateName)  	// Function for Execution (in this Module) - исполняющая функция (в этом же модуле)
			);
	
EndProcedure

Function CreatePrintForm(ObjectArray, PrintObjects, TemplateName)

	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_SalesInvoice";  // PrintParameters_ + Name_of_Document
	
	Template	= ThisObject.GetTemplate(TemplateName);
	Query		= New Query;
	
	Query.Text	=
	      "SELECT
	      |	SalesInvoice.Company AS Company,
	      |	SalesInvoice.Counterparty AS Counterparty,
	      |	SalesInvoice.Number AS DocNumber,
	      |	SalesInvoice.Date AS DocDate,
	      |	SalesInvoice.Driver AS Driver,
	      |	SalesInvoice.Driver.Code AS DriverCode,
	      |	SalesInvoice.Author AS Author,
	      |	SalesInvoice.Contract AS Contract,
		  //|	SalesInvoice.Contract.CustomerPaymentDueDate AS ContractCustomerPaymentDueDate,
	      |	SalesInvoice.Counterparty.Code AS CodeC,
	      |	SalesInvoice.Responsible AS Responsible,
	      |	SalesInvoice.Counterparty.ContactPerson AS ContactPerson,
	      |	SalesInvoice.ExchangeRate AS CurrencyRateInDoc,
	      |	SalesInvoice.DocumentCurrency AS DocumentCurrency,
	      |	SalesInvoice.Order AS Order,
	      |	SalesInvoice.Inventory.(
	      |		LineNumber AS InvLineNumber,
	      |		Products AS Products,
	      |		Characteristic AS Characteristic,
	      |		Quantity AS Quantity,
	      |		MeasurementUnit AS MeasurementUnit,
	      |		Price AS Price,
	      |		DiscountMarkupPercent AS DiscountMarkupPercent,
	      |		Amount AS Amount,
	      |		VATRate AS VATRate,
	      |		VATAmount AS VATAmount,
	      |		Total AS Total,
	      |		Inventory.Order AS Order
	      |	) AS Inventory,
	      |	SalesInvoice.Company.TIN AS CompanyTIN,
	      |	SalesInvoice.Company.BankAccountByDefault.Bank AS Bank,
	      |	SalesInvoice.Company.BankAccountByDefault.AccountNo AS BankAccount,
	      |	SalesInvoice.Counterparty.TIN AS CounterpartyTIN,
	      |	SalesInvoice.Counterparty.BankAccountByDefault.Bank AS BankC,
	      |	SalesInvoice.Counterparty.BankAccountByDefault.AccountNo AS BankAccountC,
	      |	SalesInvoice.AmountIncludesVAT AS IncludeVAT,
	      |	SalesInvoice.DocumentAmount AS DocAmount,
	      |	SalesInvoice.IncludeVATInPrice AS IncludeVATInPrice,
	      |	SalesInvoice.Prepayment.(
	      |		Ref AS Ref,
	      |		LineNumber AS AdvLineNumber,
	      |		Document AS Document,
	      |		Prepayment.Order AS Order,
	      |		SettlementsAmount AS SettlementsAmount,
	      |		ExchangeRate AS ExchangeRateInAdv,
	      |		Multiplicity AS Multiplicity,
	      |		PaymentAmount AS AdvPaymentAmount,
	      |		Document.Posted AS AdvDocumentPosted,
	      |		Document.ExchangeRate AS AdvDocumentExchangeRate
	      |	) AS Prepayment
	      |FROM
	      |	Document.SalesInvoice AS SalesInvoice
	      |WHERE
	      |	SalesInvoice.Ref IN(&Ref)";
		  
	Query.Parameters.Insert("Ref", ObjectArray);
	Selection			= Query.Execute().Choose();
	
	AreaCaption			= Template.GetArea("Caption");
	Header				= Template.GetArea("Header");
	//AreaInventoryHeader = Template.GetArea("InventoryHeader");
	//AreaInventory		= Template.GetArea("Line");
	Footer				= Template.GetArea("Footer");
	Spreadsheet.Clear();
	
	InsertPageBreak = False;
	While Selection.Next() Do          // per Documents
		If InsertPageBreak Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;
	
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////
		
		//InfoAboutCompany      =  SmallBusinessServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.DocDate, ,);
		//InfoAboutCounterparty =  SmallBusinessServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, Selection.DocDate, ,);
		InfoAboutCompany      =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.DocDate, ,);
		InfoAboutCounterparty =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, Selection.DocDate, ,);
		
		Header.Parameters["Company"] 			= InfoAboutCompany.FullDescr;
		Header.Parameters["Counterparty"] 		= InfoAboutCounterparty.FullDescr;
	
		Header.Parameters["KPP"] 				= InfoAboutCompany.TIN;
		Header.Parameters["KPPC"] 				= InfoAboutCounterparty.TIN;
		
		Header.Parameters["RegistrationNumber"] = InfoAboutCompany.RegistrationNumber;
		Header.Parameters["RegistrationNumberC"]= InfoAboutCounterparty.RegistrationNumber;
		
		//Message(Selection.Order);
		Header.Parameters["OrderParams"] 		= ?(Selection.Order <> Undefined,
														"Nr " + Selection.Order.Number + " " +
														Format(Selection.Order.Date, "DF=""dd.MM.yyyy"""), "");
	
		Header.Parameters["Address"]			= InfoAboutCompany.LegalAddress;
		Header.Parameters["AddressC"]			= InfoAboutCounterparty.LegalAddress;

		CurrencyRateInDoc 						= Selection.CurrencyRateInDoc;
		
		SelectionInventory						= Selection.Inventory.Choose();
		SelectionPrepayment						= Selection.Prepayment.Choose();
		
		Spreadsheet.Put(AreaCaption);
	
		Header.Parameters.Fill(Selection);
		
		Try
			Header.Parameters["Currency"]		= Selection.DocumentCurrency;
			Header.Parameters["CurrencyRate"]	= CurrencyRateInDoc;
		Except
			
		EndTry;
				
		Header.Parameters["Number"]  	= Selection.DocNumber;
		Header.Parameters["Date"]		= Format(Selection.DocDate,"DF=""dd.MM.yyyy""");
		
	///////////////////////////////////////////////////////////////////////////////////////////
	// Data scadenta - este limita de data in care Clientul trebuie sa achite Proforma  =  sapte zile calend.
		//If ValueIsFilled(Selection.DocDate) Then
		//	Header.Parameters["DataLimita"] = Format(Selection.DocDate + 
		//											(Selection.Contract.CustomerPaymentDueDate * 86400), 
		//											"DF=""dd.MM.yyyy""");
		//Else 													
			Header.Parameters["DataLimita"] = Format(Selection.DocDate + (7 * 86400), "DF=""dd.MM.yyyy""");
		//EndIf;
	///////////////////////////////////////////////////////////////////////////////////////////
													
		//AdditionalAttributes = PropertiesManagement.GetValuesOfProperties(Selection.Company);
		AdditionalAttributes = PropertyManager.GetValuesProperties(Selection.Company);
		For Each Attr in AdditionalAttributes Do
			If StrFind(TrimAll(Attr.Property), "Capital Social") > 0 Then
				Header.Parameters["Capital"] = String(Attr.Value) + " " + "RON";
			EndIf;
		EndDo;
		
		//17 July 2018
		//AddAttr = PropertiesManagement.GetValuesOfProperties(ObjectArray[0]);
		AdditionalAttributes = PropertyManager.GetValuesProperties(ObjectArray[0]);
		For Each Attr in AdditionalAttributes Do
			If StrFind(TrimAll(Attr.Property), "Nr aviz") > 0 Then
				Header.Parameters["NrAviz"] = Attr.Value;
			EndIf;
		EndDo;
		///
		While SelectionInventory.Next() Do
			Try
				Header.Parameters["VATRate"] = SelectionInventory.VATRate;
			Except
			EndTry;
		EndDo;
		Spreadsheet.Put(Header, Selection.Level());
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////END////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////
		//06 july 18
		AreDiscounts = Selection.Inventory.Unload().Total("DiscountMarkupPercent") <> 0;
		
		If AreDiscounts Then
			AreaInventoryHeader = Template.GetArea("InventoryHeaderWithDiscount");
			AreaInventory		= Template.GetArea("LineWithDiscount");
		Else
			AreaInventoryHeader = Template.GetArea("InventoryHeader");
			AreaInventory		= Template.GetArea("Line");
		EndIf;
		//
		Spreadsheet.Put(AreaInventoryHeader);
		SelectionInventory	= Selection.Inventory.Choose();
		TotalVAT	 		= 0;
		TotalFaraTVA 		= 0;
		RowsNumberInDoc 	= 35;
		
		//Message(Selection.IncludeVATInPrice);
		//Message(Selection.IncludeVAT);
		////////////////////  per Lines in current Document
		While SelectionInventory.Next() Do
				
			TotalFaraTVA	=  TotalFaraTVA	+ SelectionInventory.Amount;
			TotalVAT		=  TotalVAT 	+ SelectionInventory.VATAmount;
			
			AreaInventory.Parameters["LineNumber"]	= SelectionInventory.InvLineNumber;
			
			ItemDescription = "" + SelectionInventory.Products;
			If StrLen(SelectionInventory.Characteristic) > 0 Then
				ItemDescription = ItemDescription + " (" + SelectionInventory.Characteristic + ")";
			EndIf;
				
			AreaInventory.Parameters["Item"]			= ItemDescription;
			AreaInventory.Parameters["UnitOfMeasure"]	= SelectionInventory.MeasurementUnit;
			AreaInventory.Parameters["Quantity"]		= SelectionInventory.Quantity;
			
			PriceInLine	  		= Round(SelectionInventory.Price 	* CurrencyRateInDoc, 2);
			AmountInLine	 	= Round(SelectionInventory.Amount 	* CurrencyRateInDoc, 2); // With Discount  
			VATAmountInLine	 	= Round(SelectionInventory.VATAmount* CurrencyRateInDoc, 2);

			DiscountRateInLine	= SelectionInventory.DiscountMarkupPercent;
			AmountWODiscount	= 100 * AmountInLine / (100 - DiscountRateInLine);
			
			DiscountOnPriceValue= Round(PriceInLine * DiscountRateInLine / 100, 2);
			PriceDiscounted     = PriceInLine - DiscountOnPriceValue;   //DiscountOnPriceValue
								
			DiscountAmount		= Round(PriceInLine * DiscountRateInLine / 100 * SelectionInventory.Quantity, 2);
			
			If Selection.IncludeVAT	Then
			    PriceInLine			= PriceInLine		- PriceInLine * SelectionInventory.VATRate.Rate /
																			(100 + SelectionInventory.VATRate.Rate);
				AmountWODiscount	= AmountWODiscount 	- AmountWODiscount * SelectionInventory.VATRate.Rate /
																			(100 + SelectionInventory.VATRate.Rate);
				AmountInLine	 	= AmountInLine		- VATAmountInLine;
				DiscountAmount		= DiscountAmount	- DiscountAmount * SelectionInventory.VATRate.Rate /
																			(100 + SelectionInventory.VATRate.Rate);
			EndIf;
			
			AreaInventory.Parameters["Price"]	= Format(PriceInLine,"NFD=2");
			AreaInventory.Parameters["Amount"]	= Format(AmountInLine,"NFD=2");
			
			// 06 july 18
			If AreDiscounts Then
				AreaInventory.Parameters["DiscountAmount"] = Format(DiscountAmount, "NFD=2");
				AreaInventory.Parameters["AmountWODiscount"] = Format(AmountWODiscount, "NFD=2");
			EndIf;
			//
			AreaInventory.Parameters["VATAmount"] = Format(VATAmountInLine,"NFD=2");
				
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
			
			If AreDiscounts Then
				AreaInventory.Parameters["DiscountAmount"] = "";
				AreaInventory.Parameters["AmountWODiscount"] = "";
			EndIf;

			i = SelectionInventory.InvLineNumber;

			If StrLen(TrimAll(SelectionInventory.Products)) > 49 Then
				RowsNumberInDoc = RowsNumberInDoc - 1;
			EndIf;
		EndDo;

/////////////////////////////////////////////////////Prepayment///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////

		While SelectionPrepayment.next() do
			
			i = i + 1;
			AreaInventory.Parameters["LineNumber"]	= i;
			
			AreaInventory.Parameters["UnitOfMeasure"] = "";
			AreaInventory.Parameters["Quantity"] = "";
			AreaInventory.Parameters["Price"] = "";
			
			If SelectionPrepayment.AdvDocumentPosted Then
			
				AreaInventory.Parameters["Item"]			= NStr("en = 'Advance'; ro = 'Storno Avans'; ru = 'Аванс'");
				
				//////////////////////  NOT FINISHED YET !!!  //////////////////////////
				//Message(SelectionPrepayment.AdvDocumentExchangeRate);
				AdvDocExchangeRateInLine = 1;  // SelectionPrepayment.AdvDocumentExchangeRate;
	
				PrepaymentAmountInLineFaraTVA = -1 * Round(SelectionPrepayment.AdvPaymentAmount *
															AdvDocExchangeRateInLine / 119 * 100, 2);
				PrepaymentTVAInLine = -1 * Round(SelectionPrepayment.AdvPaymentAmount *
															AdvDocExchangeRateInLine / 119 * 19, 2);
				//////////////////////  NOT FINISHED YET !!!  //////////////////////////

				TotalFaraTVA	=  TotalFaraTVA	+ PrepaymentAmountInLineFaraTVA;
				TotalVAT		=  TotalVAT 	+ PrepaymentTVAInLine;
				
				//AreaInventory.Parameters["Amount"]	= Format(-1 * (SelectionPrepayment.AdvPaymentAmount - 
				//													SelectionPrepayment.AdvDocumentVATAmount),"NFD=2");
				//AreaInventory.Parameters["VATAmount"] = Format(-1 * SelectionPrepayment.AdvDocumentVATAmount,"NFD=2");
				AreaInventory.Parameters["Amount"]	= PrepaymentAmountInLineFaraTVA;
				AreaInventory.Parameters["VATAmount"] = PrepaymentTVAInLine;
				
			Else
				AreaInventory.Parameters["Item"] = "";
				AreaInventory.Parameters["Amount"]	= "";
				AreaInventory.Parameters["VATAmount"] = "";
			EndIf;
			
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());

		EndDo;

		////////////////////  per Lines in current Document
	
		For i = i + 1 To RowsNumberInDoc Do
			AreaInventory.Parameters["LineNumber"]		= i;
			AreaInventory.Parameters["Item"]			= "";
			AreaInventory.Parameters["UnitOfMeasure"]	= "";
			AreaInventory.Parameters["Quantity"]		= "";
			AreaInventory.Parameters["Price"]			= "";
			AreaInventory.Parameters["Amount"]			= "";
			
			Try
				AreaInventory.Parameters["VATAmount"]	= "";
			Except
			EndTry;
			
			// 06 july 18
			If AreDiscounts Then
				AreaInventory.Parameters["DiscountAmount"] = "";
				AreaInventory.Parameters["AmountWODiscount"] = "";
			EndIf;
			
			//
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////START//////////////////////////////////////////////////////////////////
		Query		= New Query;
		Query.Text	=
			"SELECT
			|	UserEmployees.Employee,
			|	UserEmployees.User
			|FROM
			|	InformationRegister.UserEmployees AS UserEmployees
			|WHERE
			|	UserEmployees.User = &Author";
		
		Query.SetParameter("Author", Selection.Author);
			
		Result		= Query.Execute();
		SelectionD	= Result.Choose();
			
		While SelectionD.Next() Do
			Try
				Footer.Parameters["CNPU"]	= SelectionD.Employee.Ind.PersonalCode;
				Footer.Parameters["CIU"]	= SelectionD.Employee.Ind.IDCard;
			Except
					
			EndTry;
		EndDo;
		
		Footer.Parameters.Fill(Selection);
		Footer.Parameters["Date"]	= Format(Selection.DocDate, "DF=""dd.MM.yyyy""");
		Footer.Parameters["User"]	= Selection.Author;

	///////////////////////////////////////////////////Totals///////////////////////////////////////////////
	///////////////////////////////////////////////////Start/////////////////////////////////////////////
		Footer.Parameters["TotalFaraTVA"] = Format(Round(?(Selection.IncludeVAT,
													(Selection.DocAmount - TotalVAT) * CurrencyRateInDoc,
													 TotalFaraTVA * CurrencyRateInDoc), 2), "NFD=2; NZ=0,00");
		
		Footer.Parameters["TotalTVA"]     = Format(Round(TotalVAT * CurrencyRateInDoc, 2), "NFD=2; NZ=0,00");
		
		Footer.Parameters["TotalDePlataCurr"] = "Total de plată" + " " + Selection.DocumentCurrency;
		
		//Footer.Parameters["TotalCuTVA"]	  = Round(Selection.DocAmount * CurrencyRateInDoc, 2);
		Footer.Parameters["TotalCuTVA"]	  = Format(Round((TotalFaraTVA + TotalVAT) * CurrencyRateInDoc, 2), "NFD=2; NZ=0,00");
	///////////////////////////////////////////////////Totals///////////////////////////////////////////////
	////////////////////////////////////////////////////End////////////////////////////////////////////	
	
		//17 July 2018
		For Each Attr in AdditionalAttributes Do

			If StrFind(TrimAll(Attr.Property), "Delegat") > 0 Then
				Footer.Parameters["Driver"] = Attr.Value;
			EndIf;
			
			If StrFind(TrimAll(Attr.Property), "CI") > 0 Then
				Footer.Parameters["CI"] = Attr.Value;
			EndIf;
			
			If StrFind(TrimAll(Attr.Property), "CNP") > 0 Then
				Footer.Parameters["CNP"] = Attr.Value;
			EndIf;
			
			If StrFind(TrimAll(Attr.Property), "Mijloc de transport") > 0 Then
				Footer.Parameters["MijlocDeTransport"] = Attr.Value;
			EndIf;
						
		EndDo;
		//
		Spreadsheet.Put(Footer);
		
//////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////END///////////////////////////////////////////////////////////////////
		InsertPageBreak = True;
	
	EndDo;			// per Documents
	
	Return Spreadsheet;

EndFunction
