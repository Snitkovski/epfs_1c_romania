﻿/////////////////////////////////////////////////////////////////////
//
// External print form: Factura fiscala (minus) - de Retur
// For Client:			MD-T
// Conn.to Docs:		CreditNote
//
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.CreditNote");
	
	RegistrationParametrs.Insert("Presentation", DestinationArray);
	
	RegistrationParametrs.Insert("Description", "FF (minus) MDT - la doc.Nota de Credit");
	RegistrationParametrs.Insert("Version", "1.3.1.5");
	RegistrationParametrs.Insert("SafeMode", False);
	RegistrationParametrs.Insert("Information", "Factura fiscala (minus) MDT");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
				"Factura fiscala minus MD-T",
				"FacturaFiscalaMinus",
				//"CallOfServerMethod",  	// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				"ServerMethodCall",  		// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				False,
				"MXLPrint");
	
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand	= CommandTable.Add();
	NewCommand.Presentation 	= Presentation;
	NewCommand.ID				= ID;
	NewCommand.Use				= Use;
	NewCommand.ShowNotification	= ShowNotification;
	NewCommand.Modifier			= Modifier;
	
EndProcedure

Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		//TemplateName = PrintFormsCollection[0].DesignName;
		TemplateName = "FacturaFiscalaMinus";
	Except
		Message(NStr("en='TemplateName is empty!';
					 |ro='TemplateName este goala!';
					 |ru='TemplateName este goala!'"));
		Return;
	EndTry;
	
	TemplateSynonim = "Factura fiscala(minus) " + ObjectArray[0].Number + " din " + Format(ObjectArray[0].Date,"DF=yyyy-MM-dd");
	
	PrintManagement.OutputSpreadsheetDocumentToCollection(
						PrintFormsCollection,
						TemplateName,
						TemplateSynonim,
						CreatePrintForm(ObjectArray, PrintObjects, TemplateName));
	
EndProcedure

Function CreatePrintForm(Ref, PrintObjects, TemplateName)
	
	Var Errors;
	
	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_CreditNote1";
	
	Template = ThisObject.GetTemplate(TemplateName);
	
	Query = New Query();
	Query.Text =
	"SELECT
	|	CreditNote.Counterparty AS Counterparty,
	|	CreditNote.Date AS Date,
	|	CreditNote.Number AS Number,
	|	CreditNote.BasisDocument AS BasisDocument,
	|	CreditNote.Company AS Company,
	|	CreditNote.Inventory.(
	|		LineNumber AS LineNumber,
	|		Products AS Products,
	|		Characteristic AS Characteristic,
	|		MeasurementUnit AS MeasurementUnit,
	|		Quantity AS Quantity,
//	|		Quantity AS Suma,
	|		VATAmount AS VATAmount,
	|		Total AS AmountTotal,
	|		Price AS Price,
	|		Amount AS AmountWhitoutTVA
	|	) AS Inventory,
	|	CreditNote.ExchangeRate AS ExchangeRate,
	|	CreditNote.DocumentCurrency AS DocumentCurrency,
	|	CreditNote.Company.RegistrationNumber AS NrOrcE,
	|	CreditNote.Company.BankAccountByDefault.AccountNo AS BankAccountE,
	|	CreditNote.Company.BankAccountByDefault.Bank AS BankE,
	|	CreditNote.Counterparty.RegistrationNumber AS NrOrcC,
	|	CreditNote.Counterparty.BankAccountByDefault.AccountNo AS BankAccountC,
	|	CreditNote.Counterparty.BankAccountByDefault.Bank AS BankC,
	|	CreditNote.DocumentAmount AS SDTotal,
	|	CreditNote.OperationKind AS TipTranzactie,
	|	CreditNote.Author AS Author,
	|	OperationKindsCreditNote.Ref AS RefTip,
	|	OperationKindsCreditNote.Order AS OrderTip,
	|	CreditNote.Posted AS PostedA
	|FROM
	|	Document.CreditNote AS CreditNote
	|		LEFT JOIN Enum.OperationTypesCreditNote AS OperationKindsCreditNote
	|		ON CreditNote.OperationKind = OperationKindsCreditNote.Ref
	|WHERE
	|	CreditNote.Ref IN(&Ref)
	|
	|ORDER BY
	|	LineNumber";
	
	Query.Parameters.Insert("Ref", Ref);
	Selection = Query.Execute().Choose();
	
	AreaCaption	 		= Template.GetArea("Caption");
	Header 				= Template.GetArea("Header");
	AreaInventoryHeader = Template.GetArea("InventoryHeader");
	AreaInventory 		= Template.GetArea("Inventory");
	Footer 				= Template.GetArea("Footer");
	Spreadsheet.Clear();
	
	SDTotalTVA = 0;
	SDFaraTVA = 0;
	LineNumberInPrintForm = 0;
	
	InsertPageBreak = False;
	
	While Selection.Next() Do
		If InsertPageBreak Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;

/////////////////   TO DO !!!   ///////////////		
		//DocRate = WorkWithCurrencyRates.GetCurrencyRate(Selection.DocumentCurrency, BegOfDay(Selection.Date)); 
		//NatRate = WorkWithCurrencyRates.GetCurrencyRate(Constants.NationalCurrency.get(), BegOfDay(Selection.Date));
		//
		//Try
		//	ERate = DocRate.ExchangeRate / NatRate.ExchangeRate;
		//Except
		//	Message(NStr("ro='Cursul valutar nu este actualizat!';
		//				 |en='Cursul valutar nu este actualizat!';
		//				 |ru='Cursul valutar nu este actualizat!'"));
		//	Rate = 1;
		//EndTry;
/////////////////   TO DO !!!   ///////////////		
		
		ERate = Selection.ExchangeRate;
		
		Spreadsheet.Put(AreaCaption);
		
		Header.Parameters.Fill(Selection);
		////////////////////////Header/////////////////////////////
		InfoAboutVendor  = DriveServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.Date, ,);
		InfoAboutVendorC = DriveServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, Selection.Date, ,);
		
		Header.Parameters["CFE"] 		= Selection.Company.TIN;
		Header.Parameters["CFC"] 		= Selection.Counterparty.TIN;
		Header.Parameters["AdresaE"]	= DriveServer.CompaniesDescriptionFull(InfoAboutVendor, "LegalAddress,");
		Header.Parameters["AdresaC"] 	= DriveServer.CompaniesDescriptionFull(InfoAboutVendorC, "LegalAddress,");
		Header.Parameters["Number"] 	= Selection.Number;
		Header.Parameters["Date"]		= Format(Selection.Date, "DF=dd.MM.yyyy");
		Header.Parameters["ERate"]		= ERate;
		
		//Header.Parameters["NrComanda"]		= "No " + Selection.BasisDocument.Number + " din data " + Selection.BasisDocument.Date;
		Header.Parameters["NrDataFacturaDeVanzare"] = Selection.BasisDocument.Number + " din " +
														Format(Selection.BasisDocument.Date, "DF=dd.MM.yyyy");
		
		//AdditionalAttributes = PropertiesManagement.GetValuesOfProperties(Selection.Company);
		AdditionalAttributes = PropertyManager.GetValuesProperties(Selection.Company);
		For Each Attr in AdditionalAttributes Do
			If StrFind(TrimAll(Attr.Property), "Capital Social") > 0 Then
				Header.Parameters["Capital"] = Attr.Value;
			EndIf;
		EndDo;
		
		Spreadsheet.Put(Header, Selection.Level());
		
		////////////////////////Header/////////////////////////////
		
		//		Header1.Parameters["DocumentBasis"] = Selection.DocumentBasis;
		//				Header1.Parameters["Company"]		= Selection.Company;
		//		Header1.Parameters["DocNumber"] 	= Selection.IncomingDocumentNo;
		//		Header1.Parameters["DocDate"] 		= Selection.DateOfIncomingDocument;
		//		ERate = Selection.ExchangeRate;
		//		Header1.Parameters["ERate"]= ERate;	
		//		Spreadsheet.Put(Header1);
		
		///////////////////////////////AreaInventoryHeader/////////////////////////////////////
		Spreadsheet.Put(AreaInventoryHeader);
		///////////////////////////////AreaInventoryHeader/////////////////////////////////////

		///////////////////////////////AreaInventory/////////////////////////////////////
		AreaInventory.Parameters.Fill(Selection);
		SelectionInventory = Selection.Inventory.Choose();
		While SelectionInventory.Next() Do
			
			If SelectionInventory.Quantity = 0 Then
				Continue;
			EndIf;
			
			//AreaInventory.Parameters["LineNumber"]		= SelectionInventory.LineNumber;
			LineNumberInPrintForm = LineNumberInPrintForm  + 1;
			AreaInventory.Parameters["LineNumber"]		= LineNumberInPrintForm;

			//AreaInventory.Parameters["ProductsAndServices"]	= SelectionInventory.ProductsAndServices;
			ItemDescription = "" + SelectionInventory.Products;
			If StrLen(SelectionInventory.Characteristic) > 0 Then
				ItemDescription = ItemDescription + " (" + SelectionInventory.Characteristic + ")";
			EndIf;
				
			AreaInventory.Parameters["Item"]			= ItemDescription;
			
			AreaInventory.Parameters["Price"]			= Format(Round(SelectionInventory.Price * ERate,2), "NFD=2");
			AreaInventory.Parameters["MeasurementUnit"]	= SelectionInventory.MeasurementUnit;
			AreaInventory.Parameters["Quantity"]		= Format(-1 * SelectionInventory.Quantity, "NFD=2");  //SelectionInventory.Quantity;				
			SDTotalTVA = SDTotalTVA + SelectionInventory.VATAmount;
			SDFaraTVA  = SDFaraTVA + SelectionInventory.AmountWhitoutTVA;
			
			Try
				AreaInventory.Parameters["VATAmount"] = Format(Round(-1 * SelectionInventory.VATAmount * ERate, 2),"NFD=2");
			Except
				
			EndTry;
			
			AreaInventory.Parameters["Amount"] = Format(Round(-1 * SelectionInventory.AmountWhitoutTVA * ERate,2),"NFD=2");
			
			//i = SelectionInventory.LineNumber;
			i = LineNumberInPrintForm;
			 
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
		
		For i = i + 1 To 35 Do
			
			AreaInventory.Parameters["LineNumber"]	= i;
			//AreaInventory.Parameters["ProductsAndServices"] = Undefined;
			AreaInventory.Parameters["ITEM"] 		= Undefined;
			AreaInventory.Parameters["Price"]	    = Undefined;
			AreaInventory.Parameters["Amount"]	    = Undefined;
			AreaInventory.Parameters["Quantity"]	= Undefined;
			AreaInventory.Parameters["MeasurementUnit"] = Undefined;
			
			Try
				AreaInventory.Parameters["VATAmount"]	= Undefined;
			Except
				
			EndTry;
			
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
		///////////////////////////////AreaInventory/////////////////////////////////////

		///////////////////////////////////Footer//////////////////////////////////////////////
		Footer.Parameters["SDTotalTVA"] = Format(Round(-1 * SDTotalTVA * ERate,2),"NFD=2");
		Footer.Parameters["SDFaraTVA"]  = Format(Round(-1 * SDFaraTVA * ERate,2),"NFD=2");
		Footer.Parameters["SDTotal"]    = Format(Round(-1 * Selection.SDTotal * ERate,2),"NFD=2");
		
		Query		= New Query;
		Query.Text	=
		"SELECT
		|	UserEmployees.Employee,
		|	UserEmployees.User
		|FROM
		|	InformationRegister.UserEmployees AS UserEmployees
		|WHERE
		|	UserEmployees.User = &Author";
		
		Query.SetParameter("Author", Selection.Author);
		
		Result		= Query.Execute();
		SelectionD	= Result.Choose();
		
		While SelectionD.Next() Do
			Try
				Footer.Parameters["CNPU"]	= SelectionD.Employee.Ind.PersonalCode;
				Footer.Parameters["CIU"]	= SelectionD.Employee.Ind.IDCard;
				Footer.Parameters["User"]	= Selection.Author;
			Except
				
			EndTry;

		EndDo;
		
		For Each Attr in AdditionalAttributes Do

			If StrFind(TrimAll(Attr.Property), "Delegat") > 0 Then
				Footer.Parameters["Driver"] = Attr.Value;
			EndIf;
			
			If StrFind(TrimAll(Attr.Property), "CI") > 0 Then
				Footer.Parameters["CI"] = Attr.Value;
			EndIf;
			
			If StrFind(TrimAll(Attr.Property), "CNP") > 0 Then
				Footer.Parameters["CNP"] = Attr.Value;
			EndIf;
			
			If StrFind(TrimAll(Attr.Property), "Mijloc de transport") > 0 Then
				Footer.Parameters["MijlocDeTransport"] = Attr.Value;
			EndIf;
						
		EndDo;
		
		Spreadsheet.Put(Footer);
		
		//////////////////////Footer////////////////////////////////////
		
		InsertPageBreak = True;
		
	EndDo;
	
	Message(NStr("ro='Atenție! Această imprimare este specifică doar pentru documentele cu tipul operațiunii ""Returnare de la cumpărător""';
				  |en='Atenție! Această imprimare este specifică doar pentru documentele cu tipul operațiunii ""Returnare de la cumpărător""';
				  |ru='Atenție! Această imprimare este specifică doar pentru documentele cu tipul operațiunii ""Returnare de la cumpărător""'"));
	
	If Selection.PostedA = False Then
		Spreadsheet.BackgroundPicture = New Picture(GetTemplate("Template"), True);
	EndIf;
	
	Return Spreadsheet;
	
EndFunction
