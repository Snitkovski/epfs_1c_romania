﻿
#Region FormEventsHandlers

&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
	
	// Access right check should be the first one.
	If Not AccessRight("Administration", Metadata) Then
		Raise NStr("en = 'Data processor use in interactive mode is available to administrator only.'; ru = 'Использование обработки в интерактивном режиме доступно только администратору.';tr = 'Etkileşimli modda veri işlemcisi kullanımı sadece yönetici için kullanılabilir.';ro = 'Folosirea procesorului de date în modul interactiv este disponibilă numai pentru administrator.';pl = 'Używanie przetwarzania danych w trybie interaktywnym jest dostępne tylko dla administratora.';de = 'Die Verwendung des Datenprozessors im interaktiven Modus ist nur für Administratoren verfügbar.';es_ES = 'Uso del procesador de datos en el modo interactivo está disponible solo para el administrador'");
	EndIf;
	
	If Parameters.Property("AutoTest") Then // Return if the form for analysis is received.
		Return;
	EndIf;
	
	Object.ExchangeFileName = Parameters.ExchangeFileName;
	Object.ExchangeRulesFilename = Parameters.ExchangeRulesFilename;
	Object.EventHandlersExternalDataProcessorFileName = Parameters.EventHandlersExternalDataProcessorFileName;
	Object.AlgorithmsDebugMode = Parameters.AlgorithmsDebugMode;
	Object.EventHandlersReadFromFileOfExchangeRules = Parameters.EventHandlersReadFromFileOfExchangeRules;
	
	FormTitle = NStr("en = 'Configure handler debugging on data %Event%'; ru = 'Настройка отладки обработчиков при %Event% данных';tr = '%Event% verilerinde işleyici hata ayıklaması yapılandırma';ro = 'Configurați depanarea manipulatorului pe date %Event%';pl = 'Skonfiguruj debugowanie modułu obsługi danych na %Event%';de = 'Konfigurieren des Anwender-Debugging auf Daten %Event%';es_ES = 'Configurar la depuración del manipulador de los datos %Event%'");
	Event = ?(Parameters.EventHandlersReadFromFileOfExchangeRules, NStr("en = 'export'; ru = 'выгрузке';tr = 'dışa aktarma';ro = 'export';pl = 'eksportuj';de = 'export';es_ES = 'exportar'"), NStr("en = 'import'; ru = 'загрузке';tr = 'içe aktar';ro = 'import';pl = 'importuj';de = 'import';es_ES = 'importar'"));
	FormTitle = StrReplace(FormTitle, "%Event%", Event);
	Title = FormTitle;
	
	ButtonTitle = NStr("en = 'Generate %Event% debug engine'; ru = 'Сформировать модуль отладки %Event%';tr = '%Event% hata ayıklama modülü oluştur';ro = 'Generați %Event% depanare motor';pl = 'Sformować moduł debugowania %Event%';de = 'Erzeuge %Event% Debug-Engine';es_ES = 'Generar el motor de depuración de %Event%'");
	Event = ?(Parameters.EventHandlersReadFromFileOfExchangeRules, NStr("en = 'export'; ru = 'выгрузке';tr = 'dışa aktarma';ro = 'export';pl = 'eksportuj';de = 'export';es_ES = 'exportar'"), NStr("en = 'import'; ru = 'загрузке';tr = 'içe aktar';ro = 'import';pl = 'importuj';de = 'import';es_ES = 'importar'"));
	ButtonTitle = StrReplace(ButtonTitle, "%Event%", Event);
	Items.ExportCodeHandlers.Title = ButtonTitle;
	
	SetVisible();
	
EndProcedure

#EndRegion

#Region HeaderFormItemsEventsHandlers

&AtClient
Procedure AlgorythmsDebuggingOnChange(Item)
	
	AlgorythmsDebuggingOnChangeAtServer();
	
EndProcedure

&AtClient
Procedure EventHandlersExternalDataProcessorFileNameStartChoice(Item, ChoiceData, StandardProcessing)
	
	FileDialog = New FileDialog(FileDialogMode.Open);
	
	FileDialog.Filter     = NStr("en = 'External data processor file of event handlers (*.epf)|*.epf'; ru = 'Файл внешней обработки обработчиков событий (*.epf)|*.epf';tr = 'Olay işleyicilerinin dış veri işlemci dosyası (*.epf) |*.epf';ro = 'Fișier procesor de date extern al operatorilor de evenimente (*.epf)|*.epf';pl = 'Zewnętrzny plik przetwarzania danych programów obsługi zdarzeń (*.epf)|*.epf';de = 'Externe Datenverarbeitungsdatei von Ereignis-Anerndern (*.epf)|*.epf';es_ES = 'Archivo del procesador de datos externo de los manipuladores de eventos (*.epf)|*.epf'");
	FileDialog.Extension = "epf";
	FileDialog.Title = NStr("en = 'Select file'; ru = 'Выберите файл';tr = 'Dosya seç';ro = 'Selectarea fișierului';pl = 'Wybierz plik';de = 'Datei auswählen';es_ES = 'Seleccionar un archivo'");
	FileDialog.Preview = False;
	FileDialog.FilterIndex = 0;
	FileDialog.FullFileName = Item.EditText;
	FileDialog.CheckFileExist = True;
	
	If FileDialog.Choose() Then
		
		Object.EventHandlersExternalDataProcessorFileName = FileDialog.FullFileName;
		
		EventHandlersExternalDataProcessorFileNameOnChange(Item)
		
	EndIf;
	
EndProcedure

&AtClient
Procedure EventHandlersExternalDataProcessorFileNameOnChange(Item)
	
	SetVisible();
	
EndProcedure

#EndRegion

#Region FormCommandsHandlers

&AtClient
Procedure Done(Command)
	
	ClearMessages();
	
	If IsBlankString(Object.EventHandlersExternalDataProcessorFileName) Then
		
		MessageToUser(NStr("en = 'Specify a name of the external data processor file.'; ru = 'Укажите имя файла внешней обработки.';tr = 'Harici veri işlemcisi dosyasının adını belirtin.';ro = 'Specificați un nume al fișierului procesor de date extern.';pl = 'Podaj nazwę zewnętrznego pliku przetwarzania danych.';de = 'Geben Sie einen Namen für die externe Datenprozessordatei an.';es_ES = 'Especificar un nombre del archivo del procesador de datos externo.'"), "EventHandlersExternalDataProcessorFileName");
		Return;
		
	EndIf;
	
	EventHandlersExternalDataProcessorFile = New File(Object.EventHandlersExternalDataProcessorFileName);
	If Not EventHandlersExternalDataProcessorFile.Exist() Then
		
		MessageToUser(NStr("en = 'The specified file of external data processor does not exist.'; ru = 'Указанный файл внешней обработки не существует.';tr = 'Belirtilen harici veri işlemcisi dosyası mevcut değil.';ro = 'Fișierul specificat de procesor extern de date nu există.';pl = 'Określony plik zewnętrznego przetwarzania danych nie istnieje.';de = 'Die angegebene Datei des externen Datenprozessors existiert nicht.';es_ES = 'El archivo especificado del procesador de datos externo no existe.'"), "EventHandlersExternalDataProcessorFileName");
		Return;
		
	EndIf;
	
	CloseParameters = New Structure;
	CloseParameters.Insert("EventHandlersExternalDataProcessorFileName", Object.EventHandlersExternalDataProcessorFileName);
	CloseParameters.Insert("AlgorithmsDebugMode", Object.AlgorithmsDebugMode);
	CloseParameters.Insert("ExchangeRulesFilename", Object.ExchangeRulesFilename);
	CloseParameters.Insert("ExchangeFileName", Object.ExchangeFileName);
	
	Close(CloseParameters);
	
EndProcedure

&AtClient
Procedure OpenFile(Command)
	
	ShowEventHandlersInWindow();
	
EndProcedure

#EndRegion

#Region ServiceProceduresAndFunctions

&AtServer
Procedure SetVisible()
	
	AlgorythmsDebuggingOnChangeAtServer();
	
	// Selection red assistant steps which are executed incorrectly.
	SetBorderSelection("Group_Step_4", IsBlankString(Object.EventHandlersExternalDataProcessorFileName));
	
	Items.OpenFile.Enabled = Not IsBlankString(Object.TemporaryFileNameOfEventHandlers);
	
EndProcedure

&AtServer
Procedure SetBorderSelection(NameFrames, OneMustHighlightFrame = False)
	
	AssistantStepBorder = Items[NameFrames];
	
	If OneMustHighlightFrame Then
		
		AssistantStepBorder.TitleTextColor = StyleColors.SpecialTextColor;
		
	Else
		
		AssistantStepBorder.TitleTextColor = New Color;
		
	EndIf;
	
EndProcedure

&AtClient
Procedure ExportCodeHandlers(Command)
	
	// Perhaps export was already executed previously...
	If Not IsBlankString(Object.TemporaryFileNameOfEventHandlers) Then
		
		ButtonList = New ValueList;
		ButtonList.Add(DialogReturnCode.Yes, NStr("en = 'Export again'; ru = 'Выгрузить повторно';tr = 'Tekrar dışa aktarma';ro = 'Exportați din nou';pl = 'Eksportuj ponownie';de = 'Exportieren Sie erneut';es_ES = 'Exportar de nuevo'"));
		ButtonList.Add(DialogReturnCode.No, NStr("en = 'Open module'; ru = 'Открыть модуль';tr = 'Modülü aç';ro = 'Deschis modul';pl = 'Otwórz moduł';de = 'Modul öffnen';es_ES = 'Abrir el módulo'"));
		ButtonList.Add(DialogReturnCode.Cancel);
		
		NotifyDescription = New NotifyDescription("ExportHandlerCodeEnd", ThisObject);
		ShowQueryBox(NotifyDescription, NStr("en = 'Debug engine with handler code is already exported.'; ru = 'Модуль отладки с кодом обработчиков уже выгружен.';tr = 'İşleyici koduyla hata ayıklama motoru zaten dışa aktarılıyor.';ro = 'Sistemul de depanare cu codul de manipulare este deja exportat.';pl = 'Moduł debugowania z kodem obsługi jest już eksportowany.';de = 'Die Debug-Engine mit dem Anwender-Code wurde bereits exportiert.';es_ES = 'Motor de depuración con el código del manipulador ya se ha exportado.'"), ButtonList,,DialogReturnCode.No);
		
	Else
		
		ExportHandlerCodeEnd(DialogReturnCode.Yes, Undefined);
		
	EndIf;
	
EndProcedure

&AtClient
Procedure ExportHandlerCodeEnd(Result, AdditionalParameters) Export
	
	ThereAreExportErrors = False;
	
	If Result = DialogReturnCode.Yes Then
		
		ItIsExportedWithErrors = False;
		ExportHandlersEventAtServer(ItIsExportedWithErrors);
		
	ElsIf Result = DialogReturnCode.Cancel Then
		
		Return;
		
	EndIf;
	
	If Not ThereAreExportErrors Then
		
		SetVisible();
		
		ShowEventHandlersInWindow();
		
	EndIf;
	
EndProcedure

&AtClient
Procedure ShowEventHandlersInWindow()
	
	HandlerFile = New File(Object.TemporaryFileNameOfEventHandlers);
	If HandlerFile.Exist() AND HandlerFile.Size() <> 0 Then
		TextDocument = New TextDocument;
		TextDocument.Read(Object.TemporaryFileNameOfEventHandlers);
		TextDocument.Show(NStr("en = 'Handler debug engine'; ru = 'Модуль отладки обработчиков';tr = 'İşleyici hata ayıklama motoru';ro = 'Handler-ul de depanare a motorului';pl = 'Moduł nastawienia przetwarzania';de = 'Anwender-Debug-Engine';es_ES = 'Motor de depuración del manipulador'"));
	EndIf;
	
	ErrorFile = New File(Object.TemporaryFileNameOfExchangeProtocol);
	If ErrorFile.Exist() AND ErrorFile.Size() <> 0 Then
		TextDocument = New TextDocument;
		TextDocument.Read(Object.TemporaryFileNameOfEventHandlers);
		TextDocument.Show(NStr("en = 'An error occurred when exporting the handler module'; ru = 'Ошибки выгрузки модуля обработчиков';tr = 'İşleyici modülünü dışa aktarırken bir hata oluştu';ro = 'A apărut o eroare la exportul modulului de manipulare';pl = 'Wystąpił błąd podczas eksportowania modułu obsługi';de = 'Beim Exportieren des Anwender-Moduls ist ein Fehler aufgetreten';es_ES = 'Ha ocurrido un error al exportar el módulo del manipulador'"));
	EndIf;
	
EndProcedure

&AtServer
Procedure ExportHandlersEventAtServer(Cancel)
	
	ObjectForServer = FormAttributeToValue("Object");
	FillPropertyValues(ObjectForServer, Object);
	ObjectForServer.ExportEventHandlers(Cancel);
	ValueToFormAttribute(ObjectForServer, "Object");
	
EndProcedure

&AtServer
Procedure AlgorythmsDebuggingOnChangeAtServer()
	
	ToolTip = Items.HintAlgorithmsDebugging;
	
	ToolTip.CurrentPage = ToolTip.ChildItems["Group_"+Object.AlgorithmsDebugMode];
	
EndProcedure

&AtClientAtServerNoContext
Procedure MessageToUser(Text, DataPath = "")
	
	Message = New UserMessage;
	Message.Text = Text;
	Message.DataPath = DataPath;
	Message.Message();
	
EndProcedure

#EndRegion

