﻿#Region ProgramInterface

Function ExternalDataProcessorInfo() Export

	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind = AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindAdditionalDataProcessor();
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= "Import exchange rates example";
	RegistrationParameters.Information	= NStr("en = 'Data processor / example for import currency exchange rates'; ru = 'Внешняя обработка / пример для импорта курса валют'");
	RegistrationParameters.SafeMode		= False;
	
    Return RegistrationParameters;
	
EndFunction

#EndRegion

#Region ServiceProgramInterface

// Procedure for exchange rates import by a certain period.
//
// Parameters:
// Currencies		- Any collection - with the following fields:
// 				CurrencyCode - currency numeric code.
// 				Currency - ref on currency.
// ImportBeginOfPeriod	- Date - start of the currency rates import period.
// ImportEndOfPeriod	- Date - end of currency rates import period.
//
// Returns:
// Import state array  - each item - structure with fields.
// 	Currency - imported currency.
// 	OperationStatus - if the import is complete successfully.
// 	Message - import explanation (error message text or an explanatory message).
//
Function ImportCurrencyRatesByParameters(Val Currencies, Val ImportBeginOfPeriod, Val ImportEndOfPeriod,
	ErrorsOccuredOnImport = False) Export
	
	ImportStatus = New Array;
	
	ErrorsOccuredOnImport = False;
	
	ServerSource = "cbrates.rbc.ru";
	
	If ImportBeginOfPeriod = ImportEndOfPeriod Then
		Address = "tsv/";
		Tmp   = Format(ImportEndOfPeriod, "DF=/yyyy/MM/dd");
	Else
		Address = "tsv/cb/";
		Tmp   = "";
	EndIf;
	
	For Each Currency IN Currencies Do
		FileOnWebServer = "http://" + ServerSource + "/" + Address + Right(Currency.CurrencyCode, 3) + Tmp + ".tsv";
		
		Result = GetFilesFromInternet.DownloadFileAtServer(FileOnWebServer);
		
		If Result.Status Then
			ExplainingMessage = ImportCurrencyRateFromFile(Currency.Currency, Result.Path, ImportBeginOfPeriod, ImportEndOfPeriod) + Chars.LF;
			DeleteFiles(Result.Path);
			OperationStatus = IsBlankString(ExplainingMessage);
		Else
			ExplainingMessage = StringFunctionsClientServer.SubstituteParametersToString(
				NStr("en='Unable to receive data file with exchange rates
		|%1
		|- %2): %3 There may not be an access to website with exchange rates or non-existent currency is specified.';ru='Невозможно получить файл данных с
		|курсами
		|валюты (%1 - %2): %3 Возможно, нет доступа к веб сайту с курсами валют, либо указана несуществующая валюта.'"),
				Currency.CurrencyCode,
				Currency.Currency,
				Result.ErrorInfo);
			OperationStatus = False;
			ErrorsOccuredOnImport = True;
		EndIf;
		
		ImportStatus.Add(New Structure("Currency,OperationStatus,Message", Currency.Currency, OperationStatus, ExplainingMessage));
		
	EndDo;
	
	Return ImportStatus;
	
EndFunction

// Imports information about the Currency exchange rate from the
// PathToFile file to exchange rates information register. The file with the currency rate
// is parsed and only data that meets the period is written (ImportPeriodBegin, ImportPeriodEnd).
//
Function ImportCurrencyRateFromFile(Val Currency, Val PathToFile, Val ImportBeginOfPeriod, Val ImportEndOfPeriod) Export
	
	StatusExport = 1;
	
	NumberOfDaysExportTotal = 1 + (ImportEndOfPeriod - ImportBeginOfPeriod) / ( 24 * 60 * 60);
	
	NumberOfImportedDays = 0;
	
	If IsTempStorageURL(PathToFile) Then
		FileName = GetTempFileName();
		BinaryData = GetFromTempStorage(PathToFile);
		BinaryData.Write(FileName);
	Else
		FileName = PathToFile;
	EndIf;
	
	Text = New TextDocument();
	
	TableExchangeRates = InformationRegisters.ExchangeRate;
	
	Text.Read(FileName, TextEncoding.ANSI);
	LineNumbers = Text.LineCount();
	
	For Ind = 1 To LineNumbers Do
		
		Str = Text.GetLine(Ind);
		If (Str = "") OR (Find(Str,Chars.Tab) = 0) Then
			Continue;
		EndIf;
		
		If ImportBeginOfPeriod = ImportEndOfPeriod Then
			ExchangeRateDate = ImportEndOfPeriod;
		Else
			RateDateStr = SelectSubString(Str);
			ExchangeRateDate    = Date(Left(RateDateStr,4), Mid(RateDateStr,5,2), Mid(RateDateStr,7,2));
		EndIf;
		
		Multiplicity = Number(SelectSubString(Str));
		ExchangeRate      = Number(SelectSubString(Str));
		
		If ExchangeRateDate > ImportEndOfPeriod Then
			Break;
		EndIf;
		
		If ExchangeRateDate < ImportBeginOfPeriod Then
			Continue;
		EndIf;
		
		WriteCoursesOfCurrency = TableExchangeRates.CreateRecordManager();
		
		WriteCoursesOfCurrency.Currency    = Currency;
		WriteCoursesOfCurrency.Period    = ExchangeRateDate;
		WriteCoursesOfCurrency.Rate      = ExchangeRate;
		WriteCoursesOfCurrency.Repetition = Multiplicity;
		WriteCoursesOfCurrency.Write();
		
		NumberOfImportedDays = NumberOfImportedDays + 1;
	EndDo;
	
	If IsTempStorageURL(PathToFile) Then
		DeleteFiles(FileName);
		DeleteFromTempStorage(PathToFile);
	EndIf;
	
	If NumberOfDaysExportTotal = NumberOfImportedDays Then
		ExplanationAboutExporting = "";
	ElsIf NumberOfImportedDays = 0 Then
		ExplanationAboutExporting = NStr("en='Exchange rates %1 - %2 are not imported. No data available.';ru='Курсы валюты %1 - %2 не загружены. Нет данных.'");
	Else
		ExplanationAboutExporting = NStr("en='Not all exchange rates for currency %1 are imported - %2.';ru='Загружены не все курсы по валюте %1 - %2.'");
	EndIf;
	
	ExplanationAboutExporting = StringFunctionsClientServer.SubstituteParametersToString(
									ExplanationAboutExporting,
									Currency.Code,
									Currency.Description);
	
	UserMessages = GetUserMessages(True);
	ErrorList = New Array;
	For Each UserMessage IN UserMessages Do
		ErrorList.Add(UserMessage.Text);
	EndDo;
	ErrorList = CommonClientServer.CollapseArray(ErrorList);
	ExplanationAboutExporting = ?(IsBlankString(ExplanationAboutExporting), "", Chars.LF) + StringFunctionsClientServer.StringFromSubstringArray(ErrorList, Chars.LF);
	
	Return ExplanationAboutExporting;
	
EndFunction

// Highlights from the passed
//  string the first value up to the "TAB" character.
//
// Parameters: 
//  SourceLine - String - String for parsing.
//
// Returns:
//  subrow up to the "TAB" character
//
Function SelectSubString(SourceLine)
	
	Var Substring;
	
	Pos = Find(SourceLine,Chars.Tab);
	If Pos > 0 Then
		Substring = Left(SourceLine,Pos-1);
		SourceLine = Mid(SourceLine,Pos + 1);
	Else
		Substring = SourceLine;
		SourceLine = "";
	EndIf;
	
	Return Substring;
	
EndFunction

#EndRegion
