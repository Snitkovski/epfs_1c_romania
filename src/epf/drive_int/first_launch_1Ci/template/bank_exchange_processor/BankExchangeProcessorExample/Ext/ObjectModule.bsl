﻿
#Region ProgramInterface

Function ExternalDataProcessorInfo() Export
	
	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind = AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindAdditionalDataProcessor();
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= NStr("en = 'Bank exchange processor (example)';
														|ru = 'Обработка обмена с банком (пример)'");
	RegistrationParameters.Information	= NStr("en = 'Bank exchange processor (example)';
														|ru = 'Обработка обмена с банком (пример)'");
	RegistrationParameters.SafeMode		= False;
	
	CommandTable = GetTableOfCommands();
	
	RegistrationParameters.Insert("Commands", CommandTable);
	
	Return RegistrationParameters;
	
EndFunction

Function GetTableOfCommands()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowAlert",		New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

// Function - On define settings
//
// Set common values, additional settings of catalog "BankStatementProcessingSettings"
//
//	Common settings:
//		UseImportFromFile	- Boolean	– True, if load from file supported
//		UseExportToFile		- Boolean	– True, if export to file supported
//		FileExtensions		– String	- accepted file formats (see FileDialog - Filter)
//		Encoding			– String	- file encoding (see Encoding)
//	AdditionalSettings is structure with your, will be shown in "BankStatementProcessingSettings" form
//	Can cointain number, string, date, any ref
//
// Returns:
// Structure - Settings structure
//
Function OnDefineSettings() Export
	
	Settings = New Structure("Encoding, UseImportFromFile, UseExportToFile", "UTF-8", True, True);
	
	Settings.Insert("FileExtensions", NStr("en = 'Bank statement file (*.sta)|*.sta';
										|ru = 'Файл банковской выписки (*.sta)|*.sta'"));
	
	AdditionalSettings = New Structure("CF_item_from_customer, CF_item_to_supplier, CF_item_other",
		Catalogs.CashFlowItems.PaymentFromCustomers, Catalogs.CashFlowItems.PaymentToVendor, Catalogs.CashFlowItems.Other);
	
	Settings.Insert("AdditionalSettings", AdditionalSettings);
	
	Return Settings;
	
EndFunction

// Procedure - Import data from file
//
// Parameters:
//  ParametersStructure		- Structure							- contains ExportTable and ExchangeSettings
//		* BinaryData		- BinaryData						- file binary data
//		* ExchangeSettings	- BankStatementProcessingSettings	- catalog ref settings
//  ResultStructure			- Structure		- structure with result
//		* Done				– Boolean		– the result of processing
//		* Errors			– Structure		– see CommonUseClientServer.AddUserError, if Done is false the errors will be shown
//		* ImportedTable 	– ValueTable	– data imported  from file, see BankStatementProcessing ImportTable
//
Procedure ImportDataFromFile(ParametersStructure, ResultStructure) Export
	
	Text = New TextReader(ParametersStructure.BinaryData.OpenStreamForRead(), ParametersStructure.ExchangeSettings.Encoding);
	
	ReadTXTFileToTable(Text.Read(), ResultStructure.ImportedTable, ParametersStructure.ExchangeSettings, ResultStructure);
	
EndProcedure

// Procedure - Export data to file
//
// Parameters:
//  ParametersStructure		- Structure							- contains ExportTable and ExchangeSettings
//		* ExportTable		- ValueTable						- Export table in BankStatementProcessing
//		* ExchangeSettings	- BankStatementProcessingSettings	- catalog ref settings
//  ResultStructure		- Structure	- structure with result
//		* Done			– Boolean		– the result of processing
//		* Errors		– Structure		– see CommonUseClientServer.AddUserError, if Done is false the errors will be shown
//		* BinaryData	– BinaryData	– data to write to file
//
Procedure ExportDataToFile(ParametersStructure, ResultStructure) Export
	
	ResultStructure.BinaryData = CreateBinaryDataFromTable(ParametersStructure, ParametersStructure.ExchangeSettings.Encoding);
		
EndProcedure

#EndRegion

#Region ServiceProceduresAndFunctions

Function CreateBinaryDataFromTable(Parameters, Encoding)
	
	//Export is under construction
	ResultText = "";
	
	Query = New Query;
	Query.Text =
	"SELECT
	|	PaymentExpense.Ref AS Ref,
	|	PaymentExpense.BankAccount AS BankAccount,
	|	PaymentExpense.CounterpartyAccount AS CounterpartyAccount,
	|	PaymentExpense.Company AS Company,
	|	PaymentExpense.Counterparty AS Counterparty,
	|	PaymentExpense.DocumentAmount AS DocumentAmount,
	|	PRESENTATION(PaymentExpense.CashCurrency) AS CashCurrency,
	|	PaymentExpense.PaymentPurpose AS PaymentPurpose,
	|	PaymentExpense.Number AS Number,
	|	PaymentExpense.Date AS Date,
	|	PaymentExpense.OperationKind AS OperationKind,
	|	PaymentExpense.AdvanceHolder AS AdvanceHolder
	|INTO DocumentsTable
	|FROM
	|	Document.PaymentExpense AS PaymentExpense
	|WHERE
	|	PaymentExpense.Ref IN(&Ref)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	DocumentsTable.Ref AS Ref,
	|	DocumentsTable.BankAccount AS BankAccount,
	|	CASE
	|		WHEN CompanyBankAccount.IBAN = """"
	|			THEN CompanyBankAccount.AccountNo
	|		ELSE CompanyBankAccount.IBAN
	|	END AS BankAccountNo,
	|	Companies.DescriptionFull AS CompanyDescriptionFull,
	|	CASE
	|		WHEN DocumentsTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.ToAdvanceHolder)
	|				OR DocumentsTable.OperationKind = VALUE(Enum.OperationTypesPaymentExpense.IssueLoanToEmployee)
	|			THEN Employees.Description
	|		ELSE Counterparties.DescriptionFull
	|	END AS CounterpartyDescriptionFull,
	|	CounterpartyBank.Code AS CounterpartyAccountBankCode,
	|	CounterpartyBankAccount.AccountNo AS CounterpartyAccountNo,
	|	DocumentsTable.DocumentAmount AS DocumentAmount,
	|	PRESENTATION(DocumentsTable.CashCurrency) AS CashCurrency,
	|	DocumentsTable.PaymentPurpose AS PaymentPurpose,
	|	DocumentsTable.Number AS Number,
	|	DocumentsTable.Date AS Date,
	|	CounterpartyBankAccount.IBAN AS CounterpartyIBAN,
	|	CounterpartiesContactInformation.Presentation AS CounterpartyAddress,
	|	CompaniesContactInformation.Presentation AS CompanyAddress,
	|	CompanyBankAccount.Bank AS CompanyBank,
	|	CompanyBankAccount.Bank AS BankAccountBankCode
	|FROM
	|	DocumentsTable AS DocumentsTable
	|		LEFT JOIN Catalog.BankAccounts AS CompanyBankAccount
	|		ON DocumentsTable.BankAccount = CompanyBankAccount.Ref
	|		LEFT JOIN Catalog.BankAccounts AS CounterpartyBankAccount
	|		ON DocumentsTable.CounterpartyAccount = CounterpartyBankAccount.Ref
	|		LEFT JOIN Catalog.Banks AS CompanyBank
	|		ON (CompanyBankAccount.Bank = CompanyBank.Ref)
	|		LEFT JOIN Catalog.Banks AS CounterpartyBank
	|		ON (CounterpartyBankAccount.Bank = CounterpartyBank.Ref)
	|		LEFT JOIN Catalog.Companies AS Companies
	|		ON DocumentsTable.Company = Companies.Ref
	|		LEFT JOIN Catalog.Companies.ContactInformation AS CompaniesContactInformation
	|		ON (Companies.Ref = CompaniesContactInformation.Ref)
	|			AND (CompaniesContactInformation.Kind = VALUE(Catalog.ContactInformationKinds.CompanyLegalAddress))
	|		LEFT JOIN Catalog.Counterparties AS Counterparties
	|		ON DocumentsTable.Counterparty = Counterparties.Ref
	|		LEFT JOIN Catalog.Counterparties.ContactInformation AS CounterpartiesContactInformation
	|		ON (Counterparties.Ref = CounterpartiesContactInformation.Ref)
	|			AND (CounterpartiesContactInformation.Kind = VALUE(Catalog.ContactInformationKinds.CounterpartyLegalAddress))
	|		LEFT JOIN Catalog.Employees AS Employees
	|		ON DocumentsTable.AdvanceHolder = Employees.Ref
	|TOTALS
	|	MAX(BankAccountNo),
	|	MAX(CompanyDescriptionFull),
	|	MAX(CompanyAddress),
	|	MAX(BankAccountBankCode)
	|BY
	|	BankAccount";
	
	Query.SetParameter("Ref", Parameters.DocumentsForExportTable.UnloadColumn("Document"));
	
	QueryResult = Query.Execute();
	BankAccountSelection = QueryResult.Select(QueryResultIteration.ByGroups);
	
	While BankAccountSelection.Next() Do
	
		AddTagAndText(ResultText, "20",		Format(CurrentSessionDate(), "DF=yyyyMMddhhmmss"));
		AddTagAndText(ResultText, "28D",	"1/1");
		
		Text = "/" + BankAccountSelection.BankAccountNo + Chars.LF
				+ Left(BankAccountSelection.CompanyDescriptionFull, 35);
		If ValueIsFilled(BankAccountSelection.CompanyAddress) Then
			Text = Text + Chars.LF + SplitText(BankAccountSelection.CompanyAddress, 3, 35);
		EndIf;
				
		AddTagAndText(ResultText, "50H",	Text);
		AddTagAndText(ResultText, "52A",	BankAccountSelection.BankAccountBankCode);
		AddTagAndText(ResultText, "30",		Format(CurrentSessionDate(), "DF=yyMMdd"));
		
		DocumentSelection = BankAccountSelection.Select();
		
		While DocumentSelection.Next() Do
			
			IsCrossBorderPayment = Mid(DocumentSelection.BankAccountBankCode, 5, 2) <> Mid(DocumentSelection.CounterpartyAccountBankCode, 5, 2);
			
			Text = Format(DocumentSelection.Date, "DF=yyMMddhhmm")
					+ ObjectPrefixationClientServer.GetNumberForPrinting(DocumentSelection.Number, True, True);
			AddTagAndText(ResultText, "21",	Text);
			
			Text = "";
			
			If IsCrossBorderPayment Then
				AddTagAndText(ResultText, "23E", "RTGS");
			EndIf;
			
			AddTagAndText(ResultText, "32B",	DocumentSelection.CashCurrency + Format(DocumentSelection.DocumentAmount,
																						"ND=16; NFD=2; NDS=,; NG=0"));
			AddTagAndText(ResultText, "57A",	DocumentSelection.CounterpartyAccountBankCode);
			
			Text = "/" + ?(IsCrossBorderPayment, DocumentSelection.CounterpartyIBAN, DocumentSelection.CounterpartyAccountNo)
					+ Chars.LF + Left(DocumentSelection.CounterpartyDescriptionFull, 35);
			If ValueIsFilled(DocumentSelection.CounterpartyAddress) Then
				Text = Text + Chars.LF + SplitText(DocumentSelection.CounterpartyAddress, 3, 35);
			EndIf;
			
			AddTagAndText(ResultText, "59",	Text);
			Text = SplitText(DocumentSelection.PaymentPurpose, 4, 35);
			AddTagAndText(ResultText, "70",	Text);
			
			If Not IsCrossBorderPayment Then
				Text = "OUR";
			Else
				Text = "SHA";
			EndIf;
			
			AddTagAndText(ResultText, "71A", Text);
			
		EndDo;
		
	EndDo;
	
	Return GetBinaryDataFromString(ResultText, Encoding);
	
EndFunction

Procedure AddTagAndText(ResultText, Tag, Text)
	ResultText = ResultText + ":" + Tag + ":" + Text + Chars.LF;
EndProcedure

Procedure ReadTXTFileToTable(Text, VT, SettingsRef, ResultStructure) Export
	
	If StrFind(Text, ":20:") = 0 Then
		ResultStructure.Done = False;
		CommonClientServer.AddUserError(ResultStructure.Errors,
			"",
			NStr("en = 'File format is invalid. Unable to find section :20:';
				|ru = 'Формат файла не корректен. Секция :20: не найдена'"),
			"");
		Return;
	EndIf;
	
	Try
	
		//Split file by ID / bank accounts
		BankAccountArray	= StringFunctionsClientServer.SplitStringIntoSubstringsArray(Text, ":20:", True, True);
		BankAccountNumber	= "";
		CompanyTable		= GetCompanyTable();
		
		For Each BankAccount In BankAccountArray Do
			
			//Cut the tail
			BankAccount = Mid(BankAccount, 0, StrFind(BankAccount, ":62", SearchDirection.FromEnd) - 1);
			
			//Split file to transactions
			TransactionArray = StringFunctionsClientServer.SplitStringIntoSubstringsArray(BankAccount, ":61:", True, True);
			
			//The first item considers account data
			BankAccDataArray = StringFunctionsClientServer.SplitStringIntoSubstringsArray(TransactionArray[0], ":", True, True);
			
			BankAccountNumber = BankAccDataArray[BankAccDataArray.Find("25") + 1];
			
			TransactionArray.Delete(0);
			
			For Each Transaction In TransactionArray Do
				
				DataArray	= StringFunctionsClientServer.SplitStringIntoSubstringsArray(Transaction, ":", True, True);
				Iterator	= 1;
				ArrayCount	= DataArray.Count();
				DC			= "";
				
				NewRow = VT.Add();
				NewRow.BankAccount	= BankAccountNumber;
				
				ReadingField61(NewRow, DataArray[0], DC);
				
				While Iterator < ArrayCount Do
					
					DataField = DataArray[Iterator];
					
					If DataField = "86" Then
						
						Iterator = Iterator + 1;
						If Iterator < ArrayCount Then
							ReadingField86(NewRow, DataArray[Iterator], DC, CompanyTable, SettingsRef);
						Else
							Break;
						EndIf;
						
					EndIf;
					
					Iterator = Iterator + 1;
					
				EndDo;
				
			EndDo;
			
		EndDo;
		
	Except
		ResultStructure.Done = False;
		CommonClientServer.AddUserError(ResultStructure.Errors,
			"",
			StringFunctionsClientServer.SubstituteParametersToString(
				NStr("en = 'File format is invalid. Details: %1';
					|ru = 'Формат файла не корректен. Подробности: %1'"),
				BriefErrorDescription(ErrorInfo())),
			"");
	EndTry;

EndProcedure

Function GetCompanyTable()
	
	Query = New Query;
	Query.Text =
	"SELECT DISTINCT
	|	Companies.TIN AS TIN
	|FROM
	|	Catalog.Companies AS Companies
	|WHERE
	|	NOT Companies.DeletionMark
	|	AND Companies.TIN <> """"";
	
	Return Query.Execute().Unload();
	
EndFunction

Function SplitText(SourceText, LineCount, AvailableNumberOfCharacters)
	
	If Not ValueIsFilled(SourceText) Then
		Return SourceText;
	EndIf;
		
	WordArray = StringFunctionsClientServer.SplitStringIntoWordArray(SourceText, " ");
	Text = "";
	IsFirstWord = True;
	LineNumber = 1;
	
	For Each Word In WordArray Do
		
		WordWithSpace = " " + Word;
		NumberOfCharacters = StrLen(StrGetLine(Text + WordWithSpace, LineNumber));

		If StrLineCount(Text + WordWithSpace) > LineCount Then
			Return Text;
		ElsIf NumberOfCharacters > AvailableNumberOfCharacters Then
			Text = Text + Chars.LF;
			LineNumber = LineNumber + 1;
			Text = Text + Word;
		Else
			Text = Text + ?(IsFirstWord, Word, WordWithSpace);
		EndIf;
		
		If IsFirstWord Then
			IsFirstWord = False;
		EndIf;
	EndDo;
	
	Return Text;
	
EndFunction

Procedure ReadingField61(Row, TextString, DC)
	
	Year		= 2000 + Number(Left(TextString, 2));
	Month		= Number(Mid(TextString, 3, 2));
	Day			= Number(Mid(TextString, 5, 2));
	MonthExt	= Number(Mid(TextString, 7, 2));
	DayExt		= Number(Mid(TextString, 9, 2));
	DC			= Mid(TextString, 11, 1);
	
	Row.Received				= Date(Year, Month,		Day,	12, 0, 0);
	Row.PaymentDate				= Date(Year, Month,		Day,	12, 0, 0);
	Row.ExternalDocumentDate	= Date(Year, MonthExt,	DayExt,	12, 0, 0);
	
	NextReadingPosition	= 12;
	
	If DC = "D" Then
		Row.OperationKind	= Enums.OperationTypesPaymentExpense.Vendor;
		Row.DocumentKind	= "PaymentExpense";
	ElsIf DC = "C" Then
		Row.OperationKind	= Enums.OperationTypesPaymentReceipt.FromCustomer;
		Row.DocumentKind	= "PaymentReceipt";
	ElsIf DC = "R" Then
		NextReadingPosition = 13;
	EndIf;
	
	CurrencyCode = Mid(TextString, NextReadingPosition, 1);
	
	If CurrencyCode = "0"
		OR CurrencyCode = "1"
		OR CurrencyCode = "2"
		OR CurrencyCode = "3"
		OR CurrencyCode = "4"
		OR CurrencyCode = "5"
		OR CurrencyCode = "6"
		OR CurrencyCode = "7"
		OR CurrencyCode = "8"
		OR CurrencyCode = "9" Then
		
		FirstCommaPosition = StrFind(TextString, ",",, NextReadingPosition);
		Row.Amount = Number(
			TrimAll(
				Mid(TextString,
					NextReadingPosition,
					FirstCommaPosition - NextReadingPosition)) +
			"." +
			TrimAll(
				Mid(TextString,
					FirstCommaPosition + 1,
					2)));
		
	EndIf;
	
	NextReadingPosition = FirstCommaPosition + 3;
	
	SlashPosition	= StrFind(TextString, "//",, NextReadingPosition);
	EndPosition		= Max(StrFind(TextString, Chars.LF,, NextReadingPosition), StrFind(TextString, Chars.CR,, NextReadingPosition));
	
	Row.ExternalDocumentNumber = Mid(TextString, NextReadingPosition,
									?(SlashPosition = 0,
										EndPosition - NextReadingPosition,
										SlashPosition - NextReadingPosition));
	
EndProcedure

Procedure ReadingField86(Row, TextString, DC, CompanyTable, SettingsRef)

	StrArray					= StrSplit(TextString, "+");
	StrArrayCount				= StrArray.Count() - 1;
	IsCounterpartyOperations	= False;
	IsOtherOperations			= True;
	IsTaxes						= False;
	IsSalary					= False;
	Is31						= False;
	Is32						= False;
	Is33						= False;
	OurTIN						= "";
	
	AdditionalSettings = Catalogs.BankStatementProcessingSettings.GetAdditionalSettingsStructure(SettingsRef);
	
	For i = 0 to StrArrayCount Do
		
		OpCode = Left(StrArray[i], 2);
		
		If OpCode = "31" Then
			
			IsCounterpartyOperations	= True;
			IsOtherOperations			= False;
			Is31						= True;
			
			Row.CounterpartyBankAccount = TrimAll(Mid(StrArray[i], 3));
			
		ElsIf OpCode = "32" Then
			
			IsCounterpartyOperations	= True;
			IsOtherOperations			= False;
			Is32						= True;
			
			Row.Counterparty = TrimAll(Mid(StrArray[i], 3));
			
		ElsIf OpCode = "33" Then
			
			IsCounterpartyOperations	= True;
			IsOtherOperations			= False;
			Is33						= True;
			
			CurTIN = TrimAll(Mid(StrArray[i], 10));
			
			OurTIN = CompanyTable.Find(CurTIN, "TIN");
			
			If OurTIN <> Undefined
				AND (CurTIN = OurTIN
					OR CurTIN = "") Then
					
				Row.OperationKind	= Enums.OperationTypesPaymentExpense.Taxes;
				IsTaxes				= True;
				
			EndIf;
				
			Row.CounterpartyTIN = ?(IsTaxes OR DC = "C", OurTIN, CurTIN);
			
		EndIf;
		
	EndDo;
	
	Row.PaymentPurpose = TextString;
	
	If Is31 AND NOT Is32 AND NOT Is33 Then
		IsSalary					= True;
		IsCounterpartyOperations	= False;
	EndIf;
		
	If IsCounterpartyOperations Then
		
		If DC = "D" Then
			Row.OperationKind = Enums.OperationTypesPaymentExpense.Vendor;
		ElsIf DC = "C" Then
			Row.OperationKind = Enums.OperationTypesPaymentReceipt.FromCustomer;
		EndIf;
		
	EndIf;
	
	If IsOtherOperations Then
		
		If DC = "D" Then
			Row.OperationKind = Enums.OperationTypesPaymentExpense.Other;
		ElsIf DC = "C" Then
			Row.OperationKind = Enums.OperationTypesPaymentReceipt.Other;
		EndIf;
		
		Row.CounterpartyTIN = OurTIN;
		
	EndIf;
	
	If IsSalary Then
		
		If DC = "D" Then
			Row.OperationKind = Enums.OperationTypesPaymentExpense.Salary;
		ElsIf DC = "C" Then
			Row.OperationKind = Enums.OperationTypesPaymentReceipt.Other;
		EndIf;
		
		Row.CounterpartyTIN = OurTIN;
		
	EndIf;
	
	If Row.OperationKind = Enums.OperationTypesPaymentExpense.Vendor Then
		Row.CFItem			= AdditionalSettings.CF_item_to_supplier;
	ElsIf Row.OperationKind = Enums.OperationTypesPaymentReceipt.FromCustomer Then
		Row.CFItem			= AdditionalSettings.CF_item_from_customer;
	ElsIf Row.OperationKind = Enums.OperationTypesPaymentExpense.Other Then
		Row.CFItem			= AdditionalSettings.CF_item_other;
	EndIf;
	
EndProcedure

#EndRegion
