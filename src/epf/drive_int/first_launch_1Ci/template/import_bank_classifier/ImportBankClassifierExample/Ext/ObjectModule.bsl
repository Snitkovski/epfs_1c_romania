﻿
// <Manual>
//
//This additional data processor is an example for importing bank classifier.
//The data processor imports the test data, which can be used by the developers as an example.
//The partners using this example can create their own additional data processors, which will be used at their location.
//
//All settings should be set in the procedure "OnDefineSettings()".
//If you use import from Web you should handle reading classifier or writing items in the procedure "ImportDataFromWeb()".
//If you use import from file you should handle reading classifier or writing items in the procedure "ImportDataFromFile()".
#Region ProgramInterface

Function ExternalDataProcessorInfo() Export

	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind = AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindAdditionalDataProcessor();
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= "Import bank classifier example";
	RegistrationParameters.Information	= NStr("en = 'Data processor / example for import bank classifier'; ru = 'Внешняя обработка / пример для загрузки классификатора банков'");
	RegistrationParameters.SafeMode		= False;
	
    Return RegistrationParameters;
	
EndFunction

#EndRegion

#Region DataProcessorsSettings

// Generates fields structure for settings.
// If you use import from Web you should set "UseImportFromWeb" to "True"
// If you use import from the file you should set "UseImportFromFile" to "True"
// You can use both methods.
//
// If you use import from Web you need to set:
//  - Protocol;
//  - Port;
//  - ServerSource;
//  - Address;
//  - ClassifierFileOnWeb;
//
// Returns:
//   Settings - Structure - Additional data processor settings
//
Procedure OnDefineSettings(Settings) Export

	Settings.Insert("UseImportFromWeb", 	True);
	Settings.Insert("UseImportFromFile", 	True);
	Settings.Insert("Protocol", 			"HTTPS");
	Settings.Insert("Port", 				Undefined);
	Settings.Insert("ServerSource", 		"www.ecb.europa.eu");
	Settings.Insert("Address", 				"stats/money/mfi/general/html/dla/mfi_MID");
	WeekDay = WeekDay(CurrentDate());
	If WeekDay>1 And WeekDay<7 Then
		Coefficient = 1;
	ElsIf WeekDay = 7 Then
		Coefficient = 2;
	ElsIf WeekDay = 1 Then
		Coefficient = 3;
	EndIf;
	PreviousDate = Format(CurrentDate()-86400*Coefficient,"DF=dd.MM.yy");
	ArrayDate = StrSplit(PreviousDate,".",False);
	Settings.Insert("ClassifierFileOnWeb",	"mfi_csv_"+ArrayDate[2]+ArrayDate[1]+ArrayDate[0]+".csv");
	
EndProcedure // GetSettings()

// Receives, sorts, writes banks classifier data from Web.
// 
// Parameters:
// ClassifierImportParameters - Map:
// Exported						- Number	 - Classifier new records quantity.
// Updated						- Number	 - Quantity of updated classifier records.
// MessageText					- String - import results message text.
// ImportCompleted              - Boolean - check box of successful classifier data import end.
// StorageAddress				- String - internal storage address.
Procedure ImportDataFromWeb(ClassifierImportParameters, StorageAddress) Export
	
	TemporaryDirectory = GetTempFileName();
	CreateDirectory(TemporaryDirectory);
	
	FilesReceivingParameters = New Map;
	FilesReceivingParameters.Insert("PathToFile", "");
	FilesReceivingParameters.Insert("MessageText", ClassifierImportParameters["MessageText"]);
	FilesReceivingParameters.Insert("TemporaryDirectory", TemporaryDirectory);
	
	GetDataFromInternet(FilesReceivingParameters);
	
	If Not IsBlankString(FilesReceivingParameters["MessageText"]) Then
		ClassifierImportParameters.Insert("MessageText", FilesReceivingParameters["MessageText"]);
		If Not IsBlankString(StorageAddress) Then
			PutToTempStorage(ClassifierImportParameters, StorageAddress);
		EndIf;
		Return;
	EndIf;
	
	PathToFile = FilesReceivingParameters["PathToFile"];
	File	   = New File(PathToFile);
	If Not File.Exist() Then
		MessageText = NStr("en='The problems occurred with the banks classifier file obtained form the website.
		|File is missing';
		|ru='Возникли проблемы с файлом классификатора банков, полученным с сайта.
		|Файл не найден'");
		ClassifierImportParameters.Insert("MessageText", MessageText);
		If Not IsBlankString(StorageAddress) Then
			PutToTempStorage(ClassifierImportParameters, StorageAddress);
		EndIf;
		Return;
	EndIf;
	
	FilesImportingParameters = New Map;
	FilesImportingParameters.Insert("PathToFile", PathToFile);
	FilesImportingParameters.Insert("TemporaryDirectory", TemporaryDirectory);
	FilesImportingParameters.Insert("Exported", ClassifierImportParameters["Exported"]);
	FilesImportingParameters.Insert("Updated", ClassifierImportParameters["Updated"]);
	FilesImportingParameters.Insert("MessageText", ClassifierImportParameters["MessageText"]);
	
	ImportData(FilesImportingParameters);
	
	If Not IsBlankString(FilesImportingParameters["MessageText"]) Then
		If Not IsBlankString(StorageAddress) Then
			PutToTempStorage(ClassifierImportParameters, StorageAddress);
		EndIf;
		Return;
	EndIf;
	
	DeleteFiles(TemporaryDirectory);
	
	ClassifierImportParameters.Insert("Exported", FilesImportingParameters["Exported"]);
	ClassifierImportParameters.Insert("Updated", FilesImportingParameters["Updated"]);
	ClassifierImportParameters.Insert("MessageText", FilesImportingParameters["MessageText"]);
	ClassifierImportParameters.Insert("ImportCompleted", True);
	
EndProcedure

// Receives, writes classifier data from file.
// 
// Parameters:
// FilesImportingParameters		 - Map:
// Exported						 - Number		      - Classifier new records quantity.
// Updated						 - Number			  - Quantity of updated classifier records.
// MessageText					 - String			  - import results message text.
// ImportCompleted                - Boolean             - check box of successful classifier data import end.
//
Procedure ImportDataFromFile(FilesImportingParameters, StorageAddress) Export
	
	ImportData(FilesImportingParameters);
	
EndProcedure

#EndRegion

#Region ServiceProgramInterface

// Get file from Website or WebService
// Parameters:
// FilesReceivingParameters - Map:
// PathToFile				- String - path to the received file located in the temporary directory.
// TemporaryDirectory			- String - path to the temporary directory.
//  MessageText				- String - error message text.
Procedure GetDataFromInternet(FilesReceivingParameters)

	Settings = BankManager.Settings();
	
	MessageText 		= "";
	PathToFile  		= "";
	Protocol 			= Settings.Protocol;
	ServerSource 		= Settings.ServerSource;
	Address          	= Settings.Address;
	ClassifierFileOnWeb = Settings.ClassifierFileOnWeb;
		
	FileOnWebServer  = Protocol + "://" + ServerSource + "/" + Address+ "/" + ClassifierFileOnWeb;
	TemporaryFile	 = FilesReceivingParameters["TemporaryDirectory"]+ "\" + ClassifierFileOnWeb;
	ReceivingParameters	 = New Structure("PathForSave");
	ReceivingParameters. Insert("PathForSave", TemporaryFile);
	ResultFromInternet = GetFilesFromInternet.DownloadFileAtServer(FileOnWebServer, ReceivingParameters);
   		
	If ResultFromInternet.Status Then
		
		FilesReceivingParameters.Insert("PathToFile", ResultFromInternet.Path);
		
	Else
		If Common.FileInfobase() Then
			AdditionalMessage =
				NStr("en='
		|Perhaps, the settings of the Internet connection are inaccurate or incorrect';
		|ru='Возможно неточные или неправильные настройки подключения к Интернету.'");
		Else
			AdditionalMessage =
				NStr("en='
		|Perhaps, the Internet connection settings on the 1C:Enterprise server are inaccurate or incorrect.';
		|ru='Возможно неточные или неправильные настройки подключения к Интернету на сервере 1С:Предприятие.'");
		EndIf;
		
		ErrorInfo = ResultFromInternet.ErrorInfo + AdditionalMessage;
		
		FilesReceivingParameters.Insert("MessageText", ErrorInfo);
	EndIf;
		  	
EndProcedure

// Receives, writes classifier data 
// 
// Parameters:
// FilesImportingParameters - Map:
//  PathToFile		   - String - path to the file with classifier data placed in the temporary directory.
// Exported				   - Number	- classifier new records quantity.
// Updated				   - Number	- quantity of updated classifier records.
// MessageText			   - String	- import results message text.
// ImportCompleted          - Boolean - check box of successful classifier data import end.
//
Procedure ImportData(FilesImportingParameters)
	
	BinaryDataAddress = FilesImportingParameters["DataAddress"];
	If BinaryDataAddress <> Undefined Then
		PathToFile = GetTempFileName();
		BinaryDataAddress.Get().Write(PathToFile);
	Else
		PathToFile = FilesImportingParameters["PathToFile"];
	EndIf;
	
	Text	   = New TextReader(PathToFile);
	TextString = Text.ReadLine();
	TextString = Text.ReadLine();
	
	ImportDate = CurrentUniversalDate();
	Countries = ContactsManager.ClassifierTable();
	
	DataImportingParameters = New Map;
	DataImportingParameters.Insert("Exported", FilesImportingParameters["Exported"]);
	DataImportingParameters.Insert("Updated", FilesImportingParameters["Updated"]);
	
	While TextString <> Undefined Do
		
		String = TextString;
	
		If IsBlankString(TrimAll(String)) Then
			Continue;
		EndIf;
		
		StructureBank  = GetBankFieldsStructure(String, Countries);
		TextString = Text.ReadLine();
		
		If IsBlankString(StructureBank) Then
			Continue;
		EndIf;
		
		DataImportingParameters.Insert("StructureBank", StructureBank);
		WriteBankClassifierItem(DataImportingParameters);
		
	EndDo;
	
	FilesImportingParameters.Insert("Exported", DataImportingParameters["Exported"]);
	FilesImportingParameters.Insert("Updated", DataImportingParameters["Updated"]);
	
EndProcedure

// Generates fields structure for bank.
// Parameters:
// String  - String	   - String from the classifier text file.
// States - Map - State code and bank region.
// Returns:
// Bank - Structure - Bank details.
//
Function GetBankFieldsStructure(Val String, Countries)
	
	Bank		= New Structure;
	Delimiter = Chars.Tab;
			
	Description  = "";
	SWIFT		 = "";
	City 		 = "";
	Adress 		 = "";
	Country      = "";
	BalancedAccount		 = "";
	
	SubstringArray = StringFunctionsClientServer.SplitStringIntoSubstringsArray(String, Delimiter);
	
	If SubstringArray[8] <> "Credit Institution" Then
		Return "";
	EndIf;
	
	Item = TrimAll(SubstringArray[1]);
	AllRowData = Countries.Find(TrimAll(SubstringArray[2]),"CodeAlpha2");
	If AllRowData<>Undefined Then
		RowData = New Structure("Code, Description, LongDescription, CodeAlpha2, CodeAlpha3");
		FillPropertyValues(RowData, AllRowData);
		
		CountryData = GetCountry(RowData);
		Country = CountryData.Ref;
	EndIf;
	Description	= TrimAll(SubstringArray[3]);
	Adress		= TrimAll(SubstringArray[5]);
	Box			= TrimAll(SubstringArray[4]);
	Postal		= TrimAll(SubstringArray[6]);
	SWIFT		= TrimAll(SubstringArray[1]);
    City		= TrimAll(SubstringArray[7]);

	Bank.Insert("SWIFT",		SWIFT);
	Bank.Insert("Description",	Description);
	Bank.Insert("City",			City);
	Bank.Insert("Country",		Country);
	Bank.Insert("PhoneNumbers",	"");
	Bank.Insert("Address",		?(ValueIsFilled(Postal),Postal+", ","") +Adress+?(ValueIsFilled(Box),", "+Box,""));
	
	Return Bank;
	
EndFunction

//  Writes/overwrites bank data to the BankClassifier catalog.
// Parameters:
// DataImportingParameters - Map:
// StructureBank			- Structure or ValueTableRow - Bank data.
// Exported				- Number								 - Classifier new records quantity.
// Updated				- Number								 - Quantity of updated classifier records.
//
Procedure WriteBankClassifierItem(DataImportingParameters)
	
	FlagNew		= False;
	FlagUpdated = False;
	
	StructureBank = DataImportingParameters["StructureBank"];
	Exported	  = DataImportingParameters["Exported"];
	Updated	  = DataImportingParameters["Updated"];
	
	WrittenBankClassifierCatalogItem =
		Catalogs.BankClassifier.FindByCode(StructureBank.SWIFT);
	
	If WrittenBankClassifierCatalogItem.IsEmpty() Then
		BankClassifierObject = Catalogs.BankClassifier.CreateItem();
		FlagNew				  = True;
	Else
		BankClassifierObject = WrittenBankClassifierCatalogItem.GetObject();
	EndIf;
	
	If BankClassifierObject.OutOfBusiness Then
		BankClassifierObject.OutOfBusiness = False;
	EndIf;
	
	If BankClassifierObject.Code <> StructureBank.SWIFT Then
		BankClassifierObject.Code = StructureBank.SWIFT;
	EndIf;
    	
	If BankClassifierObject.Description <> StructureBank.Description Then
		If Not IsBlankString(StructureBank.Description) Then
        	BankClassifierObject.Description = StructureBank.Description;
		EndIf;
	EndIf;
	
	If BankClassifierObject.City <> StructureBank.City Then
		If Not IsBlankString(StructureBank.City) Then
			BankClassifierObject.City = StructureBank.City;
		EndIf;
	EndIf;
			
	If BankClassifierObject.Address <> StructureBank.Address Then
		If Not IsBlankString(StructureBank.Address) Then
			BankClassifierObject.Address = StructureBank.Address;
		EndIf;
	EndIf;
	
	If BankClassifierObject.Phones <> StructureBank.PhoneNumbers Then
		If Not IsBlankString(StructureBank.PhoneNumbers) Then
			BankClassifierObject.Phones = StructureBank.PhoneNumbers;
		EndIf;
	EndIf;
	
	If BankClassifierObject.Country <> StructureBank.Country Then
		If Not IsBlankString(StructureBank.Country) Then
			BankClassifierObject.Country = StructureBank.Country;
		EndIf;
	EndIf;
	
	If BankClassifierObject.Modified() Then
		FlagUpdated		  = True;
		BankClassifierObject.Write();
	EndIf;
	
	If FlagNew Then
		Exported = Exported + 1;
	ElsIf FlagUpdated Then
		Updated = Updated + 1;
	EndIf;
	
	DataImportingParameters.Insert("Exported", Exported);
	DataImportingParameters.Insert("Updated", Updated);
	
EndProcedure

Function GetCountry(Val CountryData)
	// Searching by code only, because all codes are specified in the classifier
	Ref = Catalogs.WorldCountries.FindByCode(CountryData.Code);
	IsNew = Not ValueIsFilled(Ref);
	If IsNew Then
		Country = Catalogs.WorldCountries.CreateItem();
		FillPropertyValues(Country, CountryData);
		Country.Write();
		Ref = Country.Ref;
	EndIf;
	
	Return New Structure("Ref, IsNew, Code", Ref, IsNew, CountryData.Code);
EndFunction

#EndRegion
