﻿
&AtClient
Procedure CommandOpenFile(Command)
    
   ND = New NotifyDescription("EndingPutFileToServer", ThisObject);
   
   Dialog = New PutFilesDialogParameters;
   Dialog.MultipleChoice = False;
   Dialog.Filter = "EXCEL 2007-2019|*.xlsx|EXCEL 97-2003|*.xls|CSV file|*.csv";
   Dialog.Title = NStr("en = 'Choose file'; ro = 'Selectați fișierul'; ru = 'Выберите файл'");
   
   BeginPutFileToServer(ND, , , , Dialog, UUID);
    
EndProcedure

&AtClient
Procedure EndingPutFileToServer(PlacedFileDescription, AdditionalParameters) Export
    
    If PlacedFileDescription = Undefined
        OR PlacedFileDescription.PutFileCanceled Then
        Return
    EndIf;
    
    ReadFile(PlacedFileDescription.Address, PlacedFileDescription.FileRef.Extension);
    
EndProcedure

&AtServer
Function GetDataFromSpreadsheetDocument(Source)
    
    If Source.GetDataAreaHorizontalSize() = 0 Then
        Return Undefined
    EndIf;
    
    Builder = New QueryBuilder;
    Builder.DataSource = New DataSourceDescription(Source.Area());
    Builder.Execute();
    
    Query = New Query;
    Query.SetParameter("tmpTab", Builder.Result.Unload());
    
    Query.Text =
	"SELECT
     |  tmpTab.SKU_Produsului_Principal AS SKU_Produsului_Principal,
     |  tmpTab.SKU_Produsului_Addon AS SKU_Produsului_Addon,
     |  tmpTab.Obligatoriu AS Obligatoriu
     |INTO Source
     |FROM
     |  &tmpTab AS tmpTab
     |;
     |
     |////////////////////////////////////////////////////////////////////////////////
     |SELECT
     |  Source.SKU_Produsului_Principal AS SKU_Produsului_Principal,
     |  P_Master.Ref AS SKU_Produsului_Principal_Ref,
     |  Source.SKU_Produsului_Addon AS SKU_Produsului_Addon,
     |  P_Slave.Ref AS SKU_Produsului_Addon_Ref,
     |  Source.Obligatoriu AS Obligatoriu
     |FROM
     |  Source AS Source
     |      LEFT JOIN Catalog.Products AS P_Master
     |      ON Source.SKU_Produsului_Principal = P_Master.SKU
     |      LEFT JOIN Catalog.Products AS P_Slave
     |      ON Source.SKU_Produsului_Addon = P_Slave.SKU
     |;
     |
     |////////////////////////////////////////////////////////////////////////////////
     |DROP Source";
    
    Return Query.Execute();
    
EndFunction

&AtServer
Function GetSpreadsheetDocumentFromCSV(Source)
    
    SpdrShtDoc = New SpreadsheetDocument;
    
    ReadText = New TextReader(Source);
    
    Line = ReadText.ReadLine();
    
    While Not Line = Undefined Do
        LineArray = StrSplit(Line, ";");
        SpdrShtDocLine = SpdrShtDoc.GetArea(1, 1, 1, LineArray.Count());
        ColumnIndex = 1;
        
        For Each ArrayValue In LineArray Do
            Area = SpdrShtDocLine.Area(1, ColumnIndex, 1, ColumnIndex);
            Area.Text = ArrayValue;
            ColumnIndex = ColumnIndex + 1
        EndDo;
        
        SpdrShtDoc.Put(SpdrShtDocLine);
        Line = ReadText.ReadLine();
    EndDo;
    
    ReadText.Close();
    
    Return SpdrShtDoc;
    
EndFunction

&AtServer
Procedure ReadFile(Adress,FileExtention)
    
    tmpFile = GetTempFileName(FileExtention);
    
    GetFromTempStorage(Adress).Write(tmpFile);
    
    If Upper(FileExtention) = ".CSV" Then
        SpdrShtDoc = GetSpreadsheetDocumentFromCSV(tmpFile)
    Else
        SpdrShtDoc = New SpreadsheetDocument;
        SpdrShtDoc.Read(tmpFile);
    EndIf;
    
    Result = GetDataFromSpreadsheetDocument(SpdrShtDoc);
    
    If Result = Undefined Then
        Return
    EndIf;
    
    FileContent.Clear();
    SpdrShtDoc.Clear();
    ColumnCount = Result.Columns.Count();
    
    SpdrShtDocHead = FileContent.GetArea(1, 1, 1, ColumnCount);
    SpdrShtDocLine = FileContent.GetArea(2, 1, 2, ColumnCount);
    
    For Each Column In Result.Columns Do
        
        ColumnIndex = Result.Columns.IndexOf(Column) + 1;
        
        Area = SpdrShtDocHead.Area(1, ColumnIndex, 1, ColumnIndex);
        Area.Text = Column.Name;
        
        Area = SpdrShtDocLine.Area(1, ColumnIndex, 1, ColumnIndex);
        Area.FillType = SpreadsheetDocumentAreaFillType.Parameter;
        Area.Parameter = Column.Name;
        
    EndDo;
    
    FileContent.Put(SpdrShtDocHead);
    Selection = Result.Select();
    
    While Selection.Next() Do
        Area = SpdrShtDocLine.Area(1, 1, 1, ColumnCount);
        SpdrShtDocLine.Parameters.Fill(Selection);
        If Selection.SKU_Produsului_Principal_Ref = NULL
            OR Selection.SKU_Produsului_Addon_Ref = NULL Then
            Area.BackColor = WebColors.Salmon;
        Else
            Area.BackColor = WebColors.White;
        Endif;
		
		FileContent.Put(SpdrShtDocLine);
    EndDo;
    
EndProcedure

&AtServer
Procedure FillingDataAtServer()
    
    Result = GetDataFromSpreadsheetDocument(FileContent);
    
    If Result = Undefined Then
        Return
    EndIf;
    
    Selection = Result.Select();
    
    While Selection.Next() Do
        If Selection.SKU_Produsului_Principal_Ref = NULL
            OR Selection.SKU_Produsului_Addon_Ref = NULL Then
            Continue;
        Endif;
        
        RM = InformationRegisters.ConcomitantProducts.CreateRecordManager();
        RM.Products = Selection.SKU_Produsului_Principal_Ref;
        RM.ConcomitantProducts = Selection.SKU_Produsului_Addon_Ref;
        RM.Required = Upper(TrimAll(Selection.Obligatoriu)) = "YES";
        
        Try
            RM.Write()
        Except
            CommonClientServer.MessageToUser(BriefErrorDescription(ErrorDescription()))
        EndTry;
    EndDo;
    
EndProcedure

&AtClient
Procedure FillingData(Command)
    FillingDataAtServer();
    ShowUserNotification("Done");
    CommonClientServer.MessageToUser("Done");
EndProcedure
