﻿/////////////////////////////////// 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.SalesOrder");

	RegistrationParametrs.Insert("Presentation", DestinationArray);
	RegistrationParametrs.Insert("Description", "Proforma 1C (la doc. Comanda Client)");
	RegistrationParametrs.Insert("Version", "1.2.2.3"); 	// "1.0"
	RegistrationParametrs.Insert("SafeMode", False); 	// Variants: True, False / Варианты: Истина, Ложь 
	RegistrationParametrs.Insert("Information", "Proforma 1C (la Comanda Client)");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
				"Proforma 1C (la Comanda Client)",	// what we will see under button PRINT
				"FacturaProformaLaComanda",		// Name of Template 
				//"CallOfServerMethod",  			// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				"ServerMethodCall",  			// "ServerMethodCall" = for MXL / "ClientMethodCall" = for WORD !!! Использование.  Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				False,						// Показывать оповещение. Варианты Истина, Ложь / Variants: True, False
				"MXLPrint");           		// "MXLPrint" = for MXL / "" = for WORD !!! Модификатор 
	
	RegistrationParametrs.Insert("Commands", CommandTable);
	
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand	= CommandTable.Add();
	NewCommand.Presentation 	= Presentation;
	NewCommand.ID				= ID;
	NewCommand.Use				= Use;
	NewCommand.ShowNotification	= ShowNotification;
	NewCommand.Modifier			= Modifier;
	
EndProcedure

/////////////////////////////////// 
// Preparing of Print Form 
Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "FacturaProformaLaComanda";///PrintFormsCollection[0].DesignName;
	Except
		Message(NStr("en='TemplateName is empty!';
					  |ro='TemplateName este gol!';
					  |ru='TemplateName не заполнено!'"));
		Return;
	EndTry;
	
	PrintManagement.OutputSpreadsheetDocumentToCollection(
				PrintFormsCollection,
				TemplateName,  												// Template Name
				TemplateName,   											// Template Synonim
				CreatePrintForm(ObjectArray, PrintObjects, TemplateName)  	// Function for Execution (in this Module) - исполняющая функция (в этом же модуле)
				);
	
EndProcedure

Function CreatePrintForm(ObjectArray, PrintObjects, TemplateName)

	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_CmndClnt";  // PrintParameters_ + Name_of_Document
	
	Template	= ThisObject.GetTemplate(TemplateName);
	Query		= New Query;
	
	Query.Text	=
	      "SELECT
	      |	SalesOrder.Company AS Company,
	      |	SalesOrder.Counterparty AS Counterparty,
	      |	SalesOrder.Number AS Number,
	      |	SalesOrder.Date AS Date,
	      |	SalesOrder.Author AS Author,
	      |	SalesOrder.Contract AS Contract,
		  //|	SalesOrder.Contract.CustomerPaymentDueDate AS PaymentDueDate,
	      |	SalesOrder.Counterparty.Code AS CodeC,
	      |	SalesOrder.Responsible AS Responsible,
	      |	SalesOrder.Counterparty.ContactPerson AS ContactPerson,
	      |	SalesOrder.ExchangeRate AS Rate,
	      |	SalesOrder.DocumentCurrency AS DocumentCurrency,
	      |	SalesOrder.Inventory.(
	      |		LineNumber AS LineNumber,
	      |		Products AS Products,
	      |		Characteristic AS Characteristic,
	      |		Quantity AS Quantity,
	      |		MeasurementUnit AS MeasurementUnit,
	      |		Price AS Price,
	      |		DiscountMarkupPercent AS DiscountMarkupPercent,
	      |		Amount AS Amount,
	      |		VATRate AS VATRate,
	      |		VATAmount AS VATAmount,
	      |		Total AS Total
	      |	) AS Inventory,
	      |	SalesOrder.Company.TIN AS CompanyTIN,
	      |	SalesOrder.Company.BankAccountByDefault.Bank AS Bank,
	      |	SalesOrder.Company.BankAccountByDefault.AccountNo AS BankAccount,
	      |	SalesOrder.Counterparty.TIN AS CounterpartyTIN,
	      |	SalesOrder.Counterparty.BankAccountByDefault.Bank AS BankC,
	      |	SalesOrder.Counterparty.BankAccountByDefault.AccountNo AS BankAccountC,
	      |	SalesOrder.AmountIncludesVAT AS IncludeVAT,
	      |	SalesOrder.DocumentAmount AS DAmount
	      |FROM
	      |	Document.SalesOrder AS SalesOrder
	      |WHERE
	      |	SalesOrder.Ref IN (&Ref)";
		  
	Query.Parameters.Insert("Ref", ObjectArray);
	Selection			= Query.Execute().Choose();
	
	AreaCaption			= Template.GetArea("Caption");
	Header				= Template.GetArea("Header");
	AreaInventoryHeader = Template.GetArea("InventoryHeader");
	AreaInventory		= Template.GetArea("Line");
	Footer				= Template.GetArea("Footer");
	Spreadsheet.Clear();
	
	InsertPageBreak = False;
	
	While Selection.Next() Do
		If InsertPageBreak Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;
	
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////
		
		InfoAboutCompany      =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Company, Selection.Date, ,);
		InfoAboutCounterparty =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, Selection.Date, ,);
		
		Header.Parameters["Company"] 		= InfoAboutCompany.FullDescr;
		Header.Parameters["Counterparty"] 	= InfoAboutCounterparty.FullDescr;
	
		Header.Parameters["KPP"] 		= InfoAboutCompany.TIN;
		Header.Parameters["KPPC"] 		= InfoAboutCounterparty.TIN;
		
   		Header.Parameters["RegNumber"]  = InfoAboutCompany.RegistrationNumber;
		Header.Parameters["RegNumberC"] = InfoAboutCounterparty.RegistrationNumber;

		Header.Parameters["Address"]	= InfoAboutCompany.LegalAddress;
		Header.Parameters["AddressC"] 	= InfoAboutCounterparty.LegalAddress;

		Header.Parameters["NrComanda"] = "Nr. " + Selection.Number + " " +
										 Format(Selection.Date, "DF=""dd.MM.yyyy""");
		Rate = Selection.Rate;
		
		SelectionInventory	= Selection.Inventory.Choose();
		Spreadsheet.Put(AreaCaption);
		
		Header.Parameters.Fill(Selection);
			
		Try
			Header.Parameters["Currency"]		= Selection.DocumentCurrency;
			Header.Parameters["CurrencyRate"]	= Rate;
		Except	EndTry;
				
		Header.Parameters["Number"]	= Selection.Number;
		Header.Parameters["Date"]	= Format(Selection.Date,"DF=""dd.MM.yyyy""");
		
		////
		// 2019-02-07   AlekS
		// in Release 1.1.4.4 s-a scos CustomerPaymentDueDate  de la Catalogs.CounterpartyContracts
		//   unde a mutat ???
		////
		//If ValueIsFilled(Selection.PaymentDueDate) Then
		//	Header.Parameters["DataLimita"] = Format(Selection.Date + (Selection.PaymentDueDate * 86400), "DF=""dd.MM.yyyy""");
		//EndIf;
		
		///////////////////////////////////////////////////////////////////////////////////////////
		// Data scadenta - este limita de data in care Clientul trebuie sa achite Proforma  =  sapte zile calend.
		Header.Parameters["DataLimita"] = Format(Selection.Date + (7 * 86400), "DF=""dd.MM.yyyy""");
		
		//AdditionalAttributes = PropertiesManagement.GetValuesOfProperties(Selection.Company);
		AdditionalAttributes = PropertyManager.GetValuesProperties(Selection.Company);
		For Each Attr in AdditionalAttributes Do
			If StrFind(TrimAll(Attr.Property), "Capital Social") > 0 Then
				Header.Parameters["Capital"] = Attr.Value;
			EndIf;
		EndDo;

		While SelectionInventory.Next() Do
			Try
				Header.Parameters["VATRate"] = SelectionInventory.VATRate;
			Except
			EndTry;
		EndDo;
		Spreadsheet.Put(Header, Selection.Level());
////////////////////////////////////////////////////////HEADER///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////END////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////START////////////////////////////////////////////////////////////////
		AreDiscounts = Selection.Inventory.Unload().Total("DiscountMarkupPercent") <> 0;

		If AreDiscounts Then
			AreaInventoryHeader = Template.GetArea("InventoryHeaderWithDiscount");
			AreaInventory		= Template.GetArea("LineWithDiscount");
		EndIf;
		
		Spreadsheet.Put(AreaInventoryHeader);
		
		SelectionInventory = Selection.Inventory.Choose();

		TotalVAT	 = 0;
		TotalFaraTVA = 0;
		RowsNubmerInDoc = 35;
		
		While SelectionInventory.Next() Do
				
			TotalFaraTVA  =  TotalFaraTVA	+ SelectionInventory.Amount;
			TotalVAT      =  TotalVAT 	    + SelectionInventory.VATAmount;
			
			AreaInventory.Parameters["LineNumber"]		= SelectionInventory.LineNumber;
			AreaInventory.Parameters["Item"]			= "" + SelectionInventory.Products +
											?(StrLen(TrimAll(SelectionInventory.Characteristic)) > 0,
													" (" + SelectionInventory.Characteristic + ")", "");
			AreaInventory.Parameters["UnitOfMeasure"]	= SelectionInventory.MeasurementUnit;
			AreaInventory.Parameters["Quantity"]		= SelectionInventory.Quantity;
				
			Price	  = Format(Round(SelectionInventory.Price * Rate, 2),				"NFD=2");
			Amount	  = Format(Round(Price	* SelectionInventory.Quantity, 2),			"NFD=2");
			VATAmount = Format(Round(Amount	* SelectionInventory.VATRate.Rate /100, 2),	"NFD=2");

			RataReducere = SelectionInventory.DiscountMarkupPercent;
			Price1       = Price - (Price / 100 * RataReducere);
			Reducere     = Price / 100 * RataReducere;
			VATAmount    = Format(Round(Amount	* SelectionInventory.VATRate.Rate /100, 2), "NFD=2");
		    Price2       = Price1 * SelectionInventory.VATRate.Rate / (100 + SelectionInventory.VATRate.Rate);
			Amount1      = Amount - (Reducere * SelectionInventory.Quantity);

			If Selection.IncludeVAT Then
				AreaInventory.Parameters["Price"] = Format(Round((Price1 - Price2), 2), "NFD=2");
				AreaInventory.Parameters["Amount"]= Format(Round((Amount1 - SelectionInventory.VATAmount), 2), "NFD=2");
			Else
				AreaInventory.Parameters["Price"] = Format(Round(Price - (Price / 100 * RataReducere), 2), "NFD=2");
				AreaInventory.Parameters["Amount"]= Format(Round((Amount - (Reducere * SelectionInventory.Quantity)), 2), "NFD=2");
			EndIf;
			
			AreaInventory.Parameters["VATAmount"] 	= Format(Round(SelectionInventory.VATAmount * Rate, 2), "NFD=2");
				
			i = SelectionInventory.LineNumber;
					
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
		
		For i = i + 1 To RowsNubmerInDoc Do
			AreaInventory.Parameters["LineNumber"]		= i;
			AreaInventory.Parameters["Item"]			= Undefined;
			AreaInventory.Parameters["UnitOfMeasure"]	= Undefined;
			AreaInventory.Parameters["Quantity"]		= Undefined;
			AreaInventory.Parameters["Price"]			= Undefined;
			AreaInventory.Parameters["Amount"]			= Undefined;
			
			Try
				AreaInventory.Parameters["VATAmount"]	= Undefined;
			Except	EndTry;
					
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
/////////////////////////////////////////////////////INVENTORY///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////START//////////////////////////////////////////////////////////////////
		Query = New Query;
		Query.Text =
		"SELECT
		|	UserEmployees.Employee,
		|	UserEmployees.User
		|FROM
		|	InformationRegister.UserEmployees AS UserEmployees
		|WHERE
		|	UserEmployees.User = &Author";
		
		Query.SetParameter("Author", Selection.Author);
			
		Result		= Query.Execute();
		SelectionD	= Result.Choose();
			
		While SelectionD.Next() Do
			Try
				Footer.Parameters["CNPU"]	= SelectionD.Employee.Ind.PersonalCode;
				Footer.Parameters["CIU"]	= SelectionD.Employee.Ind.IDCard;
			Except	EndTry;
		EndDo;

		Footer.Parameters.Fill(Selection);
		Footer.Parameters["Date"]	= Format(Selection.Date, "DF=""dd.MM.yyyy""");
		Footer.Parameters["User"]	= Selection.Author;

///////////////////////////////////////////////////Totals///////////////////////////////////////////////
///////////////////////////////////////////////////Start/////////////////////////////////////////////
		Footer.Parameters["TotalCuTVA"]	  = Format((Selection.DAmount * Rate),    "NFD=2");
		Footer.Parameters["TotalTVA"]     = Format(Round(TotalVAT     * Rate, 2), "NFD=2");
		
		If Selection.IncludeVAT = False Then
			Footer.Parameters["TotalFaraTVA"] = Format(Round(TotalFaraTVA * Rate, 2), "NFD=2");
		Else
			Footer.Parameters["TotalFaraTVA"] = Format(Round((Selection.DAmount - TotalVAT) * Rate, 2), "NFD=2");
		EndIf;
///////////////////////////////////////////////////Totals///////////////////////////////////////////////
////////////////////////////////////////////////////End////////////////////////////////////////////	
	  
		Spreadsheet.Put(Footer);
	
//////////////////////////////////////////////////////FOOTER/////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////END///////////////////////////////////////////////////////////////////
	
		InsertPageBreak = True;
	
	EndDo;
	
	Return Spreadsheet;

EndFunction
