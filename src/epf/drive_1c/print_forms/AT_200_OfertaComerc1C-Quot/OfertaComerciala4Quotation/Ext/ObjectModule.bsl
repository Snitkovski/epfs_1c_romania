﻿ 
// Preparation external print form
Function ExternalDataProcessorInfo() Export
	
	RegistrationParametrs = New Structure;
	// Варианты берутся из перечисления AdditionalReportAndDataProcessorKinds: 
	// Variants are from list AdditionalReportAndDataProcessorKinds:
	//		- "ДополнительнаяОбработка"		= ""
	//		- "ДополнительныйОтчет"			= ""
	//		- "ЗаполнениеОбъекта"			= ""
	//		- "Отчет"						= ""
	//		- "ПечатнаяФорма"				= "PrintForm"
	//		- "СозданиеСвязанныхОбъектов"	= ""
	//RegistrationParametrs.Insert("Type", "PrintForm"); 
	RegistrationParametrs.Insert("Kind", "PrintForm");
	
	DestinationArray = New Array();
	DestinationArray.Add("Document.Quote");

	RegistrationParametrs.Insert("Presentation", DestinationArray);
	RegistrationParametrs.Insert("Description", "Oferta Comerciala din Cotatie");
	RegistrationParametrs.Insert("Version", "1.2.4.3");	// "1.0"
	RegistrationParametrs.Insert("SafeMode", False); 	// Variants: True, False / Варианты: Истина, Ложь 
	RegistrationParametrs.Insert("Information", "Oferta Comerciala la Cotatie");
	
	CommandTable = GetCommandTable();
	
	AddCommand(CommandTable,
				"Ofertă Comercială 1C (Cotatie)", 	// what we will see under button PRINT
				"OfertaComerciala",   			  	// Name of Template 
				"ServerMethodCall",  				// "CallOfServerMethod" = for MXL / "CallOfClientMethod" = for WORD !!! Использование.  
													// Варианты: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"   
				False,								// Показывать оповещение. Варианты Истина, Ложь / Variants: True, False
				"MXLPrint");           				// "MXLPrint" = for MXL / "" = for WORD !!! Модификатор 
				
	RegistrationParametrs.Insert("Commands", CommandTable);
	Return RegistrationParametrs;
	
EndFunction

Function GetCommandTable()
	
	Commands = New ValueTable;
	Commands.Columns.Add("Presentation",	New TypeDescription("String"));
	Commands.Columns.Add("ID",				New TypeDescription("String"));
	Commands.Columns.Add("Use",				New TypeDescription("String"));
	Commands.Columns.Add("ShowNotification",New TypeDescription("Boolean"));
	Commands.Columns.Add("Modifier",		New TypeDescription("String"));
	
	Return Commands;
	
EndFunction

Procedure AddCommand(CommandTable, Presentation, ID, Use, ShowNotification = False, Modifier = "")
	
	NewCommand	= CommandTable.Add();
	NewCommand.Presentation 	= Presentation;
	NewCommand.ID				= ID;
	NewCommand.Use				= Use;
	NewCommand.ShowNotification	= ShowNotification;
	NewCommand.Modifier			= Modifier;
	
EndProcedure

/////////////////////////////////// 
// Preparing of Print Form 
Procedure Print(ObjectArray, PrintFormsCollection, PrintObjects, OutputParametrs)  Export
	
	Try
		TemplateName = "OfertaComerciala";	//PrintFormsCollection[0].DesignName;
	Except
		Message(NStr("en='TemplateName is empty!';
					  |ro='TemplateName este gol!';
					  |ru='TemplateName не заполнено!'"));
		Return;
	EndTry;
	
	PrintManagement.OutputSpreadsheetDocumentToCollection(
						PrintFormsCollection,
						TemplateName,  												// Template Name
						TemplateName,   											// Template Synonim
						CreatePrintForm(ObjectArray, PrintObjects, TemplateName)  	// Function for Execution (in this Module) - исполняющая функция (в этом же модуле)
						);
	
EndProcedure

Function CreatePrintForm(ObjectArray, PrintObjects, TemplateName)

	Spreadsheet = New SpreadsheetDocument;
	Spreadsheet.PrintParametersKey = "PrintParameters_ComercOffQuot";  // PrintParameters_ + Name_of_Document
	
	Template	= ThisObject.GetTemplate(TemplateName);
	Query		= New Query;
	
	Query.Text	=
	 "SELECT
	 |	Quote.Ref AS Ref,
	 |	Quote.Number AS Number,
	 |	Quote.Date AS Date,
	 |	Quote.Company AS Company,
	 |	Quote.BankAccount AS BankAccount,
	 |	Quote.DocumentCurrency AS DocumentCurrency,
	 |	Quote.Counterparty AS Counterparty,
	 |	Quote.Counterparty.ContactPerson.Presentation AS ContactPerson,
	 |	Quote.Contract AS Contract,
	 |	Quote.ExchangeRate AS ExRate,
	 |	Quote.Responsible AS Responsible,
	 |	Quote.AmountIncludesVAT AS AmountIncludesVAT,
	 |	Quote.DocumentAmount AS DocumentAmount,
	 |	Quote.DocumentSubtotal AS DocumentSubtotal,
	 |	Quote.DocumentTax AS DocumentTax,
	 |	Quote.Author AS Author,
	 |	Quote.PreferredVariant AS PreferredVariant,
	 |	Quote.Inventory.(
	 |		LineNumber AS LineNumber,
	 |		Products AS Products,
	 |		Characteristic AS Characteristic,
	 |		Quantity AS Quantity,
	 |		MeasurementUnit AS MeasurementUnit,
	 |		Price AS Price,
	 |		Amount AS Amount,
	 |		VATRate AS VATRate,
	 |		VATRate.Rate AS VATRateRate,
	 |		VATAmount AS VATAmount,
	 |		Total AS Total,
	 |		Variant AS Variant
	 |	) AS Inventory
	 |FROM
	 |	Document.Quote AS Quote
	 |WHERE
	 |	Quote.Ref IN(&Ref)";
	
	Query.Parameters.Insert("Ref", ObjectArray);
	Selection	= Query.Execute().Choose();
	
	//	 |		Variant AS Variant    ==  // N for "Offer Variant"
	
	AreaCaption			= Template.GetArea("Caption");
	AreaHeader			= Template.GetArea("Header");
	AreaInventoryHeader = Template.GetArea("InventoryHeader");
	AreaInventory		= Template.GetArea("Line");
	AreaFooter			= Template.GetArea("Footer");
	Spreadsheet.Clear();
	
	InsertPageBreak = False;
	While Selection.Next() Do
		If InsertPageBreak Then
			Spreadsheet.PutHorizontalPageBreak();
		EndIf;
		
		DocCurr = Selection.DocumentCurrency;
		DocDate = Selection.Date;
		DocExRate = Selection.ExRate;
		
		////// We can put there something as PageCaption
		Spreadsheet.Put(AreaCaption);
		
///////////////////////////////////HEADER START///////////////////////////////////////////////////////////////
		InfoAboutCompany      =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Company, DocDate, ,);
		InfoAboutCounterparty =  DriveServer.InfoAboutLegalEntityIndividual(Selection.Counterparty, DocDate, ,);
		
		AreaHeader.Parameters["Counterparty"] = InfoAboutCounterparty.FullDescr;
		AreaHeader.Parameters["ContactPerson"] = "D-lui " + Selection.ContactPerson;

		AreaHeader.Parameters.Fill(Selection);
		
		Spreadsheet.Put(AreaHeader, Selection.Level());
///////////////////////////////////HEADER END///////////////////////////////////////////////////////////////

///////////////////////////////////INVENTORY START///////////////////////////////////////////////////////////////
		AreaInventoryHeader.Parameters["ColNamePrice"] = "Preț (" + DocCurr + ")";
		AreaInventoryHeader.Parameters["ColNameValue"] = "Valoare (" + DocCurr + ")";
		Spreadsheet.Put(AreaInventoryHeader);
		
		SelectionInventory = Selection.Inventory.Choose();
		
		i = 1;
		While SelectionInventory.Next() Do
			If Not SelectionInventory.Variant = Selection.PreferredVariant Then
				Continue;
			EndIf;
			
			AreaInventory.Parameters["LineNumber"]	= i;
			
			AreaInventory.Parameters["Products"]	= "" + SelectionInventory.Products +
														?(StrLen(SelectionInventory.Characteristic) <> 0,
															Chars.CR + "(" + SelectionInventory.Characteristic + ")", "");

			AreaInventory.Parameters["Quantity"]	= Format(SelectionInventory.Quantity, "L=ro_RO");
			
			LinePriceWOVAT = ?(Selection.AmountIncludesVAT, SelectionInventory.Price / (1 + SelectionInventory.VATRateRate / 100), SelectionInventory.Price);
			AreaInventory.Parameters["Price"]	    = Format(Round(LinePriceWOVAT, 2), "L=ro_RO") + "  ";
			AreaInventory.Parameters["Amount"]	    = Format(Round(LinePriceWOVAT * SelectionInventory.Quantity, 2), "L=ro_RO") + "  ";
			
			i =  i + 1;
					
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
		
		For i = i To 10 Do
			AreaInventory.Parameters["LineNumber"] 	= i;
			AreaInventory.Parameters["Products"]	= Undefined;
			AreaInventory.Parameters["Quantity"]	= Undefined;
			AreaInventory.Parameters["Price"]		= Undefined;
			AreaInventory.Parameters["Amount"]		= Undefined;
			
			Spreadsheet.Put(AreaInventory, SelectionInventory.Level());
		EndDo;
///////////////////////////////////INVENTORY END///////////////////////////////////////////////////////////////

///////////////////////////////////FOOTER START/////////////////////////////////////////////////////////////////
		AreaFooter.Parameters["Total"] = Format(Round(Selection.DocumentSubtotal, 2), "L=ro_RO") + "  ";
		AreaFooter.Parameters["ValutaPreturilor"] = "* Prețurile sunt in " + DocCurr + " si nu contin TVA.";
		Spreadsheet.Put(AreaFooter);
///////////////////////////////////FOOTER END/////////////////////////////////////////////////////////////////
	
		InsertPageBreak = True;
	EndDo;
	
	Return Spreadsheet;

EndFunction
