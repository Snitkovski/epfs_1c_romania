﻿#Region Public

Function ExternalDataProcessorInfo() Export
	
	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind			= "AdditionalDataProcessor";
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= "Import exchange rates from ECB SDMX 2.1";
	RegistrationParameters.Information	= NStr("en = 'Data processor / currency exchange rates import from webservice sdw-wsrest.ecb.europa.eu'; ru = 'Внешняя обработка / импорт курса валют из вебсервиса sdw-wsrest.ecb.europa.eu'");
	RegistrationParameters.SafeMode		= False;
	
    Return RegistrationParameters;
	
EndFunction

Function ImportCurrencyRatesByParameters(Val Currencies, Val Company, Val StartPeriod, Val EndPeriod, ErrorsOccuredOnImport = False) Export
	
	ImportStatuses = New Array;
	ErrorsOccuredOnImport = False;
	
	PresentationCurrency = Common.ObjectAttributeValue(Company, "PresentationCurrency");
	CheckPresentationCurrencyIsEUR(PresentationCurrency, ImportStatuses);
	
	If ImportStatuses.Count() >0 Then
		Return ImportStatuses;
	EndIf;
	
	For Each Currency IN Currencies Do
		
		If Currency.Currency = PresentationCurrency Then
			Continue;
		EndIf;
		
		ImportStatus = New Structure("Currency,OperationStatus,Message");
		ImportStatus.Currency = Currency;
		
		URL = "https://sdw-wsrest.ecb.europa.eu/service/data/ECB,EXR,latest/D." 
			+ Right(Currency.Currency.Description, 3) + ".EUR.SP00.A"
			+ "?startPeriod=" + FormatISO8601(StartPeriod)
			+ "&endPeriod=" + FormatISO8601(EndPeriod);
		
		XMLFile = GetFilesFromInternet.DownloadFileToTempStorage(URL);
		
		If Not XMLFile.Status Then
			ImportStatus.Message = StringFunctionsClientServer.SubstituteParametersToString(
				NStr("en='Unable to receive data file with exchange rates (%1 - %2):
					|There may not be access to website with exchange rates or non-existent currency is specified. (see Event log)';
					|ru='Невозможно получить файл данных с курсами валюты (%1 - %2):
					|Возможно, нет доступа к веб сайту с курсами валют, либо указана несуществующая валюта. (см. Журнал регистрации)'"),
				Currency.CurrencyCode,
				Currency.Currency);
			ImportStatus.OperationStatus = False;
			ImportStatuses.Add(ImportStatus);
			Continue;
		EndIf;
		
		Rates = ParseRates(XMLFile.Path, Currency, ImportStatus.Message);
		DeleteFromTempStorage(XMLFile.Path);
		
		If Rates.Count() = 0 Then
			ImportStatus.OperationStatus = False;
			ImportStatus.Message = StringFunctionsClientServer.SubstituteParametersToString(
				NStr("en='Exchange rates %1 - %2 are not imported. No data available.';
					|ru='Курсы валюты %1 - %2 не загружены. Нет данных.'"),
				Currency.CurrencyCode,
				Currency.Currency);
			ImportStatuses.Add(ImportStatus);
			Continue;
		EndIf;
		
		CreateRates(Currency.Currency, Company, Rates, StartPeriod, EndPeriod, ImportStatus.Message);
		ImportStatus.OperationStatus = IsBlankString(ImportStatus.Message);
		ImportStatuses.Add(ImportStatus);
		
	EndDo;
	
	Return ImportStatuses;
	
EndFunction

#EndRegion

#Region Private

Procedure CheckPresentationCurrencyIsEUR(PresentationCurrency, ImportStatuses)
	
	If PresentationCurrency.Description = "EUR" Then
		Return;
	EndIf;
	
	ImportStatus = New Structure("Currency,OperationStatus,Message");
	ImportStatus.Currency = PresentationCurrency;
	ImportStatus.Message =
		NStr("en='Unable to download rates, because Presentation currency is not set to EUR';
			|ru='Невозможно выполнить загрузку курсов валют, поскольку валюта регламентированного учета отличается от Евро'");
	ImportStatus.OperationStatus = False;
	ImportStatuses.Add(ImportStatus);
	
EndProcedure

Function ParseRates(Val TempStorage, Val Currency, Message)

	Rates = New Array;
	Try
		BinaryData = GetFromTempStorage(TempStorage);
		XML = GetStringFromBinaryData(BinaryData);
		
		If XML = "" Then
			Return Rates;
		EndIf;
		
		Document = BuildDOM(XML);
		
		ObsElements = GetElements(Document, ".//generic:Obs");
		
		While True Do
			ObsElement = ObsElements.IterateNext();
			If ObsElement = Undefined Then
				Break;
			EndIf;
			Date = GetFirstElement_TextContent(Document, "generic:ObsDimension/@value", ObsElement);
			Date = StrReplace(Date, "-", "");
			Date = Date(Date);
			Date = Date + 24 * 60 * 60;
			Rate = Number(GetFirstElement_TextContent(Document, "generic:ObsValue/@value", ObsElement));
			Rates.Add(New Structure("Date,Rate", Date, Rate));
		EndDo;
	
	Except
		ErrorInfo = ErrorInfo();
		ErrorDescription = BriefErrorDescription(ErrorInfo);
		Message = StringFunctionsClientServer.SubstituteParametersToString(
				NStr("en='Unable to parse data file with exchange rates (%1 - %2):
					|%3';
					|ru='Невозможно разобрать файл данных с курсами валюты (%1 - %2):
					|%3'"),
				Currency.CurrencyCode,
				Currency.Currency,
				ErrorDescription);
	EndTry;
	
	Return Rates;

EndFunction

Function BuildDOM(XML)
	XMLReader = New XMLReader;
	XMLReader.SetString(XML);
	DOMBuilder = New DOMBuilder;
	Return DOMBuilder.Read(XMLReader);
EndFunction

Function GetElements(DOMDocument, XPath, ContextNode = Undefined)
	DOMNamespaceResolver = New DOMNamespaceResolver(DOMDocument);
    XPathExpression = DOMDocument.CreateXPathExpression(XPath, DOMNamespaceResolver);
	If ContextNode = Undefined Then
		Result = XPathExpression.Evaluate(DOMDocument);
	Else
		Result = XPathExpression.Evaluate(ContextNode);
	EndIf;
	Return Result;
EndFunction

Function GetFirstElement_TextContent(DOMDocument, XPath, ContextNode = Undefined)
	Element = GetFirstElement(DOMDocument, XPath, ContextNode);
	If Element <> Undefined Then
		Return Element.TextContent;
	EndIf;
EndFunction

Function GetFirstElement(DOMDocument, XPath, ContextNode = Undefined)
	Return GetElements(DOMDocument, XPath, ContextNode).IterateNext();
EndFunction

Function CreateRates(Val Currency, Val Company, Val Rates, Val StartPeriod, Val EndPeriod, Message)
	
	For Each Rate In  Rates Do
		RecordManager = InformationRegisters.ExchangeRate.CreateRecordManager();
		RecordManager.Currency		= Currency;
		RecordManager.Company		= Company;
		RecordManager.Period		= Rate.Date;
		RecordManager.Rate			= 1 / Rate.Rate;
		RecordManager.Repetition	= 1;
		RecordManager.Write();
	EndDo;
	
EndFunction

Function FormatISO8601(Val Date)
	Return Format(Date, "DF=yyyy-MM-dd");
EndFunction

#EndRegion
