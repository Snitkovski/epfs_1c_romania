﻿#Region ProgramInterface

Function ExternalDataProcessorInfo() Export

	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind = AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindAdditionalDataProcessor();
	RegistrationParameters.Version		= "1.0";
	RegistrationParameters.Description	= "Import exchange rates";
	RegistrationParameters.Information	= NStr("en = 'Data processor / template for import currency exchange rates'; ru = 'Внешняя обработка / шаблон для импорта курса валют'");
	RegistrationParameters.SafeMode		= False;
		
    Return RegistrationParameters;
	
EndFunction

#EndRegion

#Region ServiceProgramInterface

// Procedure for exchange rates import by a certain period.
//
// Parameters:
// Currencies		- Any collection - with the following fields:
// 				CurrencyCode - currency numeric code.
// 				Currency - ref on currency.
// ImportBeginOfPeriod	- Date - start of the currency rates import period.
// ImportEndOfPeriod	- Date - end of currency rates import period.
//
// Returns:
// Import state array  - each item - structure with fields.
// 	Currency - imported currency.
// 	OperationStatus - if the import is complete successfully.
// 	Message - import explanation (error message text or an explanatory message).
//
Function CurrencyRatesImportByParameters(Val Currencies, Val Companies, Val ImportBeginOfPeriod, Val ImportEndOfPeriod,
	ErrorsOccuredOnImport = False) Export
	
	ImportStatus = New Array;
		
	Return ImportStatus;
	
EndFunction

#EndRegion
