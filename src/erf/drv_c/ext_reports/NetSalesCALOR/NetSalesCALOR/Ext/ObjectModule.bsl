#Region ProgramInterface

Function ExternalDataProcessorInfo() Export

	RegistrationParameters = AdditionalReportsAndDataProcessors.ExternalDataProcessorInfo("3.0.1.331");
	
	RegistrationParameters.Kind = AdditionalReportsAndDataProcessorsClientServer.DataProcessorKindAdditionalReport();
	RegistrationParameters.Version		= "1.4.0.0";
	RegistrationParameters.Description	= "Net sales (CALOR)";
	RegistrationParameters.Information	= NStr("en = 'Net sales (CALOR)'; ro = 'Net sales (CALOR)'; ru = 'Продажи (CALOR)'");
	RegistrationParameters.SafeMode		= False;
    
    Comand = RegistrationParameters.Commands.Add();
    Comand.ID = "NetSalesCALOR";
    Comand.Presentation = NStr("en = 'Net sales (CALOR)'; ro = 'Net sales (CALOR)'; ru = 'Продажи (CALOR)'");
    Comand.Use = AdditionalReportsAndDataProcessorsClientServer.CommandTypeOpenForm();
    Comand.ShowNotification = True;
	
    Return RegistrationParameters;
	
EndFunction

#EndRegion

Procedure OnComposeResult(ResultDocument, DetailsData, StandardProcessing)

	StandardProcessing = False;
	UserSettingsModified = False;
	
	DriveReports.ChangeGroupsValues(SettingsComposer, UserSettingsModified);
	
	ReportSettings = SettingsComposer.GetSettings();
    
    ExternalDataSets = GetExternalDataSets(ReportSettings);
	
	ReportParameters = PrepareReportParameters(ReportSettings);
	
	DriveReports.SetReportAppearanceTemplate(ReportSettings);
	DriveReports.OutputReportTitle(ReportParameters, ResultDocument);
	
	TemplateComposer = New DataCompositionTemplateComposer;
	CompositionTemplate = TemplateComposer.Execute(DataCompositionSchema, ReportSettings, DetailsData);

	// Create and initialize a composition processor
	CompositionProcessor = New DataCompositionProcessor;
	CompositionProcessor.Initialize(CompositionTemplate,ExternalDataSets, DetailsData, True);

	// Create and initialize the result output processor
	OutputProcessor = New DataCompositionResultSpreadsheetDocumentOutputProcessor;
	OutputProcessor.SetDocument(ResultDocument);

	// Indicate the output begin
	OutputProcessor.BeginOutput();
	TableFixed = False;

	ResultDocument.FixedTop = 0;
	
	// Main cycle of the report output
	AreasForDeletion = New Array;
	ChartsQuantity = 0;
	While True Do
		// Get the next item of a composition result
		ResultItem = CompositionProcessor.Next();

		If ResultItem = Undefined Then
			// The next item is not received - end the output cycle
			Break;
		Else
			// Fix header
			If  Not TableFixed
				  AND ResultItem.ParameterValues.Count() > 0
				  AND TypeOf(SettingsComposer.Settings.Structure[0]) <> Type("DataCompositionChart") Then

				TableFixed = True;
				ResultDocument.FixedTop = ResultDocument.TableHeight;

			EndIf;
			// Item is received - display it using the output processor
			OutputProcessor.OutputItem(ResultItem);
			
			If ResultDocument.Drawings.Count() > ChartsQuantity Then
				
				ChartsQuantity = ResultDocument.Drawings.Count();
				CurrentPicture = ResultDocument.Drawings[ChartsQuantity-1];
				If TypeOf(CurrentPicture.Object) = Type("Chart") Then
					
					DriveReports.SetReportChartSize(CurrentPicture);
					
					CurrentLineNumber = ResultDocument.TableHeight;
					Area = ResultDocument.Area(CurrentLineNumber - 6,,CurrentLineNumber);
					AreasForDeletion.Add(Area);
				EndIf;
			EndIf;
			
		EndIf;
	EndDo;

	OutputProcessor.EndOutput();
	
	For Each Area In AreasForDeletion Do
		ResultDocument.DeleteArea(Area, SpreadsheetDocumentShiftType.Vertical);
	EndDo;
	
	DriveReports.ProcessReportCharts(ReportParameters, ResultDocument);
	
EndProcedure

Function GetExternalDataSets(Val ReportSettings)
    
    ExternalDataSets = New Structure;
    
    ResultTab = New ValueTable;
    
    localSettingsComposer = New DataCompositionSettingsComposer;
    localSettingsComposer.Initialize(New DataCompositionAvailableSettingsSource(DataCompositionSchema));
    localSettingsComposer.LoadSettings(ReportSettings);
    
    RecorderFied = New DataCompositionField("Recorder");
    RecorderFFied = New DataCompositionField("FRecorder");
    
    For Each Item In localSettingsComposer.Settings.Selection.Items Do
        Item.Use = (Item.Field = RecorderFied OR Item.Field = RecorderFFied);
    EndDo;
    
    TemplateComposer = New DataCompositionTemplateComposer;
	CompositionTemplate = TemplateComposer.Execute(DataCompositionSchema, localSettingsComposer.Settings,,,Type("DataCompositionValueCollectionTemplateGenerator"));
    
	CompositionProcessor = New DataCompositionProcessor;
	CompositionProcessor.Initialize(CompositionTemplate);
    
	OutputProcessor = New DataCompositionResultValueCollectionOutputProcessor;
	OutputProcessor.SetObject(ResultTab);
    
    OutputProcessor.Output(CompositionProcessor);
    
    If ResultTab.Count() = 0 Then
    	Return GetEmptyExternalDataSets()
    EndIf;
    
    Query = New Query;
    Query.TempTablesManager = New TempTablesManager;
    Query.SetParameter("pTAB",ResultTab);
    Query.Text = "SELECT
                 |  pTAB.FRecorder AS FRecorder,
                 |  pTAB.Recorder AS Recorder
                 |INTO Docs
                 |FROM
                 |  &pTAB AS pTAB
                 |;
                 |
                 |////////////////////////////////////////////////////////////////////////////////
                 |SELECT
                 |  Docs.FRecorder AS FRecorder,
                 |  TaxInvoiceIssued.Date AS Date,
                 |  TaxInvoiceIssued.Number AS Number,
                 |  TaxInvoiceIssued.OperationKind AS OperationKind
                 |FROM
                 |  Docs AS Docs
                 |      INNER JOIN Document.TaxInvoiceIssued AS TaxInvoiceIssued
                 |      ON Docs.FRecorder = TaxInvoiceIssued.Ref
                 |
                 |GROUP BY
                 |  Docs.FRecorder,
                 |  TaxInvoiceIssued.Date,
                 |  TaxInvoiceIssued.Number,
                 |  TaxInvoiceIssued.OperationKind";
                 
    ExternalDataSets.Insert("FRecordersDocs",Query.Execute().Unload());
    
    Query.Text = "SELECT
                 |  VALUETYPE(Docs.Recorder) AS DocType
                 |FROM
                 |  Docs AS Docs
                 |
                 |GROUP BY
                 |  VALUETYPE(Docs.Recorder)";
    
    DocTypes = Query.Execute().Select();
    
    QuerySchema = New QuerySchema;
    
    QB = QuerySchema.QueryBatch[0];
    
    While DocTypes.Next() Do;
        
        MD = Metadata.FindByType(DocTypes.DocType);
        
        Operator = QB.Operators.Add();
        
        OpTMP = Operator.Sources.Add(Type("QuerySchemaTempTableDescription"),"Docs","Docs");
        OpTMP.Source.AvailableFields.Add("Recorder");
        
        OpTab = Operator.Sources.Add(MD.FullName(),"DOC");
        
        Operator.SelectedFields.Add("Docs.Recorder",0);
        Operator.SelectedFields.Add("DOC.Number",1);
        Operator.SelectedFields.Add("DOC.Date",2);
        Operator.SelectedFields.Add("CAST(DOC.Comment AS STRING(64))",3);
        Operator.SelectedFields.Add("DOC.Author",4);
        
        OpTMP.Joins.ADD(OpTab,"Docs.Recorder = DOC.Ref");
        OpTMP.Joins[0].JoinType = QuerySchemaJoinType.Inner;
        
        Operator.UnionType = QuerySchemaUnionType.UnionAll;
        
        Operator.Group.Add("Docs.Recorder");
        
    EndDo;
    
    QB.Columns[3].Alias = "Comment";
    
    QB.Operators.Delete(0);
    
    Query.Text = QuerySchema.GetQueryText();
    
    ExternalDataSets.Insert("RecordersDocs",Query.Execute().Unload());
    
    Query.TempTablesManager.Close();
    
    Return ExternalDataSets;
    
EndFunction

Function GetEmptyExternalDataSets()
    
    ExternalDataSets = New Structure;
    
    FRecordersDocs = New ValueTable;
    
    FRecordersDocs.Columns.Add("FRecorder");
    FRecordersDocs.Columns.Add("Date");
    FRecordersDocs.Columns.Add("Number");
    FRecordersDocs.Columns.Add("OperationKind");
    
    ExternalDataSets.Insert("FRecordersDocs",FRecordersDocs);
    
    RecordersDocs = New ValueTable;
    
    RecordersDocs.Columns.Add("Recorder");
    RecordersDocs.Columns.Add("Date");
    RecordersDocs.Columns.Add("Number");
    RecordersDocs.Columns.Add("Author");
    RecordersDocs.Columns.Add("Comment");
    
    ExternalDataSets.Insert("RecordersDocs",RecordersDocs);
    
    Return ExternalDataSets;
    
EndFunction

Function PrepareReportParameters(ReportSettings)
	
	BeginOfPeriod = Date(1,1,1);
	EndOfPeriod  = Date(1,1,1);
	TitleOutput = False;
	Title = "Sales";
	
	ParameterPeriod = ReportSettings.DataParameters.FindParameterValue(New DataCompositionParameter("PeriodReport"));
	If ParameterPeriod <> Undefined
		AND ParameterPeriod.Use
		AND ValueIsFilled(ParameterPeriod.Value) Then
		
		BeginOfPeriod = ParameterPeriod.Value.StartDate;
		EndOfPeriod  = ParameterPeriod.Value.EndDate;
	EndIf;
	
	ParameterOutputTitle = ReportSettings.DataParameters.FindParameterValue(New DataCompositionParameter("TitleOutput"));
	If ParameterOutputTitle <> Undefined
		AND ParameterOutputTitle.Use Then
		
		TitleOutput = ParameterOutputTitle.Value;
	EndIf;
	
	OutputParameter = ReportSettings.OutputParameters.FindParameterValue(New DataCompositionParameter("Title"));
	If OutputParameter <> Undefined
		AND OutputParameter.Use Then
		Title = OutputParameter.Value;
	EndIf;
	
	ReportParameters = New Structure;
	ReportParameters.Insert("BeginOfPeriod"  , BeginOfPeriod);
	ReportParameters.Insert("EndOfPeriod"    , EndOfPeriod);
	ReportParameters.Insert("TitleOutput"    , TitleOutput);
	ReportParameters.Insert("Title"          , Title);
	ReportParameters.Insert("ReportId"       , "Sales(CALOR)");
	ReportParameters.Insert("ReportSettings" , ReportSettings);
		
	Return ReportParameters;
	
EndFunction
