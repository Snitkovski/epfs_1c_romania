#!/bin/bash

#=========================Параметры установки=======================================================
 

DATABASES="bgu;zgu"   #Список создаваемых баз данных

HOSTNAME="1c-srv-test" # Адреса хоста

USERNAME='1x' #Логин на портал releases.1c.ru
PASSWORD='1x' #Пароль на портал releases.1c.ru

PG_PASSWORD='123321'  #Пароль пользователя 'postgres'

VER_1C='8.3.14.1630'  #Версия устанвливаемой платформы

CLIENT=0			  #Устанавливать толстый клиент
VM_TOOLS=1			  #Елси сервер крутиться под ESXi то ставим open-vm-tools






#=====================Вычисляемые / устанавливаем служебные переменные===========================================
RED='\033[0;31m'       #  ${RED}      # красный цвет знаков
GREEN='\033[0;32m'     #  ${GREEN}    # зелёный цвет знаков
BLUE='\033[0;34m'      #  ${BLUE}     # синий цвет знаков
YELLOW='\033[0;33m'    #  ${YELLOW}   # желтый цвет знаков
NORMAL='\033[0m'       #  ${NORMAL}   # все атрибуты по умолчанию

CUR_DIR=`dirname $0`

			# Адреса хоста
if [ $HOSTNAME != `hostname` ]; then
	echo -en "${YELLOW} Хост переименован в ${HOSTNAME} ${NORMAL}\n"
   hostnamectl set-hostname $HOSTNAME
fi


HOSTNAME=`hostname`   # имя этого хоста для файлов публицации см "hostnamectl set-hostname имя"
HOST_IP=`hostname -I | sed -r 's/ //'`  # адрес этого хоста для файлов публицации
#Настроим /etc/hosts
sed -i "/${HOST_IP}/d" /etc/hosts
echo $HOST_IP $HOSTNAME `echo $HOSTNAME | sed -r 's/\..+//'` >> /etc/hosts
#-------


if [ -f "$CUR_DIR/pg_tune.sql" ] 
then
	echo -en "${YELLOW} Файл настроек PostgresSQL найден ${NORMAL}\n"
else
	echo -en "${RED} Не найден файл настроек PostgresSQL. Создайте файл pg_tune.sql в каталоге скрипта с командами вкладки 'ALTER SYSTEM' ${YELLOW} https://pgtune.leopard.in.ua ${NORMAL}\n"
	echo -en "${GREEN} Подробнее об ALTER SYSTEM https://postgrespro.ru/docs/postgresql/10/sql-altersystem ${NORMAL}\n"
	exit 0
fi


OIFS=$IFS
IFS=';'



#===========================================================================================





#Отключим selinux
echo -en "${YELLOW} Отключим selinux..... ${NORMAL}\n"
sed -i -r 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

echo -en "${YELLOW} Установка пакетов..... ${NORMAL}\n"

yum install wget nano mc -y

yum install epel-* -y

#Елси сервер крутиться под ESXi 
if [ $VM_TOOLS -eq 1 ]; then
	yum install open-vm-tools -y
fi

yum install fontconfig-devel -y
yum install ImageMagick -y

#установим шрифты MS
wget http://li.nux.ro/download/nux/dextop/el7/x86_64//msttcore-fonts-installer-2.6-1.noarch.rpm
yum localinstall  msttcore-fonts-installer-2.6-1.noarch.rpm
rm -rf msttcore-fonts-installer-2.6-1.noarch.rpm

#===========================================================================================
echo -en "${YELLOW} Установка Apache..... ${NORMAL}\n"
yum install httpd openssl -y
firewall-cmd --permanent --zone=public --add-service=http


#===========================================================================================
echo -en "${YELLOW} Установка и настройка Samba..... ${NORMAL}\n"
yum install samba samba-client samba-common -y

mkdir /home/files
mkdir /home/files/install
chown -R apache:apache /home/files
chmod 777  /home/files
mv /etc/samba/smb.conf /etc/samba/smb.conf.bak

echo "[global]" > /etc/samba/smb.conf
#echo "workgroup = HOME" >> /etc/samba/smb.conf
echo "server string = 1C Application Server %v" >> /etc/samba/smb.conf
echo "netbios name = "$HOSTNAME >> /etc/samba/smb.conf
echo "security = user" >> /etc/samba/smb.conf
echo "map to guest = bad user" >> /etc/samba/smb.conf
echo "dns proxy = no" >> /etc/samba/smb.conf
echo "guest account = apache" >> /etc/samba/smb.conf
echo "#===Share Definitions" >> /etc/samba/smb.conf
echo "[files]" >> /etc/samba/smb.conf
echo "path = /home/files" >> /etc/samba/smb.conf
echo "browsable =yes" >> /etc/samba/smb.conf
echo "writable = yes" >> /etc/samba/smb.conf
echo "guest ok = yes" >> /etc/samba/smb.conf
echo "read only = no" >> /etc/samba/smb.conf

systemctl enable smb.service && systemctl enable nmb.service
systemctl start smb.service && systemctl start nmb.service
firewall-cmd --permanent --zone=public --add-service=samba

#===============Устанавливаем Postgres Pro 1C===================================================================

rpm --import http://repo.postgrespro.ru/keys/GPG-KEY-POSTGRESPRO
echo [postgrespro-1c] > /etc/yum.repos.d/postgrespro-1c.repo
echo name=Postgres Pro 1C repo >> /etc/yum.repos.d/postgrespro-1c.repo
echo baseurl=http://repo.postgrespro.ru/1c-archive/pg1c-10.6/centos/7/os/x86_64/rpms >> /etc/yum.repos.d/postgrespro-1c.repo
echo gpgcheck=1 >> /etc/yum.repos.d/postgrespro-1c.repo
echo enabled=1 >> /etc/yum.repos.d/postgrespro-1c.repo
yum makecache
yum install -y postgrespro-1c-10-server-10.6-1.el7.x86_64 postgrespro-1c-10-contrib-10.6-1.el7.x86_64
/opt/pgpro/1c-10/bin/pg-setup initdb
/opt/pgpro/1c-10/bin/pg-setup service enable
systemctl start postgrespro-1c-10.service

sudo -u postgres /opt/pgpro/1c-10/bin/psql -c "ALTER USER postgres WITH PASSWORD '"$PG_PASSWORD"';" 
# Файл pg_tune.sql содержит команды с https://pgtune.leopard.in.ua/ из вкладки "ALTER SYSTEM"
cp $CUR_DIR/pg_tune.sql /tmp

sed -i -r '/^#/d' /tmp/pg_tune.sql

sudo -u postgres /opt/pgpro/1c-10/bin/psql -f  /tmp/pg_tune.sql
rm -rf /tmp/pg_tune.sql
systemctl restart postgrespro-1c-10.service


#=============================Загружаем платформу 1С ===================================================
SRC=$(curl -c /tmp/cookies.txt -s -L https://releases.1c.ru)

ACTION=$(echo "$SRC" | grep -oP '(?<=form method="post" id="loginForm" action=")[^"]+(?=")')
EXECUTION=$(echo "$SRC" | grep -oP '(?<=input type="hidden" name="execution" value=")[^"]+(?=")')



curl -s -L \
    -o /dev/null \
    -b /tmp/cookies.txt \
    -c /tmp/cookies.txt \
    --data-urlencode "inviteCode=" \
    --data-urlencode "execution=$EXECUTION" \
    --data-urlencode "_eventId=submit" \
    --data-urlencode "username=$USERNAME" \
    --data-urlencode "password=$PASSWORD" \
    https://login.1c.ru"$ACTION"

if ! grep -q "TGC" /tmp/cookies.txt ;then
    echo "Auth failed"
    exit 1
fi


VER1=${VER_1C//./_}

SERVERLINK=$(curl -s -G \
    -b /tmp/cookies.txt \
    --data-urlencode "nick=Platform83" \
    --data-urlencode "ver=$VER_1C" \
    --data-urlencode "path=Platform\\$VER1\\rpm64_$VER1.tar.gz" \
    https://releases.1c.ru/version_file | grep -oP '(?<=a href=")[^"]+(?=">Скачать дистрибутив<)')
	

mkdir ~/1c

echo -en "${YELLOW} Скачиваем сервер 1С.... ${NORMAL}\n"	
curl --fail -b /tmp/cookies.txt -o ~/1c/server64.tar.gz -L "$SERVERLINK"

if [ $CLIENT -eq 1 ];  then
	CLIENTLINK=$(curl -s -G \
		-b /tmp/cookies.txt \
		--data-urlencode "nick=Platform83" \
		--data-urlencode "ver=$VER_1C" \
		--data-urlencode "path=Platform\\$VER1\\client_$VER1.rpm64.tar.gz" \
		https://releases.1c.ru/version_file | grep -oP '(?<=a href=")[^"]+(?=">Скачать дистрибутив<)')

	echo -en "${YELLOW} Скачиваем толстый клиент 1С.... ${NORMAL}\n"	
	curl --fail -b /tmp/cookies.txt -o ~/1c/client64.tar.gz -L "$CLIENTLINK"
fi

rm /tmp/cookies.txt

#======================Устанвливаем платформу ==============================================================
TMP_PATH=~/1c

service srv1cv83 stop

yum remove 1C_Enterprise83-* -y
tar -xvzf $TMP_PATH/server64.tar.gz -C $TMP_PATH

if [ $CLIENT -eq 1 ];  then
	tar -xvzf $TMP_PATH/client64.tar.gz -C $TMP_PATH 
	rm -rf $TMP_PATH/1C_Enterprise83-thin*
fi

yum localinstall $TMP_PATH/*.rpm -y

rm -rf  $TMP_PATH/1C_Enterprise83-*
rm -rf $TMP_PATH/license-tools

service srv1cv83 start
service srv1cv83 status
systemctl restart httpd

#================================Настравиваем публикации ====================================================
echo -en "${YELLOW} Настройка Apache для 1С.... ${NORMAL}\n"
mkdir /var/www/html/1c
mkdir /var/www/html/1c/bases
mkdir /var/www/html/1c/vrd


echo 'LoadModule _1cws_module "/opt/1C/v8.3/x86_64/wsap24.so"' > /var/www/html/1c/1c.conf
echo '#            --------------    Каталог обновлений платформы     ----------------------' >> /var/www/html/1c/1c.conf

echo 'Alias "/install" "/home/files/install"' >> /var/www/html/1c/1c.conf
echo '<Directory "/home/files/install">' >> /var/www/html/1c/1c.conf
echo '    AllowOverride All' >> /var/www/html/1c/1c.conf
echo '    Options None' >> /var/www/html/1c/1c.conf
echo '    Require all granted' >> /var/www/html/1c/1c.conf   
echo '</Directory>' >> /var/www/html/1c/1c.conf

ln -s /var/www/html/1c/1c.conf /etc/httpd/conf.d/1c.conf

for DB in $DATABASES
do

	mkdir /var/www/html/1c/bases/$DB

	
	echo '#            --------------    '$DB'     ----------------------' >> /var/www/html/1c/1c.conf

	echo 'Alias "/'$DB'" "/var/www/html/1c/bases/'$DB'"' >> /var/www/html/1c/1c.conf
	echo '<Directory "/var/www/html/1c/bases/'$DB'">' >> /var/www/html/1c/1c.conf
	echo '    AllowOverride All' >> /var/www/html/1c/1c.conf
	echo '    Options None' >> /var/www/html/1c/1c.conf
	echo '    Require all granted' >> /var/www/html/1c/1c.conf
	echo '    SetHandler 1c-application' >> /var/www/html/1c/1c.conf
	echo '    ManagedApplicationDescriptor "/var/www/html/1c/vrd/'$DB'.vrd"' >> /var/www/html/1c/1c.conf
	echo '</Directory>' >> /var/www/html/1c/1c.conf

	


	echo '<?xml version="1.0" encoding="UTF-8"?>' > /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '<point xmlns="http://v8.1c.ru/8.2/virtual-resource-system"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '		xmlns:xs="http://www.w3.org/2001/XMLSchema"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '		base="/'$DB'"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '		ib="Srvr='$HOST_IP';Ref='$DB';"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '		pubdst="http://'$HOST_IP'/install/windows_8_3.zip"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '		pubdst32="http://'$HOST_IP'/install/windows_8_3.zip"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '		pubdst64="http://'$HOST_IP'/install/windows64full_8_3.zip"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '>' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '	<standardOdata enable="false"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '			reuseSessions="autouse"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '			sessionMaxAge="20"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '			poolSize="10"' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '			poolTimeout="5"/>' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	echo '</point>' >> /var/www/html/1c/vrd/tmp_vrd.vrd
	
	mv /var/www/html/1c/vrd/tmp_vrd.vrd /var/www/html/1c/vrd/$DB.vrd
done



chown -R apache:apache /var/www/html/1c

systemctl enable httpd.service
systemctl restart httpd.service

echo -en "${YELLOW} Создаем базы данных в кластере...... ${NORMAL}\n"

#=========================Создаем базы данных в кластере===========================================================
for DB in $DATABASES
do
/opt/1C/v8.3/x86_64/ras --daemon cluster
ID_CLUSTER=$(/opt/1C/v8.3/x86_64/rac cluster list |  grep -P 'cluster .+' | sed -r 's/^.+: //')

/opt/1C/v8.3/x86_64/rac infobase --cluster=$ID_CLUSTER \
		create --create-database \
		--name=$DB \
		--dbms=PostgreSQL \
		--db-server=127.0.0.1 \
		--db-name=$DB \
		--locale=ru \
		--db-user=postgres \
		--db-pwd=$PG_PASSWORD
done

#===========================================================================================

IFS=$OIFS

firewall-cmd --zone=public --add-port=1540-1541/tcp --permanent
firewall-cmd --zone=public --add-port=1560-1591/tcp --permanent
firewall-cmd --reload

reboot